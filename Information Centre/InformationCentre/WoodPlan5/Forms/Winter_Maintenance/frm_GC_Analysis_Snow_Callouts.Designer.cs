namespace WoodPlan5
{
    partial class frm_GC_Analysis_Snow_Callouts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Analysis_Snow_Callouts));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup1 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup2 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup3 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.fieldCalloutYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.dataSet_AT_Reports = new WoodPlan5.DataSet_AT_Reports();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04326GCAnalysisSnowCalloutsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSnowClearanceCallOutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCPONumberSuffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritMobileTelephoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSubContractorETA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitAborted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbortedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLabourVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorContactedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractorReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClientReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarkup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientHowSoon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoursWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccessAbortedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamPOFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimesheetSubmitted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedBySubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedBySubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimesheetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit25KgBags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlCompanies = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCompanyFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04237GCCompanyFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlStatuses = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnStatusFilterOK2 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.deFromDate = new DevExpress.XtraEditors.DateEdit();
            this.deToDate = new DevExpress.XtraEditors.DateEdit();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04001GCJobCallOutStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.sp04327GCAnalysisSnowCalloutsForAnalysisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldGCPONumberSuffix1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompanyName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientsSiteCode1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldGritMobileTelephoneNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOriginalSubContractorName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldReactive1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutStatusDescription1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutStatusOrder1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCallOutDateTime1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorETA1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletedTime1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldVisitAborted1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAbortedReason1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientPONumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLabourCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLabourVatRate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLabourSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOtherCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOtherSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTotalCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTotalSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientInvoiceNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorPaid1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRecordedByName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubContractorContactedByStaffName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPaidByName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDoNotPaySubContractor1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDoNotPaySubContractorReason1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDoNotInvoiceClient1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDoNotInvoiceClientReason1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNoAccess1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStartTime1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldProfit1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMarkup1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientPOID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNonStandardCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNonStandardSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccessComments1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientHowSoon1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHoursWorked1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRemarks1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNoAccessAbortedRate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTeamPOFileName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTeamInvoiceNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTimesheetSubmitted1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRecordedBySubContractorName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTimesheetNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAddedByWebsite1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalloutCount1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.sp04237_GC_Company_Filter_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiToggleAvailableColumnsVisibility = new DevExpress.XtraBars.BarButtonItem();
            this.pmChart = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiRotateAxis = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChartWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemStatusFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditStatusFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemCompanyFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditCompanyFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiReloadData = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAnalyse = new DevExpress.XtraBars.BarButtonItem();
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter();
            this.sp04326_GC_Analysis_Snow_Callouts_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04326_GC_Analysis_Snow_Callouts_ListTableAdapter();
            this.sp04327_GC_Analysis_Snow_Callouts_For_AnalysisTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04327_GC_Analysis_Snow_Callouts_For_AnalysisTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04326GCAnalysisSnowCalloutsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).BeginInit();
            this.popupContainerControlCompanies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlStatuses)).BeginInit();
            this.popupContainerControlStatuses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04327GCAnalysisSnowCalloutsForAnalysisBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 494);
            this.barDockControlBottom.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 494);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1293, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 494);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.bbiChartWizard,
            this.bbiRotateAxis,
            this.barEditItemStatusFilter,
            this.barEditItemCompanyFilter,
            this.bbiReloadData,
            this.bbiAnalyse,
            this.bbiToggleAvailableColumnsVisibility});
            this.barManager1.MaxItemId = 33;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditStatusFilter,
            this.repositoryItemPopupContainerEditCompanyFilter});
            // 
            // fieldCalloutYear
            // 
            this.fieldCalloutYear.AreaIndex = 9;
            this.fieldCalloutYear.Caption = "Callout Year";
            this.fieldCalloutYear.ExpandedInFieldsGroup = false;
            this.fieldCalloutYear.FieldName = "CallOutDateTime";
            this.fieldCalloutYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldCalloutYear.Name = "fieldCalloutYear";
            this.fieldCalloutYear.UnboundFieldName = "fieldCalloutYear";
            // 
            // fieldCalloutQuarter
            // 
            this.fieldCalloutQuarter.AreaIndex = 1;
            this.fieldCalloutQuarter.Caption = "Callout Quarter";
            this.fieldCalloutQuarter.ExpandedInFieldsGroup = false;
            this.fieldCalloutQuarter.FieldName = "CallOutDateTime";
            this.fieldCalloutQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldCalloutQuarter.Name = "fieldCalloutQuarter";
            this.fieldCalloutQuarter.UnboundFieldName = "fieldCalloutQuarter";
            this.fieldCalloutQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldCalloutQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCalloutQuarter.Visible = false;
            // 
            // fieldCalloutMonth
            // 
            this.fieldCalloutMonth.AreaIndex = 1;
            this.fieldCalloutMonth.Caption = "Callout Month";
            this.fieldCalloutMonth.ExpandedInFieldsGroup = false;
            this.fieldCalloutMonth.FieldName = "CallOutDateTime";
            this.fieldCalloutMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldCalloutMonth.Name = "fieldCalloutMonth";
            this.fieldCalloutMonth.UnboundFieldName = "fieldCalloutMonth";
            this.fieldCalloutMonth.Visible = false;
            // 
            // fieldCalloutWeek
            // 
            this.fieldCalloutWeek.AreaIndex = 0;
            this.fieldCalloutWeek.Caption = "Callout Week";
            this.fieldCalloutWeek.ExpandedInFieldsGroup = false;
            this.fieldCalloutWeek.FieldName = "CallOutDateTime";
            this.fieldCalloutWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldCalloutWeek.Name = "fieldCalloutWeek";
            this.fieldCalloutWeek.UnboundFieldName = "fieldCalloutWeek";
            this.fieldCalloutWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldCalloutWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCalloutWeek.Visible = false;
            // 
            // fieldStartYear
            // 
            this.fieldStartYear.AreaIndex = 11;
            this.fieldStartYear.Caption = "Start Year";
            this.fieldStartYear.ExpandedInFieldsGroup = false;
            this.fieldStartYear.FieldName = "StartTime";
            this.fieldStartYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldStartYear.Name = "fieldStartYear";
            this.fieldStartYear.UnboundFieldName = "fieldStartYear";
            // 
            // fieldStartQuarter
            // 
            this.fieldStartQuarter.AreaIndex = 2;
            this.fieldStartQuarter.Caption = "Start Quarter";
            this.fieldStartQuarter.ExpandedInFieldsGroup = false;
            this.fieldStartQuarter.FieldName = "StartTime";
            this.fieldStartQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldStartQuarter.Name = "fieldStartQuarter";
            this.fieldStartQuarter.UnboundFieldName = "fieldStartQuarter";
            this.fieldStartQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldStartQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldStartQuarter.Visible = false;
            // 
            // fieldStartMonth
            // 
            this.fieldStartMonth.AreaIndex = 2;
            this.fieldStartMonth.Caption = "Start Month";
            this.fieldStartMonth.ExpandedInFieldsGroup = false;
            this.fieldStartMonth.FieldName = "StartTime";
            this.fieldStartMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldStartMonth.Name = "fieldStartMonth";
            this.fieldStartMonth.UnboundFieldName = "fieldStartMonth";
            this.fieldStartMonth.Visible = false;
            // 
            // fieldStartWeek
            // 
            this.fieldStartWeek.AreaIndex = 0;
            this.fieldStartWeek.Caption = "Start Week";
            this.fieldStartWeek.ExpandedInFieldsGroup = false;
            this.fieldStartWeek.FieldName = "StartTime";
            this.fieldStartWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldStartWeek.Name = "fieldStartWeek";
            this.fieldStartWeek.UnboundFieldName = "fieldStartWeek";
            this.fieldStartWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldStartWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldStartWeek.Visible = false;
            // 
            // fieldCompletedYear
            // 
            this.fieldCompletedYear.AreaIndex = 13;
            this.fieldCompletedYear.Caption = "Completed Year";
            this.fieldCompletedYear.ExpandedInFieldsGroup = false;
            this.fieldCompletedYear.FieldName = "CompletedTime";
            this.fieldCompletedYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldCompletedYear.Name = "fieldCompletedYear";
            this.fieldCompletedYear.UnboundFieldName = "fieldCompletedYear";
            // 
            // fieldCompletedQuarter
            // 
            this.fieldCompletedQuarter.AreaIndex = 3;
            this.fieldCompletedQuarter.Caption = "Completed Quarter";
            this.fieldCompletedQuarter.ExpandedInFieldsGroup = false;
            this.fieldCompletedQuarter.FieldName = "CompletedTime";
            this.fieldCompletedQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldCompletedQuarter.Name = "fieldCompletedQuarter";
            this.fieldCompletedQuarter.UnboundFieldName = "fieldCompletedQuarter";
            this.fieldCompletedQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldCompletedQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCompletedQuarter.Visible = false;
            // 
            // fieldCompletedMonth
            // 
            this.fieldCompletedMonth.AreaIndex = 3;
            this.fieldCompletedMonth.Caption = "Completed Month";
            this.fieldCompletedMonth.ExpandedInFieldsGroup = false;
            this.fieldCompletedMonth.FieldName = "CompletedTime";
            this.fieldCompletedMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldCompletedMonth.Name = "fieldCompletedMonth";
            this.fieldCompletedMonth.UnboundFieldName = "fieldCompletedMonth";
            this.fieldCompletedMonth.Visible = false;
            // 
            // fieldCompletedWeek
            // 
            this.fieldCompletedWeek.AreaIndex = 0;
            this.fieldCompletedWeek.Caption = "Completed Week";
            this.fieldCompletedWeek.ExpandedInFieldsGroup = false;
            this.fieldCompletedWeek.FieldName = "CompletedTime";
            this.fieldCompletedWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldCompletedWeek.Name = "fieldCompletedWeek";
            this.fieldCompletedWeek.UnboundFieldName = "fieldCompletedWeek";
            this.fieldCompletedWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldCompletedWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCompletedWeek.Visible = false;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "ProjectManDataSet";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_Reports
            // 
            this.dataSet_AT_Reports.DataSetName = "DataSet_AT_Reports";
            this.dataSet_AT_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("eca7f263-f03a-476e-be2c-7e2164986cdc");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(410, 200);
            this.dockPanel1.Size = new System.Drawing.Size(410, 494);
            this.dockPanel1.Text = "Data Supply";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.standaloneBarDockControl1);
            this.dockPanel1_Container.Controls.Add(this.gridControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(404, 462);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(404, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl1.BackgroundImage")));
            this.gridControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gridControl1.DataSource = this.sp04326GCAnalysisSnowCalloutsListBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 42);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEdit25KgBags,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditNumeric2DP});
            this.gridControl1.Size = new System.Drawing.Size(404, 420);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04326GCAnalysisSnowCalloutsListBindingSource
            // 
            this.sp04326GCAnalysisSnowCalloutsListBindingSource.DataMember = "sp04326_GC_Analysis_Snow_Callouts_List";
            this.sp04326GCAnalysisSnowCalloutsListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSnowClearanceCallOutID,
            this.colSnowClearanceSiteContractID,
            this.colGCPONumberSuffix,
            this.colSiteID,
            this.colSiteName,
            this.colClientID,
            this.colClientName,
            this.colCompanyID,
            this.colCompanyName,
            this.colSiteXCoordinate,
            this.colSiteYCoordinate,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colSiteTelephone,
            this.colSiteEmail,
            this.colClientsSiteCode,
            this.colSubContractorID,
            this.colSubContractorName,
            this.colGritMobileTelephoneNumber,
            this.colOriginalSubContractorID,
            this.colOriginalSubContractorName,
            this.colReactive,
            this.colJobStatusID,
            this.colCalloutStatusDescription,
            this.colCalloutStatusOrder,
            this.colCallOutDateTime,
            this.colSubContractorETA,
            this.colCompletedTime,
            this.colRecordedByStaffID,
            this.colVisitAborted,
            this.colAbortedReason,
            this.colClientPONumber,
            this.colLabourCost,
            this.colLabourVatRate,
            this.colLabourSell,
            this.colOtherCost,
            this.colOtherSell,
            this.colTotalCost,
            this.colTotalSell,
            this.colClientInvoiceNumber,
            this.colSubContractorPaid,
            this.colRecordedByName,
            this.colSubContractorContactedByStaffID,
            this.colSubContractorContactedByStaffName,
            this.colPaidByStaffID,
            this.colPaidByName,
            this.colDoNotPaySubContractor,
            this.colDoNotPaySubContractorReason,
            this.colDoNotInvoiceClient,
            this.colDoNotInvoiceClientReason,
            this.colNoAccess,
            this.colStartTime,
            this.colProfit,
            this.colMarkup,
            this.colClientPOID,
            this.colNonStandardCost,
            this.colNonStandardSell,
            this.colAccessComments,
            this.colClientHowSoon,
            this.colHoursWorked,
            this.colRemarks2,
            this.colNoAccessAbortedRate,
            this.colTeamInvoiceNumber,
            this.colTeamPOFileName,
            this.colTimesheetSubmitted,
            this.colRecordedBySubContractorID,
            this.colRecordedBySubContractorName,
            this.colTimesheetNumber,
            this.colAddedByWebsite});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 513, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCallOutDateTime, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSnowClearanceCallOutID
            // 
            this.colSnowClearanceCallOutID.Caption = "GC PO No";
            this.colSnowClearanceCallOutID.FieldName = "SnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.Name = "colSnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutID.Visible = true;
            this.colSnowClearanceCallOutID.VisibleIndex = 2;
            this.colSnowClearanceCallOutID.Width = 71;
            // 
            // colSnowClearanceSiteContractID
            // 
            this.colSnowClearanceSiteContractID.Caption = "Snow Clearance Contract ID";
            this.colSnowClearanceSiteContractID.FieldName = "SnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.Name = "colSnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSiteContractID.Width = 157;
            // 
            // colGCPONumberSuffix
            // 
            this.colGCPONumberSuffix.Caption = "GC PO Suffix";
            this.colGCPONumberSuffix.FieldName = "GCPONumberSuffix";
            this.colGCPONumberSuffix.Name = "colGCPONumberSuffix";
            this.colGCPONumberSuffix.OptionsColumn.AllowEdit = false;
            this.colGCPONumberSuffix.OptionsColumn.AllowFocus = false;
            this.colGCPONumberSuffix.OptionsColumn.ReadOnly = true;
            this.colGCPONumberSuffix.Visible = true;
            this.colGCPONumberSuffix.VisibleIndex = 3;
            this.colGCPONumberSuffix.Width = 84;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 9;
            this.colSiteName.Width = 157;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 8;
            this.colClientName.Width = 120;
            // 
            // colCompanyID
            // 
            this.colCompanyID.Caption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 80;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 18;
            this.colCompanyName.Width = 137;
            // 
            // colSiteXCoordinate
            // 
            this.colSiteXCoordinate.Caption = "Site X Coordinate";
            this.colSiteXCoordinate.FieldName = "SiteXCoordinate";
            this.colSiteXCoordinate.Name = "colSiteXCoordinate";
            this.colSiteXCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteXCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteXCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteXCoordinate.Width = 104;
            // 
            // colSiteYCoordinate
            // 
            this.colSiteYCoordinate.Caption = "Site Y Coordinate";
            this.colSiteYCoordinate.FieldName = "SiteYCoordinate";
            this.colSiteYCoordinate.Name = "colSiteYCoordinate";
            this.colSiteYCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteYCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteYCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteYCoordinate.Width = 104;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Location X";
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 91;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Location Y";
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 91;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Site Telephone ";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Width = 95;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Site Email";
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.AllowEdit = false;
            this.colSiteEmail.OptionsColumn.AllowFocus = false;
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            this.colSiteEmail.Visible = true;
            this.colSiteEmail.VisibleIndex = 17;
            this.colSiteEmail.Width = 116;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Width = 105;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Team Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 5;
            this.colSubContractorName.Width = 122;
            // 
            // colGritMobileTelephoneNumber
            // 
            this.colGritMobileTelephoneNumber.Caption = "Team Mobile No";
            this.colGritMobileTelephoneNumber.FieldName = "GritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.Name = "colGritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.OptionsColumn.AllowEdit = false;
            this.colGritMobileTelephoneNumber.OptionsColumn.AllowFocus = false;
            this.colGritMobileTelephoneNumber.OptionsColumn.ReadOnly = true;
            this.colGritMobileTelephoneNumber.Width = 96;
            // 
            // colOriginalSubContractorID
            // 
            this.colOriginalSubContractorID.Caption = "Original Team ID";
            this.colOriginalSubContractorID.FieldName = "OriginalSubContractorID";
            this.colOriginalSubContractorID.Name = "colOriginalSubContractorID";
            this.colOriginalSubContractorID.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorID.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorID.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorID.Width = 100;
            // 
            // colOriginalSubContractorName
            // 
            this.colOriginalSubContractorName.Caption = "Original Team Name";
            this.colOriginalSubContractorName.FieldName = "OriginalSubContractorName";
            this.colOriginalSubContractorName.Name = "colOriginalSubContractorName";
            this.colOriginalSubContractorName.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorName.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorName.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOriginalSubContractorName.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 4;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colCalloutStatusDescription
            // 
            this.colCalloutStatusDescription.Caption = "Status Description";
            this.colCalloutStatusDescription.FieldName = "CalloutStatusDescription";
            this.colCalloutStatusDescription.Name = "colCalloutStatusDescription";
            this.colCalloutStatusDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCalloutStatusDescription.Visible = true;
            this.colCalloutStatusDescription.VisibleIndex = 1;
            this.colCalloutStatusDescription.Width = 196;
            // 
            // colCalloutStatusOrder
            // 
            this.colCalloutStatusOrder.Caption = "Status Order";
            this.colCalloutStatusOrder.FieldName = "CalloutStatusOrder";
            this.colCalloutStatusOrder.Name = "colCalloutStatusOrder";
            this.colCalloutStatusOrder.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusOrder.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusOrder.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusOrder.Width = 83;
            // 
            // colCallOutDateTime
            // 
            this.colCallOutDateTime.Caption = "Callout Date\\Time";
            this.colCallOutDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCallOutDateTime.FieldName = "CallOutDateTime";
            this.colCallOutDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCallOutDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCallOutDateTime.Name = "colCallOutDateTime";
            this.colCallOutDateTime.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCallOutDateTime.Visible = true;
            this.colCallOutDateTime.VisibleIndex = 0;
            this.colCallOutDateTime.Width = 121;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colSubContractorETA
            // 
            this.colSubContractorETA.Caption = "Team ETA";
            this.colSubContractorETA.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSubContractorETA.FieldName = "SubContractorETA";
            this.colSubContractorETA.Name = "colSubContractorETA";
            this.colSubContractorETA.OptionsColumn.AllowEdit = false;
            this.colSubContractorETA.OptionsColumn.AllowFocus = false;
            this.colSubContractorETA.OptionsColumn.ReadOnly = true;
            this.colSubContractorETA.Visible = true;
            this.colSubContractorETA.VisibleIndex = 6;
            this.colSubContractorETA.Width = 117;
            // 
            // colCompletedTime
            // 
            this.colCompletedTime.Caption = "Completed Date\\Time";
            this.colCompletedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCompletedTime.FieldName = "CompletedTime";
            this.colCompletedTime.Name = "colCompletedTime";
            this.colCompletedTime.OptionsColumn.AllowEdit = false;
            this.colCompletedTime.OptionsColumn.AllowFocus = false;
            this.colCompletedTime.OptionsColumn.ReadOnly = true;
            this.colCompletedTime.Visible = true;
            this.colCompletedTime.VisibleIndex = 15;
            this.colCompletedTime.Width = 124;
            // 
            // colRecordedByStaffID
            // 
            this.colRecordedByStaffID.Caption = "Recorded By Staff ID";
            this.colRecordedByStaffID.FieldName = "RecordedByStaffID";
            this.colRecordedByStaffID.Name = "colRecordedByStaffID";
            this.colRecordedByStaffID.OptionsColumn.AllowEdit = false;
            this.colRecordedByStaffID.OptionsColumn.AllowFocus = false;
            this.colRecordedByStaffID.OptionsColumn.ReadOnly = true;
            this.colRecordedByStaffID.Width = 123;
            // 
            // colVisitAborted
            // 
            this.colVisitAborted.Caption = "Aborted";
            this.colVisitAborted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colVisitAborted.FieldName = "VisitAborted";
            this.colVisitAborted.Name = "colVisitAborted";
            this.colVisitAborted.OptionsColumn.AllowEdit = false;
            this.colVisitAborted.OptionsColumn.AllowFocus = false;
            this.colVisitAborted.OptionsColumn.ReadOnly = true;
            this.colVisitAborted.Visible = true;
            this.colVisitAborted.VisibleIndex = 12;
            // 
            // colAbortedReason
            // 
            this.colAbortedReason.Caption = "Aborted Reason";
            this.colAbortedReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colAbortedReason.FieldName = "AbortedReason";
            this.colAbortedReason.Name = "colAbortedReason";
            this.colAbortedReason.OptionsColumn.ReadOnly = true;
            this.colAbortedReason.Visible = true;
            this.colAbortedReason.VisibleIndex = 13;
            this.colAbortedReason.Width = 99;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO Number";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 10;
            this.colClientPONumber.Width = 105;
            // 
            // colLabourCost
            // 
            this.colLabourCost.Caption = "Labour Cost";
            this.colLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colLabourCost.FieldName = "LabourCost";
            this.colLabourCost.Name = "colLabourCost";
            this.colLabourCost.OptionsColumn.AllowEdit = false;
            this.colLabourCost.OptionsColumn.AllowFocus = false;
            this.colLabourCost.OptionsColumn.ReadOnly = true;
            this.colLabourCost.Visible = true;
            this.colLabourCost.VisibleIndex = 20;
            this.colLabourCost.Width = 79;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colLabourVatRate
            // 
            this.colLabourVatRate.Caption = "Labout VAT Rate";
            this.colLabourVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colLabourVatRate.FieldName = "LabourVatRate";
            this.colLabourVatRate.Name = "colLabourVatRate";
            this.colLabourVatRate.OptionsColumn.AllowEdit = false;
            this.colLabourVatRate.OptionsColumn.AllowFocus = false;
            this.colLabourVatRate.OptionsColumn.ReadOnly = true;
            this.colLabourVatRate.Visible = true;
            this.colLabourVatRate.VisibleIndex = 21;
            this.colLabourVatRate.Width = 102;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P2";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colLabourSell
            // 
            this.colLabourSell.Caption = "Labour Sell";
            this.colLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colLabourSell.FieldName = "LabourSell";
            this.colLabourSell.Name = "colLabourSell";
            this.colLabourSell.OptionsColumn.AllowEdit = false;
            this.colLabourSell.OptionsColumn.AllowFocus = false;
            this.colLabourSell.OptionsColumn.ReadOnly = true;
            this.colLabourSell.Visible = true;
            this.colLabourSell.VisibleIndex = 22;
            // 
            // colOtherCost
            // 
            this.colOtherCost.Caption = "Other Cost";
            this.colOtherCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherCost.FieldName = "OtherCost";
            this.colOtherCost.Name = "colOtherCost";
            this.colOtherCost.OptionsColumn.AllowEdit = false;
            this.colOtherCost.OptionsColumn.AllowFocus = false;
            this.colOtherCost.OptionsColumn.ReadOnly = true;
            this.colOtherCost.Visible = true;
            this.colOtherCost.VisibleIndex = 23;
            // 
            // colOtherSell
            // 
            this.colOtherSell.Caption = "Other Sell";
            this.colOtherSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherSell.FieldName = "OtherSell";
            this.colOtherSell.Name = "colOtherSell";
            this.colOtherSell.OptionsColumn.AllowEdit = false;
            this.colOtherSell.OptionsColumn.AllowFocus = false;
            this.colOtherSell.OptionsColumn.ReadOnly = true;
            this.colOtherSell.Visible = true;
            this.colOtherSell.VisibleIndex = 24;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 25;
            // 
            // colTotalSell
            // 
            this.colTotalSell.Caption = "Total Sell ";
            this.colTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalSell.FieldName = "TotalSell";
            this.colTotalSell.Name = "colTotalSell";
            this.colTotalSell.OptionsColumn.AllowEdit = false;
            this.colTotalSell.OptionsColumn.AllowFocus = false;
            this.colTotalSell.OptionsColumn.ReadOnly = true;
            this.colTotalSell.Visible = true;
            this.colTotalSell.VisibleIndex = 26;
            // 
            // colClientInvoiceNumber
            // 
            this.colClientInvoiceNumber.Caption = "Client Invoice Number";
            this.colClientInvoiceNumber.FieldName = "ClientInvoiceNumber";
            this.colClientInvoiceNumber.Name = "colClientInvoiceNumber";
            this.colClientInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientInvoiceNumber.Visible = true;
            this.colClientInvoiceNumber.VisibleIndex = 31;
            this.colClientInvoiceNumber.Width = 126;
            // 
            // colSubContractorPaid
            // 
            this.colSubContractorPaid.Caption = "Team Paid";
            this.colSubContractorPaid.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSubContractorPaid.FieldName = "SubContractorPaid";
            this.colSubContractorPaid.Name = "colSubContractorPaid";
            this.colSubContractorPaid.OptionsColumn.AllowEdit = false;
            this.colSubContractorPaid.OptionsColumn.AllowFocus = false;
            this.colSubContractorPaid.OptionsColumn.ReadOnly = true;
            this.colSubContractorPaid.Visible = true;
            this.colSubContractorPaid.VisibleIndex = 32;
            // 
            // colRecordedByName
            // 
            this.colRecordedByName.Caption = "Recorded By Staff";
            this.colRecordedByName.FieldName = "RecordedByName";
            this.colRecordedByName.Name = "colRecordedByName";
            this.colRecordedByName.OptionsColumn.AllowEdit = false;
            this.colRecordedByName.OptionsColumn.AllowFocus = false;
            this.colRecordedByName.OptionsColumn.ReadOnly = true;
            this.colRecordedByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRecordedByName.Visible = true;
            this.colRecordedByName.VisibleIndex = 33;
            this.colRecordedByName.Width = 112;
            // 
            // colSubContractorContactedByStaffID
            // 
            this.colSubContractorContactedByStaffID.Caption = "Team Contacted By Staff ID";
            this.colSubContractorContactedByStaffID.FieldName = "SubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.Name = "colSubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.OptionsColumn.AllowEdit = false;
            this.colSubContractorContactedByStaffID.OptionsColumn.AllowFocus = false;
            this.colSubContractorContactedByStaffID.OptionsColumn.ReadOnly = true;
            this.colSubContractorContactedByStaffID.Width = 156;
            // 
            // colSubContractorContactedByStaffName
            // 
            this.colSubContractorContactedByStaffName.Caption = "Team Contacted By Staff Name";
            this.colSubContractorContactedByStaffName.FieldName = "SubContractorContactedByStaffName";
            this.colSubContractorContactedByStaffName.Name = "colSubContractorContactedByStaffName";
            this.colSubContractorContactedByStaffName.OptionsColumn.AllowEdit = false;
            this.colSubContractorContactedByStaffName.OptionsColumn.AllowFocus = false;
            this.colSubContractorContactedByStaffName.OptionsColumn.ReadOnly = true;
            this.colSubContractorContactedByStaffName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubContractorContactedByStaffName.Visible = true;
            this.colSubContractorContactedByStaffName.VisibleIndex = 35;
            this.colSubContractorContactedByStaffName.Width = 172;
            // 
            // colPaidByStaffID
            // 
            this.colPaidByStaffID.Caption = "Paid By Staff ID";
            this.colPaidByStaffID.FieldName = "PaidByStaffID";
            this.colPaidByStaffID.Name = "colPaidByStaffID";
            this.colPaidByStaffID.OptionsColumn.AllowEdit = false;
            this.colPaidByStaffID.OptionsColumn.AllowFocus = false;
            this.colPaidByStaffID.OptionsColumn.ReadOnly = true;
            this.colPaidByStaffID.Width = 97;
            // 
            // colPaidByName
            // 
            this.colPaidByName.Caption = "Paid By Staff Name";
            this.colPaidByName.FieldName = "PaidByName";
            this.colPaidByName.Name = "colPaidByName";
            this.colPaidByName.OptionsColumn.AllowEdit = false;
            this.colPaidByName.OptionsColumn.AllowFocus = false;
            this.colPaidByName.OptionsColumn.ReadOnly = true;
            this.colPaidByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPaidByName.Visible = true;
            this.colPaidByName.VisibleIndex = 36;
            this.colPaidByName.Width = 113;
            // 
            // colDoNotPaySubContractor
            // 
            this.colDoNotPaySubContractor.Caption = "Do Not Pay Team";
            this.colDoNotPaySubContractor.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPaySubContractor.FieldName = "DoNotPaySubContractor";
            this.colDoNotPaySubContractor.Name = "colDoNotPaySubContractor";
            this.colDoNotPaySubContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPaySubContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPaySubContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractor.Visible = true;
            this.colDoNotPaySubContractor.VisibleIndex = 37;
            this.colDoNotPaySubContractor.Width = 104;
            // 
            // colDoNotPaySubContractorReason
            // 
            this.colDoNotPaySubContractorReason.Caption = "Do Not Pay Team Reason";
            this.colDoNotPaySubContractorReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDoNotPaySubContractorReason.FieldName = "DoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.Name = "colDoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractorReason.Visible = true;
            this.colDoNotPaySubContractorReason.VisibleIndex = 38;
            this.colDoNotPaySubContractorReason.Width = 143;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 39;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colDoNotInvoiceClientReason
            // 
            this.colDoNotInvoiceClientReason.Caption = "Do Not Invoice Client Reason";
            this.colDoNotInvoiceClientReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDoNotInvoiceClientReason.FieldName = "DoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.Name = "colDoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClientReason.Visible = true;
            this.colDoNotInvoiceClientReason.VisibleIndex = 40;
            this.colDoNotInvoiceClientReason.Width = 161;
            // 
            // colNoAccess
            // 
            this.colNoAccess.Caption = "No Access";
            this.colNoAccess.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoAccess.FieldName = "NoAccess";
            this.colNoAccess.Name = "colNoAccess";
            this.colNoAccess.OptionsColumn.AllowEdit = false;
            this.colNoAccess.OptionsColumn.AllowFocus = false;
            this.colNoAccess.OptionsColumn.ReadOnly = true;
            this.colNoAccess.Visible = true;
            this.colNoAccess.VisibleIndex = 11;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Date\\Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 14;
            this.colStartTime.Width = 105;
            // 
            // colProfit
            // 
            this.colProfit.Caption = "Profit";
            this.colProfit.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colProfit.FieldName = "Profit";
            this.colProfit.Name = "colProfit";
            this.colProfit.OptionsColumn.AllowEdit = false;
            this.colProfit.OptionsColumn.AllowFocus = false;
            this.colProfit.OptionsColumn.ReadOnly = true;
            this.colProfit.Visible = true;
            this.colProfit.VisibleIndex = 27;
            // 
            // colMarkup
            // 
            this.colMarkup.Caption = "% Markup";
            this.colMarkup.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colMarkup.FieldName = "Markup";
            this.colMarkup.Name = "colMarkup";
            this.colMarkup.OptionsColumn.AllowEdit = false;
            this.colMarkup.OptionsColumn.AllowFocus = false;
            this.colMarkup.OptionsColumn.ReadOnly = true;
            this.colMarkup.Visible = true;
            this.colMarkup.VisibleIndex = 28;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 79;
            // 
            // colNonStandardCost
            // 
            this.colNonStandardCost.Caption = "Non Standard Cost";
            this.colNonStandardCost.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardCost.FieldName = "NonStandardCost";
            this.colNonStandardCost.Name = "colNonStandardCost";
            this.colNonStandardCost.OptionsColumn.AllowEdit = false;
            this.colNonStandardCost.OptionsColumn.AllowFocus = false;
            this.colNonStandardCost.OptionsColumn.ReadOnly = true;
            this.colNonStandardCost.Visible = true;
            this.colNonStandardCost.VisibleIndex = 29;
            this.colNonStandardCost.Width = 112;
            // 
            // colNonStandardSell
            // 
            this.colNonStandardSell.Caption = "Non Standard Sell";
            this.colNonStandardSell.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardSell.FieldName = "NonStandardSell";
            this.colNonStandardSell.Name = "colNonStandardSell";
            this.colNonStandardSell.OptionsColumn.AllowEdit = false;
            this.colNonStandardSell.OptionsColumn.AllowFocus = false;
            this.colNonStandardSell.OptionsColumn.ReadOnly = true;
            this.colNonStandardSell.Visible = true;
            this.colNonStandardSell.VisibleIndex = 30;
            this.colNonStandardSell.Width = 106;
            // 
            // colAccessComments
            // 
            this.colAccessComments.Caption = "Access Comments";
            this.colAccessComments.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colAccessComments.FieldName = "AccessComments";
            this.colAccessComments.Name = "colAccessComments";
            this.colAccessComments.OptionsColumn.ReadOnly = true;
            this.colAccessComments.Visible = true;
            this.colAccessComments.VisibleIndex = 41;
            this.colAccessComments.Width = 107;
            // 
            // colClientHowSoon
            // 
            this.colClientHowSoon.Caption = "How Soon";
            this.colClientHowSoon.FieldName = "ClientHowSoon";
            this.colClientHowSoon.Name = "colClientHowSoon";
            this.colClientHowSoon.OptionsColumn.AllowEdit = false;
            this.colClientHowSoon.OptionsColumn.AllowFocus = false;
            this.colClientHowSoon.OptionsColumn.ReadOnly = true;
            this.colClientHowSoon.Visible = true;
            this.colClientHowSoon.VisibleIndex = 7;
            // 
            // colHoursWorked
            // 
            this.colHoursWorked.Caption = "Hours Worked";
            this.colHoursWorked.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colHoursWorked.FieldName = "HoursWorked";
            this.colHoursWorked.Name = "colHoursWorked";
            this.colHoursWorked.OptionsColumn.AllowEdit = false;
            this.colHoursWorked.OptionsColumn.AllowFocus = false;
            this.colHoursWorked.OptionsColumn.ReadOnly = true;
            this.colHoursWorked.Visible = true;
            this.colHoursWorked.VisibleIndex = 16;
            this.colHoursWorked.Width = 89;
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 47;
            // 
            // colNoAccessAbortedRate
            // 
            this.colNoAccessAbortedRate.Caption = "No Access \\ Aborted Rate";
            this.colNoAccessAbortedRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colNoAccessAbortedRate.FieldName = "NoAccessAbortedRate";
            this.colNoAccessAbortedRate.Name = "colNoAccessAbortedRate";
            this.colNoAccessAbortedRate.OptionsColumn.AllowEdit = false;
            this.colNoAccessAbortedRate.OptionsColumn.AllowFocus = false;
            this.colNoAccessAbortedRate.OptionsColumn.ReadOnly = true;
            this.colNoAccessAbortedRate.Visible = true;
            this.colNoAccessAbortedRate.VisibleIndex = 19;
            this.colNoAccessAbortedRate.Width = 145;
            // 
            // colTeamInvoiceNumber
            // 
            this.colTeamInvoiceNumber.Caption = "Team Invoice No.";
            this.colTeamInvoiceNumber.FieldName = "TeamInvoiceNumber";
            this.colTeamInvoiceNumber.Name = "colTeamInvoiceNumber";
            this.colTeamInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colTeamInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colTeamInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colTeamInvoiceNumber.Visible = true;
            this.colTeamInvoiceNumber.VisibleIndex = 44;
            this.colTeamInvoiceNumber.Width = 105;
            // 
            // colTeamPOFileName
            // 
            this.colTeamPOFileName.Caption = "Team P.O. File";
            this.colTeamPOFileName.FieldName = "TeamPOFileName";
            this.colTeamPOFileName.Name = "colTeamPOFileName";
            this.colTeamPOFileName.Visible = true;
            this.colTeamPOFileName.VisibleIndex = 45;
            this.colTeamPOFileName.Width = 91;
            // 
            // colTimesheetSubmitted
            // 
            this.colTimesheetSubmitted.Caption = "Timesheet Submitted";
            this.colTimesheetSubmitted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTimesheetSubmitted.FieldName = "TimesheetSubmitted";
            this.colTimesheetSubmitted.Name = "colTimesheetSubmitted";
            this.colTimesheetSubmitted.Visible = true;
            this.colTimesheetSubmitted.VisibleIndex = 42;
            this.colTimesheetSubmitted.Width = 121;
            // 
            // colRecordedBySubContractorID
            // 
            this.colRecordedBySubContractorID.Caption = "Recorded By Team ID";
            this.colRecordedBySubContractorID.FieldName = "RecordedBySubContractorID";
            this.colRecordedBySubContractorID.Name = "colRecordedBySubContractorID";
            this.colRecordedBySubContractorID.OptionsColumn.AllowEdit = false;
            this.colRecordedBySubContractorID.OptionsColumn.AllowFocus = false;
            this.colRecordedBySubContractorID.OptionsColumn.ReadOnly = true;
            this.colRecordedBySubContractorID.Width = 125;
            // 
            // colRecordedBySubContractorName
            // 
            this.colRecordedBySubContractorName.Caption = "Recorded By Team";
            this.colRecordedBySubContractorName.FieldName = "RecordedBySubContractorName";
            this.colRecordedBySubContractorName.Name = "colRecordedBySubContractorName";
            this.colRecordedBySubContractorName.OptionsColumn.AllowEdit = false;
            this.colRecordedBySubContractorName.OptionsColumn.AllowFocus = false;
            this.colRecordedBySubContractorName.OptionsColumn.ReadOnly = true;
            this.colRecordedBySubContractorName.Visible = true;
            this.colRecordedBySubContractorName.VisibleIndex = 34;
            this.colRecordedBySubContractorName.Width = 111;
            // 
            // colTimesheetNumber
            // 
            this.colTimesheetNumber.Caption = "Timesheet Number";
            this.colTimesheetNumber.FieldName = "TimesheetNumber";
            this.colTimesheetNumber.Name = "colTimesheetNumber";
            this.colTimesheetNumber.OptionsColumn.ReadOnly = true;
            this.colTimesheetNumber.Visible = true;
            this.colTimesheetNumber.VisibleIndex = 43;
            this.colTimesheetNumber.Width = 110;
            // 
            // colAddedByWebsite
            // 
            this.colAddedByWebsite.Caption = "Added By Website";
            this.colAddedByWebsite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAddedByWebsite.FieldName = "AddedByWebsite";
            this.colAddedByWebsite.Name = "colAddedByWebsite";
            this.colAddedByWebsite.OptionsColumn.AllowEdit = false;
            this.colAddedByWebsite.OptionsColumn.AllowFocus = false;
            this.colAddedByWebsite.OptionsColumn.ReadOnly = true;
            this.colAddedByWebsite.Visible = true;
            this.colAddedByWebsite.VisibleIndex = 46;
            this.colAddedByWebsite.Width = 109;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit25KgBags
            // 
            this.repositoryItemTextEdit25KgBags.AutoHeight = false;
            this.repositoryItemTextEdit25KgBags.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25KgBags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KgBags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KgBags.Name = "repositoryItemTextEdit25KgBags";
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(410, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlCompanies);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlStatuses);
            this.splitContainerControl1.Panel1.Controls.Add(this.pivotGridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.chartControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(883, 494);
            this.splitContainerControl1.SplitterPosition = 388;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl2";
            // 
            // popupContainerControlCompanies
            // 
            this.popupContainerControlCompanies.Controls.Add(this.btnCompanyFilterOK);
            this.popupContainerControlCompanies.Controls.Add(this.gridControl5);
            this.popupContainerControlCompanies.Location = new System.Drawing.Point(363, 215);
            this.popupContainerControlCompanies.Name = "popupContainerControlCompanies";
            this.popupContainerControlCompanies.Size = new System.Drawing.Size(189, 163);
            this.popupContainerControlCompanies.TabIndex = 35;
            // 
            // btnCompanyFilterOK
            // 
            this.btnCompanyFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCompanyFilterOK.Location = new System.Drawing.Point(3, 138);
            this.btnCompanyFilterOK.Name = "btnCompanyFilterOK";
            this.btnCompanyFilterOK.Size = new System.Drawing.Size(77, 22);
            this.btnCompanyFilterOK.TabIndex = 18;
            this.btnCompanyFilterOK.Text = "OK";
            this.btnCompanyFilterOK.Click += new System.EventHandler(this.btnCompanyFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp04237GCCompanyFilterListBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(183, 133);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04237GCCompanyFilterListBindingSource
            // 
            this.sp04237GCCompanyFilterListBindingSource.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.colCompanyCode,
            this.colCompanyOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Company ID";
            this.gridColumn5.FieldName = "CompanyID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 80;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Company Name";
            this.gridColumn6.FieldName = "CompanyName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 152;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyOrder
            // 
            this.colCompanyOrder.Caption = "Order";
            this.colCompanyOrder.FieldName = "CompanyOrder";
            this.colCompanyOrder.Name = "colCompanyOrder";
            this.colCompanyOrder.OptionsColumn.AllowEdit = false;
            this.colCompanyOrder.OptionsColumn.AllowFocus = false;
            this.colCompanyOrder.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerControlStatuses
            // 
            this.popupContainerControlStatuses.Controls.Add(this.btnStatusFilterOK2);
            this.popupContainerControlStatuses.Controls.Add(this.groupControl2);
            this.popupContainerControlStatuses.Controls.Add(this.gridControl3);
            this.popupContainerControlStatuses.Location = new System.Drawing.Point(33, 97);
            this.popupContainerControlStatuses.Name = "popupContainerControlStatuses";
            this.popupContainerControlStatuses.Size = new System.Drawing.Size(312, 268);
            this.popupContainerControlStatuses.TabIndex = 15;
            // 
            // btnStatusFilterOK2
            // 
            this.btnStatusFilterOK2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStatusFilterOK2.Location = new System.Drawing.Point(3, 243);
            this.btnStatusFilterOK2.Name = "btnStatusFilterOK2";
            this.btnStatusFilterOK2.Size = new System.Drawing.Size(77, 22);
            this.btnStatusFilterOK2.TabIndex = 19;
            this.btnStatusFilterOK2.Text = "OK";
            this.btnStatusFilterOK2.Click += new System.EventHandler(this.btnStatusFilterOK);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.deFromDate);
            this.groupControl2.Controls.Add(this.deToDate);
            this.groupControl2.Location = new System.Drawing.Point(3, 186);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(305, 51);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "Date Filter";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(170, 28);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "From:";
            // 
            // deFromDate
            // 
            this.deFromDate.EditValue = null;
            this.deFromDate.Location = new System.Drawing.Point(41, 25);
            this.deFromDate.Name = "deFromDate";
            this.deFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deFromDate.Properties.NullText = "Not Used";
            this.deFromDate.Size = new System.Drawing.Size(108, 20);
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "From Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "All gritting and snow clearance records with a <b>Creation Date</b> which fall be" +
    "tween the From Date and To Date will be returned.\r\n";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.deFromDate.SuperTip = superToolTip1;
            this.deFromDate.TabIndex = 10;
            // 
            // deToDate
            // 
            this.deToDate.EditValue = null;
            this.deToDate.Location = new System.Drawing.Point(192, 25);
            this.deToDate.Name = "deToDate";
            this.deToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deToDate.Properties.NullText = "Not Used";
            this.deToDate.Size = new System.Drawing.Size(108, 20);
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "To Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "All gritting and snow clearance records with a <b>Creation Date</b> which fall be" +
    "tween the From Date and To Date will be returned.\r\n";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.deToDate.SuperTip = superToolTip2;
            this.deToDate.TabIndex = 7;
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp04001GCJobCallOutStatusesBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl3.Size = new System.Drawing.Size(305, 179);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp04001GCJobCallOutStatusesBindingSource
            // 
            this.sp04001GCJobCallOutStatusesBindingSource.DataMember = "sp04001_GC_Job_CallOut_Statuses";
            this.sp04001GCJobCallOutStatusesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colValue,
            this.colOrder});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Status";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 278;
            // 
            // colValue
            // 
            this.colValue.Caption = "Status ID";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.DataSource = this.sp04327GCAnalysisSnowCalloutsForAnalysisBindingSource;
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldGCPONumberSuffix1,
            this.fieldSiteName1,
            this.fieldClientName1,
            this.fieldCompanyName1,
            this.fieldClientsSiteCode1,
            this.fieldSubContractorName1,
            this.fieldGritMobileTelephoneNumber1,
            this.fieldOriginalSubContractorName1,
            this.fieldReactive1,
            this.fieldCalloutStatusDescription1,
            this.fieldCalloutStatusOrder1,
            this.fieldCallOutDateTime1,
            this.fieldSubContractorETA1,
            this.fieldCompletedTime1,
            this.fieldVisitAborted1,
            this.fieldAbortedReason1,
            this.fieldClientPONumber1,
            this.fieldLabourCost1,
            this.fieldLabourVatRate1,
            this.fieldLabourSell1,
            this.fieldOtherCost1,
            this.fieldOtherSell1,
            this.fieldTotalCost1,
            this.fieldTotalSell1,
            this.fieldClientInvoiceNumber1,
            this.fieldSubContractorPaid1,
            this.fieldRecordedByName1,
            this.fieldSubContractorContactedByStaffName1,
            this.fieldPaidByName1,
            this.fieldDoNotPaySubContractor1,
            this.fieldDoNotPaySubContractorReason1,
            this.fieldDoNotInvoiceClient1,
            this.fieldDoNotInvoiceClientReason1,
            this.fieldNoAccess1,
            this.fieldStartTime1,
            this.fieldProfit1,
            this.fieldMarkup1,
            this.fieldClientPOID1,
            this.fieldNonStandardCost1,
            this.fieldNonStandardSell1,
            this.fieldAccessComments1,
            this.fieldClientHowSoon1,
            this.fieldHoursWorked1,
            this.fieldRemarks1,
            this.fieldNoAccessAbortedRate1,
            this.fieldTeamPOFileName1,
            this.fieldTeamInvoiceNumber1,
            this.fieldTimesheetSubmitted1,
            this.fieldRecordedBySubContractorName1,
            this.fieldTimesheetNumber1,
            this.fieldAddedByWebsite1,
            this.fieldCalloutCount1,
            this.fieldCalloutYear,
            this.fieldCalloutQuarter,
            this.fieldCalloutMonth,
            this.fieldCalloutWeek,
            this.fieldStartYear,
            this.fieldStartQuarter,
            this.fieldStartMonth,
            this.fieldStartWeek,
            this.fieldCompletedYear,
            this.fieldCompletedQuarter,
            this.fieldCompletedMonth,
            this.fieldCompletedWeek});
            pivotGridGroup1.Caption = "Callout Year - Quarter - Month";
            pivotGridGroup1.Fields.Add(this.fieldCalloutYear);
            pivotGridGroup1.Fields.Add(this.fieldCalloutQuarter);
            pivotGridGroup1.Fields.Add(this.fieldCalloutMonth);
            pivotGridGroup1.Fields.Add(this.fieldCalloutWeek);
            pivotGridGroup1.Hierarchy = null;
            pivotGridGroup1.ShowNewValues = true;
            pivotGridGroup2.Caption = "Start Year - Quarter - Month";
            pivotGridGroup2.Fields.Add(this.fieldStartYear);
            pivotGridGroup2.Fields.Add(this.fieldStartQuarter);
            pivotGridGroup2.Fields.Add(this.fieldStartMonth);
            pivotGridGroup2.Fields.Add(this.fieldStartWeek);
            pivotGridGroup2.Hierarchy = null;
            pivotGridGroup2.ShowNewValues = true;
            pivotGridGroup3.Caption = "Completed Year - Quarter - Month";
            pivotGridGroup3.Fields.Add(this.fieldCompletedYear);
            pivotGridGroup3.Fields.Add(this.fieldCompletedQuarter);
            pivotGridGroup3.Fields.Add(this.fieldCompletedMonth);
            pivotGridGroup3.Fields.Add(this.fieldCompletedWeek);
            pivotGridGroup3.Hierarchy = null;
            pivotGridGroup3.ShowNewValues = true;
            this.pivotGridControl1.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup1,
            pivotGridGroup2,
            pivotGridGroup3});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl1.MenuManager = this.barManager1;
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pivotGridControl1.OptionsLayout.Columns.StoreAllOptions = true;
            this.pivotGridControl1.OptionsLayout.StoreAppearance = true;
            this.pivotGridControl1.OptionsLayout.StoreFormatRules = true;
            this.pivotGridControl1.OptionsMenu.EnableFormatRulesMenu = true;
            this.pivotGridControl1.Size = new System.Drawing.Size(883, 388);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.PopupMenuShowing += new DevExpress.XtraPivotGrid.PopupMenuShowingEventHandler(this.pivotGridControl1_PopupMenuShowing);
            this.pivotGridControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pivotGridControl1_MouseUp);
            // 
            // sp04327GCAnalysisSnowCalloutsForAnalysisBindingSource
            // 
            this.sp04327GCAnalysisSnowCalloutsForAnalysisBindingSource.DataMember = "sp04327_GC_Analysis_Snow_Callouts_For_Analysis";
            this.sp04327GCAnalysisSnowCalloutsForAnalysisBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // fieldGCPONumberSuffix1
            // 
            this.fieldGCPONumberSuffix1.AreaIndex = 0;
            this.fieldGCPONumberSuffix1.Caption = "GC PO Number Suffix";
            this.fieldGCPONumberSuffix1.FieldName = "GCPONumberSuffix";
            this.fieldGCPONumberSuffix1.Name = "fieldGCPONumberSuffix1";
            this.fieldGCPONumberSuffix1.Visible = false;
            // 
            // fieldSiteName1
            // 
            this.fieldSiteName1.AreaIndex = 1;
            this.fieldSiteName1.Caption = "Site Name";
            this.fieldSiteName1.FieldName = "SiteName";
            this.fieldSiteName1.Name = "fieldSiteName1";
            // 
            // fieldClientName1
            // 
            this.fieldClientName1.AreaIndex = 0;
            this.fieldClientName1.Caption = "Client Name";
            this.fieldClientName1.FieldName = "ClientName";
            this.fieldClientName1.Name = "fieldClientName1";
            // 
            // fieldCompanyName1
            // 
            this.fieldCompanyName1.AreaIndex = 2;
            this.fieldCompanyName1.Caption = "Company Name";
            this.fieldCompanyName1.FieldName = "CompanyName";
            this.fieldCompanyName1.Name = "fieldCompanyName1";
            // 
            // fieldClientsSiteCode1
            // 
            this.fieldClientsSiteCode1.AreaIndex = 3;
            this.fieldClientsSiteCode1.Caption = "Clients Site Code";
            this.fieldClientsSiteCode1.FieldName = "ClientsSiteCode";
            this.fieldClientsSiteCode1.Name = "fieldClientsSiteCode1";
            // 
            // fieldSubContractorName1
            // 
            this.fieldSubContractorName1.AreaIndex = 4;
            this.fieldSubContractorName1.Caption = "SubContractor Name";
            this.fieldSubContractorName1.FieldName = "SubContractorName";
            this.fieldSubContractorName1.Name = "fieldSubContractorName1";
            // 
            // fieldGritMobileTelephoneNumber1
            // 
            this.fieldGritMobileTelephoneNumber1.AreaIndex = 5;
            this.fieldGritMobileTelephoneNumber1.Caption = "Grit Mobile Telephone Number";
            this.fieldGritMobileTelephoneNumber1.FieldName = "GritMobileTelephoneNumber";
            this.fieldGritMobileTelephoneNumber1.Name = "fieldGritMobileTelephoneNumber1";
            this.fieldGritMobileTelephoneNumber1.Visible = false;
            // 
            // fieldOriginalSubContractorName1
            // 
            this.fieldOriginalSubContractorName1.AreaIndex = 5;
            this.fieldOriginalSubContractorName1.Caption = "Original SubContractor Name";
            this.fieldOriginalSubContractorName1.FieldName = "OriginalSubContractorName";
            this.fieldOriginalSubContractorName1.Name = "fieldOriginalSubContractorName1";
            // 
            // fieldReactive1
            // 
            this.fieldReactive1.AreaIndex = 6;
            this.fieldReactive1.Caption = "Reactive";
            this.fieldReactive1.FieldName = "Reactive";
            this.fieldReactive1.Name = "fieldReactive1";
            // 
            // fieldCalloutStatusDescription1
            // 
            this.fieldCalloutStatusDescription1.AreaIndex = 7;
            this.fieldCalloutStatusDescription1.Caption = "Callout Status";
            this.fieldCalloutStatusDescription1.FieldName = "CalloutStatusDescription";
            this.fieldCalloutStatusDescription1.Name = "fieldCalloutStatusDescription1";
            // 
            // fieldCalloutStatusOrder1
            // 
            this.fieldCalloutStatusOrder1.AreaIndex = 8;
            this.fieldCalloutStatusOrder1.Caption = "Callout Status Order";
            this.fieldCalloutStatusOrder1.FieldName = "CalloutStatusOrder";
            this.fieldCalloutStatusOrder1.Name = "fieldCalloutStatusOrder1";
            this.fieldCalloutStatusOrder1.Visible = false;
            // 
            // fieldCallOutDateTime1
            // 
            this.fieldCallOutDateTime1.AreaIndex = 8;
            this.fieldCallOutDateTime1.Caption = "CallOut Date";
            this.fieldCallOutDateTime1.FieldName = "CallOutDateTime";
            this.fieldCallOutDateTime1.Name = "fieldCallOutDateTime1";
            // 
            // fieldSubContractorETA1
            // 
            this.fieldSubContractorETA1.AreaIndex = 9;
            this.fieldSubContractorETA1.Caption = "SubContractor ETA";
            this.fieldSubContractorETA1.FieldName = "SubContractorETA";
            this.fieldSubContractorETA1.Name = "fieldSubContractorETA1";
            this.fieldSubContractorETA1.Visible = false;
            // 
            // fieldCompletedTime1
            // 
            this.fieldCompletedTime1.AreaIndex = 12;
            this.fieldCompletedTime1.Caption = "Completed Time";
            this.fieldCompletedTime1.FieldName = "CompletedTime";
            this.fieldCompletedTime1.Name = "fieldCompletedTime1";
            // 
            // fieldVisitAborted1
            // 
            this.fieldVisitAborted1.AreaIndex = 14;
            this.fieldVisitAborted1.Caption = "Visit Aborted";
            this.fieldVisitAborted1.FieldName = "VisitAborted";
            this.fieldVisitAborted1.Name = "fieldVisitAborted1";
            // 
            // fieldAbortedReason1
            // 
            this.fieldAbortedReason1.AreaIndex = 11;
            this.fieldAbortedReason1.Caption = "Aborted Reason";
            this.fieldAbortedReason1.FieldName = "AbortedReason";
            this.fieldAbortedReason1.Name = "fieldAbortedReason1";
            this.fieldAbortedReason1.Visible = false;
            // 
            // fieldClientPONumber1
            // 
            this.fieldClientPONumber1.AreaIndex = 16;
            this.fieldClientPONumber1.Caption = "Client PO Number";
            this.fieldClientPONumber1.FieldName = "ClientPONumber";
            this.fieldClientPONumber1.Name = "fieldClientPONumber1";
            // 
            // fieldLabourCost1
            // 
            this.fieldLabourCost1.AreaIndex = 17;
            this.fieldLabourCost1.Caption = "Labour Cost";
            this.fieldLabourCost1.CellFormat.FormatString = "c";
            this.fieldLabourCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourCost1.FieldName = "LabourCost";
            this.fieldLabourCost1.GrandTotalCellFormat.FormatString = "c";
            this.fieldLabourCost1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourCost1.Name = "fieldLabourCost1";
            this.fieldLabourCost1.TotalCellFormat.FormatString = "c";
            this.fieldLabourCost1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourCost1.TotalValueFormat.FormatString = "c";
            this.fieldLabourCost1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourCost1.ValueFormat.FormatString = "c";
            this.fieldLabourCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldLabourVatRate1
            // 
            this.fieldLabourVatRate1.AreaIndex = 18;
            this.fieldLabourVatRate1.Caption = "Labour Vat Rate";
            this.fieldLabourVatRate1.CellFormat.FormatString = "P2";
            this.fieldLabourVatRate1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourVatRate1.FieldName = "LabourVatRate";
            this.fieldLabourVatRate1.GrandTotalCellFormat.FormatString = "P2";
            this.fieldLabourVatRate1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourVatRate1.Name = "fieldLabourVatRate1";
            this.fieldLabourVatRate1.TotalCellFormat.FormatString = "P2";
            this.fieldLabourVatRate1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourVatRate1.TotalValueFormat.FormatString = "P2";
            this.fieldLabourVatRate1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourVatRate1.ValueFormat.FormatString = "P2";
            this.fieldLabourVatRate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldLabourSell1
            // 
            this.fieldLabourSell1.AreaIndex = 19;
            this.fieldLabourSell1.Caption = "Labour Sell";
            this.fieldLabourSell1.CellFormat.FormatString = "c";
            this.fieldLabourSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourSell1.FieldName = "LabourSell";
            this.fieldLabourSell1.GrandTotalCellFormat.FormatString = "c";
            this.fieldLabourSell1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourSell1.Name = "fieldLabourSell1";
            this.fieldLabourSell1.TotalCellFormat.FormatString = "c";
            this.fieldLabourSell1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourSell1.TotalValueFormat.FormatString = "c";
            this.fieldLabourSell1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLabourSell1.ValueFormat.FormatString = "c";
            this.fieldLabourSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldOtherCost1
            // 
            this.fieldOtherCost1.AreaIndex = 20;
            this.fieldOtherCost1.Caption = "Other Cost";
            this.fieldOtherCost1.CellFormat.FormatString = "c";
            this.fieldOtherCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherCost1.FieldName = "OtherCost";
            this.fieldOtherCost1.GrandTotalCellFormat.FormatString = "c";
            this.fieldOtherCost1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherCost1.Name = "fieldOtherCost1";
            this.fieldOtherCost1.TotalCellFormat.FormatString = "c";
            this.fieldOtherCost1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherCost1.TotalValueFormat.FormatString = "c";
            this.fieldOtherCost1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherCost1.ValueFormat.FormatString = "c";
            this.fieldOtherCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldOtherSell1
            // 
            this.fieldOtherSell1.AreaIndex = 21;
            this.fieldOtherSell1.Caption = "Other Sell";
            this.fieldOtherSell1.CellFormat.FormatString = "c";
            this.fieldOtherSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherSell1.FieldName = "OtherSell";
            this.fieldOtherSell1.GrandTotalCellFormat.FormatString = "c";
            this.fieldOtherSell1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherSell1.Name = "fieldOtherSell1";
            this.fieldOtherSell1.TotalCellFormat.FormatString = "c";
            this.fieldOtherSell1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherSell1.TotalValueFormat.FormatString = "c";
            this.fieldOtherSell1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldOtherSell1.ValueFormat.FormatString = "c";
            this.fieldOtherSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTotalCost1
            // 
            this.fieldTotalCost1.AreaIndex = 22;
            this.fieldTotalCost1.Caption = "Total Cost";
            this.fieldTotalCost1.CellFormat.FormatString = "c";
            this.fieldTotalCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalCost1.FieldName = "TotalCost";
            this.fieldTotalCost1.GrandTotalCellFormat.FormatString = "c";
            this.fieldTotalCost1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalCost1.Name = "fieldTotalCost1";
            this.fieldTotalCost1.TotalCellFormat.FormatString = "c";
            this.fieldTotalCost1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalCost1.TotalValueFormat.FormatString = "c";
            this.fieldTotalCost1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalCost1.ValueFormat.FormatString = "c";
            this.fieldTotalCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTotalSell1
            // 
            this.fieldTotalSell1.AreaIndex = 23;
            this.fieldTotalSell1.Caption = "Total Sell";
            this.fieldTotalSell1.CellFormat.FormatString = "c";
            this.fieldTotalSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalSell1.FieldName = "TotalSell";
            this.fieldTotalSell1.GrandTotalCellFormat.FormatString = "c";
            this.fieldTotalSell1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalSell1.Name = "fieldTotalSell1";
            this.fieldTotalSell1.TotalCellFormat.FormatString = "c";
            this.fieldTotalSell1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalSell1.TotalValueFormat.FormatString = "c";
            this.fieldTotalSell1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalSell1.ValueFormat.FormatString = "c";
            this.fieldTotalSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldClientInvoiceNumber1
            // 
            this.fieldClientInvoiceNumber1.AreaIndex = 28;
            this.fieldClientInvoiceNumber1.Caption = "Client Invoice Number";
            this.fieldClientInvoiceNumber1.FieldName = "ClientInvoiceNumber";
            this.fieldClientInvoiceNumber1.Name = "fieldClientInvoiceNumber1";
            // 
            // fieldSubContractorPaid1
            // 
            this.fieldSubContractorPaid1.AreaIndex = 29;
            this.fieldSubContractorPaid1.Caption = "SubContractor Paid";
            this.fieldSubContractorPaid1.FieldName = "SubContractorPaid";
            this.fieldSubContractorPaid1.Name = "fieldSubContractorPaid1";
            // 
            // fieldRecordedByName1
            // 
            this.fieldRecordedByName1.AreaIndex = 30;
            this.fieldRecordedByName1.Caption = "Recorded By Name";
            this.fieldRecordedByName1.FieldName = "RecordedByName";
            this.fieldRecordedByName1.Name = "fieldRecordedByName1";
            // 
            // fieldSubContractorContactedByStaffName1
            // 
            this.fieldSubContractorContactedByStaffName1.AreaIndex = 31;
            this.fieldSubContractorContactedByStaffName1.Caption = "SubContractor Contacted By Staff";
            this.fieldSubContractorContactedByStaffName1.FieldName = "SubContractorContactedByStaffName";
            this.fieldSubContractorContactedByStaffName1.Name = "fieldSubContractorContactedByStaffName1";
            // 
            // fieldPaidByName1
            // 
            this.fieldPaidByName1.AreaIndex = 32;
            this.fieldPaidByName1.Caption = "Paid By Name";
            this.fieldPaidByName1.FieldName = "PaidByName";
            this.fieldPaidByName1.Name = "fieldPaidByName1";
            // 
            // fieldDoNotPaySubContractor1
            // 
            this.fieldDoNotPaySubContractor1.AreaIndex = 33;
            this.fieldDoNotPaySubContractor1.Caption = "Do Not Pay Sub Contractor";
            this.fieldDoNotPaySubContractor1.FieldName = "DoNotPaySubContractor";
            this.fieldDoNotPaySubContractor1.Name = "fieldDoNotPaySubContractor1";
            // 
            // fieldDoNotPaySubContractorReason1
            // 
            this.fieldDoNotPaySubContractorReason1.AreaIndex = 25;
            this.fieldDoNotPaySubContractorReason1.Caption = "Do Not Pay Sub Contractor Reason";
            this.fieldDoNotPaySubContractorReason1.FieldName = "DoNotPaySubContractorReason";
            this.fieldDoNotPaySubContractorReason1.Name = "fieldDoNotPaySubContractorReason1";
            this.fieldDoNotPaySubContractorReason1.Visible = false;
            // 
            // fieldDoNotInvoiceClient1
            // 
            this.fieldDoNotInvoiceClient1.AreaIndex = 34;
            this.fieldDoNotInvoiceClient1.Caption = "Do Not Invoice Client";
            this.fieldDoNotInvoiceClient1.FieldName = "DoNotInvoiceClient";
            this.fieldDoNotInvoiceClient1.Name = "fieldDoNotInvoiceClient1";
            // 
            // fieldDoNotInvoiceClientReason1
            // 
            this.fieldDoNotInvoiceClientReason1.AreaIndex = 26;
            this.fieldDoNotInvoiceClientReason1.Caption = "Do Not Invoice Client Reason";
            this.fieldDoNotInvoiceClientReason1.FieldName = "DoNotInvoiceClientReason";
            this.fieldDoNotInvoiceClientReason1.Name = "fieldDoNotInvoiceClientReason1";
            this.fieldDoNotInvoiceClientReason1.Visible = false;
            // 
            // fieldNoAccess1
            // 
            this.fieldNoAccess1.AreaIndex = 15;
            this.fieldNoAccess1.Caption = "No Access";
            this.fieldNoAccess1.FieldName = "NoAccess";
            this.fieldNoAccess1.Name = "fieldNoAccess1";
            // 
            // fieldStartTime1
            // 
            this.fieldStartTime1.AreaIndex = 10;
            this.fieldStartTime1.Caption = "Start Time";
            this.fieldStartTime1.FieldName = "StartTime";
            this.fieldStartTime1.Name = "fieldStartTime1";
            // 
            // fieldProfit1
            // 
            this.fieldProfit1.AreaIndex = 24;
            this.fieldProfit1.Caption = "Profit";
            this.fieldProfit1.CellFormat.FormatString = "c";
            this.fieldProfit1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldProfit1.FieldName = "Profit";
            this.fieldProfit1.GrandTotalCellFormat.FormatString = "c";
            this.fieldProfit1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldProfit1.Name = "fieldProfit1";
            this.fieldProfit1.TotalCellFormat.FormatString = "c";
            this.fieldProfit1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldProfit1.TotalValueFormat.FormatString = "c";
            this.fieldProfit1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldProfit1.ValueFormat.FormatString = "c";
            this.fieldProfit1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldMarkup1
            // 
            this.fieldMarkup1.AreaIndex = 25;
            this.fieldMarkup1.Caption = "Markup %";
            this.fieldMarkup1.CellFormat.FormatString = "P2";
            this.fieldMarkup1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldMarkup1.FieldName = "Markup";
            this.fieldMarkup1.GrandTotalCellFormat.FormatString = "P2";
            this.fieldMarkup1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldMarkup1.Name = "fieldMarkup1";
            this.fieldMarkup1.TotalCellFormat.FormatString = "P2";
            this.fieldMarkup1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldMarkup1.TotalValueFormat.FormatString = "P2";
            this.fieldMarkup1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldMarkup1.ValueFormat.FormatString = "P2";
            this.fieldMarkup1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldClientPOID1
            // 
            this.fieldClientPOID1.AreaIndex = 35;
            this.fieldClientPOID1.Caption = "Client PO ID";
            this.fieldClientPOID1.FieldName = "ClientPOID";
            this.fieldClientPOID1.Name = "fieldClientPOID1";
            // 
            // fieldNonStandardCost1
            // 
            this.fieldNonStandardCost1.AreaIndex = 26;
            this.fieldNonStandardCost1.Caption = "Non Standard Cost";
            this.fieldNonStandardCost1.FieldName = "NonStandardCost";
            this.fieldNonStandardCost1.Name = "fieldNonStandardCost1";
            // 
            // fieldNonStandardSell1
            // 
            this.fieldNonStandardSell1.AreaIndex = 27;
            this.fieldNonStandardSell1.Caption = "Non Standard Sell";
            this.fieldNonStandardSell1.FieldName = "NonStandardSell";
            this.fieldNonStandardSell1.Name = "fieldNonStandardSell1";
            // 
            // fieldAccessComments1
            // 
            this.fieldAccessComments1.AreaIndex = 36;
            this.fieldAccessComments1.Caption = "Access Comments";
            this.fieldAccessComments1.FieldName = "AccessComments";
            this.fieldAccessComments1.Name = "fieldAccessComments1";
            // 
            // fieldClientHowSoon1
            // 
            this.fieldClientHowSoon1.AreaIndex = 34;
            this.fieldClientHowSoon1.Caption = "Client How Soon";
            this.fieldClientHowSoon1.FieldName = "ClientHowSoon";
            this.fieldClientHowSoon1.Name = "fieldClientHowSoon1";
            this.fieldClientHowSoon1.Visible = false;
            // 
            // fieldHoursWorked1
            // 
            this.fieldHoursWorked1.AreaIndex = 37;
            this.fieldHoursWorked1.Caption = "Hours Worked";
            this.fieldHoursWorked1.CellFormat.FormatString = "f2";
            this.fieldHoursWorked1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHoursWorked1.FieldName = "HoursWorked";
            this.fieldHoursWorked1.GrandTotalCellFormat.FormatString = "f2";
            this.fieldHoursWorked1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHoursWorked1.Name = "fieldHoursWorked1";
            this.fieldHoursWorked1.TotalCellFormat.FormatString = "f2";
            this.fieldHoursWorked1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHoursWorked1.TotalValueFormat.FormatString = "f2";
            this.fieldHoursWorked1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldHoursWorked1.ValueFormat.FormatString = "f2";
            this.fieldHoursWorked1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldRemarks1
            // 
            this.fieldRemarks1.AreaIndex = 35;
            this.fieldRemarks1.Caption = "Remarks";
            this.fieldRemarks1.FieldName = "Remarks";
            this.fieldRemarks1.Name = "fieldRemarks1";
            this.fieldRemarks1.Visible = false;
            // 
            // fieldNoAccessAbortedRate1
            // 
            this.fieldNoAccessAbortedRate1.AreaIndex = 38;
            this.fieldNoAccessAbortedRate1.Caption = "No Access Aborted Rate";
            this.fieldNoAccessAbortedRate1.CellFormat.FormatString = "c";
            this.fieldNoAccessAbortedRate1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNoAccessAbortedRate1.FieldName = "NoAccessAbortedRate";
            this.fieldNoAccessAbortedRate1.GrandTotalCellFormat.FormatString = "c";
            this.fieldNoAccessAbortedRate1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNoAccessAbortedRate1.Name = "fieldNoAccessAbortedRate1";
            this.fieldNoAccessAbortedRate1.TotalCellFormat.FormatString = "c";
            this.fieldNoAccessAbortedRate1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNoAccessAbortedRate1.TotalValueFormat.FormatString = "c";
            this.fieldNoAccessAbortedRate1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNoAccessAbortedRate1.ValueFormat.FormatString = "c";
            this.fieldNoAccessAbortedRate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldTeamPOFileName1
            // 
            this.fieldTeamPOFileName1.AreaIndex = 36;
            this.fieldTeamPOFileName1.Caption = "Team PO File Name";
            this.fieldTeamPOFileName1.FieldName = "TeamPOFileName";
            this.fieldTeamPOFileName1.Name = "fieldTeamPOFileName1";
            this.fieldTeamPOFileName1.Visible = false;
            // 
            // fieldTeamInvoiceNumber1
            // 
            this.fieldTeamInvoiceNumber1.AreaIndex = 39;
            this.fieldTeamInvoiceNumber1.Caption = "Team Invoice Number";
            this.fieldTeamInvoiceNumber1.FieldName = "TeamInvoiceNumber";
            this.fieldTeamInvoiceNumber1.Name = "fieldTeamInvoiceNumber1";
            // 
            // fieldTimesheetSubmitted1
            // 
            this.fieldTimesheetSubmitted1.AreaIndex = 40;
            this.fieldTimesheetSubmitted1.Caption = "Timesheet Submitted";
            this.fieldTimesheetSubmitted1.FieldName = "TimesheetSubmitted";
            this.fieldTimesheetSubmitted1.Name = "fieldTimesheetSubmitted1";
            // 
            // fieldRecordedBySubContractorName1
            // 
            this.fieldRecordedBySubContractorName1.AreaIndex = 38;
            this.fieldRecordedBySubContractorName1.Caption = "Recorded By SubContractor Name";
            this.fieldRecordedBySubContractorName1.FieldName = "RecordedBySubContractorName";
            this.fieldRecordedBySubContractorName1.Name = "fieldRecordedBySubContractorName1";
            this.fieldRecordedBySubContractorName1.Visible = false;
            // 
            // fieldTimesheetNumber1
            // 
            this.fieldTimesheetNumber1.AreaIndex = 41;
            this.fieldTimesheetNumber1.Caption = "Timesheet Number";
            this.fieldTimesheetNumber1.FieldName = "TimesheetNumber";
            this.fieldTimesheetNumber1.Name = "fieldTimesheetNumber1";
            // 
            // fieldAddedByWebsite1
            // 
            this.fieldAddedByWebsite1.AreaIndex = 42;
            this.fieldAddedByWebsite1.Caption = "Added By Website";
            this.fieldAddedByWebsite1.FieldName = "AddedByWebsite";
            this.fieldAddedByWebsite1.Name = "fieldAddedByWebsite1";
            // 
            // fieldCalloutCount1
            // 
            this.fieldCalloutCount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldCalloutCount1.AreaIndex = 0;
            this.fieldCalloutCount1.Caption = "Callout Count";
            this.fieldCalloutCount1.FieldName = "CalloutCount";
            this.fieldCalloutCount1.Name = "fieldCalloutCount1";
            // 
            // chartControl1
            // 
            this.chartControl1.DataAdapter = this.sp04237_GC_Company_Filter_ListTableAdapter;
            this.chartControl1.DataSource = this.dataSet_GC_Reports.sp04237_GC_Company_Filter_List;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl1.EmptyChartText.Text = "No Data To Graph - Try Selecting Data from the Analysis Grid.";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            sideBySideBarSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl1.SeriesTemplate.Label = sideBySideBarSeriesLabel1;
            this.chartControl1.Size = new System.Drawing.Size(883, 100);
            this.chartControl1.TabIndex = 1;
            // 
            // sp04237_GC_Company_Filter_ListTableAdapter
            // 
            this.sp04237_GC_Company_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiToggleAvailableColumnsVisibility, true)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Copy to Clipboard";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Hint = "Copy the selected cells to the Clipboard for pasting to external application.";
            this.barButtonItem1.Id = 25;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // bbiToggleAvailableColumnsVisibility
            // 
            this.bbiToggleAvailableColumnsVisibility.Caption = "Toggle Available Columns Visibility";
            this.bbiToggleAvailableColumnsVisibility.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiToggleAvailableColumnsVisibility.Glyph")));
            this.bbiToggleAvailableColumnsVisibility.Id = 32;
            this.bbiToggleAvailableColumnsVisibility.Name = "bbiToggleAvailableColumnsVisibility";
            this.bbiToggleAvailableColumnsVisibility.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiToggleAvailableColumnsVisibility_ItemClick);
            // 
            // pmChart
            // 
            this.pmChart.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateAxis),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChartWizard, true)});
            this.pmChart.Manager = this.barManager1;
            this.pmChart.MenuCaption = "Chart Menu";
            this.pmChart.Name = "pmChart";
            // 
            // bbiRotateAxis
            // 
            this.bbiRotateAxis.Caption = "Rotate Axis";
            this.bbiRotateAxis.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRotateAxis.Glyph")));
            this.bbiRotateAxis.Hint = "Rotate Chart Axis";
            this.bbiRotateAxis.Id = 27;
            this.bbiRotateAxis.Name = "bbiRotateAxis";
            this.bbiRotateAxis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAxis_ItemClick);
            // 
            // bbiChartWizard
            // 
            this.bbiChartWizard.Caption = "Chart Wizard";
            this.bbiChartWizard.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiChartWizard.Glyph")));
            this.bbiChartWizard.Hint = "Open Chart Wizard";
            this.bbiChartWizard.Id = 26;
            this.bbiChartWizard.Name = "bbiChartWizard";
            this.bbiChartWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChartWizard_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(580, 206);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemStatusFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemCompanyFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadData),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAnalyse, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // barEditItemStatusFilter
            // 
            this.barEditItemStatusFilter.Caption = "Status Filter";
            this.barEditItemStatusFilter.Edit = this.repositoryItemPopupContainerEditStatusFilter;
            this.barEditItemStatusFilter.EditValue = "All Statuses";
            this.barEditItemStatusFilter.EditWidth = 83;
            this.barEditItemStatusFilter.Id = 28;
            this.barEditItemStatusFilter.Name = "barEditItemStatusFilter";
            // 
            // repositoryItemPopupContainerEditStatusFilter
            // 
            this.repositoryItemPopupContainerEditStatusFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditStatusFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditStatusFilter.Name = "repositoryItemPopupContainerEditStatusFilter";
            this.repositoryItemPopupContainerEditStatusFilter.PopupControl = this.popupContainerControlStatuses;
            this.repositoryItemPopupContainerEditStatusFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditStatusFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditStatusFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditStatusFilter_QueryResultValue);
            // 
            // barEditItemCompanyFilter
            // 
            this.barEditItemCompanyFilter.Caption = "Company Filter";
            this.barEditItemCompanyFilter.Edit = this.repositoryItemPopupContainerEditCompanyFilter;
            this.barEditItemCompanyFilter.EditValue = "All Companies";
            this.barEditItemCompanyFilter.EditWidth = 93;
            this.barEditItemCompanyFilter.Id = 29;
            this.barEditItemCompanyFilter.Name = "barEditItemCompanyFilter";
            // 
            // repositoryItemPopupContainerEditCompanyFilter
            // 
            this.repositoryItemPopupContainerEditCompanyFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditCompanyFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditCompanyFilter.Name = "repositoryItemPopupContainerEditCompanyFilter";
            this.repositoryItemPopupContainerEditCompanyFilter.PopupControl = this.popupContainerControlCompanies;
            this.repositoryItemPopupContainerEditCompanyFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditCompanyFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditCompanyFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditCompanyFilter_QueryResultValue);
            // 
            // bbiReloadData
            // 
            this.bbiReloadData.Caption = "Reload";
            this.bbiReloadData.Glyph = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiReloadData.Id = 30;
            this.bbiReloadData.Name = "bbiReloadData";
            this.bbiReloadData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Reload Data - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to reload the Data Supply List using the data supplied in the Filter.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiReloadData.SuperTip = superToolTip3;
            this.bbiReloadData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadData_ItemClick);
            // 
            // bbiAnalyse
            // 
            this.bbiAnalyse.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiAnalyse.Caption = "Analyse";
            this.bbiAnalyse.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAnalyse.Glyph")));
            this.bbiAnalyse.Id = 31;
            this.bbiAnalyse.Name = "bbiAnalyse";
            this.bbiAnalyse.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Text = "Analyse - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to load the Analysis Grid and Chart with the records selected in the Dat" +
    "a Supply list.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiAnalyse.SuperTip = superToolTip4;
            this.bbiAnalyse.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAnalyse_ItemClick);
            // 
            // sp04001_GC_Job_CallOut_StatusesTableAdapter
            // 
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04326_GC_Analysis_Snow_Callouts_ListTableAdapter
            // 
            this.sp04326_GC_Analysis_Snow_Callouts_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04327_GC_Analysis_Snow_Callouts_For_AnalysisTableAdapter
            // 
            this.sp04327_GC_Analysis_Snow_Callouts_For_AnalysisTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_GC_Analysis_Snow_Callouts
            // 
            this.ClientSize = new System.Drawing.Size(1293, 494);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.dockPanel1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Analysis_Snow_Callouts";
            this.Text = "Snow Clearance Callout Analysis";
            this.Activated += new System.EventHandler(this.frm_GC_Analysis_Snow_Callouts_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_GC_Analysis_Snow_Callouts_FormClosed);
            this.Load += new System.EventHandler(this.frm_GC_Analysis_Snow_Callouts_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04326GCAnalysisSnowCalloutsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).EndInit();
            this.popupContainerControlCompanies.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlStatuses)).EndInit();
            this.popupContainerControlStatuses.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04327GCAnalysisSnowCalloutsForAnalysisBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT_Reports dataSet_AT_Reports;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiChartWizard;
        private DevExpress.XtraBars.PopupMenu pmChart;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAxis;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemStatusFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditStatusFilter;
        private DevExpress.XtraBars.BarEditItem barEditItemCompanyFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditCompanyFilter;
        private DevExpress.XtraBars.BarButtonItem bbiReloadData;
        private DevExpress.XtraBars.BarButtonItem bbiAnalyse;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlStatuses;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit deFromDate;
        private DevExpress.XtraEditors.DateEdit deToDate;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCompanies;
        private DevExpress.XtraEditors.SimpleButton btnCompanyFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOrder;
        private DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter sp04237_GC_Company_Filter_ListTableAdapter;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private System.Windows.Forms.BindingSource sp04001GCJobCallOutStatusesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter sp04001_GC_Job_CallOut_StatusesTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnStatusFilterOK2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorETA;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitAborted;
        private DevExpress.XtraGrid.Columns.GridColumn colAbortedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KgBags;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursWorked;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCost;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherSell;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByName;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractorReason;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colProfit;
        private DevExpress.XtraGrid.Columns.GridColumn colMarkup;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardSell;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClientReason;
        private DevExpress.XtraGrid.Columns.GridColumn colGritMobileTelephoneNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccessAbortedRate;
        private System.Windows.Forms.BindingSource sp04326GCAnalysisSnowCalloutsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutID;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCPONumberSuffix;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorContactedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientHowSoon;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamPOFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colTimesheetSubmitted;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedBySubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedBySubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colTimesheetNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByWebsite;
        private DataSet_GC_ReportsTableAdapters.sp04326_GC_Analysis_Snow_Callouts_ListTableAdapter sp04326_GC_Analysis_Snow_Callouts_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp04327GCAnalysisSnowCalloutsForAnalysisBindingSource;
        private DataSet_GC_ReportsTableAdapters.sp04327_GC_Analysis_Snow_Callouts_For_AnalysisTableAdapter sp04327_GC_Analysis_Snow_Callouts_For_AnalysisTableAdapter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldGCPONumberSuffix1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompanyName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientsSiteCode1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldGritMobileTelephoneNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOriginalSubContractorName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldReactive1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutStatusDescription1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutStatusOrder1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCallOutDateTime1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorETA1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedTime1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldVisitAborted1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAbortedReason1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientPONumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLabourCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLabourVatRate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLabourSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOtherCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOtherSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTotalCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTotalSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientInvoiceNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorPaid1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRecordedByName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubContractorContactedByStaffName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPaidByName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDoNotPaySubContractor1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDoNotPaySubContractorReason1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDoNotInvoiceClient1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDoNotInvoiceClientReason1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNoAccess1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartTime1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldProfit1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMarkup1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientPOID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNonStandardCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNonStandardSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccessComments1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientHowSoon1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHoursWorked1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRemarks1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNoAccessAbortedRate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTeamPOFileName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTeamInvoiceNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTimesheetSubmitted1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRecordedBySubContractorName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTimesheetNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAddedByWebsite1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutCount1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedMonth;
        private DevExpress.XtraBars.BarButtonItem bbiToggleAvailableColumnsVisibility;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalloutWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStartWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletedWeek;
    }
}

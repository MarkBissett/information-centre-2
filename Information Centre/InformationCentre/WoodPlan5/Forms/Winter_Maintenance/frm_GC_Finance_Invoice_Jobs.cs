﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //

using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Menu;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;
using System.Runtime.InteropServices;  // Required for Excel Generation //
using Excel;
using System.Reflection;

namespace WoodPlan5
{
    public partial class frm_GC_Finance_Invoice_Jobs : BaseObjects.frmBase
    {

        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;

        WaitDialogForm loadingForm;

        public string i_str_SavedDirectoryName = "";

        private string i_str_selected_company_ids = "";
        private string i_str_selected_companies = "";
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection5;

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState5;  // Used by Grid View State Facilities //
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
 
        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        int i_int_FocusedGrid = 1;
        string istr_FurtherAction = "";  // Set by Wizard only //

        #endregion
        
        public frm_GC_Finance_Invoice_Jobs()
        {
            InitializeComponent();
        }

        private void frm_GC_Finance_Invoice_Jobs_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4016;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            dateEditFromDate.DateTime = DateTime.Today.AddDays(-7);
            dateEditToDate.DateTime = DateTime.Now;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04258_GC_Finance_Client_Invoice_LinesTableAdapter.Connection.ConnectionString = strConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //       

            sp04237_GC_Company_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04237_GC_Company_Filter_ListTableAdapter.Fill(this.dataSet_GC_Reports.sp04237_GC_Company_Filter_List);

            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            gridControl2.ForceInitialize();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_LineTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_LineTableAdapter.Connection.ConnectionString = strConnectionString;

            sp04265_GC_Finance_Team_Jobs_For_ExportTableAdapter.Connection.ConnectionString = strConnectionString;
            // Add record selection checkboxes to popup grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;
            gridControl5.ForceInitialize();

            RefreshGridViewState1 = new RefreshGridState(gridView1, "ClientInvoiceLineID");
            RefreshGridViewState2 = new RefreshGridState(gridView2, "GrittingCallOutID");
            RefreshGridViewState3 = new RefreshGridState(gridView3, "SnowClearanceCallOutID");
            RefreshGridViewState5 = new RefreshGridState(gridView5, "UniqueID");
            emptyEditor = new RepositoryItem();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_SavedDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "FinanceClientInvoiceSSDefaultSavePath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Client Job Breakdown Spreadsheets (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Breakdown Spreadsheet Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            Load_Data2();  // Load Team Jobs on Pahe 2 //

            if (!i_str_SavedDirectoryName.EndsWith("\\")) i_str_SavedDirectoryName += "\\";  // Add Backslash to end //
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }

        private void frm_GC_Finance_Invoice_Jobs_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                    if (!string.IsNullOrEmpty(istr_FurtherAction))
                    {
                        string strAction = istr_FurtherAction;
                        istr_FurtherAction = "";
                        if (strAction == "spreadsheets")
                        {
                            bbiClientSpreadsheets.PerformClick();
                        }
                    }
                }
                else if (UpdateRefreshStatus == 2)
                {
                    LoadLinkedRecords();
                    Load_Data2();
                }
            }
            SetMenuStatus();
        }

        private void frm_GC_Finance_Invoice_Jobs_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                try
                {
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "IncludeCompleted", (checkEditIncludeCompleted.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_company_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
                catch (Exception)
                {
                }
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            LoadLastSavedUserScreenSettings();
            if (!splitContainerControl2.Collapsed)
            {
                splitContainerControl2.SplitterPosition = splitContainerControl2.Height / 2;
            }
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                 }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
                        
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            switch (i_int_FocusedGrid)
            {
                case 1:    // Client Invoice Lines //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowEdit && intRowHandles.Length == 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                        else if (iBool_AllowEdit && intRowHandles.Length > 1)
                        {
                            alItems.AddRange(new string[] { "iEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = false;
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case 2:    // Linked Gritting Jobs //
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case 3:    // Linked Snow Clearance Jobs //
                    {
                       view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case 5:    // Gritting \ Snow Clearance Jobs //
                    {
                        view = (GridView)gridControl5.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        bbiDelete.Enabled = false;
                    }
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowEdit)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            // Set enabled status of GridView4 navigator custom buttons //
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowEdit)
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            // Set enabled status of GridView5 navigator custom buttons //
            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowEdit)
            {
                gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Colour Code //
                string strIncludeCompleted = default_screen_settings.RetrieveSetting("IncludeCompleted");
                if (!string.IsNullOrEmpty(strIncludeCompleted)) checkEditIncludeCompleted.Checked = (strIncludeCompleted == "0" ? false : true);
                
                // Gritting Callout Filter //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("CompanyFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] strArray = strItemFilter.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl2.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in strArray)
                    {
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["CompanyID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    PopupContainerEdit1_Get_Selected();
                }
                bbiReloadData.PerformClick();
            }
        }

        private void Load_Data()
        {
            WaitDialogForm loading = new WaitDialogForm("Loading Client Invoice Lines...", "Finance - Job Invoicing Manager");
            if (fProgress == null)
            {
                loading.Show();
            }
            else
            {
                loading.Hide();
                loading.Dispose();
            }
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (i_str_AddedRecordIDs1 != "" && dateEditToDate.DateTime < DateTime.Now) dateEditToDate.DateTime = DateTime.Now;  // Make sure Period spanned is big enough //
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            int intShowCompleted = Convert.ToInt32(checkEditIncludeCompleted.Checked);
            if (i_str_selected_company_ids == null) i_str_selected_company_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
            view.BeginUpdate();
            sp04258_GC_Finance_Client_Invoice_LinesTableAdapter.Fill(this.dataSet_GC_Core.sp04258_GC_Finance_Client_Invoice_Lines, dtFromDate, dtToDate, i_str_selected_company_ids, intShowCompleted);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ClientInvoiceLineID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (fProgress == null)
            {
                loading.Close();
                loading.Dispose();
            }


 

        }

        private void LoadLinkedRecords()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount1 = intRowHandles.Length;
            string strSelectedIDs1 = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs1 += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ClientInvoiceLineID"])) + ',';
            }

             //Populate Child Grid //
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState2.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount1 == 0)
            {
                this.dataSet_GC_Core.sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_Line.Clear();
            }
            else
            {
                sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_LineTableAdapter.Fill(dataSet_GC_Core.sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_Line, strSelectedIDs1);
                this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl3.MainView.EndUpdate();

            gridControl4.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount1 == 0)
            {
                this.dataSet_GC_Core.sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_Line.Clear();
            }
            else
            {
                sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_LineTableAdapter.Fill(dataSet_GC_Core.sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_Line, strSelectedIDs1);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl4.MainView.EndUpdate();

        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strFurtherAction)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            istr_FurtherAction = strFurtherAction;
        }


        #region Parameters Filter Panel

        private void repositoryItemPopupContainerEditParameters_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            PopupContainerEdit1_Get_Selected();
        }

        private void repositoryItemPopupContainerEditParameters_QueryPopUp(object sender, CancelEventArgs e)
        {
            repositoryItemPopupContainerEditParameters.PopupControl = popupContainerControlParameters;  // Needed as more than 1 Popup Container Edit cannot share the same PopupContainerControl //
        }

        private void btnOK2_Click(object sender, EventArgs e)
        {
            PopupContainerEdit1_Get_Selected();
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_company_ids = "";    // Reset any prior values first //
            i_str_selected_companies = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_company_ids = "";
                return "All Companies";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_company_ids = "";
                return "All Companies";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_company_ids += Convert.ToString(view.GetRowCellValue(i, "CompanyID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_companies = Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_companies += ", " + Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_companies;
        }

        private void dateEditFromDate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Invoice Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Invoice Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Client Invoice Lines Available - Click Reload Data";
                    break;
                case "gridView3":
                    message = "No Linked Gritting Jobs Available - Select one or more Client Invoice Lines";
                    break;
                case "gridView4":
                    message = "No Linked Snow Clearance Jobs Available - Select one or more Client Invoice Lines";
                    break;
                case "gridView5":
                    message = "No Team Jobs Available - Click Reload Data";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedRecords();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl4.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                //bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "SubContractorName")
            {
                switch (view.GetRowCellValue(e.RowHandle, "SubContractorName").ToString())
                {
                    case "":
                        //e.Appearance.BackColor = Color.LightCoral;
                        //e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }*/
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "GrittingJobCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "GrittingJobCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "SnowClearanceJobCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SnowClearanceJobCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "GrittingJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("GrittingJobCount")) == 0) e.Cancel = true;
                    break;
                case "SnowClearanceJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("SnowClearanceJobCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strClientIDs = view.GetRowCellValue(view.FocusedRowHandle, "ClientInvoiceLineID").ToString() + ",";
            DateTime FromDate = (string.IsNullOrEmpty(dateEditFromDate.DateTime.ToString()) ? Convert.ToDateTime("2011-01-01") : dateEditFromDate.DateTime);
            DateTime ToDate = (string.IsNullOrEmpty(dateEditToDate.DateTime.ToString()) ? Convert.ToDateTime("2011-01-01") : dateEditToDate.DateTime);
            switch (view.FocusedColumn.FieldName)
            {
                case "GrittingJobCount":
                    {
                        DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetData = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                        GetData.ChangeConnectionString(strConnectionString);
                        string strRecordIDs = GetData.sp04077_GC_DrillDown_Get_Linked_Callouts(strClientIDs, "client_invoice_line", "", FromDate, ToDate).ToString();
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "client_invoicing_gritting");
                    }
                    break;
                case "SnowClearanceJobCount":
                    {
                        DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter GetData = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
                        GetData.ChangeConnectionString(strConnectionString);
                        string strRecordIDs = GetData.sp04196_GC_Snow_DrillDown_Get_Linked_Callouts(strClientIDs, "client_invoice_line", "", FromDate, ToDate).ToString();
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "client_invoicing_snow_clearance");
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                //bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "SubContractorName")
            {
                switch (view.GetRowCellValue(e.RowHandle, "SubContractorName").ToString())
                {
                    case "":
                        //e.Appearance.BackColor = Color.LightCoral;
                        //e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }*/
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }
        
        #endregion


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                //bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView4_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "SubContractorName")
            {
                switch (view.GetRowCellValue(e.RowHandle, "SubContractorName").ToString())
                {
                    case "":
                        //e.Appearance.BackColor = Color.LightCoral;
                        //e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }*/
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void bbiClientInvoiceWizard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            System.Windows.Forms.Application.DoEvents();

            //this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            frm_GC_Finance_Invoice_Client_Wizard fChildForm = new frm_GC_Finance_Invoice_Client_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strFormMode = "edit";
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.fProgress = fProgress;
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiReloadData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Set Invoice Number //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length < 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one Client Invoice Line to Block Edit before proceeding.", "Block Edit Client Invoice Line Details", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strClientInvoiceDetails = null;
                        string strInvoiceNumber = null;
                        int? intSentToAccounts = null;
                        int? intSentToClientMethodID = null;
                        DateTime? dtDateTimeSent = null;
                        int? intAudited = null;
                        int? intOnHoldReasonID = null;
                        string strRemarks = null;
                        strClientInvoiceDetails = "Block Editing - N\\A";

                        frm_GC_Finance_Invoice_Set_Invoice_No fChildForm = new frm_GC_Finance_Invoice_Set_Invoice_No();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strClientInvoiceDetails = strClientInvoiceDetails;
                        fChildForm.strInvoiceNumber = strInvoiceNumber;
                        fChildForm.intSentToAccounts = intSentToAccounts;
                        fChildForm.intSentToClientMethodID = intSentToClientMethodID;
                        fChildForm.dtDateTimeSent = dtDateTimeSent;
                        fChildForm.intAudited = intAudited;
                        fChildForm.intOnHoldReasonID = intOnHoldReasonID;
                        fChildForm.strRemarks = strRemarks;

                        if (fChildForm.ShowDialog() != DialogResult.OK) return;

                        strInvoiceNumber = fChildForm.strInvoiceNumber;
                        intSentToAccounts = fChildForm.intSentToAccounts;
                        intSentToClientMethodID = fChildForm.intSentToClientMethodID;
                        dtDateTimeSent = fChildForm.dtDateTimeSent;
                        intAudited = fChildForm.intAudited;
                        intOnHoldReasonID = fChildForm.intOnHoldReasonID;
                        strRemarks = fChildForm.strRemarks;
                        string strMode = "blockedit";

                        DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter UpdateInvoiceLines = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                        UpdateInvoiceLines.ChangeConnectionString(strConnectionString);
                        StringBuilder sb = new StringBuilder();
                        try
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                sb.Append(view.GetRowCellValue(intRowHandle, "ClientInvoiceLineID").ToString() + ",");
                            }
                            UpdateInvoiceLines.sp04261_GC_Finance_Set_Invoice_Number(strMode, sb.ToString(), strInvoiceNumber, strRemarks, intSentToAccounts, intSentToClientMethodID, dtDateTimeSent, intAudited, intOnHoldReasonID);
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while updating the Client Invoice Line Details.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Block Edit Client Invoice Line Details", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        Load_Data();  // Refresh grid to reflect record's updated status //
                        DevExpress.XtraEditors.XtraMessageBox.Show("Client Invoice Line(s) Updated Successfully.", "Block Edit Client Invoice Line Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:     // Gritting Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        System.Windows.Forms.Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ',';
                        }
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Edit fChildForm = new frm_GC_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 4:     // Snow Clearance Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        System.Windows.Forms.Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ',';
                        }
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Edit fChildForm = new frm_GC_Snow_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 5:     // Gritting / Snow Clearance Callouts //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl5.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        intCount = 0;
                        int intCount2 = 0;
                        string strRecordIDs2 = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (view.GetRowCellValue(intRowHandle, "RecordType").ToString() == "Gritting")
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CalloutID")) + ',';
                                intCount++;
                            }
                            else  // Snow Clearance //
                            {
                                strRecordIDs2 += Convert.ToString(view.GetRowCellValue(intRowHandle, "CalloutID")) + ',';
                                intCount2++;
                            }
                        }
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        if (!string.IsNullOrEmpty(strRecordIDs))  // Gritting //
                        {
                            fProgress = new frmProgress(10);
                            this.AddOwnedForm(fProgress);
                            fProgress.Show();  // ***** Closed in PostOpen event ***** //
                            System.Windows.Forms.Application.DoEvents();
                            
                            frm_GC_Callout_Edit fChildForm = new frm_GC_Callout_Edit();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "blockedit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            fChildForm.fProgress = fProgress;
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        if (!string.IsNullOrEmpty(strRecordIDs2))  // Snow Clearance //
                        {
                            fProgress = new frmProgress(10);
                            this.AddOwnedForm(fProgress);
                            fProgress.Show();  // ***** Closed in PostOpen event ***** //
                            System.Windows.Forms.Application.DoEvents();
                            
                            frm_GC_Snow_Callout_Edit fChildForm = new frm_GC_Snow_Callout_Edit();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs2;
                            fChildForm.strFormMode = "blockedit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount2;
                            fChildForm.FormPermissions = this.FormPermissions;
                            fChildForm.fProgress = fProgress;
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Set Invoice Number //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one Client Invoice Line to Edit before proceeding.", "Edit Client Invoice Line Details", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strClientInvoiceDetails = null;
                        string strInvoiceNumber = null;
                        int? intSentToAccounts = null;
                        int? intSentToClientMethodID = null;
                        DateTime? dtDateTimeSent = null;
                        int? intAudited = null;
                        int? intOnHoldReasonID = null;
                        string strRemarks = null;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strClientInvoiceDetails = "Client: " + (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "ClientName").ToString()) ? "Unknown Client" : view.GetRowCellValue(intRowHandle, "ClientName").ToString()) + "  -  Date: " + (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "DateRaised").ToString()) ? "Unknown Date" : Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "DateRaised")).ToString("dd/MM/yyyy HH:mm"));
                            try { if (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "SentToAccounts").ToString())) intSentToAccounts = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SentToAccounts")); } 
                            catch (Exception) { }
                            try { if (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "SentToClientMethodID").ToString())) intSentToClientMethodID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SentToClientMethodID")); }
                            catch (Exception) { }
                            try { if (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "DateTimeSent").ToString())) dtDateTimeSent = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "DateTimeSent")); }
                            catch (Exception) { }
                            try { if (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "Audited").ToString())) intAudited = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Audited")); }
                            catch (Exception) { }
                            try { if (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "OnHoldReasonID").ToString())) intOnHoldReasonID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "OnHoldReasonID")); }
                            catch (Exception) { }
                        }

                        frm_GC_Finance_Invoice_Set_Invoice_No fChildForm = new frm_GC_Finance_Invoice_Set_Invoice_No();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strClientInvoiceDetails = strClientInvoiceDetails;
                        fChildForm.strInvoiceNumber = strInvoiceNumber;
                        fChildForm.intSentToAccounts = intSentToAccounts;
                        fChildForm.intSentToClientMethodID = intSentToClientMethodID;
                        fChildForm.dtDateTimeSent = dtDateTimeSent;
                        fChildForm.intAudited = intAudited;
                        fChildForm.intOnHoldReasonID = intOnHoldReasonID;
                        fChildForm.strRemarks = strRemarks;

                        if (fChildForm.ShowDialog() != DialogResult.OK) return;

                        strInvoiceNumber = fChildForm.strInvoiceNumber;
                        intSentToAccounts = fChildForm.intSentToAccounts;
                        intSentToClientMethodID = fChildForm.intSentToClientMethodID;
                        dtDateTimeSent = fChildForm.dtDateTimeSent;
                        intAudited = fChildForm.intAudited;
                        intOnHoldReasonID = fChildForm.intOnHoldReasonID;
                        strRemarks = fChildForm.strRemarks;
                        string strMode = "edit";

                        DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter UpdateInvoiceLines = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                        UpdateInvoiceLines.ChangeConnectionString(strConnectionString);
                        StringBuilder sb = new StringBuilder();
                        try
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                sb.Append(view.GetRowCellValue(intRowHandle, "ClientInvoiceLineID").ToString());
                            }
                            UpdateInvoiceLines.sp04261_GC_Finance_Set_Invoice_Number(strMode, sb.ToString(), strInvoiceNumber, strRemarks, intSentToAccounts, intSentToClientMethodID, dtDateTimeSent, intAudited, intOnHoldReasonID);
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while updating the Client Invoice Line Details.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Edit Client Invoice Line Details", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        Load_Data();  // Refresh grid to reflect record's updated status //
                        DevExpress.XtraEditors.XtraMessageBox.Show("Client Invoice Line Updated Successfully.", "Edit Client Invoice Line Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:     // Gritting Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        System.Windows.Forms.Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ',';
                        }
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Edit fChildForm = new frm_GC_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Snow Clearance Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        System.Windows.Forms.Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ',';
                        }
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Edit fChildForm = new frm_GC_Snow_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 5:     // Gritting / Snow Clearance Callouts //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl5.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        intCount = 0;
                        int intCount2 = 0;
                        string strRecordIDs2 = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (view.GetRowCellValue(intRowHandle, "RecordType").ToString() == "Gritting")
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CalloutID")) + ',';
                                intCount++;
                            }
                            else  // Snow Clearance //
                            {
                                strRecordIDs2 += Convert.ToString(view.GetRowCellValue(intRowHandle, "CalloutID")) + ',';
                                intCount2++;
                            }
                        }
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        if (!string.IsNullOrEmpty(strRecordIDs))  // Gritting //
                        {
                            fProgress = new frmProgress(10);
                            this.AddOwnedForm(fProgress);
                            fProgress.Show();  // ***** Closed in PostOpen event ***** //
                            System.Windows.Forms.Application.DoEvents();
                            
                            frm_GC_Callout_Edit fChildForm = new frm_GC_Callout_Edit();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            fChildForm.fProgress = fProgress;
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        if (!string.IsNullOrEmpty(strRecordIDs2))  // Snow Clearance //
                        {
                            fProgress = new frmProgress(10);
                            this.AddOwnedForm(fProgress);
                            fProgress.Show();  // ***** Closed in PostOpen event ***** //
                            System.Windows.Forms.Application.DoEvents();
                            
                            frm_GC_Snow_Callout_Edit fChildForm = new frm_GC_Snow_Callout_Edit();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs2;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount2;
                            fChildForm.FormPermissions = this.FormPermissions;
                            fChildForm.fProgress = fProgress;
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Client Invoice Line //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Client Invoice Lines to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Client Invoice Line" : Convert.ToString(intRowHandles.Length) + " Client Invoice Lines") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Client Invoice Line" : "these Client Invoice Lines") + " will no longer be available for selection!\n\nIMPORTANT NOTE: Any Jobs linked will be un-linked making them available for inclusion on another Client Invoice Line.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            System.Windows.Forms.Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientInvoiceLineID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //


                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp03002_EP_Client_Delete("gc_client_invoice_line", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            Load_Data();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 2:  // Gritting Callout Line //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Gritting Callouts to remove from the Client Invoice Line by clicking on them then try again.", "Remove Callout from Client Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Gritting Callout" : Convert.ToString(intRowHandles.Length) + "Gritting Callouts") + " selected for removing!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Gritting Callout" : "these Gritting Callouts") + " will be un-linked from the CLient Invoice Line and the value of the Client Invoice Line will be reduced by the value of the un-linked callout(s).\n\nIMPORTANT NOTE: Any callouts un-linked will then be available for inclusion on another Client Invoice Line.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Un-Linking...");
                            fProgress.Show();
                            System.Windows.Forms.Application.DoEvents();

                            string strRecordIDs = "";
                            string strClientInvoiceLineIDs = ",";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ",";
                                if (!strClientInvoiceLineIDs.Contains(',' + view.GetRowCellValue(intRowHandle, "ClientInvoiceLineID").ToString() + ','))
                                {
                                    strClientInvoiceLineIDs += view.GetRowCellValue(intRowHandle, "ClientInvoiceLineID").ToString() + ',';
                                }
                            }
                            strClientInvoiceLineIDs = strClientInvoiceLineIDs.Remove(0, 1);  // Remove dummy ',' at start //
                            
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp04284_GC_Finance_Spreadsheets_Unlink_Callouts_And_Recalculate(strRecordIDs, 0, strClientInvoiceLineIDs);  // Unlink the records in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            Load_Data();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Callout(s) Removed from Client Invoice Line.", "Remove Callout from Client Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 3:  // Snow Clearance Callout Line //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Snow Clearance Callouts to remove from the Client Invoice Line by clicking on them then try again.", "Remove Callout from Client Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Snow Clearance Callout" : Convert.ToString(intRowHandles.Length) + "Snow Clearance Callouts") + " selected for removing!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Snow Clearance Callout" : "these Snow Clearance Callouts") + " will be un-linked from the CLient Invoice Line and the value of the Client Invoice Line will be reduced by the value of the un-linked callout(s).\n\nIMPORTANT NOTE: Any callouts un-linked will then be available for inclusion on another Client Invoice Line.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Un-Linking...");
                            fProgress.Show();
                            System.Windows.Forms.Application.DoEvents();

                            string strRecordIDs = "";
                            string strClientInvoiceLineIDs = ",";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ",";
                                if (!strClientInvoiceLineIDs.Contains(',' + view.GetRowCellValue(intRowHandle, "ClientInvoiceLineID").ToString() + ','))
                                {
                                    strClientInvoiceLineIDs += view.GetRowCellValue(intRowHandle, "ClientInvoiceLineID").ToString() + ',';
                                }
                            }
                            strClientInvoiceLineIDs = strClientInvoiceLineIDs.Remove(0, 1);  // Remove dummy ',' at start //

                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp04284_GC_Finance_Spreadsheets_Unlink_Callouts_And_Recalculate(strRecordIDs, 1, strClientInvoiceLineIDs);  // Unlink the records in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            Load_Data();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Callout(s) Removed from Client Invoice Line.", "Remove Callout from Client Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void bbiClientSpreadsheets_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0) 
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Client Invoice Lines to generate client spreadsheets before proceeding.", "Create Client Spreadsheets", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to create client spreadsheets for " + intCount.ToString() + " Client Invoice " + (intCount == 1 ? "Line" : "Lines") + ".\n\nProceed?", "Create Client Spreadsheets", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            // OK to proceed //

            frmProgress fProgress = new frmProgress(20);
            fProgress.UpdateCaption("Creating Spreadsheets...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intCount / 10;
            int intUpdateProgressTempCount = 0;

            int intClientInvoiceLineID = 0;
            int intClientID = 0;
            string strExportFileName = "";
            string strClientName = "";
            string strSkipColumns = "";
            DateTime? dtDate = null;
 
            int intBagsOfSaltColumnNumber = -1;
            int intSaltCostColumnNumber = -1;
            int intOtherCostColumnNumber = -1;
            int intLabourCostColumnNumber = -1;
            int intTotalCostColumnNumber = -1;

            SqlDataAdapter sdaJobDetails = new SqlDataAdapter();
            DataSet dsJobDetails = new DataSet("NewDataSet");
            SqlConnection SQlConn = new SqlConnection(strConnectionString);

            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter GetSkipColumns = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
            GetSkipColumns.ChangeConnectionString(strConnectionString);

            char[] delimiters = new char[] { ',' };

            foreach (int intRowHandle in intRowHandles)
            {
                intClientInvoiceLineID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientInvoiceLineID"));
                strClientName = view.GetRowCellValue(intRowHandle, "ClientName").ToString();
                intClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID"));
                dtDate = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "DateRaised"));

                intBagsOfSaltColumnNumber = 0;
                intSaltCostColumnNumber = 0;
                intOtherCostColumnNumber = 0;
                intLabourCostColumnNumber = 0;
                intTotalCostColumnNumber = 0;

                // Load Dataset //
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp04262_GC_Finance_Create_Client_Spreadsheets", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ClientInvoiceLineID", intClientInvoiceLineID));
                sdaJobDetails = new SqlDataAdapter(cmd);
                sdaJobDetails.Fill(dsJobDetails, "Table");
                if (dsJobDetails.Tables[0].Rows.Count <= 0) continue;

                // Create Spreadsheet //
                #region Create Excel File

                strExportFileName = i_str_SavedDirectoryName + "CLIENT EXPORT " + strClientName.ToUpper() + " " + Convert.ToDateTime(dtDate).ToString("yyyy-MM-dd_HH-mm-ss") + ".xls";

                Excel.Application xl = null;
                Excel._Workbook wb = null;
                Excel._Worksheet sheet = null;
                bool SaveChanges = false;

                try
                {
                    if (!Directory.Exists(i_str_SavedDirectoryName)) Directory.CreateDirectory(i_str_SavedDirectoryName);

                    if (File.Exists(strExportFileName)) File.Delete(strExportFileName);

                    GC.Collect();



                    // Create a new instance of Excel from scratch
                    xl = new Excel.Application();
                    xl.Visible = false;

                    // Add one workbook to the instance of Excel
                    wb = (Excel._Workbook)(xl.Workbooks.Add(Missing.Value));
                    wb.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                    sheet = (Excel._Worksheet)(wb.Sheets[1]); // Get a reference to the one and only worksheet in our workbook //
                    sheet.Name = "Callouts";  // Set column heading names //

                    // Remove any columns from the dataset which are not required for the client //
                    strSkipColumns = GetSkipColumns.sp04263_GC_Finance_Create_Client_Spreadsheets_Skip_Columns(intClientID).ToString();
                    if (!string.IsNullOrEmpty(strSkipColumns))
                    {
                        string[] strArray = strSkipColumns.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (strArray.Length > 0)
                        {
                            List<string> strList = new List<string>(strArray);  // Cast array to List so we can use Contains clause //
                            for (int j = dsJobDetails.Tables[0].Columns.Count - 1; j >= 0; j--)
                            {
                                if (strList.Contains(dsJobDetails.Tables[0].Columns[j].ColumnName))
                                {
                                    dsJobDetails.Tables[0].Columns.Remove(dsJobDetails.Tables[0].Columns[j].ColumnName);
                                }
                            }
                        }
                    }

                    // Copy data into array then write it in one hit to Excel //
                    object[,] arr = new object[dsJobDetails.Tables[0].Rows.Count, dsJobDetails.Tables[0].Columns.Count];
                    for (int r = 0; r < dsJobDetails.Tables[0].Rows.Count; r++)
                    {
                        DataRow dr = dsJobDetails.Tables[0].Rows[r];
                        for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                        {
                            arr[r, c] = dr[c];
                        }
                    }

                    Excel.Range c1 = (Excel.Range)xl.Cells[2, 1];
                    Excel.Range c2 = (Excel.Range)xl.Cells[2 + dsJobDetails.Tables[0].Rows.Count - 1, dsJobDetails.Tables[0].Columns.Count];
                    Excel.Range range = xl.get_Range(c1, c2);

                    range.Value = arr;

                    // Set column Header text and Autosize all columns //
                    int intColumnCount = 1;
                    for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                    {
                        sheet.Cells[1, intColumnCount] = dsJobDetails.Tables[0].Columns[c].ColumnName;
                        intColumnCount++;
                        ((Range)sheet.Cells[1, c + 1]).EntireColumn.AutoFit();  // Autosize columns //
                        switch (dsJobDetails.Tables[0].Columns[c].DataType.ToString().ToLower())
                        {
                            case "system.string":
                                // Do Nothing //
                                break;
                            case "system.decimal":
                                ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "0.00";
                                break;
                            case "system.datetime":
                                ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "dd/mm/yyyy HH:mm";
                                break;
                        }
                        // Store column position of columns which will need totals calculating at end of spreadsheet //
                        switch (dsJobDetails.Tables[0].Columns[c].ColumnName.ToLower())
                        {
                            case "bagsofsalt":
                                intBagsOfSaltColumnNumber = c + 1;
                                break;
                            case "saltcost":
                                intSaltCostColumnNumber = c + 1;
                                break;
                            case "othercost":
                                intOtherCostColumnNumber = c + 1;
                                break;
                            case "labourcost":
                                intLabourCostColumnNumber = c + 1;
                                break;
                            case "totalcost":
                                intTotalCostColumnNumber = c + 1;
                                break;
                        }
                    }

                    string strLastColumnExcelName = GetExcelColumnName(dsJobDetails.Tables[0].Columns.Count);

                    // Set Column Header back colour //
                    range = sheet.get_Range("A1", strLastColumnExcelName + "1");
                    range.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xFA, 0xBB, 0x6F));

                    // Colour odd and even row back colour //
                    int intCurrentRow = 2;  // start at 2 to skip past header row //
                    foreach (DataRow row in dsJobDetails.Tables[0].Rows)
                    {
                        Range range2 = sheet.get_Range("A" + intCurrentRow.ToString(), strLastColumnExcelName + intCurrentRow.ToString());
                        if ((intCurrentRow & 1) == 1)  // 1 = Odd row //
                        {
                            //range2.Interior.Color = Color.PaleGreen.ToArgb();
                            //range2.Interior.Color = intOddBackColour;
                            range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xB4, 0xFA, 0xB4));

                        }
                        else  //  0 = Even row //
                        {
                            //range2.Interior.Color = Color.NavajoWhite.ToArgb();
                            //range2.Interior.Color = intEvenBackColour;
                            range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.NavajoWhite);
                        }
                        intCurrentRow++;
                    }

                    // Get Full range of used cells on sheet //
                    Range last = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);

                    // Calculate Totals //
                    if (intBagsOfSaltColumnNumber > 0)
                    {
                        Range tempRange = sheet.get_Range(GetExcelColumnName(intBagsOfSaltColumnNumber) + (last.Row + 1).ToString(), GetExcelColumnName(intBagsOfSaltColumnNumber) + (last.Row + 1).ToString());
                        tempRange.Formula = "=Sum(" + GetExcelColumnName(intBagsOfSaltColumnNumber) + "2:" + GetExcelColumnName(intBagsOfSaltColumnNumber) + last.Row.ToString() + ")";
                        tempRange.NumberFormat = "0.00";
                        ((Range)sheet.Cells[1, intBagsOfSaltColumnNumber]).EntireColumn.AutoFit();  // Autosize column //
                    }
                    if (intSaltCostColumnNumber > 0)
                    {
                        Range tempRange = sheet.get_Range(GetExcelColumnName(intSaltCostColumnNumber) + (last.Row + 1).ToString(), GetExcelColumnName(intSaltCostColumnNumber) + (last.Row + 1).ToString());
                        tempRange.Formula = "=Sum(" + GetExcelColumnName(intSaltCostColumnNumber) + "2:" + GetExcelColumnName(intSaltCostColumnNumber) + last.Row.ToString() + ")";
                        tempRange.NumberFormat = "£0.00";
                        ((Range)sheet.Cells[1, intSaltCostColumnNumber]).EntireColumn.AutoFit();  // Autosize column //
                    }
                    if (intOtherCostColumnNumber > 0)
                    {
                        Range tempRange = sheet.get_Range(GetExcelColumnName(intOtherCostColumnNumber) + (last.Row + 1).ToString(), GetExcelColumnName(intOtherCostColumnNumber) + (last.Row + 1).ToString());
                        tempRange.Formula = "=Sum(" + GetExcelColumnName(intOtherCostColumnNumber) + "2:" + GetExcelColumnName(intOtherCostColumnNumber) + last.Row.ToString() + ")";
                        tempRange.NumberFormat = "£0.00";
                        ((Range)sheet.Cells[1, intOtherCostColumnNumber]).EntireColumn.AutoFit();  // Autosize column //
                    }
                    if (intLabourCostColumnNumber > 0)
                    {
                        Range tempRange = sheet.get_Range(GetExcelColumnName(intLabourCostColumnNumber) + (last.Row + 1).ToString(), GetExcelColumnName(intLabourCostColumnNumber) + (last.Row + 1).ToString());
                        tempRange.Formula = "=Sum(" + GetExcelColumnName(intLabourCostColumnNumber) + "2:" + GetExcelColumnName(intLabourCostColumnNumber) + last.Row.ToString() + ")";
                        tempRange.NumberFormat = "£0.00";
                        ((Range)sheet.Cells[1, intLabourCostColumnNumber]).EntireColumn.AutoFit();  // Autosize column //
                    }
                    if (intTotalCostColumnNumber > 0)
                    {
                        Range tempRange = sheet.get_Range(GetExcelColumnName(intTotalCostColumnNumber) + (last.Row + 1).ToString(), GetExcelColumnName(intTotalCostColumnNumber) + (last.Row + 1).ToString());
                        tempRange.Formula = "=Sum(" + GetExcelColumnName(intTotalCostColumnNumber) + "2:" + GetExcelColumnName(intTotalCostColumnNumber) + last.Row.ToString() + ")";
                        tempRange.NumberFormat = "£0.00";
                        ((Range)sheet.Cells[1, intTotalCostColumnNumber]).EntireColumn.AutoFit();  // Autosize column //
                    }
 
                    // Merge all cells of Totals Row up to the first totals column //
                    int intMin = 999;
                    if (intBagsOfSaltColumnNumber > 0 && intBagsOfSaltColumnNumber < intMin) intMin = intBagsOfSaltColumnNumber;
                    if (intSaltCostColumnNumber > 0 && intSaltCostColumnNumber < intMin) intMin = intSaltCostColumnNumber;
                    if (intOtherCostColumnNumber > 0 && intOtherCostColumnNumber < intMin) intMin = intOtherCostColumnNumber;
                    if (intLabourCostColumnNumber > 0 && intLabourCostColumnNumber < intMin) intMin = intLabourCostColumnNumber;
                    if (intTotalCostColumnNumber > 0 && intTotalCostColumnNumber < intMin) intMin = intTotalCostColumnNumber;
                    if (intMin != 999)
                    {
                        if (intMin > 2) intMin--; // Move back a column so we merge just before the totals column //
                        Range rangeExcludingTotals = sheet.get_Range("A" + (last.Row + 1).ToString(),  GetExcelColumnName(intMin) + (last.Row + 1).ToString());
                        rangeExcludingTotals.Merge();
                        rangeExcludingTotals = sheet.get_Range("A" + (last.Row + 1).ToString(), "A" + (last.Row + 1).ToString());
                        rangeExcludingTotals.Value = "Totals:";
                        // Set backcolour of Totals Row //
                        Range rangeTotals = sheet.get_Range("A" + (last.Row + 1).ToString(), strLastColumnExcelName + (last.Row + 1).ToString());
                        rangeTotals.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xFA, 0xBB, 0x6F));
                    }
                    last = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);  // Update Last cell now we have added totals //
                  
                    // Colour all cell borders Dark Orange //
                    Range range3 = sheet.get_Range("A1", last);
                    range3.Borders[XlBordersIndex.xlEdgeBottom].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                    range3.Borders[XlBordersIndex.xlEdgeLeft].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                    range3.Borders[XlBordersIndex.xlEdgeRight].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                    range3.Borders[XlBordersIndex.xlEdgeTop].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                    range3.Borders[XlBordersIndex.xlInsideHorizontal].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                    range3.Borders[XlBordersIndex.xlInsideVertical].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);

                    // Save and Close of the Excel instance
                    xl.Visible = false;
                    xl.UserControl = false;

                    // Set a flag saying that all is well and it is ok to save our changes to a file.
                    SaveChanges = true;
                    xl.DisplayAlerts = false;  // Prevent Compatibility Checked dialogue displaying due to cell colours etc //

                    //  Save the file to disk
                    wb.SaveAs(strExportFileName, Excel.XlFileFormat.xlWorkbookNormal,
                                null, null, false, false, Excel.XlSaveAsAccessMode.xlShared,
                                false, false, null, null);  //Excel.XlFileFormat.xlWorkbookNormal

                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Excel Spreadsheet [" + strExportFileName + "]. Message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Client Spreadsheets", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                finally
                {
                    try
                    {
                        // Repeat xl.Visible and xl.UserControl releases just to be sure we didn't error out ahead of time.
                        xl.Visible = false;
                        xl.UserControl = false;
                        // Close the document and avoid user prompts to save if our method failed.
                        wb.Close(SaveChanges, null, null);
                        xl.Workbooks.Close();
                    }
                    catch { }

                    // Gracefully exit out and destroy all COM objects to avoid hanging instances of Excel.exe whether our method failed or not.
                    xl.Quit();

                    //if (module != null) { Marshal.ReleaseComObject(module); }
                    if (sheet != null) { Marshal.ReleaseComObject(sheet); }
                    if (wb != null) { Marshal.ReleaseComObject(wb); }
                    if (xl != null) { Marshal.ReleaseComObject(xl); }

                    //module = null;
                    sheet = null;
                    wb = null;
                    xl = null;
                    GC.Collect();
                }
                #endregion

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            DevExpress.XtraEditors.XtraMessageBox.Show("Client Spreadsheet Creation Completed.\n\nFiles stored in: " + i_str_SavedDirectoryName + " Folder.", "Create Client Spreadsheets", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private string GetExcelColumnName(int columnIndex)
        {
            string columnName = "";

            // check whether the column count is > 26
            if (columnIndex > 26)
            {
                // If the column count is > 26, the the last column index will be something 
                // like "AA", "DE", "BC" etc

                // Get the first letter
                // ASCII index 65 represent char. 'A'. So, we use 64 in this calculation as a starting point
                char first = Convert.ToChar(64 + ((columnIndex - 1) / 26));

                // Get the second letter
                char second = Convert.ToChar(64 + (columnIndex % 26 == 0 ? 26 : columnIndex % 26));

                // Concat. them
                columnName = first.ToString() + second.ToString();
            }
            else
            {
                // ASCII index 65 represent char. 'A'. So, we use 64 in this calculation as a starting point
                columnName = Convert.ToChar(64 + columnIndex).ToString();
            }
            return columnName;
        }


        #region GridView5

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView5_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "SubContractorName")
            {
                switch (view.GetRowCellValue(e.RowHandle, "SubContractorName").ToString())
                {
                    case "":
                        //e.Appearance.BackColor = Color.LightCoral;
                        //e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }*/
        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void repositoryItemPopupContainerEditCompanyFilter2_QueryPopUp(object sender, CancelEventArgs e)
        {
            repositoryItemPopupContainerEditCompanyFilter2.PopupControl = popupContainerControlParameters;  // Needed as more than 1 Popup Container Edit cannot share the same PopupContainerControl //
        }

        private void bbiReloadData2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data2();
        }

        private void Load_Data2()
        {
            WaitDialogForm loading = new WaitDialogForm("Loading Jobs Available for Paying...", "Finance - Job Invoicing Manager");
            if (fProgress == null)
            {
                loading.Show();
            }
            else
            {
                loading.Hide();
                loading.Dispose();
            }
            if (i_str_selected_company_ids == null) i_str_selected_company_ids = "";
            GridView view = (GridView)gridControl5.MainView;
            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
            view.BeginUpdate();
            sp04265_GC_Finance_Team_Jobs_For_ExportTableAdapter.Fill(this.dataSet_GC_Core.sp04265_GC_Finance_Team_Jobs_For_Export, i_str_selected_company_ids);
            this.RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

            if (fProgress == null)
            {
                loading.Close();
                loading.Dispose();
            }




        }

        private void bbiFinanceTeamSpreadsheets_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            //int[] intRowHandles = view.GetSelectedRows();
            int intCount = selection5.SelectedCount;

            if (view.DataRowCount <= 0 || selection5.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to export to spreadsheet for Finance Team Payment Import by ticking them before proceeding.", "Create Team Payment Spreadsheet for Importing into Finance System", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intCount = selection5.SelectedCount;
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to export " + intCount.ToString() + (intCount == 1 ? " Callout" : " Callouts") + ".\n\nProceed?", "Create Team Payment Spreadsheet for Importing into Finance System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            // OK to proceed //
            frmProgress fProgress = new frmProgress(20);
            fProgress.UpdateCaption("Creating Spreadsheets...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            string strGrittingCallouts = "";
            string strSnowClearanceCallouts = "";
            int intCount2 = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    if (view.GetRowCellValue(i, "RecordType").ToString() == "Gritting")
                    {
                        strGrittingCallouts += view.GetRowCellValue(i, "CalloutID").ToString() + ",";
                    }
                    else  // Snow Clearance //
                    {
                        strSnowClearanceCallouts += view.GetRowCellValue(i, "CalloutID").ToString() + ",";
                    }
                    intCount2++;
                    if (intCount2 >= intCount) break;
                }
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            DateTime? dtDate = DateTime.Now;
            string strStartTime = Convert.ToDateTime(dtDate).ToString("yyyy-MM-dd_HH-mm-ss");
            string strCreatedTopLevelDirectory = "";
            string strExportFileName = "";

            #region Get_Settings
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strCreatedTopLevelDirectory = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "FinanceTeamCostsSSDefaultSavePath").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Client Job Breakdown Spreadsheets (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Team Export Spreadsheet Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strCreatedTopLevelDirectory.EndsWith("\\")) strCreatedTopLevelDirectory += "\\";  // Add Backslash to end //

            string FinanceTeamCostSSGrittingAnalysisCode = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                FinanceTeamCostSSGrittingAnalysisCode = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "FinanceTeamCostSSGrittingAnalysisCode").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Gritting Analysis Code for the Client Job Breakdown Spreadsheets (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Gritting Analysis Code Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string FinanceTeamCostSSSnowClearanceAnalysisCode = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                FinanceTeamCostSSSnowClearanceAnalysisCode = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "FinanceTeamCostSSSnowClearanceAnalysisCode").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Snow Clearance Analysis Code for the Client Job Breakdown Spreadsheets (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Snow Clearance Analysis Code Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string FinanceTeamCostSSGrittingCostCentre = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                FinanceTeamCostSSGrittingCostCentre = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "FinanceTeamCostSSGrittingCostCentre").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Gritting Cost Centre for the Client Job Breakdown Spreadsheets (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Gritting Cost Centre Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string FinanceTeamCostSSSnowClearanceCostCentre = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                FinanceTeamCostSSSnowClearanceCostCentre = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "FinanceTeamCostSSGrittingCostCentre").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Snow Clearance Cost Centre for the Client Job Breakdown Spreadsheets (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Snow Clearance Cost Centre Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            #endregion

            SqlDataAdapter sdaJobDetails = new SqlDataAdapter();
            DataSet dsJobDetails = new DataSet("NewDataSet");
            SqlConnection SQlConn = new SqlConnection(strConnectionString);

            // Create new top level dir to hold new spreadsheets //
            try
            {
                if (!Directory.Exists(strCreatedTopLevelDirectory)) Directory.CreateDirectory(strCreatedTopLevelDirectory);

            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Top level folder to hold the Export Spreadsheets. Message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Team Payment Spreadsheet for Importing into Finance System", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!string.IsNullOrEmpty(strGrittingCallouts))
            {
                // Create Spreadsheet for Gritting Internal Teams //
                SqlCommand cmd = null;
                sdaJobDetails = null;  // Reset //
                dsJobDetails.Clear();  // Reset //
                cmd = new SqlCommand("sp04266_GC_Finance_Create_Team_Spreadsheets_Internal_Gritting", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutIDs", strGrittingCallouts));
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceCalloutIDs", strSnowClearanceCallouts));
                cmd.Parameters.Add(new SqlParameter("@AnalysisCode", FinanceTeamCostSSGrittingAnalysisCode));
                cmd.Parameters.Add(new SqlParameter("@CostCentre", FinanceTeamCostSSGrittingCostCentre));
                cmd.Parameters.Add(new SqlParameter("@CurrentDateTime", dtDate));
                sdaJobDetails = new SqlDataAdapter(cmd);
                sdaJobDetails.Fill(dsJobDetails, "Table");
                if (dsJobDetails.Tables[0].Rows.Count > 0)
                {
                    #region Create Excel File

                    strExportFileName = strCreatedTopLevelDirectory + "GRITTING DE " + strStartTime + ".xls";

                    Excel.Application xl = null;
                    Excel._Workbook wb = null;
                    Excel._Worksheet sheet = null;
                    bool SaveChanges = false;

                    try
                    {
                        if (File.Exists(strExportFileName)) File.Delete(strExportFileName);
                        GC.Collect();

                        // Create a new instance of Excel from scratch
                        xl = new Excel.Application();
                        xl.Visible = false;
                        wb = (Excel._Workbook)(xl.Workbooks.Add(Missing.Value));  // Add one workbook to the instance of Excel
                        wb.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                        sheet = (Excel._Worksheet)(wb.Sheets[1]);  // Get a reference to the one and only worksheet in our workbook 
                        sheet.Name = "Teams by Clients";  // Set column heading names //

                        // Copy data into array then write it in one hit to Excel //
                        object[,] arr = new object[dsJobDetails.Tables[0].Rows.Count, dsJobDetails.Tables[0].Columns.Count];
                        for (int r = 0; r < dsJobDetails.Tables[0].Rows.Count; r++)
                        {
                            DataRow dr = dsJobDetails.Tables[0].Rows[r];
                            for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                            {
                                arr[r, c] = dr[c];
                            }
                        }

                        Excel.Range c1 = (Excel.Range)xl.Cells[2, 1];
                        Excel.Range c2 = (Excel.Range)xl.Cells[2 + dsJobDetails.Tables[0].Rows.Count - 1, dsJobDetails.Tables[0].Columns.Count];
                        Excel.Range range = xl.get_Range(c1, c2);

                        range.Value = arr;

                        // Set column Header text and Autosize all columns //
                        int intColumnCount = 1;
                        for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                        {
                            sheet.Cells[1, intColumnCount] = dsJobDetails.Tables[0].Columns[c].ColumnName;
                            intColumnCount++;
                            ((Range)sheet.Cells[1, c + 1]).EntireColumn.AutoFit();  // Autosize columns //
                            switch (dsJobDetails.Tables[0].Columns[c].DataType.ToString().ToLower())
                            {
                                case "system.string":
                                    // Do Nothing //
                                    break;
                                case "system.decimal":
                                    ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "0.00";
                                    break;
                                case "system.datetime":
                                    ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "dd/mm/yyyy";
                                    break;
                            }
                        }

                        string strLastColumnExcelName = GetExcelColumnName(dsJobDetails.Tables[0].Columns.Count);

                        // Set Column Header back colour //
                        range = sheet.get_Range("A1", strLastColumnExcelName + "1");
                        range.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xFA, 0xBB, 0x6F));

                        // Colour odd and even row back colour //
                        int intCurrentRow = 2;  // start at 2 to skip past header row //
                        foreach (DataRow row in dsJobDetails.Tables[0].Rows)
                        {
                            Range range2 = sheet.get_Range("A" + intCurrentRow.ToString(), strLastColumnExcelName + intCurrentRow.ToString());
                            if ((intCurrentRow & 1) == 1)  // 1 = Odd row //
                            {
                                 range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xB4, 0xFA, 0xB4));

                            }
                            else  //  0 = Even row //
                            {
                                range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.NavajoWhite);
                            }
                            intCurrentRow++;
                        }

                        // Get Full range of used cells on sheet //
                        Range last = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);

                        // Colour all cell borders Dark Orange //
                        Range range3 = sheet.get_Range("A1", last);
                        range3.Borders[XlBordersIndex.xlEdgeBottom].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeLeft].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeRight].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeTop].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlInsideHorizontal].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlInsideVertical].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);

                        // Save and Close of the Excel instance
                        xl.Visible = false;
                        xl.UserControl = false;

                        // Set a flag saying that all is well and it is ok to save our changes to a file.
                        SaveChanges = true;
                        xl.DisplayAlerts = false;  // Prevent Compatibility Checked dialogue displaying due to cell colours etc //

                        //  Save the file to disk
                        wb.SaveAs(strExportFileName, Excel.XlFileFormat.xlWorkbookNormal,
                                    null, null, false, false, Excel.XlSaveAsAccessMode.xlShared,
                                    false, false, null, null);  //Excel.XlFileFormat.xlWorkbookNormal

                    }
                    catch (Exception ex)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Excel Spreadsheet [" + strExportFileName + "]. Message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Team Payment Spreadsheet for Importing into Finance System", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    finally
                    {
                        try
                        {
                            // Repeat xl.Visible and xl.UserControl releases just to be sure we didn't error out ahead of time.
                            xl.Visible = false;
                            xl.UserControl = false;
                            // Close the document and avoid user prompts to save if our method failed.
                            wb.Close(SaveChanges, null, null);
                            xl.Workbooks.Close();
                        }
                        catch { }

                        // Gracefully exit out and destroy all COM objects to avoid hanging instances of Excel.exe whether our method failed or not.
                        xl.Quit();

                        //if (module != null) { Marshal.ReleaseComObject(module); }
                        if (sheet != null) { Marshal.ReleaseComObject(sheet); }
                        if (wb != null) { Marshal.ReleaseComObject(wb); }
                        if (xl != null) { Marshal.ReleaseComObject(xl); }

                        //module = null;
                        sheet = null;
                        wb = null;
                        xl = null;
                        GC.Collect();
                    }
                    #endregion
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                // Create Spreadsheet for Gritting External Teams //
                cmd = null;
                sdaJobDetails = null;  // Reset //
                dsJobDetails.Clear();  // Reset //
                cmd = new SqlCommand("sp04268_GC_Finance_Create_Team_Spreadsheets_External_Gritting", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutIDs", strGrittingCallouts));
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceCalloutIDs", strSnowClearanceCallouts));
                cmd.Parameters.Add(new SqlParameter("@AnalysisCode", FinanceTeamCostSSGrittingAnalysisCode));
                cmd.Parameters.Add(new SqlParameter("@CostCentre", FinanceTeamCostSSGrittingCostCentre));
                cmd.Parameters.Add(new SqlParameter("@CurrentDateTime", dtDate));
                sdaJobDetails = new SqlDataAdapter(cmd);
                sdaJobDetails.Fill(dsJobDetails, "Table");
                if (dsJobDetails.Tables[0].Rows.Count > 0)
                {
                    #region Create Excel File

                    strExportFileName = strCreatedTopLevelDirectory + "GRITTING " + strStartTime + ".xls";

                    Excel.Application xl = null;
                    Excel._Workbook wb = null;
                    Excel._Worksheet sheet = null;
                    bool SaveChanges = false;

                    try
                    {
                        if (File.Exists(strExportFileName)) File.Delete(strExportFileName);
                        GC.Collect();

                        // Create a new instance of Excel from scratch
                        xl = new Excel.Application();
                        xl.Visible = false;
                        wb = (Excel._Workbook)(xl.Workbooks.Add(Missing.Value));  // Add one workbook to the instance of Excel
                        wb.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                        sheet = (Excel._Worksheet)(wb.Sheets[1]);  // Get a reference to the one and only worksheet in our workbook 
                        sheet.Name = "Teams by Clients";  // Set column heading names //

                        // Copy data into array then write it in one hit to Excel //
                        object[,] arr = new object[dsJobDetails.Tables[0].Rows.Count, dsJobDetails.Tables[0].Columns.Count];
                        for (int r = 0; r < dsJobDetails.Tables[0].Rows.Count; r++)
                        {
                            DataRow dr = dsJobDetails.Tables[0].Rows[r];
                            for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                            {
                                arr[r, c] = dr[c];
                            }
                        }

                        Excel.Range c1 = (Excel.Range)xl.Cells[2, 1];
                        Excel.Range c2 = (Excel.Range)xl.Cells[2 + dsJobDetails.Tables[0].Rows.Count - 1, dsJobDetails.Tables[0].Columns.Count];
                        Excel.Range range = xl.get_Range(c1, c2);

                        range.Value = arr;

                        // Set column Header text and Autosize all columns //
                        int intColumnCount = 1;
                        for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                        {
                            sheet.Cells[1, intColumnCount] = dsJobDetails.Tables[0].Columns[c].ColumnName;
                            intColumnCount++;
                            ((Range)sheet.Cells[1, c + 1]).EntireColumn.AutoFit();  // Autosize columns //
                            switch (dsJobDetails.Tables[0].Columns[c].DataType.ToString().ToLower())
                            {
                                case "system.string":
                                    // Do Nothing //
                                    break;
                                case "system.decimal":
                                    ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "0.00";
                                    break;
                                case "system.datetime":
                                    ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "dd/mm/yyyy";
                                    break;
                            }
                        }

                        string strLastColumnExcelName = GetExcelColumnName(dsJobDetails.Tables[0].Columns.Count);

                        // Set Column Header back colour //
                        range = sheet.get_Range("A1", strLastColumnExcelName + "1");
                        range.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xFA, 0xBB, 0x6F));

                        // Colour odd and even row back colour //
                        int intCurrentRow = 2;  // start at 2 to skip past header row //
                        foreach (DataRow row in dsJobDetails.Tables[0].Rows)
                        {
                            Range range2 = sheet.get_Range("A" + intCurrentRow.ToString(), strLastColumnExcelName + intCurrentRow.ToString());
                            if ((intCurrentRow & 1) == 1)  // 1 = Odd row //
                            {
                                range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xB4, 0xFA, 0xB4));

                            }
                            else  //  0 = Even row //
                            {
                                range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.NavajoWhite);
                            }
                            intCurrentRow++;
                        }

                        // Get Full range of used cells on sheet //
                        Range last = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);

                        // Colour all cell borders Dark Orange //
                        Range range3 = sheet.get_Range("A1", last);
                        range3.Borders[XlBordersIndex.xlEdgeBottom].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeLeft].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeRight].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeTop].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlInsideHorizontal].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlInsideVertical].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);

                        // Save and Close of the Excel instance
                        xl.Visible = false;
                        xl.UserControl = false;

                        // Set a flag saying that all is well and it is ok to save our changes to a file.
                        SaveChanges = true;
                        xl.DisplayAlerts = false;  // Prevent Compatibility Checked dialogue displaying due to cell colours etc //

                        //  Save the file to disk
                        wb.SaveAs(strExportFileName, Excel.XlFileFormat.xlWorkbookNormal,
                                    null, null, false, false, Excel.XlSaveAsAccessMode.xlShared,
                                    false, false, null, null);  //Excel.XlFileFormat.xlWorkbookNormal

                    }
                    catch (Exception ex)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Excel Spreadsheet [" + strExportFileName + "]. Message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Team Payment Spreadsheet for Importing into Finance System", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    finally
                    {
                        try
                        {
                            // Repeat xl.Visible and xl.UserControl releases just to be sure we didn't error out ahead of time.
                            xl.Visible = false;
                            xl.UserControl = false;
                            // Close the document and avoid user prompts to save if our method failed.
                            wb.Close(SaveChanges, null, null);
                            xl.Workbooks.Close();
                        }
                        catch { }

                        // Gracefully exit out and destroy all COM objects to avoid hanging instances of Excel.exe whether our method failed or not.
                        xl.Quit();

                        //if (module != null) { Marshal.ReleaseComObject(module); }
                        if (sheet != null) { Marshal.ReleaseComObject(sheet); }
                        if (wb != null) { Marshal.ReleaseComObject(wb); }
                        if (xl != null) { Marshal.ReleaseComObject(xl); }

                        //module = null;
                        sheet = null;
                        wb = null;
                        xl = null;
                        GC.Collect();
                    }
                    #endregion
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            }
            else
            {
                if (fProgress != null) fProgress.UpdateProgress(40); // Update Progress Bar //
            }
            if (!string.IsNullOrEmpty(strSnowClearanceCallouts))
            {
                // Create Spreadsheet for Snow Clearance Internal Teams //
                SqlCommand cmd = null;
                sdaJobDetails = null;  // Reset //
                dsJobDetails.Clear();  // Reset //
                cmd = new SqlCommand("sp04267_GC_Finance_Create_Team_Spreadsheets_Internal_SnowClearance", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutIDs", strGrittingCallouts));
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceCalloutIDs", strSnowClearanceCallouts));
                cmd.Parameters.Add(new SqlParameter("@AnalysisCode", FinanceTeamCostSSGrittingAnalysisCode));
                cmd.Parameters.Add(new SqlParameter("@CostCentre", FinanceTeamCostSSGrittingCostCentre));
                cmd.Parameters.Add(new SqlParameter("@CurrentDateTime", dtDate));
                sdaJobDetails = new SqlDataAdapter(cmd);
                sdaJobDetails.Fill(dsJobDetails, "Table");
                if (dsJobDetails.Tables[0].Rows.Count > 0)
                {
                    #region Create Excel File

                    strExportFileName = strCreatedTopLevelDirectory + "SNOW CLEARANCE DE " + strStartTime + ".xls";

                    Excel.Application xl = null;
                    Excel._Workbook wb = null;
                    Excel._Worksheet sheet = null;
                    bool SaveChanges = false;

                    try
                    {
                        if (File.Exists(strExportFileName)) File.Delete(strExportFileName);
                        GC.Collect();

                        // Create a new instance of Excel from scratch
                        xl = new Excel.Application();
                        xl.Visible = false;
                        wb = (Excel._Workbook)(xl.Workbooks.Add(Missing.Value));  // Add one workbook to the instance of Excel
                        wb.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                        sheet = (Excel._Worksheet)(wb.Sheets[1]);  // Get a reference to the one and only worksheet in our workbook 
                        sheet.Name = "Teams by Clients";  // Set column heading names //

                        // Copy data into array then write it in one hit to Excel //
                        object[,] arr = new object[dsJobDetails.Tables[0].Rows.Count, dsJobDetails.Tables[0].Columns.Count];
                        for (int r = 0; r < dsJobDetails.Tables[0].Rows.Count; r++)
                        {
                            DataRow dr = dsJobDetails.Tables[0].Rows[r];
                            for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                            {
                                arr[r, c] = dr[c];
                            }
                        }

                        Excel.Range c1 = (Excel.Range)xl.Cells[2, 1];
                        Excel.Range c2 = (Excel.Range)xl.Cells[2 + dsJobDetails.Tables[0].Rows.Count - 1, dsJobDetails.Tables[0].Columns.Count];
                        Excel.Range range = xl.get_Range(c1, c2);

                        range.Value = arr;

                        // Set column Header text and Autosize all columns //
                        int intColumnCount = 1;
                        for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                        {
                            sheet.Cells[1, intColumnCount] = dsJobDetails.Tables[0].Columns[c].ColumnName;
                            intColumnCount++;
                            ((Range)sheet.Cells[1, c + 1]).EntireColumn.AutoFit();  // Autosize columns //
                            switch (dsJobDetails.Tables[0].Columns[c].DataType.ToString().ToLower())
                            {
                                case "system.string":
                                    // Do Nothing //
                                    break;
                                case "system.decimal":
                                    ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "0.00";
                                    break;
                                case "system.datetime":
                                    ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "dd/mm/yyyy";
                                    break;
                            }
                        }

                        string strLastColumnExcelName = GetExcelColumnName(dsJobDetails.Tables[0].Columns.Count);

                        // Set Column Header back colour //
                        range = sheet.get_Range("A1", strLastColumnExcelName + "1");
                        range.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xFA, 0xBB, 0x6F));

                        // Colour odd and even row back colour //
                        int intCurrentRow = 2;  // start at 2 to skip past header row //
                        foreach (DataRow row in dsJobDetails.Tables[0].Rows)
                        {
                            Range range2 = sheet.get_Range("A" + intCurrentRow.ToString(), strLastColumnExcelName + intCurrentRow.ToString());
                            if ((intCurrentRow & 1) == 1)  // 1 = Odd row //
                            {
                                range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xB4, 0xFA, 0xB4));

                            }
                            else  //  0 = Even row //
                            {
                                range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.NavajoWhite);
                            }
                            intCurrentRow++;
                        }

                        // Get Full range of used cells on sheet //
                        Range last = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);

                        // Colour all cell borders Dark Orange //
                        Range range3 = sheet.get_Range("A1", last);
                        range3.Borders[XlBordersIndex.xlEdgeBottom].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeLeft].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeRight].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeTop].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlInsideHorizontal].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlInsideVertical].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);

                        // Save and Close of the Excel instance
                        xl.Visible = false;
                        xl.UserControl = false;

                        // Set a flag saying that all is well and it is ok to save our changes to a file.
                        SaveChanges = true;
                        xl.DisplayAlerts = false;  // Prevent Compatibility Checked dialogue displaying due to cell colours etc //

                        //  Save the file to disk
                        wb.SaveAs(strExportFileName, Excel.XlFileFormat.xlWorkbookNormal,
                                    null, null, false, false, Excel.XlSaveAsAccessMode.xlShared,
                                    false, false, null, null);  //Excel.XlFileFormat.xlWorkbookNormal

                    }
                    catch (Exception ex)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Excel Spreadsheet [" + strExportFileName + "]. Message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Team Payment Spreadsheet for Importing into Finance System", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    finally
                    {
                        try
                        {
                            // Repeat xl.Visible and xl.UserControl releases just to be sure we didn't error out ahead of time.
                            xl.Visible = false;
                            xl.UserControl = false;
                            // Close the document and avoid user prompts to save if our method failed.
                            wb.Close(SaveChanges, null, null);
                            xl.Workbooks.Close();
                        }
                        catch { }

                        // Gracefully exit out and destroy all COM objects to avoid hanging instances of Excel.exe whether our method failed or not.
                        xl.Quit();

                        //if (module != null) { Marshal.ReleaseComObject(module); }
                        if (sheet != null) { Marshal.ReleaseComObject(sheet); }
                        if (wb != null) { Marshal.ReleaseComObject(wb); }
                        if (xl != null) { Marshal.ReleaseComObject(xl); }

                        //module = null;
                        sheet = null;
                        wb = null;
                        xl = null;
                        GC.Collect();
                    }
                    #endregion
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                // Create Spreadsheet for Gritting External Teams //
                cmd = null;
                sdaJobDetails = null;  // Reset //
                dsJobDetails.Clear();  // Reset //
                cmd = new SqlCommand("sp04269_GC_Finance_Create_Team_Spreadsheets_External_SnowClearance", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutIDs", strGrittingCallouts));
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceCalloutIDs", strSnowClearanceCallouts));
                cmd.Parameters.Add(new SqlParameter("@AnalysisCode", FinanceTeamCostSSGrittingAnalysisCode));
                cmd.Parameters.Add(new SqlParameter("@CostCentre", FinanceTeamCostSSGrittingCostCentre));
                cmd.Parameters.Add(new SqlParameter("@CurrentDateTime", dtDate));
                sdaJobDetails = new SqlDataAdapter(cmd);
                sdaJobDetails.Fill(dsJobDetails, "Table");
                if (dsJobDetails.Tables[0].Rows.Count > 0)
                {
                    #region Create Excel File

                    strExportFileName = strCreatedTopLevelDirectory + "SNOW CLEARANCE " + strStartTime + ".xls";

                    Excel.Application xl = null;
                    Excel._Workbook wb = null;
                    Excel._Worksheet sheet = null;
                    bool SaveChanges = false;

                    try
                    {
                        if (File.Exists(strExportFileName)) File.Delete(strExportFileName);
                        GC.Collect();

                        // Create a new instance of Excel from scratch
                        xl = new Excel.Application();
                        xl.Visible = false;
                        wb = (Excel._Workbook)(xl.Workbooks.Add(Missing.Value));  // Add one workbook to the instance of Excel
                        wb.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                        sheet = (Excel._Worksheet)(wb.Sheets[1]);  // Get a reference to the one and only worksheet in our workbook 
                        sheet.Name = "Teams by Clients";  // Set column heading names //

                        // Copy data into array then write it in one hit to Excel //
                        object[,] arr = new object[dsJobDetails.Tables[0].Rows.Count, dsJobDetails.Tables[0].Columns.Count];
                        for (int r = 0; r < dsJobDetails.Tables[0].Rows.Count; r++)
                        {
                            DataRow dr = dsJobDetails.Tables[0].Rows[r];
                            for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                            {
                                arr[r, c] = dr[c];
                            }
                        }

                        Excel.Range c1 = (Excel.Range)xl.Cells[2, 1];
                        Excel.Range c2 = (Excel.Range)xl.Cells[2 + dsJobDetails.Tables[0].Rows.Count - 1, dsJobDetails.Tables[0].Columns.Count];
                        Excel.Range range = xl.get_Range(c1, c2);

                        range.Value = arr;

                        // Set column Header text and Autosize all columns //
                        int intColumnCount = 1;
                        for (int c = 0; c < dsJobDetails.Tables[0].Columns.Count; c++)
                        {
                            sheet.Cells[1, intColumnCount] = dsJobDetails.Tables[0].Columns[c].ColumnName;
                            intColumnCount++;
                            ((Range)sheet.Cells[1, c + 1]).EntireColumn.AutoFit();  // Autosize columns //
                            switch (dsJobDetails.Tables[0].Columns[c].DataType.ToString().ToLower())
                            {
                                case "system.string":
                                    // Do Nothing //
                                    break;
                                case "system.decimal":
                                    ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "0.00";
                                    break;
                                case "system.datetime":
                                    ((Range)sheet.Cells[2, c + 1]).EntireColumn.NumberFormat = "dd/mm/yyyy";
                                    break;
                            }
                        }

                        string strLastColumnExcelName = GetExcelColumnName(dsJobDetails.Tables[0].Columns.Count);

                        // Set Column Header back colour //
                        range = sheet.get_Range("A1", strLastColumnExcelName + "1");
                        range.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xFA, 0xBB, 0x6F));

                        // Colour odd and even row back colour //
                        int intCurrentRow = 2;  // start at 2 to skip past header row //
                        foreach (DataRow row in dsJobDetails.Tables[0].Rows)
                        {
                            Range range2 = sheet.get_Range("A" + intCurrentRow.ToString(), strLastColumnExcelName + intCurrentRow.ToString());
                            if ((intCurrentRow & 1) == 1)  // 1 = Odd row //
                            {
                                range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xB4, 0xFA, 0xB4));

                            }
                            else  //  0 = Even row //
                            {
                                range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.NavajoWhite);
                            }
                            intCurrentRow++;
                        }

                        // Get Full range of used cells on sheet //
                        Range last = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);

                        // Colour all cell borders Dark Orange //
                        Range range3 = sheet.get_Range("A1", last);
                        range3.Borders[XlBordersIndex.xlEdgeBottom].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeLeft].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeRight].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlEdgeTop].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlInsideHorizontal].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                        range3.Borders[XlBordersIndex.xlInsideVertical].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);

                        // Save and Close of the Excel instance
                        xl.Visible = false;
                        xl.UserControl = false;

                        // Set a flag saying that all is well and it is ok to save our changes to a file.
                        SaveChanges = true;
                        xl.DisplayAlerts = false;  // Prevent Compatibility Checked dialogue displaying due to cell colours etc //

                        //  Save the file to disk
                        wb.SaveAs(strExportFileName, Excel.XlFileFormat.xlWorkbookNormal,
                                    null, null, false, false, Excel.XlSaveAsAccessMode.xlShared,
                                    false, false, null, null);  //Excel.XlFileFormat.xlWorkbookNormal

                    }
                    catch (Exception ex)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Excel Spreadsheet [" + strExportFileName + "]. Message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Team Payment Spreadsheet for Importing into Finance System", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    finally
                    {
                        try
                        {
                            // Repeat xl.Visible and xl.UserControl releases just to be sure we didn't error out ahead of time.
                            xl.Visible = false;
                            xl.UserControl = false;
                            // Close the document and avoid user prompts to save if our method failed.
                            wb.Close(SaveChanges, null, null);
                            xl.Workbooks.Close();
                        }
                        catch { }

                        // Gracefully exit out and destroy all COM objects to avoid hanging instances of Excel.exe whether our method failed or not.
                        xl.Quit();

                        //if (module != null) { Marshal.ReleaseComObject(module); }
                        if (sheet != null) { Marshal.ReleaseComObject(sheet); }
                        if (wb != null) { Marshal.ReleaseComObject(wb); }
                        if (xl != null) { Marshal.ReleaseComObject(xl); }

                        //module = null;
                        sheet = null;
                        wb = null;
                        xl = null;
                        GC.Collect();
                    }
                    #endregion
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            }
            else
            {
                if (fProgress != null) fProgress.UpdateProgress(40); // Update Progress Bar //
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            DevExpress.XtraEditors.XtraMessageBox.Show("Spreadsheet Completion Completed.\n\nGenerated Files Stored in: " + strCreatedTopLevelDirectory + " Folder.", "Create Team Payment Spreadsheet for Importing into Finance System", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiPaidComplete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            //int[] intRowHandles = view.GetSelectedRows();
            int intCount = selection5.SelectedCount;

            if (view.DataRowCount <= 0 || selection5.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to set as Paid by ticking them before proceeding.", "Set Callouts as Paid", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intCount = selection5.SelectedCount;
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to set " + intCount.ToString() + (intCount == 1 ? " Callout" : " Callouts") + " as Paid.\n\nProceed?", "Create Team Payment Spreadsheet for Importing into Finance System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            // OK to proceed //
            frmProgress fProgress = new frmProgress(20);
            fProgress.UpdateCaption("Updating Callouts...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            string strGrittingCallouts = "";
            string strSnowClearanceCallouts = "";
            int intCount2 = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    if (view.GetRowCellValue(i, "RecordType").ToString() == "Gritting")
                    {
                        strGrittingCallouts += view.GetRowCellValue(i, "CalloutID").ToString() + ",";
                    }
                    else  // Snow Clearance //
                    {
                        strSnowClearanceCallouts += view.GetRowCellValue(i, "CalloutID").ToString() + ",";
                    }
                    intCount2++;
                    if (intCount2 >= intCount) break;
                }
            }
            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Fire SP to update Gritting Callouts //
            if (!string.IsNullOrEmpty(strGrittingCallouts))
            {
                DataSet_GC_CoreTableAdapters.QueriesTableAdapter UpdateRecords = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                UpdateRecords.ChangeConnectionString(strConnectionString);
                UpdateRecords.sp04271_GC_Finance_Set_Job_Status_Paid(0, strGrittingCallouts, this.GlobalSettings.UserID);  // Set status of jobs to paid //
            }
            if (fProgress != null) fProgress.UpdateProgress(40); // Update Progress Bar //
            
            // Fire SP to update Snow Clearance Callouts //
            if (!string.IsNullOrEmpty(strSnowClearanceCallouts))
            {
                DataSet_GC_CoreTableAdapters.QueriesTableAdapter UpdateRecords = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                UpdateRecords.ChangeConnectionString(strConnectionString);
                UpdateRecords.sp04271_GC_Finance_Set_Job_Status_Paid(1, strSnowClearanceCallouts, this.GlobalSettings.UserID);  // Set status of jobs to paid //
            }
            if (fProgress != null) fProgress.UpdateProgress(40); // Update Progress Bar //
  
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data2();
            DevExpress.XtraEditors.XtraMessageBox.Show("Callouts set as Paid.", "Set Callouts as Paid", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }


   }
}

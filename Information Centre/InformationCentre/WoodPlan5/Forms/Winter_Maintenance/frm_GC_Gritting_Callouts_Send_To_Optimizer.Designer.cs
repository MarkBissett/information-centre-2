﻿namespace WoodPlan5
{
    partial class frm_GC_Gritting_Callouts_Send_To_Optimizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04355GCExportGrittingCalloutsForRouteOptimisationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colACTION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORDERTYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colORDERREF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDEPNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colADDRESS_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colADDRESS_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colADDRESS_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colADDRESS_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colADDRESS_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZIPCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPREF_DEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPROD10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWORKTIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOOKDAY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBOOKTIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOPEN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCLOSE1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOPEN2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCLOSE2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUST_INFO1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUST_INFO2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUST_INFO3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUST_INFO4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCUST_INFO5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCAP_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCAP_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepFleet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp04355_GC_Export_Gritting_Callouts_For_Route_OptimisationTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04355_GC_Export_Gritting_Callouts_For_Route_OptimisationTableAdapter();
            this.btnExport = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.colVehGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04355GCExportGrittingCalloutsForRouteOptimisationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(744, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 418);
            this.barDockControlBottom.Size = new System.Drawing.Size(744, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 392);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(744, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 392);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp04355GCExportGrittingCalloutsForRouteOptimisationBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 27);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(744, 356);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04355GCExportGrittingCalloutsForRouteOptimisationBindingSource
            // 
            this.sp04355GCExportGrittingCalloutsForRouteOptimisationBindingSource.DataMember = "sp04355_GC_Export_Gritting_Callouts_For_Route_Optimisation";
            this.sp04355GCExportGrittingCalloutsForRouteOptimisationBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colACTION,
            this.colORDERTYPE,
            this.colORDERREF,
            this.colNAME,
            this.colDEPNAME,
            this.colADDRESS_1,
            this.colADDRESS_2,
            this.colADDRESS_3,
            this.colADDRESS_4,
            this.colADDRESS_5,
            this.colZIPCODE,
            this.colPREF_DEP,
            this.colPROD1,
            this.colPROD2,
            this.colPROD3,
            this.colPROD4,
            this.colPROD5,
            this.colPROD6,
            this.colPROD7,
            this.colPROD8,
            this.colPROD9,
            this.colPROD10,
            this.colWORKTIME,
            this.colBOOKDAY,
            this.colBOOKTIME,
            this.colOPEN1,
            this.colCLOSE1,
            this.colOPEN2,
            this.colCLOSE2,
            this.colCUST_INFO1,
            this.colCUST_INFO2,
            this.colCUST_INFO3,
            this.colCUST_INFO4,
            this.colCUST_INFO5,
            this.colTYPE,
            this.colDepGroup,
            this.colCAP_1,
            this.colCAP_2,
            this.colDepFleet,
            this.colVehGroup,
            this.colVehicle,
            this.colDummyOrder});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDummyOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colORDERREF, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            // 
            // colACTION
            // 
            this.colACTION.FieldName = "ACTION";
            this.colACTION.Name = "colACTION";
            this.colACTION.OptionsColumn.AllowEdit = false;
            this.colACTION.OptionsColumn.AllowFocus = false;
            this.colACTION.OptionsColumn.ReadOnly = true;
            this.colACTION.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colACTION.Visible = true;
            this.colACTION.VisibleIndex = 0;
            // 
            // colORDERTYPE
            // 
            this.colORDERTYPE.FieldName = "ORDERTYPE";
            this.colORDERTYPE.Name = "colORDERTYPE";
            this.colORDERTYPE.OptionsColumn.AllowEdit = false;
            this.colORDERTYPE.OptionsColumn.AllowFocus = false;
            this.colORDERTYPE.OptionsColumn.ReadOnly = true;
            this.colORDERTYPE.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colORDERTYPE.Visible = true;
            this.colORDERTYPE.VisibleIndex = 1;
            this.colORDERTYPE.Width = 81;
            // 
            // colORDERREF
            // 
            this.colORDERREF.FieldName = "ORDERREF";
            this.colORDERREF.Name = "colORDERREF";
            this.colORDERREF.OptionsColumn.AllowEdit = false;
            this.colORDERREF.OptionsColumn.AllowFocus = false;
            this.colORDERREF.OptionsColumn.ReadOnly = true;
            this.colORDERREF.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colORDERREF.Visible = true;
            this.colORDERREF.VisibleIndex = 2;
            this.colORDERREF.Width = 89;
            // 
            // colNAME
            // 
            this.colNAME.FieldName = "NAME";
            this.colNAME.Name = "colNAME";
            this.colNAME.OptionsColumn.AllowEdit = false;
            this.colNAME.OptionsColumn.AllowFocus = false;
            this.colNAME.OptionsColumn.ReadOnly = true;
            this.colNAME.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNAME.Visible = true;
            this.colNAME.VisibleIndex = 3;
            // 
            // colDEPNAME
            // 
            this.colDEPNAME.Caption = "DEPNAME";
            this.colDEPNAME.FieldName = "DEPNAME";
            this.colDEPNAME.Name = "colDEPNAME";
            this.colDEPNAME.OptionsColumn.AllowEdit = false;
            this.colDEPNAME.OptionsColumn.AllowFocus = false;
            this.colDEPNAME.OptionsColumn.ReadOnly = true;
            this.colDEPNAME.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDEPNAME.Visible = true;
            this.colDEPNAME.VisibleIndex = 4;
            this.colDEPNAME.Width = 99;
            // 
            // colADDRESS_1
            // 
            this.colADDRESS_1.FieldName = "ADDRESS_1";
            this.colADDRESS_1.Name = "colADDRESS_1";
            this.colADDRESS_1.OptionsColumn.AllowEdit = false;
            this.colADDRESS_1.OptionsColumn.AllowFocus = false;
            this.colADDRESS_1.OptionsColumn.ReadOnly = true;
            this.colADDRESS_1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colADDRESS_1.Visible = true;
            this.colADDRESS_1.VisibleIndex = 5;
            this.colADDRESS_1.Width = 80;
            // 
            // colADDRESS_2
            // 
            this.colADDRESS_2.FieldName = "ADDRESS_2";
            this.colADDRESS_2.Name = "colADDRESS_2";
            this.colADDRESS_2.OptionsColumn.AllowEdit = false;
            this.colADDRESS_2.OptionsColumn.AllowFocus = false;
            this.colADDRESS_2.OptionsColumn.ReadOnly = true;
            this.colADDRESS_2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colADDRESS_2.Visible = true;
            this.colADDRESS_2.VisibleIndex = 6;
            this.colADDRESS_2.Width = 80;
            // 
            // colADDRESS_3
            // 
            this.colADDRESS_3.FieldName = "ADDRESS_3";
            this.colADDRESS_3.Name = "colADDRESS_3";
            this.colADDRESS_3.OptionsColumn.AllowEdit = false;
            this.colADDRESS_3.OptionsColumn.AllowFocus = false;
            this.colADDRESS_3.OptionsColumn.ReadOnly = true;
            this.colADDRESS_3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colADDRESS_3.Visible = true;
            this.colADDRESS_3.VisibleIndex = 7;
            this.colADDRESS_3.Width = 80;
            // 
            // colADDRESS_4
            // 
            this.colADDRESS_4.FieldName = "ADDRESS_4";
            this.colADDRESS_4.Name = "colADDRESS_4";
            this.colADDRESS_4.OptionsColumn.AllowEdit = false;
            this.colADDRESS_4.OptionsColumn.AllowFocus = false;
            this.colADDRESS_4.OptionsColumn.ReadOnly = true;
            this.colADDRESS_4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colADDRESS_4.Visible = true;
            this.colADDRESS_4.VisibleIndex = 8;
            this.colADDRESS_4.Width = 80;
            // 
            // colADDRESS_5
            // 
            this.colADDRESS_5.FieldName = "ADDRESS_5";
            this.colADDRESS_5.Name = "colADDRESS_5";
            this.colADDRESS_5.OptionsColumn.AllowEdit = false;
            this.colADDRESS_5.OptionsColumn.AllowFocus = false;
            this.colADDRESS_5.OptionsColumn.ReadOnly = true;
            this.colADDRESS_5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colADDRESS_5.Visible = true;
            this.colADDRESS_5.VisibleIndex = 9;
            this.colADDRESS_5.Width = 80;
            // 
            // colZIPCODE
            // 
            this.colZIPCODE.FieldName = "ZIPCODE";
            this.colZIPCODE.Name = "colZIPCODE";
            this.colZIPCODE.OptionsColumn.AllowEdit = false;
            this.colZIPCODE.OptionsColumn.AllowFocus = false;
            this.colZIPCODE.OptionsColumn.ReadOnly = true;
            this.colZIPCODE.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colZIPCODE.Visible = true;
            this.colZIPCODE.VisibleIndex = 10;
            // 
            // colPREF_DEP
            // 
            this.colPREF_DEP.FieldName = "PREF_DEP";
            this.colPREF_DEP.Name = "colPREF_DEP";
            this.colPREF_DEP.OptionsColumn.AllowEdit = false;
            this.colPREF_DEP.OptionsColumn.AllowFocus = false;
            this.colPREF_DEP.OptionsColumn.ReadOnly = true;
            this.colPREF_DEP.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPREF_DEP.Visible = true;
            this.colPREF_DEP.VisibleIndex = 11;
            // 
            // colPROD1
            // 
            this.colPROD1.FieldName = "PROD1";
            this.colPROD1.Name = "colPROD1";
            this.colPROD1.OptionsColumn.AllowEdit = false;
            this.colPROD1.OptionsColumn.AllowFocus = false;
            this.colPROD1.OptionsColumn.ReadOnly = true;
            this.colPROD1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD1.Visible = true;
            this.colPROD1.VisibleIndex = 12;
            // 
            // colPROD2
            // 
            this.colPROD2.FieldName = "PROD2";
            this.colPROD2.Name = "colPROD2";
            this.colPROD2.OptionsColumn.AllowEdit = false;
            this.colPROD2.OptionsColumn.AllowFocus = false;
            this.colPROD2.OptionsColumn.ReadOnly = true;
            this.colPROD2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD2.Visible = true;
            this.colPROD2.VisibleIndex = 13;
            // 
            // colPROD3
            // 
            this.colPROD3.FieldName = "PROD3";
            this.colPROD3.Name = "colPROD3";
            this.colPROD3.OptionsColumn.AllowEdit = false;
            this.colPROD3.OptionsColumn.AllowFocus = false;
            this.colPROD3.OptionsColumn.ReadOnly = true;
            this.colPROD3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD3.Visible = true;
            this.colPROD3.VisibleIndex = 14;
            // 
            // colPROD4
            // 
            this.colPROD4.FieldName = "PROD4";
            this.colPROD4.Name = "colPROD4";
            this.colPROD4.OptionsColumn.AllowEdit = false;
            this.colPROD4.OptionsColumn.AllowFocus = false;
            this.colPROD4.OptionsColumn.ReadOnly = true;
            this.colPROD4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD4.Visible = true;
            this.colPROD4.VisibleIndex = 15;
            // 
            // colPROD5
            // 
            this.colPROD5.FieldName = "PROD5";
            this.colPROD5.Name = "colPROD5";
            this.colPROD5.OptionsColumn.AllowEdit = false;
            this.colPROD5.OptionsColumn.AllowFocus = false;
            this.colPROD5.OptionsColumn.ReadOnly = true;
            this.colPROD5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD5.Visible = true;
            this.colPROD5.VisibleIndex = 16;
            // 
            // colPROD6
            // 
            this.colPROD6.FieldName = "PROD6";
            this.colPROD6.Name = "colPROD6";
            this.colPROD6.OptionsColumn.AllowEdit = false;
            this.colPROD6.OptionsColumn.AllowFocus = false;
            this.colPROD6.OptionsColumn.ReadOnly = true;
            this.colPROD6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD6.Visible = true;
            this.colPROD6.VisibleIndex = 17;
            // 
            // colPROD7
            // 
            this.colPROD7.FieldName = "PROD7";
            this.colPROD7.Name = "colPROD7";
            this.colPROD7.OptionsColumn.AllowEdit = false;
            this.colPROD7.OptionsColumn.AllowFocus = false;
            this.colPROD7.OptionsColumn.ReadOnly = true;
            this.colPROD7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD7.Visible = true;
            this.colPROD7.VisibleIndex = 18;
            // 
            // colPROD8
            // 
            this.colPROD8.FieldName = "PROD8";
            this.colPROD8.Name = "colPROD8";
            this.colPROD8.OptionsColumn.AllowEdit = false;
            this.colPROD8.OptionsColumn.AllowFocus = false;
            this.colPROD8.OptionsColumn.ReadOnly = true;
            this.colPROD8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD8.Visible = true;
            this.colPROD8.VisibleIndex = 19;
            // 
            // colPROD9
            // 
            this.colPROD9.FieldName = "PROD9";
            this.colPROD9.Name = "colPROD9";
            this.colPROD9.OptionsColumn.AllowEdit = false;
            this.colPROD9.OptionsColumn.AllowFocus = false;
            this.colPROD9.OptionsColumn.ReadOnly = true;
            this.colPROD9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD9.Visible = true;
            this.colPROD9.VisibleIndex = 20;
            // 
            // colPROD10
            // 
            this.colPROD10.FieldName = "PROD10";
            this.colPROD10.Name = "colPROD10";
            this.colPROD10.OptionsColumn.AllowEdit = false;
            this.colPROD10.OptionsColumn.AllowFocus = false;
            this.colPROD10.OptionsColumn.ReadOnly = true;
            this.colPROD10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPROD10.Visible = true;
            this.colPROD10.VisibleIndex = 21;
            // 
            // colWORKTIME
            // 
            this.colWORKTIME.FieldName = "WORKTIME";
            this.colWORKTIME.Name = "colWORKTIME";
            this.colWORKTIME.OptionsColumn.AllowEdit = false;
            this.colWORKTIME.OptionsColumn.AllowFocus = false;
            this.colWORKTIME.OptionsColumn.ReadOnly = true;
            this.colWORKTIME.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWORKTIME.Visible = true;
            this.colWORKTIME.VisibleIndex = 22;
            this.colWORKTIME.Width = 77;
            // 
            // colBOOKDAY
            // 
            this.colBOOKDAY.FieldName = "BOOKDAY";
            this.colBOOKDAY.Name = "colBOOKDAY";
            this.colBOOKDAY.OptionsColumn.AllowEdit = false;
            this.colBOOKDAY.OptionsColumn.AllowFocus = false;
            this.colBOOKDAY.OptionsColumn.ReadOnly = true;
            this.colBOOKDAY.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBOOKDAY.Visible = true;
            this.colBOOKDAY.VisibleIndex = 23;
            // 
            // colBOOKTIME
            // 
            this.colBOOKTIME.FieldName = "BOOKTIME";
            this.colBOOKTIME.Name = "colBOOKTIME";
            this.colBOOKTIME.OptionsColumn.AllowEdit = false;
            this.colBOOKTIME.OptionsColumn.AllowFocus = false;
            this.colBOOKTIME.OptionsColumn.ReadOnly = true;
            this.colBOOKTIME.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBOOKTIME.Visible = true;
            this.colBOOKTIME.VisibleIndex = 24;
            // 
            // colOPEN1
            // 
            this.colOPEN1.FieldName = "OPEN1";
            this.colOPEN1.Name = "colOPEN1";
            this.colOPEN1.OptionsColumn.AllowEdit = false;
            this.colOPEN1.OptionsColumn.AllowFocus = false;
            this.colOPEN1.OptionsColumn.ReadOnly = true;
            this.colOPEN1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOPEN1.Visible = true;
            this.colOPEN1.VisibleIndex = 25;
            // 
            // colCLOSE1
            // 
            this.colCLOSE1.FieldName = "CLOSE1";
            this.colCLOSE1.Name = "colCLOSE1";
            this.colCLOSE1.OptionsColumn.AllowEdit = false;
            this.colCLOSE1.OptionsColumn.AllowFocus = false;
            this.colCLOSE1.OptionsColumn.ReadOnly = true;
            this.colCLOSE1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCLOSE1.Visible = true;
            this.colCLOSE1.VisibleIndex = 26;
            // 
            // colOPEN2
            // 
            this.colOPEN2.FieldName = "OPEN2";
            this.colOPEN2.Name = "colOPEN2";
            this.colOPEN2.OptionsColumn.AllowEdit = false;
            this.colOPEN2.OptionsColumn.AllowFocus = false;
            this.colOPEN2.OptionsColumn.ReadOnly = true;
            this.colOPEN2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOPEN2.Visible = true;
            this.colOPEN2.VisibleIndex = 27;
            // 
            // colCLOSE2
            // 
            this.colCLOSE2.FieldName = "CLOSE2";
            this.colCLOSE2.Name = "colCLOSE2";
            this.colCLOSE2.OptionsColumn.AllowEdit = false;
            this.colCLOSE2.OptionsColumn.AllowFocus = false;
            this.colCLOSE2.OptionsColumn.ReadOnly = true;
            this.colCLOSE2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCLOSE2.Visible = true;
            this.colCLOSE2.VisibleIndex = 28;
            // 
            // colCUST_INFO1
            // 
            this.colCUST_INFO1.FieldName = "CUST_INFO1";
            this.colCUST_INFO1.Name = "colCUST_INFO1";
            this.colCUST_INFO1.OptionsColumn.AllowEdit = false;
            this.colCUST_INFO1.OptionsColumn.AllowFocus = false;
            this.colCUST_INFO1.OptionsColumn.ReadOnly = true;
            this.colCUST_INFO1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCUST_INFO1.Visible = true;
            this.colCUST_INFO1.VisibleIndex = 29;
            this.colCUST_INFO1.Width = 85;
            // 
            // colCUST_INFO2
            // 
            this.colCUST_INFO2.FieldName = "CUST_INFO2";
            this.colCUST_INFO2.Name = "colCUST_INFO2";
            this.colCUST_INFO2.OptionsColumn.AllowEdit = false;
            this.colCUST_INFO2.OptionsColumn.AllowFocus = false;
            this.colCUST_INFO2.OptionsColumn.ReadOnly = true;
            this.colCUST_INFO2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCUST_INFO2.Visible = true;
            this.colCUST_INFO2.VisibleIndex = 30;
            this.colCUST_INFO2.Width = 85;
            // 
            // colCUST_INFO3
            // 
            this.colCUST_INFO3.FieldName = "CUST_INFO3";
            this.colCUST_INFO3.Name = "colCUST_INFO3";
            this.colCUST_INFO3.OptionsColumn.AllowEdit = false;
            this.colCUST_INFO3.OptionsColumn.AllowFocus = false;
            this.colCUST_INFO3.OptionsColumn.ReadOnly = true;
            this.colCUST_INFO3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCUST_INFO3.Visible = true;
            this.colCUST_INFO3.VisibleIndex = 31;
            this.colCUST_INFO3.Width = 85;
            // 
            // colCUST_INFO4
            // 
            this.colCUST_INFO4.FieldName = "CUST_INFO4";
            this.colCUST_INFO4.Name = "colCUST_INFO4";
            this.colCUST_INFO4.OptionsColumn.AllowEdit = false;
            this.colCUST_INFO4.OptionsColumn.AllowFocus = false;
            this.colCUST_INFO4.OptionsColumn.ReadOnly = true;
            this.colCUST_INFO4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCUST_INFO4.Visible = true;
            this.colCUST_INFO4.VisibleIndex = 32;
            this.colCUST_INFO4.Width = 85;
            // 
            // colCUST_INFO5
            // 
            this.colCUST_INFO5.FieldName = "CUST_INFO5";
            this.colCUST_INFO5.Name = "colCUST_INFO5";
            this.colCUST_INFO5.OptionsColumn.AllowEdit = false;
            this.colCUST_INFO5.OptionsColumn.AllowFocus = false;
            this.colCUST_INFO5.OptionsColumn.ReadOnly = true;
            this.colCUST_INFO5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCUST_INFO5.Visible = true;
            this.colCUST_INFO5.VisibleIndex = 33;
            this.colCUST_INFO5.Width = 85;
            // 
            // colTYPE
            // 
            this.colTYPE.Caption = "TYPE";
            this.colTYPE.FieldName = "TYPE";
            this.colTYPE.Name = "colTYPE";
            this.colTYPE.OptionsColumn.AllowEdit = false;
            this.colTYPE.OptionsColumn.AllowFocus = false;
            this.colTYPE.OptionsColumn.ReadOnly = true;
            this.colTYPE.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTYPE.Visible = true;
            this.colTYPE.VisibleIndex = 34;
            this.colTYPE.Width = 46;
            // 
            // colDepGroup
            // 
            this.colDepGroup.Caption = "DepGroup";
            this.colDepGroup.FieldName = "DepGroup";
            this.colDepGroup.Name = "colDepGroup";
            this.colDepGroup.OptionsColumn.AllowEdit = false;
            this.colDepGroup.OptionsColumn.AllowFocus = false;
            this.colDepGroup.OptionsColumn.ReadOnly = true;
            this.colDepGroup.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepGroup.Visible = true;
            this.colDepGroup.VisibleIndex = 35;
            this.colDepGroup.Width = 70;
            // 
            // colCAP_1
            // 
            this.colCAP_1.Caption = "CAP_1";
            this.colCAP_1.FieldName = "CAP_1";
            this.colCAP_1.Name = "colCAP_1";
            this.colCAP_1.OptionsColumn.AllowEdit = false;
            this.colCAP_1.OptionsColumn.AllowFocus = false;
            this.colCAP_1.OptionsColumn.ReadOnly = true;
            this.colCAP_1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCAP_1.Visible = true;
            this.colCAP_1.VisibleIndex = 36;
            this.colCAP_1.Width = 54;
            // 
            // colCAP_2
            // 
            this.colCAP_2.Caption = "CAP_2";
            this.colCAP_2.FieldName = "CAP_2";
            this.colCAP_2.Name = "colCAP_2";
            this.colCAP_2.OptionsColumn.AllowEdit = false;
            this.colCAP_2.OptionsColumn.AllowFocus = false;
            this.colCAP_2.OptionsColumn.ReadOnly = true;
            this.colCAP_2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCAP_2.Visible = true;
            this.colCAP_2.VisibleIndex = 37;
            this.colCAP_2.Width = 54;
            // 
            // colDepFleet
            // 
            this.colDepFleet.Caption = "DepFleet";
            this.colDepFleet.FieldName = "DepFleet";
            this.colDepFleet.Name = "colDepFleet";
            this.colDepFleet.OptionsColumn.AllowEdit = false;
            this.colDepFleet.OptionsColumn.AllowFocus = false;
            this.colDepFleet.OptionsColumn.ReadOnly = true;
            this.colDepFleet.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepFleet.Visible = true;
            this.colDepFleet.VisibleIndex = 38;
            // 
            // colDummyOrder
            // 
            this.colDummyOrder.Caption = "Dummy Order";
            this.colDummyOrder.FieldName = "DummyOrder";
            this.colDummyOrder.Name = "colDummyOrder";
            this.colDummyOrder.OptionsColumn.AllowEdit = false;
            this.colDummyOrder.OptionsColumn.AllowFocus = false;
            this.colDummyOrder.OptionsColumn.ReadOnly = true;
            this.colDummyOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDummyOrder.Width = 88;
            // 
            // sp04355_GC_Export_Gritting_Callouts_For_Route_OptimisationTableAdapter
            // 
            this.sp04355_GC_Export_Gritting_Callouts_For_Route_OptimisationTableAdapter.ClearBeforeFill = true;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExport.Location = new System.Drawing.Point(6, 389);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(165, 23);
            this.btnExport.TabIndex = 5;
            this.btnExport.Text = "Send Data to Route Optimizer";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(662, 389);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // colVehGroup
            // 
            this.colVehGroup.Caption = "VehGroup";
            this.colVehGroup.FieldName = "VehGroup";
            this.colVehGroup.Name = "colVehGroup";
            this.colVehGroup.OptionsColumn.AllowEdit = false;
            this.colVehGroup.OptionsColumn.AllowFocus = false;
            this.colVehGroup.OptionsColumn.ReadOnly = true;
            this.colVehGroup.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVehGroup.Visible = true;
            this.colVehGroup.VisibleIndex = 39;
            this.colVehGroup.Width = 67;
            // 
            // colVehicle
            // 
            this.colVehicle.Caption = "Vehicle";
            this.colVehicle.FieldName = "Vehicle";
            this.colVehicle.Name = "colVehicle";
            this.colVehicle.OptionsColumn.AllowEdit = false;
            this.colVehicle.OptionsColumn.AllowFocus = false;
            this.colVehicle.OptionsColumn.ReadOnly = true;
            this.colVehicle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVehicle.Visible = true;
            this.colVehicle.VisibleIndex = 40;
            this.colVehicle.Width = 53;
            // 
            // frm_GC_Gritting_Callouts_Send_To_Optimizer
            // 
            this.ClientSize = new System.Drawing.Size(744, 418);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.gridControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_GC_Gritting_Callouts_Send_To_Optimizer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Send Gritting Callouts to Route Optimizer";
            this.Load += new System.EventHandler(this.frm_GC_Gritting_Callouts_Send_To_Optimizer_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            this.Controls.SetChildIndex(this.btnExport, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04355GCExportGrittingCalloutsForRouteOptimisationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp04355GCExportGrittingCalloutsForRouteOptimisationBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colACTION;
        private DevExpress.XtraGrid.Columns.GridColumn colORDERTYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colORDERREF;
        private DevExpress.XtraGrid.Columns.GridColumn colNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colADDRESS_1;
        private DevExpress.XtraGrid.Columns.GridColumn colADDRESS_2;
        private DevExpress.XtraGrid.Columns.GridColumn colADDRESS_3;
        private DevExpress.XtraGrid.Columns.GridColumn colADDRESS_4;
        private DevExpress.XtraGrid.Columns.GridColumn colADDRESS_5;
        private DevExpress.XtraGrid.Columns.GridColumn colZIPCODE;
        private DevExpress.XtraGrid.Columns.GridColumn colPREF_DEP;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD1;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD2;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD3;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD4;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD5;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD6;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD7;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD8;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD9;
        private DevExpress.XtraGrid.Columns.GridColumn colPROD10;
        private DevExpress.XtraGrid.Columns.GridColumn colWORKTIME;
        private DevExpress.XtraGrid.Columns.GridColumn colBOOKDAY;
        private DevExpress.XtraGrid.Columns.GridColumn colBOOKTIME;
        private DevExpress.XtraGrid.Columns.GridColumn colOPEN1;
        private DevExpress.XtraGrid.Columns.GridColumn colCLOSE1;
        private DevExpress.XtraGrid.Columns.GridColumn colOPEN2;
        private DevExpress.XtraGrid.Columns.GridColumn colCLOSE2;
        private DevExpress.XtraGrid.Columns.GridColumn colCUST_INFO1;
        private DevExpress.XtraGrid.Columns.GridColumn colCUST_INFO2;
        private DevExpress.XtraGrid.Columns.GridColumn colCUST_INFO3;
        private DevExpress.XtraGrid.Columns.GridColumn colCUST_INFO4;
        private DevExpress.XtraGrid.Columns.GridColumn colCUST_INFO5;
        private DataSet_GC_CoreTableAdapters.sp04355_GC_Export_Gritting_Callouts_For_Route_OptimisationTableAdapter sp04355_GC_Export_Gritting_Callouts_For_Route_OptimisationTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnExport;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraGrid.Columns.GridColumn colTYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colDepGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colCAP_1;
        private DevExpress.XtraGrid.Columns.GridColumn colCAP_2;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDepFleet;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colDEPNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colVehGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicle;
    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WoodPlan5
{
    public partial class frm_GC_Callout_SiteGrittingContract_Changed_Next : DevExpress.XtraEditors.XtraForm
    {
        
        #region Instance Variables...
        
        public string strReturnedValue = "Close";
 
        #endregion

        public frm_GC_Callout_SiteGrittingContract_Changed_Next()
        {
            InitializeComponent();
        }

        private void GC_Callout_Block_Add_Next_Load(object sender, EventArgs e)
        {
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            strReturnedValue = "Nothing";
            this.Close();
        }
 
        private void btnClear_Click(object sender, EventArgs e)
        {
            strReturnedValue = "Clear";
            this.Close();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            strReturnedValue = "Open";
            this.Close();
        }
    }
}
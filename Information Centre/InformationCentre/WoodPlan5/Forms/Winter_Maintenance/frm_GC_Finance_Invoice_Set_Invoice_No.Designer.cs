﻿namespace WoodPlan5
{
    partial class frm_GC_Finance_Invoice_Set_Invoice_No
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InvoiceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.OnHoldReasonIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04372WMSentToClientOnHoldReasonsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AuditedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SentToAccountsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SentToClientMethodIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04371WMSentToClientMethodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientInvoiceLineTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DateTimeSentDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForFinanceInvoiceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInvoiceLine = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSentToAccounts = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateTimeSent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAudited = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSentToClientMethodID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOnHoldReasonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            this.sp04371_WM_Sent_To_Client_MethodsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04371_WM_Sent_To_Client_MethodsTableAdapter();
            this.sp04372_WM_Sent_To_Client_On_Hold_ReasonsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04372_WM_Sent_To_Client_On_Hold_ReasonsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OnHoldReasonIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04372WMSentToClientOnHoldReasonsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AuditedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentToAccountsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentToClientMethodIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04371WMSentToClientMethodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceLineTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeSentDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeSentDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceInvoiceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentToAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTimeSent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAudited)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentToClientMethodID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnHoldReasonID)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(602, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 360);
            this.barDockControlBottom.Size = new System.Drawing.Size(602, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 334);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(602, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 334);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // InvoiceNumberTextEdit
            // 
            this.InvoiceNumberTextEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.InvoiceNumberTextEdit.Location = new System.Drawing.Point(125, 36);
            this.InvoiceNumberTextEdit.MenuManager = this.barManager1;
            this.InvoiceNumberTextEdit.Name = "InvoiceNumberTextEdit";
            this.InvoiceNumberTextEdit.Properties.MaxLength = 50;
            this.InvoiceNumberTextEdit.Properties.NullValuePrompt = "No Invoice Number";
            this.InvoiceNumberTextEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.InvoiceNumberTextEdit, true);
            this.InvoiceNumberTextEdit.Size = new System.Drawing.Size(464, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.InvoiceNumberTextEdit, optionsSpelling1);
            this.InvoiceNumberTextEdit.StyleController = this.layoutControl1;
            this.InvoiceNumberTextEdit.TabIndex = 4;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.OnHoldReasonIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.AuditedCheckEdit);
            this.layoutControl1.Controls.Add(this.SentToAccountsCheckEdit);
            this.layoutControl1.Controls.Add(this.SentToClientMethodIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.ClientInvoiceLineTextEdit);
            this.layoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.layoutControl1.Controls.Add(this.InvoiceNumberTextEdit);
            this.layoutControl1.Controls.Add(this.DateTimeSentDateEdit);
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1424, 170, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(601, 297);
            this.layoutControl1.TabIndex = 10;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // OnHoldReasonIDGridLookUpEdit
            // 
            this.OnHoldReasonIDGridLookUpEdit.Location = new System.Drawing.Point(125, 154);
            this.OnHoldReasonIDGridLookUpEdit.MenuManager = this.barManager1;
            this.OnHoldReasonIDGridLookUpEdit.Name = "OnHoldReasonIDGridLookUpEdit";
            this.OnHoldReasonIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OnHoldReasonIDGridLookUpEdit.Properties.DataSource = this.sp04372WMSentToClientOnHoldReasonsBindingSource;
            this.OnHoldReasonIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.OnHoldReasonIDGridLookUpEdit.Properties.NullText = "";
            this.OnHoldReasonIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.OnHoldReasonIDGridLookUpEdit.Properties.View = this.gridView1;
            this.OnHoldReasonIDGridLookUpEdit.Size = new System.Drawing.Size(464, 20);
            this.OnHoldReasonIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.OnHoldReasonIDGridLookUpEdit.TabIndex = 12;
            // 
            // sp04372WMSentToClientOnHoldReasonsBindingSource
            // 
            this.sp04372WMSentToClientOnHoldReasonsBindingSource.DataMember = "sp04372_WM_Sent_To_Client_On_Hold_Reasons";
            this.sp04372WMSentToClientOnHoldReasonsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "On-Hold Reason";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // AuditedCheckEdit
            // 
            this.AuditedCheckEdit.Location = new System.Drawing.Point(125, 131);
            this.AuditedCheckEdit.MenuManager = this.barManager1;
            this.AuditedCheckEdit.Name = "AuditedCheckEdit";
            this.AuditedCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.AuditedCheckEdit.Properties.ValueChecked = 1;
            this.AuditedCheckEdit.Properties.ValueUnchecked = 0;
            this.AuditedCheckEdit.Size = new System.Drawing.Size(127, 19);
            this.AuditedCheckEdit.StyleController = this.layoutControl1;
            this.AuditedCheckEdit.TabIndex = 19;
            // 
            // SentToAccountsCheckEdit
            // 
            this.SentToAccountsCheckEdit.Location = new System.Drawing.Point(125, 60);
            this.SentToAccountsCheckEdit.MenuManager = this.barManager1;
            this.SentToAccountsCheckEdit.Name = "SentToAccountsCheckEdit";
            this.SentToAccountsCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.SentToAccountsCheckEdit.Properties.ValueChecked = 1;
            this.SentToAccountsCheckEdit.Properties.ValueUnchecked = 0;
            this.SentToAccountsCheckEdit.Size = new System.Drawing.Size(127, 19);
            this.SentToAccountsCheckEdit.StyleController = this.layoutControl1;
            this.SentToAccountsCheckEdit.TabIndex = 18;
            // 
            // SentToClientMethodIDGridLookUpEdit
            // 
            this.SentToClientMethodIDGridLookUpEdit.Location = new System.Drawing.Point(125, 83);
            this.SentToClientMethodIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SentToClientMethodIDGridLookUpEdit.Name = "SentToClientMethodIDGridLookUpEdit";
            this.SentToClientMethodIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SentToClientMethodIDGridLookUpEdit.Properties.DataSource = this.sp04371WMSentToClientMethodsBindingSource;
            this.SentToClientMethodIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SentToClientMethodIDGridLookUpEdit.Properties.NullText = "";
            this.SentToClientMethodIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SentToClientMethodIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.SentToClientMethodIDGridLookUpEdit.Size = new System.Drawing.Size(464, 20);
            this.SentToClientMethodIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.SentToClientMethodIDGridLookUpEdit.TabIndex = 11;
            // 
            // sp04371WMSentToClientMethodsBindingSource
            // 
            this.sp04371WMSentToClientMethodsBindingSource.DataMember = "sp04371_WM_Sent_To_Client_Methods";
            this.sp04371WMSentToClientMethodsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colID1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Sent To Client Method";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ClientInvoiceLineTextEdit
            // 
            this.ClientInvoiceLineTextEdit.Location = new System.Drawing.Point(125, 12);
            this.ClientInvoiceLineTextEdit.MenuManager = this.barManager1;
            this.ClientInvoiceLineTextEdit.Name = "ClientInvoiceLineTextEdit";
            this.ClientInvoiceLineTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientInvoiceLineTextEdit, true);
            this.ClientInvoiceLineTextEdit.Size = new System.Drawing.Size(464, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientInvoiceLineTextEdit, optionsSpelling2);
            this.ClientInvoiceLineTextEdit.StyleController = this.layoutControl1;
            this.ClientInvoiceLineTextEdit.TabIndex = 6;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.Location = new System.Drawing.Point(125, 178);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(464, 107);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling3);
            this.RemarksMemoEdit.StyleController = this.layoutControl1;
            this.RemarksMemoEdit.TabIndex = 5;
            // 
            // DateTimeSentDateEdit
            // 
            this.DateTimeSentDateEdit.EditValue = null;
            this.DateTimeSentDateEdit.Location = new System.Drawing.Point(125, 107);
            this.DateTimeSentDateEdit.MenuManager = this.barManager1;
            this.DateTimeSentDateEdit.Name = "DateTimeSentDateEdit";
            this.DateTimeSentDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateTimeSentDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateTimeSentDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateTimeSentDateEdit.Properties.Mask.EditMask = "g";
            this.DateTimeSentDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateTimeSentDateEdit.Size = new System.Drawing.Size(127, 20);
            this.DateTimeSentDateEdit.StyleController = this.layoutControl1;
            this.DateTimeSentDateEdit.TabIndex = 17;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForFinanceInvoiceNumber,
            this.ItemForRemarks,
            this.ItemForInvoiceLine,
            this.ItemForSentToAccounts,
            this.ItemForDateTimeSent,
            this.ItemForAudited,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.ItemForSentToClientMethodID,
            this.ItemForOnHoldReasonID});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(601, 297);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForFinanceInvoiceNumber
            // 
            this.ItemForFinanceInvoiceNumber.AllowHide = false;
            this.ItemForFinanceInvoiceNumber.Control = this.InvoiceNumberTextEdit;
            this.ItemForFinanceInvoiceNumber.CustomizationFormText = "Invoice Number:";
            this.ItemForFinanceInvoiceNumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForFinanceInvoiceNumber.Name = "ItemForFinanceInvoiceNumber";
            this.ItemForFinanceInvoiceNumber.Size = new System.Drawing.Size(581, 24);
            this.ItemForFinanceInvoiceNumber.Text = "Invoice Number:";
            this.ItemForFinanceInvoiceNumber.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForRemarks.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 166);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(581, 111);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForInvoiceLine
            // 
            this.ItemForInvoiceLine.Control = this.ClientInvoiceLineTextEdit;
            this.ItemForInvoiceLine.CustomizationFormText = "Invoice Line:";
            this.ItemForInvoiceLine.Location = new System.Drawing.Point(0, 0);
            this.ItemForInvoiceLine.Name = "ItemForInvoiceLine";
            this.ItemForInvoiceLine.Size = new System.Drawing.Size(581, 24);
            this.ItemForInvoiceLine.Text = "Invoice Line:";
            this.ItemForInvoiceLine.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForSentToAccounts
            // 
            this.ItemForSentToAccounts.Control = this.SentToAccountsCheckEdit;
            this.ItemForSentToAccounts.Location = new System.Drawing.Point(0, 48);
            this.ItemForSentToAccounts.MaxSize = new System.Drawing.Size(244, 23);
            this.ItemForSentToAccounts.MinSize = new System.Drawing.Size(244, 23);
            this.ItemForSentToAccounts.Name = "ItemForSentToAccounts";
            this.ItemForSentToAccounts.Size = new System.Drawing.Size(244, 23);
            this.ItemForSentToAccounts.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSentToAccounts.Text = "Sent To Accounts:";
            this.ItemForSentToAccounts.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForDateTimeSent
            // 
            this.ItemForDateTimeSent.Control = this.DateTimeSentDateEdit;
            this.ItemForDateTimeSent.Location = new System.Drawing.Point(0, 95);
            this.ItemForDateTimeSent.MaxSize = new System.Drawing.Size(244, 24);
            this.ItemForDateTimeSent.MinSize = new System.Drawing.Size(244, 24);
            this.ItemForDateTimeSent.Name = "ItemForDateTimeSent";
            this.ItemForDateTimeSent.Size = new System.Drawing.Size(244, 24);
            this.ItemForDateTimeSent.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDateTimeSent.Text = "Date Sent:";
            this.ItemForDateTimeSent.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForAudited
            // 
            this.ItemForAudited.Control = this.AuditedCheckEdit;
            this.ItemForAudited.Location = new System.Drawing.Point(0, 119);
            this.ItemForAudited.MaxSize = new System.Drawing.Size(244, 23);
            this.ItemForAudited.MinSize = new System.Drawing.Size(244, 23);
            this.ItemForAudited.Name = "ItemForAudited";
            this.ItemForAudited.Size = new System.Drawing.Size(244, 23);
            this.ItemForAudited.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAudited.Text = "Audited:";
            this.ItemForAudited.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(244, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(337, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(244, 119);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(337, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(244, 95);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(337, 24);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSentToClientMethodID
            // 
            this.ItemForSentToClientMethodID.Control = this.SentToClientMethodIDGridLookUpEdit;
            this.ItemForSentToClientMethodID.Location = new System.Drawing.Point(0, 71);
            this.ItemForSentToClientMethodID.Name = "ItemForSentToClientMethodID";
            this.ItemForSentToClientMethodID.Size = new System.Drawing.Size(581, 24);
            this.ItemForSentToClientMethodID.Text = "Sent To Client Method:";
            this.ItemForSentToClientMethodID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForOnHoldReasonID
            // 
            this.ItemForOnHoldReasonID.Control = this.OnHoldReasonIDGridLookUpEdit;
            this.ItemForOnHoldReasonID.Location = new System.Drawing.Point(0, 142);
            this.ItemForOnHoldReasonID.Name = "ItemForOnHoldReasonID";
            this.ItemForOnHoldReasonID.Size = new System.Drawing.Size(581, 24);
            this.ItemForOnHoldReasonID.Text = "On-Hold Reason:";
            this.ItemForOnHoldReasonID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(426, 326);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(515, 326);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Complete the fields displayed. Any fields left blank will not be overwritten.";
            this.bsiInformation.Glyph = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiInformation.Id = 30;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // sp04371_WM_Sent_To_Client_MethodsTableAdapter
            // 
            this.sp04371_WM_Sent_To_Client_MethodsTableAdapter.ClearBeforeFill = true;
            // 
            // sp04372_WM_Sent_To_Client_On_Hold_ReasonsTableAdapter
            // 
            this.sp04372_WM_Sent_To_Client_On_Hold_ReasonsTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Finance_Invoice_Set_Invoice_No
            // 
            this.ClientSize = new System.Drawing.Size(602, 390);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Finance_Invoice_Set_Invoice_No";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Client Invoice Lines - Edit \\ Block Edit Details";
            this.Load += new System.EventHandler(this.frm_GC_Finance_Invoice_Set_Invoice_No_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OnHoldReasonIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04372WMSentToClientOnHoldReasonsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AuditedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentToAccountsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentToClientMethodIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04371WMSentToClientMethodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceLineTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeSentDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeSentDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceInvoiceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentToAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTimeSent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAudited)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentToClientMethodID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnHoldReasonID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit InvoiceNumberTextEdit;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinanceInvoiceNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit ClientInvoiceLineTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvoiceLine;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
        private DevExpress.XtraEditors.GridLookUpEdit SentToClientMethodIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSentToClientMethodID;
        private DevExpress.XtraEditors.CheckEdit AuditedCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SentToAccountsCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSentToAccounts;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAudited;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateTimeSent;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.GridLookUpEdit OnHoldReasonIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOnHoldReasonID;
        private System.Windows.Forms.BindingSource sp04372WMSentToClientOnHoldReasonsBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private System.Windows.Forms.BindingSource sp04371WMSentToClientMethodsBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04371_WM_Sent_To_Client_MethodsTableAdapter sp04371_WM_Sent_To_Client_MethodsTableAdapter;
        private DataSet_GC_CoreTableAdapters.sp04372_WM_Sent_To_Client_On_Hold_ReasonsTableAdapter sp04372_WM_Sent_To_Client_On_Hold_ReasonsTableAdapter;
        private DevExpress.XtraEditors.DateEdit DateTimeSentDateEdit;
    }
}

namespace WoodPlan5
{
    partial class frm_GC_Snow_Team_Rate_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Snow_Team_Rate_Edit));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.MinimumHours4SpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sp04157GCSnowClearanceTeamRatesEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Snow_DataEntry = new WoodPlan5.DataSet_GC_Snow_DataEntry();
            this.MinimumHours3SpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MinimumHours2SpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MachineRate4SpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MachineRate3SpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MachineRate2SpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MinimumHoursSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.HoursSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EndTimeTimeEdit = new DevExpress.XtraEditors.TimeEdit();
            this.DayTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04159GCWorkDayTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SubContractorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04026ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalCOntractorDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.SnowClearanceSubContractorRateRuleSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.StartTimeTimeEdit = new DevExpress.XtraEditors.TimeEdit();
            this.ItemForSnowClearanceSubContractorRateRule = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForStartTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDayTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMachineRate2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMachineRate3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMachineRate4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMinimumHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMinimumHours2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMinimumHours3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMinimumHours4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEndTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.sp04026_Contractor_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04026_Contractor_List_With_BlankTableAdapter();
            this.sp04157_GC_Snow_Clearance_Team_Rates_EditTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04157_GC_Snow_Clearance_Team_Rates_EditTableAdapter();
            this.sp04159_GC_Work_Day_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04159_GC_Work_Day_Types_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumHours4SpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04157GCSnowClearanceTeamRatesEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumHours3SpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumHours2SpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MachineRate4SpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MachineRate3SpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MachineRate2SpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumHoursSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndTimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DayTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04159GCWorkDayTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04026ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceSubContractorRateRuleSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceSubContractorRateRule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDayTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMachineRate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMachineRate3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMachineRate4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumHours2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumHours3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumHours4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 502);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            this.colValue.Width = 70;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 502);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 476);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.MinimumHours4SpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MinimumHours3SpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MinimumHours2SpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MachineRate4SpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MachineRate3SpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MachineRate2SpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MinimumHoursSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.HoursSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EndTimeTimeEdit);
            this.dataLayoutControl1.Controls.Add(this.DayTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SnowClearanceSubContractorRateRuleSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.StartTimeTimeEdit);
            this.dataLayoutControl1.DataSource = this.sp04157GCSnowClearanceTeamRatesEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSnowClearanceSubContractorRateRule});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(830, 345, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 476);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // MinimumHours4SpinEdit
            // 
            this.MinimumHours4SpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "MinimumHours4", true));
            this.MinimumHours4SpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MinimumHours4SpinEdit.Location = new System.Drawing.Point(403, 230);
            this.MinimumHours4SpinEdit.MenuManager = this.barManager1;
            this.MinimumHours4SpinEdit.Name = "MinimumHours4SpinEdit";
            this.MinimumHours4SpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MinimumHours4SpinEdit.Properties.Mask.EditMask = "f2";
            this.MinimumHours4SpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MinimumHours4SpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.MinimumHours4SpinEdit.Size = new System.Drawing.Size(213, 20);
            this.MinimumHours4SpinEdit.StyleController = this.dataLayoutControl1;
            this.MinimumHours4SpinEdit.TabIndex = 27;
            // 
            // sp04157GCSnowClearanceTeamRatesEditBindingSource
            // 
            this.sp04157GCSnowClearanceTeamRatesEditBindingSource.DataMember = "sp04157_GC_Snow_Clearance_Team_Rates_Edit";
            this.sp04157GCSnowClearanceTeamRatesEditBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // dataSet_GC_Snow_DataEntry
            // 
            this.dataSet_GC_Snow_DataEntry.DataSetName = "DataSet_GC_Snow_DataEntry";
            this.dataSet_GC_Snow_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // MinimumHours3SpinEdit
            // 
            this.MinimumHours3SpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "MinimumHours3", true));
            this.MinimumHours3SpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MinimumHours3SpinEdit.Location = new System.Drawing.Point(403, 206);
            this.MinimumHours3SpinEdit.MenuManager = this.barManager1;
            this.MinimumHours3SpinEdit.Name = "MinimumHours3SpinEdit";
            this.MinimumHours3SpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MinimumHours3SpinEdit.Properties.Mask.EditMask = "f2";
            this.MinimumHours3SpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MinimumHours3SpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.MinimumHours3SpinEdit.Size = new System.Drawing.Size(213, 20);
            this.MinimumHours3SpinEdit.StyleController = this.dataLayoutControl1;
            this.MinimumHours3SpinEdit.TabIndex = 26;
            // 
            // MinimumHours2SpinEdit
            // 
            this.MinimumHours2SpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "MinimumHours2", true));
            this.MinimumHours2SpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MinimumHours2SpinEdit.Location = new System.Drawing.Point(403, 182);
            this.MinimumHours2SpinEdit.MenuManager = this.barManager1;
            this.MinimumHours2SpinEdit.Name = "MinimumHours2SpinEdit";
            this.MinimumHours2SpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MinimumHours2SpinEdit.Properties.Mask.EditMask = "f2";
            this.MinimumHours2SpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MinimumHours2SpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.MinimumHours2SpinEdit.Size = new System.Drawing.Size(213, 20);
            this.MinimumHours2SpinEdit.StyleController = this.dataLayoutControl1;
            this.MinimumHours2SpinEdit.TabIndex = 25;
            // 
            // MachineRate4SpinEdit
            // 
            this.MachineRate4SpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "MachineRate4", true));
            this.MachineRate4SpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MachineRate4SpinEdit.Location = new System.Drawing.Point(99, 230);
            this.MachineRate4SpinEdit.MenuManager = this.barManager1;
            this.MachineRate4SpinEdit.Name = "MachineRate4SpinEdit";
            this.MachineRate4SpinEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.MachineRate4SpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MachineRate4SpinEdit.Properties.Mask.EditMask = "c";
            this.MachineRate4SpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MachineRate4SpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.MachineRate4SpinEdit.Size = new System.Drawing.Size(213, 20);
            this.MachineRate4SpinEdit.StyleController = this.dataLayoutControl1;
            this.MachineRate4SpinEdit.TabIndex = 24;
            // 
            // MachineRate3SpinEdit
            // 
            this.MachineRate3SpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "MachineRate3", true));
            this.MachineRate3SpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MachineRate3SpinEdit.Location = new System.Drawing.Point(99, 206);
            this.MachineRate3SpinEdit.MenuManager = this.barManager1;
            this.MachineRate3SpinEdit.Name = "MachineRate3SpinEdit";
            this.MachineRate3SpinEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.MachineRate3SpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MachineRate3SpinEdit.Properties.Mask.EditMask = "c";
            this.MachineRate3SpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MachineRate3SpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.MachineRate3SpinEdit.Size = new System.Drawing.Size(213, 20);
            this.MachineRate3SpinEdit.StyleController = this.dataLayoutControl1;
            this.MachineRate3SpinEdit.TabIndex = 23;
            // 
            // MachineRate2SpinEdit
            // 
            this.MachineRate2SpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "MachineRate2", true));
            this.MachineRate2SpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MachineRate2SpinEdit.Location = new System.Drawing.Point(99, 182);
            this.MachineRate2SpinEdit.MenuManager = this.barManager1;
            this.MachineRate2SpinEdit.Name = "MachineRate2SpinEdit";
            this.MachineRate2SpinEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.MachineRate2SpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MachineRate2SpinEdit.Properties.Mask.EditMask = "c";
            this.MachineRate2SpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MachineRate2SpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.MachineRate2SpinEdit.Size = new System.Drawing.Size(213, 20);
            this.MachineRate2SpinEdit.StyleController = this.dataLayoutControl1;
            this.MachineRate2SpinEdit.TabIndex = 22;
            // 
            // MinimumHoursSpinEdit
            // 
            this.MinimumHoursSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "MinimumHours", true));
            this.MinimumHoursSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MinimumHoursSpinEdit.Location = new System.Drawing.Point(403, 158);
            this.MinimumHoursSpinEdit.MenuManager = this.barManager1;
            this.MinimumHoursSpinEdit.Name = "MinimumHoursSpinEdit";
            this.MinimumHoursSpinEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.MinimumHoursSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MinimumHoursSpinEdit.Properties.Mask.EditMask = "f2";
            this.MinimumHoursSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MinimumHoursSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.MinimumHoursSpinEdit.Size = new System.Drawing.Size(213, 20);
            this.MinimumHoursSpinEdit.StyleController = this.dataLayoutControl1;
            this.MinimumHoursSpinEdit.TabIndex = 21;
            // 
            // RateSpinEdit
            // 
            this.RateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "Rate", true));
            this.RateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RateSpinEdit.Location = new System.Drawing.Point(99, 158);
            this.RateSpinEdit.MenuManager = this.barManager1;
            this.RateSpinEdit.Name = "RateSpinEdit";
            this.RateSpinEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.RateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.RateSpinEdit.Properties.Mask.EditMask = "c";
            this.RateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.RateSpinEdit.Size = new System.Drawing.Size(213, 20);
            this.RateSpinEdit.StyleController = this.dataLayoutControl1;
            this.RateSpinEdit.TabIndex = 20;
            // 
            // HoursSpinEdit
            // 
            this.HoursSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "Hours", true));
            this.HoursSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.HoursSpinEdit.Location = new System.Drawing.Point(99, 134);
            this.HoursSpinEdit.MenuManager = this.barManager1;
            this.HoursSpinEdit.Name = "HoursSpinEdit";
            this.HoursSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.HoursSpinEdit.Properties.Mask.EditMask = "f2";
            this.HoursSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HoursSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.HoursSpinEdit.Properties.ReadOnly = true;
            this.HoursSpinEdit.Size = new System.Drawing.Size(517, 20);
            this.HoursSpinEdit.StyleController = this.dataLayoutControl1;
            this.HoursSpinEdit.TabIndex = 19;
            // 
            // EndTimeTimeEdit
            // 
            this.EndTimeTimeEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "EndTime", true));
            this.EndTimeTimeEdit.EditValue = null;
            this.EndTimeTimeEdit.Location = new System.Drawing.Point(99, 110);
            this.EndTimeTimeEdit.MenuManager = this.barManager1;
            this.EndTimeTimeEdit.Name = "EndTimeTimeEdit";
            this.EndTimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EndTimeTimeEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EndTimeTimeEdit.Size = new System.Drawing.Size(213, 20);
            this.EndTimeTimeEdit.StyleController = this.dataLayoutControl1;
            this.EndTimeTimeEdit.TabIndex = 18;
            this.EndTimeTimeEdit.EditValueChanged += new System.EventHandler(this.EndTimeTimeEdit_EditValueChanged);
            this.EndTimeTimeEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EndTimeTimeEdit_Validating);
            this.EndTimeTimeEdit.Validated += new System.EventHandler(this.EndTimeTimeEdit_Validated);
            // 
            // DayTypeIDGridLookUpEdit
            // 
            this.DayTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "DayTypeID", true));
            this.DayTypeIDGridLookUpEdit.Location = new System.Drawing.Point(99, 62);
            this.DayTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.DayTypeIDGridLookUpEdit.Name = "DayTypeIDGridLookUpEdit";
            this.DayTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DayTypeIDGridLookUpEdit.Properties.DataSource = this.sp04159GCWorkDayTypesWithBlankBindingSource;
            this.DayTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.DayTypeIDGridLookUpEdit.Properties.NullText = "";
            this.DayTypeIDGridLookUpEdit.Properties.ValueMember = "Value";
            this.DayTypeIDGridLookUpEdit.Properties.View = this.gridView1;
            this.DayTypeIDGridLookUpEdit.Size = new System.Drawing.Size(517, 20);
            this.DayTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.DayTypeIDGridLookUpEdit.TabIndex = 17;
            this.DayTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DayTypeIDGridLookUpEdit_Validating);
            // 
            // sp04159GCWorkDayTypesWithBlankBindingSource
            // 
            this.sp04159GCWorkDayTypesWithBlankBindingSource.DataMember = "sp04159_GC_Work_Day_Types_With_Blank";
            this.sp04159GCWorkDayTypesWithBlankBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colValue,
            this.colDescription,
            this.colOrder});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colValue;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 235;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // SubContractorIDGridLookUpEdit
            // 
            this.SubContractorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "SubContractorID", true));
            this.SubContractorIDGridLookUpEdit.Location = new System.Drawing.Point(99, 36);
            this.SubContractorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SubContractorIDGridLookUpEdit.Name = "SubContractorIDGridLookUpEdit";
            this.SubContractorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Edit Underlying Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Reload Underlying Data", "reload", null, true)});
            this.SubContractorIDGridLookUpEdit.Properties.DataSource = this.sp04026ContractorListWithBlankBindingSource;
            this.SubContractorIDGridLookUpEdit.Properties.DisplayMember = "ContractorName";
            this.SubContractorIDGridLookUpEdit.Properties.NullText = "";
            this.SubContractorIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemMemoExEdit1});
            this.SubContractorIDGridLookUpEdit.Properties.ValueMember = "ContractorID";
            this.SubContractorIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.SubContractorIDGridLookUpEdit.Size = new System.Drawing.Size(517, 22);
            this.SubContractorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorIDGridLookUpEdit.TabIndex = 16;
            this.SubContractorIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SubContractorIDGridLookUpEdit_ButtonClick);
            this.SubContractorIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SubContractorIDGridLookUpEdit_Validating);
            // 
            // sp04026ContractorListWithBlankBindingSource
            // 
            this.sp04026ContractorListWithBlankBindingSource.DataMember = "sp04026_Contractor_List_With_Blank";
            this.sp04026ContractorListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colContractorCode,
            this.colContractorID,
            this.colContractorName,
            this.colDisabled,
            this.colEmailPassword,
            this.colInternalContractor,
            this.colInternalCOntractorDescription,
            this.colMobile,
            this.colPostcode,
            this.colRemarks,
            this.colTelephone1,
            this.colTelephone2,
            this.colTypeDescription,
            this.colTypeID,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colVatReg,
            this.colWebsite});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colContractorID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInternalCOntractorDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 273;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor ID";
            this.colInternalContractor.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 118;
            // 
            // colInternalCOntractorDescription
            // 
            this.colInternalCOntractorDescription.Caption = "Status";
            this.colInternalCOntractorDescription.FieldName = "InternalCOntractorDescription";
            this.colInternalCOntractorDescription.Name = "colInternalCOntractorDescription";
            this.colInternalCOntractorDescription.OptionsColumn.AllowEdit = false;
            this.colInternalCOntractorDescription.OptionsColumn.AllowFocus = false;
            this.colInternalCOntractorDescription.OptionsColumn.ReadOnly = true;
            this.colInternalCOntractorDescription.Width = 52;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Width = 51;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Width = 62;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 1;
            this.colTypeDescription.Width = 111;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.Width = 49;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Reg";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 62;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04157GCSnowClearanceTeamRatesEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(112, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // SnowClearanceSubContractorRateRuleSpinEdit
            // 
            this.SnowClearanceSubContractorRateRuleSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "SnowClearanceSubContractorRateRule", true));
            this.SnowClearanceSubContractorRateRuleSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SnowClearanceSubContractorRateRuleSpinEdit.Location = new System.Drawing.Point(88, 36);
            this.SnowClearanceSubContractorRateRuleSpinEdit.MenuManager = this.barManager1;
            this.SnowClearanceSubContractorRateRuleSpinEdit.Name = "SnowClearanceSubContractorRateRuleSpinEdit";
            this.SnowClearanceSubContractorRateRuleSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SnowClearanceSubContractorRateRuleSpinEdit.Properties.ReadOnly = true;
            this.SnowClearanceSubContractorRateRuleSpinEdit.Size = new System.Drawing.Size(528, 20);
            this.SnowClearanceSubContractorRateRuleSpinEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearanceSubContractorRateRuleSpinEdit.TabIndex = 4;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 336);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(556, 94);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling1);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // StartTimeTimeEdit
            // 
            this.StartTimeTimeEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04157GCSnowClearanceTeamRatesEditBindingSource, "StartTime", true));
            this.StartTimeTimeEdit.EditValue = null;
            this.StartTimeTimeEdit.Location = new System.Drawing.Point(99, 86);
            this.StartTimeTimeEdit.MenuManager = this.barManager1;
            this.StartTimeTimeEdit.Name = "StartTimeTimeEdit";
            this.StartTimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StartTimeTimeEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartTimeTimeEdit.Size = new System.Drawing.Size(213, 20);
            this.StartTimeTimeEdit.StyleController = this.dataLayoutControl1;
            this.StartTimeTimeEdit.TabIndex = 9;
            this.StartTimeTimeEdit.EditValueChanged += new System.EventHandler(this.StartTimeTimeEdit_EditValueChanged);
            this.StartTimeTimeEdit.Validating += new System.ComponentModel.CancelEventHandler(this.StartTimeTimeEdit_Validating);
            this.StartTimeTimeEdit.Validated += new System.EventHandler(this.StartTimeTimeEdit_Validated);
            // 
            // ItemForSnowClearanceSubContractorRateRule
            // 
            this.ItemForSnowClearanceSubContractorRateRule.Control = this.SnowClearanceSubContractorRateRuleSpinEdit;
            this.ItemForSnowClearanceSubContractorRateRule.CustomizationFormText = "Rate Rule ID:";
            this.ItemForSnowClearanceSubContractorRateRule.Location = new System.Drawing.Point(0, 24);
            this.ItemForSnowClearanceSubContractorRateRule.Name = "ItemForSnowClearanceSubContractorRateRule";
            this.ItemForSnowClearanceSubContractorRateRule.Size = new System.Drawing.Size(608, 24);
            this.ItemForSnowClearanceSubContractorRateRule.Text = "Rate Rule ID:";
            this.ItemForSnowClearanceSubContractorRateRule.TextSize = new System.Drawing.Size(72, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 476);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroup4,
            this.emptySpaceItem2,
            this.ItemForStartTime,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.ItemForSubContractorID,
            this.ItemForDayTypeID,
            this.emptySpaceItem4,
            this.ItemForHours,
            this.ItemForRate,
            this.ItemForMachineRate2,
            this.ItemForMachineRate3,
            this.ItemForMachineRate4,
            this.ItemForMinimumHours,
            this.ItemForMinimumHours2,
            this.ItemForMinimumHours3,
            this.ItemForMinimumHours4,
            this.emptySpaceItem3,
            this.ItemForEndTime});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 456);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 446);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 254);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(608, 192);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 146);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(560, 98);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(560, 98);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 242);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 12);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForStartTime
            // 
            this.ItemForStartTime.AllowHide = false;
            this.ItemForStartTime.Control = this.StartTimeTimeEdit;
            this.ItemForStartTime.CustomizationFormText = "Start Time:";
            this.ItemForStartTime.Location = new System.Drawing.Point(0, 74);
            this.ItemForStartTime.Name = "ItemForStartTime";
            this.ItemForStartTime.Size = new System.Drawing.Size(304, 24);
            this.ItemForStartTime.Text = "Start Time:";
            this.ItemForStartTime.TextSize = new System.Drawing.Size(84, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(300, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(308, 24);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(100, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForSubContractorID
            // 
            this.ItemForSubContractorID.Control = this.SubContractorIDGridLookUpEdit;
            this.ItemForSubContractorID.CustomizationFormText = "Team:";
            this.ItemForSubContractorID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSubContractorID.Name = "ItemForSubContractorID";
            this.ItemForSubContractorID.Size = new System.Drawing.Size(608, 26);
            this.ItemForSubContractorID.Text = "Team:";
            this.ItemForSubContractorID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForDayTypeID
            // 
            this.ItemForDayTypeID.Control = this.DayTypeIDGridLookUpEdit;
            this.ItemForDayTypeID.CustomizationFormText = "Day Type:";
            this.ItemForDayTypeID.Location = new System.Drawing.Point(0, 50);
            this.ItemForDayTypeID.Name = "ItemForDayTypeID";
            this.ItemForDayTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForDayTypeID.Text = "Day Type:";
            this.ItemForDayTypeID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(100, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(100, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(100, 24);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForHours
            // 
            this.ItemForHours.Control = this.HoursSpinEdit;
            this.ItemForHours.CustomizationFormText = "Elapsed Hours:";
            this.ItemForHours.Location = new System.Drawing.Point(0, 122);
            this.ItemForHours.Name = "ItemForHours";
            this.ItemForHours.Size = new System.Drawing.Size(608, 24);
            this.ItemForHours.Text = "Elapsed Hours:";
            this.ItemForHours.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForRate
            // 
            this.ItemForRate.Control = this.RateSpinEdit;
            this.ItemForRate.CustomizationFormText = "Machine Rate 1:";
            this.ItemForRate.Location = new System.Drawing.Point(0, 146);
            this.ItemForRate.Name = "ItemForRate";
            this.ItemForRate.Size = new System.Drawing.Size(304, 24);
            this.ItemForRate.Text = "Machine Rate 1:";
            this.ItemForRate.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForMachineRate2
            // 
            this.ItemForMachineRate2.Control = this.MachineRate2SpinEdit;
            this.ItemForMachineRate2.CustomizationFormText = "Machine Rate 2:";
            this.ItemForMachineRate2.Location = new System.Drawing.Point(0, 170);
            this.ItemForMachineRate2.Name = "ItemForMachineRate2";
            this.ItemForMachineRate2.Size = new System.Drawing.Size(304, 24);
            this.ItemForMachineRate2.Text = "Machine Rate 2:";
            this.ItemForMachineRate2.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForMachineRate3
            // 
            this.ItemForMachineRate3.Control = this.MachineRate3SpinEdit;
            this.ItemForMachineRate3.CustomizationFormText = "Machine Rate 3:";
            this.ItemForMachineRate3.Location = new System.Drawing.Point(0, 194);
            this.ItemForMachineRate3.Name = "ItemForMachineRate3";
            this.ItemForMachineRate3.Size = new System.Drawing.Size(304, 24);
            this.ItemForMachineRate3.Text = "Machine Rate 3:";
            this.ItemForMachineRate3.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForMachineRate4
            // 
            this.ItemForMachineRate4.Control = this.MachineRate4SpinEdit;
            this.ItemForMachineRate4.CustomizationFormText = "Machine Rate 4:";
            this.ItemForMachineRate4.Location = new System.Drawing.Point(0, 218);
            this.ItemForMachineRate4.Name = "ItemForMachineRate4";
            this.ItemForMachineRate4.Size = new System.Drawing.Size(304, 24);
            this.ItemForMachineRate4.Text = "Machine Rate 4:";
            this.ItemForMachineRate4.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForMinimumHours
            // 
            this.ItemForMinimumHours.Control = this.MinimumHoursSpinEdit;
            this.ItemForMinimumHours.CustomizationFormText = "Minimum Hours 1:";
            this.ItemForMinimumHours.Location = new System.Drawing.Point(304, 146);
            this.ItemForMinimumHours.Name = "ItemForMinimumHours";
            this.ItemForMinimumHours.Size = new System.Drawing.Size(304, 24);
            this.ItemForMinimumHours.Text = "Minimum Hours 1:";
            this.ItemForMinimumHours.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForMinimumHours2
            // 
            this.ItemForMinimumHours2.Control = this.MinimumHours2SpinEdit;
            this.ItemForMinimumHours2.CustomizationFormText = "Minimum Hours 2:";
            this.ItemForMinimumHours2.Location = new System.Drawing.Point(304, 170);
            this.ItemForMinimumHours2.Name = "ItemForMinimumHours2";
            this.ItemForMinimumHours2.Size = new System.Drawing.Size(304, 24);
            this.ItemForMinimumHours2.Text = "Minimum Hours 2:";
            this.ItemForMinimumHours2.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForMinimumHours3
            // 
            this.ItemForMinimumHours3.Control = this.MinimumHours3SpinEdit;
            this.ItemForMinimumHours3.CustomizationFormText = "Minimum Hours 3:";
            this.ItemForMinimumHours3.Location = new System.Drawing.Point(304, 194);
            this.ItemForMinimumHours3.Name = "ItemForMinimumHours3";
            this.ItemForMinimumHours3.Size = new System.Drawing.Size(304, 24);
            this.ItemForMinimumHours3.Text = "Minimum Hours 3:";
            this.ItemForMinimumHours3.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForMinimumHours4
            // 
            this.ItemForMinimumHours4.Control = this.MinimumHours4SpinEdit;
            this.ItemForMinimumHours4.CustomizationFormText = "Minimum Hours 4:";
            this.ItemForMinimumHours4.Location = new System.Drawing.Point(304, 218);
            this.ItemForMinimumHours4.Name = "ItemForMinimumHours4";
            this.ItemForMinimumHours4.Size = new System.Drawing.Size(304, 24);
            this.ItemForMinimumHours4.Text = "Minimum Hours 4:";
            this.ItemForMinimumHours4.TextSize = new System.Drawing.Size(84, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(304, 74);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(304, 48);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEndTime
            // 
            this.ItemForEndTime.Control = this.EndTimeTimeEdit;
            this.ItemForEndTime.CustomizationFormText = "End Time:";
            this.ItemForEndTime.Location = new System.Drawing.Point(0, 98);
            this.ItemForEndTime.Name = "ItemForEndTime";
            this.ItemForEndTime.Size = new System.Drawing.Size(304, 24);
            this.ItemForEndTime.Text = "End Time:";
            this.ItemForEndTime.TextSize = new System.Drawing.Size(84, 13);
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp04026_Contractor_List_With_BlankTableAdapter
            // 
            this.sp04026_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04157_GC_Snow_Clearance_Team_Rates_EditTableAdapter
            // 
            this.sp04157_GC_Snow_Clearance_Team_Rates_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04159_GC_Work_Day_Types_With_BlankTableAdapter
            // 
            this.sp04159_GC_Work_Day_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Snow_Team_Rate_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 532);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Snow_Team_Rate_Edit";
            this.Text = "Edit Linked Team Snow Clearance Rate";
            this.Activated += new System.EventHandler(this.frm_GC_Snow_Team_Rate_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Snow_Team_Rate_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Snow_Team_Rate_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MinimumHours4SpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04157GCSnowClearanceTeamRatesEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumHours3SpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumHours2SpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MachineRate4SpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MachineRate3SpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MachineRate2SpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumHoursSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndTimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DayTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04159GCWorkDayTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04026ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceSubContractorRateRuleSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceSubContractorRateRule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDayTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMachineRate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMachineRate3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMachineRate4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumHours2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumHours3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumHours4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SpinEdit SnowClearanceSubContractorRateRuleSpinEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearanceSubContractorRateRule;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartTime;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.GridLookUpEdit SubContractorIDGridLookUpEdit;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorID;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private DevExpress.XtraEditors.GridLookUpEdit DayTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDayTypeID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.BindingSource sp04026ContractorListWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04026_Contractor_List_With_BlankTableAdapter sp04026_Contractor_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalCOntractorDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private System.Windows.Forms.BindingSource sp04157GCSnowClearanceTeamRatesEditBindingSource;
        private DataSet_GC_Snow_DataEntry dataSet_GC_Snow_DataEntry;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04157_GC_Snow_Clearance_Team_Rates_EditTableAdapter sp04157_GC_Snow_Clearance_Team_Rates_EditTableAdapter;
        private System.Windows.Forms.BindingSource sp04159GCWorkDayTypesWithBlankBindingSource;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04159_GC_Work_Day_Types_With_BlankTableAdapter sp04159_GC_Work_Day_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TimeEdit StartTimeTimeEdit;
        private DevExpress.XtraEditors.TimeEdit EndTimeTimeEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndTime;
        private DevExpress.XtraEditors.SpinEdit HoursSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHours;
        private DevExpress.XtraEditors.SpinEdit RateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRate;
        private DevExpress.XtraEditors.SpinEdit MachineRate2SpinEdit;
        private DevExpress.XtraEditors.SpinEdit MinimumHoursSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimumHours;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMachineRate2;
        private DevExpress.XtraEditors.SpinEdit MachineRate3SpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMachineRate3;
        private DevExpress.XtraEditors.SpinEdit MachineRate4SpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMachineRate4;
        private DevExpress.XtraEditors.SpinEdit MinimumHours4SpinEdit;
        private DevExpress.XtraEditors.SpinEdit MinimumHours3SpinEdit;
        private DevExpress.XtraEditors.SpinEdit MinimumHours2SpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimumHours2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimumHours3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimumHours4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}

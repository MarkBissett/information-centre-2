﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Menu;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.UserDesigner;

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;
using System.Runtime.InteropServices;  // Required for Excel Generation //
using Excel;
using System.Reflection;

namespace WoodPlan5
{
    public partial class frm_GC_Job_Sheet_Numbers : BaseObjects.frmBase
    {

        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool iBool_EditLayout = false;

        WaitDialogForm loadingForm;

        public string i_str_ImportDirectoryName = "";
        public string i_str_StoredReportLayoutDirectory = "";

        private string i_str_selected_company_ids = "";
        private string i_str_selected_companies = "";
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection1;

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
 
        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        int i_int_FocusedGrid = 1;

        rpt_GC_Gritting_Team_Missing_Job_Numbers_Sheet rptReport;

        #endregion
        
        public frm_GC_Job_Sheet_Numbers()
        {
            InitializeComponent();
        }

        private void frm_GC_Job_Sheet_Numbers_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4017;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            dateEditFromDate.DateTime = DateTime.Today.AddDays(-30);
            dateEditToDate.DateTime = DateTime.Now;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

 
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //       

            sp04237_GC_Company_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04237_GC_Company_Filter_ListTableAdapter.Fill(this.dataSet_GC_Reports.sp04237_GC_Company_Filter_List);

            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            gridControl2.ForceInitialize();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04275_GC_Job_Sheet_Numbers_Jobs_Missing_NumbersTableAdapter.Connection.ConnectionString = strConnectionString;
            // Add record selection checkboxes to grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.Fixed = FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            gridControl1.ForceInitialize();

            RefreshGridViewState1 = new RefreshGridState(gridView1, "GrittingCallOutID");

            sp04279_GC_Missing_Job_Sheet_Numbers_ImportTableAdapter.Connection.ConnectionString = strConnectionString;

            emptyEditor = new RepositoryItem();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_ImportDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "MissingJobNumberSheetLoadPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Import Spreadsheets (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Import Spreadsheet Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_StoredReportLayoutDirectory = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Team Missing Job Number Sheets Layout (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Missing Job Number Sheets Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_StoredReportLayoutDirectory.EndsWith("\\")) i_str_StoredReportLayoutDirectory += "\\";  // Add Backslash to end //

            if (!i_str_ImportDirectoryName.EndsWith("\\")) i_str_ImportDirectoryName += "\\";  // Add Backslash to end //
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }

        private void frm_GC_Job_Sheet_Numbers_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
             }
            SetMenuStatus();
        }

        private void frm_GC_Job_Sheet_Numbers_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "IncludeCompleted", (checkEditIncludeCompleted.Checked ? "1" : "0"));
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_company_ids);
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            LoadLastSavedUserScreenSettings();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Edit Layout Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_EditLayout = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
                        
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            switch (i_int_FocusedGrid)
            {
                 case 1:    // Linked Gritting Jobs //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        bbiDelete.Enabled = false;
                    }
                    break;
                case 3:    // Client Invoice Lines //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        bsiEdit.Enabled = false;
                        bbiSingleEdit.Enabled = false;
                        bbiBlockEdit.Enabled = false;
                        bbiDelete.Enabled = true;
                    }
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowDelete)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            // Set Enabled Status of Main Toolbar Buttons //
            bbiEditLayout.Enabled = iBool_EditLayout;
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Include Comepleted //
                string strIncludeCompleted = default_screen_settings.RetrieveSetting("IncludeCompleted");
                if (!string.IsNullOrEmpty(strIncludeCompleted)) checkEditIncludeCompleted.Checked = (strIncludeCompleted == "0" ? false : true);
                
                // Gritting Callout Filter //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("CompanyFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] strArray = strItemFilter.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl2.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in strArray)
                    {
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["CompanyID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    PopupContainerEdit1_Get_Selected();
                }
                bbiReloadData.PerformClick();
            }
        }

        private void Load_Data()
        {
            WaitDialogForm loading = new WaitDialogForm("Loading Jobs with Missing Job Sheet Numbers...", "Job Sheet Numbers");
            if (fProgress == null)
            {
                loading.Show();
            }
            else
            {
                loading.Hide();
                loading.Dispose();
            }
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (i_str_AddedRecordIDs1 != "" && dateEditToDate.DateTime < DateTime.Now) dateEditToDate.DateTime = DateTime.Now;  // Make sure Period spanned is big enough //
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            int intShowCompleted = Convert.ToInt32(checkEditIncludeCompleted.Checked);
            if (i_str_selected_company_ids == null) i_str_selected_company_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
            view.BeginUpdate();
            sp04275_GC_Job_Sheet_Numbers_Jobs_Missing_NumbersTableAdapter.Fill(this.dataSet_GC_DataEntry.sp04275_GC_Job_Sheet_Numbers_Jobs_Missing_Numbers, i_str_selected_company_ids, dtFromDate, dtToDate, intShowCompleted);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["GrittingCallOutID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (fProgress == null)
            {
                loading.Close();
                loading.Dispose();
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }


        #region Parameters Filter Panel

        private void repositoryItemPopupContainerEditParameters_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            PopupContainerEdit1_Get_Selected();
        }

        private void btnOK2_Click(object sender, EventArgs e)
        {
            PopupContainerEdit1_Get_Selected();
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_company_ids = "";    // Reset any prior values first //
            i_str_selected_companies = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_company_ids = "";
                return "All Companies";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_company_ids = "";
                return "All Companies";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_company_ids += Convert.ToString(view.GetRowCellValue(i, "CompanyID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_companies = Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_companies += ", " + Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_companies;
        }

        private void dateEditFromDate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Gritting Callouts with Missing Job Sheet Numbers Available - Try changing the Parameters and click Reload Data";
                    break;
                case "gridView3":
                    message = "No Job Sheet Numbers Available - Click Load Spreadsheets button";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                //bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "SubContractorName")
            {
                switch (view.GetRowCellValue(e.RowHandle, "SubContractorName").ToString())
                {
                    case "":
                        //e.Appearance.BackColor = Color.LightCoral;
                        //e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }*/
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                //bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "Sheet Number")
            {
                if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "Sheet Number").ToString()))
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0xFC, 0x80);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x7E, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void bbiReloadData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Gritting Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        System.Windows.Forms.Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Edit fChildForm = new frm_GC_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                 case 1:     // Gritting Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        System.Windows.Forms.Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Edit fChildForm = new frm_GC_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 3:  // Imported Job Numbers //
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    if (intRowHandles.Length <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or imported job numbers to remove by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + (intRowHandles.Length == 1 ? " Imported Job Number" : " Imported Job Numbers") + " selected for delete!\n\nProceed?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();
                        int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                        int intUpdateProgressTempCount = 0;

                        gridControl3.BeginUpdate();
                        for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                        {
                            view.DeleteRow(intRowHandles[intRowHandle]);
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }
                        gridControl3.EndUpdate();
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void bbiClientSpreadsheets_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;

            if (view.DataRowCount <= 0 || selection1.SelectedCount <= 0) 
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to include on the Missing Job Sheet Numbers spreadsheets / PDAs by ticking them before proceeding.", "Create Missing Job Sheet Numbers Spreadsheets / PDFs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intCountSS = 0;
            int intCountPDF = 0;
            string strTeamsForSS = ",";
            string strTeamsForPDF = ",";
            string strCalloutIDs = "";
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strCalloutIDs += view.GetRowCellValue(i, "GrittingCallOutID").ToString() + ",";
                    if (Convert.ToInt32(view.GetRowCellValue(i, "MissingJobSheetDeliveryMethodID")) == 1)  // Spreadsheet //
                    {
                        if (!strTeamsForSS.Contains(',' + view.GetRowCellValue(i, "SubContractorID").ToString() + ','))
                        {
                            strTeamsForSS += view.GetRowCellValue(i, "SubContractorID").ToString() + ',';
                            intCountSS++;
                        }
                    }
                    else if (Convert.ToInt32(view.GetRowCellValue(i, "MissingJobSheetDeliveryMethodID")) == 2)  // Device //
                    {
                        if (!strTeamsForPDF.Contains(',' + view.GetRowCellValue(i, "SubContractorID").ToString() + ','))
                        {
                            strTeamsForPDF += view.GetRowCellValue(i, "SubContractorID").ToString() + ',';
                            intCountPDF++;
                        }
                    }
                    else
                    {
                        view.SetRowCellValue(i, "CheckMarkSelection", false);  // Untick item as they have no delivery method set //
                    }
                }
            }
            strTeamsForSS = strTeamsForSS.Remove(0, 1);  // Remove dummy ',' at start //
            strTeamsForPDF = strTeamsForPDF.Remove(0, 1);  // Remove dummy ',' at start //
            if (string.IsNullOrEmpty(strTeamsForSS) && string.IsNullOrEmpty(strTeamsForPDF))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to include on the Missing Job Sheet Numbers spreadsheets / PDAs by ticking them before proceeding.", "Create Missing Job Sheet Numbers Spreadsheets / PDFs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to create send missing job sheet number reports to Teams.\n\nProceed?", "Create Missing Job Sheet Numbers Spreadsheets / PDFs", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            // OK to proceed //
            #region GetEmailSettings
            // Get DB Settings for generated files //
            string strSavePath = "";
            string strEmailFromAddress = "";
            string strSubjectLine = "";
            string strEmailCCAddress = "";
            string strSS_HTMLLayoutFile = "";
            string strPDF_HTMLLayoutFile = "";

            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd2 = null;
            cmd2 = new SqlCommand("sp04277_GC_Missing_Job_Sheets_Get_Email_Settings", SQlConn);
            cmd2.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd2);
            DataSet dsSettings = new DataSet("NewDataSet");
            try
            {
                sdaSettings.Fill(dsSettings, "Table");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Client Report Folder and Email Settings' (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dsSettings.Tables[0].Rows.Count != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Client Report Folder and Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DataRow dr1 = dsSettings.Tables[0].Rows[0];
            strSavePath = dr1["SavePath"].ToString();
            strEmailFromAddress = dr1["EmailFromAddress"].ToString();
            strSubjectLine = dr1["SubjectLine"].ToString();
            strEmailCCAddress = dr1["EmailCCAddress"].ToString();
            strSS_HTMLLayoutFile = dr1["SS_HTMLLayoutFile"].ToString();
            strPDF_HTMLLayoutFile = dr1["PDF_HTMLLayoutFile"].ToString();

            strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
            strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
            strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
            strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

            if (string.IsNullOrEmpty(strSavePath) || string.IsNullOrEmpty(strEmailFromAddress) || string.IsNullOrEmpty(strSS_HTMLLayoutFile) || string.IsNullOrEmpty(strPDF_HTMLLayoutFile) || string.IsNullOrEmpty(strSMTPMailServerAddress))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the folder and email settings (Email Layout File, From Email Address, PDF File Folder and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strSavePath.EndsWith("\\")) strSavePath += "\\";  // Add Backslash to end //

            #endregion

            string strCreatedDirectory = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "\\";
            DateTime dtCreationDate = DateTime.Now;

            try
            {
                if (!Directory.Exists(strSavePath + strCreatedDirectory)) Directory.CreateDirectory(strSavePath + strCreatedDirectory);
                GC.Collect();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Folder to store the created spreadsheets / PDFs. Message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Missing Job Sheet Numbers Spreadsheets / PDFs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            #region Email Spreadsheets
            if (!string.IsNullOrEmpty(strTeamsForSS))  // Email spreadsheets //
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Creating Team Spreadsheets...");
                fProgress.Show();
                System.Windows.Forms.Application.DoEvents();
                int intUpdateProgressThreshhold = intCountSS / 10;
                int intUpdateProgressTempCount = 0;

                string strExportFileName = "";
                int intTeamID = 0;
                char[] delimiters = new char[] { ',' };
                string[] strElements = strTeamsForSS.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                try
                {
                    foreach (string strElement in strElements)
                    {
                        intTeamID = Convert.ToInt32(strElement);

                        // Load Dataset //
                        SqlCommand cmd = null;
                        DataSet dsCallouts = new DataSet("NewDataSet");

                        cmd = new SqlCommand("sp04278_GC_Missing_Job_Sheet_Numbers_For_Team_ID", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
                        cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strCalloutIDs));
                        SqlDataAdapter sdaCallouts = new SqlDataAdapter(cmd);
                        sdaCallouts.Fill(dsCallouts, "Table");
                        if (dsCallouts.Tables[0].Rows.Count <= 0) continue;

                        string strBody = System.IO.File.ReadAllText(strSS_HTMLLayoutFile);

                        // Create Spreadsheet //
                        #region Create Excel File

                        strExportFileName = strSavePath + strCreatedDirectory + "TEAM MISSING JOB NUMBERS " + intTeamID.ToString() + " " + Convert.ToDateTime(dtCreationDate).ToString("yyyy-MM-hh_HH-mm-ss") + ".xls";

                        Excel.Application xl = null;
                        Excel._Workbook wb = null;
                        Excel._Worksheet sheet = null;
                        bool SaveChanges = false;

                        try
                        {
                            if (File.Exists(strExportFileName)) File.Delete(strExportFileName);
                            GC.Collect();

                            // Create a new instance of Excel from scratch
                            xl = new Excel.Application();
                            xl.Visible = false;

                            // Add one workbook to the instance of Excel
                            wb = (Excel._Workbook)(xl.Workbooks.Add(Missing.Value));
                            wb.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                            sheet = (Excel._Worksheet)(wb.Sheets[1]); // Get a reference to the one and only worksheet in our workbook //
                            sheet.Name = "Callouts";  // Set column heading names //

                            DataRow d = dsCallouts.Tables[0].Rows[0];
                            string strTeamID = d["Team ID"].ToString();
                            string strTeamName = d["Team Name"].ToString();

                            dsCallouts.Tables[0].Columns.Remove("Team ID");
                            dsCallouts.Tables[0].Columns.Remove("Team Name");

                            // Copy data into array then write it in one hit to Excel //
                            object[,] arr = new object[dsCallouts.Tables[0].Rows.Count, dsCallouts.Tables[0].Columns.Count];
                            for (int r = 0; r < dsCallouts.Tables[0].Rows.Count; r++)
                            {
                                DataRow dr = dsCallouts.Tables[0].Rows[r];
                                for (int c = 0; c < dsCallouts.Tables[0].Columns.Count; c++)
                                {
                                    arr[r, c] = dr[c];
                                }
                            }

                            Excel.Range c1 = (Excel.Range)xl.Cells[3, 1];
                            Excel.Range c2 = (Excel.Range)xl.Cells[3 + dsCallouts.Tables[0].Rows.Count - 1, dsCallouts.Tables[0].Columns.Count];
                            Excel.Range range = xl.get_Range(c1, c2);
                            range.Value = arr;


                            // Set Top Row Details //
                            sheet.Cells[1, 1] = "Date:";
                            sheet.Cells[1, 2] = dtCreationDate.ToString();
                            ((Range)sheet.Cells[1, 2]).NumberFormat = "dd/mm/yyyy HH:mm";
                            sheet.Cells[1, 3] = "Team ID:";
                            sheet.Cells[1, 4] = strTeamID;
                            sheet.Cells[1, 5] = "Team Name:";
                            sheet.Cells[1, 6] = strTeamName;

                            // Colour header cell borders Dark Orange //
                            range = sheet.get_Range("A1", "B1");
                            range.Borders[XlBordersIndex.xlEdgeBottom].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range.Borders[XlBordersIndex.xlEdgeLeft].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range.Borders[XlBordersIndex.xlEdgeRight].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range.Borders[XlBordersIndex.xlEdgeTop].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);

                            range = sheet.get_Range("C1", "D1");
                            range.Borders[XlBordersIndex.xlEdgeBottom].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range.Borders[XlBordersIndex.xlEdgeLeft].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range.Borders[XlBordersIndex.xlEdgeRight].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range.Borders[XlBordersIndex.xlEdgeTop].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);

                            range = sheet.get_Range("E1", "G1");
                            range.Borders[XlBordersIndex.xlEdgeBottom].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range.Borders[XlBordersIndex.xlEdgeLeft].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range.Borders[XlBordersIndex.xlEdgeRight].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range.Borders[XlBordersIndex.xlEdgeTop].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);

                            ((Range)sheet.get_Range("A1", "G1")).Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.NavajoWhite);
                            ((Range)sheet.get_Range("F1", "G1")).Merge();


                            // Set column Header text and Autosize all columns //
                            int intColumnCount = 1;
                            for (int c = 0; c < dsCallouts.Tables[0].Columns.Count; c++)
                            {
                                sheet.Cells[2, intColumnCount] = dsCallouts.Tables[0].Columns[c].ColumnName;
                                intColumnCount++;
                                ((Range)sheet.Cells[2, c + 1]).EntireColumn.AutoFit();  // Autosize columns //
                                switch (dsCallouts.Tables[0].Columns[c].ColumnName.ToString().ToLower())
                                {
                                    case "sheet number":
                                        ((Range)sheet.Cells[1, c + 1]).EntireColumn.NumberFormat = "@";  // Full Column including headers //
                                        ((Range)sheet.Cells[2, c + 1]).Font.Bold = true;  // Bold Column Header //
                                        break;
                                    case "callout date":
                                        ((Range)sheet.Cells[3, c + 1]).EntireColumn.NumberFormat = "dd/mm/yyyy";
                                        break;
                                    case "Completed Time":
                                        ((Range)sheet.Cells[3, c + 1]).EntireColumn.NumberFormat = "dd/mm/yyyy HH:mm";
                                        break;
                                }
                            }

                            string strLastColumnExcelName = GetExcelColumnName(dsCallouts.Tables[0].Columns.Count);

                            // Set Column Header back colour //
                            range = sheet.get_Range("A2", strLastColumnExcelName + "2");
                            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xFA, 0xBB, 0x6F));

                            // Colour odd and even row back colour //
                            int intCurrentRow = 3;  // start at 3 to skip past 2 header rows //
                            foreach (DataRow row in dsCallouts.Tables[0].Rows)
                            {
                                Range range2 = sheet.get_Range("A" + intCurrentRow.ToString(), strLastColumnExcelName + intCurrentRow.ToString());
                                if ((intCurrentRow & 1) == 1)  // 1 = Odd row //
                                {
                                    range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0xB4, 0xFA, 0xB4));

                                }
                                else  //  0 = Even row //
                                {
                                    range2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.NavajoWhite);
                                }
                                intCurrentRow++;
                            }

                            Range last = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);  // Update Last cell now we have added totals //

                            // Colour all cell borders Dark Orange //
                            Range range3 = sheet.get_Range("A2", last);
                            range3.Borders[XlBordersIndex.xlEdgeBottom].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range3.Borders[XlBordersIndex.xlEdgeLeft].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range3.Borders[XlBordersIndex.xlEdgeRight].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range3.Borders[XlBordersIndex.xlEdgeTop].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range3.Borders[XlBordersIndex.xlInsideHorizontal].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);
                            range3.Borders[XlBordersIndex.xlInsideVertical].Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkOrange);

                            // Save and Close of the Excel instance
                            xl.Visible = false;
                            xl.UserControl = false;

                            // Set a flag saying that all is well and it is ok to save our changes to a file.
                            SaveChanges = true;
                            xl.DisplayAlerts = false;  // Prevent Compatibility Checked dialogue displaying due to cell colours etc //

                            //  Save the file to disk
                            wb.SaveAs(strExportFileName, Excel.XlFileFormat.xlWorkbookNormal,
                                        null, null, false, false, Excel.XlSaveAsAccessMode.xlShared,
                                        false, false, null, null);  //Excel.XlFileFormat.xlWorkbookNormal

                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Excel Spreadsheet [" + strExportFileName + "]. Message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Missing Job Sheet Numbers Spreadsheets / PDFs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        finally
                        {
                            try
                            {
                                // Repeat xl.Visible and xl.UserControl releases just to be sure we didn't error out ahead of time.
                                xl.Visible = false;
                                xl.UserControl = false;
                                // Close the document and avoid user prompts to save if our method failed.
                                wb.Close(SaveChanges, null, null);
                                xl.Workbooks.Close();
                            }
                            catch { }

                            // Gracefully exit out and destroy all COM objects to avoid hanging instances of Excel.exe whether our method failed or not.
                            xl.Quit();

                            //if (module != null) { Marshal.ReleaseComObject(module); }
                            if (sheet != null) { Marshal.ReleaseComObject(sheet); }
                            if (wb != null) { Marshal.ReleaseComObject(wb); }
                            if (xl != null) { Marshal.ReleaseComObject(xl); }

                            //module = null;
                            sheet = null;
                            wb = null;
                            xl = null;
                            GC.Collect();
                        }
                        #endregion

                        // Spreadsheet Created so Email to Team //
                        #region Email To Team
                        DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetEmailAddress = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                        GetEmailAddress.ChangeConnectionString(strConnectionString);
                        string strEmailAddresses = GetEmailAddress.sp04117_GC_Team_Email_Addressed_For_Callout_Email(intTeamID).ToString();
                        if (!String.IsNullOrEmpty(strEmailAddresses))
                        {
                            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                            msg.From = new System.Net.Mail.MailAddress(strEmailFromAddress);
                            string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strEmailTo.Length > 0)
                            {
                                foreach (string strEmailAddress in strEmailTo)
                                {
                                    msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                                }
                            }
                            else
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                            }
                            msg.Subject = strSubjectLine;
                            msg.CC.Add(strEmailCCAddress);
                            msg.Priority = System.Net.Mail.MailPriority.High;
                            msg.IsBodyHtml = true;

                            System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                            System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                            // Create a new attachment //
                            System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strExportFileName);  // Create the attachment //
                            msg.Attachments.Add(mailAttachment);

                            //create the LinkedResource (embedded image)
                            System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                            logo.ContentId = "companylogo";
                            //add the LinkedResource to the appropriate view
                            htmlView.LinkedResources.Add(logo);

                            //create the LinkedResource (embedded image)
                            System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                            logo2.ContentId = "companyfooter";
                            //add the LinkedResource to the appropriate view
                            htmlView.LinkedResources.Add(logo2);

                            msg.AlternateViews.Add(plainView);
                            msg.AlternateViews.Add(htmlView);

                            object userState = msg;
                            System.Net.Mail.SmtpClient emailClient = null;
                            if (intSMTPMailServerPort != 0)
                            {
                                emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                            }
                            else
                            {
                                emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                            }
                            if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                            {
                                System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                                emailClient.UseDefaultCredentials = false;
                                emailClient.Credentials = basicCredential;
                            }
                            emailClient.SendAsync(msg, userState);
                        }
                        #endregion

                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            if (fProgress != null) fProgress.UpdateProgress(10);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Creating and Sending Missing Job Sheet Numbers to Teams.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Missing Job Sheet Numbers Spreadsheets / PDFs", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                System.Windows.Forms.Application.DoEvents();
            }
            #endregion

            #region Email PDFs
            if (!string.IsNullOrEmpty(strTeamsForPDF))  // Email PDFs //
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Creating Team PDFs...");
                fProgress.Show();
                System.Windows.Forms.Application.DoEvents();
                int intUpdateProgressThreshhold = intCountSS / 10;
                int intUpdateProgressTempCount = 0;

                string strExportFileName = "";
                int intTeamID = 0;
                char[] delimiters = new char[] { ',' };
                string[] strElements = strTeamsForPDF.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                try
                {
                    foreach (string strElement in strElements)
                    {
                        intTeamID = Convert.ToInt32(strElement);
                        string strBody = System.IO.File.ReadAllText(strPDF_HTMLLayoutFile);

                        // Create PDF based on Report //
                        #region Create PDF File

                        strExportFileName = strSavePath + strCreatedDirectory + "TEAM MISSING JOB NUMBERS " + intTeamID.ToString() + " " + Convert.ToDateTime(dtCreationDate).ToString("yyyy-MM-hh_HH-mm-ss") + ".pdf";
 
                        string strReportFileName = "TeamMissingJobSheetNumber.repx";
                        rpt_GC_Gritting_Team_Missing_Job_Numbers_Sheet rptReport = new rpt_GC_Gritting_Team_Missing_Job_Numbers_Sheet(this.GlobalSettings, intTeamID, strCalloutIDs);
                        rptReport.LoadLayout(i_str_StoredReportLayoutDirectory + strReportFileName);

                        // Set security options of report so when it is exported, it can't be edited and is password protected //
                        rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
                        rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
                        rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
                        rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsPassword = "GroundControlXX";
                        //if (!string.IsNullOrEmpty(strEmailPassword)) rptReport.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = strEmailPassword;

                        rptReport.ExportToPdf(strExportFileName);

                        #endregion

                        // Spreadsheet Created so Email to Team //
                        #region Email To Team
                        DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetEmailAddress = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                        GetEmailAddress.ChangeConnectionString(strConnectionString);
                        string strEmailAddresses = GetEmailAddress.sp04117_GC_Team_Email_Addressed_For_Callout_Email(intTeamID).ToString();
                        if (!String.IsNullOrEmpty(strEmailAddresses))
                        {
                            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                            msg.From = new System.Net.Mail.MailAddress(strEmailFromAddress);
                            string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strEmailTo.Length > 0)
                            {
                                foreach (string strEmailAddress in strEmailTo)
                                {
                                    msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                                }
                            }
                            else
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                            }
                            msg.Subject = strSubjectLine;
                            msg.CC.Add(strEmailCCAddress);
                            msg.Priority = System.Net.Mail.MailPriority.High;
                            msg.IsBodyHtml = true;

                            System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                            System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                            // Create a new attachment //
                            System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strExportFileName);  // Create the attachment //
                            msg.Attachments.Add(mailAttachment);

                            //create the LinkedResource (embedded image)
                            System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                            logo.ContentId = "companylogo";
                            //add the LinkedResource to the appropriate view
                            htmlView.LinkedResources.Add(logo);

                            //create the LinkedResource (embedded image)
                            System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                            logo2.ContentId = "companyfooter";
                            //add the LinkedResource to the appropriate view
                            htmlView.LinkedResources.Add(logo2);

                            msg.AlternateViews.Add(plainView);
                            msg.AlternateViews.Add(htmlView);

                            object userState = msg;
                            System.Net.Mail.SmtpClient emailClient = null;
                            if (intSMTPMailServerPort != 0)
                            {
                                emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                            }
                            else
                            {
                                emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                            }
                            if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                            {
                                System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                                emailClient.UseDefaultCredentials = false;
                                emailClient.Credentials = basicCredential;
                            }
                            emailClient.SendAsync(msg, userState);
                        }
                        #endregion

                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            if (fProgress != null) fProgress.UpdateProgress(10);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Creating and Sending Missing Job Sheet Numbers to Teams.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Missing Job Sheet Numbers Spreadsheets / PDFs", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                System.Windows.Forms.Application.DoEvents();
            }
            #endregion

            DevExpress.XtraEditors.XtraMessageBox.Show("Team Missing Job Number Sheet PDF and Spreadsheet Creation Completed.\n\nFiles stored in: " + i_str_ImportDirectoryName + strCreatedDirectory + " Folder.", "Create Missing Job Sheet Numbers Spreadsheets / PDFs", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

 
        private string GetExcelColumnName(int columnIndex)
        {
            string columnName = "";

            // check whether the column count is > 26
            if (columnIndex > 26)
            {
                // If the column count is > 26, the the last column index will be something like "AA", "DE", "BC" etc

                // Get the first letter
                // ASCII index 65 represent char. 'A'. So, we use 64 in this calculation as a starting point
                char first = Convert.ToChar(64 + ((columnIndex - 1) / 26));

                // Get the second letter
                char second = Convert.ToChar(64 + (columnIndex % 26 == 0 ? 26 : columnIndex % 26));

                // Concat. them
                columnName = first.ToString() + second.ToString();
            }
            else
            {
                // ASCII index 65 represent char. 'A'. So, we use 64 in this calculation as a starting point
                columnName = Convert.ToChar(64 + columnIndex).ToString();
            }
            return columnName;
        }

        private void bbiEditLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strReportFileName = "TeamMissingJobSheetNumber.repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                rpt_GC_Gritting_Team_Missing_Job_Numbers_Sheet rptReport = null;
                //frm_GC_Team_Self_Billing_Invoice_Test_Layout fChildForm = new frm_GC_Team_Self_Billing_Invoice_Test_Layout();
                //fChildForm.GlobalSettings = this.GlobalSettings;
                //if (fChildForm.ShowDialog() == DialogResult.OK)
                //{
                //    rptReport = new rpt_GC_Gritting_Team_Missing_Job_Numbers_Sheet(this.GlobalSettings, fChildForm.intInvoiceHeader);
                //}
                //else
                //{
                    rptReport = new rpt_GC_Gritting_Team_Missing_Job_Numbers_Sheet(this.GlobalSettings, 0, "");
                //}
                try
                {
                    rptReport.LoadLayout(i_str_StoredReportLayoutDirectory + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (Control.ModifierKeys == Keys.Control)  // Open report for preview so we can see result of any changes //
                {
                    rptReport.ShowRibbonPreview();
                    return;
                }


                loadingForm = new WaitDialogForm("Loading Report Builder...", "Reporting");
                loadingForm.Show();

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_StoredReportLayoutDirectory, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                loadingForm.Close();
                form.ShowDialog();
                panel.CloseReport();
            }
        }

        private void bbiLoadSpreadsheets_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strDefaultPath = i_str_ImportDirectoryName;
            string strImportPath = "";
            string strFileName = "";

            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.DefaultExt = "xls";
                dlg.Filter = "Excel Files (*.xls)|*.xls"; ;
                if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                dlg.Multiselect = true;
                dlg.ShowDialog();
                if (dlg.FileNames.Length > 0)
                {
                    this.dataSet_GC_DataEntry.sp04279_GC_Missing_Job_Sheet_Numbers_Import.Clear();  // Clear DataTable //
                    gridControl3.BeginUpdate();
                    
                    frmProgress fProgress = new frmProgress(20);
                    fProgress.UpdateCaption("Importing Spreadsheet(s)...");
                    fProgress.Show();
                    System.Windows.Forms.Application.DoEvents();
                    int intUpdateProgressThreshhold = dlg.FileNames.Length / 10;
                    int intUpdateProgressTempCount = 0;

                    foreach (string filename in dlg.FileNames)
                    {
                        FileInfo file = new FileInfo(filename);

                        strImportPath = file.DirectoryName;
                        string strPathAndFileName = file.FullName;
                        strFileName = file.Name.Substring(0, file.Name.Length - 4);

                        if (!strFileName.ToUpper().Contains("TEAM MISSING JOB NUMBERS"))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("The file selected for Importing must be named 'TEAM MISSING JOB NUMBERS......xls'.\n\nTry selecting the correct file before proceeding.", "Load Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            System.Windows.Forms.Application.DoEvents();
                            gridControl3.EndUpdate();
                            return;
                        }

                        Excel.Application xl = null;
                        Excel._Workbook wb = null;
                        Excel._Worksheet sheet = null;
                        try
                        {
                            xl = new Excel.Application();// Create a new instance of Excel from scratch
                            xl.Visible = false;

                            wb = xl.Workbooks.Open(strPathAndFileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                            sheet = (Worksheet)wb.Sheets[1];

                            Range range2 = (Range)sheet.Cells[1, 4];
                            int intTeamID = Convert.ToInt32(range2.Value);
                            range2 = (Range)sheet.Cells[1, 6];
                            string strTeamName = range2.Value.ToString();

                            Range last = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
                            for (int i = 3; i <= last.Row; i++)  // Skip first 2 Header rows //
                            {
                                Excel.Range range = sheet.get_Range("A" + i.ToString(), "G" + i.ToString());
                                System.Array myvalues = (System.Array)range.Cells.Value;
                                if (myvalues.Length != 7)
                                {
                                    continue;
                                }
                                DataRow drNewRow;
                                drNewRow = this.dataSet_GC_DataEntry.sp04279_GC_Missing_Job_Sheet_Numbers_Import.NewRow();
                                drNewRow["Callout ID"] = Convert.ToInt32(myvalues.GetValue(1, 1));
                                drNewRow["Site ID"] = Convert.ToInt32(myvalues.GetValue(1, 2));
                                drNewRow["Site Name"] = Convert.ToString(myvalues.GetValue(1, 3));
                                drNewRow["Sheet Number"] = Convert.ToString(myvalues.GetValue(1, 4));
                                drNewRow["Callout Date"] = Convert.ToDateTime(myvalues.GetValue(1, 5));
                                drNewRow["Completed Time"] = Convert.ToDateTime(myvalues.GetValue(1, 6));
                                drNewRow["Reactive"] = Convert.ToString(myvalues.GetValue(1, 7));
                                drNewRow["Team ID"] = intTeamID;
                                drNewRow["Team Name"] = strTeamName;
                                drNewRow["RowError"] = 0;
                                this.dataSet_GC_DataEntry.sp04279_GC_Missing_Job_Sheet_Numbers_Import.Rows.Add(drNewRow);
                            }
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading spreadsheets.\n\nThe file may be open in another application or it may be an invalid Missing Job Sheet Numbers file.", "Load Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        finally
                        {
                            try
                            {
                                // Repeat xl.Visible and xl.UserControl releases just to be sure we didn't error out ahead of time.
                                xl.Visible = false;
                                xl.UserControl = false;
                                xl.Workbooks.Close();
                            }
                            catch { }

                            // Gracefully exit out and destroy all COM objects to avoid hanging instances of Excel.exe whether our method failed or not.
                            xl.Quit();

                            //if (module != null) { Marshal.ReleaseComObject(module); }
                            if (sheet != null) { Marshal.ReleaseComObject(sheet); }
                            if (wb != null) { Marshal.ReleaseComObject(wb); }
                            if (xl != null) { Marshal.ReleaseComObject(xl); }

                            //module = null;
                            sheet = null;
                            wb = null;
                            xl = null;
                            GC.Collect();
                        }
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            if (fProgress != null) fProgress.UpdateProgress(10);
                        }
                    }
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    System.Windows.Forms.Application.DoEvents();
                    gridControl3.EndUpdate();
                    GridView view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                }
            }
        }

        private void bbiImport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
 
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         
            
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Grid - Correct before procceeding.", "Import Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            } 

            int intCount = view.DataRowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Load one or more Completed Job Number Spreadsheets before.", "Import Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to Import Job Sheet Numbers for " + intCount.ToString() + (intCount == 1 ? " Callout" : " Callouts") + ".\n\nProceed?", "Import Missing Job Sheet Numbers", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            // OK to proceed //
            frmProgress fProgress = new frmProgress(20);
            fProgress.UpdateCaption("Importing Spreadsheets...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intCount / 10;
            int intUpdateProgressTempCount = 0;

            int intCalloutID = 0;
            string strJobSheetNumber = "";
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter UpdateJobSheetNumbers = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                UpdateJobSheetNumbers.ChangeConnectionString(strConnectionString);

                for (int i = 0; i < intCount; i++)
                {
                    intCalloutID = Convert.ToInt32(view.GetRowCellValue(i, "Callout ID"));
                    strJobSheetNumber = view.GetRowCellValue(i, "Sheet Number").ToString();

                    UpdateJobSheetNumbers.sp04280_GC_Missing_Job_Sheet_Numbers_Update(intCalloutID, strJobSheetNumber);  // Update DB //

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        if (fProgress != null) fProgress.UpdateProgress(10);
                    }
                }
            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while importing Job Sheet Numbers.\n\nTry Again. If the problem persist, contact Technical Support.", "Import Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            this.dataSet_GC_DataEntry.sp04279_GC_Missing_Job_Sheet_Numbers_Import.Clear();  // Clear DataTable //
            Load_Data();  // Reload list on page 1 //
            DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + (intCount == 1 ? " Callout Job Sheet Number" : " Callout Job Sheet Numbers") + " imported.", "Import Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        private void bbiUpdateJobs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;

            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Grid - Correct before procceeding.", "Update Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            } 

            int intCount = view.DataRowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Callouts to Update.", "Update Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intUpdatedCount = 0;
            for (int i = 0; i < intCount; i++)
            {
                if (!string.IsNullOrEmpty(view.GetRowCellValue(i, "JobSheetNumber").ToString())) intUpdatedCount++;
            }
            if (intUpdatedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Callouts to Update - Enter Sheet Numbers in one or more Callouts before proceeding.", "Update Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to Update Job Sheet Numbers for " + intUpdatedCount.ToString() + (intUpdatedCount == 1 ? " Callout" : " Callouts") + ".\n\nProceed?", "Update Missing Job Sheet Numbers", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            // OK to proceed //
            frmProgress fProgress = new frmProgress(20);
            fProgress.UpdateCaption("Saving Changes...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intUpdatedCount / 10;
            int intUpdateProgressTempCount = 0;

            int intCalloutID = 0;
            int intProcessed = 0;
            string strJobSheetNumber = "";
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter UpdateJobSheetNumbers = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                UpdateJobSheetNumbers.ChangeConnectionString(strConnectionString);

                for (int i = intCount - 1; i >= 0; i--)
                {
                    strJobSheetNumber = view.GetRowCellValue(i, "JobSheetNumber").ToString();
                    if (!string.IsNullOrEmpty(strJobSheetNumber))
                    {
                        intCalloutID = Convert.ToInt32(view.GetRowCellValue(i, "GrittingCallOutID"));
                        UpdateJobSheetNumbers.sp04280_GC_Missing_Job_Sheet_Numbers_Update(intCalloutID, strJobSheetNumber);  // Update DB //

                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            if (fProgress != null) fProgress.UpdateProgress(10);
                        }
                        intProcessed++;
                        if (intProcessed >= intUpdatedCount) break;
                    }
                }
            }
            catch (Exception Ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while updating Job Sheet Numbers.\n\nTry Again. If the problem persist, contact Technical Support.", "Update Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + (intCount == 1 ? " Callout Job Sheet Number" : " Callout Job Sheet Numbers") + " updated.", "Update Missing Job Sheet Numbers", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Report...", "Reporting");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }








   }
}

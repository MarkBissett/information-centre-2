namespace WoodPlan5
{
    partial class frm_GC_Snow_Callout_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Snow_Callout_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling28 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling29 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling30 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling31 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling32 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling33 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling34 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling35 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.InternalRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.sp04163GCSnowCalloutEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Snow_DataEntry = new WoodPlan5.DataSet_GC_Snow_DataEntry();
            this.SignedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NoOneToSignCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.CompletedOnPDACheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TeamMembersPresentMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.FinishedLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinishedLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteManagerNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RecordedBySubContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinanceTeamPaymentExportedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SitePostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TimesheetNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TimesheetSubmittedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TeamInvoiceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NoAccessAbortedRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WarningLabel = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Snow_Core = new WoodPlan5.DataSet_GC_Snow_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamAddressLine11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSnowClearanceCallOutID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceCallOutRateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceSiteContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMachineCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditMachineCount = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colMachineCount2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineCount3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineCount4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineRate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineRate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineRate4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colHours2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMinimumHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMinimumHours2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SubContractorContactedByStaffIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00226StaffListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisplayName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetworkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PaidByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientPOIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RecordedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OriginalSubContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SnowClearanceSiteContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ChargeMethodDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientHowSoonIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04165GCClientHowSoonTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DoNotInvoiceClientReasonMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DoNotInvoiceClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DoNotPaySubContractorReasonMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DoNotPaySubContractorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PaidByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientInvoiceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NonStandardSellCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NonStandardCostCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TotalSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SubContractorPaidCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.OtherSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.OtherCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LabourSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LabourCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LabourVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AbortedReasonMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.AccessCommentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SubContractorETADateEdit = new DevExpress.XtraEditors.DateEdit();
            this.HoursWorkedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CompletedTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.StartTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.GCPONumberSuffixTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SnowClearanceCallOutIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CallOutDateTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RecordedByNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NoAccessCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.JobStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCalloutStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitAbortedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientPOIDDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SubContractorNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SiteNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.OriginalSubContractorNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemFor = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOriginalSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRecordedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPOID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPaidByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRecordedBySubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLabourVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLabourSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForChargeMethodDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForOtherCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOtherSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNonStandardCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNonStandardSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSubContractorPaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPaidByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotPaySubContractor = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotPaySubContractorReason = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTeamInvoiceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTimesheetSubmitted = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.itemForTimesheetNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinanceTeamPaymentExported = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClientInvoiceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotInvoiceClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotInvoiceClientReason = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem3 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAccessComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAbortedReason = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSitePostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTeamMembersPresent = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInternalRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPOIDDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOriginalSubContractorName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRecordedByName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCallOutDateTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHoursWorked = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorETA = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCompletedTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientHowSoonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSubContractorContactedByStaffIDGridLookUpEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWarningLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearanceCallOutID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGCPONumberSuffix = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNoAccess = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoAccessAbortedRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForVisitAborted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteManagerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup18 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStartLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinishedLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinishedLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCompletedOnPDA = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoOneToSign = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSigned = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04163_GC_Snow_Callout_EditTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04163_GC_Snow_Callout_EditTableAdapter();
            this.sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter();
            this.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter();
            this.sp00226_Staff_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter();
            this.sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter = new WoodPlan5.DataSet_GC_Snow_CoreTableAdapters.sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InternalRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04163GCSnowCalloutEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoOneToSignCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedOnPDACheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamMembersPresentMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishedLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishedLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteManagerNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedBySubContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceTeamPaymentExportedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimesheetNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimesheetSubmittedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamInvoiceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoAccessAbortedRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMachineCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMinimumHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorContactedByStaffIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalSubContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceSiteContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeMethodDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientHowSoonIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04165GCClientHowSoonTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientReasonMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPaySubContractorReasonMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPaySubContractorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardSellCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardCostCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorPaidCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbortedReasonMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessCommentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETADateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETADateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursWorkedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCPONumberSuffixTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceCallOutIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallOutDateTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallOutDateTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedByNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoAccessCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitAbortedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalSubContractorNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOriginalSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedBySubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeMethodDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPaySubContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPaySubContractorReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamInvoiceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimesheetSubmitted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemForTimesheetNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceTeamPaymentExported)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInvoiceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClientReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbortedReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMembersPresent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInternalRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOIDDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOriginalSubContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedByName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCallOutDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursWorked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorETA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletedTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientHowSoonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorContactedByStaffIDGridLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceCallOutID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCPONumberSuffix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoAccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoAccessAbortedRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitAborted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteManagerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishedLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishedLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletedOnPDA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoOneToSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSigned)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(956, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            this.barDockControlBottom.Size = new System.Drawing.Size(956, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(956, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 59;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colCalloutStatusID
            // 
            this.colCalloutStatusID.Caption = "Status ID";
            this.colCalloutStatusID.FieldName = "CalloutStatusID";
            this.colCalloutStatusID.Name = "colCalloutStatusID";
            this.colCalloutStatusID.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusID.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusID.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(956, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(956, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(956, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.InternalRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SignedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.NoOneToSignCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.CompletedOnPDACheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamMembersPresentMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.FinishedLongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinishedLatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StartLongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StartLatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteManagerNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RecordedBySubContractorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinanceTeamPaymentExportedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SitePostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.TimesheetNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TimesheetSubmittedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamInvoiceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NoAccessAbortedRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.WarningLabel);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.SubContractorContactedByStaffIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.PaidByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPOIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RecordedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OriginalSubContractorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearanceSiteContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ChargeMethodDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientHowSoonIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotInvoiceClientReasonMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotInvoiceClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotPaySubContractorReasonMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DoNotPaySubContractorCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.PaidByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientInvoiceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NonStandardSellCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.NonStandardCostCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorPaidCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.OtherSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.OtherCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LabourSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LabourCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LabourVatRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AbortedReasonMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.AccessCommentsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorETADateEdit);
            this.dataLayoutControl1.Controls.Add(this.HoursWorkedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CompletedTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.StartTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.GCPONumberSuffixTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearanceCallOutIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CallOutDateTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RecordedByNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NoAccessCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.JobStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPONumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitAbortedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPOIDDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.OriginalSubContractorNameButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp04163GCSnowCalloutEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemFor,
            this.ItemForSubContractorID,
            this.ItemForOriginalSubContractorID,
            this.ItemForRecordedByStaffID,
            this.ItemForClientPOID,
            this.ItemForPaidByStaffID,
            this.ItemForRecordedBySubContractorID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1125, 178, 401, 462);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(956, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // InternalRemarksMemoEdit
            // 
            this.InternalRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "InternalRemarks", true));
            this.InternalRemarksMemoEdit.Location = new System.Drawing.Point(161, 693);
            this.InternalRemarksMemoEdit.MenuManager = this.barManager1;
            this.InternalRemarksMemoEdit.Name = "InternalRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.InternalRemarksMemoEdit, true);
            this.InternalRemarksMemoEdit.Size = new System.Drawing.Size(742, 152);
            this.scSpellChecker.SetSpellCheckerOptions(this.InternalRemarksMemoEdit, optionsSpelling1);
            this.InternalRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.InternalRemarksMemoEdit.TabIndex = 145;
            // 
            // sp04163GCSnowCalloutEditBindingSource
            // 
            this.sp04163GCSnowCalloutEditBindingSource.DataMember = "sp04163_GC_Snow_Callout_Edit";
            this.sp04163GCSnowCalloutEditBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // dataSet_GC_Snow_DataEntry
            // 
            this.dataSet_GC_Snow_DataEntry.DataSetName = "DataSet_GC_Snow_DataEntry";
            this.dataSet_GC_Snow_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SignedCheckEdit
            // 
            this.SignedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "Signed", true));
            this.SignedCheckEdit.Location = new System.Drawing.Point(905, 180);
            this.SignedCheckEdit.MenuManager = this.barManager1;
            this.SignedCheckEdit.Name = "SignedCheckEdit";
            this.SignedCheckEdit.Properties.Caption = "";
            this.SignedCheckEdit.Properties.ReadOnly = true;
            this.SignedCheckEdit.Properties.ValueChecked = 1;
            this.SignedCheckEdit.Properties.ValueUnchecked = 0;
            this.SignedCheckEdit.Size = new System.Drawing.Size(22, 19);
            this.SignedCheckEdit.StyleController = this.dataLayoutControl1;
            this.SignedCheckEdit.TabIndex = 144;
            // 
            // NoOneToSignCheckEdit
            // 
            this.NoOneToSignCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "NoOneToSign", true));
            this.NoOneToSignCheckEdit.Location = new System.Drawing.Point(754, 180);
            this.NoOneToSignCheckEdit.MenuManager = this.barManager1;
            this.NoOneToSignCheckEdit.Name = "NoOneToSignCheckEdit";
            this.NoOneToSignCheckEdit.Properties.Caption = "";
            this.NoOneToSignCheckEdit.Properties.ReadOnly = true;
            this.NoOneToSignCheckEdit.Properties.ValueChecked = 1;
            this.NoOneToSignCheckEdit.Properties.ValueUnchecked = 0;
            this.NoOneToSignCheckEdit.Size = new System.Drawing.Size(22, 19);
            this.NoOneToSignCheckEdit.StyleController = this.dataLayoutControl1;
            this.NoOneToSignCheckEdit.TabIndex = 143;
            // 
            // CompletedOnPDACheckEdit
            // 
            this.CompletedOnPDACheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "CompletedOnPDA", true));
            this.CompletedOnPDACheckEdit.Location = new System.Drawing.Point(597, 180);
            this.CompletedOnPDACheckEdit.MenuManager = this.barManager1;
            this.CompletedOnPDACheckEdit.Name = "CompletedOnPDACheckEdit";
            this.CompletedOnPDACheckEdit.Properties.Caption = "";
            this.CompletedOnPDACheckEdit.Properties.ReadOnly = true;
            this.CompletedOnPDACheckEdit.Properties.ValueChecked = 1;
            this.CompletedOnPDACheckEdit.Properties.ValueUnchecked = 0;
            this.CompletedOnPDACheckEdit.Size = new System.Drawing.Size(28, 19);
            this.CompletedOnPDACheckEdit.StyleController = this.dataLayoutControl1;
            this.CompletedOnPDACheckEdit.TabIndex = 142;
            // 
            // TeamMembersPresentMemoEdit
            // 
            this.TeamMembersPresentMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "TeamMembersPresent", true));
            this.TeamMembersPresentMemoEdit.Location = new System.Drawing.Point(36, 539);
            this.TeamMembersPresentMemoEdit.MenuManager = this.barManager1;
            this.TeamMembersPresentMemoEdit.Name = "TeamMembersPresentMemoEdit";
            this.TeamMembersPresentMemoEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TeamMembersPresentMemoEdit, true);
            this.TeamMembersPresentMemoEdit.Size = new System.Drawing.Size(867, 306);
            this.scSpellChecker.SetSpellCheckerOptions(this.TeamMembersPresentMemoEdit, optionsSpelling2);
            this.TeamMembersPresentMemoEdit.StyleController = this.dataLayoutControl1;
            this.TeamMembersPresentMemoEdit.TabIndex = 141;
            // 
            // FinishedLongitudeTextEdit
            // 
            this.FinishedLongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "FinishedLongitude", true));
            this.FinishedLongitudeTextEdit.Location = new System.Drawing.Point(609, 320);
            this.FinishedLongitudeTextEdit.MenuManager = this.barManager1;
            this.FinishedLongitudeTextEdit.Name = "FinishedLongitudeTextEdit";
            this.FinishedLongitudeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishedLongitudeTextEdit, true);
            this.FinishedLongitudeTextEdit.Size = new System.Drawing.Size(306, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishedLongitudeTextEdit, optionsSpelling3);
            this.FinishedLongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinishedLongitudeTextEdit.TabIndex = 140;
            // 
            // FinishedLatitudeTextEdit
            // 
            this.FinishedLatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "FinishedLatitude", true));
            this.FinishedLatitudeTextEdit.Location = new System.Drawing.Point(609, 296);
            this.FinishedLatitudeTextEdit.MenuManager = this.barManager1;
            this.FinishedLatitudeTextEdit.Name = "FinishedLatitudeTextEdit";
            this.FinishedLatitudeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishedLatitudeTextEdit, true);
            this.FinishedLatitudeTextEdit.Size = new System.Drawing.Size(306, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishedLatitudeTextEdit, optionsSpelling4);
            this.FinishedLatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinishedLatitudeTextEdit.TabIndex = 139;
            // 
            // StartLongitudeTextEdit
            // 
            this.StartLongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "StartLongitude", true));
            this.StartLongitudeTextEdit.Location = new System.Drawing.Point(609, 272);
            this.StartLongitudeTextEdit.MenuManager = this.barManager1;
            this.StartLongitudeTextEdit.Name = "StartLongitudeTextEdit";
            this.StartLongitudeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLongitudeTextEdit, true);
            this.StartLongitudeTextEdit.Size = new System.Drawing.Size(306, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLongitudeTextEdit, optionsSpelling5);
            this.StartLongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.StartLongitudeTextEdit.TabIndex = 138;
            // 
            // StartLatitudeTextEdit
            // 
            this.StartLatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "StartLatitude", true));
            this.StartLatitudeTextEdit.Location = new System.Drawing.Point(609, 248);
            this.StartLatitudeTextEdit.MenuManager = this.barManager1;
            this.StartLatitudeTextEdit.Name = "StartLatitudeTextEdit";
            this.StartLatitudeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLatitudeTextEdit, true);
            this.StartLatitudeTextEdit.Size = new System.Drawing.Size(306, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLatitudeTextEdit, optionsSpelling6);
            this.StartLatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.StartLatitudeTextEdit.TabIndex = 137;
            // 
            // SiteManagerNameTextEdit
            // 
            this.SiteManagerNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SiteManagerName", true));
            this.SiteManagerNameTextEdit.Location = new System.Drawing.Point(597, 156);
            this.SiteManagerNameTextEdit.MenuManager = this.barManager1;
            this.SiteManagerNameTextEdit.Name = "SiteManagerNameTextEdit";
            this.SiteManagerNameTextEdit.Properties.MaxLength = 50;
            this.SiteManagerNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteManagerNameTextEdit, true);
            this.SiteManagerNameTextEdit.Size = new System.Drawing.Size(330, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteManagerNameTextEdit, optionsSpelling7);
            this.SiteManagerNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteManagerNameTextEdit.TabIndex = 136;
            // 
            // RecordedBySubContractorIDTextEdit
            // 
            this.RecordedBySubContractorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "RecordedBySubContractorID", true));
            this.RecordedBySubContractorIDTextEdit.Location = new System.Drawing.Point(138, 204);
            this.RecordedBySubContractorIDTextEdit.MenuManager = this.barManager1;
            this.RecordedBySubContractorIDTextEdit.Name = "RecordedBySubContractorIDTextEdit";
            this.RecordedBySubContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RecordedBySubContractorIDTextEdit, true);
            this.RecordedBySubContractorIDTextEdit.Size = new System.Drawing.Size(789, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RecordedBySubContractorIDTextEdit, optionsSpelling8);
            this.RecordedBySubContractorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RecordedBySubContractorIDTextEdit.TabIndex = 127;
            // 
            // FinanceTeamPaymentExportedCheckEdit
            // 
            this.FinanceTeamPaymentExportedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "FinanceTeamPaymentExported", true));
            this.FinanceTeamPaymentExportedCheckEdit.Location = new System.Drawing.Point(173, 794);
            this.FinanceTeamPaymentExportedCheckEdit.MenuManager = this.barManager1;
            this.FinanceTeamPaymentExportedCheckEdit.Name = "FinanceTeamPaymentExportedCheckEdit";
            this.FinanceTeamPaymentExportedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.FinanceTeamPaymentExportedCheckEdit.Properties.ValueChecked = 1;
            this.FinanceTeamPaymentExportedCheckEdit.Properties.ValueUnchecked = 0;
            this.FinanceTeamPaymentExportedCheckEdit.Size = new System.Drawing.Size(282, 19);
            this.FinanceTeamPaymentExportedCheckEdit.StyleController = this.dataLayoutControl1;
            this.FinanceTeamPaymentExportedCheckEdit.TabIndex = 135;
            this.FinanceTeamPaymentExportedCheckEdit.ToolTip = "Team Payment Exported To Finance";
            // 
            // SitePostcodeTextEdit
            // 
            this.SitePostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SitePostcode", true));
            this.SitePostcodeTextEdit.Location = new System.Drawing.Point(161, 659);
            this.SitePostcodeTextEdit.MenuManager = this.barManager1;
            this.SitePostcodeTextEdit.Name = "SitePostcodeTextEdit";
            this.SitePostcodeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SitePostcodeTextEdit, true);
            this.SitePostcodeTextEdit.Size = new System.Drawing.Size(742, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SitePostcodeTextEdit, optionsSpelling9);
            this.SitePostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.SitePostcodeTextEdit.TabIndex = 134;
            // 
            // SiteAddressLine5TextEdit
            // 
            this.SiteAddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SiteAddressLine5", true));
            this.SiteAddressLine5TextEdit.Location = new System.Drawing.Point(161, 635);
            this.SiteAddressLine5TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine5TextEdit.Name = "SiteAddressLine5TextEdit";
            this.SiteAddressLine5TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine5TextEdit, true);
            this.SiteAddressLine5TextEdit.Size = new System.Drawing.Size(742, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine5TextEdit, optionsSpelling10);
            this.SiteAddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine5TextEdit.TabIndex = 133;
            // 
            // SiteAddressLine4TextEdit
            // 
            this.SiteAddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SiteAddressLine4", true));
            this.SiteAddressLine4TextEdit.Location = new System.Drawing.Point(161, 611);
            this.SiteAddressLine4TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine4TextEdit.Name = "SiteAddressLine4TextEdit";
            this.SiteAddressLine4TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine4TextEdit, true);
            this.SiteAddressLine4TextEdit.Size = new System.Drawing.Size(742, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine4TextEdit, optionsSpelling11);
            this.SiteAddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine4TextEdit.TabIndex = 132;
            // 
            // SiteAddressLine3TextEdit
            // 
            this.SiteAddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SiteAddressLine3", true));
            this.SiteAddressLine3TextEdit.Location = new System.Drawing.Point(161, 587);
            this.SiteAddressLine3TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine3TextEdit.Name = "SiteAddressLine3TextEdit";
            this.SiteAddressLine3TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine3TextEdit, true);
            this.SiteAddressLine3TextEdit.Size = new System.Drawing.Size(742, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine3TextEdit, optionsSpelling12);
            this.SiteAddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine3TextEdit.TabIndex = 131;
            // 
            // SiteAddressLine2TextEdit
            // 
            this.SiteAddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SiteAddressLine2", true));
            this.SiteAddressLine2TextEdit.Location = new System.Drawing.Point(161, 563);
            this.SiteAddressLine2TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine2TextEdit.Name = "SiteAddressLine2TextEdit";
            this.SiteAddressLine2TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine2TextEdit, true);
            this.SiteAddressLine2TextEdit.Size = new System.Drawing.Size(742, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine2TextEdit, optionsSpelling13);
            this.SiteAddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine2TextEdit.TabIndex = 130;
            // 
            // SiteAddressLine1TextEdit
            // 
            this.SiteAddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SiteAddressLine1", true));
            this.SiteAddressLine1TextEdit.Location = new System.Drawing.Point(161, 539);
            this.SiteAddressLine1TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine1TextEdit.Name = "SiteAddressLine1TextEdit";
            this.SiteAddressLine1TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine1TextEdit, true);
            this.SiteAddressLine1TextEdit.Size = new System.Drawing.Size(742, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine1TextEdit, optionsSpelling14);
            this.SiteAddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine1TextEdit.TabIndex = 129;
            // 
            // TimesheetNumberTextEdit
            // 
            this.TimesheetNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "TimesheetNumber", true));
            this.TimesheetNumberTextEdit.Location = new System.Drawing.Point(173, 620);
            this.TimesheetNumberTextEdit.MenuManager = this.barManager1;
            this.TimesheetNumberTextEdit.Name = "TimesheetNumberTextEdit";
            this.TimesheetNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TimesheetNumberTextEdit, true);
            this.TimesheetNumberTextEdit.Size = new System.Drawing.Size(282, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TimesheetNumberTextEdit, optionsSpelling15);
            this.TimesheetNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.TimesheetNumberTextEdit.TabIndex = 128;
            // 
            // TimesheetSubmittedCheckEdit
            // 
            this.TimesheetSubmittedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "TimesheetSubmitted", true));
            this.TimesheetSubmittedCheckEdit.Location = new System.Drawing.Point(173, 573);
            this.TimesheetSubmittedCheckEdit.MenuManager = this.barManager1;
            this.TimesheetSubmittedCheckEdit.Name = "TimesheetSubmittedCheckEdit";
            this.TimesheetSubmittedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.TimesheetSubmittedCheckEdit.Properties.ValueChecked = 1;
            this.TimesheetSubmittedCheckEdit.Properties.ValueUnchecked = 0;
            this.TimesheetSubmittedCheckEdit.Size = new System.Drawing.Size(282, 19);
            this.TimesheetSubmittedCheckEdit.StyleController = this.dataLayoutControl1;
            this.TimesheetSubmittedCheckEdit.TabIndex = 126;
            // 
            // TeamInvoiceNumberTextEdit
            // 
            this.TeamInvoiceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "TeamInvoiceNumber", true));
            this.TeamInvoiceNumberTextEdit.Location = new System.Drawing.Point(173, 596);
            this.TeamInvoiceNumberTextEdit.MenuManager = this.barManager1;
            this.TeamInvoiceNumberTextEdit.Name = "TeamInvoiceNumberTextEdit";
            this.TeamInvoiceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TeamInvoiceNumberTextEdit, true);
            this.TeamInvoiceNumberTextEdit.Size = new System.Drawing.Size(282, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TeamInvoiceNumberTextEdit, optionsSpelling16);
            this.TeamInvoiceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.TeamInvoiceNumberTextEdit.TabIndex = 125;
            // 
            // NoAccessAbortedRateSpinEdit
            // 
            this.NoAccessAbortedRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "NoAccessAbortedRate", true));
            this.NoAccessAbortedRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NoAccessAbortedRateSpinEdit.Location = new System.Drawing.Point(609, 413);
            this.NoAccessAbortedRateSpinEdit.MenuManager = this.barManager1;
            this.NoAccessAbortedRateSpinEdit.Name = "NoAccessAbortedRateSpinEdit";
            this.NoAccessAbortedRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.NoAccessAbortedRateSpinEdit.Properties.Mask.EditMask = "c";
            this.NoAccessAbortedRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.NoAccessAbortedRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.NoAccessAbortedRateSpinEdit.Properties.ReadOnly = true;
            this.NoAccessAbortedRateSpinEdit.Size = new System.Drawing.Size(306, 20);
            this.NoAccessAbortedRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.NoAccessAbortedRateSpinEdit.TabIndex = 124;
            this.NoAccessAbortedRateSpinEdit.Validated += new System.EventHandler(this.NoAccessAbortedRateSpinEdit_Validated);
            // 
            // WarningLabel
            // 
            this.WarningLabel.AllowHtmlString = true;
            this.WarningLabel.Appearance.ForeColor = System.Drawing.Color.Red;
            this.WarningLabel.Appearance.Image = global::WoodPlan5.Properties.Resources.attention_16;
            this.WarningLabel.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.WarningLabel.Appearance.Options.UseForeColor = true;
            this.WarningLabel.Appearance.Options.UseImage = true;
            this.WarningLabel.Appearance.Options.UseImageAlign = true;
            this.WarningLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.WarningLabel.Location = new System.Drawing.Point(459, 12);
            this.WarningLabel.Name = "WarningLabel";
            this.WarningLabel.Size = new System.Drawing.Size(468, 20);
            this.WarningLabel.StyleController = this.dataLayoutControl1;
            this.WarningLabel.TabIndex = 123;
            this.WarningLabel.Text = "       <b>Warning:</b> Hours Worked and Sum of Hours from Linked Rates Don\'t Matc" +
    "h!";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(24, 917);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditMinimumHours,
            this.repositoryItemSpinEditMachineCount});
            this.gridControl1.Size = new System.Drawing.Size(891, 200);
            this.gridControl1.TabIndex = 122;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04162GCSnowClearanceCalloutLinkedRatesBindingSource
            // 
            this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource.DataMember = "sp04162_GC_Snow_Clearance_Callout_Linked_Rates";
            this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource.DataSource = this.dataSet_GC_Snow_Core;
            // 
            // dataSet_GC_Snow_Core
            // 
            this.dataSet_GC_Snow_Core.DataSetName = "DataSet_GC_Snow_Core";
            this.dataSet_GC_Snow_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRemarks1,
            this.colSiteName2,
            this.colClientName2,
            this.colSubContractorID2,
            this.colTeamName1,
            this.colTeamAddressLine11,
            this.colCalloutDescription1,
            this.colCallOutDateTime2,
            this.colDayTypeDescription,
            this.colDayTypeID,
            this.colEndDateTime,
            this.colSnowClearanceCallOutID2,
            this.colSnowClearanceCallOutRateID,
            this.colSnowClearanceSiteContractID2,
            this.colStartDateTime,
            this.colTotalCost2,
            this.colMachineCount1,
            this.colMachineCount2,
            this.colMachineCount3,
            this.colMachineCount4,
            this.colRate,
            this.colMachineRate2,
            this.colMachineRate3,
            this.colMachineRate4,
            this.colHours1,
            this.colHours2,
            this.colHours3,
            this.colHours4,
            this.colMinimumHours,
            this.colMinimumHours2,
            this.colMinimumHours3,
            this.colMinimumHours4});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView1.OptionsFilter.AllowMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDayTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDateTime, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            // 
            // colRemarks1
            // 
            this.colRemarks1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colRemarks1.AppearanceHeader.Options.UseFont = true;
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 19;
            this.colRemarks1.Width = 120;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            this.repositoryItemMemoExEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemMemoExEdit1_EditValueChanged);
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 78;
            // 
            // colSubContractorID2
            // 
            this.colSubContractorID2.Caption = "Team ID";
            this.colSubContractorID2.FieldName = "SubContractorID";
            this.colSubContractorID2.Name = "colSubContractorID2";
            this.colSubContractorID2.OptionsColumn.AllowEdit = false;
            this.colSubContractorID2.OptionsColumn.AllowFocus = false;
            this.colSubContractorID2.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName1
            // 
            this.colTeamName1.Caption = "Team Name";
            this.colTeamName1.FieldName = "TeamName";
            this.colTeamName1.Name = "colTeamName1";
            this.colTeamName1.OptionsColumn.AllowEdit = false;
            this.colTeamName1.OptionsColumn.AllowFocus = false;
            this.colTeamName1.OptionsColumn.ReadOnly = true;
            this.colTeamName1.Width = 77;
            // 
            // colTeamAddressLine11
            // 
            this.colTeamAddressLine11.Caption = "Team Address Line 1";
            this.colTeamAddressLine11.FieldName = "TeamAddressLine1";
            this.colTeamAddressLine11.Name = "colTeamAddressLine11";
            this.colTeamAddressLine11.OptionsColumn.AllowEdit = false;
            this.colTeamAddressLine11.OptionsColumn.AllowFocus = false;
            this.colTeamAddressLine11.OptionsColumn.ReadOnly = true;
            this.colTeamAddressLine11.Width = 120;
            // 
            // colCalloutDescription1
            // 
            this.colCalloutDescription1.Caption = "Callout Description";
            this.colCalloutDescription1.FieldName = "CalloutDescription";
            this.colCalloutDescription1.Name = "colCalloutDescription1";
            this.colCalloutDescription1.OptionsColumn.AllowEdit = false;
            this.colCalloutDescription1.OptionsColumn.AllowFocus = false;
            this.colCalloutDescription1.OptionsColumn.ReadOnly = true;
            this.colCalloutDescription1.Width = 100;
            // 
            // colCallOutDateTime2
            // 
            this.colCallOutDateTime2.Caption = "Callout Time";
            this.colCallOutDateTime2.FieldName = "CallOutDateTime";
            this.colCallOutDateTime2.Name = "colCallOutDateTime2";
            this.colCallOutDateTime2.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime2.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime2.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime2.Width = 79;
            // 
            // colDayTypeDescription
            // 
            this.colDayTypeDescription.Caption = "Day Type";
            this.colDayTypeDescription.FieldName = "DayTypeDescription";
            this.colDayTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDayTypeDescription.Name = "colDayTypeDescription";
            this.colDayTypeDescription.OptionsColumn.AllowEdit = false;
            this.colDayTypeDescription.OptionsColumn.AllowFocus = false;
            this.colDayTypeDescription.OptionsColumn.ReadOnly = true;
            this.colDayTypeDescription.Visible = true;
            this.colDayTypeDescription.VisibleIndex = 0;
            this.colDayTypeDescription.Width = 151;
            // 
            // colDayTypeID
            // 
            this.colDayTypeID.Caption = "Day Type ID";
            this.colDayTypeID.FieldName = "DayTypeID";
            this.colDayTypeID.Name = "colDayTypeID";
            this.colDayTypeID.OptionsColumn.AllowEdit = false;
            this.colDayTypeID.OptionsColumn.AllowFocus = false;
            this.colDayTypeID.OptionsColumn.ReadOnly = true;
            this.colDayTypeID.Width = 81;
            // 
            // colEndDateTime
            // 
            this.colEndDateTime.Caption = "End Time";
            this.colEndDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDateTime.FieldName = "EndDateTime";
            this.colEndDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDateTime.Name = "colEndDateTime";
            this.colEndDateTime.OptionsColumn.AllowEdit = false;
            this.colEndDateTime.OptionsColumn.AllowFocus = false;
            this.colEndDateTime.OptionsColumn.ReadOnly = true;
            this.colEndDateTime.Visible = true;
            this.colEndDateTime.VisibleIndex = 2;
            this.colEndDateTime.Width = 71;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "t";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colSnowClearanceCallOutID2
            // 
            this.colSnowClearanceCallOutID2.Caption = "Snow Clearance Callout ID";
            this.colSnowClearanceCallOutID2.FieldName = "SnowClearanceCallOutID";
            this.colSnowClearanceCallOutID2.Name = "colSnowClearanceCallOutID2";
            this.colSnowClearanceCallOutID2.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutID2.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutID2.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutID2.Width = 148;
            // 
            // colSnowClearanceCallOutRateID
            // 
            this.colSnowClearanceCallOutRateID.Caption = "Snow Clearance Callout Rate ID";
            this.colSnowClearanceCallOutRateID.FieldName = "SnowClearanceCallOutRateID";
            this.colSnowClearanceCallOutRateID.Name = "colSnowClearanceCallOutRateID";
            this.colSnowClearanceCallOutRateID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutRateID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutRateID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutRateID.Width = 174;
            // 
            // colSnowClearanceSiteContractID2
            // 
            this.colSnowClearanceSiteContractID2.Caption = "Snow Clearance Contract ID";
            this.colSnowClearanceSiteContractID2.FieldName = "SnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID2.Name = "colSnowClearanceSiteContractID2";
            this.colSnowClearanceSiteContractID2.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSiteContractID2.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSiteContractID2.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSiteContractID2.Width = 157;
            // 
            // colStartDateTime
            // 
            this.colStartDateTime.Caption = "Start Time";
            this.colStartDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDateTime.FieldName = "StartDateTime";
            this.colStartDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDateTime.Name = "colStartDateTime";
            this.colStartDateTime.OptionsColumn.AllowEdit = false;
            this.colStartDateTime.OptionsColumn.AllowFocus = false;
            this.colStartDateTime.OptionsColumn.ReadOnly = true;
            this.colStartDateTime.Visible = true;
            this.colStartDateTime.VisibleIndex = 1;
            this.colStartDateTime.Width = 80;
            // 
            // colTotalCost2
            // 
            this.colTotalCost2.Caption = "Total Cost";
            this.colTotalCost2.ColumnEdit = this.repositoryItemTextEdit1;
            this.colTotalCost2.FieldName = "TotalCost";
            this.colTotalCost2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colTotalCost2.Name = "colTotalCost2";
            this.colTotalCost2.OptionsColumn.AllowEdit = false;
            this.colTotalCost2.OptionsColumn.AllowFocus = false;
            this.colTotalCost2.OptionsColumn.ReadOnly = true;
            this.colTotalCost2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCost", "{0:c2}")});
            this.colTotalCost2.Visible = true;
            this.colTotalCost2.VisibleIndex = 20;
            this.colTotalCost2.Width = 70;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colMachineCount1
            // 
            this.colMachineCount1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMachineCount1.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMachineCount1.AppearanceCell.Options.UseBackColor = true;
            this.colMachineCount1.AppearanceCell.Options.UseForeColor = true;
            this.colMachineCount1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colMachineCount1.AppearanceHeader.Options.UseFont = true;
            this.colMachineCount1.AppearanceHeader.Options.UseTextOptions = true;
            this.colMachineCount1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colMachineCount1.Caption = "Machine Count 1";
            this.colMachineCount1.ColumnEdit = this.repositoryItemSpinEditMachineCount;
            this.colMachineCount1.FieldName = "MachineCount1";
            this.colMachineCount1.Name = "colMachineCount1";
            this.colMachineCount1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MachineCount1", "{0:n0}")});
            this.colMachineCount1.Visible = true;
            this.colMachineCount1.VisibleIndex = 6;
            this.colMachineCount1.Width = 104;
            // 
            // repositoryItemSpinEditMachineCount
            // 
            this.repositoryItemSpinEditMachineCount.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemSpinEditMachineCount.AutoHeight = false;
            this.repositoryItemSpinEditMachineCount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditMachineCount.Mask.EditMask = "n0";
            this.repositoryItemSpinEditMachineCount.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditMachineCount.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.repositoryItemSpinEditMachineCount.Name = "repositoryItemSpinEditMachineCount";
            this.repositoryItemSpinEditMachineCount.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditMachineCount_EditValueChanged);
            this.repositoryItemSpinEditMachineCount.Validating += new System.ComponentModel.CancelEventHandler(this.repositoryItemSpinEditMachineCount_Validating);
            // 
            // colMachineCount2
            // 
            this.colMachineCount2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colMachineCount2.AppearanceHeader.Options.UseFont = true;
            this.colMachineCount2.Caption = "Machine Count 2";
            this.colMachineCount2.ColumnEdit = this.repositoryItemSpinEditMachineCount;
            this.colMachineCount2.FieldName = "MachineCount2";
            this.colMachineCount2.Name = "colMachineCount2";
            this.colMachineCount2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MachineCount2", "{0:n0}")});
            this.colMachineCount2.Visible = true;
            this.colMachineCount2.VisibleIndex = 10;
            this.colMachineCount2.Width = 104;
            // 
            // colMachineCount3
            // 
            this.colMachineCount3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMachineCount3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMachineCount3.AppearanceCell.Options.UseBackColor = true;
            this.colMachineCount3.AppearanceCell.Options.UseForeColor = true;
            this.colMachineCount3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colMachineCount3.AppearanceHeader.Options.UseFont = true;
            this.colMachineCount3.Caption = "Machine Count 3";
            this.colMachineCount3.ColumnEdit = this.repositoryItemSpinEditMachineCount;
            this.colMachineCount3.FieldName = "MachineCount3";
            this.colMachineCount3.Name = "colMachineCount3";
            this.colMachineCount3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MachineCount3", "{0:n0}")});
            this.colMachineCount3.Visible = true;
            this.colMachineCount3.VisibleIndex = 14;
            this.colMachineCount3.Width = 104;
            // 
            // colMachineCount4
            // 
            this.colMachineCount4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colMachineCount4.AppearanceHeader.Options.UseFont = true;
            this.colMachineCount4.Caption = "Machine Count 4";
            this.colMachineCount4.ColumnEdit = this.repositoryItemSpinEditMachineCount;
            this.colMachineCount4.FieldName = "MachineCount4";
            this.colMachineCount4.Name = "colMachineCount4";
            this.colMachineCount4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MachineCount4", "{0:n0}")});
            this.colMachineCount4.Visible = true;
            this.colMachineCount4.VisibleIndex = 18;
            this.colMachineCount4.Width = 104;
            // 
            // colRate
            // 
            this.colRate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colRate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colRate.AppearanceCell.Options.UseBackColor = true;
            this.colRate.AppearanceCell.Options.UseForeColor = true;
            this.colRate.Caption = "Machine Rate 1";
            this.colRate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colRate.FieldName = "Rate";
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowEdit = false;
            this.colRate.OptionsColumn.AllowFocus = false;
            this.colRate.OptionsColumn.ReadOnly = true;
            this.colRate.Visible = true;
            this.colRate.VisibleIndex = 3;
            this.colRate.Width = 85;
            // 
            // colMachineRate2
            // 
            this.colMachineRate2.Caption = "Machine Rate 2";
            this.colMachineRate2.ColumnEdit = this.repositoryItemTextEdit1;
            this.colMachineRate2.FieldName = "MachineRate2";
            this.colMachineRate2.Name = "colMachineRate2";
            this.colMachineRate2.OptionsColumn.AllowEdit = false;
            this.colMachineRate2.OptionsColumn.AllowFocus = false;
            this.colMachineRate2.OptionsColumn.ReadOnly = true;
            this.colMachineRate2.Visible = true;
            this.colMachineRate2.VisibleIndex = 7;
            this.colMachineRate2.Width = 85;
            // 
            // colMachineRate3
            // 
            this.colMachineRate3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMachineRate3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMachineRate3.AppearanceCell.Options.UseBackColor = true;
            this.colMachineRate3.AppearanceCell.Options.UseForeColor = true;
            this.colMachineRate3.Caption = "Machine Rate 3";
            this.colMachineRate3.ColumnEdit = this.repositoryItemTextEdit1;
            this.colMachineRate3.FieldName = "MachineRate3";
            this.colMachineRate3.Name = "colMachineRate3";
            this.colMachineRate3.OptionsColumn.AllowEdit = false;
            this.colMachineRate3.OptionsColumn.AllowFocus = false;
            this.colMachineRate3.OptionsColumn.ReadOnly = true;
            this.colMachineRate3.Visible = true;
            this.colMachineRate3.VisibleIndex = 11;
            this.colMachineRate3.Width = 85;
            // 
            // colMachineRate4
            // 
            this.colMachineRate4.Caption = "Machine Rate 4";
            this.colMachineRate4.ColumnEdit = this.repositoryItemTextEdit1;
            this.colMachineRate4.FieldName = "MachineRate4";
            this.colMachineRate4.Name = "colMachineRate4";
            this.colMachineRate4.OptionsColumn.AllowEdit = false;
            this.colMachineRate4.OptionsColumn.AllowFocus = false;
            this.colMachineRate4.OptionsColumn.ReadOnly = true;
            this.colMachineRate4.Visible = true;
            this.colMachineRate4.VisibleIndex = 15;
            this.colMachineRate4.Width = 85;
            // 
            // colHours1
            // 
            this.colHours1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colHours1.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colHours1.AppearanceCell.Options.UseBackColor = true;
            this.colHours1.AppearanceCell.Options.UseForeColor = true;
            this.colHours1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colHours1.AppearanceHeader.Options.UseFont = true;
            this.colHours1.Caption = "Hours 1";
            this.colHours1.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colHours1.FieldName = "Hours";
            this.colHours1.Name = "colHours1";
            this.colHours1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Hours", "{0:n2}")});
            this.colHours1.Visible = true;
            this.colHours1.VisibleIndex = 5;
            this.colHours1.Width = 55;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            this.repositoryItemSpinEdit1.ValueChanged += new System.EventHandler(this.repositoryItemSpinEdit1_ValueChanged);
            this.repositoryItemSpinEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit1_EditValueChanged);
            // 
            // colHours2
            // 
            this.colHours2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colHours2.AppearanceHeader.Options.UseFont = true;
            this.colHours2.Caption = "Hours 2";
            this.colHours2.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colHours2.FieldName = "Hours2";
            this.colHours2.Name = "colHours2";
            this.colHours2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Hours2", "{0:n2}")});
            this.colHours2.Visible = true;
            this.colHours2.VisibleIndex = 9;
            this.colHours2.Width = 55;
            // 
            // colHours3
            // 
            this.colHours3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colHours3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colHours3.AppearanceCell.Options.UseBackColor = true;
            this.colHours3.AppearanceCell.Options.UseForeColor = true;
            this.colHours3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colHours3.AppearanceHeader.Options.UseFont = true;
            this.colHours3.Caption = "Hours 3";
            this.colHours3.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colHours3.FieldName = "Hours3";
            this.colHours3.Name = "colHours3";
            this.colHours3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Hours3", "{0:n2}")});
            this.colHours3.Visible = true;
            this.colHours3.VisibleIndex = 13;
            this.colHours3.Width = 55;
            // 
            // colHours4
            // 
            this.colHours4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colHours4.AppearanceHeader.Options.UseFont = true;
            this.colHours4.Caption = "Hours 4";
            this.colHours4.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colHours4.FieldName = "Hours4";
            this.colHours4.Name = "colHours4";
            this.colHours4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Hours4", "{0:n2}")});
            this.colHours4.Visible = true;
            this.colHours4.VisibleIndex = 17;
            this.colHours4.Width = 55;
            // 
            // colMinimumHours
            // 
            this.colMinimumHours.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMinimumHours.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMinimumHours.AppearanceCell.Options.UseBackColor = true;
            this.colMinimumHours.AppearanceCell.Options.UseForeColor = true;
            this.colMinimumHours.Caption = "Minimum Hours 1";
            this.colMinimumHours.ColumnEdit = this.repositoryItemTextEditMinimumHours;
            this.colMinimumHours.FieldName = "MinimumHours";
            this.colMinimumHours.Name = "colMinimumHours";
            this.colMinimumHours.OptionsColumn.AllowEdit = false;
            this.colMinimumHours.OptionsColumn.AllowFocus = false;
            this.colMinimumHours.OptionsColumn.ReadOnly = true;
            this.colMinimumHours.Visible = true;
            this.colMinimumHours.VisibleIndex = 4;
            this.colMinimumHours.Width = 90;
            // 
            // repositoryItemTextEditMinimumHours
            // 
            this.repositoryItemTextEditMinimumHours.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemTextEditMinimumHours.AutoHeight = false;
            this.repositoryItemTextEditMinimumHours.Mask.EditMask = "n2";
            this.repositoryItemTextEditMinimumHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMinimumHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMinimumHours.Name = "repositoryItemTextEditMinimumHours";
            // 
            // colMinimumHours2
            // 
            this.colMinimumHours2.Caption = "Minimum Hours 2";
            this.colMinimumHours2.ColumnEdit = this.repositoryItemTextEditMinimumHours;
            this.colMinimumHours2.FieldName = "MinimumHours2";
            this.colMinimumHours2.Name = "colMinimumHours2";
            this.colMinimumHours2.OptionsColumn.AllowEdit = false;
            this.colMinimumHours2.OptionsColumn.AllowFocus = false;
            this.colMinimumHours2.OptionsColumn.ReadOnly = true;
            this.colMinimumHours2.Visible = true;
            this.colMinimumHours2.VisibleIndex = 8;
            this.colMinimumHours2.Width = 91;
            // 
            // colMinimumHours3
            // 
            this.colMinimumHours3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.colMinimumHours3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMinimumHours3.AppearanceCell.Options.UseBackColor = true;
            this.colMinimumHours3.AppearanceCell.Options.UseForeColor = true;
            this.colMinimumHours3.Caption = "Minimum Hours 3";
            this.colMinimumHours3.ColumnEdit = this.repositoryItemTextEditMinimumHours;
            this.colMinimumHours3.FieldName = "MinimumHours3";
            this.colMinimumHours3.Name = "colMinimumHours3";
            this.colMinimumHours3.OptionsColumn.AllowEdit = false;
            this.colMinimumHours3.OptionsColumn.AllowFocus = false;
            this.colMinimumHours3.OptionsColumn.ReadOnly = true;
            this.colMinimumHours3.Visible = true;
            this.colMinimumHours3.VisibleIndex = 12;
            this.colMinimumHours3.Width = 91;
            // 
            // colMinimumHours4
            // 
            this.colMinimumHours4.Caption = "Minimum Hours 4";
            this.colMinimumHours4.ColumnEdit = this.repositoryItemTextEditMinimumHours;
            this.colMinimumHours4.FieldName = "MinimumHours4";
            this.colMinimumHours4.Name = "colMinimumHours4";
            this.colMinimumHours4.OptionsColumn.AllowEdit = false;
            this.colMinimumHours4.OptionsColumn.AllowFocus = false;
            this.colMinimumHours4.OptionsColumn.ReadOnly = true;
            this.colMinimumHours4.Visible = true;
            this.colMinimumHours4.VisibleIndex = 16;
            this.colMinimumHours4.Width = 91;
            // 
            // SubContractorContactedByStaffIDGridLookUpEdit
            // 
            this.SubContractorContactedByStaffIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SubContractorContactedByStaffID", true));
            this.SubContractorContactedByStaffIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SubContractorContactedByStaffIDGridLookUpEdit.Location = new System.Drawing.Point(137, 180);
            this.SubContractorContactedByStaffIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SubContractorContactedByStaffIDGridLookUpEdit.Name = "SubContractorContactedByStaffIDGridLookUpEdit";
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.NullText = "";
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.SubContractorContactedByStaffIDGridLookUpEdit.Size = new System.Drawing.Size(331, 20);
            this.SubContractorContactedByStaffIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorContactedByStaffIDGridLookUpEdit.TabIndex = 17;
            this.SubContractorContactedByStaffIDGridLookUpEdit.TabStop = false;
            // 
            // sp00226StaffListWithBlankBindingSource
            // 
            this.sp00226StaffListWithBlankBindingSource.DataMember = "sp00226_Staff_List_With_Blank";
            this.sp00226StaffListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActive,
            this.colDisplayName,
            this.colEmail,
            this.colForename,
            this.colNetworkID,
            this.colStaffID,
            this.colStaffName,
            this.colSurname,
            this.colUserType});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colStaffID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDisplayName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 51;
            // 
            // colDisplayName
            // 
            this.colDisplayName.Caption = "Surname: Forename";
            this.colDisplayName.FieldName = "DisplayName";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.OptionsColumn.AllowEdit = false;
            this.colDisplayName.OptionsColumn.AllowFocus = false;
            this.colDisplayName.OptionsColumn.ReadOnly = true;
            this.colDisplayName.Visible = true;
            this.colDisplayName.VisibleIndex = 0;
            this.colDisplayName.Width = 225;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowEdit = false;
            this.colEmail.OptionsColumn.AllowFocus = false;
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Width = 192;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Width = 178;
            // 
            // colNetworkID
            // 
            this.colNetworkID.Caption = "Network ID";
            this.colNetworkID.FieldName = "NetworkID";
            this.colNetworkID.Name = "colNetworkID";
            this.colNetworkID.OptionsColumn.AllowEdit = false;
            this.colNetworkID.OptionsColumn.AllowFocus = false;
            this.colNetworkID.OptionsColumn.ReadOnly = true;
            this.colNetworkID.Width = 180;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Width = 231;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Width = 191;
            // 
            // colUserType
            // 
            this.colUserType.Caption = "User Type";
            this.colUserType.FieldName = "UserType";
            this.colUserType.Name = "colUserType";
            this.colUserType.OptionsColumn.AllowEdit = false;
            this.colUserType.OptionsColumn.AllowFocus = false;
            this.colUserType.OptionsColumn.ReadOnly = true;
            this.colUserType.Width = 223;
            // 
            // PaidByStaffIDTextEdit
            // 
            this.PaidByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "PaidByStaffID", true));
            this.PaidByStaffIDTextEdit.Location = new System.Drawing.Point(173, 381);
            this.PaidByStaffIDTextEdit.MenuManager = this.barManager1;
            this.PaidByStaffIDTextEdit.Name = "PaidByStaffIDTextEdit";
            this.PaidByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PaidByStaffIDTextEdit, true);
            this.PaidByStaffIDTextEdit.Size = new System.Drawing.Size(263, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PaidByStaffIDTextEdit, optionsSpelling17);
            this.PaidByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PaidByStaffIDTextEdit.TabIndex = 119;
            // 
            // ClientPOIDTextEdit
            // 
            this.ClientPOIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "ClientPOID", true));
            this.ClientPOIDTextEdit.Location = new System.Drawing.Point(161, 415);
            this.ClientPOIDTextEdit.MenuManager = this.barManager1;
            this.ClientPOIDTextEdit.Name = "ClientPOIDTextEdit";
            this.ClientPOIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPOIDTextEdit, true);
            this.ClientPOIDTextEdit.Size = new System.Drawing.Size(742, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPOIDTextEdit, optionsSpelling18);
            this.ClientPOIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPOIDTextEdit.TabIndex = 117;
            // 
            // RecordedByStaffIDTextEdit
            // 
            this.RecordedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "RecordedByStaffID", true));
            this.RecordedByStaffIDTextEdit.Location = new System.Drawing.Point(161, 398);
            this.RecordedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.RecordedByStaffIDTextEdit.Name = "RecordedByStaffIDTextEdit";
            this.RecordedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RecordedByStaffIDTextEdit, true);
            this.RecordedByStaffIDTextEdit.Size = new System.Drawing.Size(759, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RecordedByStaffIDTextEdit, optionsSpelling19);
            this.RecordedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RecordedByStaffIDTextEdit.TabIndex = 113;
            // 
            // OriginalSubContractorIDTextEdit
            // 
            this.OriginalSubContractorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "OriginalSubContractorID", true));
            this.OriginalSubContractorIDTextEdit.Location = new System.Drawing.Point(161, 398);
            this.OriginalSubContractorIDTextEdit.MenuManager = this.barManager1;
            this.OriginalSubContractorIDTextEdit.Name = "OriginalSubContractorIDTextEdit";
            this.OriginalSubContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OriginalSubContractorIDTextEdit, true);
            this.OriginalSubContractorIDTextEdit.Size = new System.Drawing.Size(759, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OriginalSubContractorIDTextEdit, optionsSpelling20);
            this.OriginalSubContractorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.OriginalSubContractorIDTextEdit.TabIndex = 112;
            // 
            // SubContractorIDTextEdit
            // 
            this.SubContractorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SubContractorID", true));
            this.SubContractorIDTextEdit.Location = new System.Drawing.Point(161, 398);
            this.SubContractorIDTextEdit.MenuManager = this.barManager1;
            this.SubContractorIDTextEdit.Name = "SubContractorIDTextEdit";
            this.SubContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubContractorIDTextEdit, true);
            this.SubContractorIDTextEdit.Size = new System.Drawing.Size(759, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubContractorIDTextEdit, optionsSpelling21);
            this.SubContractorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorIDTextEdit.TabIndex = 111;
            // 
            // SnowClearanceSiteContractIDTextEdit
            // 
            this.SnowClearanceSiteContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SnowClearanceSiteContractID", true));
            this.SnowClearanceSiteContractIDTextEdit.Location = new System.Drawing.Point(180, 398);
            this.SnowClearanceSiteContractIDTextEdit.MenuManager = this.barManager1;
            this.SnowClearanceSiteContractIDTextEdit.Name = "SnowClearanceSiteContractIDTextEdit";
            this.SnowClearanceSiteContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SnowClearanceSiteContractIDTextEdit, true);
            this.SnowClearanceSiteContractIDTextEdit.Size = new System.Drawing.Size(740, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SnowClearanceSiteContractIDTextEdit, optionsSpelling22);
            this.SnowClearanceSiteContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearanceSiteContractIDTextEdit.TabIndex = 110;
            // 
            // ChargeMethodDescriptionTextEdit
            // 
            this.ChargeMethodDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "ChargeMethodDescription", true));
            this.ChargeMethodDescriptionTextEdit.Location = new System.Drawing.Point(173, 573);
            this.ChargeMethodDescriptionTextEdit.MenuManager = this.barManager1;
            this.ChargeMethodDescriptionTextEdit.Name = "ChargeMethodDescriptionTextEdit";
            this.ChargeMethodDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ChargeMethodDescriptionTextEdit, true);
            this.ChargeMethodDescriptionTextEdit.Size = new System.Drawing.Size(140, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ChargeMethodDescriptionTextEdit, optionsSpelling23);
            this.ChargeMethodDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem4.Text = "Client Charge Method - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "This value is Read Only, and set by the selected Site Snow Clearance Contract.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.ChargeMethodDescriptionTextEdit.SuperTip = superToolTip4;
            this.ChargeMethodDescriptionTextEdit.TabIndex = 121;
            // 
            // ClientHowSoonIDGridLookUpEdit
            // 
            this.ClientHowSoonIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "ClientHowSoonID", true));
            this.ClientHowSoonIDGridLookUpEdit.Location = new System.Drawing.Point(149, 272);
            this.ClientHowSoonIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ClientHowSoonIDGridLookUpEdit.Name = "ClientHowSoonIDGridLookUpEdit";
            this.ClientHowSoonIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClientHowSoonIDGridLookUpEdit.Properties.DataSource = this.sp04165GCClientHowSoonTypesWithBlankBindingSource;
            this.ClientHowSoonIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ClientHowSoonIDGridLookUpEdit.Properties.NullText = "";
            this.ClientHowSoonIDGridLookUpEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.ClientHowSoonIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.ClientHowSoonIDGridLookUpEdit.Properties.ValueMember = "Value";
            this.ClientHowSoonIDGridLookUpEdit.Size = new System.Drawing.Size(307, 20);
            this.ClientHowSoonIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ClientHowSoonIDGridLookUpEdit.TabIndex = 120;
            // 
            // sp04165GCClientHowSoonTypesWithBlankBindingSource
            // 
            this.sp04165GCClientHowSoonTypesWithBlankBindingSource.DataMember = "sp04165_GC_Client_How_Soon_Types_With_Blank";
            this.sp04165GCClientHowSoonTypesWithBlankBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colOrder,
            this.colValue});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colValue;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 210;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // DoNotInvoiceClientReasonMemoEdit
            // 
            this.DoNotInvoiceClientReasonMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "DoNotInvoiceClientReason", true));
            this.DoNotInvoiceClientReasonMemoEdit.Location = new System.Drawing.Point(614, 620);
            this.DoNotInvoiceClientReasonMemoEdit.MenuManager = this.barManager1;
            this.DoNotInvoiceClientReasonMemoEdit.Name = "DoNotInvoiceClientReasonMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.DoNotInvoiceClientReasonMemoEdit, true);
            this.DoNotInvoiceClientReasonMemoEdit.Size = new System.Drawing.Size(277, 183);
            this.scSpellChecker.SetSpellCheckerOptions(this.DoNotInvoiceClientReasonMemoEdit, optionsSpelling24);
            this.DoNotInvoiceClientReasonMemoEdit.StyleController = this.dataLayoutControl1;
            this.DoNotInvoiceClientReasonMemoEdit.TabIndex = 79;
            // 
            // DoNotInvoiceClientCheckEdit
            // 
            this.DoNotInvoiceClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "DoNotInvoiceClient", true));
            this.DoNotInvoiceClientCheckEdit.Location = new System.Drawing.Point(614, 597);
            this.DoNotInvoiceClientCheckEdit.MenuManager = this.barManager1;
            this.DoNotInvoiceClientCheckEdit.Name = "DoNotInvoiceClientCheckEdit";
            this.DoNotInvoiceClientCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DoNotInvoiceClientCheckEdit.Properties.ValueChecked = 1;
            this.DoNotInvoiceClientCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotInvoiceClientCheckEdit.Size = new System.Drawing.Size(277, 19);
            this.DoNotInvoiceClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.DoNotInvoiceClientCheckEdit.TabIndex = 78;
            // 
            // DoNotPaySubContractorReasonMemoEdit
            // 
            this.DoNotPaySubContractorReasonMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "DoNotPaySubContractorReason", true));
            this.DoNotPaySubContractorReasonMemoEdit.Location = new System.Drawing.Point(173, 724);
            this.DoNotPaySubContractorReasonMemoEdit.MenuManager = this.barManager1;
            this.DoNotPaySubContractorReasonMemoEdit.Name = "DoNotPaySubContractorReasonMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.DoNotPaySubContractorReasonMemoEdit, true);
            this.DoNotPaySubContractorReasonMemoEdit.Size = new System.Drawing.Size(282, 66);
            this.scSpellChecker.SetSpellCheckerOptions(this.DoNotPaySubContractorReasonMemoEdit, optionsSpelling25);
            this.DoNotPaySubContractorReasonMemoEdit.StyleController = this.dataLayoutControl1;
            this.DoNotPaySubContractorReasonMemoEdit.TabIndex = 77;
            // 
            // DoNotPaySubContractorCheckEdit
            // 
            this.DoNotPaySubContractorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "DoNotPaySubContractor", true));
            this.DoNotPaySubContractorCheckEdit.Location = new System.Drawing.Point(173, 701);
            this.DoNotPaySubContractorCheckEdit.MenuManager = this.barManager1;
            this.DoNotPaySubContractorCheckEdit.Name = "DoNotPaySubContractorCheckEdit";
            this.DoNotPaySubContractorCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DoNotPaySubContractorCheckEdit.Properties.ValueChecked = 1;
            this.DoNotPaySubContractorCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotPaySubContractorCheckEdit.Size = new System.Drawing.Size(282, 19);
            this.DoNotPaySubContractorCheckEdit.StyleController = this.dataLayoutControl1;
            this.DoNotPaySubContractorCheckEdit.TabIndex = 76;
            // 
            // PaidByStaffNameTextEdit
            // 
            this.PaidByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "PaidByStaffName", true));
            this.PaidByStaffNameTextEdit.Location = new System.Drawing.Point(173, 677);
            this.PaidByStaffNameTextEdit.MenuManager = this.barManager1;
            this.PaidByStaffNameTextEdit.Name = "PaidByStaffNameTextEdit";
            this.PaidByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PaidByStaffNameTextEdit, true);
            this.PaidByStaffNameTextEdit.Size = new System.Drawing.Size(282, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PaidByStaffNameTextEdit, optionsSpelling26);
            this.PaidByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PaidByStaffNameTextEdit.TabIndex = 95;
            // 
            // ClientInvoiceNumberTextEdit
            // 
            this.ClientInvoiceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "ClientInvoiceNumber", true));
            this.ClientInvoiceNumberTextEdit.Location = new System.Drawing.Point(614, 573);
            this.ClientInvoiceNumberTextEdit.MenuManager = this.barManager1;
            this.ClientInvoiceNumberTextEdit.Name = "ClientInvoiceNumberTextEdit";
            this.ClientInvoiceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientInvoiceNumberTextEdit, true);
            this.ClientInvoiceNumberTextEdit.Size = new System.Drawing.Size(277, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientInvoiceNumberTextEdit, optionsSpelling27);
            this.ClientInvoiceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientInvoiceNumberTextEdit.TabIndex = 118;
            // 
            // NonStandardSellCheckEdit
            // 
            this.NonStandardSellCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "NonStandardSell", true));
            this.NonStandardSellCheckEdit.Location = new System.Drawing.Point(751, 644);
            this.NonStandardSellCheckEdit.MenuManager = this.barManager1;
            this.NonStandardSellCheckEdit.Name = "NonStandardSellCheckEdit";
            this.NonStandardSellCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NonStandardSellCheckEdit.Properties.ValueChecked = 1;
            this.NonStandardSellCheckEdit.Properties.ValueUnchecked = 0;
            this.NonStandardSellCheckEdit.Size = new System.Drawing.Size(140, 19);
            this.NonStandardSellCheckEdit.StyleController = this.dataLayoutControl1;
            this.NonStandardSellCheckEdit.TabIndex = 101;
            this.NonStandardSellCheckEdit.EditValueChanged += new System.EventHandler(this.NonStandardSellCheckEdit_EditValueChanged);
            this.NonStandardSellCheckEdit.Validated += new System.EventHandler(this.NonStandardSellCheckEdit_Validated);
            // 
            // NonStandardCostCheckEdit
            // 
            this.NonStandardCostCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "NonStandardCost", true));
            this.NonStandardCostCheckEdit.Location = new System.Drawing.Point(751, 621);
            this.NonStandardCostCheckEdit.MenuManager = this.barManager1;
            this.NonStandardCostCheckEdit.Name = "NonStandardCostCheckEdit";
            this.NonStandardCostCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NonStandardCostCheckEdit.Properties.ValueChecked = 1;
            this.NonStandardCostCheckEdit.Properties.ValueUnchecked = 0;
            this.NonStandardCostCheckEdit.Size = new System.Drawing.Size(140, 19);
            this.NonStandardCostCheckEdit.StyleController = this.dataLayoutControl1;
            this.NonStandardCostCheckEdit.TabIndex = 100;
            this.NonStandardCostCheckEdit.EditValueChanged += new System.EventHandler(this.NonStandardCostCheckEdit_EditValueChanged);
            this.NonStandardCostCheckEdit.Validated += new System.EventHandler(this.NonStandardCostCheckEdit_Validated);
            // 
            // TotalSellSpinEdit
            // 
            this.TotalSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "TotalSell", true));
            this.TotalSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalSellSpinEdit.Location = new System.Drawing.Point(751, 597);
            this.TotalSellSpinEdit.MenuManager = this.barManager1;
            this.TotalSellSpinEdit.Name = "TotalSellSpinEdit";
            this.TotalSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalSellSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSellSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TotalSellSpinEdit.Properties.ReadOnly = true;
            this.TotalSellSpinEdit.Size = new System.Drawing.Size(140, 20);
            this.TotalSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalSellSpinEdit.TabIndex = 69;
            // 
            // SubContractorPaidCheckEdit
            // 
            this.SubContractorPaidCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SubContractorPaid", true));
            this.SubContractorPaidCheckEdit.Location = new System.Drawing.Point(173, 654);
            this.SubContractorPaidCheckEdit.MenuManager = this.barManager1;
            this.SubContractorPaidCheckEdit.Name = "SubContractorPaidCheckEdit";
            this.SubContractorPaidCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SubContractorPaidCheckEdit.Properties.ValueChecked = 1;
            this.SubContractorPaidCheckEdit.Properties.ValueUnchecked = 0;
            this.SubContractorPaidCheckEdit.Size = new System.Drawing.Size(282, 19);
            this.SubContractorPaidCheckEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorPaidCheckEdit.TabIndex = 71;
            // 
            // TotalCostSpinEdit
            // 
            this.TotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "TotalCost", true));
            this.TotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalCostSpinEdit.Location = new System.Drawing.Point(751, 573);
            this.TotalCostSpinEdit.MenuManager = this.barManager1;
            this.TotalCostSpinEdit.Name = "TotalCostSpinEdit";
            this.TotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TotalCostSpinEdit.Properties.ReadOnly = true;
            this.TotalCostSpinEdit.Size = new System.Drawing.Size(140, 20);
            this.TotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostSpinEdit.TabIndex = 68;
            // 
            // OtherSellSpinEdit
            // 
            this.OtherSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "OtherSell", true));
            this.OtherSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OtherSellSpinEdit.Location = new System.Drawing.Point(472, 597);
            this.OtherSellSpinEdit.MenuManager = this.barManager1;
            this.OtherSellSpinEdit.Name = "OtherSellSpinEdit";
            this.OtherSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OtherSellSpinEdit.Properties.Mask.EditMask = "c";
            this.OtherSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OtherSellSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.OtherSellSpinEdit.Properties.ReadOnly = true;
            this.OtherSellSpinEdit.Size = new System.Drawing.Size(120, 20);
            this.OtherSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.OtherSellSpinEdit.TabIndex = 66;
            // 
            // OtherCostSpinEdit
            // 
            this.OtherCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "OtherCost", true));
            this.OtherCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OtherCostSpinEdit.Location = new System.Drawing.Point(472, 573);
            this.OtherCostSpinEdit.MenuManager = this.barManager1;
            this.OtherCostSpinEdit.Name = "OtherCostSpinEdit";
            this.OtherCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OtherCostSpinEdit.Properties.Mask.EditMask = "c";
            this.OtherCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OtherCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.OtherCostSpinEdit.Properties.ReadOnly = true;
            this.OtherCostSpinEdit.Size = new System.Drawing.Size(120, 20);
            this.OtherCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.OtherCostSpinEdit.TabIndex = 65;
            // 
            // LabourSellSpinEdit
            // 
            this.LabourSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "LabourSell", true));
            this.LabourSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LabourSellSpinEdit.Location = new System.Drawing.Point(173, 645);
            this.LabourSellSpinEdit.MenuManager = this.barManager1;
            this.LabourSellSpinEdit.Name = "LabourSellSpinEdit";
            this.LabourSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LabourSellSpinEdit.Properties.Mask.EditMask = "c";
            this.LabourSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LabourSellSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.LabourSellSpinEdit.Properties.ReadOnly = true;
            this.LabourSellSpinEdit.Size = new System.Drawing.Size(140, 20);
            this.LabourSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.LabourSellSpinEdit.TabIndex = 93;
            this.LabourSellSpinEdit.EditValueChanged += new System.EventHandler(this.LabourSellSpinEdit_EditValueChanged);
            this.LabourSellSpinEdit.Validated += new System.EventHandler(this.LabourSellSpinEdit_Validated);
            // 
            // LabourCostSpinEdit
            // 
            this.LabourCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "LabourCost", true));
            this.LabourCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LabourCostSpinEdit.Location = new System.Drawing.Point(173, 621);
            this.LabourCostSpinEdit.MenuManager = this.barManager1;
            this.LabourCostSpinEdit.Name = "LabourCostSpinEdit";
            this.LabourCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LabourCostSpinEdit.Properties.Mask.EditMask = "c";
            this.LabourCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LabourCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.LabourCostSpinEdit.Properties.ReadOnly = true;
            this.LabourCostSpinEdit.Size = new System.Drawing.Size(140, 20);
            this.LabourCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.LabourCostSpinEdit.TabIndex = 62;
            this.LabourCostSpinEdit.EditValueChanged += new System.EventHandler(this.LabourCostSpinEdit_EditValueChanged);
            this.LabourCostSpinEdit.Validated += new System.EventHandler(this.LabourCostSpinEdit_Validated);
            // 
            // LabourVatRateSpinEdit
            // 
            this.LabourVatRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "LabourVatRate", true));
            this.LabourVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LabourVatRateSpinEdit.Location = new System.Drawing.Point(173, 597);
            this.LabourVatRateSpinEdit.MenuManager = this.barManager1;
            this.LabourVatRateSpinEdit.Name = "LabourVatRateSpinEdit";
            this.LabourVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LabourVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.LabourVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LabourVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.LabourVatRateSpinEdit.Size = new System.Drawing.Size(140, 20);
            this.LabourVatRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.LabourVatRateSpinEdit.TabIndex = 64;
            // 
            // AbortedReasonMemoEdit
            // 
            this.AbortedReasonMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "AbortedReason", true));
            this.AbortedReasonMemoEdit.Location = new System.Drawing.Point(36, 539);
            this.AbortedReasonMemoEdit.MenuManager = this.barManager1;
            this.AbortedReasonMemoEdit.Name = "AbortedReasonMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbortedReasonMemoEdit, true);
            this.AbortedReasonMemoEdit.Size = new System.Drawing.Size(867, 306);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbortedReasonMemoEdit, optionsSpelling28);
            this.AbortedReasonMemoEdit.StyleController = this.dataLayoutControl1;
            this.AbortedReasonMemoEdit.TabIndex = 116;
            // 
            // AccessCommentsMemoEdit
            // 
            this.AccessCommentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "AccessComments", true));
            this.AccessCommentsMemoEdit.Location = new System.Drawing.Point(36, 539);
            this.AccessCommentsMemoEdit.MenuManager = this.barManager1;
            this.AccessCommentsMemoEdit.Name = "AccessCommentsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccessCommentsMemoEdit, true);
            this.AccessCommentsMemoEdit.Size = new System.Drawing.Size(867, 306);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccessCommentsMemoEdit, optionsSpelling29);
            this.AccessCommentsMemoEdit.StyleController = this.dataLayoutControl1;
            this.AccessCommentsMemoEdit.TabIndex = 115;
            // 
            // SubContractorETADateEdit
            // 
            this.SubContractorETADateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SubContractorETA", true));
            this.SubContractorETADateEdit.EditValue = null;
            this.SubContractorETADateEdit.Location = new System.Drawing.Point(149, 296);
            this.SubContractorETADateEdit.MenuManager = this.barManager1;
            this.SubContractorETADateEdit.Name = "SubContractorETADateEdit";
            this.SubContractorETADateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubContractorETADateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SubContractorETADateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.SubContractorETADateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SubContractorETADateEdit.Size = new System.Drawing.Size(307, 20);
            this.SubContractorETADateEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorETADateEdit.TabIndex = 114;
            // 
            // HoursWorkedSpinEdit
            // 
            this.HoursWorkedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "HoursWorked", true));
            this.HoursWorkedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.HoursWorkedSpinEdit.Location = new System.Drawing.Point(149, 368);
            this.HoursWorkedSpinEdit.MenuManager = this.barManager1;
            this.HoursWorkedSpinEdit.Name = "HoursWorkedSpinEdit";
            this.HoursWorkedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.HoursWorkedSpinEdit.Properties.Mask.EditMask = "f";
            this.HoursWorkedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HoursWorkedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.HoursWorkedSpinEdit.Size = new System.Drawing.Size(307, 20);
            this.HoursWorkedSpinEdit.StyleController = this.dataLayoutControl1;
            this.HoursWorkedSpinEdit.TabIndex = 59;
            // 
            // CompletedTimeDateEdit
            // 
            this.CompletedTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "CompletedTime", true));
            this.CompletedTimeDateEdit.EditValue = null;
            this.CompletedTimeDateEdit.Location = new System.Drawing.Point(149, 344);
            this.CompletedTimeDateEdit.MenuManager = this.barManager1;
            this.CompletedTimeDateEdit.Name = "CompletedTimeDateEdit";
            this.CompletedTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CompletedTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CompletedTimeDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.CompletedTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CompletedTimeDateEdit.Size = new System.Drawing.Size(307, 20);
            this.CompletedTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.CompletedTimeDateEdit.TabIndex = 45;
            this.CompletedTimeDateEdit.EditValueChanged += new System.EventHandler(this.CompletedTimeDateEdit_EditValueChanged);
            this.CompletedTimeDateEdit.Validated += new System.EventHandler(this.CompletedTimeDateEdit_Validated);
            // 
            // StartTimeDateEdit
            // 
            this.StartTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "StartTime", true));
            this.StartTimeDateEdit.EditValue = null;
            this.StartTimeDateEdit.Location = new System.Drawing.Point(149, 320);
            this.StartTimeDateEdit.MenuManager = this.barManager1;
            this.StartTimeDateEdit.Name = "StartTimeDateEdit";
            this.StartTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StartTimeDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.StartTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartTimeDateEdit.Size = new System.Drawing.Size(307, 20);
            this.StartTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.StartTimeDateEdit.TabIndex = 109;
            this.StartTimeDateEdit.EditValueChanged += new System.EventHandler(this.StartTimeDateEdit_EditValueChanged);
            this.StartTimeDateEdit.Validated += new System.EventHandler(this.StartTimeDateEdit_Validated);
            // 
            // GCPONumberSuffixTextEdit
            // 
            this.GCPONumberSuffixTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "GCPONumberSuffix", true));
            this.GCPONumberSuffixTextEdit.Location = new System.Drawing.Point(597, 108);
            this.GCPONumberSuffixTextEdit.MenuManager = this.barManager1;
            this.GCPONumberSuffixTextEdit.Name = "GCPONumberSuffixTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.GCPONumberSuffixTextEdit, true);
            this.GCPONumberSuffixTextEdit.Size = new System.Drawing.Size(330, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GCPONumberSuffixTextEdit, optionsSpelling30);
            this.GCPONumberSuffixTextEdit.StyleController = this.dataLayoutControl1;
            this.GCPONumberSuffixTextEdit.TabIndex = 108;
            // 
            // SnowClearanceCallOutIDTextEdit
            // 
            this.SnowClearanceCallOutIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SnowClearanceCallOutID", true));
            this.SnowClearanceCallOutIDTextEdit.Location = new System.Drawing.Point(137, 108);
            this.SnowClearanceCallOutIDTextEdit.MenuManager = this.barManager1;
            this.SnowClearanceCallOutIDTextEdit.Name = "SnowClearanceCallOutIDTextEdit";
            this.SnowClearanceCallOutIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SnowClearanceCallOutIDTextEdit, true);
            this.SnowClearanceCallOutIDTextEdit.Size = new System.Drawing.Size(331, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SnowClearanceCallOutIDTextEdit, optionsSpelling31);
            this.SnowClearanceCallOutIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearanceCallOutIDTextEdit.TabIndex = 107;
            // 
            // CallOutDateTimeDateEdit
            // 
            this.CallOutDateTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "CallOutDateTime", true));
            this.CallOutDateTimeDateEdit.EditValue = null;
            this.CallOutDateTimeDateEdit.Location = new System.Drawing.Point(149, 248);
            this.CallOutDateTimeDateEdit.MenuManager = this.barManager1;
            this.CallOutDateTimeDateEdit.Name = "CallOutDateTimeDateEdit";
            this.CallOutDateTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CallOutDateTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CallOutDateTimeDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.CallOutDateTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CallOutDateTimeDateEdit.Properties.ReadOnly = true;
            this.CallOutDateTimeDateEdit.Size = new System.Drawing.Size(307, 20);
            this.CallOutDateTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.CallOutDateTimeDateEdit.TabIndex = 106;
            // 
            // RecordedByNameTextEdit
            // 
            this.RecordedByNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "RecordedByName", true));
            this.RecordedByNameTextEdit.Location = new System.Drawing.Point(137, 156);
            this.RecordedByNameTextEdit.MenuManager = this.barManager1;
            this.RecordedByNameTextEdit.Name = "RecordedByNameTextEdit";
            this.RecordedByNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RecordedByNameTextEdit, true);
            this.RecordedByNameTextEdit.Size = new System.Drawing.Size(331, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RecordedByNameTextEdit, optionsSpelling32);
            this.RecordedByNameTextEdit.StyleController = this.dataLayoutControl1;
            this.RecordedByNameTextEdit.TabIndex = 105;
            // 
            // NoAccessCheckEdit
            // 
            this.NoAccessCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "NoAccess", true));
            this.NoAccessCheckEdit.Location = new System.Drawing.Point(609, 390);
            this.NoAccessCheckEdit.MenuManager = this.barManager1;
            this.NoAccessCheckEdit.Name = "NoAccessCheckEdit";
            this.NoAccessCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NoAccessCheckEdit.Properties.ValueChecked = 1;
            this.NoAccessCheckEdit.Properties.ValueUnchecked = 0;
            this.NoAccessCheckEdit.Size = new System.Drawing.Size(88, 19);
            this.NoAccessCheckEdit.StyleController = this.dataLayoutControl1;
            this.NoAccessCheckEdit.TabIndex = 104;
            this.NoAccessCheckEdit.Validated += new System.EventHandler(this.NoAccessCheckEdit_Validated);
            // 
            // JobStatusIDGridLookUpEdit
            // 
            this.JobStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "JobStatusID", true));
            this.JobStatusIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.JobStatusIDGridLookUpEdit.Location = new System.Drawing.Point(597, 132);
            this.JobStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.JobStatusIDGridLookUpEdit.Name = "JobStatusIDGridLookUpEdit";
            this.JobStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.JobStatusIDGridLookUpEdit.Properties.DataSource = this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource;
            this.JobStatusIDGridLookUpEdit.Properties.DisplayMember = "CalloutStatusDescription";
            this.JobStatusIDGridLookUpEdit.Properties.NullText = "";
            this.JobStatusIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.JobStatusIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.JobStatusIDGridLookUpEdit.Properties.ValueMember = "CalloutStatusID";
            this.JobStatusIDGridLookUpEdit.Size = new System.Drawing.Size(330, 20);
            this.JobStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.JobStatusIDGridLookUpEdit.TabIndex = 103;
            this.JobStatusIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.JobStatusIDGridLookUpEdit_EditValueChanged);
            this.JobStatusIDGridLookUpEdit.Enter += new System.EventHandler(this.JobStatusIDGridLookUpEdit_Enter);
            this.JobStatusIDGridLookUpEdit.Leave += new System.EventHandler(this.JobStatusIDGridLookUpEdit_Leave);
            this.JobStatusIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobStatusIDGridLookUpEdit_Validating);
            // 
            // sp04085GCGrittingCalloutStatusListWithBlankBindingSource
            // 
            this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource.DataMember = "sp04085_GC_Gritting_Callout_Status_List_With_Blank";
            this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCalloutStatusDescription,
            this.colCalloutStatusID,
            this.colCalloutStatusOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colCalloutStatusID;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCalloutStatusOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCalloutStatusDescription
            // 
            this.colCalloutStatusDescription.Caption = "Callout Status";
            this.colCalloutStatusDescription.FieldName = "CalloutStatusDescription";
            this.colCalloutStatusDescription.Name = "colCalloutStatusDescription";
            this.colCalloutStatusDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusDescription.Visible = true;
            this.colCalloutStatusDescription.VisibleIndex = 0;
            this.colCalloutStatusDescription.Width = 271;
            // 
            // colCalloutStatusOrder
            // 
            this.colCalloutStatusOrder.Caption = "Status Order";
            this.colCalloutStatusOrder.FieldName = "CalloutStatusOrder";
            this.colCalloutStatusOrder.Name = "colCalloutStatusOrder";
            this.colCalloutStatusOrder.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusOrder.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusOrder.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusOrder.Width = 97;
            // 
            // ClientPONumberTextEdit
            // 
            this.ClientPONumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "ClientPONumber", true));
            this.ClientPONumberTextEdit.Location = new System.Drawing.Point(597, 84);
            this.ClientPONumberTextEdit.MenuManager = this.barManager1;
            this.ClientPONumberTextEdit.Name = "ClientPONumberTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPONumberTextEdit, true);
            this.ClientPONumberTextEdit.Size = new System.Drawing.Size(330, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPONumberTextEdit, optionsSpelling33);
            this.ClientPONumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPONumberTextEdit.TabIndex = 102;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(597, 36);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(330, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling34);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 32;
            // 
            // VisitAbortedCheckEdit
            // 
            this.VisitAbortedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "VisitAborted", true));
            this.VisitAbortedCheckEdit.Location = new System.Drawing.Point(826, 390);
            this.VisitAbortedCheckEdit.MenuManager = this.barManager1;
            this.VisitAbortedCheckEdit.Name = "VisitAbortedCheckEdit";
            this.VisitAbortedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.VisitAbortedCheckEdit.Properties.ValueChecked = 1;
            this.VisitAbortedCheckEdit.Properties.ValueUnchecked = 0;
            this.VisitAbortedCheckEdit.Size = new System.Drawing.Size(89, 19);
            this.VisitAbortedCheckEdit.StyleController = this.dataLayoutControl1;
            this.VisitAbortedCheckEdit.TabIndex = 100;
            this.VisitAbortedCheckEdit.Validated += new System.EventHandler(this.VisitAbortedCheckEdit_Validated);
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "Reactive", true));
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(137, 132);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(331, 19);
            this.ReactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCheckEdit.TabIndex = 99;
            // 
            // ClientPOIDDescriptionButtonEdit
            // 
            this.ClientPOIDDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "ClientPOIDDescription", true));
            this.ClientPOIDDescriptionButtonEdit.Location = new System.Drawing.Point(137, 84);
            this.ClientPOIDDescriptionButtonEdit.MenuManager = this.barManager1;
            this.ClientPOIDDescriptionButtonEdit.Name = "ClientPOIDDescriptionButtonEdit";
            this.ClientPOIDDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientPOIDDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientPOIDDescriptionButtonEdit.Size = new System.Drawing.Size(331, 20);
            this.ClientPOIDDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientPOIDDescriptionButtonEdit.TabIndex = 98;
            this.ClientPOIDDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientPOIDDescriptionButtonEdit_ButtonClick);
            // 
            // SubContractorNameButtonEdit
            // 
            this.SubContractorNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SubContractorName", true));
            this.SubContractorNameButtonEdit.Location = new System.Drawing.Point(137, 60);
            this.SubContractorNameButtonEdit.MenuManager = this.barManager1;
            this.SubContractorNameButtonEdit.Name = "SubContractorNameButtonEdit";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Text = "Choose Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click to open the <b>Select Gritting Team</b> screen where the Gritting Team for " +
    "this callout can be selected.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Text = "View Button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click to view the <b>linked Gritting Team</b> details.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.SubContractorNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "choose", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "view", superToolTip6, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SubContractorNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SubContractorNameButtonEdit.Size = new System.Drawing.Size(331, 20);
            this.SubContractorNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorNameButtonEdit.TabIndex = 35;
            this.SubContractorNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SubContractorNameButtonEdit_ButtonClick);
            // 
            // SiteNameButtonEdit
            // 
            this.SiteNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "SiteName", true));
            this.SiteNameButtonEdit.Location = new System.Drawing.Point(137, 36);
            this.SiteNameButtonEdit.MenuManager = this.barManager1;
            this.SiteNameButtonEdit.Name = "SiteNameButtonEdit";
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Text = "Choose Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click to open the <b>Select Site Gritting Contract</b> screen from where the Site" +
    " and Client for this gritting callout can be specified.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Text = "View Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click to <B>View</b> the linked <b>Site Gritting Contract</b>.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.SiteNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "choose", superToolTip7, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "view", superToolTip8, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SiteNameButtonEdit.Size = new System.Drawing.Size(331, 20);
            this.SiteNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameButtonEdit.TabIndex = 33;
            this.SiteNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteNameButtonEdit_ButtonClick);
            this.SiteNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteNameButtonEdit_Validating);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04163GCSnowCalloutEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(138, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 9;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(161, 539);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(742, 150);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling35);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 8;
            // 
            // OriginalSubContractorNameButtonEdit
            // 
            this.OriginalSubContractorNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04163GCSnowCalloutEditBindingSource, "OriginalSubContractorName", true));
            this.OriginalSubContractorNameButtonEdit.Location = new System.Drawing.Point(597, 60);
            this.OriginalSubContractorNameButtonEdit.MenuManager = this.barManager1;
            this.OriginalSubContractorNameButtonEdit.Name = "OriginalSubContractorNameButtonEdit";
            this.OriginalSubContractorNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.OriginalSubContractorNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.OriginalSubContractorNameButtonEdit.Size = new System.Drawing.Size(330, 20);
            this.OriginalSubContractorNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.OriginalSubContractorNameButtonEdit.TabIndex = 101;
            this.OriginalSubContractorNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.OriginalSubContractorNameButtonEdit_ButtonClick);
            // 
            // ItemFor
            // 
            this.ItemFor.Control = this.SnowClearanceSiteContractIDTextEdit;
            this.ItemFor.CustomizationFormText = "Snow Clearance Contract ID:";
            this.ItemFor.Location = new System.Drawing.Point(0, 0);
            this.ItemFor.Name = "ItemFor";
            this.ItemFor.Size = new System.Drawing.Size(888, 24);
            this.ItemFor.Text = "Snow Clearance Contract ID:";
            this.ItemFor.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSubContractorID
            // 
            this.ItemForSubContractorID.Control = this.SubContractorIDTextEdit;
            this.ItemForSubContractorID.CustomizationFormText = "Team ID:";
            this.ItemForSubContractorID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSubContractorID.Name = "ItemForSubContractorID";
            this.ItemForSubContractorID.Size = new System.Drawing.Size(888, 24);
            this.ItemForSubContractorID.Text = "Team ID:";
            this.ItemForSubContractorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForOriginalSubContractorID
            // 
            this.ItemForOriginalSubContractorID.Control = this.OriginalSubContractorIDTextEdit;
            this.ItemForOriginalSubContractorID.CustomizationFormText = "Original Team ID:";
            this.ItemForOriginalSubContractorID.Location = new System.Drawing.Point(0, 0);
            this.ItemForOriginalSubContractorID.Name = "ItemForOriginalSubContractorID";
            this.ItemForOriginalSubContractorID.Size = new System.Drawing.Size(888, 24);
            this.ItemForOriginalSubContractorID.Text = "Original Team ID:";
            this.ItemForOriginalSubContractorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForRecordedByStaffID
            // 
            this.ItemForRecordedByStaffID.Control = this.RecordedByStaffIDTextEdit;
            this.ItemForRecordedByStaffID.CustomizationFormText = "Recorded By Staff ID:";
            this.ItemForRecordedByStaffID.Location = new System.Drawing.Point(0, 0);
            this.ItemForRecordedByStaffID.Name = "ItemForRecordedByStaffID";
            this.ItemForRecordedByStaffID.Size = new System.Drawing.Size(888, 24);
            this.ItemForRecordedByStaffID.Text = "Recorded By Staff ID:";
            this.ItemForRecordedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientPOID
            // 
            this.ItemForClientPOID.Control = this.ClientPOIDTextEdit;
            this.ItemForClientPOID.CustomizationFormText = "Client PO ID:";
            this.ItemForClientPOID.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientPOID.Name = "ItemForClientPOID";
            this.ItemForClientPOID.Size = new System.Drawing.Size(871, 24);
            this.ItemForClientPOID.Text = "Client PO ID:";
            this.ItemForClientPOID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPaidByStaffID
            // 
            this.ItemForPaidByStaffID.Control = this.PaidByStaffIDTextEdit;
            this.ItemForPaidByStaffID.CustomizationFormText = "Paid By Staff ID:";
            this.ItemForPaidByStaffID.Location = new System.Drawing.Point(0, 47);
            this.ItemForPaidByStaffID.Name = "ItemForPaidByStaffID";
            this.ItemForPaidByStaffID.Size = new System.Drawing.Size(392, 47);
            this.ItemForPaidByStaffID.Text = "Paid By Staff ID:";
            this.ItemForPaidByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForRecordedBySubContractorID
            // 
            this.ItemForRecordedBySubContractorID.Control = this.RecordedBySubContractorIDTextEdit;
            this.ItemForRecordedBySubContractorID.CustomizationFormText = "Recorded By Team ID:";
            this.ItemForRecordedBySubContractorID.Location = new System.Drawing.Point(0, 192);
            this.ItemForRecordedBySubContractorID.Name = "ItemForRecordedBySubContractorID";
            this.ItemForRecordedBySubContractorID.Size = new System.Drawing.Size(919, 24);
            this.ItemForRecordedBySubContractorID.Text = "Recorded By Team ID:";
            this.ItemForRecordedBySubContractorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(939, 1151);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.emptySpaceItem1,
            this.ItemForSiteName,
            this.ItemForSubContractorName,
            this.ItemForClientPOIDDescription,
            this.ItemForClientName,
            this.ItemForOriginalSubContractorName,
            this.ItemForClientPONumber,
            this.ItemForJobStatusID,
            this.ItemForRecordedByName,
            this.layoutControlGroup5,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItem1,
            this.emptySpaceItem12,
            this.ItemForSubContractorContactedByStaffIDGridLookUpEdit,
            this.emptySpaceItem2,
            this.layoutControlGroup15,
            this.emptySpaceItem13,
            this.ItemForWarningLabel,
            this.ItemForSnowClearanceCallOutID,
            this.ItemForGCPONumberSuffix,
            this.ItemForReactive,
            this.layoutControlGroup17,
            this.ItemForSiteManagerName,
            this.layoutControlGroup18,
            this.ItemForCompletedOnPDA,
            this.ItemForNoOneToSign,
            this.ItemForSigned});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(919, 1131);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 457);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(919, 404);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(895, 358);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup12,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.layoutControlGroup16,
            this.layoutControlGroup19,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Costs";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup10,
            this.splitterItem1,
            this.layoutControlGroup11,
            this.splitterItem2,
            this.emptySpaceItem8});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(871, 310);
            this.layoutControlGroup6.Text = "Costs";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Labour";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLabourVatRate,
            this.ItemForLabourCost,
            this.ItemForLabourSell,
            this.emptySpaceItem5,
            this.ItemForChargeMethodDescription});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(293, 236);
            this.layoutControlGroup9.Text = "Labour";
            // 
            // ItemForLabourVatRate
            // 
            this.ItemForLabourVatRate.Control = this.LabourVatRateSpinEdit;
            this.ItemForLabourVatRate.CustomizationFormText = "Labour VAT Rate:";
            this.ItemForLabourVatRate.Location = new System.Drawing.Point(0, 24);
            this.ItemForLabourVatRate.Name = "ItemForLabourVatRate";
            this.ItemForLabourVatRate.Size = new System.Drawing.Size(269, 24);
            this.ItemForLabourVatRate.Text = "Labour VAT Rate:";
            this.ItemForLabourVatRate.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForLabourCost
            // 
            this.ItemForLabourCost.Control = this.LabourCostSpinEdit;
            this.ItemForLabourCost.CustomizationFormText = "Labour Cost:";
            this.ItemForLabourCost.Location = new System.Drawing.Point(0, 48);
            this.ItemForLabourCost.Name = "ItemForLabourCost";
            this.ItemForLabourCost.Size = new System.Drawing.Size(269, 24);
            this.ItemForLabourCost.Text = "Labour Cost:";
            this.ItemForLabourCost.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForLabourSell
            // 
            this.ItemForLabourSell.Control = this.LabourSellSpinEdit;
            this.ItemForLabourSell.CustomizationFormText = "Labour Sell:";
            this.ItemForLabourSell.Location = new System.Drawing.Point(0, 72);
            this.ItemForLabourSell.Name = "ItemForLabourSell";
            this.ItemForLabourSell.Size = new System.Drawing.Size(269, 24);
            this.ItemForLabourSell.Text = "Labour Sell:";
            this.ItemForLabourSell.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(269, 94);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForChargeMethodDescription
            // 
            this.ItemForChargeMethodDescription.Control = this.ChargeMethodDescriptionTextEdit;
            this.ItemForChargeMethodDescription.CustomizationFormText = "Client Charge Method:";
            this.ItemForChargeMethodDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForChargeMethodDescription.Name = "ItemForChargeMethodDescription";
            this.ItemForChargeMethodDescription.Size = new System.Drawing.Size(269, 24);
            this.ItemForChargeMethodDescription.Text = "Client Charge Method:";
            this.ItemForChargeMethodDescription.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Other Cost Totals";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForOtherCost,
            this.ItemForOtherSell,
            this.emptySpaceItem6});
            this.layoutControlGroup10.Location = new System.Drawing.Point(299, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(273, 236);
            this.layoutControlGroup10.Text = "Other Cost Totals";
            // 
            // ItemForOtherCost
            // 
            this.ItemForOtherCost.Control = this.OtherCostSpinEdit;
            this.ItemForOtherCost.CustomizationFormText = "Other Cost:";
            this.ItemForOtherCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForOtherCost.Name = "ItemForOtherCost";
            this.ItemForOtherCost.Size = new System.Drawing.Size(249, 24);
            this.ItemForOtherCost.Text = "Other Cost:";
            this.ItemForOtherCost.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForOtherSell
            // 
            this.ItemForOtherSell.Control = this.OtherSellSpinEdit;
            this.ItemForOtherSell.CustomizationFormText = "Other Sell:";
            this.ItemForOtherSell.Location = new System.Drawing.Point(0, 24);
            this.ItemForOtherSell.Name = "ItemForOtherSell";
            this.ItemForOtherSell.Size = new System.Drawing.Size(249, 24);
            this.ItemForOtherSell.Text = "Other Sell:";
            this.ItemForOtherSell.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(249, 142);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(293, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 236);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Callout Totals";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTotalCost,
            this.ItemForTotalSell,
            this.ItemForNonStandardCost,
            this.ItemForNonStandardSell,
            this.emptySpaceItem7});
            this.layoutControlGroup11.Location = new System.Drawing.Point(578, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(293, 236);
            this.layoutControlGroup11.Text = "Callout Totals";
            // 
            // ItemForTotalCost
            // 
            this.ItemForTotalCost.Control = this.TotalCostSpinEdit;
            this.ItemForTotalCost.CustomizationFormText = "Total Cost:";
            this.ItemForTotalCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForTotalCost.Name = "ItemForTotalCost";
            this.ItemForTotalCost.Size = new System.Drawing.Size(269, 24);
            this.ItemForTotalCost.Text = "Total Cost:";
            this.ItemForTotalCost.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForTotalSell
            // 
            this.ItemForTotalSell.Control = this.TotalSellSpinEdit;
            this.ItemForTotalSell.CustomizationFormText = "Total Sell:";
            this.ItemForTotalSell.Location = new System.Drawing.Point(0, 24);
            this.ItemForTotalSell.Name = "ItemForTotalSell";
            this.ItemForTotalSell.Size = new System.Drawing.Size(269, 24);
            this.ItemForTotalSell.Text = "Total Sell:";
            this.ItemForTotalSell.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForNonStandardCost
            // 
            this.ItemForNonStandardCost.Control = this.NonStandardCostCheckEdit;
            this.ItemForNonStandardCost.CustomizationFormText = "Non-Standard Cost:";
            this.ItemForNonStandardCost.Location = new System.Drawing.Point(0, 48);
            this.ItemForNonStandardCost.Name = "ItemForNonStandardCost";
            this.ItemForNonStandardCost.Size = new System.Drawing.Size(269, 23);
            this.ItemForNonStandardCost.Text = "Non-Standard Cost:";
            this.ItemForNonStandardCost.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForNonStandardSell
            // 
            this.ItemForNonStandardSell.Control = this.NonStandardSellCheckEdit;
            this.ItemForNonStandardSell.CustomizationFormText = "Non-Standard Sell:";
            this.ItemForNonStandardSell.Location = new System.Drawing.Point(0, 71);
            this.ItemForNonStandardSell.Name = "ItemForNonStandardSell";
            this.ItemForNonStandardSell.Size = new System.Drawing.Size(269, 23);
            this.ItemForNonStandardSell.Text = "Non-Standard Sell:";
            this.ItemForNonStandardSell.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 94);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(269, 96);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(572, 0);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(6, 236);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 236);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(871, 74);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Payment";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup13,
            this.layoutControlGroup14,
            this.splitterItem3,
            this.emptySpaceItem11});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(871, 310);
            this.layoutControlGroup12.Text = "Payment";
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Snow Clearance Team";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSubContractorPaid,
            this.ItemForPaidByStaffName,
            this.ItemForDoNotPaySubContractor,
            this.ItemForDoNotPaySubContractorReason,
            this.emptySpaceItem9,
            this.ItemForTeamInvoiceNumber,
            this.ItemForTimesheetSubmitted,
            this.emptySpaceItem14,
            this.itemForTimesheetNumber,
            this.ItemForFinanceTeamPaymentExported});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(435, 300);
            this.layoutControlGroup13.Text = "Snow Clearance Team";
            // 
            // ItemForSubContractorPaid
            // 
            this.ItemForSubContractorPaid.Control = this.SubContractorPaidCheckEdit;
            this.ItemForSubContractorPaid.CustomizationFormText = "Team Paid:";
            this.ItemForSubContractorPaid.Location = new System.Drawing.Point(0, 81);
            this.ItemForSubContractorPaid.Name = "ItemForSubContractorPaid";
            this.ItemForSubContractorPaid.Size = new System.Drawing.Size(411, 23);
            this.ItemForSubContractorPaid.Text = "Team Paid:";
            this.ItemForSubContractorPaid.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForPaidByStaffName
            // 
            this.ItemForPaidByStaffName.Control = this.PaidByStaffNameTextEdit;
            this.ItemForPaidByStaffName.CustomizationFormText = "Paid By Name:";
            this.ItemForPaidByStaffName.Location = new System.Drawing.Point(0, 104);
            this.ItemForPaidByStaffName.Name = "ItemForPaidByStaffName";
            this.ItemForPaidByStaffName.Size = new System.Drawing.Size(411, 24);
            this.ItemForPaidByStaffName.Text = "Paid By Name:";
            this.ItemForPaidByStaffName.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForDoNotPaySubContractor
            // 
            this.ItemForDoNotPaySubContractor.Control = this.DoNotPaySubContractorCheckEdit;
            this.ItemForDoNotPaySubContractor.CustomizationFormText = "Do Not Pay Team:";
            this.ItemForDoNotPaySubContractor.Location = new System.Drawing.Point(0, 128);
            this.ItemForDoNotPaySubContractor.Name = "ItemForDoNotPaySubContractor";
            this.ItemForDoNotPaySubContractor.Size = new System.Drawing.Size(411, 23);
            this.ItemForDoNotPaySubContractor.Text = "Do Not Pay Team:";
            this.ItemForDoNotPaySubContractor.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForDoNotPaySubContractorReason
            // 
            this.ItemForDoNotPaySubContractorReason.Control = this.DoNotPaySubContractorReasonMemoEdit;
            this.ItemForDoNotPaySubContractorReason.CustomizationFormText = "Do Not Pay Reason:";
            this.ItemForDoNotPaySubContractorReason.Location = new System.Drawing.Point(0, 151);
            this.ItemForDoNotPaySubContractorReason.MaxSize = new System.Drawing.Size(0, 70);
            this.ItemForDoNotPaySubContractorReason.MinSize = new System.Drawing.Size(139, 70);
            this.ItemForDoNotPaySubContractorReason.Name = "ItemForDoNotPaySubContractorReason";
            this.ItemForDoNotPaySubContractorReason.Size = new System.Drawing.Size(411, 70);
            this.ItemForDoNotPaySubContractorReason.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDoNotPaySubContractorReason.Text = "Do Not Pay Reason:";
            this.ItemForDoNotPaySubContractorReason.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 244);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(411, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTeamInvoiceNumber
            // 
            this.ItemForTeamInvoiceNumber.Control = this.TeamInvoiceNumberTextEdit;
            this.ItemForTeamInvoiceNumber.CustomizationFormText = "Team Invoice Number:";
            this.ItemForTeamInvoiceNumber.Location = new System.Drawing.Point(0, 23);
            this.ItemForTeamInvoiceNumber.Name = "ItemForTeamInvoiceNumber";
            this.ItemForTeamInvoiceNumber.Size = new System.Drawing.Size(411, 24);
            this.ItemForTeamInvoiceNumber.Text = "Team Invoice Number:";
            this.ItemForTeamInvoiceNumber.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForTimesheetSubmitted
            // 
            this.ItemForTimesheetSubmitted.Control = this.TimesheetSubmittedCheckEdit;
            this.ItemForTimesheetSubmitted.CustomizationFormText = "Timesheet Submitted:";
            this.ItemForTimesheetSubmitted.Location = new System.Drawing.Point(0, 0);
            this.ItemForTimesheetSubmitted.Name = "ItemForTimesheetSubmitted";
            this.ItemForTimesheetSubmitted.Size = new System.Drawing.Size(411, 23);
            this.ItemForTimesheetSubmitted.Text = "Timesheet Submitted:";
            this.ItemForTimesheetSubmitted.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(411, 10);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // itemForTimesheetNumber
            // 
            this.itemForTimesheetNumber.Control = this.TimesheetNumberTextEdit;
            this.itemForTimesheetNumber.CustomizationFormText = "Timesheet Number:";
            this.itemForTimesheetNumber.Location = new System.Drawing.Point(0, 47);
            this.itemForTimesheetNumber.Name = "itemForTimesheetNumber";
            this.itemForTimesheetNumber.Size = new System.Drawing.Size(411, 24);
            this.itemForTimesheetNumber.Text = "Timesheet Number:";
            this.itemForTimesheetNumber.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForFinanceTeamPaymentExported
            // 
            this.ItemForFinanceTeamPaymentExported.Control = this.FinanceTeamPaymentExportedCheckEdit;
            this.ItemForFinanceTeamPaymentExported.CustomizationFormText = "Team Payment Exported:";
            this.ItemForFinanceTeamPaymentExported.Location = new System.Drawing.Point(0, 221);
            this.ItemForFinanceTeamPaymentExported.Name = "ItemForFinanceTeamPaymentExported";
            this.ItemForFinanceTeamPaymentExported.Size = new System.Drawing.Size(411, 23);
            this.ItemForFinanceTeamPaymentExported.Text = "Team Payment Exported:";
            this.ItemForFinanceTeamPaymentExported.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "Client";
            this.layoutControlGroup14.ExpandButtonVisible = true;
            this.layoutControlGroup14.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientInvoiceNumber,
            this.ItemForDoNotInvoiceClient,
            this.ItemForDoNotInvoiceClientReason,
            this.emptySpaceItem10});
            this.layoutControlGroup14.Location = new System.Drawing.Point(441, 0);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(430, 300);
            this.layoutControlGroup14.Text = "Client";
            // 
            // ItemForClientInvoiceNumber
            // 
            this.ItemForClientInvoiceNumber.Control = this.ClientInvoiceNumberTextEdit;
            this.ItemForClientInvoiceNumber.CustomizationFormText = "Client Invoice Number:";
            this.ItemForClientInvoiceNumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientInvoiceNumber.Name = "ItemForClientInvoiceNumber";
            this.ItemForClientInvoiceNumber.Size = new System.Drawing.Size(406, 24);
            this.ItemForClientInvoiceNumber.Text = "Client Invoice Number:";
            this.ItemForClientInvoiceNumber.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForDoNotInvoiceClient
            // 
            this.ItemForDoNotInvoiceClient.Control = this.DoNotInvoiceClientCheckEdit;
            this.ItemForDoNotInvoiceClient.CustomizationFormText = "Do Not Invoice Client:";
            this.ItemForDoNotInvoiceClient.Location = new System.Drawing.Point(0, 24);
            this.ItemForDoNotInvoiceClient.Name = "ItemForDoNotInvoiceClient";
            this.ItemForDoNotInvoiceClient.Size = new System.Drawing.Size(406, 23);
            this.ItemForDoNotInvoiceClient.Text = "Do Not Invoice Client:";
            this.ItemForDoNotInvoiceClient.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForDoNotInvoiceClientReason
            // 
            this.ItemForDoNotInvoiceClientReason.Control = this.DoNotInvoiceClientReasonMemoEdit;
            this.ItemForDoNotInvoiceClientReason.CustomizationFormText = "Do Not Invoice Reason:";
            this.ItemForDoNotInvoiceClientReason.Location = new System.Drawing.Point(0, 47);
            this.ItemForDoNotInvoiceClientReason.Name = "ItemForDoNotInvoiceClientReason";
            this.ItemForDoNotInvoiceClientReason.Size = new System.Drawing.Size(406, 187);
            this.ItemForDoNotInvoiceClientReason.Text = "Do Not Invoice Reason:";
            this.ItemForDoNotInvoiceClientReason.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 234);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(406, 20);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem3
            // 
            this.splitterItem3.AllowHotTrack = true;
            this.splitterItem3.CustomizationFormText = "splitterItem3";
            this.splitterItem3.Location = new System.Drawing.Point(435, 0);
            this.splitterItem3.Name = "splitterItem3";
            this.splitterItem3.Size = new System.Drawing.Size(6, 300);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 300);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(871, 10);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Access Details";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAccessComments});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(871, 310);
            this.layoutControlGroup7.Text = "Access Details";
            // 
            // ItemForAccessComments
            // 
            this.ItemForAccessComments.Control = this.AccessCommentsMemoEdit;
            this.ItemForAccessComments.CustomizationFormText = "Access Details:";
            this.ItemForAccessComments.Location = new System.Drawing.Point(0, 0);
            this.ItemForAccessComments.Name = "ItemForAccessComments";
            this.ItemForAccessComments.Size = new System.Drawing.Size(871, 310);
            this.ItemForAccessComments.Text = "Access Details:";
            this.ItemForAccessComments.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForAccessComments.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Visit Aborted Reason";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAbortedReason});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(871, 310);
            this.layoutControlGroup8.Text = "Visit Aborted Reason";
            // 
            // ItemForAbortedReason
            // 
            this.ItemForAbortedReason.Control = this.AbortedReasonMemoEdit;
            this.ItemForAbortedReason.CustomizationFormText = "Visit Aborted Reason:";
            this.ItemForAbortedReason.Location = new System.Drawing.Point(0, 0);
            this.ItemForAbortedReason.Name = "ItemForAbortedReason";
            this.ItemForAbortedReason.Size = new System.Drawing.Size(871, 310);
            this.ItemForAbortedReason.Text = "Visit Aborted Reason:";
            this.ItemForAbortedReason.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForAbortedReason.TextVisible = false;
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.CustomizationFormText = "Site Address";
            this.layoutControlGroup16.ExpandButtonVisible = true;
            this.layoutControlGroup16.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteAddressLine1,
            this.ItemForSiteAddressLine2,
            this.ItemForSiteAddressLine3,
            this.ItemForSiteAddressLine4,
            this.ItemForSiteAddressLine5,
            this.ItemForSitePostcode});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Size = new System.Drawing.Size(871, 310);
            this.layoutControlGroup16.Text = "Site Address";
            // 
            // ItemForSiteAddressLine1
            // 
            this.ItemForSiteAddressLine1.Control = this.SiteAddressLine1TextEdit;
            this.ItemForSiteAddressLine1.CustomizationFormText = "Site Address Line 1:";
            this.ItemForSiteAddressLine1.Location = new System.Drawing.Point(0, 0);
            this.ItemForSiteAddressLine1.Name = "ItemForSiteAddressLine1";
            this.ItemForSiteAddressLine1.Size = new System.Drawing.Size(871, 24);
            this.ItemForSiteAddressLine1.Text = "Site Address Line 1:";
            this.ItemForSiteAddressLine1.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForSiteAddressLine2
            // 
            this.ItemForSiteAddressLine2.Control = this.SiteAddressLine2TextEdit;
            this.ItemForSiteAddressLine2.CustomizationFormText = "Site Address Line 2:";
            this.ItemForSiteAddressLine2.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteAddressLine2.Name = "ItemForSiteAddressLine2";
            this.ItemForSiteAddressLine2.Size = new System.Drawing.Size(871, 24);
            this.ItemForSiteAddressLine2.Text = "Site Address Line 2:";
            this.ItemForSiteAddressLine2.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForSiteAddressLine3
            // 
            this.ItemForSiteAddressLine3.Control = this.SiteAddressLine3TextEdit;
            this.ItemForSiteAddressLine3.CustomizationFormText = "Site Address Line 3:";
            this.ItemForSiteAddressLine3.Location = new System.Drawing.Point(0, 48);
            this.ItemForSiteAddressLine3.Name = "ItemForSiteAddressLine3";
            this.ItemForSiteAddressLine3.Size = new System.Drawing.Size(871, 24);
            this.ItemForSiteAddressLine3.Text = "Site Address Line 3:";
            this.ItemForSiteAddressLine3.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForSiteAddressLine4
            // 
            this.ItemForSiteAddressLine4.Control = this.SiteAddressLine4TextEdit;
            this.ItemForSiteAddressLine4.CustomizationFormText = "Site Address Line 4:";
            this.ItemForSiteAddressLine4.Location = new System.Drawing.Point(0, 72);
            this.ItemForSiteAddressLine4.Name = "ItemForSiteAddressLine4";
            this.ItemForSiteAddressLine4.Size = new System.Drawing.Size(871, 24);
            this.ItemForSiteAddressLine4.Text = "Site Address Line 4:";
            this.ItemForSiteAddressLine4.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForSiteAddressLine5
            // 
            this.ItemForSiteAddressLine5.Control = this.SiteAddressLine5TextEdit;
            this.ItemForSiteAddressLine5.CustomizationFormText = "Site Address Line 5:";
            this.ItemForSiteAddressLine5.Location = new System.Drawing.Point(0, 96);
            this.ItemForSiteAddressLine5.Name = "ItemForSiteAddressLine5";
            this.ItemForSiteAddressLine5.Size = new System.Drawing.Size(871, 24);
            this.ItemForSiteAddressLine5.Text = "Site Address Line 5:";
            this.ItemForSiteAddressLine5.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForSitePostcode
            // 
            this.ItemForSitePostcode.Control = this.SitePostcodeTextEdit;
            this.ItemForSitePostcode.CustomizationFormText = "Site Postcode:";
            this.ItemForSitePostcode.Location = new System.Drawing.Point(0, 120);
            this.ItemForSitePostcode.Name = "ItemForSitePostcode";
            this.ItemForSitePostcode.Size = new System.Drawing.Size(871, 190);
            this.ItemForSitePostcode.Text = "Site Postcode:";
            this.ItemForSitePostcode.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlGroup19
            // 
            this.layoutControlGroup19.CustomizationFormText = "Team Members Present";
            this.layoutControlGroup19.ExpandButtonVisible = true;
            this.layoutControlGroup19.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTeamMembersPresent});
            this.layoutControlGroup19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup19.Name = "layoutControlGroup19";
            this.layoutControlGroup19.Size = new System.Drawing.Size(871, 310);
            this.layoutControlGroup19.Text = "Team Members Present";
            // 
            // ItemForTeamMembersPresent
            // 
            this.ItemForTeamMembersPresent.Control = this.TeamMembersPresentMemoEdit;
            this.ItemForTeamMembersPresent.CustomizationFormText = "Team Members Present:";
            this.ItemForTeamMembersPresent.Location = new System.Drawing.Point(0, 0);
            this.ItemForTeamMembersPresent.Name = "ItemForTeamMembersPresent";
            this.ItemForTeamMembersPresent.Size = new System.Drawing.Size(871, 310);
            this.ItemForTeamMembersPresent.Text = "Team Members Present:";
            this.ItemForTeamMembersPresent.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForTeamMembersPresent.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks,
            this.ItemForInternalRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(871, 310);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(871, 154);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForInternalRemarks
            // 
            this.ItemForInternalRemarks.Control = this.InternalRemarksMemoEdit;
            this.ItemForInternalRemarks.CustomizationFormText = "Internal Remarks:";
            this.ItemForInternalRemarks.Location = new System.Drawing.Point(0, 154);
            this.ItemForInternalRemarks.Name = "ItemForInternalRemarks";
            this.ItemForInternalRemarks.Size = new System.Drawing.Size(871, 156);
            this.ItemForInternalRemarks.Text = "Internal Remarks:";
            this.ItemForInternalRemarks.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 192);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(919, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameButtonEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(460, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForSubContractorName
            // 
            this.ItemForSubContractorName.Control = this.SubContractorNameButtonEdit;
            this.ItemForSubContractorName.CustomizationFormText = "Team Name:";
            this.ItemForSubContractorName.Location = new System.Drawing.Point(0, 48);
            this.ItemForSubContractorName.Name = "ItemForSubContractorName";
            this.ItemForSubContractorName.Size = new System.Drawing.Size(460, 24);
            this.ItemForSubContractorName.Text = "Team Name:";
            this.ItemForSubContractorName.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForClientPOIDDescription
            // 
            this.ItemForClientPOIDDescription.Control = this.ClientPOIDDescriptionButtonEdit;
            this.ItemForClientPOIDDescription.CustomizationFormText = "Linked Client PO Number:";
            this.ItemForClientPOIDDescription.Location = new System.Drawing.Point(0, 72);
            this.ItemForClientPOIDDescription.Name = "ItemForClientPOIDDescription";
            this.ItemForClientPOIDDescription.Size = new System.Drawing.Size(460, 24);
            this.ItemForClientPOIDDescription.Text = "Linked Client PO Number:";
            this.ItemForClientPOIDDescription.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(460, 24);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(459, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForOriginalSubContractorName
            // 
            this.ItemForOriginalSubContractorName.Control = this.OriginalSubContractorNameButtonEdit;
            this.ItemForOriginalSubContractorName.CustomizationFormText = "Original Team Name:";
            this.ItemForOriginalSubContractorName.Location = new System.Drawing.Point(460, 48);
            this.ItemForOriginalSubContractorName.Name = "ItemForOriginalSubContractorName";
            this.ItemForOriginalSubContractorName.Size = new System.Drawing.Size(459, 24);
            this.ItemForOriginalSubContractorName.Text = "Original Team Name:";
            this.ItemForOriginalSubContractorName.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberTextEdit;
            this.ItemForClientPONumber.CustomizationFormText = "Client PO Number:";
            this.ItemForClientPONumber.Location = new System.Drawing.Point(460, 72);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(459, 24);
            this.ItemForClientPONumber.Text = "Client PO Number:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForJobStatusID
            // 
            this.ItemForJobStatusID.Control = this.JobStatusIDGridLookUpEdit;
            this.ItemForJobStatusID.CustomizationFormText = "Callout Status:";
            this.ItemForJobStatusID.Location = new System.Drawing.Point(460, 120);
            this.ItemForJobStatusID.Name = "ItemForJobStatusID";
            this.ItemForJobStatusID.Size = new System.Drawing.Size(459, 24);
            this.ItemForJobStatusID.Text = "Callout Status:";
            this.ItemForJobStatusID.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForRecordedByName
            // 
            this.ItemForRecordedByName.Control = this.RecordedByNameTextEdit;
            this.ItemForRecordedByName.CustomizationFormText = "Recorded By Name:";
            this.ItemForRecordedByName.Location = new System.Drawing.Point(0, 144);
            this.ItemForRecordedByName.Name = "ItemForRecordedByName";
            this.ItemForRecordedByName.Size = new System.Drawing.Size(460, 24);
            this.ItemForRecordedByName.Text = "Recorded By Name:";
            this.ItemForRecordedByName.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Timings";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCallOutDateTime,
            this.ItemForStartTime,
            this.ItemForHoursWorked,
            this.ItemForSubContractorETA,
            this.ItemForCompletedTime,
            this.ItemForClientHowSoonID,
            this.emptySpaceItem17});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 202);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(460, 245);
            this.layoutControlGroup5.Text = "Timings";
            // 
            // ItemForCallOutDateTime
            // 
            this.ItemForCallOutDateTime.Control = this.CallOutDateTimeDateEdit;
            this.ItemForCallOutDateTime.CustomizationFormText = "Callout Date \\ Time:";
            this.ItemForCallOutDateTime.Location = new System.Drawing.Point(0, 0);
            this.ItemForCallOutDateTime.Name = "ItemForCallOutDateTime";
            this.ItemForCallOutDateTime.Size = new System.Drawing.Size(436, 24);
            this.ItemForCallOutDateTime.Text = "Callout Date \\ Time:";
            this.ItemForCallOutDateTime.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForStartTime
            // 
            this.ItemForStartTime.Control = this.StartTimeDateEdit;
            this.ItemForStartTime.CustomizationFormText = "Start Time:";
            this.ItemForStartTime.Location = new System.Drawing.Point(0, 72);
            this.ItemForStartTime.Name = "ItemForStartTime";
            this.ItemForStartTime.Size = new System.Drawing.Size(436, 24);
            this.ItemForStartTime.Text = "Start Time:";
            this.ItemForStartTime.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForHoursWorked
            // 
            this.ItemForHoursWorked.Control = this.HoursWorkedSpinEdit;
            this.ItemForHoursWorked.CustomizationFormText = "Hours Worked:";
            this.ItemForHoursWorked.Location = new System.Drawing.Point(0, 120);
            this.ItemForHoursWorked.Name = "ItemForHoursWorked";
            this.ItemForHoursWorked.Size = new System.Drawing.Size(436, 24);
            this.ItemForHoursWorked.Text = "Hours Worked:";
            this.ItemForHoursWorked.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForSubContractorETA
            // 
            this.ItemForSubContractorETA.Control = this.SubContractorETADateEdit;
            this.ItemForSubContractorETA.CustomizationFormText = "Team ETA:";
            this.ItemForSubContractorETA.Location = new System.Drawing.Point(0, 48);
            this.ItemForSubContractorETA.Name = "ItemForSubContractorETA";
            this.ItemForSubContractorETA.Size = new System.Drawing.Size(436, 24);
            this.ItemForSubContractorETA.Text = "Team ETA:";
            this.ItemForSubContractorETA.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForCompletedTime
            // 
            this.ItemForCompletedTime.Control = this.CompletedTimeDateEdit;
            this.ItemForCompletedTime.CustomizationFormText = "Completed Time:";
            this.ItemForCompletedTime.Location = new System.Drawing.Point(0, 96);
            this.ItemForCompletedTime.Name = "ItemForCompletedTime";
            this.ItemForCompletedTime.Size = new System.Drawing.Size(436, 24);
            this.ItemForCompletedTime.Text = "Completed Time:";
            this.ItemForCompletedTime.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForClientHowSoonID
            // 
            this.ItemForClientHowSoonID.Control = this.ClientHowSoonIDGridLookUpEdit;
            this.ItemForClientHowSoonID.CustomizationFormText = "How Soon:";
            this.ItemForClientHowSoonID.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientHowSoonID.Name = "ItemForClientHowSoonID";
            this.ItemForClientHowSoonID.Size = new System.Drawing.Size(436, 24);
            this.ItemForClientHowSoonID.Text = "How Soon:";
            this.ItemForClientHowSoonID.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.CustomizationFormText = "emptySpaceItem17";
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 144);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(436, 55);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(126, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(126, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(126, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(326, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(121, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "Navigator";
            this.layoutControlItem1.Location = new System.Drawing.Point(126, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem1.Text = "Navigator";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 447);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(919, 10);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSubContractorContactedByStaffIDGridLookUpEdit
            // 
            this.ItemForSubContractorContactedByStaffIDGridLookUpEdit.Control = this.SubContractorContactedByStaffIDGridLookUpEdit;
            this.ItemForSubContractorContactedByStaffIDGridLookUpEdit.CustomizationFormText = "Team Contacted By:";
            this.ItemForSubContractorContactedByStaffIDGridLookUpEdit.Location = new System.Drawing.Point(0, 168);
            this.ItemForSubContractorContactedByStaffIDGridLookUpEdit.Name = "ItemForSubContractorContactedByStaffIDGridLookUpEdit";
            this.ItemForSubContractorContactedByStaffIDGridLookUpEdit.Size = new System.Drawing.Size(460, 24);
            this.ItemForSubContractorContactedByStaffIDGridLookUpEdit.Text = "Team Contacted By:";
            this.ItemForSubContractorContactedByStaffIDGridLookUpEdit.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 1121);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(919, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.CustomizationFormText = "Linked Rates";
            this.layoutControlGroup15.ExpandButtonVisible = true;
            this.layoutControlGroup15.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 871);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(919, 250);
            this.layoutControlGroup15.Text = "Linked Rates";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Rates";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 204);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(229, 204);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(895, 204);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Rates";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 861);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(919, 10);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWarningLabel
            // 
            this.ItemForWarningLabel.Control = this.WarningLabel;
            this.ItemForWarningLabel.CustomizationFormText = "Warning Label:";
            this.ItemForWarningLabel.Location = new System.Drawing.Point(447, 0);
            this.ItemForWarningLabel.MinSize = new System.Drawing.Size(380, 18);
            this.ItemForWarningLabel.Name = "ItemForWarningLabel";
            this.ItemForWarningLabel.Size = new System.Drawing.Size(472, 24);
            this.ItemForWarningLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWarningLabel.Text = "Warning Label:";
            this.ItemForWarningLabel.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForWarningLabel.TextVisible = false;
            // 
            // ItemForSnowClearanceCallOutID
            // 
            this.ItemForSnowClearanceCallOutID.Control = this.SnowClearanceCallOutIDTextEdit;
            this.ItemForSnowClearanceCallOutID.CustomizationFormText = "Ground Control PO No:";
            this.ItemForSnowClearanceCallOutID.Location = new System.Drawing.Point(0, 96);
            this.ItemForSnowClearanceCallOutID.Name = "ItemForSnowClearanceCallOutID";
            this.ItemForSnowClearanceCallOutID.Size = new System.Drawing.Size(460, 24);
            this.ItemForSnowClearanceCallOutID.Text = "Ground Control PO No:";
            this.ItemForSnowClearanceCallOutID.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForGCPONumberSuffix
            // 
            this.ItemForGCPONumberSuffix.Control = this.GCPONumberSuffixTextEdit;
            this.ItemForGCPONumberSuffix.CustomizationFormText = "GC PO Suffix:";
            this.ItemForGCPONumberSuffix.Location = new System.Drawing.Point(460, 96);
            this.ItemForGCPONumberSuffix.Name = "ItemForGCPONumberSuffix";
            this.ItemForGCPONumberSuffix.Size = new System.Drawing.Size(459, 24);
            this.ItemForGCPONumberSuffix.Text = "GC PO Suffix:";
            this.ItemForGCPONumberSuffix.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 120);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(460, 24);
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlGroup17
            // 
            this.layoutControlGroup17.CustomizationFormText = "Access";
            this.layoutControlGroup17.ExpandButtonVisible = true;
            this.layoutControlGroup17.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNoAccess,
            this.ItemForNoAccessAbortedRate,
            this.emptySpaceItem16,
            this.ItemForVisitAborted});
            this.layoutControlGroup17.Location = new System.Drawing.Point(460, 344);
            this.layoutControlGroup17.Name = "layoutControlGroup17";
            this.layoutControlGroup17.Size = new System.Drawing.Size(459, 103);
            this.layoutControlGroup17.Text = "Access";
            // 
            // ItemForNoAccess
            // 
            this.ItemForNoAccess.Control = this.NoAccessCheckEdit;
            this.ItemForNoAccess.CustomizationFormText = "No Access:";
            this.ItemForNoAccess.Location = new System.Drawing.Point(0, 0);
            this.ItemForNoAccess.Name = "ItemForNoAccess";
            this.ItemForNoAccess.Size = new System.Drawing.Size(217, 23);
            this.ItemForNoAccess.Text = "No Access:";
            this.ItemForNoAccess.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForNoAccessAbortedRate
            // 
            this.ItemForNoAccessAbortedRate.Control = this.NoAccessAbortedRateSpinEdit;
            this.ItemForNoAccessAbortedRate.CustomizationFormText = "No Access\\Aborted Rate:";
            this.ItemForNoAccessAbortedRate.Location = new System.Drawing.Point(0, 23);
            this.ItemForNoAccessAbortedRate.Name = "ItemForNoAccessAbortedRate";
            this.ItemForNoAccessAbortedRate.Size = new System.Drawing.Size(435, 24);
            this.ItemForNoAccessAbortedRate.Text = "No Access\\Aborted Rate:";
            this.ItemForNoAccessAbortedRate.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.CustomizationFormText = "emptySpaceItem16";
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(435, 10);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForVisitAborted
            // 
            this.ItemForVisitAborted.Control = this.VisitAbortedCheckEdit;
            this.ItemForVisitAborted.CustomizationFormText = "Visit Aborted:";
            this.ItemForVisitAborted.Location = new System.Drawing.Point(217, 0);
            this.ItemForVisitAborted.Name = "ItemForVisitAborted";
            this.ItemForVisitAborted.Size = new System.Drawing.Size(218, 23);
            this.ItemForVisitAborted.Text = "Visit Aborted:";
            this.ItemForVisitAborted.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForSiteManagerName
            // 
            this.ItemForSiteManagerName.Control = this.SiteManagerNameTextEdit;
            this.ItemForSiteManagerName.CustomizationFormText = "Site Manager Name:";
            this.ItemForSiteManagerName.Location = new System.Drawing.Point(460, 144);
            this.ItemForSiteManagerName.Name = "ItemForSiteManagerName";
            this.ItemForSiteManagerName.Size = new System.Drawing.Size(459, 24);
            this.ItemForSiteManagerName.Text = "Site Manager Name:";
            this.ItemForSiteManagerName.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlGroup18
            // 
            this.layoutControlGroup18.CustomizationFormText = "Location";
            this.layoutControlGroup18.ExpandButtonVisible = true;
            this.layoutControlGroup18.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartLatitude,
            this.ItemForFinishedLatitude,
            this.ItemForStartLongitude,
            this.ItemForFinishedLongitude});
            this.layoutControlGroup18.Location = new System.Drawing.Point(460, 202);
            this.layoutControlGroup18.Name = "layoutControlGroup18";
            this.layoutControlGroup18.Size = new System.Drawing.Size(459, 142);
            this.layoutControlGroup18.Text = "Location";
            // 
            // ItemForStartLatitude
            // 
            this.ItemForStartLatitude.Control = this.StartLatitudeTextEdit;
            this.ItemForStartLatitude.CustomizationFormText = "Start Latitude:";
            this.ItemForStartLatitude.Location = new System.Drawing.Point(0, 0);
            this.ItemForStartLatitude.Name = "ItemForStartLatitude";
            this.ItemForStartLatitude.Size = new System.Drawing.Size(435, 24);
            this.ItemForStartLatitude.Text = "Start Latitude:";
            this.ItemForStartLatitude.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForFinishedLatitude
            // 
            this.ItemForFinishedLatitude.Control = this.FinishedLatitudeTextEdit;
            this.ItemForFinishedLatitude.CustomizationFormText = "Finished Latitude:";
            this.ItemForFinishedLatitude.Location = new System.Drawing.Point(0, 48);
            this.ItemForFinishedLatitude.Name = "ItemForFinishedLatitude";
            this.ItemForFinishedLatitude.Size = new System.Drawing.Size(435, 24);
            this.ItemForFinishedLatitude.Text = "Finished Latitude:";
            this.ItemForFinishedLatitude.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForStartLongitude
            // 
            this.ItemForStartLongitude.Control = this.StartLongitudeTextEdit;
            this.ItemForStartLongitude.CustomizationFormText = "Start Longitude:";
            this.ItemForStartLongitude.Location = new System.Drawing.Point(0, 24);
            this.ItemForStartLongitude.Name = "ItemForStartLongitude";
            this.ItemForStartLongitude.Size = new System.Drawing.Size(435, 24);
            this.ItemForStartLongitude.Text = "Start Longitude:";
            this.ItemForStartLongitude.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForFinishedLongitude
            // 
            this.ItemForFinishedLongitude.Control = this.FinishedLongitudeTextEdit;
            this.ItemForFinishedLongitude.CustomizationFormText = "Finished Longitude:";
            this.ItemForFinishedLongitude.Location = new System.Drawing.Point(0, 72);
            this.ItemForFinishedLongitude.Name = "ItemForFinishedLongitude";
            this.ItemForFinishedLongitude.Size = new System.Drawing.Size(435, 24);
            this.ItemForFinishedLongitude.Text = "Finished Longitude:";
            this.ItemForFinishedLongitude.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForCompletedOnPDA
            // 
            this.ItemForCompletedOnPDA.Control = this.CompletedOnPDACheckEdit;
            this.ItemForCompletedOnPDA.CustomizationFormText = "Completed On Device:";
            this.ItemForCompletedOnPDA.Location = new System.Drawing.Point(460, 168);
            this.ItemForCompletedOnPDA.Name = "ItemForCompletedOnPDA";
            this.ItemForCompletedOnPDA.Size = new System.Drawing.Size(157, 24);
            this.ItemForCompletedOnPDA.Text = "Completed On Device:";
            this.ItemForCompletedOnPDA.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForNoOneToSign
            // 
            this.ItemForNoOneToSign.Control = this.NoOneToSignCheckEdit;
            this.ItemForNoOneToSign.CustomizationFormText = "No One To Sign:";
            this.ItemForNoOneToSign.Location = new System.Drawing.Point(617, 168);
            this.ItemForNoOneToSign.Name = "ItemForNoOneToSign";
            this.ItemForNoOneToSign.Size = new System.Drawing.Size(151, 24);
            this.ItemForNoOneToSign.Text = "No One To Sign:";
            this.ItemForNoOneToSign.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForSigned
            // 
            this.ItemForSigned.Control = this.SignedCheckEdit;
            this.ItemForSigned.CustomizationFormText = "Signed:";
            this.ItemForSigned.Location = new System.Drawing.Point(768, 168);
            this.ItemForSigned.Name = "ItemForSigned";
            this.ItemForSigned.Size = new System.Drawing.Size(151, 24);
            this.ItemForSigned.Text = "Signed:";
            this.ItemForSigned.TextSize = new System.Drawing.Size(122, 13);
            // 
            // sp04163_GC_Snow_Callout_EditTableAdapter
            // 
            this.sp04163_GC_Snow_Callout_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter
            // 
            this.sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter
            // 
            this.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00226_Staff_List_With_BlankTableAdapter
            // 
            this.sp00226_Staff_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter
            // 
            this.sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_GC_Snow_Callout_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(956, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Snow_Callout_Edit";
            this.Text = "Edit Snow Clearance Callout";
            this.Activated += new System.EventHandler(this.frm_GC_Snow_Callout_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Snow_Callout_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Snow_Callout_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.InternalRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04163GCSnowCalloutEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoOneToSignCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedOnPDACheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamMembersPresentMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishedLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishedLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteManagerNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedBySubContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceTeamPaymentExportedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimesheetNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimesheetSubmittedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamInvoiceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoAccessAbortedRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04162GCSnowClearanceCalloutLinkedRatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMachineCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMinimumHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorContactedByStaffIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalSubContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceSiteContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeMethodDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientHowSoonIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04165GCClientHowSoonTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientReasonMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPaySubContractorReasonMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPaySubContractorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInvoiceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardSellCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardCostCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorPaidCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabourVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbortedReasonMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessCommentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETADateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETADateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursWorkedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompletedTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCPONumberSuffixTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceCallOutIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallOutDateTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CallOutDateTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordedByNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoAccessCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04085GCGrittingCalloutStatusListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitAbortedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalSubContractorNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOriginalSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedBySubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeMethodDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPaySubContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPaySubContractorReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamInvoiceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimesheetSubmitted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemForTimesheetNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceTeamPaymentExported)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInvoiceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClientReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbortedReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMembersPresent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInternalRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOIDDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOriginalSubContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordedByName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCallOutDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursWorked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorETA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletedTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientHowSoonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorContactedByStaffIDGridLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceCallOutID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCPONumberSuffix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoAccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoAccessAbortedRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitAborted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteManagerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishedLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishedLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletedOnPDA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoOneToSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSigned)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp04163GCSnowCalloutEditBindingSource;
        private DataSet_GC_Snow_DataEntry dataSet_GC_Snow_DataEntry;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04163_GC_Snow_Callout_EditTableAdapter sp04163_GC_Snow_Callout_EditTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit SiteNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.ButtonEdit SubContractorNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorName;
        private DevExpress.XtraEditors.ButtonEdit ClientPOIDDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOIDDescription;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraEditors.CheckEdit VisitAbortedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitAborted;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraEditors.ButtonEdit OriginalSubContractorNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOriginalSubContractorName;
        private DevExpress.XtraEditors.TextEdit ClientPONumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraEditors.GridLookUpEdit JobStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobStatusID;
        private DevExpress.XtraEditors.CheckEdit NoAccessCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoAccess;
        private DevExpress.XtraEditors.TextEdit RecordedByNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRecordedByName;
        private DevExpress.XtraEditors.DateEdit CallOutDateTimeDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCallOutDateTime;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.TextEdit SnowClearanceCallOutIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearanceCallOutID;
        private DevExpress.XtraEditors.TextEdit GCPONumberSuffixTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGCPONumberSuffix;
        private DevExpress.XtraEditors.DateEdit StartTimeDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartTime;
        private DevExpress.XtraEditors.DateEdit CompletedTimeDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompletedTime;
        private DevExpress.XtraEditors.SpinEdit HoursWorkedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHoursWorked;
        private DevExpress.XtraEditors.TextEdit SnowClearanceSiteContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemFor;
        private DevExpress.XtraEditors.TextEdit SubContractorIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorID;
        private DevExpress.XtraEditors.TextEdit OriginalSubContractorIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOriginalSubContractorID;
        private DevExpress.XtraEditors.TextEdit RecordedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRecordedByStaffID;
        private DevExpress.XtraEditors.DateEdit SubContractorETADateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorETA;
        private DevExpress.XtraEditors.MemoEdit AccessCommentsMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccessComments;
        private DevExpress.XtraEditors.MemoEdit AbortedReasonMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbortedReason;
        private DevExpress.XtraEditors.TextEdit ClientPOIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOID;
        private DevExpress.XtraEditors.SpinEdit LabourVatRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourVatRate;
        private DevExpress.XtraEditors.SpinEdit LabourCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourCost;
        private DevExpress.XtraEditors.SpinEdit LabourSellSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourSell;
        private DevExpress.XtraEditors.SpinEdit OtherCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOtherCost;
        private DevExpress.XtraEditors.SpinEdit OtherSellSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOtherSell;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.SpinEdit TotalCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCost;
        private DevExpress.XtraEditors.SpinEdit TotalSellSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalSell;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraEditors.CheckEdit NonStandardCostCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonStandardCost;
        private DevExpress.XtraEditors.CheckEdit NonStandardSellCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonStandardSell;
        private DevExpress.XtraEditors.TextEdit ClientInvoiceNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientInvoiceNumber;
        private DevExpress.XtraEditors.CheckEdit SubContractorPaidCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorPaid;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.SplitterItem splitterItem3;
        private DevExpress.XtraEditors.TextEdit PaidByStaffNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPaidByStaffName;
        private DevExpress.XtraEditors.TextEdit PaidByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPaidByStaffID;
        private DevExpress.XtraEditors.CheckEdit DoNotPaySubContractorCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotPaySubContractor;
        private DevExpress.XtraEditors.MemoEdit DoNotPaySubContractorReasonMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotPaySubContractorReason;
        private DevExpress.XtraEditors.CheckEdit DoNotInvoiceClientCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotInvoiceClient;
        private DevExpress.XtraEditors.MemoEdit DoNotInvoiceClientReasonMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotInvoiceClientReason;
        private DevExpress.XtraEditors.GridLookUpEdit ClientHowSoonIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientHowSoonID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private System.Windows.Forms.BindingSource sp04085GCGrittingCalloutStatusListWithBlankBindingSource;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DataSet_GC_DataEntryTableAdapters.sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter sp04085_GC_Gritting_Callout_Status_List_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp04165GCClientHowSoonTypesWithBlankBindingSource;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraEditors.TextEdit ChargeMethodDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChargeMethodDescription;
        private DevExpress.XtraEditors.GridLookUpEdit SubContractorContactedByStaffIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colNetworkID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorContactedByStaffIDGridLookUpEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp00226StaffListWithBlankBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter sp00226_Staff_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource sp04162GCSnowClearanceCalloutLinkedRatesBindingSource;
        private DataSet_GC_Snow_Core dataSet_GC_Snow_Core;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutRateID;
        private DevExpress.XtraGrid.Columns.GridColumn colDayTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDayTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colRate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DataSet_GC_Snow_CoreTableAdapters.sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamAddressLine11;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colHours1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSiteContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.LabelControl WarningLabel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarningLabel;
        private DevExpress.XtraEditors.SpinEdit NoAccessAbortedRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoAccessAbortedRate;
        private DevExpress.XtraEditors.CheckEdit TimesheetSubmittedCheckEdit;
        private DevExpress.XtraEditors.TextEdit TeamInvoiceNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamInvoiceNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTimesheetSubmitted;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraEditors.TextEdit RecordedBySubContractorIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRecordedBySubContractorID;
        private DevExpress.XtraEditors.TextEdit TimesheetNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem itemForTimesheetNumber;
        private DevExpress.XtraEditors.TextEdit SitePostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine2TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine1TextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSitePostcode;
        private DevExpress.XtraEditors.CheckEdit FinanceTeamPaymentExportedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinanceTeamPaymentExported;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMinimumHours;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineCount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditMachineCount;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineCount2;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineCount3;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineCount4;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineRate2;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineRate3;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineRate4;
        private DevExpress.XtraEditors.TextEdit SiteManagerNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteManagerName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraGrid.Columns.GridColumn colHours2;
        private DevExpress.XtraGrid.Columns.GridColumn colHours3;
        private DevExpress.XtraGrid.Columns.GridColumn colHours4;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours2;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours3;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.TextEdit StartLongitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit StartLatitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup18;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLatitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLongitude;
        private DevExpress.XtraEditors.TextEdit FinishedLongitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit FinishedLatitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishedLatitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishedLongitude;
        private DevExpress.XtraEditors.MemoEdit TeamMembersPresentMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup19;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamMembersPresent;
        private DevExpress.XtraEditors.CheckEdit CompletedOnPDACheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompletedOnPDA;
        private DevExpress.XtraEditors.CheckEdit NoOneToSignCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoOneToSign;
        private DevExpress.XtraEditors.CheckEdit SignedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSigned;
        private DevExpress.XtraEditors.MemoEdit InternalRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInternalRemarks;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

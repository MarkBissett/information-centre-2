using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Drawing.Imaging;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraMap;
using DevExpress.XtraPrinting;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Reports;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_GC_Callout_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        bool iBool_SendJobsButtonEnabled = false;
        bool iBool_ReassignJobsButtonEnabled = false;
        bool iBool_AuthoriseButtonEnabled = false;
        bool iBool_PayButtonEnabled = false;
        bool iBool_ViewSMSErrorsButtonEnabled = false;
        bool iBool_AddWizardButtonEnabled = false;
        bool iBool_ManuallyCompleteJobsEnabled = false;
        bool iBool_SendNoProactiveGrittingMessagesEnabled = false;
        bool iBool_EnableGridColumnChooser = false;
        bool _iBool_CompletionSheetEditLayoutButtonEnabled = false;

        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState12;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState6;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs12 = "";
        string i_str_AddedRecordIDs6 = "";

        string i_str_selected_CallOutType_ids = "";
        string i_str_selected_CallOutType_names = "";
        BaseObjects.GridCheckMarksSelection selection1;

        string i_str_selected_Company_ids = "";
        string i_str_selected_Company_names = "";
        BaseObjects.GridCheckMarksSelection selection2;

        private DateTime i_dtStart = DateTime.MinValue;  // Used if Panel is Date Range //
        private DateTime i_dtEnd = DateTime.MaxValue;  // Used if Panel is Date Range //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        public string strPassedInDrillDownIDs = "";
        string istr_FurtherAction = "";  // Set by Gritting Callout Wizard only //

        Bitmap bmpBlank = null;
        Bitmap bmpAlert = null;
        
        public string i_str_SiteIDsToLink = "";  // Populated via Snow Clearance Manager when it needs to create Gritting Callout for linking to the Snow Callouts //

        MapControl map;
        ImageTilesLayer imageLayer;
        VectorItemsLayer kmlLayer;
        Timer timer;
        bool tilesLoaded;

        //private string bingkey = "";
        //string strMapStartType = "Bing Satellite";

        // Following Used to merge updated changes back into the original dataset without reloading the whole dataset. IMPORTANT NOTE: backgroundWorker1 added as object to form and events set on it. //
        private string strEditedVisitIDs = "";
        bool boolProcessRunning = false;
        private int intErrorValue = 0;
        private string strErrorMessage = "";
        SqlDataAdapter sdaDataMerge = null;
        DataSet dsDataMerge = null;
        SqlCommand cmd = null;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        #endregion

        public frm_GC_Callout_Manager()
        {
            InitializeComponent();
        }

        private void frm_GC_Callout_Manager_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4007;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            bmpBlank = new Bitmap(16, 16);
            bmpAlert = new Bitmap(imageCollection1.Images[6], 16, 16);

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionStringREADONLY);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutPictureFilesFolderInternal").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Callout Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Callout Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sp04074_GC_Gritting_Callout_ManagerTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "GrittingCallOutID");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Add record selection checkboxes to popup Callout Type grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04001_GC_Job_CallOut_StatusesTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp04001_GC_Job_CallOut_StatusesTableAdapter.Fill(dataSet_GC_Core.sp04001_GC_Job_CallOut_Statuses);
            gridControl3.ForceInitialize();

            i_dtStart = DateTime.Today;
            i_dtEnd = DateTime.Today.AddDays(2).AddSeconds(-1);  // Should give todays date + 1 day + 23:59:59 //
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;

            sp04075_GC_Gritting_Callout_Linked_Extra_CostsTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "ExtraCostID");

            sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState3 = new RefreshGridState(gridView4, "PictureID");

            sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState12 = new RefreshGridState(gridView15, "CRMID");

            sp04337_GC_Service_Areas_Linked_To_Gritting_CalloutTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState6 = new RefreshGridState(gridView6, "GrittingServicedSiteID");        
            
            // Add record selection checkboxes to popup Company Filter grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            sp04237_GC_Company_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp04237_GC_Company_Filter_ListTableAdapter.Fill(dataSet_GC_Reports.sp04237_GC_Company_Filter_List);
            gridControl5.ForceInitialize();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                tabbedControlGroupFilterType.SelectedTabPageIndex = 1;
                VisitIDsMemoEdit.EditValue = strPassedInDrillDownIDs;
                Load_Data();  // Load records //
            }

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            emptyEditor = new RepositoryItem();

        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_GC_Callout_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    if (!string.IsNullOrEmpty(istr_FurtherAction))
                    {
                        GridView view = (GridView)gridControl1.MainView;
                        if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                        if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear and Find in place //         
                    }
                    Load_Data();
                    if (!string.IsNullOrEmpty(istr_FurtherAction))
                    {
                        string strAction = istr_FurtherAction;
                        istr_FurtherAction = "";
                        if (strAction == "edit")
                        {
                            Edit_Record();
                        }
                        else if (strAction == "blockedit")
                        {
                            Block_Edit();
                        }
                        else if (strAction == "send")
                        {
                            SendJobs();
                        }
                    }
                }
                else if (UpdateRefreshStatus == 2)
                {
                    Load_Data();  // Require this to repopulate main list to show updated total costs //
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_GC_Callout_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ColourCode", (checkEditColourCode.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CallOutFilter", i_str_selected_CallOutType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }


        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Colour Code //
                string strColourCode = default_screen_settings.RetrieveSetting("ColourCode");
                if (!string.IsNullOrEmpty(strColourCode)) checkEditColourCode.Checked = (strColourCode == "0" ? false : true);

                // Gritting Callout Filter //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("CallOutFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl3.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["Value"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit2.Text = PopupContainerEdit1_Get_Selected();
                }

                // Company Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("CompanyFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["CompanyID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditCompanies.Text = PopupContainerEditCompanies_Get_Selected();
                }

                Load_Data();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                Point location = dockPanelFilters.PointToScreen(Point.Empty);

                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (boolProcessRunning) return;  // Abort since process already running //

            dataSet_GC_Core.sp04074_GC_Gritting_Callout_Manager.Clear(); // Clear all existing data since the data picked up by the merge may be a smaller dataset due to any new filters in place //

            backgroundWorker1.RunWorkerAsync();
            boolProcessRunning = true;
            SetLoadingVisibility(true);
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Load_Data();
            if (boolProcessRunning)
            {
                //MessageBox.Show("Process already running.");
                return;
            }
            backgroundWorker1.RunWorkerAsync();
            boolProcessRunning = true;
            SetLoadingVisibility(true);
        }


        private void Load_Data()
        {
            if (boolProcessRunning) return;  // Abort since process already running //

            backgroundWorker1.RunWorkerAsync();
            boolProcessRunning = true;
            SetLoadingVisibility(true);
        }

        private void Populate_Visits()
        {
            intErrorValue = 0;  // Reset error flag //
            strErrorMessage = "";
            sdaDataMerge = new SqlDataAdapter();
            dsDataMerge = new DataSet("NewDataSet");

            if (i_str_selected_CallOutType_ids == null) i_str_selected_CallOutType_ids = "";

            string strTempDrilldownIDs = "";
            if (UpdateRefreshStatus > 0)
            {
                UpdateRefreshStatus = 0;
                // Merge just changed rows back in to dataset - no need to reload the full dataset //
                strTempDrilldownIDs = (string.IsNullOrWhiteSpace(i_str_AddedRecordIDs1) ? strEditedVisitIDs : i_str_AddedRecordIDs1.Replace(';', ','));
                if (string.IsNullOrEmpty(strTempDrilldownIDs))
                {
                    backgroundWorker1.CancelAsync();  // Nothing to load so abort cleanly //
                    return;
                }
            }
            else if (tabbedControlGroupFilterType.SelectedTabPageIndex == 1 && !string.IsNullOrWhiteSpace(VisitIDsMemoEdit.EditValue.ToString()))
            {
                strTempDrilldownIDs = VisitIDsMemoEdit.EditValue.ToString();
            }
            try
            {
                using (var conn = new SqlConnection(strConnectionStringREADONLY))
                {
                    // Load data into memory - Background worked will merge this on RunWorkerCompleted event //
                    conn.Open();
                    cmd = new SqlCommand("sp04074_GC_Gritting_Callout_Manager", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DrillDownIDs", strTempDrilldownIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", i_dtStart));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", i_dtEnd));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", i_str_selected_CallOutType_ids));
                    cmd.Parameters.Add(new SqlParameter("@CompanyFilter", i_str_selected_Company_ids));
                    sdaDataMerge = new SqlDataAdapter(cmd);
                    sdaDataMerge.Fill(dsDataMerge, "Table");
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == -2)  // SQL Server Time-Out //
                {
                    intErrorValue = -2;
                    return;
                }
                else  // Other SQL Server issue //
                {
                    intErrorValue = -3;
                    strErrorMessage = ex.Message.ToString();
                    return;
                }
            }
            catch (Exception ex)  // Some other issue //
            {
                intErrorValue = -1;
                strErrorMessage = ex.Message.ToString();
                return;
            }
        }


        private void LoadLinkedRecords()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GrittingCallOutID"])) + ',';
            }

            // Populate Linked Extra Costs //
            gridControl2.MainView.BeginUpdate();
            this.RefreshGridViewState2.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_GC_Core.sp04075_GC_Gritting_Callout_Linked_Extra_Costs.Clear();
            }
            else
            {
                sp04075_GC_Gritting_Callout_Linked_Extra_CostsTableAdapter.Fill(dataSet_GC_Core.sp04075_GC_Gritting_Callout_Linked_Extra_Costs, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl2.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl2.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ExtraCostID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }

            //Populate Linked Pictures //
            gridControl4.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_GC_Core.sp04092_GC_Gritting_Callout_Pictures_For_Callout.Clear();
            }
            else
            {
                sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter.Fill(dataSet_GC_Core.sp04092_GC_Gritting_Callout_Pictures_For_Callout, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), strDefaultPath, 0);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl4.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl4.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }

            if (xtraTabPage3.PageVisible)
            {
                // Populate CRM Contacts //
                gridControl15.MainView.BeginUpdate();
                this.RefreshGridViewState12.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.woodPlanDataSet.sp05089_CRM_Contacts_Linked_To_Record.Clear();
                }
                else
                {
                    sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.Fill(woodPlanDataSet.sp05089_CRM_Contacts_Linked_To_Record, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 24);
                    this.RefreshGridViewState12.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl15.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs12 != "")
                {
                    strArray = i_str_AddedRecordIDs12.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl15.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["CRMID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs12 = "";
                }
            }

            if (xtraTabPage4.PageVisible)
            {
                // Populate Linked Service Areas //
                gridControl6.MainView.BeginUpdate();
                this.RefreshGridViewState6.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04337_GC_Service_Areas_Linked_To_Gritting_Callout.Clear();
                }
                else
                {

                    sp04337_GC_Service_Areas_Linked_To_Gritting_CalloutTableAdapter.Fill(dataSet_GC_Core.sp04337_GC_Service_Areas_Linked_To_Gritting_Callout, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState6.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl6.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs6 != "")
                {
                    strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl6.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["GrittingServicedSiteID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs6 = "";
                }
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3, string strFurtherAction, string strNewIDs12, string strNewIDs6)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
            if (strNewIDs12 != "") i_str_AddedRecordIDs12 = strNewIDs12;
            if (strNewIDs6 != "") i_str_AddedRecordIDs12 = strNewIDs6;
            istr_FurtherAction = strFurtherAction;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Send Jobs Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_SendJobsButtonEnabled = true;
                        }
                        break;
                    case 2:  // Reassign Jobs Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_ReassignJobsButtonEnabled = true;
                        }
                        break;
                    case 3:  // Authorise Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_AuthoriseButtonEnabled = true;
                        }
                        break;
                    case 4:  // Pay Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_PayButtonEnabled = true;
                        }
                        break;
                   case 5:  // View SMS Errors Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_ViewSMSErrorsButtonEnabled = true;
                        }
                        break;       
                   case 6:  // Add Wizard Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_AddWizardButtonEnabled = true;
                        }
                        break;    
                   case 7:  // Manually Complete Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_ManuallyCompleteJobsEnabled = true;
                        }
                        break;
                   case 8:  // Show Column Chooser for GridControl 1 //
                        if (sfpPermissions.blRead)
                        {
                            iBool_EnableGridColumnChooser = true;
                        }
                        break;
                   case 9:  // Send No Proactive Gritting Messages Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_SendNoProactiveGrittingMessagesEnabled = true;
                        }
                        break;
                   case 10:  // Completion Sheet Edit Layout Button (on called child form) //    
                        if (sfpPermissions.blRead)
                        {
                            _iBool_CompletionSheetEditLayoutButtonEnabled = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            switch (i_int_FocusedGrid)
            {
                case 1:    // Gritting Callouts //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                            bbiBlockAdd.Enabled = false; // true;
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case 2:    // Linked Extra Costs //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            GridView viewParent = (GridView)gridControl1.MainView;
                            int[] intRowHandlesParent;
                            intRowHandlesParent = viewParent.GetSelectedRows();
                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case 3:    // Linked Pictures //
                    {
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            GridView viewParent = (GridView)gridControl1.MainView;
                            int[] intRowHandlesParent;
                            intRowHandlesParent = viewParent.GetSelectedRows();
                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case 15:  // CRM Contact //
                    view = (GridView)gridControl15.MainView;
                    intRowHandles = view.GetSelectedRows();
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        //if (intRowHandlesParent.Length >= 2)
                        //{
                        //    alItems.Add("iBlockAdd");
                        //    bbiBlockAdd.Enabled = true;
                        //}
                        //alItems.Add("iBlockAdd");
                        //bbiBlockAdd.Enabled = true;
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case 6:    // Linked Service Areas //
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            GridView viewParent = (GridView)gridControl1.MainView;
                            int[] intRowHandlesParent;
                            intRowHandlesParent = viewParent.GetSelectedRows();
                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);


            // Set Enabled Status of Main Toolbar Buttons //
            bbiSendJobs.Enabled = iBool_SendJobsButtonEnabled;
            bbiSelectJobsReadyToSend.Enabled = iBool_SendJobsButtonEnabled;
            bbiReassignJobs.Enabled = iBool_ReassignJobsButtonEnabled;
            bbiAuthorise.Enabled = iBool_AuthoriseButtonEnabled;
            bbiPay.Enabled = iBool_PayButtonEnabled;
            bbiViewSMSErrors.Enabled = iBool_ViewSMSErrorsButtonEnabled;
            bbiAddJobs.Enabled = iBool_AddWizardButtonEnabled;
            
            bsiManuallyComplete.Enabled = iBool_ManuallyCompleteJobsEnabled;
            bbiManuallyCompleteJobs.Enabled = (iBool_ManuallyCompleteJobsEnabled && intRowHandles.Length > 0);
            bbiSelectJobsToComplete.Enabled = iBool_ManuallyCompleteJobsEnabled;
            bsiCompletionSheet.Enabled = true;
            bsiKMLMapImage.Enabled = true;
            bbiSelectJobsForKMLCreation.Enabled = true;
            bbiCreateKMLAndMapImages.Enabled = (intRowHandles.Length > 0);
            bsiCompletionSheetCreate.Enabled = true;
            bbiSelectCompletionSheetJobs.Enabled = true;
            bbiCompletionSheet.Enabled = (intRowHandles.Length > 0);
            bbiPreviewCompletionSheets.Enabled = (intRowHandles.Length > 0);

            bbiSendNoProactiveMessages.Enabled = iBool_SendNoProactiveGrittingMessagesEnabled;
            
            // Set enabled status of CRM Contact Grid navigator custom buttons //
            view = (GridView)gridControl15.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl15.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl15.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl15.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView4 navigator custom buttons //
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView6 navigator custom buttons //
            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);

        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Callout //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                        frm_GC_Callout_Edit fChildForm = new frm_GC_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        try
                        {
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        catch (Exception)
                        {
                        }
                    }
                    break;
                case 2:     // Linked Extra Costs //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Extra_Cost_Edit fChildForm2 = new frm_GC_Callout_Extra_Cost_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "GrittingCallOutID"));

                            string strSelectedValue = "Site: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName")))
                                                        + "   Team: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName"))) ? "No Team" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName")))
                                                        + "   Callout Time: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime"))) ? "No Callout Time" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime")));
                            fChildForm2.strLinkedToRecordDescription = strSelectedValue;
                        }

                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 3:     // Linked Pictures //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Picture_Edit fChildForm2 = new frm_GC_Callout_Picture_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "GrittingCallOutID"));

                            string strSelectedValue = "Site: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName")))
                                                        + "   Team: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName"))) ? "No Team" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName")))
                                                        + "   Callout Time: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime"))) ? "No Callout Time" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime")));
                            fChildForm2.strLinkedToRecordDescription = strSelectedValue;
                        }

                        fChildForm2.intLinkedRecordTypeID = 0;  // 0 = Gritting, 1 = Snow //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 15:     // CRM Contacts //
                    {
                        if (!iBool_AllowAdd) return;

                        // Check if only one parent tender selected - if yes, pass it to child screen otherwise pass none //
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //

                        frm_Core_CRM_Contact_Edit fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        int intClientID = (intRowHandles.Length == 1 ? Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")) : 0);
                        string strClientName = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "ClientName")) ?? "Unknown Client";
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "GrittingCallOutID"));

                            string strSelectedValue = "Site: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName")))
                                                        + "   Team: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName"))) ? "No Team" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName")))
                                                        + "   Callout Time: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime"))) ? "No Callout Time" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime")));
                            fChildForm.strLinkedToRecordDesc = strSelectedValue;
                        }
                        fChildForm.intClientID = intClientID;
                        fChildForm.strClientName = strClientName;
                        fChildForm.intRecordTypeID = 24;  // Gritting Callout //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 6:     // Linked Service Areas //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Serviced_Area_Edit fChildForm2 = new frm_GC_Callout_Serviced_Area_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "GrittingCallOutID"));

                            string strSelectedValue = "Site: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName")))
                                                        + "   Team: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName"))) ? "No Team" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName")))
                                                        + "   Callout Time: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime"))) ? "No Callout Time" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime")));
                            fChildForm2.strLinkedToRecordDescription = strSelectedValue;
                        }

                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;

                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Gritting Callout //
                    {
                        if (!iBool_AllowAdd) return;

                        // Load Data into Dataset and pass into selection form for editing //
                        SqlDataAdapter sdaSites = new SqlDataAdapter();
                        DataSet dsSites = new DataSet("NewDataSet");
                        try
                        {
                            SqlConnection SQlConn = new SqlConnection(strConnectionString);
                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp04083_GC_Block_Add_Callout_Select_Sites", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@strRecordIDs", ""));
                            sdaSites = new SqlDataAdapter(cmd);
                            sdaSites.Fill(dsSites, "Table");
                            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

                            SQlConn.Close();
                            SQlConn.Dispose();

                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Block Adding the Callouts.\n\nMessage = [" + ex.Message + "].\n\nRefresh the Callout Manager screen then try again. If the problem persists, contact Technical Support.", "Block Add Gritting Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        frm_GC_Callout_Block_Add_Select_Sites fChildForm = new frm_GC_Callout_Block_Add_Select_Sites();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.dsSites = dsSites;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;
                        intCount = fChildForm.i_int_selected_count;
                        int intReactive = fChildForm.intReactive;

                        if (intCount <= 0) return;

                        #region AddRecords

                        // Add Records To Callout Table //
                        fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Block Adding Callouts...");
                        fProgress.Show();

                        DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter AddRecord = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                        AddRecord.ChangeConnectionString(strConnectionString);

                        int SiteGrittingContractID = 0;
		                int SubContractorID = 0;
		                int Reactive = 0;
		                int JobStatusID = 0;
		                DateTime CallOutDateTime = DateTime.Now;
		                decimal SaltUsed = (decimal)0.00;
		                decimal SaltCost = (decimal)0.00;
		                decimal SaltSell = (decimal)0.00;
		                decimal SaltVatRate = (decimal)0.00;
		                decimal TeamHourlyRate = (decimal)0.00;
		                decimal TeamCharge = (decimal)0.00;
		                int RecordedByStaffID = this.GlobalSettings.UserID;
		                int GritSourceLocationID = 0;
		                int GritSourceLocationTypeID = 0;
		                string ClientPONumber = "";
                        decimal decClientReactivePrice = (decimal)0.00;

                        int intUpdateProgressThreshhold = dsSites.Tables[0].Rows.Count / 10;
                        int intUpdateProgressTempCount = 0;
                        int intNewRecordCount = 0;

                        // Get Default VAT Rate //
                        decimal decVatRate = (decimal)0.00;
                        try
                        {
                            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            GetSetting.ChangeConnectionString(strConnectionString);
                            decVatRate = Convert.ToDecimal(GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate"));
                        }
                        catch (Exception)
                        {
                            decVatRate = (decimal)0.00;
                        }

                        i_str_AddedRecordIDs1 = "";
                        foreach (DataRow dr in dsSites.Tables[0].Rows)
                        {
                            SiteGrittingContractID = Convert.ToInt32(dr["SiteGrittingContractID"]);
		                    SubContractorID = Convert.ToInt32(dr["SubContractorID"]);
                            JobStatusID = (SubContractorID <= 0 ? 30 : 50);
		                    SaltUsed = Convert.ToDecimal(dr["SaltUsed"]);
		                    SaltCost = Convert.ToDecimal(dr["SaltCost"]);
		                    SaltSell = Convert.ToDecimal(dr["SaltSell"]);
		                    SaltVatRate = Convert.ToDecimal(dr["SaltVatRate"]);
		                    TeamHourlyRate = Convert.ToDecimal(dr["TeamHourlyRate"]);
		                    TeamCharge = Convert.ToDecimal(dr["TeamCharge"]);
		                    GritSourceLocationID = Convert.ToInt32(dr["GritSourceLocationID"]);
                            ClientPONumber = dr["ClientPONumber"].ToString();
                            Reactive = intReactive;
                            decClientReactivePrice = Convert.ToDecimal(dr["ClientReactivePrice"]);
                            try
                            {
                                i_str_AddedRecordIDs1 += AddRecord.sp04084_GC_Gritting_Callout_Insert_From_Block_Add(SiteGrittingContractID, SubContractorID, Reactive, JobStatusID, CallOutDateTime, SaltUsed, SaltCost, SaltSell, SaltVatRate, TeamHourlyRate, TeamCharge, RecordedByStaffID, GritSourceLocationID, GritSourceLocationTypeID, ClientPONumber, decClientReactivePrice, decVatRate).ToString() + ';';  // Add Row to database //  
                                intNewRecordCount++;
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Block Adding the Callouts.\n\nMessage = [" + ex.Message + "].\n\nRefresh the Callout Manager screen then try again. If the problem persists, contact Technical Support.", "Block Add Gritting Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                            
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }

                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        Load_Data();  // Load new records into grid and highlight //

                        if (intNewRecordCount > 0)
                        {
                            // Display success form and ask what the user wants to do next //
                            frm_GC_Callout_Block_Add_Next fCompletion = new frm_GC_Callout_Block_Add_Next();
                            fCompletion.intRecordCount = intNewRecordCount;
                            fCompletion.ShowDialog();
                            switch (fCompletion.strReturnedValue)
                            {
                                case "Close":
                                    break;
                                case "Edit":
                                    Edit_Record();
                                    break;
                                case "BlockEdit":
                                    Block_Edit();
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No Callouts Created.", "Block Add Gritting Callouts", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        #endregion

                        break;
                    }
                case 2:     // Related Documents Link //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Extra_Cost_Edit fChildForm2 = new frm_GC_Callout_Extra_Cost_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 3:     // Related Picture //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Picture_Edit fChildForm2 = new frm_GC_Callout_Picture_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intLinkedRecordTypeID = 0;  // 0 = Gritting, 1 = Snow //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 6:     // Linked Site Service Areas //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Serviced_Area_Edit fChildForm2 = new frm_GC_Callout_Serviced_Area_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ',');
                        }
                        strEditedVisitIDs = sb.ToString();  // Put IDs into the instance var so we can merge changes if any are made without reloading the whole screen //

                        frm_GC_Callout_Edit fChildForm = new frm_GC_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strEditedVisitIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Linked Extra Costs //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ExtraCostID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Extra_Cost_Edit fChildForm2 = new frm_GC_Callout_Extra_Cost_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 3:     // Linked Pictures //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Picture_Edit fChildForm2 = new frm_GC_Callout_Picture_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 15:  // Linked CRM Contacts //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl15.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_Core_CRM_Contact_Edit fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 6:     // Linked Service Areas //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingServicedSiteID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Serviced_Area_Edit fChildForm2 = new frm_GC_Callout_Serviced_Area_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingCallOutID")) + ',');
                        }
                        strEditedVisitIDs = sb.ToString();  // Put IDs into the instance var so we can merge changes if any are made without reloading the whole screen //
                        
                        frm_GC_Callout_Edit fChildForm = new frm_GC_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strEditedVisitIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Linked Extra Costs //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ExtraCostID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Extra_Cost_Edit fChildForm2 = new frm_GC_Callout_Extra_Cost_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 3:     // Linked Pictures //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Picture_Edit fChildForm2 = new frm_GC_Callout_Picture_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 15:  // Linked CRM Contact //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl15.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_Core_CRM_Contact_Edit fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 6:     // Linked Site Service Areas //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingServicedSiteID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Serviced_Area_Edit fChildForm2 = new frm_GC_Callout_Serviced_Area_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Callouts //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Callout" : Convert.ToString(intRowHandles.Length) + " Callouts") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Callout" : "these Callouts") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            Array.Reverse(intRowHandles);  // Reverse the order so the last record is deleted first //
                            StringBuilder sb = new StringBuilder();
                            foreach (int intRowHandle in intRowHandles)
                            {
                                sb.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "GrittingCallOutID")) + ",");
                                view.DeleteRow(intRowHandle);
                            }

                            using (var RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp03002_EP_Client_Delete("gc_gritting_callout", sb.ToString());  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            view.DeleteSelectedRows();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 2:  // Linked Extra Costs //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more extra costs to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Extra Cost" : Convert.ToString(intRowHandles.Length) + " Extra Costs") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Extra Cost" : "these Extra Costs") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ExtraCostID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp03002_EP_Client_Delete("gc_gritting_callout_extra_cost", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            //LoadLinkedRecords();
                            Load_Data();  // ***** Load Top level as the total costs for the parent callout(s) will have been updated ***** //

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 3:  // Linked Pictures //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Picture" : Convert.ToString(intRowHandles.Length) + " Pictures") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Picture" : "these Pictures") + " will no longer be available for selection!\n\nNote: The physical picture files will still exist within the Linked Pictures Folder.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp03002_EP_Client_Delete("gc_gritting_callout_picture", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 15:  // Linked CRM Contacts //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl15.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked CRM Contacts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked CRM Contact" : Convert.ToString(intRowHandles.Length) + " Linked CRM Contacts") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Linked CRM Contact" : "these Linked CRM Contacts") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ",";
                            }
                            DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            try
                            {
                                RemoveRecords.sp01000_Core_Delete("customer_contact", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager.IsSplashFormVisible)
                            {
                                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 6:  // Linked Site Service Areas //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Site Service Areas to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Site Service Area" : Convert.ToString(intRowHandles.Length) + " Linked Site Service Areas") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Linked Site Service Area" : "these Linked Site Service Areas") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingServicedSiteID")) + ",";
                            }
                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            try
                            {
                                RemoveRecords.sp03002_EP_Client_Delete("gc_gritting_callout_serviced_site", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager.IsSplashFormVisible)
                            {
                                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void Set_Selected_Count()
        {
            GridView view = (GridView)gridControl1.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            if (intSelectedCount == 0)
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Callout Selected" : "<color=red>" + intSelectedCount.ToString() + " Callouts</Color> Selected");
                }
                bsiSelectedCount.Caption = strText;
            }
        }



        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Callouts Available";
                    break;
                case "gridView2":
                    message = "No Linked Extra Costs Available - Select one or more Callouts to view Linked Extra Costs";
                    break;
                case "gridView4":
                    message = "No Linked Pictures Available - Select one or more Callouts to view Linked Pictures";
                    break;
                case "gridView15":
                    message = "No CRM Contacts - Select one or more Gritting Callouts to view Linked CRM Contacts";
                    break;
                case "gridView6":
                    message = "No Linked Service Area Available - Select one or more Callouts to view Linked Service Areas";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedRecords();
                    view = (GridView)gridControl2.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl4.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl15.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl6.MainView;
                    view.ExpandAllGroups();
                    Set_Selected_Count();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                     else if ("reload_selected".Equals(e.Button.Tag))
                    {
                        Reload_Selected_Records_Only();
                    }
                  break;
                default:
                    break;
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "SubContractorName")
            {
                switch (view.GetRowCellValue(e.RowHandle, "SubContractorName").ToString())
                {
                    case "":
                        //e.Appearance.BackColor = Color.LightCoral;
                        //e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }
            else if (e.Column.FieldName == "SiteAddress1")
            {
                if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "SiteAddress1").ToString()))
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "SiteLat")
            {
                if (Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "SiteLat")) == (double)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "SiteLong")
            {
                if (Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "SiteLong")) == (double)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }

        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "ClientSignatureFileName":
                        if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "ClientSignatureFileName").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "SiteEmail":
                        if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "SiteEmail").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "KMLFile":
                        if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "KMLFile").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "KMLImageFile":
                        if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "KMLImageFile").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "CompletionSheetFile":
                        if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "CompletionSheetFile").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridView1_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Alert" && e.IsGetData)
            {
                e.Value = bmpBlank;
                GridView view = (GridView)sender;
                DateTime dtEnd = DateTime.Now;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                if (Convert.ToInt32(view.GetRowCellValue(rHandle, "JobStatusID")) == 70)  // Sent - Awaiting Response //
                {
                    if (!string.IsNullOrEmpty(view.GetRowCellValue(rHandle, "TextSentTime").ToString()))
                    {
                        DateTime dtStart = Convert.ToDateTime(view.GetRowCellValue(rHandle, "TextSentTime"));
                        TimeSpan elapsedTime = dtEnd.Subtract(dtStart);
                        if (elapsedTime.TotalMinutes > 15) e.Value = bmpAlert;
                    }
                }
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }
        
        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            // Generic version of code not used as we need to disable Column Chooser Customize //
            GridView view = (GridView)sender;
            ExtendedGridMenu egmMenu = new ExtendedGridMenu(view);

            if (e.MenuType == GridMenuType.Column)
            {   
                DXMenuItem miCustomize = GetItemByStringId(e.Menu, GridStringId.MenuColumnColumnCustomization);
                if (miCustomize != null) miCustomize.Enabled = iBool_EnableGridColumnChooser;
            }

            egmMenu.PopupMenuShowing(sender, e);
        }
        private DXMenuItem GetItemByStringId(DXPopupMenu menu, GridStringId id)
        {
            foreach (DXMenuItem item in menu.Items)
            {
                if (item.Caption == GridLocalizer.Active.GetLocalizedString(id)) return item;
            }
            return null;
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "ClientSignatureFileName":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("ClientSignatureFileName").ToString())) e.Cancel = true;
                    break;
                case "SiteEmail":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SiteEmail").ToString())) e.Cancel = true;
                    break;
                case "KMLFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("KMLFile").ToString())) e.Cancel = true;
                    break;
                case "KMLImageFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("KMLImageFile").ToString())) e.Cancel = true;
                    break;
                case "CompletionSheetFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("CompletionSheetFile").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strPath = "";
            switch (view.FocusedColumn.FieldName)
            {
                case "ClientSignatureFileName":
                    {
                        try
                        {
                            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            GetSetting.ChangeConnectionString(strConnectionString);
                            strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingSavedCalloutSignatureFolder").ToString();
                        }
                        catch (Exception)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Signatures (from the System Configuration Screen).\n\nPlease try again. If the problem persists, contact Technical Support.", "Get Linked Client Signatures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        if (!strPath.EndsWith("\\")) strPath += "\\";
                        string strFile = view.GetRowCellValue(view.FocusedRowHandle, "ClientSignatureFileName").ToString();
                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No Client Signature Linked - unable to proceed.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            System.Diagnostics.Process.Start(strPath + strFile);
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view client signature: " + strPath + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case "SiteEmail":
                    {
                        string strEmailAddress = view.GetRowCellValue(view.FocusedRowHandle, "SiteEmail").ToString();
                        if (string.IsNullOrEmpty(strEmailAddress))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No Email Address Available - unable to proceed.", "Email Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            System.Diagnostics.Process.Start("mailto:" + strEmailAddress);
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to email: " + strEmailAddress + ".", "Email Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedFile_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            
            int intGrittingCallOutID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "GrittingCallOutID"));
            if (intGrittingCallOutID <= 0) return;

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionStringREADONLY);
            switch (view.FocusedColumn.FieldName)
            {
                case "KMLFile":
                    {
                        string strFile = view.GetRowCellValue(view.FocusedRowHandle, "KMLFile").ToString();
                        if (string.IsNullOrWhiteSpace(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No KML File Linked - unable to proceed.", "View Map Image", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strPath = "";
                        try
                        {
                            strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingLocationTrackKMLFilesFolder").ToString();
                        }
                        catch (Exception)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the saved KML files.\n\nUnable to Proceed.", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strFullPath = Path.Combine(strPath, strFile);

                        var fChildForm = new frm_Core_KML_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInKMLFileName = strFullPath;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case "KMLImageFile":
                    {
                        string strFile = view.GetRowCellValue(view.FocusedRowHandle, "KMLImageFile").ToString();
                        if (string.IsNullOrWhiteSpace(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No Mapping Image Linked - unable to proceed.", "View Map Image", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strPath = "";
                        try
                        {
                            strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingLocationTrackImageFilesFolder").ToString();
                        }
                        catch (Exception)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the saved KML Image files.\n\nUnable to Proceed.", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strFullPath = Path.Combine(strPath, strFile);
                        try
                        {
                            System.Diagnostics.Process.Start(strFullPath);
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Mapping Image: " + strFullPath + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Map Image", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }

                    }
                    break;
                case "CompletionSheetFile":
                    {
                        string strFile = view.GetRowCellValue(view.FocusedRowHandle, "CompletionSheetFile").ToString();
                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No Completion Sheet Linked - unable to proceed. Click the Create Completion Sheet menu item for this Gritting Callout before trying again.", "View Gritting Callout Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strPath = view.GetRowCellValue(view.FocusedRowHandle, "CompletionSheetFile").ToString();
                        try
                        {
                            strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutCompletionSheetsFolder").ToString();
                        }
                        catch (Exception)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the saved Gritting Callout Completion Sheets.\n\nUnable to Proceed.", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strFullPath = Path.Combine(strPath, strFile);
                        if (!File.Exists(strFullPath))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Completion Sheet File is missing - unable to proceed. Click the Create Completion Sheet menu item for this Gritting Callout before trying again.", "View Gritting Callout Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        try
                        {
                            if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                            {
                                frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                fChildForm.strPDFFile = strFullPath;
                                fChildForm.MdiParent = this.MdiParent;
                                fChildForm.GlobalSettings = this.GlobalSettings;
                                fChildForm.Show();
                            }
                            else
                            {
                                System.Diagnostics.Process.Start(strFullPath);
                            }
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Gritting Callout Completion Report: " + strFullPath + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Gritting Callout Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void Reload_Selected_Records_Only()
        {
            strEditedVisitIDs = Get_Selected_IDs((GridView)gridControl1.MainView, "GrittingCallOutID");
            if (string.IsNullOrWhiteSpace(strEditedVisitIDs)) return;
            UpdateRefreshStatus = 1;
            //Set_DictionaryDataRefreshes_All_Value(dictionaryDataRefreshesMain, true);
            Load_Data();
        }
        private string Get_Selected_IDs(GridView view, string ColumnName)
        {
            int intRowCount = view.DataRowCount;
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0) return "";
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellValue(intRowHandle, ColumnName).ToString() + ",");
            }
            return sb.ToString();
        }

        #endregion
 

        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView4_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "PicturePath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "PicturePath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView4_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "PicturePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("PicturePath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
             }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView15 - CRM Contacts

        private void gridView15_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView15_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 15;
            SetMenuStatus();
        }

        private void gridView15_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 15;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl15_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView6

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 6;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Callout Status Filter Panel

        private void btnGritCalloutFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 3;
            //SetMenuStatus();
        }

        private void popupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_CallOutType_ids = "";    // Reset any prior values first //
            i_str_selected_CallOutType_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CallOutType_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CallOutType_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CallOutType_names;
        }

        #endregion


        #region DateTime

        private void dateEditFromDate_EditValueChanged(object sender, EventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (de.DateTime >= DateTime.MinValue && de.DateTime <= DateTime.MaxValue)
            {
                i_dtStart = (de.DateTime >= DateTime.MinValue && de.DateTime <= DateTime.MaxValue ? de.DateTime : DateTime.MinValue);
            }
        }

        private void dateEditToDate_EditValueChanged(object sender, EventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (de.DateTime >= DateTime.MinValue && de.DateTime <= DateTime.MaxValue)
            {
                i_dtEnd = (de.DateTime >= DateTime.MinValue && de.DateTime <= DateTime.MaxValue ? de.DateTime : DateTime.MaxValue);
            }
        }

        #endregion


        #region Company Filter Panel

        private void popupContainerEditCompanies_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCompanies_Get_Selected();
        }

        private void btnCompanyFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditCompanies_Get_Selected()
        {
            i_str_selected_Company_ids = "";    // Reset any prior values first //
            i_str_selected_Company_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Company_ids += Convert.ToString(view.GetRowCellValue(i, "CompanyID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Company_names = Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Company_names += ", " + Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Company_names;
        }

        #endregion


        private void btnFormatData_Click(object sender, EventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Formatting data...");

            VisitIDsMemoEdit.EditValue = BaseObjects.ExtensionFunctions.FormatStringToCSV(VisitIDsMemoEdit.EditValue.ToString(), true);

            if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
        }


        private void checkEditColourCode_CheckedChanged(object sender, EventArgs e)
        {
            SetGridFormatConditions();
        }

        private void SetGridFormatConditions()
        {
            CheckEdit ce = (CheckEdit)checkEditColourCode;
            if (ce.Checked)
            {
                {
                    GridFormatRule gridFormatRule = new GridFormatRule();
                    FormatConditionRuleExpression formatConditionRuleExpression = new FormatConditionRuleExpression();
                    gridFormatRule.Column = colJobStatusID;
                    gridFormatRule.ApplyToRow = true;
                    formatConditionRuleExpression.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                    formatConditionRuleExpression.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                    formatConditionRuleExpression.Appearance.ForeColor = Color.Black;
                    formatConditionRuleExpression.Expression = "[JobStatusID] == 20";
                    gridFormatRule.Rule = formatConditionRuleExpression;
                    gridView1.FormatRules.Add(gridFormatRule);
                }
                {
                    GridFormatRule gridFormatRule = new GridFormatRule();
                    FormatConditionRuleExpression formatConditionRuleExpression = new FormatConditionRuleExpression();
                    gridFormatRule.Column = colJobStatusID;
                    gridFormatRule.ApplyToRow = true;
                    formatConditionRuleExpression.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                    formatConditionRuleExpression.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                    formatConditionRuleExpression.Appearance.ForeColor = Color.Black;
                    formatConditionRuleExpression.Expression = "[JobStatusID] == 40";
                    gridFormatRule.Rule = formatConditionRuleExpression;
                    gridView1.FormatRules.Add(gridFormatRule);
                }
                {
                    GridFormatRule gridFormatRule = new GridFormatRule();
                    FormatConditionRuleExpression formatConditionRuleExpression = new FormatConditionRuleExpression();
                    gridFormatRule.Column = colJobStatusID;
                    gridFormatRule.ApplyToRow = true;
                    formatConditionRuleExpression.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                    formatConditionRuleExpression.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                    formatConditionRuleExpression.Appearance.ForeColor = Color.Black;
                    formatConditionRuleExpression.Expression = "[JobStatusID] == 60";
                    gridFormatRule.Rule = formatConditionRuleExpression;
                    gridView1.FormatRules.Add(gridFormatRule);
                }
                {
                    GridFormatRule gridFormatRule = new GridFormatRule();
                    FormatConditionRuleExpression formatConditionRuleExpression = new FormatConditionRuleExpression();
                    gridFormatRule.Column = colJobStatusID;
                    gridFormatRule.ApplyToRow = true;
                    formatConditionRuleExpression.Appearance.BackColor = Color.FromArgb(255, 255, 128);
                    formatConditionRuleExpression.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                    formatConditionRuleExpression.Appearance.ForeColor = Color.Black;
                    formatConditionRuleExpression.Expression = "[JobStatusID] == 10";
                    gridFormatRule.Rule = formatConditionRuleExpression;
                    gridView1.FormatRules.Add(gridFormatRule);
                }
                {
                    GridFormatRule gridFormatRule = new GridFormatRule();
                    FormatConditionRuleExpression formatConditionRuleExpression = new FormatConditionRuleExpression();
                    gridFormatRule.Column = colJobStatusID;
                    gridFormatRule.ApplyToRow = true;
                    formatConditionRuleExpression.Appearance.BackColor = Color.FromArgb(255, 255, 128);
                    formatConditionRuleExpression.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                    formatConditionRuleExpression.Appearance.ForeColor = Color.Black;
                    formatConditionRuleExpression.Expression = "[JobStatusID] == 30";
                    gridFormatRule.Rule = formatConditionRuleExpression;
                    gridView1.FormatRules.Add(gridFormatRule);
                }
                {
                    GridFormatRule gridFormatRule = new GridFormatRule();
                    FormatConditionRuleExpression formatConditionRuleExpression = new FormatConditionRuleExpression();
                    gridFormatRule.Column = colJobStatusID;
                    gridFormatRule.ApplyToRow = true;
                    formatConditionRuleExpression.Appearance.BackColor = Color.FromArgb(192, 255, 192);
                    formatConditionRuleExpression.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                    formatConditionRuleExpression.Appearance.ForeColor = Color.Black;
                    formatConditionRuleExpression.Expression = "[JobStatusID] == 50";
                    gridFormatRule.Rule = formatConditionRuleExpression;
                    gridView1.FormatRules.Add(gridFormatRule);
                }

                /*
                                StyleFormatCondition condition1 = new DevExpress.XtraGrid.StyleFormatCondition();
                                condition1.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                                condition1.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                                condition1.Appearance.Options.UseBackColor = true;
                                condition1.Appearance.ForeColor = Color.Black;
                                condition1.ApplyToRow = true;
                                condition1.Condition = FormatConditionEnum.Expression;
                                condition1.Expression = "[JobStatusID] == 20";
                                gridView1.FormatConditions.Add(condition1);

                                StyleFormatCondition condition2 = new DevExpress.XtraGrid.StyleFormatCondition();
                                condition2.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                                condition2.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                                condition2.Appearance.Options.UseBackColor = true;
                                condition2.Appearance.ForeColor = Color.Black;
                                condition2.ApplyToRow = true;
                                condition2.Condition = FormatConditionEnum.Expression;
                                condition2.Expression = "[JobStatusID] == 40";
                                gridView1.FormatConditions.Add(condition2);

                                StyleFormatCondition condition3 = new DevExpress.XtraGrid.StyleFormatCondition();
                                condition3.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                                condition3.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                                condition3.Appearance.Options.UseBackColor = true;
                                condition3.Appearance.ForeColor = Color.Black;
                                condition3.ApplyToRow = true;
                                condition3.Condition = FormatConditionEnum.Expression;
                                condition3.Expression = "[JobStatusID] == 60";
                                gridView1.FormatConditions.Add(condition3);


                                StyleFormatCondition condition4 = new DevExpress.XtraGrid.StyleFormatCondition();
                                condition4.Appearance.BackColor = Color.FromArgb(255, 255, 128);
                                condition4.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                                condition4.Appearance.Options.UseBackColor = true;
                                condition4.Appearance.ForeColor = Color.Black;
                                condition4.ApplyToRow = true;
                                condition4.Condition = FormatConditionEnum.Expression;
                                condition4.Expression = "[JobStatusID] == 10";
                                gridView1.FormatConditions.Add(condition4);

                                StyleFormatCondition condition5 = new DevExpress.XtraGrid.StyleFormatCondition();
                                condition5.Appearance.BackColor = Color.FromArgb(255, 255, 128);
                                condition5.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                                condition5.Appearance.Options.UseBackColor = true;
                                condition5.Appearance.ForeColor = Color.Black;
                                condition5.ApplyToRow = true;
                                condition5.Condition = FormatConditionEnum.Expression;
                                condition5.Expression = "[JobStatusID] == 30";
                                gridView1.FormatConditions.Add(condition5);


                                StyleFormatCondition condition6 = new DevExpress.XtraGrid.StyleFormatCondition();
                                condition6.Appearance.BackColor = Color.FromArgb(192, 255, 192);
                                condition6.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                                condition6.Appearance.Options.UseBackColor = true;
                                condition6.Appearance.ForeColor = Color.Black;
                                condition6.ApplyToRow = true;
                                condition6.Condition = FormatConditionEnum.Expression;
                                condition6.Expression = "[JobStatusID] == 50";
                                gridView1.FormatConditions.Add(condition6);
                 */
            }
            else
            {
                gridView1.FormatRules.Clear();
            }

        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) LoadLinkedRecords();
        }


        private void bbiSelectJobsReadyToSend_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 50 (Ready to send) //
            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Selecting...");

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 1 && Convert.ToInt32(view.GetRowCellValue(i, "PdaID")) <= 0)
                {
                    view.UnselectRow(i);
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 1 && Convert.ToInt32(view.GetRowCellValue(i, "AttendanceOrder")) == 0)
                {
                    view.UnselectRow(i);
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 1 && Convert.ToDouble(view.GetRowCellValue(i, "SiteLat")) == 0.00 && Convert.ToDouble(view.GetRowCellValue(i, "SiteLong")) == 0.00)
                {
                    view.UnselectRow(i);
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 50 && !string.IsNullOrEmpty(view.GetRowCellValue(i, "GritMobileTelephoneNumber").ToString())) view.SelectRow(i);          
            }
            view.EndSelection();
            view.EndUpdate();
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
        }

        private void bbiSendJobs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SendJobs();
        }

        private void SendJobs()
        {
            // First... Check selected jobs are all appropriate - Status of 50 (Ready to send) - if not, de-select them //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to send before proceeding.", "Send Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Selected Jobs...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.SelectedRowsCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < intRowHandles.Length; i++)
            {
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
                if (Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 1 && Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "PdaID")) <= 0)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [No PDA ID] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID")) != 50)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [Wrong Status] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "SiteGrittingContractID")) == 0)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [Missing SiteGrittingContractID] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "SubContractorID")) == 0)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [Missing SubContractorID] //
                    continue;
                }

                if (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[i], "GritMobileTelephoneNumber").ToString()))
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [Its needs to be sent via text message to Team and No Phone Number is present] //
                    continue;
                }

                if (Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 1 && Convert.ToDouble(view.GetRowCellValue(i, "SiteLat")) == 0.00 && Convert.ToDouble(view.GetRowCellValue(i, "SiteLong")) == 0.00)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [missing Lat \ Long] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 1 && Convert.ToInt32(view.GetRowCellValue(i, "AttendanceOrder")) == 0)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [missing Job Order] //
                    continue;
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to send before proceeding.\n\nNote: This process de-selects any selected jobs which are not ready to send...\n\nJobs must have:-\n==> Site\n==> Team\n==> Appropriate Status\n==> Latitude and Longitude\n==> Team must have a Grit Mobile Telephone Number\n==> If the Site if a Floating Site, the Job must have a Route Order.", "Send Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

            // Get DB Settings for sending emails //
            string strConfirmationBodyFile = "";
            string strEmailFrom = "";
            string strConfirmationEmailSubjectLine = "";
            string strCCToEmailAddress = "";
            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp04118_GC_Team_Email_System_Setting", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
            DataSet dsSettings = new DataSet("NewDataSet");
            sdaSettings.Fill(dsSettings, "Table");

            if (dsSettings.Tables[0].Rows.Count != 1)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Team Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            DataRow dr1 = dsSettings.Tables[0].Rows[0];
            strConfirmationBodyFile = dr1["BodyFileName"].ToString();
            strEmailFrom = dr1["EmailFrom"].ToString();
            strConfirmationEmailSubjectLine = dr1["SubjectLine"].ToString();
            strCCToEmailAddress = dr1["CCToName"].ToString();
            strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
            strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
            strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
            strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

            if (string.IsNullOrEmpty(strConfirmationBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strSMTPMailServerAddress))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the email settings (Email Layout File, From Email Address and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to send jobs so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Job" : intRowHandles.Length.ToString() + " Jobs") + " selected for sending to teams.\n\nProceed?", "Send Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Sending Jobs...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            string strSubContractorIDs = ",";
            string strTextCallOutIDs = "";
            string strPDACallOutIDs = "";
            string strAllJobIDs = "";
            int intSentCount = intRowHandles.Length;

            string strSitesRequiringGrit = "";
            int intSiteID = 0;
            string strFoundMatchDetails = "";
            int intMatchingRecordID = 0;
            string strMatchingTeamName = "";
            int intMatchingTeamID = 0;
            string strMatchingTeamMobile = "";
            string strLinkedTextMessage1 = "";
            string strLinkedTextMessage2 = "";
            string strTuncatedSiteID = "";
            string strTuncatedClientName = "";
            string strTuncatedSiteName = "";
            string strTuncatedSubContractorName = "";
            string strTruncatedPhoneNumber = "";
            DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter GetLinkedCallout = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
            GetLinkedCallout.ChangeConnectionString(strConnectionString);
            DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter StoreLinks = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
            StoreLinks.ChangeConnectionString(strConnectionString);

            bool boolTextServiceAvailable = true;
            bool boolTextServiceLoginDetailsAvailable = true;
            try
            {
                // Get Unique list of Teams to send jobs, a list of jobs to be sent via text message and a list of jobs to be sent to PDAs //
                foreach (int intRowHandle in intRowHandles)
                {
                    if (!strSubContractorIDs.Contains(',' + view.GetRowCellValue(intRowHandle, "SubContractorID").ToString() + ',')) strSubContractorIDs += view.GetRowCellValue(intRowHandle, "SubContractorID").ToString() + ',';
                    if (view.GetRowCellValue(intRowHandle, "GritJobTransferMethod").ToString() == "PDA")
                    {
                        strPDACallOutIDs += view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + ",";
                    }
                    else  // Text //
                    {
                        strTextCallOutIDs += view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + ",";
                    }
                    strAllJobIDs += view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + ",";
                }
                strSubContractorIDs = strSubContractorIDs.Remove(0, 1);  // Remove dummy ',' at start //
                if (string.IsNullOrEmpty(strSubContractorIDs))  // Nothing available for sending so abort //
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                // Check for internet Connectivity to Text Service //
                boolTextServiceAvailable = BaseObjects.PingTest.Ping("textanywhere.net");

                if (!boolTextServiceAvailable)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the Text Anywhere service - NO TEXT JOBS SENT.\n\nThe problem may be with your internet connection or with the Text Anywhere service. Please check then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                #region SendTextJobs

                string strTextServiceUsername = "";
                string strTextServicePassword = "";
                if (boolTextServiceAvailable && !string.IsNullOrEmpty(strAllJobIDs)) // strTextCallOutIDs - this was used when text messages were sent to just teams taking text messages (now texts also goes to PDA users) //
                {
 
                    // Get Settings from System Settings table //
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strTextServiceUsername = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "TextAnywhereUsername").ToString();
                    strTextServicePassword = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "TextAnywherePassword").ToString();
                    if (string.IsNullOrEmpty(strTextServiceUsername) || string.IsNullOrEmpty(strTextServicePassword)) boolTextServiceLoginDetailsAvailable = false;
                    if (!boolTextServiceLoginDetailsAvailable)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the Text Anywhere service [Missing Connection Credentials] - NO TEXT JOBS SENT.\n\nPlease contact Technical Support.", "Get Text Service Login Credentials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // We have connection info for Text Service //
                    {
                        // Populate resolved Jobs to go as text messages //
                        SqlDataAdapter sdaTextJobs = new SqlDataAdapter();
                        DataSet dsTextJobs = new DataSet("NewDataSet");
                        cmd = new SqlCommand("sp04103_GC_Get_Callouts_For_Texting_To_Teams", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.Add(new SqlParameter("@CallOutIDs", strTextCallOutIDs));  // Just Text Jobs //
                        cmd.Parameters.Add(new SqlParameter("@CallOutIDs", strAllJobIDs));  // This has been changed so text message sent to all jobs regardless if they use Text or PDA method. //      
                        sdaTextJobs = new SqlDataAdapter(cmd);
                        sdaTextJobs.Fill(dsTextJobs, "Table");
                        cmd = null;
                        int intCurrentReactiveStatus = 0;
                        int intLastReactiveStatus = -1;
                        int intCurrentSubContractorID = 0;
                        int intLastSubContractorID = -1;
                        int intTextMessageMaxLength = 117;  // 160 - longest pre-amble [Reactive 43, Proactive = 42] --
                        string strPhoneNumber = "";
                        StringBuilder sb = new StringBuilder();
                        string strCurrentJob = "";
                        string strCallOutIDs = "";
                        foreach (DataRow dr in dsTextJobs.Tables[0].Rows)
                        {
                            intCurrentReactiveStatus = Convert.ToInt32(dr["Reactive"]);
                            intCurrentSubContractorID = Convert.ToInt32(dr["SubContractorID"]);
                            strCurrentJob = "\n" + dr["TextToSend"].ToString();
                            if ((sb.Length + strCurrentJob.Length > intTextMessageMaxLength) || (intLastSubContractorID != -1 && intLastSubContractorID != intCurrentSubContractorID) || (intLastReactiveStatus != -1 && intLastReactiveStatus != intCurrentReactiveStatus))
                            {
                                // Either the maximum text message length will be reached or SubContractor ID is changing or Reactive status is changing so send text message //
                                SendMessage(sb.ToString(), intLastReactiveStatus, strPhoneNumber, strTextServiceUsername, strTextServicePassword, strCallOutIDs, intLastSubContractorID);
                                sb = new StringBuilder();
                                strCallOutIDs = "";
                            }
                            sb.Append(strCurrentJob);

                            intLastReactiveStatus = intCurrentReactiveStatus;  // Store last value to see if it changes on next iteration of loop //
                            intLastSubContractorID = intCurrentSubContractorID;  // Store last value to see if it changes on next iteration of loop //
                            strPhoneNumber = dr["GritMobileTelephoneNumber"].ToString();
                            strCallOutIDs += dr["GrittingCallOutID"].ToString() + ",";
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }
                        if (sb.Length > 0)  // A final text message has been started so send it now //
                        {
                            SendMessage(sb.ToString(), intLastReactiveStatus, strPhoneNumber, strTextServiceUsername, strTextServicePassword, strCallOutIDs, intLastSubContractorID);
                        }
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                    }
                }
                #endregion

                #region SendPDAJobs

                if (boolTextServiceAvailable && !string.IsNullOrEmpty(strPDACallOutIDs))
                {
                    fProgress = new frmProgress(0);
                    fProgress.UpdateCaption("Sending Device Jobs...");
                    fProgress.Show();
                    System.Windows.Forms.Application.DoEvents();

                    // Send list of job IDs through to Database SP which will send the jobs to the PDA specific tables //
                    try
                    {
                        DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter CreatePDAJobs = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                        CreatePDAJobs.ChangeConnectionString(strConnectionString);
                        CreatePDAJobs.sp04133_GC_Gritting_Add_Job_For_PDA(strPDACallOutIDs);
                    }
                    catch (Exception)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while sending the Device Jobs.\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Device Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                }
                #endregion

                #region SendTeamEmails

                fProgress = new frmProgress(0);
                fProgress.UpdateCaption("Sending Teams Emails...");
                fProgress.Show();
                System.Windows.Forms.Application.DoEvents();

                // Populate resolved Jobs to go as emails //
                SqlDataAdapter sdaEmailJobs = new SqlDataAdapter();
                DataSet dsEmailJobs = new DataSet("NewDataSet");
                cmd = new SqlCommand("sp04116_GC_Get_Callouts_For_Emailing_To_Teams", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CallOutIDs", strAllJobIDs));
                sdaEmailJobs = new SqlDataAdapter(cmd);
                sdaEmailJobs.Fill(dsEmailJobs, "Table");

                char[] delimiters = new char[] { ',' };
                string[] strArray = strSubContractorIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intUpdateProgressThreshhold = strArray.Length / 10;
                intUpdateProgressTempCount = 0;

                if (strArray.Length > 0)
                {
                    string strClientName = "";
                    string strSiteName = "";
                    string strDoubleGrit = "";
                    decimal decBagsOfSalt = (decimal)0.00;
                    string strClientPO = "";
                    string strSitePostcode = "";
                    int intAttendanceOrder = 0;
                    string strPDACode = "";

                    DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetEmailAddress = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                    GetEmailAddress.ChangeConnectionString(strConnectionString);
                    string strEmailAddresses = "";
                    foreach (string strTeamID in strArray)
                    {
                        strEmailAddresses = GetEmailAddress.sp04117_GC_Team_Email_Addressed_For_Callout_Email(Convert.ToInt32(strTeamID)).ToString();
                        if (!String.IsNullOrEmpty(strEmailAddresses))
                        {
                            string strGrittedSites = "";
                            // Build list of Sites to be Reactively Gritted //
                            DataRow[] drRows = dsEmailJobs.Tables[0].Select("SubContractorID = " + strTeamID + " and Reactive = 1");
                            if (drRows.Length > 0)
                            {
                                strGrittedSites += "<br/>Please <b>reactively</b> grit the following sites:";
                                //strGrittedSites += "<table  border=\"1\">\r\n <tr> <th>Client Name</th> <th>Site ID</th> <th>Site Name</th> <th>Double Grit</th><th>Bags Required</th><th>P.O. Number</th></th><th>Postcode</th></tr>";
                                strGrittedSites += "<table  border=\"1\">\r\n <tr> <th>Client Name</th> <th>Site ID</th> <th>Site Name</th> <th>Double Grit</th><th>Bags Required</th><th>P.O. Number</th></th><th>Postcode</th><th>Attendance Order</th><th>PDA</th></tr>";
                                foreach (DataRow row in drRows)
                                {
                                    // Add Record //
                                    strClientName = row["ClientName"].ToString();
                                    intSiteID = Convert.ToInt32(row["SiteID"]);
                                    strSiteName = row["SiteName"].ToString();
                                    strDoubleGrit = (row["DoubleGrit"].ToString() == "*" ? "Yes" : "No");
                                    decBagsOfSalt = Convert.ToDecimal(row["BagsOfSalt"]);
                                    strClientPO = row["ClientPONumber"].ToString();
                                    strSitePostcode = row["SitePostcode"].ToString();
                                    intAttendanceOrder = Convert.ToInt32(row["AttendanceOrder"]);
                                    strPDACode = row["PDACode"].ToString();
                                    // Build Email //
                                    //strGrittedSites += "<tr>\r\n<td>" + strClientName + "</td>\r\n" + "<td>" + intSiteID.ToString() + "</td>\r\n" + "<td>" + strSiteName + "</td>\r\n" + "<td>" + strDoubleGrit + "</td>\r\n" + "<td>" + decBagsOfSalt.ToString("######0") + "</td>\r\n" + "<td>" + (string.IsNullOrEmpty(strClientPO) ? "" : strClientPO) + "</td>\r\n" + "<td>" + strSitePostcode + "</td>\r\n</tr>";
                                    strGrittedSites += "<tr>\r\n<td>" + strClientName + "</td>\r\n" + "<td>" + intSiteID.ToString() + "</td>\r\n" + "<td>" + strSiteName + "</td>\r\n" + "<td>" + strDoubleGrit + "</td>\r\n" + "<td>" + decBagsOfSalt.ToString("######0") + "</td>\r\n" + "<td>" + (string.IsNullOrEmpty(strClientPO) ? "" : strClientPO) + "</td>\r\n" + "<td>" + strSitePostcode + "</td>\r\n" + "<td>" + intAttendanceOrder.ToString() + "</td>\r\n" + "<td>" + strPDACode + "</td>\r\n</tr>";
                                    row.Delete(); // Remove row now it has been added to the email (this should speed up further interations) //

                                    intUpdateProgressTempCount++;
                                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                    {
                                        intUpdateProgressTempCount = 0;
                                        fProgress.UpdateProgress(10);
                                    }
                                }
                                strGrittedSites += "\r\n</table>";
                            }
                            // Build list of Sites to be Proactively Gritted //
                            drRows = dsEmailJobs.Tables[0].Select("SubContractorID = " + strTeamID + " and Reactive = 0");
                            if (drRows.Length > 0)
                            {
                                strGrittedSites += "<br/>Please <b>proactively</b> grit the following sites:";
                                //strGrittedSites += "<table  border=\"1\">\r\n <tr> <th>Client Name</th> <th>Site ID</th> <th>Site Name</th> <th>Double Grit</th><th>Bags Required</th> <th>Postcode</th></tr>";
                                strGrittedSites += "<table  border=\"1\">\r\n <tr> <th>Client Name</th> <th>Site ID</th> <th>Site Name</th> <th>Double Grit</th><th>Bags Required</th> <th>Postcode</th><th>Attendance Order</th><th>PDA</th></tr>";
                                foreach (DataRow row in drRows)
                                {
                                    // Add Record //
                                    strClientName = row["ClientName"].ToString();
                                    intSiteID = Convert.ToInt32(row["SiteID"]);
                                    strSiteName = row["SiteName"].ToString();
                                    strDoubleGrit = (row["DoubleGrit"].ToString() == "*" ? "Yes" : "No");
                                    decBagsOfSalt = Convert.ToDecimal(row["BagsOfSalt"]);
                                    strSitePostcode = row["SitePostcode"].ToString();
                                    intAttendanceOrder = Convert.ToInt32(row["AttendanceOrder"]);
                                    strPDACode = row["PDACode"].ToString();
                                    // Build Email //
                                    //strGrittedSites += "<tr>\r\n<td>" + strClientName + "</td>\r\n" + "<td>" + intSiteID.ToString() + "</td>\r\n" + "<td>" + strSiteName + "</td>\r\n" + "<td>" + strDoubleGrit + "</td>\r\n" + "<td>" + decBagsOfSalt.ToString("######0") + "</td>\r\n" + "<td>" + strSitePostcode + "</td>\r\n</tr>";
                                    strGrittedSites += "<tr>\r\n<td>" + strClientName + "</td>\r\n" + "<td>" + intSiteID.ToString() + "</td>\r\n" + "<td>" + strSiteName + "</td>\r\n" + "<td>" + strDoubleGrit + "</td>\r\n" + "<td>" + decBagsOfSalt.ToString("######0") + "</td>\r\n" + "<td>" + strSitePostcode + "</td>\r\n" + "<td>" + intAttendanceOrder.ToString() + "</td>\r\n" + "<td>" + strPDACode + "</td>\r\n</tr>";
                                    row.Delete(); // Remove row now it has been added to the email (this should speed up further interations) //

                                    intUpdateProgressTempCount++;
                                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                    {
                                        intUpdateProgressTempCount = 0;
                                        fProgress.UpdateProgress(10);
                                    }
                                }
                                strGrittedSites += "\r\n</table>";
                            }
                            // Now Send Email //
                            string strBody = System.IO.File.ReadAllText(strConfirmationBodyFile);
                            strBody = strBody.Replace("%Sites%", strGrittedSites);

                            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                            msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                            string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strEmailTo.Length > 0)
                            {
                                foreach (string strEmailAddress in strEmailTo)
                                {
                                    msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                                }
                            }
                            else
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                            }
                            msg.Subject = strConfirmationEmailSubjectLine;
                            if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                            msg.Priority = System.Net.Mail.MailPriority.High;
                            msg.IsBodyHtml = true;

                            System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                            System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                            //create the LinkedResource (embedded image)
                            System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                            logo.ContentId = "companylogo";
                            //add the LinkedResource to the appropriate view
                            htmlView.LinkedResources.Add(logo);

                            //create the LinkedResource (embedded image)
                            System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                            logo2.ContentId = "companyfooter";
                            //add the LinkedResource to the appropriate view
                            htmlView.LinkedResources.Add(logo2);

                            msg.AlternateViews.Add(plainView);
                            msg.AlternateViews.Add(htmlView);

                            object userState = msg;
                            System.Net.Mail.SmtpClient emailClient = null;
                            if (intSMTPMailServerPort != 0)
                            {
                                emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                            }
                            else
                            {
                                emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                            }
                            if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                            {
                                System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                                emailClient.UseDefaultCredentials = false;
                                emailClient.Credentials = basicCredential;
                            }
                            emailClient.SendAsync(msg, userState);
//                            Application.DoEvents();  // Give Application time to catch up in case we run into a timing issue - MIGHT NEED TO SWITCH THIS ON //
                        }
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                    }
                }
                #endregion

                #region SendLinkTexts
                
                // Check if a snow clearance callout is available if yes, send linking text messages between snow and grit teams //
                char[] delimiters2 = new char[] { '|' };
                int intSubContractorID = 0;
                int intGrittingCallOutID = 0;
                foreach (int intRowHandle in intRowHandles)
                {
                    intSubContractorID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SubContractorID"));
                    intGrittingCallOutID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "GrittingCallOutID"));
                    if (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "GritMobileTelephoneNumber").ToString()))
                    {
                        intSiteID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteID"));
                        strFoundMatchDetails = GetLinkedCallout.sp04233_GC_Get_Linked_Callout_Record(intSiteID, 0).ToString();
                        string[] strMatchingRecordDetails = strFoundMatchDetails.Split(delimiters2);  // | delimeter //
                        if (strMatchingRecordDetails.Length == 4)
                        {
                            intMatchingRecordID = Convert.ToInt32(strMatchingRecordDetails[0]);
                            strMatchingTeamName = strMatchingRecordDetails[1];
                            strMatchingTeamMobile = strMatchingRecordDetails[2];
                            intMatchingTeamID = Convert.ToInt32(strMatchingRecordDetails[3]);

                            if (intMatchingRecordID != 0)  // Matching Record so sent both sides a text message and link records at end of process //
                            {
                                // Check to ensure the Grit and Snow Clear are linked to different teams - if they match we don't need any text messages //
                                if (intMatchingTeamID != intSubContractorID)
                                {
                                    if (strMatchingTeamMobile != "0")
                                    {
                                        string[] strSplitGritTel = view.GetRowCellValue(intRowHandle, "GritMobileTelephoneNumber").ToString().Split(delimiters);  // Snow Tel No //
                                        string[] strSplitSnowTel = strMatchingTeamMobile.Split(delimiters);  // Grit Tel No //
                                        if (strSplitSnowTel.Length > 0 && strSplitGritTel.Length > 0)
                                        {
                                            strTuncatedSiteID = (intSiteID.ToString().Length > 6 ? intSiteID.ToString().Substring(0, 6) : intSiteID.ToString());
                                            strTuncatedClientName = (view.GetRowCellValue(intRowHandle, "ClientName").ToString().Length > 10 ? view.GetRowCellValue(intRowHandle, "ClientName").ToString().Substring(0, 10) : view.GetRowCellValue(intRowHandle, "ClientName").ToString());
                                            strTuncatedSiteName = (view.GetRowCellValue(intRowHandle, "SiteName").ToString().Length > 10 ? view.GetRowCellValue(intRowHandle, "SiteName").ToString().Substring(0, 10) : view.GetRowCellValue(intRowHandle, "SiteName").ToString());
                                            strTuncatedSubContractorName = (view.GetRowCellValue(intRowHandle, "SubContractorName").ToString().Length > 20 ? view.GetRowCellValue(intRowHandle, "SubContractorName").ToString().Substring(0, 20) : view.GetRowCellValue(intRowHandle, "SubContractorName").ToString());
                                            for (int i = 0; i < strSplitSnowTel.Length; i++)  // Send Snow team text message for each recorded gritting mobile tel no //
                                            {
                                                strTruncatedPhoneNumber = (strSplitGritTel[i].Length > 20 ? strSplitGritTel[i].Substring(0, 20) : strSplitGritTel[i]);
                                                strLinkedTextMessage1 = "Gritting after snow clearance at " +
                                                                           strTuncatedSiteID + "-" + strTuncatedClientName + "-" + strTuncatedSiteName + ". " +
                                                                           "Contact " + strTuncatedSubContractorName + " on (" + strTruncatedPhoneNumber + ") to confirm time to attend.";
                                                Send_Link_Text_Messages(strTextServiceUsername, strTextServicePassword, intMatchingTeamID, 2, strLinkedTextMessage1, strMatchingTeamMobile);  // Potential multiple comma seperated recipients //
                                            }

                                            for (int j = 0; j < strSplitGritTel.Length; j++)    // Send Gritting team text message for each recorded snow clearance mobile tel no //
                                            {
                                                strLinkedTextMessage2 = "SNOW Clearance scheduled at " +
                                                                            strTuncatedSiteID + "-" + strTuncatedClientName + "-" + strTuncatedSiteName + ". " +
                                                                           "Contact " + strMatchingTeamName + " on (" + strSplitSnowTel[j] + ") ) to confirm when GRITTING required at site.";
                                                Send_Link_Text_Messages(strTextServiceUsername, strTextServicePassword, intSubContractorID, 3, strLinkedTextMessage2, view.GetRowCellValue(intRowHandle, "GritMobileTelephoneNumber").ToString());  // Potential multiple comma seperated recipients //
                                            }
                                        }
                                    }
                                }
                                // Update both grit and snow clearance records (Link IDs) with each others ID //
                                StoreLinks.sp04234_GC_Update_Callouts_With_Linked_Record_IDs(intGrittingCallOutID, Convert.ToInt32(intMatchingRecordID));  // Grit ID, Snow Clearance ID //
                            }
                        }
                    }
                }
                #endregion



                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                System.Windows.Forms.Application.DoEvents();

                SQlConn.Close();
                SQlConn.Dispose();
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while sending the Gritting Callouts.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Gritting Callouts to Teams", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            System.Windows.Forms.Application.DoEvents();
            Load_Data();  // Refresh grid to reflect record's updated status //


            if (intRowHandles.Length <= 0)
            {
                string strMessage = "";
                if (!boolTextServiceAvailable)
                {
                    strMessage = (intSentCount == 1 ? "1 Device Callout" : intSentCount.ToString() + " Device Callouts") + "Sent to Teams Successfully.";
                    strMessage += "\n\nWARNING: Callouts to be sent by Text Message were NOT sent. Please check your internet connection before trying again.";
                }
                else
                {
                    strMessage = (intSentCount == 1 ? "1 Callout" : intSentCount.ToString() + " Callouts") + "Sent to Teams Successfully.";
                }
                DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Send Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Send_Link_Text_Messages(string strTextServiceUsername, string strTextServicePassword, int intSubContractorID, int intTextMessageType, string strTextMessage, string strPhoneNumber)
        {
            // Add Text Sent Header to DB and pick up the returned Header ID //
            int intNewHeaderID = 0;
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter InsertTextHeader = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                InsertTextHeader.ChangeConnectionString(strConnectionString);
                intNewHeaderID = Convert.ToInt32(InsertTextHeader.sp04104_GC_Get_Callouts_Create_Text_Header(intSubContractorID, intTextMessageType, strTextMessage, strPhoneNumber));
            }
            catch (Exception)
            {
            }
            // Link Jobs to Text Message and update Job Status to sent //
            /*try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter InsertTextHeaderLink = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                InsertTextHeaderLink.ChangeConnectionString(strConnectionString);
                InsertTextHeaderLink.sp04105_GC_Get_Callouts_Create_Text_Header_CallOuts(intNewHeaderID, strCalloutIDs);
            }
            catch (Exception)
            {
            }*/
            if (!string.IsNullOrEmpty(strPhoneNumber))
            {
                TxtMessage msg = new TxtMessage(strPhoneNumber, strTextMessage, intNewHeaderID, strTextServiceUsername, strTextServicePassword);
                msg.Send(new object());
                if (msg.Status != EnmStatus.Ok)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Problem sending Message to " + strPhoneNumber, "Problem sending text", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // Remove the Sent_Text_Message and Link Callouts from the DB since the text message failed to send //
                }
            }
        }

        private void SendMessage(string strTextMessage, int intType, string strPhoneNumber, string strTextServiceUsername, string strTextServicePassword, string strCalloutIDs, int intSubContractorID)
        {
            string strPreAmble = (intType == 1 ? "Please reactively grit the following sites" : "Please proactively grit the following sites");
            strTextMessage = strPreAmble + strTextMessage;
            if (strTextMessage.Length > 160) strTextMessage.Remove(160);  // Trim to 160 long (start at 160 as 0 based so we need 0 - 159). Note that this should never be required! //
            
            // Add Text Sent Header to DB and pick up the returned Header ID //
            int intNewHeaderID = 0;
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter InsertTextHeader = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                InsertTextHeader.ChangeConnectionString(strConnectionString);
                intNewHeaderID = Convert.ToInt32(InsertTextHeader.sp04104_GC_Get_Callouts_Create_Text_Header(intSubContractorID, 1, strTextMessage, strPhoneNumber));
            }
            catch (Exception)
            {
            }
            // Link Jobs to Text Message and update Job Status to sent //
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter InsertTextHeaderLink = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                InsertTextHeaderLink.ChangeConnectionString(strConnectionString);
                InsertTextHeaderLink.sp04105_GC_Get_Callouts_Create_Text_Header_CallOuts(intNewHeaderID, strCalloutIDs);
            }
            catch (Exception)
            {
            }

            if (!string.IsNullOrEmpty(strPhoneNumber))
            {
                TxtMessage msg = new TxtMessage(strPhoneNumber, strTextMessage, intNewHeaderID, strTextServiceUsername, strTextServicePassword);
                msg.Send(new object());
                if (msg.Status != EnmStatus.Ok)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Problem sending Message to " + strPhoneNumber, "Problem sending text", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // Remove the Sent_Text_Message and Link Callouts from the DB since the text message failed to send //
                }
            }
        }

        private void bbiReassignJobs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to Reassign before proceeding.", "Reassign Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            for (int i = 0; i < intRowHandles.Length; i++)
            {

                intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                if (!(intJobStatusID < 70 || intJobStatusID == 90)) view.UnselectRow(intRowHandles[i]);
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to Reassign before proceeding.\n\nNote: This process only allows jobs not yet sent or jobs sent but rejected by the team to be re-assigned. Any other jobs are automatically de-selected.", "Reassign Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Now allow the user to change the selected contractor - open screen to allow the user to select the new team //
            string strRecordIDs = ",";  // Put leading comma in so we can easily detect if it contains a value within the coming loop //
            string strGrittingCalloutIDs = "";
            int intSiteGrittingContractID = 0;
            int intRecordCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                intSiteGrittingContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID"));
                if (intSiteGrittingContractID > 0 && !strRecordIDs.Contains("," + intSiteGrittingContractID.ToString() + ","))
                {
                    strRecordIDs += intSiteGrittingContractID.ToString() + ",";
                    intRecordCount++;
                }
                strGrittingCalloutIDs += view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + ",";
            }
            strRecordIDs = strRecordIDs.Remove(0, 1);  // Remove leading ',' added earlier //
            frm_GC_Import_Weather_Forecasts_Edit_Team fChildForm = new frm_GC_Import_Weather_Forecasts_Edit_Team();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInSiteGrittingContractIDs = strRecordIDs;
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = intRecordCount;
            fChildForm.boolRemoveBlankRow = true;
            if (fChildForm.ShowDialog() != DialogResult.OK) return; // User Aborted //
            try
            {
                DataSet_GC_CoreTableAdapters.QueriesTableAdapter UpdateDatabase = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                UpdateDatabase.ChangeConnectionString(strConnectionString);
                i_str_AddedRecordIDs1 = UpdateDatabase.sp04091_GC_Gritting_Callout_Reassign(strGrittingCalloutIDs, fChildForm.intSubContractorID).ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to Reassign the selected Callouts.\n\nPlease try again. If the problem persists, contact Technical Support.", "Reassign Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Load_Data();  // Reload to see updated records //
            DevExpress.XtraEditors.XtraMessageBox.Show((intRecordCount == 1 ? "1 Callout" : intRecordCount.ToString() + " Callouts") + " Reassigned Successfully\n\n***** IMPORTANT: REMEMBER TO SEND THESE CALLOUTS *****", "Reassign Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }        

        private void bbiAuthorise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_GC_Team_Self_Billing_Invoicing frmInstance = new frm_GC_Team_Self_Billing_Invoicing();

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            System.Windows.Forms.Application.DoEvents();

            frmInstance.MdiParent = this.MdiParent;
            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.fProgress = fProgress;
            frmInstance.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmInstance, new object[] { null });
        
            
            //net.textapp.www.Service mySms = new net.textapp.www.Service();
            //string strReply = mySms.GetSMSInbound(true, "BU048984", "steps", "+447537402046", "");

            /*
            string[] lines = System.IO.File.ReadAllLines(@"C:\Mark_Development\Ground_Control_Documents\SMS_Return2.txt");
            if (lines.Length < 2) return;  // Nothing returned yet //
            char[] delimiters = new char[] { ',' };
  
            char[] delimiters2 = new char[] { ' ' };
            char[] arr = new char[] { '"' };
            DateTime dtDateTime = DateTime.Today;
            int intSiteID = 0;
            decimal decSaltUsed = (decimal)0.00;
            for (int i = 1; i < lines.Length; i++)  // Skip Row 0 as this is the Transaction Code from TextAnywhere //
            {
//eventLog1.WriteEntry("Job Completion - " + strReply, EventLogEntryType.Information);
                string[] strArray = lines[i].Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length < 8) continue;  // Skip row and move to next column - cant flag as error as we don't have enought columns //

                dtDateTime = DateTime.Today;
                if (DateTime.TryParse(strArray[4].TrimStart(arr).TrimEnd(arr).Trim() + " " + strArray[5].TrimStart(arr).TrimEnd(arr).Trim(), out dtDateTime))
                {
                    if (dtDateTime == Convert.ToDateTime("01/01/0001"))  // Error - invalid date so use todays with no time so it's obvious //
                    {
                        dtDateTime = DateTime.Today;
                    }
                }

                string strTelephone = strArray[1].TrimStart(arr).TrimEnd(arr).Trim();
                string strBody = strArray[6].TrimStart(arr).TrimEnd(arr).Trim();
                strBody = strArray[6].TrimStart(arr).TrimEnd(arr);
                string[] strArray2 = strBody.Split(delimiters2, StringSplitOptions.RemoveEmptyEntries);
                string strAction = "";
                decSaltUsed = (decimal)0.00;

                if (strArray2.Length < 2)  // Error - invalid number of columns so flag as an error //
                {
                    //StoreError2(SQLConn, strTelephone, dtDateTime, strBody, 0, (decimal)0.00, 1, "Wrong number of parameters");
                    continue;
                }
                if (!int.TryParse(strArray2[0].TrimStart(arr).Trim(), out intSiteID))  // Error - non numeric value so flag as an error //
                {
                    //StoreError2(SQLConn, strTelephone, dtDateTime, strBody, 0, (decimal)0.00, 1, "Non-numeric Site ID");
                    continue;
                }
                if (strArray2[1].ToUpper().TrimEnd(arr).Trim() == "ABORTED")
                {
                    strAction = "Aborted";
                }
                else if (strArray2[1].ToUpper().TrimEnd(arr).Trim() == "NO" && strArray2.Length == 3)
                {
                    if (strArray2[2].ToUpper().TrimEnd(arr).Trim() == "ACCESS")
                    {
                        //StoreError2(SQLConn, strTelephone, dtDateTime, strBody, intSiteID, (decimal)0.00, 1, "Wrong\\Invalid number of parameters");
                        continue;
                    }
                    strAction = "No Access";
                }
                else
                {
                    strAction = "Completed"; 
                    if (!decimal.TryParse(strArray2[1].TrimEnd(arr).Trim(), out decSaltUsed))  // Error - non numeric value so flag as an error //
                    {
                        //StoreError2(SQLConn, strTelephone, dtDateTime, strBody, intSiteID, (decimal)0.00, 1, "Non-numeric Salt Value");
                        continue;
                    }
                }
                // If we are here attempt to update database - Note the Update SP will create an error record if it can't find an appropriate callout for the site //
               
                continue;
            }*/
            
            /*
            
            net.textapp.www.Service mySms = new net.textapp.www.Service();
            string strReply = mySms.GetSMSReply(true, "BU048984", "steps", "1");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "2");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "3");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "4");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "5");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "6");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "7");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "8");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "9");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "10");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "11");


            string[] lines = System.IO.File.ReadAllLines(@"C:\Mark_Development\Ground_Control_Documents\SMS_Return.txt");
            if (lines.Length < 2) return;  // Nothing returned yet //
            string strMessageResponse = lines[1];
            char[] delimiters = new char[] { ',' };
            string[] strArray;
            strArray = strMessageResponse.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length < 4) return;  // Error //
            string strMessageResponseColumns = strArray[4];
            int intLinkedJobCount= 0;

            
                // Parse Reply //
                delimiters = new char[] { ' ' };
                string[] strArray2;
                strArray2 = strMessageResponseColumns.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                char[] arr = new char[] { '"' };
                if (strArray2.Length <= 1 || strArray == null) // Check for Rejection first //
                {
                    if (strMessageResponseColumns.ToUpper().TrimStart(arr).TrimEnd(arr) != "NO")  // Error - not = OK //
                    {
                        return;
                   }
                    // Value = NO so get Date and Time of acceptance //
                    DateTime dtResponded = DateTime.Today;
                    delimiters = new char[] { ',' };
                    strMessageResponse = lines[lines.Length - 1];
                    string[] strArray3;
                    strArray3 = strMessageResponse.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray3.Length > 2)
                    {
                        string strDate = strArray3[strArray3.Length - 2];
                        string strTime = strArray3[strArray3.Length - 1];

                        if (DateTime.TryParse(strDate.TrimStart(arr).TrimEnd(arr) + " " + strTime.TrimStart(arr).TrimEnd(arr) + ":00", out dtResponded))  // Error - invalid date completed so flag as an error //
                        {
                            if (dtResponded == Convert.ToDateTime("01/01/0001"))  // Error - invalid date completed so flag as an error //
                            {
                                dtResponded = DateTime.Today;
                            }
                        }
                    }
                    // Checks Passed so Set Job as rejected //
                }
                else  // Try for acceptance //
                {
                    if (strArray2.Length != 2)  // Error - invalid number of columns so flag as an error //
                    {
                         return;
                    }
                    // Check for Acceptance //
                    int intSites = 0;
                    if (!int.TryParse(strArray2[0].TrimStart(arr), out intSites))  // Error - non numeric value so flag as an error //
                    {
                        return;
                    }
                    if (strArray2[1].ToUpper().TrimEnd(arr) != "OK")  // Error - not = OK //
                    {
                         return;
                    }
                    if (intSites < intLinkedJobCount)  // Invalid number of jobs entered //
                    {
                        return;
                    }
                    // Get Date and Time of acceptance //
                    DateTime dtResponded = DateTime.Today;
                    delimiters = new char[] { ',' };
                    strMessageResponse = lines[lines.Length - 1];
                    string[] strArray3;
                    strArray3 = strMessageResponse.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray3.Length > 2)
                    {
                        string strDate = strArray3[strArray3.Length - 2];
                        string strTime = strArray3[strArray3.Length - 1];

                        if (DateTime.TryParse(strDate.TrimStart(arr).TrimEnd(arr) + " " + strTime.TrimStart(arr).TrimEnd(arr) + ":00", out dtResponded))  // Error - invalid date completed so flag as an error //
                        {
                            if (dtResponded == Convert.ToDateTime("01/01/0001"))  // Error - invalid date completed so flag as an error //
                            {
                                dtResponded = DateTime.Today;
                            }
                        }
                    }
                    // Checks Passed so Update text message and callout //
                 }

            */
    
        }

        private void bbiPay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_GC_Finance_Invoice_Jobs frmInstance = new frm_GC_Finance_Invoice_Jobs();

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            System.Windows.Forms.Application.DoEvents();

            frmInstance.MdiParent = this.MdiParent;
            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.fProgress = fProgress;
            frmInstance.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmInstance, new object[] { null });
        }

        private void bbiViewSiteOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view on the map before proceeding.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = ",";  // Used to stop the same location being picked up more than once //
            double dStartLat = (double)0.00;
            double dStartLong = (double)0.00;
            List<Mapping_Objects> mapObjectsList = new List<Mapping_Objects>();
            foreach (int intRowHandle in intRowHandles)
            {
                if (!strSelectedIDs.Contains(view.GetRowCellValue(intRowHandle, "SiteID").ToString() + ","))
                {
                    dStartLat = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "SiteLat").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLat")) : (double)0.00);
                    dStartLong = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "SiteLong").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLong")) : (double)0.00);
                    if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
                    {
                        Mapping_Objects mapObject = new Mapping_Objects();
                        mapObject.doubleLat = dStartLat;
                        mapObject.doubleLong = dStartLong;
                        mapObject.stringToolTip = "Client: " + view.GetRowCellValue(intRowHandle, "ClientName").ToString() +  "\nSite: " + view.GetRowCellValue(intRowHandle, "SiteName").ToString();
                        mapObject.stringID = "Site|" + view.GetRowCellValue(intRowHandle, "SiteID").ToString();
                        mapObjectsList.Add(mapObject);
                        strSelectedIDs += view.GetRowCellValue(intRowHandle, "SiteID").ToString() + ",";
                    }
                }
            }
            if (mapObjectsList.Count == 0)  // No objects added due to missing coordinates //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view on the map before proceeding.\n\nTip: Records must have coordinates stored against them before they can be viewed on the map.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                Mapping_Functions mapFunctions = new Mapping_Functions();
                mapFunctions.View_Object_In_Internet_Mapping(this.GlobalSettings, this.strConnectionString, this, mapObjectsList);
            }
        }

        private void bbiViewJobOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view on the map before proceeding.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            double dStartLat = (double)0.00;
            double dStartLong = (double)0.00;
            List<Mapping_Objects> mapObjectsList = new List<Mapping_Objects>();
            foreach (int intRowHandle in intRowHandles)
            {
                dStartLat = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "StartLatitude").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "StartLatitude")) : (double)0.00);
                dStartLong = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "StartLongitude").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "StartLongitude")) : (double)0.00);
                if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
                {
                    Mapping_Objects mapObject = new Mapping_Objects();
                    mapObject.doubleLat = dStartLat;
                    mapObject.doubleLong = dStartLong;
                    mapObject.stringToolTip = "Job Start Position \n\nJob ID: " + view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + "\nClient: " + view.GetRowCellValue(intRowHandle, "ClientName").ToString() + "\nSite: " + view.GetRowCellValue(intRowHandle, "SiteName").ToString();
                    mapObject.stringID = "Job Start|" + view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString();
                    mapObjectsList.Add(mapObject);
                }
                dStartLat = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "FinishedLatitude").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "FinishedLatitude")) : (double)0.00);
                dStartLong = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "FinishedLongitude").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "FinishedLongitude")) : (double)0.00);
                if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
                {
                    Mapping_Objects mapObject = new Mapping_Objects();
                    mapObject.doubleLat = dStartLat;
                    mapObject.doubleLong = dStartLong;
                    mapObject.stringToolTip = "Job End Position \n\nJob ID: " + view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + "\nClient: " + view.GetRowCellValue(intRowHandle, "ClientName").ToString() + "\nSite: " + view.GetRowCellValue(intRowHandle, "SiteName").ToString();
                    mapObject.stringID = "Job End|" + view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString();
                    mapObjectsList.Add(mapObject);
                }
            }
            if (mapObjectsList.Count == 0)  // No objects added due to missing coordinates //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view on the map before proceeding.\n\nTip: Records must have coordinates stored against them before they can be viewed on the map.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                Mapping_Functions mapFunctions = new Mapping_Functions();
                mapFunctions.View_Object_In_Internet_Mapping(this.GlobalSettings, this.strConnectionString, this, mapObjectsList);
            }
        }

        private void barManager1_HighlightedLinkChanged(object sender, DevExpress.XtraBars.HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo {Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip};

            toolTipController1.ShowHint(Info);
        }

        private void bbiViewSMSErrors_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Open Grittin Text Errors screen //
            frm_GC_Gritting_Text_Message_Errors frmInstance = new frm_GC_Gritting_Text_Message_Errors();

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            System.Windows.Forms.Application.DoEvents();

            frmInstance.MdiParent = this.MdiParent;
            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.fProgress = fProgress;
            frmInstance.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmInstance, new object[] { null });
        }


        private void bbiSetReadyToSend1_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetJobStatus(50);  // 50: Completed - Ready To Send //
        }

        private void bbiSetReadyToSend2_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetJobStatus(50);  // 50: Completed - Ready To Send //
        }

        private void bbiSelectSetReadyToSend_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 10 (Started - To Be Completed - No Authorisation Required) or 30 (Started - To Be Completed - Authorised) //
            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "SiteGrittingContractID")) == 0) continue;
                if (Convert.ToInt32(view.GetRowCellValue(i, "SubContractorID")) == 0) continue;
                if (Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 1 && Convert.ToDouble(view.GetRowCellValue(i, "SiteLat")) == 0.00 && Convert.ToDouble(view.GetRowCellValue(i, "SiteLong")) == 0.00) continue;
                if (Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 1 && Convert.ToInt32(view.GetRowCellValue(i, "AttendanceOrder")) == 0) continue;
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 10 || Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 30) view.SelectRow(i);

                
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void SetJobStatus(int intStatus)
        {
            // First... Check selected jobs are all appropriate - Status of 10 0r 30 - if not, de-select them //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to set as 'Ready to Send' before proceeding.", "Set Callouts as 'Ready to Send'", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Selected Jobs...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.SelectedRowsCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            foreach (int intRowHandle in intRowHandles)
            {
                if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID")) == 10 || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID")) == 30))
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to send [Wrong Status] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) == 0)
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to send [Missing SiteGrittingContractID] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SubContractorID")) == 0)
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to send [Missing SubContractorID] //
                    continue;
                }

                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "IsFloatingSite")) == 1 && Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLat")) == 0.00 && Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLong")) == 0.00)
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to send [Missing Lat \ Long] //
                    continue;
                }

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to set as 'Ready to Send' before proceeding.\n\nNote: This process de-selects any selected jobs which are not ready to set as 'Ready to Send'...\n\nJobs must have a Site, Team, appropriate Status and a Latitude and Longitude.", "Set Callouts as 'Ready to Send'", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

             // If at this point, we are good to change status so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Job" : intRowHandles.Length.ToString() + " Jobs") + " selected for setting as 'Ready to Send'.\n\nProceed?", "Set Callout(s) as 'Ready to Send'", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Changing Job Status...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            
            string strJobIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strJobIDs += view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + ",";
                
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter CreatePDAJobs = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                CreatePDAJobs.ChangeConnectionString(strConnectionString);
                CreatePDAJobs.sp04155_GC_Gritting_Set_Job_Status(strJobIDs, intStatus);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while changing the status of the selected Gritting Callouts.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Set Callouts as 'Ready To Send'", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();  // Refresh grid to reflect record's updated status //

            DevExpress.XtraEditors.XtraMessageBox.Show("Callout Statuses Changed Successfully.", "Set Callouts as 'Ready to Send'", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void bbiAddJobs_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
            frm_GC_Callout_Wizard_Add fChildForm = new frm_GC_Callout_Wizard_Add();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strFormMode = "edit";
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.fProgress = fProgress;
            fChildForm.i_str_SiteIDsToLink = this.i_str_SiteIDsToLink;  // Only populated when passed though Snow Clearance Manager to this screen //
            fChildForm.Show();
            if (!string.IsNullOrEmpty(this.i_str_SiteIDsToLink)) this.i_str_SiteIDsToLink = "";  // Clear Passed In Values //


            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiSelectJobsToComplete_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 70, 80, 90, 100, 110 //
            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Selecting...");

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) >= 70 && Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) <= 110) view.SelectRow(i);
            }
            view.EndSelection();
            view.EndUpdate();
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
        }

        private void bbiManuallyCompleteJobs_ItemClick(object sender, ItemClickEventArgs e)
        {
            // First... Check selected jobs are all appropriate - Status of 70 - 110, if not, de-select them //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to Manually Complete before proceeding.", "Manually Complete Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Checking Selecting Jobs...");

            view.BeginUpdate();
            view.BeginSelection();

            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID")) < 70 || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID")) > 110)
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to complete [Wrong Status] //
                    continue;
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to Manually Complete before proceeding.\n\nNote: This process de-selects any selected jobs which are not ready to complete.", "Manually Complete Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to complete callouts so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Job" : intRowHandles.Length.ToString() + " Jobs") + " selected for completing.\n\nProceed?", "Manually Complete Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Completing Job(s)...");
            
            frm_GC_Callout_Manually_Complete fChildForm = new frm_GC_Callout_Manually_Complete();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            DateTime? dtStartDate = fChildForm.dtStartDate;
            DateTime? dtCompletedDate = fChildForm.dtCompletedDate;
            decimal? decSaltUsed = fChildForm.decSaltUsed;
            string strComments = fChildForm.strComments;

            DataSet_GC_CoreTableAdapters.QueriesTableAdapter UpdatePDAJobs = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            UpdatePDAJobs.ChangeConnectionString(strConnectionString);
            int intCalloutID = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                intCalloutID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "GrittingCallOutID"));
                try
                {
                    UpdatePDAJobs.sp04241_GC_Gritting_Manually_Complete_Job(intCalloutID, dtStartDate, dtCompletedDate, decSaltUsed, strComments);
                }
                catch (Exception ex)
                {
                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while completing the selected Gritting Callouts.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Manually Complete Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            Load_Data();  // Refresh grid to reflect record's updated status //
            DevExpress.XtraEditors.XtraMessageBox.Show("Callout(s) Completed Successfully.", "Manually Complete Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiSendNoProactiveMessages_ItemClick(object sender, ItemClickEventArgs e)
        {
            frm_GC_Callout_Send_No_Proactive_Messages fChildForm = new frm_GC_Callout_Send_No_Proactive_Messages();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
        }


        #region Route Optimization

        private void bbiOptimization_ItemClick(object sender, ItemClickEventArgs e)
        {
            Send_Data_To_Optimizer(false);
        }

        private void bbiSendDataToRouteOptimizer_ItemClick(object sender, ItemClickEventArgs e)
        {
            Send_Data_To_Optimizer(false);
        }

        private void bbiSendDataToRouteOptimizerLeaks_ItemClick(object sender, ItemClickEventArgs e)
        {
            Send_Data_To_Optimizer(true);
        }

        private void Send_Data_To_Optimizer(bool boolLeakData)
        {
            // First... Check selected jobs are all appropriate - if not, de-select them //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to send to the Route Optimizer before proceeding.", "Route Optimize Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedCalloutIDs = "";
            int intStatus = 0;
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                intStatus = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                if (!(intStatus == 50 || intStatus == 70 || intStatus == 72 || intStatus == 73 || intStatus == 75 || intStatus == 76 || intStatus == 80 || intStatus == 100))
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to optimise [Wrong Status] //
                    continue;
                }
                if (Convert.ToDecimal(view.GetRowCellValue(intRowHandles[i], "SiteLat")) == (decimal)0 && Convert.ToDecimal(view.GetRowCellValue(intRowHandles[i], "SiteLong")) == (decimal)0)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to optimise [Wrong Status] //
                    continue;
                }
                if (!boolLeakData)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "IsFloatingSite")) != 0)
                    {
                        view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to optimise [Wrong Status] //
                        continue;
                    }
                }
                else  // Leak Data so make sure it is a floating site //
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "IsFloatingSite")) != 1)
                    {
                        view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to optimise [Wrong Status] //
                        continue;
                    }
                }
                strSelectedCalloutIDs += view.GetRowCellValue(intRowHandles[i], "GrittingCallOutID").ToString() + ",";
            }
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to send to the Route Optimizer before proceeding.\n\nNote: The system will automatically de-select any callouts with an inappopriate status for route optimisation.", "Route Optimize Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            frm_GC_Gritting_Callouts_Send_To_Optimizer fChildForm = new frm_GC_Gritting_Callouts_Send_To_Optimizer();
            fChildForm.GlobalSettings = this.GlobalSettings;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();

            fChildForm._CalloutIDs = strSelectedCalloutIDs;
            fChildForm._boolLeakData = boolLeakData;
            fChildForm.ShowDialog();
        }

        
        private void bbiGetDataToRouteOptimize_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 50 (Ready to send) and Not Leak data //
            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 50 && !string.IsNullOrEmpty(view.GetRowCellValue(i, "GritMobileTelephoneNumber").ToString()) && (Convert.ToDecimal(view.GetRowCellValue(i, "SiteLat")) != (decimal)0.00 && Convert.ToDecimal(view.GetRowCellValue(i, "SiteLong")) != (decimal)0.00) && Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 0) view.SelectRow(i);

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void bbiGetDataToRouteOptimizeLeaks_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 50 (Ready to send) and Leak data //
            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 50 && !string.IsNullOrEmpty(view.GetRowCellValue(i, "GritMobileTelephoneNumber").ToString()) && (Convert.ToDecimal(view.GetRowCellValue(i, "SiteLat")) != (decimal)0.00 && Convert.ToDecimal(view.GetRowCellValue(i, "SiteLong")) != (decimal)0.00) && Convert.ToInt32(view.GetRowCellValue(i, "IsFloatingSite")) == 1) view.SelectRow(i);

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }


        private void bbiGetDataFromRouteOptimizer_ItemClick(object sender, ItemClickEventArgs e)
        {
            frm_GC_Gritting_Callouts_Optimizer_Results fChildForm = new frm_GC_Gritting_Callouts_Optimizer_Results();
            fChildForm.GlobalSettings = this.GlobalSettings;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();

            fChildForm.ShowDialog();
            if (fChildForm.boolRefreshCallingScreen)
            {
                this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                Load_Data();
            }
        }

        #endregion


        #region Completion Sheets

        private void bbiSelectJobsForKMLCreation_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 120, 150, 160, 170, 180, 190 //
            List<int> list = new List<int>() { 120, 150, 160, 170, 180, 190 };

            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager.ShowWaitForm();
            splashScreenManager.SetWaitFormDescription("Selecting Records...");

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (list.IndexOf(Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID"))) >= 0 && (string.IsNullOrWhiteSpace(view.GetRowCellValue(i, "KMLFile").ToString()) || string.IsNullOrWhiteSpace(view.GetRowCellValue(i, "KMLImageFile").ToString()))) view.SelectRow(i);
            }
            view.EndSelection();
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }
        
        private void bbiCreateKMLAndMapImages_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Only jobs with a Status of 120, 150, 160, 170, 180, 190 //
            List<int> list = new List<int>() { 120, 150, 160, 170, 180, 190 };

            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Callouts to Create KML Files \\ Map Image Files for by clicking on them then try again.", "Create Callout KML Files \\ Map Image Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (list.IndexOf(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID"))) <= -1) view.UnselectRow(intRowHandle);
            }
            view.EndSelection();

            // Check there is at least one row still selected //
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Callouts to Create KML Files \\ Map Image Files for by clicking on them then try again.\n\nOnly Completed Callouts can be selected. If other Callouts are selected this process will de-select them.", "Create KML Files \\ Map Image Files", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intCalloutID = 0;
            string strFileName = "";
            string strKMLFilename = "";
            string strKMLImageFilename = "";
            string strKMLPath = "";
            string strKMLImagePath = "";

            strKMLPath = Get_Path(3, "GrittingLocationTrackKMLFilesFolder");
            if (string.IsNullOrWhiteSpace(strKMLPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the saved KML files.\n\nNo KML Files or Map Images can be created.", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            strKMLImagePath = Get_Path(3, "GrittingLocationTrackImageFilesFolder");
            if (string.IsNullOrWhiteSpace(strKMLImagePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the saved KML Image files.\n\nNo Map Images will be created.", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter UpdateCallout = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
            UpdateCallout.ChangeConnectionString(strConnectionString);

            string strMessage = "You have " + (intCount == 1 ? "1 Callout" : Convert.ToString(intRowHandles.Length) + " Callouts") + " selected for Creating KML File Creation.\n\nProceed?\n\nNote: This process may take some time.";
            if (XtraMessageBox.Show(strMessage, "Create KML File", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) != DialogResult.Yes) return;

            foreach (int intRowHandle in intRowHandles)
            {
                intCalloutID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "GrittingCallOutID"));
                strKMLFilename = intCalloutID.ToString().PadLeft(8, '0') + ".KML";
                strKMLImageFilename = intCalloutID.ToString().PadLeft(8, '0') + ".JPG";
                strFileName = Path.Combine(strKMLPath, strKMLFilename);
                if (Write_Location_Tracks_To_KML(intCalloutID, strFileName))  // KML file written successfully //
                {
                    // Write KML file link to record //
                    try
                    {
                        UpdateCallout.sp04374_WM_Write_Filename_To_Callout(intCalloutID, 1, strKMLFilename);
                        view.SetRowCellValue(intRowHandle, "KMLFile", strKMLFilename);
                    }
                    catch (Exception)
                    {
                        continue;  // Skip past any further code in this iteration of the loop //
                    }

                    if (!string.IsNullOrWhiteSpace(strKMLImagePath))
                    {
                        // Following used for generating Map Images //
                        var frmSaveKMLImage = new frm_Core_Mapping_Create_Image(Path.Combine(strKMLPath, strKMLFilename), Path.Combine(strKMLImagePath, strKMLImageFilename));
                        frmSaveKMLImage.Show();  // This will create the Map Image using the timer event on the form that fires after the data is loaded into the mapping layers //

                        // Write Map Image file link to record //
                        try
                        {
                            UpdateCallout.sp04374_WM_Write_Filename_To_Callout(intCalloutID, 2, strKMLImageFilename);
                            view.SetRowCellValue(intRowHandle, "KMLImageFile", strKMLImageFilename);
                        }
                        catch (Exception)
                        {
                            continue;  // Skip past any further code in this iteration of the loop //
                        }
                    }
                }
            }
        }
        public bool Write_Location_Tracks_To_KML(int CalloutID, string FileName)
        {
            if (CalloutID <= 0 || string.IsNullOrWhiteSpace(FileName)) return false;
            SqlDataAdapter sdaData_Tracks = new SqlDataAdapter();
            DataSet dsData_Tracks = new DataSet("NewDataSet");
            try
            {
                using (var conn = new SqlConnection(strConnectionStringREADONLY))
                {
                    conn.Open();
                    using (var cmd1 = new SqlCommand())
                    {
                        cmd1.CommandText = "sp01049_Core_Loaction_Track_For_Record";
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.Add(new SqlParameter("@RecordID", CalloutID));
                        cmd1.Parameters.Add(new SqlParameter("@RecordTypeID", 1));
                        cmd1.Connection = conn;
                        sdaData_Tracks = new SqlDataAdapter(cmd1);
                        sdaData_Tracks.Fill(dsData_Tracks, "Table");
                    }
                    conn.Close();
                    if (dsData_Tracks.Tables[0].Rows.Count <= 0) return false;

                    if (File.Exists(FileName))
                    {
                        try
                        {
                            File.Delete(FileName);
                        }
                        catch (Exception) { }
                    }
                }

                DataRow drMin = dsData_Tracks.Tables[0].Rows[0];
                DataRow drMax = dsData_Tracks.Tables[0].Rows[dsData_Tracks.Tables[0].Rows.Count - 1];

                using (StreamWriter file = new StreamWriter(FileName))
                {
                    file.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    file.WriteLine("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
                    {
                        file.WriteLine("<Document id=\"Tracking\">");
                        {
                            file.WriteLine("<name>Tracking</name>");
                            file.WriteLine("<Placemark>");
                            {
                                file.WriteLine("<name>Job Start</name>");
                                file.WriteLine("<Point>");
                                {
                                    file.WriteLine("<coordinates>" + drMin["Longitude"].ToString() + "," + drMin["Latitude"] + ",0 </coordinates>");
                                }
                                file.WriteLine("</Point>");
                            }
                            file.WriteLine("</Placemark>");

                            file.WriteLine("<Placemark>");
                            {
                                file.WriteLine("<name>Route</name>");
                                file.WriteLine("<LineString>");
                                {
                                    file.WriteLine("<tessellate>true</tessellate>");
                                    file.WriteLine("<altitudeMode>ClampToGround</altitudeMode>");
                                    file.WriteLine("<coordinates>");
                                    {
                                        foreach (DataRow dr in dsData_Tracks.Tables[0].Rows)
                                        {
                                            file.WriteLine(dr["Longitude"].ToString() + "," + dr["Latitude"].ToString() + ",0 ");
                                        }
                                    }
                                    file.WriteLine("</coordinates>");
                                }
                                file.WriteLine("</LineString>");
                                file.WriteLine("<Style>");
                                {
                                    file.WriteLine("<LineStyle>");
                                    {
                                        file.WriteLine("<color>96ff0000</color>");
                                        file.WriteLine("<width>5</width>");
                                    }
                                    file.WriteLine("</LineStyle>");
                                }
                                file.WriteLine("</Style>");
                            }
                            file.WriteLine("</Placemark>");

                            file.WriteLine("<Placemark>");
                            {
                                file.WriteLine("<name>Job End</name>");
                                file.WriteLine("<Point>");
                                file.WriteLine("<coordinates>" + drMax["Longitude"].ToString() + "," + drMax["Latitude"] + ",0 </coordinates>");
                                file.WriteLine("</Point>");
                            }
                            file.WriteLine("</Placemark>");
                        }
                        file.WriteLine("</Document>");
                    }
                    file.WriteLine("</kml>");
                    file.Close();
                }
            }
            catch (Exception ex) { return false; }
            return true;
        }

        private void bbiSelectCompletionSheetJobs_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 120, 150, 160, 170, 180, 190 //
            List<int> list = new List<int>() { 120, 150, 160, 170, 180, 190 };

            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager.ShowWaitForm();
            splashScreenManager.SetWaitFormDescription("Selecting Records...");

            view.BeginUpdate();
            view.BeginSelection();

            int intMissingKMLFileCount = 0;
            int intMissingMapImageCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (list.IndexOf(Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID"))) >= 0 && string.IsNullOrEmpty(view.GetRowCellValue(i, "CompletionSheetFile").ToString()))
                {
                    view.SelectRow(i);
                    if (string.IsNullOrWhiteSpace(view.GetRowCellValue(i, "KMLFile").ToString())) intMissingKMLFileCount++;
                    if (string.IsNullOrWhiteSpace(view.GetRowCellValue(i, "KMLImageFile").ToString())) intMissingMapImageCount++;
                }
            }
            view.EndSelection();
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
            string strMessage = "";
            if (intMissingKMLFileCount > 0) strMessage += intMissingKMLFileCount.ToString() + " Selected Job(s) have missing KML Files.\n";
            if (intMissingMapImageCount > 0) strMessage += intMissingMapImageCount.ToString() + " Selected Job(s) have missing Map Image Files.\n";
            if (!string.IsNullOrWhiteSpace(strMessage))
            {
                strMessage+= "\nYou may wish to select the Create KML \\ Map Images menu item before Creating Completion Sheets.";
                XtraMessageBox.Show(strMessage, "Create Completion Sheets", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void bbiPreviewCompletionSheets_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Only jobs with a Status of 120, 150, 160, 170, 180, 190 //
            List<int> list = new List<int>() { 120, 150, 160, 170, 180, 190 };

            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Callouts to Preview Completion Sheets for by clicking on them then try again.", "Preview Callout Completion Sheets", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (list.IndexOf(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID"))) <= -1) view.UnselectRow(intRowHandle);
            }
            view.EndSelection();

            // Check there is at least one row still selected //
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Callouts to Preview Completion Sheets for by clicking on them then try again.\n\nOnly Completed Callouts can be selected. If other Callouts are selected this process will de-select them.", "Preview Callout Completion Sheets", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            bool boolIncludeMaps = Convert.ToBoolean(beiShowMapsOnCompletionSheets.EditValue);
            string strMapImagePath = "";
            string strPicturePath = "";

            if (boolIncludeMaps)
            {
                strMapImagePath = Get_Path(3, "GrittingLocationTrackImageFilesFolder");
                if (string.IsNullOrWhiteSpace(strMapImagePath))
                {
                    boolIncludeMaps = false;
                    XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the saved KML Image files.\n\nNo Map Images will be created for the completion reports generated", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            strPicturePath = Get_Path(3, "GrittingCalloutPictureFilesFolder");
            if (string.IsNullOrWhiteSpace(strPicturePath))
            {
                XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Picture files.\n\nUnable to Proceed!", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + ",");
            }
            var fChildForm = new frm_GC_Gritting_Callout_Completion_Sheet();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm._iBool_CompletionSheetEditLayoutButtonEnabled = _iBool_CompletionSheetEditLayoutButtonEnabled;
            fChildForm._strSignaturePath = strPicturePath;
            fChildForm._strPicturePath = strPicturePath;
            fChildForm._strMapImagePath = strMapImagePath;
            fChildForm._strGrittingCalloutID = sb.ToString();
            fChildForm._ShowMaps = (boolIncludeMaps ? 1 : 0);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiCompletionSheet_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Only jobs with a Status of 120, 150, 160, 170, 180, 190 //
            List<int> list = new List<int>() { 120, 150, 160, 170, 180, 190 };

            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Callouts to Preview Completion Sheets for by clicking on them then try again.", "Preview Callout Completion Sheets", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (list.IndexOf(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID"))) <= -1) view.UnselectRow(intRowHandle);
            }
            view.EndSelection();

            // Check there is at least one row still selected //
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Callouts to Preview Completion Sheets for by clicking on them then try again.\n\nOnly Completed Callouts can be selected. If other Callouts are selected this process will de-select them.", "Preview Callout Completion Sheets", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            bool boolIncludeMaps = Convert.ToBoolean(beiShowMapsOnCompletionSheets.EditValue);
            string strMapImagePath = "";
            string strPicturePath = "";
            string strCompletionSheetPath = "";
            string strCompletionSheetFilename = "";
            string strFullCompletionSheetPath = "";
            string strReportLayoutFolder = "";
            int intCalloutID = 0;

            if (boolIncludeMaps)
            {
                strMapImagePath = Get_Path(3, "GrittingLocationTrackImageFilesFolder");
                if (string.IsNullOrWhiteSpace(strMapImagePath))
                {
                    boolIncludeMaps = false;
                    XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the saved KML Image files.\n\nNo Map Images will be created for the completion reports generated", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            strPicturePath = Get_Path(3, "GrittingCalloutPictureFilesFolderInternal"); //"GrittingCalloutPictureFilesFolder");
            if (string.IsNullOrWhiteSpace(strPicturePath))
            {
                XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Picture files.\n\nUnable to Proceed!", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            strCompletionSheetPath = Get_Path(3, "GrittingCalloutCompletionSheetsFolder");
            if (string.IsNullOrWhiteSpace(strCompletionSheetPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the saved Completion Sheet files.\n\nUnable to Proceed!", "Get Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            strReportLayoutFolder = Get_Path(3, "GrittingReportLayouts");
            if (string.IsNullOrWhiteSpace(strCompletionSheetPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Completion Sheet Folder (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "You have " + (intCount == 1 ? "1 Callout" : Convert.ToString(intRowHandles.Length) + " Callouts") + " selected for Completion Sheet Creation.\n\nProceed?\n\nNote: This process may take some time.";
            if (XtraMessageBox.Show(strMessage, "Create Completion Sheets", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) != DialogResult.Yes) return;

            DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter UpdateCallout = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
            UpdateCallout.ChangeConnectionString(strConnectionString);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Creating Completion Sheets...");

            int intCreated = 0;
            int intNotCreated = 0;

            // Completions Sheets now ready to be created //
            string strReportFileName = "GrittingCalloutCompletionSheet.repx";
            foreach (int intRowHandle in intRowHandles)
            {
                intCalloutID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "GrittingCallOutID"));
                strCompletionSheetFilename = intCalloutID.ToString().PadLeft(8, '0') + ".PDF";
                strFullCompletionSheetPath = Path.Combine(strCompletionSheetPath, strCompletionSheetFilename);

                try
                {
                    // Create PDF File //
                    // Load Report into memory //
                    var rptReport = new rpt_WM_Callout_Completion_Sheet(this.GlobalSettings, intCalloutID.ToString() + ",", strMapImagePath, strPicturePath, strPicturePath, (boolIncludeMaps ? 1 : 0));
                    rptReport.LoadLayout(Path.Combine(strReportLayoutFolder, strReportFileName));

                    // Set security options of report so when it is exported, it can't be edited and is password protected //
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
                    rptReport.ExportOptions.Pdf.Compressed = true;
                    rptReport.ExportOptions.Pdf.ImageQuality = PdfJpegImageQuality.Low;
                    rptReport.ExportToPdf(strFullCompletionSheetPath);

                    UpdateCallout.sp04374_WM_Write_Filename_To_Callout(intCalloutID, 3, strCompletionSheetFilename);  // Write PDF filename to callout record //
                    view.SetRowCellValue(intRowHandle, "CompletionSheetFile", strCompletionSheetFilename);  // Refresh grid value //
                    intCreated++;
                }
                catch (Exception) 
                {
                    intNotCreated++;
                }
            }
            if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
            strMessage = (intNotCreated > 0 ? (intNotCreated == 1 ? "1 Completion Sheet" : Convert.ToString(intNotCreated) + "  Completion Sheets") + " <color=red>Not Created</color>.\n" : "");
            strMessage += (intCreated == 1 ? "1 Completion Sheet" : Convert.ToString(intCreated) + "  Completion Sheets") + " Created.\n";
            XtraMessageBox.Show(strMessage, "Create Completion Sheets", MessageBoxButtons.OK, (intNotCreated > 0 ? MessageBoxIcon.Exclamation : MessageBoxIcon.Information), DefaultBoolean.True);
        }

        #endregion


        private void bciFilterVisitsSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (bciFilterVisitsSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Callout records to filter by before proceeding.", "Filter Selected Callout Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControl1.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControl1.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControl1.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_GC_Core .sp04074_GC_Gritting_Callout_Manager.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControl1.EndUpdate();
        }

        private string Get_Path(int ModuleID, string strPathName)
        {
            string strPath = "";
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionStringREADONLY);
            try
            {
                strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(ModuleID, strPathName).ToString();
            }
            catch (Exception) { }
            return strPath;
        }


        #region Background Worker

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Populate_Visits();
            if (backgroundWorker1.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            e.Result = "Success";
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            strEditedVisitIDs = "";  // Clear - it will be re-populated if we need to do a partial reload by the caller //
            UpdateRefreshStatus = 0; // Clear - it will be re-populated if we need to do a partial reload by the caller //

            if (e.Cancelled)
            {
                sdaDataMerge = null;
                dsDataMerge = null;
                SetLoadingVisibility(false);
                boolProcessRunning = false;
                return;
            }
            else if (e.Error != null)
            {
                MessageBox.Show("An Error Occurred While Attempting to Load Data.\n\nError: " + (e.Error as Exception).ToString());
            }
            else
            {
                if (intErrorValue < 0)
                {
                    sdaDataMerge = null;
                    dsDataMerge = null;
                    SetLoadingVisibility(false);
                    boolProcessRunning = false;
                    switch (intErrorValue)
                    {
                        case -2:
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Callouts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            break;
                        case -3:
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Callouts Data.\n\nMessage = [" + strErrorMessage + "].\n\nIf the problem persists, contact Technical Support.", "Load Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            break;
                        case -1:
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Callouts Data.\n\nMessage = [" + strErrorMessage + "].\n\nIf the problem persists, contact Technical Support.", "Load Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            break;
                        default:
                            break;
                    }
                    return;
                }
                try
                {
                    beiLoadingProgress.EditValue = "Merging Data...";
                    Application.DoEvents();   // Allow Form time to repaint itself so updated caption is made visible form line above //
                    dataSet_GC_Core.sp04074_GC_Gritting_Callout_Manager.Merge(dsDataMerge.Tables[0]);

                    sdaDataMerge = null;
                    dsDataMerge = null;

                    GridView view = (GridView)gridControl1.MainView;
                    if (tabbedControlGroupFilterType.SelectedTabPageIndex == 1 && strPassedInDrillDownIDs != "") view.ExpandAllGroups();  // Drilldown so expand all groups //

                    // Highlight any recently added new rows //
                    if (i_str_AddedRecordIDs1 != "")
                    {
                        char[] delimiters = new char[] { ';' };
                        string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        int intID = 0;
                        int intRowHandle = 0;
                        view.BeginSelection();
                        view.ClearSelection(); // Clear any current selection so just the new record is selected //
                        foreach (string strElement in strArray)
                        {
                            intID = Convert.ToInt32(strElement);
                            intRowHandle = view.LocateByValue(0, view.Columns["GrittingCallOutID"], intID);
                            if (intRowHandle != GridControl.InvalidRowHandle)
                            {
                                view.SelectRow(intRowHandle);
                                view.MakeRowVisible(intRowHandle, false);
                                view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);  // Tick the new record so it is picked up by the calendar if it is active //
                            }
                        }
                        i_str_AddedRecordIDs1 = "";
                        view.EndSelection();
                    }
                    SetLoadingVisibility(false);
                    boolProcessRunning = false;
                    LoadLinkedRecords();  // Refresh any children //
                }
                catch (Exception) { }
            }
        }

        private void bbiLoadingCancel_ItemClick(object sender, ItemClickEventArgs e)
        {
            //notify background worker we want to cancel the operation.
            //this code doesn't actually cancel or kill the thread that is executing the job.
            try
            {
                cmd.Cancel();  // Kill the SQL Command //
                cmd = null;
                backgroundWorker1.CancelAsync();  // Tell the thread it is cancelled - the line 2 above does the cancel since there is no loop in the DoWork to check for the cancelled status of the thread //
            }
            catch (Exception) { }
        }

        private void SetLoadingVisibility(Boolean boolVisible)
        {
            bbiRefresh.Visibility = (boolVisible ? BarItemVisibility.Never : BarItemVisibility.Always);
            beiLoadingProgress.Visibility = (boolVisible ? BarItemVisibility.Always : BarItemVisibility.Never);
            bbiLoadingCancel.Visibility = (boolVisible ? BarItemVisibility.Always : BarItemVisibility.Never);
            if (!boolVisible) beiLoadingProgress.EditValue = "Loading Data...";
        }




        #endregion

    }
}



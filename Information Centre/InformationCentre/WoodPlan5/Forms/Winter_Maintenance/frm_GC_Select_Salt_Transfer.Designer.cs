namespace WoodPlan5
{
    partial class frm_GC_Select_Salt_Transfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Select_Salt_Transfer));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04215GCSaltTransferSelectListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGritTransferID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferredByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromLocationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToLocationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountTransferred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAmountTransferredDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUnitDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountTransferredConverted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25KGBags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTransferCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSaltColourID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp04215_GC_Salt_Transfer_Select_ListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04215_GC_Salt_Transfer_Select_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04215GCSaltTransferSelectListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KGBags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 432);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 406);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 406);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.MaxItemId = 31;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp04215GCSaltTransferSelectListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEdit25KGBags,
            this.repositoryItemTextEditCurrency});
            this.gridControl1.Size = new System.Drawing.Size(628, 329);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04215GCSaltTransferSelectListBindingSource
            // 
            this.sp04215GCSaltTransferSelectListBindingSource.DataMember = "sp04215_GC_Salt_Transfer_Select_List";
            this.sp04215GCSaltTransferSelectListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGritTransferID,
            this.colTransferredByStaffID,
            this.colTransferDate,
            this.colFromLocationID,
            this.colFromLocationTypeID,
            this.colFromLocationName,
            this.colFromLocationType,
            this.colToLocationID,
            this.colToLocationTypeID,
            this.colToLocationName,
            this.colToLocationType,
            this.colAmountTransferred,
            this.colAmountTransferredDescriptorID,
            this.colSaltUnitDescription,
            this.colAmountTransferredConverted,
            this.colTransferCost,
            this.colRemarks,
            this.colSaltColourID,
            this.colSaltColour});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colGritTransferID
            // 
            this.colGritTransferID.Caption = "Salt Transfer ID";
            this.colGritTransferID.FieldName = "GritTransferID";
            this.colGritTransferID.Name = "colGritTransferID";
            this.colGritTransferID.OptionsColumn.AllowEdit = false;
            this.colGritTransferID.OptionsColumn.AllowFocus = false;
            this.colGritTransferID.OptionsColumn.ReadOnly = true;
            this.colGritTransferID.Width = 97;
            // 
            // colTransferredByStaffID
            // 
            this.colTransferredByStaffID.Caption = "Transferred By Staff ID";
            this.colTransferredByStaffID.FieldName = "TransferredByStaffID";
            this.colTransferredByStaffID.Name = "colTransferredByStaffID";
            this.colTransferredByStaffID.OptionsColumn.AllowEdit = false;
            this.colTransferredByStaffID.OptionsColumn.AllowFocus = false;
            this.colTransferredByStaffID.OptionsColumn.ReadOnly = true;
            this.colTransferredByStaffID.Width = 134;
            // 
            // colTransferDate
            // 
            this.colTransferDate.Caption = "Tranfer Date\\Time";
            this.colTransferDate.FieldName = "TransferDate";
            this.colTransferDate.Name = "colTransferDate";
            this.colTransferDate.OptionsColumn.AllowEdit = false;
            this.colTransferDate.OptionsColumn.AllowFocus = false;
            this.colTransferDate.OptionsColumn.ReadOnly = true;
            this.colTransferDate.Visible = true;
            this.colTransferDate.VisibleIndex = 0;
            this.colTransferDate.Width = 109;
            // 
            // colFromLocationID
            // 
            this.colFromLocationID.Caption = "From Location ID";
            this.colFromLocationID.FieldName = "FromLocationID";
            this.colFromLocationID.Name = "colFromLocationID";
            this.colFromLocationID.OptionsColumn.AllowEdit = false;
            this.colFromLocationID.OptionsColumn.AllowFocus = false;
            this.colFromLocationID.OptionsColumn.ReadOnly = true;
            this.colFromLocationID.Width = 102;
            // 
            // colFromLocationTypeID
            // 
            this.colFromLocationTypeID.Caption = "From Location Type ID";
            this.colFromLocationTypeID.FieldName = "FromLocationTypeID";
            this.colFromLocationTypeID.Name = "colFromLocationTypeID";
            this.colFromLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colFromLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colFromLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colFromLocationTypeID.Width = 129;
            // 
            // colFromLocationName
            // 
            this.colFromLocationName.Caption = "From Location";
            this.colFromLocationName.FieldName = "FromLocationName";
            this.colFromLocationName.Name = "colFromLocationName";
            this.colFromLocationName.OptionsColumn.AllowEdit = false;
            this.colFromLocationName.OptionsColumn.AllowFocus = false;
            this.colFromLocationName.OptionsColumn.ReadOnly = true;
            this.colFromLocationName.Visible = true;
            this.colFromLocationName.VisibleIndex = 1;
            this.colFromLocationName.Width = 174;
            // 
            // colFromLocationType
            // 
            this.colFromLocationType.Caption = "From Location Type";
            this.colFromLocationType.FieldName = "FromLocationType";
            this.colFromLocationType.Name = "colFromLocationType";
            this.colFromLocationType.OptionsColumn.AllowEdit = false;
            this.colFromLocationType.OptionsColumn.AllowFocus = false;
            this.colFromLocationType.OptionsColumn.ReadOnly = true;
            this.colFromLocationType.Visible = true;
            this.colFromLocationType.VisibleIndex = 2;
            this.colFromLocationType.Width = 115;
            // 
            // colToLocationID
            // 
            this.colToLocationID.Caption = "To Location ID";
            this.colToLocationID.FieldName = "ToLocationID";
            this.colToLocationID.Name = "colToLocationID";
            this.colToLocationID.OptionsColumn.AllowEdit = false;
            this.colToLocationID.OptionsColumn.AllowFocus = false;
            this.colToLocationID.OptionsColumn.ReadOnly = true;
            this.colToLocationID.Width = 90;
            // 
            // colToLocationTypeID
            // 
            this.colToLocationTypeID.Caption = "To Location Type ID";
            this.colToLocationTypeID.FieldName = "ToLocationTypeID";
            this.colToLocationTypeID.Name = "colToLocationTypeID";
            this.colToLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colToLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colToLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colToLocationTypeID.Width = 117;
            // 
            // colToLocationName
            // 
            this.colToLocationName.Caption = "To Location";
            this.colToLocationName.FieldName = "ToLocationName";
            this.colToLocationName.Name = "colToLocationName";
            this.colToLocationName.OptionsColumn.AllowEdit = false;
            this.colToLocationName.OptionsColumn.AllowFocus = false;
            this.colToLocationName.OptionsColumn.ReadOnly = true;
            this.colToLocationName.Visible = true;
            this.colToLocationName.VisibleIndex = 3;
            this.colToLocationName.Width = 160;
            // 
            // colToLocationType
            // 
            this.colToLocationType.Caption = "To Location Type";
            this.colToLocationType.FieldName = "ToLocationType";
            this.colToLocationType.Name = "colToLocationType";
            this.colToLocationType.OptionsColumn.AllowEdit = false;
            this.colToLocationType.OptionsColumn.AllowFocus = false;
            this.colToLocationType.OptionsColumn.ReadOnly = true;
            this.colToLocationType.Visible = true;
            this.colToLocationType.VisibleIndex = 4;
            this.colToLocationType.Width = 103;
            // 
            // colAmountTransferred
            // 
            this.colAmountTransferred.Caption = "Amount Transferred";
            this.colAmountTransferred.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colAmountTransferred.FieldName = "AmountTransferred";
            this.colAmountTransferred.Name = "colAmountTransferred";
            this.colAmountTransferred.OptionsColumn.AllowEdit = false;
            this.colAmountTransferred.OptionsColumn.AllowFocus = false;
            this.colAmountTransferred.OptionsColumn.ReadOnly = true;
            this.colAmountTransferred.Visible = true;
            this.colAmountTransferred.VisibleIndex = 5;
            this.colAmountTransferred.Width = 118;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colAmountTransferredDescriptorID
            // 
            this.colAmountTransferredDescriptorID.Caption = "Amount Transferred Descriptor ID";
            this.colAmountTransferredDescriptorID.FieldName = "AmountTransferredDescriptorID";
            this.colAmountTransferredDescriptorID.Name = "colAmountTransferredDescriptorID";
            this.colAmountTransferredDescriptorID.OptionsColumn.AllowEdit = false;
            this.colAmountTransferredDescriptorID.OptionsColumn.AllowFocus = false;
            this.colAmountTransferredDescriptorID.OptionsColumn.ReadOnly = true;
            this.colAmountTransferredDescriptorID.Width = 184;
            // 
            // colSaltUnitDescription
            // 
            this.colSaltUnitDescription.Caption = "Salt Unit Descriptor";
            this.colSaltUnitDescription.FieldName = "SaltUnitDescription";
            this.colSaltUnitDescription.Name = "colSaltUnitDescription";
            this.colSaltUnitDescription.OptionsColumn.AllowEdit = false;
            this.colSaltUnitDescription.OptionsColumn.AllowFocus = false;
            this.colSaltUnitDescription.OptionsColumn.ReadOnly = true;
            this.colSaltUnitDescription.Visible = true;
            this.colSaltUnitDescription.VisibleIndex = 6;
            this.colSaltUnitDescription.Width = 113;
            // 
            // colAmountTransferredConverted
            // 
            this.colAmountTransferredConverted.Caption = "Amount Converted";
            this.colAmountTransferredConverted.ColumnEdit = this.repositoryItemTextEdit25KGBags;
            this.colAmountTransferredConverted.FieldName = "AmountTransferredConverted";
            this.colAmountTransferredConverted.Name = "colAmountTransferredConverted";
            this.colAmountTransferredConverted.OptionsColumn.AllowEdit = false;
            this.colAmountTransferredConverted.OptionsColumn.AllowFocus = false;
            this.colAmountTransferredConverted.OptionsColumn.ReadOnly = true;
            this.colAmountTransferredConverted.Visible = true;
            this.colAmountTransferredConverted.VisibleIndex = 7;
            this.colAmountTransferredConverted.Width = 112;
            // 
            // repositoryItemTextEdit25KGBags
            // 
            this.repositoryItemTextEdit25KGBags.AutoHeight = false;
            this.repositoryItemTextEdit25KGBags.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25KGBags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KGBags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KGBags.Name = "repositoryItemTextEdit25KGBags";
            // 
            // colTransferCost
            // 
            this.colTransferCost.Caption = "Transfer Cost";
            this.colTransferCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTransferCost.FieldName = "TransferCost";
            this.colTransferCost.Name = "colTransferCost";
            this.colTransferCost.OptionsColumn.AllowEdit = false;
            this.colTransferCost.OptionsColumn.AllowFocus = false;
            this.colTransferCost.OptionsColumn.ReadOnly = true;
            this.colTransferCost.Visible = true;
            this.colTransferCost.VisibleIndex = 9;
            this.colTransferCost.Width = 87;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colSaltColourID
            // 
            this.colSaltColourID.Caption = "Salt Colour ID";
            this.colSaltColourID.FieldName = "SaltColourID";
            this.colSaltColourID.Name = "colSaltColourID";
            this.colSaltColourID.OptionsColumn.AllowEdit = false;
            this.colSaltColourID.OptionsColumn.AllowFocus = false;
            this.colSaltColourID.OptionsColumn.ReadOnly = true;
            this.colSaltColourID.Width = 87;
            // 
            // colSaltColour
            // 
            this.colSaltColour.Caption = "Salt Colour";
            this.colSaltColour.FieldName = "SaltColour";
            this.colSaltColour.Name = "colSaltColour";
            this.colSaltColour.OptionsColumn.AllowEdit = false;
            this.colSaltColour.OptionsColumn.AllowFocus = false;
            this.colSaltColour.OptionsColumn.ReadOnly = true;
            this.colSaltColour.Visible = true;
            this.colSaltColour.VisibleIndex = 8;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(461, 401);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(542, 401);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupSizeable = false;
            this.repositoryItemPopupContainerEdit1.ShowPopupCloseButton = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 29);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(628, 366);
            this.splitContainerControl1.SplitterPosition = 31;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnLoad);
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(464, 222, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(628, 31);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(560, 4);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(64, 22);
            this.btnLoad.StyleController = this.layoutControl1;
            this.btnLoad.TabIndex = 13;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(268, 4);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all tr" +
    "ansfers will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, true)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(146, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 12;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(61, 4);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all tr" +
    "ansfers will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, true)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(146, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 11;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 31);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDate;
            this.layoutControlItem1.CustomizationFormText = "From Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(207, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(207, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(207, 27);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "From Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDate;
            this.layoutControlItem2.CustomizationFormText = "To Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(207, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(207, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(207, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(207, 27);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "To Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnLoad;
            this.layoutControlItem3.CustomizationFormText = "Load Button";
            this.layoutControlItem3.Location = new System.Drawing.Point(556, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(68, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(68, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(68, 27);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Load Button";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(414, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(142, 27);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp04215_GC_Salt_Transfer_Select_ListTableAdapter
            // 
            this.sp04215_GC_Salt_Transfer_Select_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Select_Salt_Transfer
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(628, 432);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_GC_Select_Salt_Transfer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Salt Transfer";
            this.Load += new System.EventHandler(this.frm_GC_Select_Salt_Transfer_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04215GCSaltTransferSelectListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KGBags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DataSet_GC_CoreTableAdapters.sp04206_GC_Salt_Supplier_Select_ListTableAdapter sp04206_GC_Salt_Supplier_Select_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.BindingSource sp04215GCSaltTransferSelectListBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private DataSet_GC_CoreTableAdapters.sp04215_GC_Salt_Transfer_Select_ListTableAdapter sp04215_GC_Salt_Transfer_Select_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colGritTransferID;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferredByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferDate;
        private DevExpress.XtraGrid.Columns.GridColumn colFromLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colFromLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colFromLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colFromLocationType;
        private DevExpress.XtraGrid.Columns.GridColumn colToLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colToLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colToLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colToLocationType;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountTransferred;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountTransferredDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUnitDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountTransferredConverted;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KGBags;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltColourID;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltColour;
    }
}

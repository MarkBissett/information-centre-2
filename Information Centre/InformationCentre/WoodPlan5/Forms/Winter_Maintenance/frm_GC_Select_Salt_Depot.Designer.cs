namespace WoodPlan5
{
    partial class frm_GC_Select_Salt_Depot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04211GCSaltDepotSelectListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactNamePosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentCostConverted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCurrentGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepotName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepotType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUrgentGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colWarningGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepotTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlSearchRadius = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnOKSearchRadius = new DevExpress.XtraEditors.SimpleButton();
            this.radiusLabel = new DevExpress.XtraEditors.LabelControl();
            this.trackBarControlRadius = new DevExpress.XtraEditors.TrackBarControl();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemSearchRadius = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp04211_GC_Salt_Depot_Select_ListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04211_GC_Salt_Depot_Select_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04211GCSaltDepotSelectListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSearchRadius)).BeginInit();
            this.popupContainerControlSearchRadius.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlRadius.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 432);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 406);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 406);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemSearchRadius});
            this.barManager1.MaxItemId = 31;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp04211GCSaltDepotSelectListBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 24);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEdit2DP});
            this.gridControl1.Size = new System.Drawing.Size(628, 369);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04211GCSaltDepotSelectListBindingSource
            // 
            this.sp04211GCSaltDepotSelectListBindingSource.DataMember = "sp04211_GC_Salt_Depot_Select_List";
            this.sp04211GCSaltDepotSelectListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colPostcode,
            this.colTelephone,
            this.colMobile,
            this.colFax,
            this.colText,
            this.colEmail,
            this.colWebSite,
            this.colContactName,
            this.colContactNamePosition,
            this.colLatitude,
            this.colLongitude,
            this.colEmailPassword,
            this.colCurrentCostConverted,
            this.colCurrentGritLevel,
            this.colDepotName,
            this.colDepotType,
            this.colGritDepotID,
            this.colUrgentGritLevel,
            this.colWarningGritLevel,
            this.colRemarks,
            this.colDepotTypeID});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepotType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepotName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 4;
            this.colAddressLine1.Width = 91;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 5;
            // 
            // colTelephone
            // 
            this.colTelephone.Caption = "Telephone";
            this.colTelephone.FieldName = "Telephone";
            this.colTelephone.Name = "colTelephone";
            this.colTelephone.OptionsColumn.AllowEdit = false;
            this.colTelephone.OptionsColumn.AllowFocus = false;
            this.colTelephone.OptionsColumn.ReadOnly = true;
            this.colTelephone.Visible = true;
            this.colTelephone.VisibleIndex = 6;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Visible = true;
            this.colMobile.VisibleIndex = 7;
            // 
            // colFax
            // 
            this.colFax.Caption = "Fax";
            this.colFax.FieldName = "Fax";
            this.colFax.Name = "colFax";
            this.colFax.OptionsColumn.AllowEdit = false;
            this.colFax.OptionsColumn.AllowFocus = false;
            this.colFax.OptionsColumn.ReadOnly = true;
            this.colFax.Visible = true;
            this.colFax.VisibleIndex = 8;
            // 
            // colText
            // 
            this.colText.Caption = "Text";
            this.colText.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colText.FieldName = "Text";
            this.colText.Name = "colText";
            this.colText.OptionsColumn.ReadOnly = true;
            this.colText.Visible = true;
            this.colText.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Visible = true;
            this.colEmail.VisibleIndex = 10;
            // 
            // colWebSite
            // 
            this.colWebSite.Caption = "Website";
            this.colWebSite.FieldName = "WebSite";
            this.colWebSite.Name = "colWebSite";
            this.colWebSite.OptionsColumn.AllowEdit = false;
            this.colWebSite.OptionsColumn.AllowFocus = false;
            this.colWebSite.OptionsColumn.ReadOnly = true;
            this.colWebSite.Visible = true;
            this.colWebSite.VisibleIndex = 12;
            // 
            // colContactName
            // 
            this.colContactName.Caption = "Contact Name";
            this.colContactName.FieldName = "ContactName";
            this.colContactName.Name = "colContactName";
            this.colContactName.OptionsColumn.AllowEdit = false;
            this.colContactName.OptionsColumn.AllowFocus = false;
            this.colContactName.OptionsColumn.ReadOnly = true;
            this.colContactName.Visible = true;
            this.colContactName.VisibleIndex = 13;
            this.colContactName.Width = 89;
            // 
            // colContactNamePosition
            // 
            this.colContactNamePosition.Caption = "Contact Name Position";
            this.colContactNamePosition.FieldName = "ContactNamePosition";
            this.colContactNamePosition.Name = "colContactNamePosition";
            this.colContactNamePosition.OptionsColumn.AllowEdit = false;
            this.colContactNamePosition.OptionsColumn.AllowFocus = false;
            this.colContactNamePosition.OptionsColumn.ReadOnly = true;
            this.colContactNamePosition.Visible = true;
            this.colContactNamePosition.VisibleIndex = 14;
            this.colContactNamePosition.Width = 129;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Visible = true;
            this.colEmailPassword.VisibleIndex = 11;
            this.colEmailPassword.Width = 94;
            // 
            // colCurrentCostConverted
            // 
            this.colCurrentCostConverted.Caption = "Cost Per Unit";
            this.colCurrentCostConverted.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCurrentCostConverted.FieldName = "CurrentCostConverted";
            this.colCurrentCostConverted.Name = "colCurrentCostConverted";
            this.colCurrentCostConverted.OptionsColumn.AllowEdit = false;
            this.colCurrentCostConverted.OptionsColumn.AllowFocus = false;
            this.colCurrentCostConverted.OptionsColumn.ReadOnly = true;
            this.colCurrentCostConverted.Visible = true;
            this.colCurrentCostConverted.VisibleIndex = 15;
            this.colCurrentCostConverted.Width = 84;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colCurrentGritLevel
            // 
            this.colCurrentGritLevel.Caption = "Current Salt Level";
            this.colCurrentGritLevel.FieldName = "CurrentGritLevel";
            this.colCurrentGritLevel.Name = "colCurrentGritLevel";
            this.colCurrentGritLevel.OptionsColumn.AllowEdit = false;
            this.colCurrentGritLevel.OptionsColumn.AllowFocus = false;
            this.colCurrentGritLevel.OptionsColumn.ReadOnly = true;
            this.colCurrentGritLevel.Visible = true;
            this.colCurrentGritLevel.VisibleIndex = 1;
            this.colCurrentGritLevel.Width = 107;
            // 
            // colDepotName
            // 
            this.colDepotName.Caption = "Depot Name";
            this.colDepotName.FieldName = "DepotName";
            this.colDepotName.Name = "colDepotName";
            this.colDepotName.OptionsColumn.AllowEdit = false;
            this.colDepotName.OptionsColumn.AllowFocus = false;
            this.colDepotName.OptionsColumn.ReadOnly = true;
            this.colDepotName.Visible = true;
            this.colDepotName.VisibleIndex = 0;
            this.colDepotName.Width = 202;
            // 
            // colDepotType
            // 
            this.colDepotType.Caption = "Depot Description";
            this.colDepotType.FieldName = "DepotType";
            this.colDepotType.Name = "colDepotType";
            this.colDepotType.OptionsColumn.AllowEdit = false;
            this.colDepotType.OptionsColumn.AllowFocus = false;
            this.colDepotType.OptionsColumn.ReadOnly = true;
            this.colDepotType.Width = 106;
            // 
            // colGritDepotID
            // 
            this.colGritDepotID.Caption = "Grit Depot ID";
            this.colGritDepotID.FieldName = "GritDepotID";
            this.colGritDepotID.Name = "colGritDepotID";
            this.colGritDepotID.OptionsColumn.AllowEdit = false;
            this.colGritDepotID.OptionsColumn.AllowFocus = false;
            this.colGritDepotID.OptionsColumn.ReadOnly = true;
            this.colGritDepotID.Width = 84;
            // 
            // colUrgentGritLevel
            // 
            this.colUrgentGritLevel.Caption = "Urgent Level";
            this.colUrgentGritLevel.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colUrgentGritLevel.FieldName = "UrgentGritLevel";
            this.colUrgentGritLevel.Name = "colUrgentGritLevel";
            this.colUrgentGritLevel.OptionsColumn.AllowEdit = false;
            this.colUrgentGritLevel.OptionsColumn.AllowFocus = false;
            this.colUrgentGritLevel.OptionsColumn.ReadOnly = true;
            this.colUrgentGritLevel.Visible = true;
            this.colUrgentGritLevel.VisibleIndex = 2;
            this.colUrgentGritLevel.Width = 82;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colWarningGritLevel
            // 
            this.colWarningGritLevel.Caption = "Warning Level";
            this.colWarningGritLevel.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colWarningGritLevel.FieldName = "WarningGritLevel";
            this.colWarningGritLevel.Name = "colWarningGritLevel";
            this.colWarningGritLevel.OptionsColumn.AllowEdit = false;
            this.colWarningGritLevel.OptionsColumn.AllowFocus = false;
            this.colWarningGritLevel.OptionsColumn.ReadOnly = true;
            this.colWarningGritLevel.Visible = true;
            this.colWarningGritLevel.VisibleIndex = 3;
            this.colWarningGritLevel.Width = 89;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 16;
            // 
            // colDepotTypeID
            // 
            this.colDepotTypeID.Caption = "Depot Type ID";
            this.colDepotTypeID.FieldName = "DepotTypeID";
            this.colDepotTypeID.Name = "colDepotTypeID";
            this.colDepotTypeID.OptionsColumn.AllowEdit = false;
            this.colDepotTypeID.OptionsColumn.AllowFocus = false;
            this.colDepotTypeID.OptionsColumn.ReadOnly = true;
            this.colDepotTypeID.Width = 91;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(461, 401);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(542, 401);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // popupContainerControlSearchRadius
            // 
            this.popupContainerControlSearchRadius.Controls.Add(this.btnOKSearchRadius);
            this.popupContainerControlSearchRadius.Controls.Add(this.radiusLabel);
            this.popupContainerControlSearchRadius.Controls.Add(this.trackBarControlRadius);
            this.popupContainerControlSearchRadius.Location = new System.Drawing.Point(26, 126);
            this.popupContainerControlSearchRadius.Name = "popupContainerControlSearchRadius";
            this.popupContainerControlSearchRadius.Size = new System.Drawing.Size(564, 74);
            this.popupContainerControlSearchRadius.TabIndex = 19;
            // 
            // btnOKSearchRadius
            // 
            this.btnOKSearchRadius.Location = new System.Drawing.Point(489, 45);
            this.btnOKSearchRadius.Name = "btnOKSearchRadius";
            this.btnOKSearchRadius.Size = new System.Drawing.Size(69, 23);
            this.btnOKSearchRadius.TabIndex = 18;
            this.btnOKSearchRadius.Text = "OK";
            this.btnOKSearchRadius.Click += new System.EventHandler(this.btnOKSearchRadius_Click);
            // 
            // radiusLabel
            // 
            this.radiusLabel.Location = new System.Drawing.Point(182, 45);
            this.radiusLabel.Name = "radiusLabel";
            this.radiusLabel.Size = new System.Drawing.Size(201, 13);
            this.radiusLabel.TabIndex = 17;
            this.radiusLabel.Text = "Postcode: XXXX - Seach Radius: XX miles. ";
            // 
            // trackBarControlRadius
            // 
            this.trackBarControlRadius.EditValue = null;
            this.trackBarControlRadius.Location = new System.Drawing.Point(3, 3);
            this.trackBarControlRadius.MenuManager = this.barManager1;
            this.trackBarControlRadius.Name = "trackBarControlRadius";
            this.trackBarControlRadius.Properties.Maximum = 250;
            this.trackBarControlRadius.Properties.ShowValueToolTip = true;
            this.trackBarControlRadius.Properties.TickFrequency = 5;
            this.trackBarControlRadius.Size = new System.Drawing.Size(558, 45);
            this.trackBarControlRadius.TabIndex = 16;
            this.trackBarControlRadius.ValueChanged += new System.EventHandler(this.trackBarControlRadius_ValueChanged);
            this.trackBarControlRadius.EditValueChanged += new System.EventHandler(this.trackBarControlRadius_EditValueChanged);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(749, 214);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemSearchRadius)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Extra Tools";
            // 
            // barEditItemSearchRadius
            // 
            this.barEditItemSearchRadius.Caption = "Search Radius:";
            this.barEditItemSearchRadius.Edit = this.repositoryItemPopupContainerEdit1;
            this.barEditItemSearchRadius.EditValue = "Disabled";
            this.barEditItemSearchRadius.EditWidth = 125;
            this.barEditItemSearchRadius.Id = 30;
            this.barEditItemSearchRadius.ItemAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.barEditItemSearchRadius.ItemAppearance.Normal.Options.UseBackColor = true;
            this.barEditItemSearchRadius.Name = "barEditItemSearchRadius";
            this.barEditItemSearchRadius.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControlSearchRadius;
            this.repositoryItemPopupContainerEdit1.PopupSizeable = false;
            this.repositoryItemPopupContainerEdit1.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditSearchRadius_QueryResultValue);
            // 
            // sp04211_GC_Salt_Depot_Select_ListTableAdapter
            // 
            this.sp04211_GC_Salt_Depot_Select_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Select_Salt_Depot
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(628, 432);
            this.Controls.Add(this.popupContainerControlSearchRadius);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gridControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_GC_Select_Salt_Depot";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Salt Depot";
            this.Load += new System.EventHandler(this.frm_GC_Select_Salt_Depot_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.popupContainerControlSearchRadius, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04211GCSaltDepotSelectListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSearchRadius)).EndInit();
            this.popupContainerControlSearchRadius.ResumeLayout(false);
            this.popupContainerControlSearchRadius.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlRadius.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colFax;
        private DevExpress.XtraGrid.Columns.GridColumn colText;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colWebSite;
        private DevExpress.XtraGrid.Columns.GridColumn colContactName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactNamePosition;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSearchRadius;
        private DevExpress.XtraEditors.SimpleButton btnOKSearchRadius;
        private DevExpress.XtraEditors.LabelControl radiusLabel;
        private DevExpress.XtraEditors.TrackBarControl trackBarControlRadius;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemSearchRadius;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private System.Windows.Forms.BindingSource sp04211GCSaltDepotSelectListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentCostConverted;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colDepotName;
        private DevExpress.XtraGrid.Columns.GridColumn colDepotType;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotID;
        private DevExpress.XtraGrid.Columns.GridColumn colUrgentGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DataSet_GC_CoreTableAdapters.sp04211_GC_Salt_Depot_Select_ListTableAdapter sp04211_GC_Salt_Depot_Select_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colDepotTypeID;
    }
}

namespace WoodPlan5
{
    partial class frm_GC_Snow_Contract_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Snow_Contract_Edit));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChargeMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.MinimumPictureCountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sp04140GCSnowClearanceSiteContractEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Snow_DataEntry = new WoodPlan5.DataSet_GC_Snow_DataEntry();
            this.AccessRestrictionsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ProactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ChargeMarkupPercentageSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ChargeHourlyExtraRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ChargeFixedPriceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SnowClearOnSundayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SnowClearOnSaturdayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SnowClearOnFridayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SnowClearOnThursdayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SnowClearOnWednesdayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SnowClearOnTuesdayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SnowClearOnMondayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ChargeHourlyFixedRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ChargeFixedNumberOfHoursSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AreaSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.StartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ContractManagerIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00226StaffListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SiteIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04139GCSitesWithBlankJustSnowClearSitesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.SnowClearanceSiteContractIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ChargeMethodIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04142GCSnowClearanceChargeMethodsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colChargeMethodDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChargeMethodOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForSnowClearanceSiteContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForArea = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSnowClearOnMonday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearOnTuesday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearOnWednesday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearOnThursday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearOnFriday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearOnSaturday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearOnSunday = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForChargeMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChargeFixedPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChargeFixedNumberOfHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChargeHourlyFixedRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChargeHourlyExtraRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChargeMarkupPercentage = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAccessRestrictions = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForContractManagerID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMinimumPictureCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00226_Staff_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter();
            this.sp04139_GC_Sites_With_Blank_Just_Snow_Clear_SitesTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04139_GC_Sites_With_Blank_Just_Snow_Clear_SitesTableAdapter();
            this.sp04140_GC_Snow_Clearance_Site_Contract_EditTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04140_GC_Snow_Clearance_Site_Contract_EditTableAdapter();
            this.sp04142_GC_Snow_Clearance_Charge_Methods_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04142_GC_Snow_Clearance_Charge_Methods_With_BlankTableAdapter();
            this.SeasonPeriodIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForSeasonPeriodID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.sp04373WMSeasonPeriodsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp04373_WM_Season_Periods_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04373_WM_Season_Periods_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumPictureCountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04140GCSnowClearanceSiteContractEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessRestrictionsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeMarkupPercentageSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeHourlyExtraRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeFixedPriceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnSundayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnSaturdayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnFridayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnThursdayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnWednesdayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnTuesdayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnMondayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeHourlyFixedRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeFixedNumberOfHoursSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractManagerIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04139GCSitesWithBlankJustSnowClearSitesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceSiteContractIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeMethodIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04142GCSnowClearanceChargeMethodsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceSiteContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnMonday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnTuesday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnWednesday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnThursday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnFriday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnSaturday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnSunday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeFixedPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeFixedNumberOfHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeHourlyFixedRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeHourlyExtraRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeMarkupPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessRestrictions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractManagerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumPictureCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeasonPeriodIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSeasonPeriodID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04373WMSeasonPeriodsWithBlankBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 502);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Staff ID";
            this.gridColumn57.FieldName = "StaffID";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Width = 59;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colChargeMethodID
            // 
            this.colChargeMethodID.Caption = "Charge Method ID";
            this.colChargeMethodID.FieldName = "ChargeMethodID";
            this.colChargeMethodID.Name = "colChargeMethodID";
            this.colChargeMethodID.OptionsColumn.AllowEdit = false;
            this.colChargeMethodID.OptionsColumn.AllowFocus = false;
            this.colChargeMethodID.OptionsColumn.ReadOnly = true;
            this.colChargeMethodID.Width = 99;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Save Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiFormSave.SuperTip = superToolTip4;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Cancel Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiFormCancel.SuperTip = superToolTip5;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Form Mode - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.barStaticItemFormMode.SuperTip = superToolTip6;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 502);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 476);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SeasonPeriodIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.MinimumPictureCountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AccessRestrictionsCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ProactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ChargeMarkupPercentageSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ChargeHourlyExtraRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ChargeFixedPriceSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearOnSundayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearOnSaturdayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearOnFridayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearOnThursdayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearOnWednesdayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearOnTuesdayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearOnMondayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ChargeHourlyFixedRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ChargeFixedNumberOfHoursSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AreaSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.StartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractManagerIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SnowClearanceSiteContractIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ChargeMethodIDGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp04140GCSnowClearanceSiteContractEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSnowClearanceSiteContractID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1170, 212, 705, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 476);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // MinimumPictureCountSpinEdit
            // 
            this.MinimumPictureCountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "MinimumPictureCount", true));
            this.MinimumPictureCountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MinimumPictureCountSpinEdit.Location = new System.Drawing.Point(143, 155);
            this.MinimumPictureCountSpinEdit.MenuManager = this.barManager1;
            this.MinimumPictureCountSpinEdit.Name = "MinimumPictureCountSpinEdit";
            this.MinimumPictureCountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MinimumPictureCountSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.MinimumPictureCountSpinEdit.Properties.IsFloatValue = false;
            this.MinimumPictureCountSpinEdit.Properties.Mask.EditMask = "N00";
            this.MinimumPictureCountSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.MinimumPictureCountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MinimumPictureCountSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.MinimumPictureCountSpinEdit.Size = new System.Drawing.Size(81, 20);
            this.MinimumPictureCountSpinEdit.StyleController = this.dataLayoutControl1;
            this.MinimumPictureCountSpinEdit.TabIndex = 68;
            // 
            // sp04140GCSnowClearanceSiteContractEditBindingSource
            // 
            this.sp04140GCSnowClearanceSiteContractEditBindingSource.DataMember = "sp04140_GC_Snow_Clearance_Site_Contract_Edit";
            this.sp04140GCSnowClearanceSiteContractEditBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // dataSet_GC_Snow_DataEntry
            // 
            this.dataSet_GC_Snow_DataEntry.DataSetName = "DataSet_GC_Snow_DataEntry";
            this.dataSet_GC_Snow_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // AccessRestrictionsCheckEdit
            // 
            this.AccessRestrictionsCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "AccessRestrictions", true));
            this.AccessRestrictionsCheckEdit.EditValue = 0;
            this.AccessRestrictionsCheckEdit.Location = new System.Drawing.Point(167, 357);
            this.AccessRestrictionsCheckEdit.MenuManager = this.barManager1;
            this.AccessRestrictionsCheckEdit.Name = "AccessRestrictionsCheckEdit";
            this.AccessRestrictionsCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.AccessRestrictionsCheckEdit.Properties.ValueChecked = 1;
            this.AccessRestrictionsCheckEdit.Properties.ValueUnchecked = 0;
            this.AccessRestrictionsCheckEdit.Size = new System.Drawing.Size(83, 19);
            this.AccessRestrictionsCheckEdit.StyleController = this.dataLayoutControl1;
            this.AccessRestrictionsCheckEdit.TabIndex = 44;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "Reactive", true));
            this.ReactiveCheckEdit.EditValue = 0;
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(143, 132);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(81, 19);
            this.ReactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCheckEdit.TabIndex = 43;
            // 
            // ProactiveCheckEdit
            // 
            this.ProactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "Proactive", true));
            this.ProactiveCheckEdit.EditValue = 0;
            this.ProactiveCheckEdit.Location = new System.Drawing.Point(143, 109);
            this.ProactiveCheckEdit.MenuManager = this.barManager1;
            this.ProactiveCheckEdit.Name = "ProactiveCheckEdit";
            this.ProactiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ProactiveCheckEdit.Properties.ValueChecked = 1;
            this.ProactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ProactiveCheckEdit.Size = new System.Drawing.Size(81, 19);
            this.ProactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ProactiveCheckEdit.TabIndex = 42;
            // 
            // ChargeMarkupPercentageSpinEdit
            // 
            this.ChargeMarkupPercentageSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "ChargeMarkupPercentage", true));
            this.ChargeMarkupPercentageSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ChargeMarkupPercentageSpinEdit.Location = new System.Drawing.Point(179, 544);
            this.ChargeMarkupPercentageSpinEdit.MenuManager = this.barManager1;
            this.ChargeMarkupPercentageSpinEdit.Name = "ChargeMarkupPercentageSpinEdit";
            this.ChargeMarkupPercentageSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ChargeMarkupPercentageSpinEdit.Properties.Mask.EditMask = "P";
            this.ChargeMarkupPercentageSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ChargeMarkupPercentageSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ChargeMarkupPercentageSpinEdit.Size = new System.Drawing.Size(124, 20);
            this.ChargeMarkupPercentageSpinEdit.StyleController = this.dataLayoutControl1;
            this.ChargeMarkupPercentageSpinEdit.TabIndex = 41;
            // 
            // ChargeHourlyExtraRateSpinEdit
            // 
            this.ChargeHourlyExtraRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "ChargeExtraHourlyRate", true));
            this.ChargeHourlyExtraRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ChargeHourlyExtraRateSpinEdit.Location = new System.Drawing.Point(179, 520);
            this.ChargeHourlyExtraRateSpinEdit.MenuManager = this.barManager1;
            this.ChargeHourlyExtraRateSpinEdit.Name = "ChargeHourlyExtraRateSpinEdit";
            this.ChargeHourlyExtraRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ChargeHourlyExtraRateSpinEdit.Properties.Mask.EditMask = "c";
            this.ChargeHourlyExtraRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ChargeHourlyExtraRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ChargeHourlyExtraRateSpinEdit.Size = new System.Drawing.Size(124, 20);
            this.ChargeHourlyExtraRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.ChargeHourlyExtraRateSpinEdit.TabIndex = 40;
            // 
            // ChargeFixedPriceSpinEdit
            // 
            this.ChargeFixedPriceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "ChargeFixedPrice", true));
            this.ChargeFixedPriceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ChargeFixedPriceSpinEdit.Location = new System.Drawing.Point(179, 448);
            this.ChargeFixedPriceSpinEdit.MenuManager = this.barManager1;
            this.ChargeFixedPriceSpinEdit.Name = "ChargeFixedPriceSpinEdit";
            this.ChargeFixedPriceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ChargeFixedPriceSpinEdit.Properties.Mask.EditMask = "c";
            this.ChargeFixedPriceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ChargeFixedPriceSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ChargeFixedPriceSpinEdit.Size = new System.Drawing.Size(124, 20);
            this.ChargeFixedPriceSpinEdit.StyleController = this.dataLayoutControl1;
            this.ChargeFixedPriceSpinEdit.TabIndex = 38;
            // 
            // SnowClearOnSundayCheckEdit
            // 
            this.SnowClearOnSundayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SnowClearOnSunday", true));
            this.SnowClearOnSundayCheckEdit.Location = new System.Drawing.Point(179, 762);
            this.SnowClearOnSundayCheckEdit.MenuManager = this.barManager1;
            this.SnowClearOnSundayCheckEdit.Name = "SnowClearOnSundayCheckEdit";
            this.SnowClearOnSundayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearOnSundayCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearOnSundayCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearOnSundayCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.SnowClearOnSundayCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearOnSundayCheckEdit.TabIndex = 37;
            // 
            // SnowClearOnSaturdayCheckEdit
            // 
            this.SnowClearOnSaturdayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SnowClearOnSaturday", true));
            this.SnowClearOnSaturdayCheckEdit.Location = new System.Drawing.Point(179, 739);
            this.SnowClearOnSaturdayCheckEdit.MenuManager = this.barManager1;
            this.SnowClearOnSaturdayCheckEdit.Name = "SnowClearOnSaturdayCheckEdit";
            this.SnowClearOnSaturdayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearOnSaturdayCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearOnSaturdayCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearOnSaturdayCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.SnowClearOnSaturdayCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearOnSaturdayCheckEdit.TabIndex = 36;
            // 
            // SnowClearOnFridayCheckEdit
            // 
            this.SnowClearOnFridayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SnowClearOnFriday", true));
            this.SnowClearOnFridayCheckEdit.Location = new System.Drawing.Point(179, 716);
            this.SnowClearOnFridayCheckEdit.MenuManager = this.barManager1;
            this.SnowClearOnFridayCheckEdit.Name = "SnowClearOnFridayCheckEdit";
            this.SnowClearOnFridayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearOnFridayCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearOnFridayCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearOnFridayCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.SnowClearOnFridayCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearOnFridayCheckEdit.TabIndex = 35;
            // 
            // SnowClearOnThursdayCheckEdit
            // 
            this.SnowClearOnThursdayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SnowClearOnThursday", true));
            this.SnowClearOnThursdayCheckEdit.Location = new System.Drawing.Point(179, 693);
            this.SnowClearOnThursdayCheckEdit.MenuManager = this.barManager1;
            this.SnowClearOnThursdayCheckEdit.Name = "SnowClearOnThursdayCheckEdit";
            this.SnowClearOnThursdayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearOnThursdayCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearOnThursdayCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearOnThursdayCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.SnowClearOnThursdayCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearOnThursdayCheckEdit.TabIndex = 34;
            // 
            // SnowClearOnWednesdayCheckEdit
            // 
            this.SnowClearOnWednesdayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SnowClearOnWednesday", true));
            this.SnowClearOnWednesdayCheckEdit.Location = new System.Drawing.Point(179, 670);
            this.SnowClearOnWednesdayCheckEdit.MenuManager = this.barManager1;
            this.SnowClearOnWednesdayCheckEdit.Name = "SnowClearOnWednesdayCheckEdit";
            this.SnowClearOnWednesdayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearOnWednesdayCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearOnWednesdayCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearOnWednesdayCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.SnowClearOnWednesdayCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearOnWednesdayCheckEdit.TabIndex = 33;
            // 
            // SnowClearOnTuesdayCheckEdit
            // 
            this.SnowClearOnTuesdayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SnowClearOnTuesday", true));
            this.SnowClearOnTuesdayCheckEdit.Location = new System.Drawing.Point(179, 647);
            this.SnowClearOnTuesdayCheckEdit.MenuManager = this.barManager1;
            this.SnowClearOnTuesdayCheckEdit.Name = "SnowClearOnTuesdayCheckEdit";
            this.SnowClearOnTuesdayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearOnTuesdayCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearOnTuesdayCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearOnTuesdayCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.SnowClearOnTuesdayCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearOnTuesdayCheckEdit.TabIndex = 32;
            // 
            // SnowClearOnMondayCheckEdit
            // 
            this.SnowClearOnMondayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SnowClearOnMonday", true));
            this.SnowClearOnMondayCheckEdit.Location = new System.Drawing.Point(179, 624);
            this.SnowClearOnMondayCheckEdit.MenuManager = this.barManager1;
            this.SnowClearOnMondayCheckEdit.Name = "SnowClearOnMondayCheckEdit";
            this.SnowClearOnMondayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClearOnMondayCheckEdit.Properties.ValueChecked = 1;
            this.SnowClearOnMondayCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClearOnMondayCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.SnowClearOnMondayCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearOnMondayCheckEdit.TabIndex = 31;
            // 
            // ChargeHourlyFixedRateSpinEdit
            // 
            this.ChargeHourlyFixedRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "ChargeFixedHourlyRate", true));
            this.ChargeHourlyFixedRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ChargeHourlyFixedRateSpinEdit.Location = new System.Drawing.Point(179, 496);
            this.ChargeHourlyFixedRateSpinEdit.MenuManager = this.barManager1;
            this.ChargeHourlyFixedRateSpinEdit.Name = "ChargeHourlyFixedRateSpinEdit";
            this.ChargeHourlyFixedRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ChargeHourlyFixedRateSpinEdit.Properties.Mask.EditMask = "c";
            this.ChargeHourlyFixedRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ChargeHourlyFixedRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ChargeHourlyFixedRateSpinEdit.Size = new System.Drawing.Size(124, 20);
            this.ChargeHourlyFixedRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.ChargeHourlyFixedRateSpinEdit.TabIndex = 28;
            // 
            // ChargeFixedNumberOfHoursSpinEdit
            // 
            this.ChargeFixedNumberOfHoursSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "ChargeFixedNumberOfHours", true));
            this.ChargeFixedNumberOfHoursSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ChargeFixedNumberOfHoursSpinEdit.Location = new System.Drawing.Point(179, 472);
            this.ChargeFixedNumberOfHoursSpinEdit.MenuManager = this.barManager1;
            this.ChargeFixedNumberOfHoursSpinEdit.Name = "ChargeFixedNumberOfHoursSpinEdit";
            this.ChargeFixedNumberOfHoursSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ChargeFixedNumberOfHoursSpinEdit.Properties.Mask.EditMask = "########0.00 Hours";
            this.ChargeFixedNumberOfHoursSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ChargeFixedNumberOfHoursSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ChargeFixedNumberOfHoursSpinEdit.Size = new System.Drawing.Size(124, 20);
            this.ChargeFixedNumberOfHoursSpinEdit.StyleController = this.dataLayoutControl1;
            this.ChargeFixedNumberOfHoursSpinEdit.TabIndex = 27;
            // 
            // AreaSpinEdit
            // 
            this.AreaSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "Area", true));
            this.AreaSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AreaSpinEdit.Location = new System.Drawing.Point(167, 333);
            this.AreaSpinEdit.MenuManager = this.barManager1;
            this.AreaSpinEdit.Name = "AreaSpinEdit";
            this.AreaSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.AreaSpinEdit.Properties.Mask.EditMask = "########0.00 M�";
            this.AreaSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AreaSpinEdit.Properties.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            131072});
            this.AreaSpinEdit.Size = new System.Drawing.Size(182, 20);
            this.AreaSpinEdit.StyleController = this.dataLayoutControl1;
            this.AreaSpinEdit.TabIndex = 23;
            // 
            // EndDateDateEdit
            // 
            this.EndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "EndDate", true));
            this.EndDateDateEdit.EditValue = null;
            this.EndDateDateEdit.Location = new System.Drawing.Point(167, 309);
            this.EndDateDateEdit.MenuManager = this.barManager1;
            this.EndDateDateEdit.Name = "EndDateDateEdit";
            this.EndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EndDateDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.EndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Size = new System.Drawing.Size(182, 20);
            this.EndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EndDateDateEdit.TabIndex = 22;
            // 
            // StartDateDateEdit
            // 
            this.StartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "StartDate", true));
            this.StartDateDateEdit.EditValue = null;
            this.StartDateDateEdit.Location = new System.Drawing.Point(167, 285);
            this.StartDateDateEdit.MenuManager = this.barManager1;
            this.StartDateDateEdit.Name = "StartDateDateEdit";
            this.StartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StartDateDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.StartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Size = new System.Drawing.Size(182, 20);
            this.StartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.StartDateDateEdit.TabIndex = 21;
            // 
            // ContractManagerIDGridLookUpEdit
            // 
            this.ContractManagerIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "ContractManagerID", true));
            this.ContractManagerIDGridLookUpEdit.Location = new System.Drawing.Point(143, 62);
            this.ContractManagerIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContractManagerIDGridLookUpEdit.Name = "ContractManagerIDGridLookUpEdit";
            this.ContractManagerIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractManagerIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.ContractManagerIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.ContractManagerIDGridLookUpEdit.Properties.NullText = "";
            this.ContractManagerIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.ContractManagerIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.ContractManagerIDGridLookUpEdit.Properties.View = this.gridView1;
            this.ContractManagerIDGridLookUpEdit.Size = new System.Drawing.Size(456, 20);
            this.ContractManagerIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContractManagerIDGridLookUpEdit.TabIndex = 19;
            // 
            // sp00226StaffListWithBlankBindingSource
            // 
            this.sp00226StaffListWithBlankBindingSource.DataMember = "sp00226_Staff_List_With_Blank";
            this.sp00226StaffListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.gridColumn57;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn53, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Active";
            this.gridColumn52.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn52.FieldName = "Active";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 1;
            this.gridColumn52.Width = 51;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Surname: Forename";
            this.gridColumn53.FieldName = "DisplayName";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 0;
            this.gridColumn53.Width = 225;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Email";
            this.gridColumn54.FieldName = "Email";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 192;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Forename";
            this.gridColumn55.FieldName = "Forename";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Width = 178;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Network ID";
            this.gridColumn56.FieldName = "NetworkID";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Width = 180;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Staff Name";
            this.gridColumn58.FieldName = "StaffName";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Width = 231;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Surname";
            this.gridColumn59.FieldName = "Surname";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Width = 191;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "User Type";
            this.gridColumn60.FieldName = "UserType";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Width = 223;
            // 
            // ActiveCheckEdit
            // 
            this.ActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "Active", true));
            this.ActiveCheckEdit.Location = new System.Drawing.Point(143, 86);
            this.ActiveCheckEdit.MenuManager = this.barManager1;
            this.ActiveCheckEdit.Name = "ActiveCheckEdit";
            this.ActiveCheckEdit.Properties.Caption = "(Tick if Yes, Only one per Site)";
            this.ActiveCheckEdit.Properties.ValueChecked = 1;
            this.ActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveCheckEdit.Size = new System.Drawing.Size(166, 19);
            this.ActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveCheckEdit.TabIndex = 18;
            // 
            // SiteIDGridLookUpEdit
            // 
            this.SiteIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SiteID", true));
            this.SiteIDGridLookUpEdit.Location = new System.Drawing.Point(143, 36);
            this.SiteIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SiteIDGridLookUpEdit.Name = "SiteIDGridLookUpEdit";
            this.SiteIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Edit Underlying Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Reload Underlying Data", "reload", null, true)});
            this.SiteIDGridLookUpEdit.Properties.DataSource = this.sp04139GCSitesWithBlankJustSnowClearSitesBindingSource;
            this.SiteIDGridLookUpEdit.Properties.DisplayMember = "SiteName";
            this.SiteIDGridLookUpEdit.Properties.NullText = "";
            this.SiteIDGridLookUpEdit.Properties.ValueMember = "SiteID";
            this.SiteIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.SiteIDGridLookUpEdit.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteIDGridLookUpEdit_Properties_ButtonClick);
            this.SiteIDGridLookUpEdit.Size = new System.Drawing.Size(456, 22);
            this.SiteIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDGridLookUpEdit.TabIndex = 16;
            this.SiteIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteIDGridLookUpEdit_Validating);
            // 
            // sp04139GCSitesWithBlankJustSnowClearSitesBindingSource
            // 
            this.sp04139GCSitesWithBlankJustSnowClearSitesBindingSource.DataMember = "sp04139_GC_Sites_With_Blank_Just_Snow_Clear_Sites";
            this.sp04139GCSitesWithBlankJustSnowClearSitesBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientCode,
            this.colClientID,
            this.colClientName,
            this.colClientSiteName,
            this.colContactPerson,
            this.colSiteAddressLine1,
            this.colSiteCode,
            this.colSiteID,
            this.colSiteName,
            this.colSiteTypeDescription,
            this.colSiteTypeID,
            this.colXCoordinate,
            this.colYCoordinate});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.colSiteID;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 181;
            // 
            // colClientSiteName
            // 
            this.colClientSiteName.Caption = "Client \\ Site Name";
            this.colClientSiteName.FieldName = "ClientSiteName";
            this.colClientSiteName.Name = "colClientSiteName";
            this.colClientSiteName.OptionsColumn.AllowEdit = false;
            this.colClientSiteName.OptionsColumn.AllowFocus = false;
            this.colClientSiteName.OptionsColumn.ReadOnly = true;
            this.colClientSiteName.Width = 242;
            // 
            // colContactPerson
            // 
            this.colContactPerson.Caption = "Contact Person";
            this.colContactPerson.FieldName = "ContactPerson";
            this.colContactPerson.Name = "colContactPerson";
            this.colContactPerson.OptionsColumn.AllowEdit = false;
            this.colContactPerson.OptionsColumn.AllowFocus = false;
            this.colContactPerson.OptionsColumn.ReadOnly = true;
            this.colContactPerson.Visible = true;
            this.colContactPerson.VisibleIndex = 3;
            this.colContactPerson.Width = 168;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 4;
            this.colSiteAddressLine1.Width = 141;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 1;
            this.colSiteCode.Width = 96;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 251;
            // 
            // colSiteTypeDescription
            // 
            this.colSiteTypeDescription.Caption = "Site Type";
            this.colSiteTypeDescription.FieldName = "SiteTypeDescription";
            this.colSiteTypeDescription.Name = "colSiteTypeDescription";
            this.colSiteTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSiteTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSiteTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSiteTypeDescription.Visible = true;
            this.colSiteTypeDescription.VisibleIndex = 2;
            this.colSiteTypeDescription.Width = 99;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04140GCSnowClearanceSiteContractEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(144, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(174, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // SnowClearanceSiteContractIDSpinEdit
            // 
            this.SnowClearanceSiteContractIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SnowClearanceSiteContractID", true));
            this.SnowClearanceSiteContractIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SnowClearanceSiteContractIDSpinEdit.Location = new System.Drawing.Point(156, 36);
            this.SnowClearanceSiteContractIDSpinEdit.MenuManager = this.barManager1;
            this.SnowClearanceSiteContractIDSpinEdit.Name = "SnowClearanceSiteContractIDSpinEdit";
            this.SnowClearanceSiteContractIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SnowClearanceSiteContractIDSpinEdit.Properties.ReadOnly = true;
            this.SnowClearanceSiteContractIDSpinEdit.Size = new System.Drawing.Size(443, 20);
            this.SnowClearanceSiteContractIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearanceSiteContractIDSpinEdit.TabIndex = 4;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 261);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(539, 532);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling2);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // ChargeMethodIDGridLookUpEdit
            // 
            this.ChargeMethodIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "ChargeMethodID", true));
            this.ChargeMethodIDGridLookUpEdit.Location = new System.Drawing.Point(179, 424);
            this.ChargeMethodIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ChargeMethodIDGridLookUpEdit.Name = "ChargeMethodIDGridLookUpEdit";
            this.ChargeMethodIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ChargeMethodIDGridLookUpEdit.Properties.DataSource = this.sp04142GCSnowClearanceChargeMethodsWithBlankBindingSource;
            this.ChargeMethodIDGridLookUpEdit.Properties.DisplayMember = "ChargeMethodDescription";
            this.ChargeMethodIDGridLookUpEdit.Properties.MaxLength = 50;
            this.ChargeMethodIDGridLookUpEdit.Properties.NullText = "";
            this.ChargeMethodIDGridLookUpEdit.Properties.ValueMember = "ChargeMethodID";
            this.ChargeMethodIDGridLookUpEdit.Properties.View = this.gridView3;
            this.ChargeMethodIDGridLookUpEdit.Size = new System.Drawing.Size(384, 20);
            this.ChargeMethodIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ChargeMethodIDGridLookUpEdit.TabIndex = 26;
            this.ChargeMethodIDGridLookUpEdit.Validated += new System.EventHandler(this.ChargeMethodIDGridLookUpEdit_Validated);
            // 
            // sp04142GCSnowClearanceChargeMethodsWithBlankBindingSource
            // 
            this.sp04142GCSnowClearanceChargeMethodsWithBlankBindingSource.DataMember = "sp04142_GC_Snow_Clearance_Charge_Methods_With_Blank";
            this.sp04142GCSnowClearanceChargeMethodsWithBlankBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colChargeMethodID,
            this.colChargeMethodDescription,
            this.colChargeMethodOrder});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.colChargeMethodID;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsFilter.AllowMRUFilterList = false;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colChargeMethodOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colChargeMethodDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colChargeMethodDescription
            // 
            this.colChargeMethodDescription.Caption = "Charge Method Description";
            this.colChargeMethodDescription.FieldName = "ChargeMethodDescription";
            this.colChargeMethodDescription.Name = "colChargeMethodDescription";
            this.colChargeMethodDescription.OptionsColumn.AllowEdit = false;
            this.colChargeMethodDescription.OptionsColumn.AllowFocus = false;
            this.colChargeMethodDescription.OptionsColumn.ReadOnly = true;
            this.colChargeMethodDescription.Visible = true;
            this.colChargeMethodDescription.VisibleIndex = 0;
            this.colChargeMethodDescription.Width = 190;
            // 
            // colChargeMethodOrder
            // 
            this.colChargeMethodOrder.Caption = "Charge Method Order";
            this.colChargeMethodOrder.FieldName = "ChargeMethodOrder";
            this.colChargeMethodOrder.Name = "colChargeMethodOrder";
            this.colChargeMethodOrder.OptionsColumn.AllowEdit = false;
            this.colChargeMethodOrder.OptionsColumn.AllowFocus = false;
            this.colChargeMethodOrder.OptionsColumn.ReadOnly = true;
            this.colChargeMethodOrder.Width = 134;
            // 
            // ItemForSnowClearanceSiteContractID
            // 
            this.ItemForSnowClearanceSiteContractID.Control = this.SnowClearanceSiteContractIDSpinEdit;
            this.ItemForSnowClearanceSiteContractID.CustomizationFormText = "Snow Clearance Contract ID:";
            this.ItemForSnowClearanceSiteContractID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSnowClearanceSiteContractID.Name = "ItemForSnowClearanceSiteContractID";
            this.ItemForSnowClearanceSiteContractID.Size = new System.Drawing.Size(591, 24);
            this.ItemForSnowClearanceSiteContractID.Text = "Snow Clearance Contract ID:";
            this.ItemForSnowClearanceSiteContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(611, 839);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroup4,
            this.emptySpaceItem2,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.ItemForSiteID,
            this.ItemForActive,
            this.emptySpaceItem3,
            this.ItemForContractManagerID,
            this.ItemForProactive,
            this.emptySpaceItem10,
            this.emptySpaceItem8,
            this.ItemForReactive,
            this.ItemForMinimumPictureCount});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(591, 819);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 809);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(591, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 179);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(591, 630);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(567, 584);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.emptySpaceItem4,
            this.emptySpaceItem6,
            this.layoutControlGroup7,
            this.ItemForAccessRestrictions,
            this.emptySpaceItem7,
            this.ItemForSeasonPeriodID,
            this.emptySpaceItem9,
            this.ItemForStartDate,
            this.ItemForEndDate,
            this.ItemForArea});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(543, 536);
            this.layoutControlGroup5.Text = "Details";
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.StartDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForStartDate.MaxSize = new System.Drawing.Size(0, 24);
            this.ItemForStartDate.MinSize = new System.Drawing.Size(182, 24);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(317, 24);
            this.ItemForStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStartDate.Text = "Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.EndDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(317, 24);
            this.ItemForEndDate.Text = "End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForArea
            // 
            this.ItemForArea.Control = this.AreaSpinEdit;
            this.ItemForArea.CustomizationFormText = "Area:";
            this.ItemForArea.Location = new System.Drawing.Point(0, 72);
            this.ItemForArea.Name = "ItemForArea";
            this.ItemForArea.Size = new System.Drawing.Size(317, 24);
            this.ItemForArea.Text = "Area:";
            this.ItemForArea.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Gritting Pattern";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSnowClearOnMonday,
            this.ItemForSnowClearOnTuesday,
            this.ItemForSnowClearOnWednesday,
            this.ItemForSnowClearOnThursday,
            this.ItemForSnowClearOnFriday,
            this.ItemForSnowClearOnSaturday,
            this.ItemForSnowClearOnSunday,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.emptySpaceItem15,
            this.emptySpaceItem16,
            this.emptySpaceItem17});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 329);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(543, 207);
            this.layoutControlGroup6.Text = "Snow Clearance Pattern";
            // 
            // ItemForSnowClearOnMonday
            // 
            this.ItemForSnowClearOnMonday.Control = this.SnowClearOnMondayCheckEdit;
            this.ItemForSnowClearOnMonday.CustomizationFormText = "Monday Snow Clear:";
            this.ItemForSnowClearOnMonday.Location = new System.Drawing.Point(0, 0);
            this.ItemForSnowClearOnMonday.MaxSize = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearOnMonday.MinSize = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearOnMonday.Name = "ItemForSnowClearOnMonday";
            this.ItemForSnowClearOnMonday.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearOnMonday.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSnowClearOnMonday.Text = "Monday Snow Clear:";
            this.ItemForSnowClearOnMonday.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForSnowClearOnTuesday
            // 
            this.ItemForSnowClearOnTuesday.Control = this.SnowClearOnTuesdayCheckEdit;
            this.ItemForSnowClearOnTuesday.CustomizationFormText = "Tuesday Snow Clear:";
            this.ItemForSnowClearOnTuesday.Location = new System.Drawing.Point(0, 23);
            this.ItemForSnowClearOnTuesday.Name = "ItemForSnowClearOnTuesday";
            this.ItemForSnowClearOnTuesday.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearOnTuesday.Text = "Tuesday Snow Clear:";
            this.ItemForSnowClearOnTuesday.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForSnowClearOnWednesday
            // 
            this.ItemForSnowClearOnWednesday.Control = this.SnowClearOnWednesdayCheckEdit;
            this.ItemForSnowClearOnWednesday.CustomizationFormText = "Wednesday Snow Clear:";
            this.ItemForSnowClearOnWednesday.Location = new System.Drawing.Point(0, 46);
            this.ItemForSnowClearOnWednesday.Name = "ItemForSnowClearOnWednesday";
            this.ItemForSnowClearOnWednesday.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearOnWednesday.Text = "Wednesday Snow Clear:";
            this.ItemForSnowClearOnWednesday.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForSnowClearOnThursday
            // 
            this.ItemForSnowClearOnThursday.Control = this.SnowClearOnThursdayCheckEdit;
            this.ItemForSnowClearOnThursday.CustomizationFormText = "Thursday Snow Clear:";
            this.ItemForSnowClearOnThursday.Location = new System.Drawing.Point(0, 69);
            this.ItemForSnowClearOnThursday.Name = "ItemForSnowClearOnThursday";
            this.ItemForSnowClearOnThursday.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearOnThursday.Text = "Thursday Snow Clear:";
            this.ItemForSnowClearOnThursday.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForSnowClearOnFriday
            // 
            this.ItemForSnowClearOnFriday.Control = this.SnowClearOnFridayCheckEdit;
            this.ItemForSnowClearOnFriday.CustomizationFormText = "Friday Snow Clear:";
            this.ItemForSnowClearOnFriday.Location = new System.Drawing.Point(0, 92);
            this.ItemForSnowClearOnFriday.Name = "ItemForSnowClearOnFriday";
            this.ItemForSnowClearOnFriday.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearOnFriday.Text = "Friday Snow Clear:";
            this.ItemForSnowClearOnFriday.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForSnowClearOnSaturday
            // 
            this.ItemForSnowClearOnSaturday.Control = this.SnowClearOnSaturdayCheckEdit;
            this.ItemForSnowClearOnSaturday.CustomizationFormText = "Saturday Snow Clear:";
            this.ItemForSnowClearOnSaturday.Location = new System.Drawing.Point(0, 115);
            this.ItemForSnowClearOnSaturday.Name = "ItemForSnowClearOnSaturday";
            this.ItemForSnowClearOnSaturday.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearOnSaturday.Text = "Saturday Snow Clear:";
            this.ItemForSnowClearOnSaturday.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForSnowClearOnSunday
            // 
            this.ItemForSnowClearOnSunday.Control = this.SnowClearOnSundayCheckEdit;
            this.ItemForSnowClearOnSunday.CustomizationFormText = "Sunday Snow Clear:";
            this.ItemForSnowClearOnSunday.Location = new System.Drawing.Point(0, 138);
            this.ItemForSnowClearOnSunday.Name = "ItemForSnowClearOnSunday";
            this.ItemForSnowClearOnSunday.Size = new System.Drawing.Size(219, 23);
            this.ItemForSnowClearOnSunday.Text = "Sunday Snow Clear:";
            this.ItemForSnowClearOnSunday.TextSize = new System.Drawing.Size(128, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(219, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(300, 23);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(219, 23);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(300, 23);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(219, 46);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(300, 23);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(219, 69);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(300, 23);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(219, 92);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(300, 23);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(219, 115);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(300, 23);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(219, 138);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(300, 23);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 319);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(543, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 119);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(543, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Client Charging";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForChargeMethod,
            this.ItemForChargeFixedPrice,
            this.emptySpaceItem18,
            this.ItemForChargeFixedNumberOfHours,
            this.ItemForChargeHourlyFixedRate,
            this.ItemForChargeHourlyExtraRate,
            this.ItemForChargeMarkupPercentage});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 129);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(543, 190);
            this.layoutControlGroup7.Text = "Client Charging";
            // 
            // ItemForChargeMethod
            // 
            this.ItemForChargeMethod.Control = this.ChargeMethodIDGridLookUpEdit;
            this.ItemForChargeMethod.CustomizationFormText = "Weather Activation Point:";
            this.ItemForChargeMethod.Location = new System.Drawing.Point(0, 0);
            this.ItemForChargeMethod.Name = "ItemForChargeMethod";
            this.ItemForChargeMethod.Size = new System.Drawing.Size(519, 24);
            this.ItemForChargeMethod.Text = "Charge Method:";
            this.ItemForChargeMethod.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForChargeFixedPrice
            // 
            this.ItemForChargeFixedPrice.Control = this.ChargeFixedPriceSpinEdit;
            this.ItemForChargeFixedPrice.CustomizationFormText = "Charge Fixed Price:";
            this.ItemForChargeFixedPrice.Location = new System.Drawing.Point(0, 24);
            this.ItemForChargeFixedPrice.MaxSize = new System.Drawing.Size(259, 24);
            this.ItemForChargeFixedPrice.MinSize = new System.Drawing.Size(259, 24);
            this.ItemForChargeFixedPrice.Name = "ItemForChargeFixedPrice";
            this.ItemForChargeFixedPrice.Size = new System.Drawing.Size(259, 24);
            this.ItemForChargeFixedPrice.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForChargeFixedPrice.Text = "Charge Fixed Price:";
            this.ItemForChargeFixedPrice.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForChargeFixedNumberOfHours
            // 
            this.ItemForChargeFixedNumberOfHours.Control = this.ChargeFixedNumberOfHoursSpinEdit;
            this.ItemForChargeFixedNumberOfHours.CustomizationFormText = "Charge Fixed Hours:";
            this.ItemForChargeFixedNumberOfHours.Location = new System.Drawing.Point(0, 48);
            this.ItemForChargeFixedNumberOfHours.Name = "ItemForChargeFixedNumberOfHours";
            this.ItemForChargeFixedNumberOfHours.Size = new System.Drawing.Size(259, 24);
            this.ItemForChargeFixedNumberOfHours.Text = "Charge Fixed Hours:";
            this.ItemForChargeFixedNumberOfHours.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForChargeHourlyFixedRate
            // 
            this.ItemForChargeHourlyFixedRate.Control = this.ChargeHourlyFixedRateSpinEdit;
            this.ItemForChargeHourlyFixedRate.CustomizationFormText = "Charge Hourly Fixed Rate:";
            this.ItemForChargeHourlyFixedRate.Location = new System.Drawing.Point(0, 72);
            this.ItemForChargeHourlyFixedRate.Name = "ItemForChargeHourlyFixedRate";
            this.ItemForChargeHourlyFixedRate.Size = new System.Drawing.Size(259, 24);
            this.ItemForChargeHourlyFixedRate.Text = "Charge Hourly Fixed Rate:";
            this.ItemForChargeHourlyFixedRate.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForChargeHourlyExtraRate
            // 
            this.ItemForChargeHourlyExtraRate.Control = this.ChargeHourlyExtraRateSpinEdit;
            this.ItemForChargeHourlyExtraRate.CustomizationFormText = "Charge Hourly Extra Rate:";
            this.ItemForChargeHourlyExtraRate.Location = new System.Drawing.Point(0, 96);
            this.ItemForChargeHourlyExtraRate.Name = "ItemForChargeHourlyExtraRate";
            this.ItemForChargeHourlyExtraRate.Size = new System.Drawing.Size(259, 24);
            this.ItemForChargeHourlyExtraRate.Text = "Charge Hourly Extra Rate:";
            this.ItemForChargeHourlyExtraRate.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForChargeMarkupPercentage
            // 
            this.ItemForChargeMarkupPercentage.Control = this.ChargeMarkupPercentageSpinEdit;
            this.ItemForChargeMarkupPercentage.CustomizationFormText = "Charge Markup %:";
            this.ItemForChargeMarkupPercentage.Location = new System.Drawing.Point(0, 120);
            this.ItemForChargeMarkupPercentage.Name = "ItemForChargeMarkupPercentage";
            this.ItemForChargeMarkupPercentage.Size = new System.Drawing.Size(259, 24);
            this.ItemForChargeMarkupPercentage.Text = "Charge Markup %:";
            this.ItemForChargeMarkupPercentage.TextSize = new System.Drawing.Size(128, 13);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(259, 24);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(260, 120);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAccessRestrictions
            // 
            this.ItemForAccessRestrictions.Control = this.AccessRestrictionsCheckEdit;
            this.ItemForAccessRestrictions.Location = new System.Drawing.Point(0, 96);
            this.ItemForAccessRestrictions.MaxSize = new System.Drawing.Size(218, 23);
            this.ItemForAccessRestrictions.MinSize = new System.Drawing.Size(218, 23);
            this.ItemForAccessRestrictions.Name = "ItemForAccessRestrictions";
            this.ItemForAccessRestrictions.Size = new System.Drawing.Size(218, 23);
            this.ItemForAccessRestrictions.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAccessRestrictions.Text = "Access Restrictions:";
            this.ItemForAccessRestrictions.TextSize = new System.Drawing.Size(128, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(218, 96);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(325, 23);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(543, 536);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(543, 536);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(591, 12);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(310, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(281, 24);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(132, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(178, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDGridLookUpEdit;
            this.ItemForSiteID.CustomizationFormText = "Site:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(591, 26);
            this.ItemForSiteID.Text = "Site:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForActive
            // 
            this.ItemForActive.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForActive.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForActive.Control = this.ActiveCheckEdit;
            this.ItemForActive.CustomizationFormText = "Active Contract:";
            this.ItemForActive.Location = new System.Drawing.Point(0, 74);
            this.ItemForActive.MaxSize = new System.Drawing.Size(301, 23);
            this.ItemForActive.MinSize = new System.Drawing.Size(301, 23);
            this.ItemForActive.Name = "ItemForActive";
            this.ItemForActive.Size = new System.Drawing.Size(301, 23);
            this.ItemForActive.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActive.Text = "Active Contract:";
            this.ItemForActive.TextSize = new System.Drawing.Size(128, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(132, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(132, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(132, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForContractManagerID
            // 
            this.ItemForContractManagerID.Control = this.ContractManagerIDGridLookUpEdit;
            this.ItemForContractManagerID.CustomizationFormText = "Contract Manager:";
            this.ItemForContractManagerID.Location = new System.Drawing.Point(0, 50);
            this.ItemForContractManagerID.Name = "ItemForContractManagerID";
            this.ItemForContractManagerID.Size = new System.Drawing.Size(591, 24);
            this.ItemForContractManagerID.Text = "Contract Manager:";
            this.ItemForContractManagerID.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForProactive
            // 
            this.ItemForProactive.Control = this.ProactiveCheckEdit;
            this.ItemForProactive.CustomizationFormText = "Proactive:";
            this.ItemForProactive.Location = new System.Drawing.Point(0, 97);
            this.ItemForProactive.MaxSize = new System.Drawing.Size(216, 23);
            this.ItemForProactive.MinSize = new System.Drawing.Size(216, 23);
            this.ItemForProactive.Name = "ItemForProactive";
            this.ItemForProactive.Size = new System.Drawing.Size(216, 23);
            this.ItemForProactive.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForProactive.Text = "Proactive:";
            this.ItemForProactive.TextSize = new System.Drawing.Size(128, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(301, 74);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(290, 23);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(216, 97);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(375, 70);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Ractive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 120);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(216, 23);
            this.ItemForReactive.Text = "Ractive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(128, 13);
            // 
            // ItemForMinimumPictureCount
            // 
            this.ItemForMinimumPictureCount.Control = this.MinimumPictureCountSpinEdit;
            this.ItemForMinimumPictureCount.Location = new System.Drawing.Point(0, 143);
            this.ItemForMinimumPictureCount.Name = "ItemForMinimumPictureCount";
            this.ItemForMinimumPictureCount.Size = new System.Drawing.Size(216, 24);
            this.ItemForMinimumPictureCount.Text = "Min Pictures from App:";
            this.ItemForMinimumPictureCount.TextSize = new System.Drawing.Size(128, 13);
            // 
            // sp00226_Staff_List_With_BlankTableAdapter
            // 
            this.sp00226_Staff_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04139_GC_Sites_With_Blank_Just_Snow_Clear_SitesTableAdapter
            // 
            this.sp04139_GC_Sites_With_Blank_Just_Snow_Clear_SitesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04140_GC_Snow_Clearance_Site_Contract_EditTableAdapter
            // 
            this.sp04140_GC_Snow_Clearance_Site_Contract_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04142_GC_Snow_Clearance_Charge_Methods_With_BlankTableAdapter
            // 
            this.sp04142_GC_Snow_Clearance_Charge_Methods_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // SeasonPeriodIDGridLookUpEdit
            // 
            this.SeasonPeriodIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04140GCSnowClearanceSiteContractEditBindingSource, "SeasonPeriodID", true));
            this.SeasonPeriodIDGridLookUpEdit.Location = new System.Drawing.Point(167, 261);
            this.SeasonPeriodIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SeasonPeriodIDGridLookUpEdit.Name = "SeasonPeriodIDGridLookUpEdit";
            this.SeasonPeriodIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SeasonPeriodIDGridLookUpEdit.Properties.DataSource = this.sp04373WMSeasonPeriodsWithBlankBindingSource;
            this.SeasonPeriodIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SeasonPeriodIDGridLookUpEdit.Properties.MaxLength = 50;
            this.SeasonPeriodIDGridLookUpEdit.Properties.NullText = "";
            this.SeasonPeriodIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SeasonPeriodIDGridLookUpEdit.Properties.View = this.gridView5;
            this.SeasonPeriodIDGridLookUpEdit.Size = new System.Drawing.Size(182, 20);
            this.SeasonPeriodIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SeasonPeriodIDGridLookUpEdit.TabIndex = 28;
            this.SeasonPeriodIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SeasonPeriodIDGridLookUpEdit_Validating);
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription2,
            this.colRecordOrder});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colID1;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView5.FormatRules.Add(gridFormatRule2);
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsCustomization.AllowFilter = false;
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID1
            // 
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Season Period";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 171;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Width = 87;
            // 
            // ItemForSeasonPeriodID
            // 
            this.ItemForSeasonPeriodID.Control = this.SeasonPeriodIDGridLookUpEdit;
            this.ItemForSeasonPeriodID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSeasonPeriodID.MaxSize = new System.Drawing.Size(317, 24);
            this.ItemForSeasonPeriodID.MinSize = new System.Drawing.Size(317, 24);
            this.ItemForSeasonPeriodID.Name = "ItemForSeasonPeriodID";
            this.ItemForSeasonPeriodID.Size = new System.Drawing.Size(317, 24);
            this.ItemForSeasonPeriodID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSeasonPeriodID.Text = "Season Period:";
            this.ItemForSeasonPeriodID.TextSize = new System.Drawing.Size(128, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(317, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(226, 96);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp04373WMSeasonPeriodsWithBlankBindingSource
            // 
            this.sp04373WMSeasonPeriodsWithBlankBindingSource.DataMember = "sp04373_WM_Season_Periods_With_Blank";
            this.sp04373WMSeasonPeriodsWithBlankBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // sp04373_WM_Season_Periods_With_BlankTableAdapter
            // 
            this.sp04373_WM_Season_Periods_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Snow_Contract_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 532);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Snow_Contract_Edit";
            this.Text = "Edit Site Snow Clearance Contract";
            this.Activated += new System.EventHandler(this.frm_GC_Snow_Contract_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Snow_Contract_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Snow_Contract_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MinimumPictureCountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04140GCSnowClearanceSiteContractEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessRestrictionsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeMarkupPercentageSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeHourlyExtraRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeFixedPriceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnSundayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnSaturdayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnFridayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnThursdayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnWednesdayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnTuesdayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearOnMondayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeHourlyFixedRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeFixedNumberOfHoursSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractManagerIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04139GCSitesWithBlankJustSnowClearSitesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceSiteContractIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargeMethodIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04142GCSnowClearanceChargeMethodsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceSiteContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnMonday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnTuesday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnWednesday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnThursday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnFriday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnSaturday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearOnSunday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeFixedPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeFixedNumberOfHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeHourlyFixedRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeHourlyExtraRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargeMarkupPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessRestrictions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractManagerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumPictureCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeasonPeriodIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSeasonPeriodID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04373WMSeasonPeriodsWithBlankBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SpinEdit SnowClearanceSiteContractIDSpinEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearanceSiteContractID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.GridLookUpEdit SiteIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private DevExpress.XtraEditors.CheckEdit ActiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActive;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.GridLookUpEdit ContractManagerIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractManagerID;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp00226StaffListWithBlankBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter sp00226_Staff_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraEditors.DateEdit EndDateDateEdit;
        private DevExpress.XtraEditors.DateEdit StartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.SpinEdit AreaSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForArea;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChargeMethod;
        private DevExpress.XtraEditors.SpinEdit ChargeHourlyFixedRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ChargeFixedNumberOfHoursSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChargeFixedNumberOfHours;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChargeHourlyFixedRate;
        private DevExpress.XtraEditors.CheckEdit SnowClearOnSundayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SnowClearOnSaturdayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SnowClearOnFridayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SnowClearOnThursdayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SnowClearOnWednesdayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SnowClearOnTuesdayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SnowClearOnMondayCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearOnMonday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearOnTuesday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearOnWednesday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearOnThursday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearOnFriday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearOnSaturday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearOnSunday;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.GridLookUpEdit ChargeMethodIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.SpinEdit ChargeFixedPriceSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChargeFixedPrice;
        private DevExpress.XtraEditors.SpinEdit ChargeMarkupPercentageSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ChargeHourlyExtraRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChargeMarkupPercentage;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChargeHourlyExtraRate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private System.Windows.Forms.BindingSource sp04139GCSitesWithBlankJustSnowClearSitesBindingSource;
        private DataSet_GC_Snow_DataEntry dataSet_GC_Snow_DataEntry;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04139_GC_Sites_With_Blank_Just_Snow_Clear_SitesTableAdapter sp04139_GC_Sites_With_Blank_Just_Snow_Clear_SitesTableAdapter;
        private System.Windows.Forms.BindingSource sp04140GCSnowClearanceSiteContractEditBindingSource;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04140_GC_Snow_Clearance_Site_Contract_EditTableAdapter sp04140_GC_Snow_Clearance_Site_Contract_EditTableAdapter;
        private System.Windows.Forms.BindingSource sp04142GCSnowClearanceChargeMethodsWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colChargeMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colChargeMethodDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colChargeMethodOrder;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04142_GC_Snow_Clearance_Charge_Methods_With_BlankTableAdapter sp04142_GC_Snow_Clearance_Charge_Methods_With_BlankTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraEditors.CheckEdit ProactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProactive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraEditors.CheckEdit AccessRestrictionsCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccessRestrictions;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.SpinEdit MinimumPictureCountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimumPictureCount;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.GridLookUpEdit SeasonPeriodIDGridLookUpEdit;
        private System.Windows.Forms.BindingSource sp04373WMSeasonPeriodsWithBlankBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSeasonPeriodID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DataSet_GC_CoreTableAdapters.sp04373_WM_Season_Periods_With_BlankTableAdapter sp04373_WM_Season_Periods_With_BlankTableAdapter;
    }
}

namespace WoodPlan5
{
    partial class frm_GC_Salt_Transfer_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Salt_Transfer_Edit));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.colItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUnitConversionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ToLocationTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp04216GCSaltTransferEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.ToLocationNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.FromLocationTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FromLocationNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SaltColourIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04218GCSaltColoursListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AmountTransferredConvertedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AmountTransferredDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04209GCSaltUnitConversionListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colConversionValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUnitDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AmountTransferredSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ToLocationTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ToLocationIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FromLocationTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FromLocationIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TransferredByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TransferredByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TransferDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.GritTransferIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.TransferCostTextEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForGritTransferID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransferredByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFromLocationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForToLocationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFromLocationTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForToLocationTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTransferDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransferredByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAmountTransferred = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForFromLocationName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForFromLocationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForToLocationName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForToLocationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAmountTransferredDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAmountTransferredConverted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransferCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSaltColourID = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04216_GC_Salt_Transfer_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04216_GC_Salt_Transfer_EditTableAdapter();
            this.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter();
            this.sp04218_GC_Salt_Colours_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04218_GC_Salt_Colours_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ToLocationTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04216GCSaltTransferEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToLocationNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromLocationTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromLocationNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltColourIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04218GCSaltColoursListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountTransferredConvertedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountTransferredDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04209GCSaltUnitConversionListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountTransferredSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToLocationTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToLocationIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromLocationTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromLocationIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritTransferIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferCostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritTransferID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromLocationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToLocationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromLocationTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToLocationTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountTransferred)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromLocationName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromLocationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToLocationName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToLocationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountTransferredDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountTransferredConverted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltColourID)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(731, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            this.barDockControlBottom.Size = new System.Drawing.Size(731, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(731, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colItemID
            // 
            this.colItemID.Caption = "Item ID";
            this.colItemID.FieldName = "ItemID";
            this.colItemID.Name = "colItemID";
            this.colItemID.OptionsColumn.AllowEdit = false;
            this.colItemID.OptionsColumn.AllowFocus = false;
            this.colItemID.OptionsColumn.ReadOnly = true;
            // 
            // colSaltUnitConversionID
            // 
            this.colSaltUnitConversionID.Caption = "Salt Unit Conversion ID";
            this.colSaltUnitConversionID.FieldName = "SaltUnitConversionID";
            this.colSaltUnitConversionID.Name = "colSaltUnitConversionID";
            this.colSaltUnitConversionID.OptionsColumn.AllowEdit = false;
            this.colSaltUnitConversionID.OptionsColumn.AllowFocus = false;
            this.colSaltUnitConversionID.OptionsColumn.ReadOnly = true;
            this.colSaltUnitConversionID.Width = 132;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(731, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Size = new System.Drawing.Size(731, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(731, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ToLocationTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ToLocationNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.FromLocationTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FromLocationNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SaltColourIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AmountTransferredConvertedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AmountTransferredDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AmountTransferredSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ToLocationTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ToLocationIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FromLocationTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FromLocationIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TransferredByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TransferredByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TransferDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.GritTransferIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.TransferCostTextEdit);
            this.dataLayoutControl1.DataSource = this.sp04216GCSaltTransferEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGritTransferID,
            this.ItemForTransferredByStaffID,
            this.ItemForFromLocationID,
            this.ItemForToLocationID,
            this.ItemForFromLocationTypeID,
            this.ItemForToLocationTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1362, 340, 250, 478);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(731, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ToLocationTypeTextEdit
            // 
            this.ToLocationTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "ToLocationType", true));
            this.ToLocationTypeTextEdit.Location = new System.Drawing.Point(587, 118);
            this.ToLocationTypeTextEdit.MenuManager = this.barManager1;
            this.ToLocationTypeTextEdit.Name = "ToLocationTypeTextEdit";
            this.ToLocationTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ToLocationTypeTextEdit, true);
            this.ToLocationTypeTextEdit.Size = new System.Drawing.Size(132, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ToLocationTypeTextEdit, optionsSpelling1);
            this.ToLocationTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.ToLocationTypeTextEdit.TabIndex = 48;
            // 
            // sp04216GCSaltTransferEditBindingSource
            // 
            this.sp04216GCSaltTransferEditBindingSource.DataMember = "sp04216_GC_Salt_Transfer_Edit";
            this.sp04216GCSaltTransferEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ToLocationNameButtonEdit
            // 
            this.ToLocationNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "ToLocationName", true));
            this.ToLocationNameButtonEdit.Location = new System.Drawing.Point(130, 118);
            this.ToLocationNameButtonEdit.MenuManager = this.barManager1;
            this.ToLocationNameButtonEdit.Name = "ToLocationNameButtonEdit";
            this.ToLocationNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "View", null, true)});
            this.ToLocationNameButtonEdit.Size = new System.Drawing.Size(335, 20);
            this.ToLocationNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ToLocationNameButtonEdit.TabIndex = 47;
            this.ToLocationNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ToLocationNameButtonEdit_ButtonClick);
            this.ToLocationNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ToLocationNameButtonEdit_Validating);
            // 
            // FromLocationTypeTextEdit
            // 
            this.FromLocationTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "FromLocationType", true));
            this.FromLocationTypeTextEdit.Location = new System.Drawing.Point(587, 94);
            this.FromLocationTypeTextEdit.MenuManager = this.barManager1;
            this.FromLocationTypeTextEdit.Name = "FromLocationTypeTextEdit";
            this.FromLocationTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FromLocationTypeTextEdit, true);
            this.FromLocationTypeTextEdit.Size = new System.Drawing.Size(132, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FromLocationTypeTextEdit, optionsSpelling2);
            this.FromLocationTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.FromLocationTypeTextEdit.TabIndex = 46;
            // 
            // FromLocationNameButtonEdit
            // 
            this.FromLocationNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "FromLocationName", true));
            this.FromLocationNameButtonEdit.Location = new System.Drawing.Point(130, 94);
            this.FromLocationNameButtonEdit.MenuManager = this.barManager1;
            this.FromLocationNameButtonEdit.Name = "FromLocationNameButtonEdit";
            this.FromLocationNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "view", null, true)});
            this.FromLocationNameButtonEdit.Size = new System.Drawing.Size(335, 20);
            this.FromLocationNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.FromLocationNameButtonEdit.TabIndex = 45;
            this.FromLocationNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.FromLocationNameButtonEdit_ButtonClick);
            this.FromLocationNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.FromLocationNameButtonEdit_Validating);
            // 
            // SaltColourIDGridLookUpEdit
            // 
            this.SaltColourIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "SaltColourID", true));
            this.SaltColourIDGridLookUpEdit.Location = new System.Drawing.Point(130, 248);
            this.SaltColourIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SaltColourIDGridLookUpEdit.Name = "SaltColourIDGridLookUpEdit";
            this.SaltColourIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Edit Picklist Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Reload Picklist Data", "reload", null, true)});
            this.SaltColourIDGridLookUpEdit.Properties.DataSource = this.sp04218GCSaltColoursListWithBlankBindingSource;
            this.SaltColourIDGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.SaltColourIDGridLookUpEdit.Properties.NullText = "";
            this.SaltColourIDGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.SaltColourIDGridLookUpEdit.Properties.View = this.gridView1;
            this.SaltColourIDGridLookUpEdit.Size = new System.Drawing.Size(233, 22);
            this.SaltColourIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SaltColourIDGridLookUpEdit.TabIndex = 44;
            this.SaltColourIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SaltColourIDGridLookUpEdit_ButtonClick);
            // 
            // sp04218GCSaltColoursListWithBlankBindingSource
            // 
            this.sp04218GCSaltColoursListWithBlankBindingSource.DataMember = "sp04218_GC_Salt_Colours_List_With_Blank";
            this.sp04218GCSaltColoursListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemDescription,
            this.colItemID,
            this.colOrder,
            this.colItemCode});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colItemID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Description";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 303;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.Width = 73;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Code";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.OptionsColumn.AllowFocus = false;
            this.colItemCode.OptionsColumn.ReadOnly = true;
            // 
            // AmountTransferredConvertedSpinEdit
            // 
            this.AmountTransferredConvertedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "AmountTransferredConverted", true));
            this.AmountTransferredConvertedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AmountTransferredConvertedSpinEdit.Location = new System.Drawing.Point(130, 200);
            this.AmountTransferredConvertedSpinEdit.MenuManager = this.barManager1;
            this.AmountTransferredConvertedSpinEdit.Name = "AmountTransferredConvertedSpinEdit";
            this.AmountTransferredConvertedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.AmountTransferredConvertedSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.AmountTransferredConvertedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AmountTransferredConvertedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.AmountTransferredConvertedSpinEdit.Properties.ReadOnly = true;
            this.AmountTransferredConvertedSpinEdit.Size = new System.Drawing.Size(233, 20);
            this.AmountTransferredConvertedSpinEdit.StyleController = this.dataLayoutControl1;
            this.AmountTransferredConvertedSpinEdit.TabIndex = 43;
            this.AmountTransferredConvertedSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AmountTransferredConvertedSpinEdit_Validating);
            // 
            // AmountTransferredDescriptorIDGridLookUpEdit
            // 
            this.AmountTransferredDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "AmountTransferredDescriptorID", true));
            this.AmountTransferredDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(130, 176);
            this.AmountTransferredDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.AmountTransferredDescriptorIDGridLookUpEdit.Name = "AmountTransferredDescriptorIDGridLookUpEdit";
            this.AmountTransferredDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AmountTransferredDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp04209GCSaltUnitConversionListWithBlankBindingSource;
            this.AmountTransferredDescriptorIDGridLookUpEdit.Properties.DisplayMember = "SaltUnitDescription";
            this.AmountTransferredDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.AmountTransferredDescriptorIDGridLookUpEdit.Properties.ValueMember = "SaltUnitConversionID";
            this.AmountTransferredDescriptorIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.AmountTransferredDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(233, 20);
            this.AmountTransferredDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AmountTransferredDescriptorIDGridLookUpEdit.TabIndex = 42;
            this.AmountTransferredDescriptorIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.AmountTransferredDescriptorIDGridLookUpEdit_EditValueChanged);
            this.AmountTransferredDescriptorIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AmountTransferredDescriptorIDGridLookUpEdit_Validating);
            this.AmountTransferredDescriptorIDGridLookUpEdit.Validated += new System.EventHandler(this.AmountTransferredDescriptorIDGridLookUpEdit_Validated);
            // 
            // sp04209GCSaltUnitConversionListWithBlankBindingSource
            // 
            this.sp04209GCSaltUnitConversionListWithBlankBindingSource.DataMember = "sp04209_GC_Salt_Unit_Conversion_List_With_Blank";
            this.sp04209GCSaltUnitConversionListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colConversionValue,
            this.colSaltUnitConversionID,
            this.colSaltUnitDescription});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colSaltUnitConversionID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSaltUnitDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colConversionValue
            // 
            this.colConversionValue.Caption = "Conversion Value";
            this.colConversionValue.FieldName = "ConversionValue";
            this.colConversionValue.Name = "colConversionValue";
            this.colConversionValue.OptionsColumn.AllowEdit = false;
            this.colConversionValue.OptionsColumn.AllowFocus = false;
            this.colConversionValue.OptionsColumn.ReadOnly = true;
            this.colConversionValue.Visible = true;
            this.colConversionValue.VisibleIndex = 1;
            this.colConversionValue.Width = 107;
            // 
            // colSaltUnitDescription
            // 
            this.colSaltUnitDescription.Caption = "Salt Unit Description";
            this.colSaltUnitDescription.FieldName = "SaltUnitDescription";
            this.colSaltUnitDescription.Name = "colSaltUnitDescription";
            this.colSaltUnitDescription.OptionsColumn.AllowEdit = false;
            this.colSaltUnitDescription.OptionsColumn.AllowFocus = false;
            this.colSaltUnitDescription.OptionsColumn.ReadOnly = true;
            this.colSaltUnitDescription.Visible = true;
            this.colSaltUnitDescription.VisibleIndex = 0;
            this.colSaltUnitDescription.Width = 166;
            // 
            // AmountTransferredSpinEdit
            // 
            this.AmountTransferredSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "AmountTransferred", true));
            this.AmountTransferredSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AmountTransferredSpinEdit.Location = new System.Drawing.Point(130, 152);
            this.AmountTransferredSpinEdit.MenuManager = this.barManager1;
            this.AmountTransferredSpinEdit.Name = "AmountTransferredSpinEdit";
            this.AmountTransferredSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.AmountTransferredSpinEdit.Properties.Mask.EditMask = "n2";
            this.AmountTransferredSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AmountTransferredSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.AmountTransferredSpinEdit.Size = new System.Drawing.Size(233, 20);
            this.AmountTransferredSpinEdit.StyleController = this.dataLayoutControl1;
            this.AmountTransferredSpinEdit.TabIndex = 41;
            this.AmountTransferredSpinEdit.EditValueChanged += new System.EventHandler(this.AmountTransferredSpinEdit_EditValueChanged);
            this.AmountTransferredSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AmountTransferredSpinEdit_Validating);
            this.AmountTransferredSpinEdit.Validated += new System.EventHandler(this.AmountTransferredSpinEdit_Validated);
            // 
            // ToLocationTypeIDTextEdit
            // 
            this.ToLocationTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "ToLocationTypeID", true));
            this.ToLocationTypeIDTextEdit.Location = new System.Drawing.Point(128, 1);
            this.ToLocationTypeIDTextEdit.MenuManager = this.barManager1;
            this.ToLocationTypeIDTextEdit.Name = "ToLocationTypeIDTextEdit";
            this.ToLocationTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ToLocationTypeIDTextEdit, true);
            this.ToLocationTypeIDTextEdit.Size = new System.Drawing.Size(574, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ToLocationTypeIDTextEdit, optionsSpelling3);
            this.ToLocationTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ToLocationTypeIDTextEdit.TabIndex = 40;
            // 
            // ToLocationIDTextEdit
            // 
            this.ToLocationIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "ToLocationID", true));
            this.ToLocationIDTextEdit.Location = new System.Drawing.Point(128, -16);
            this.ToLocationIDTextEdit.MenuManager = this.barManager1;
            this.ToLocationIDTextEdit.Name = "ToLocationIDTextEdit";
            this.ToLocationIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ToLocationIDTextEdit, true);
            this.ToLocationIDTextEdit.Size = new System.Drawing.Size(574, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ToLocationIDTextEdit, optionsSpelling4);
            this.ToLocationIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ToLocationIDTextEdit.TabIndex = 39;
            // 
            // FromLocationTypeIDTextEdit
            // 
            this.FromLocationTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "FromLocationTypeID", true));
            this.FromLocationTypeIDTextEdit.Location = new System.Drawing.Point(128, 0);
            this.FromLocationTypeIDTextEdit.MenuManager = this.barManager1;
            this.FromLocationTypeIDTextEdit.Name = "FromLocationTypeIDTextEdit";
            this.FromLocationTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FromLocationTypeIDTextEdit, true);
            this.FromLocationTypeIDTextEdit.Size = new System.Drawing.Size(574, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FromLocationTypeIDTextEdit, optionsSpelling5);
            this.FromLocationTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.FromLocationTypeIDTextEdit.TabIndex = 38;
            // 
            // FromLocationIDTextEdit
            // 
            this.FromLocationIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "FromLocationID", true));
            this.FromLocationIDTextEdit.Location = new System.Drawing.Point(128, -20);
            this.FromLocationIDTextEdit.MenuManager = this.barManager1;
            this.FromLocationIDTextEdit.Name = "FromLocationIDTextEdit";
            this.FromLocationIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FromLocationIDTextEdit, true);
            this.FromLocationIDTextEdit.Size = new System.Drawing.Size(574, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FromLocationIDTextEdit, optionsSpelling6);
            this.FromLocationIDTextEdit.StyleController = this.dataLayoutControl1;
            this.FromLocationIDTextEdit.TabIndex = 37;
            // 
            // TransferredByStaffIDTextEdit
            // 
            this.TransferredByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "TransferredByStaffID", true));
            this.TransferredByStaffIDTextEdit.Location = new System.Drawing.Point(128, 84);
            this.TransferredByStaffIDTextEdit.MenuManager = this.barManager1;
            this.TransferredByStaffIDTextEdit.Name = "TransferredByStaffIDTextEdit";
            this.TransferredByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TransferredByStaffIDTextEdit, true);
            this.TransferredByStaffIDTextEdit.Size = new System.Drawing.Size(574, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TransferredByStaffIDTextEdit, optionsSpelling7);
            this.TransferredByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TransferredByStaffIDTextEdit.TabIndex = 36;
            // 
            // TransferredByStaffNameTextEdit
            // 
            this.TransferredByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "TransferredByStaffName", true));
            this.TransferredByStaffNameTextEdit.Location = new System.Drawing.Point(130, 60);
            this.TransferredByStaffNameTextEdit.MenuManager = this.barManager1;
            this.TransferredByStaffNameTextEdit.Name = "TransferredByStaffNameTextEdit";
            this.TransferredByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TransferredByStaffNameTextEdit, true);
            this.TransferredByStaffNameTextEdit.Size = new System.Drawing.Size(589, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TransferredByStaffNameTextEdit, optionsSpelling8);
            this.TransferredByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.TransferredByStaffNameTextEdit.TabIndex = 35;
            // 
            // TransferDateDateEdit
            // 
            this.TransferDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "TransferDate", true));
            this.TransferDateDateEdit.EditValue = null;
            this.TransferDateDateEdit.Location = new System.Drawing.Point(130, 36);
            this.TransferDateDateEdit.MenuManager = this.barManager1;
            this.TransferDateDateEdit.Name = "TransferDateDateEdit";
            this.TransferDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TransferDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TransferDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TransferDateDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.TransferDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TransferDateDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.TransferDateDateEdit.Properties.MinValue = new System.DateTime(2011, 1, 1, 0, 0, 0, 0);
            this.TransferDateDateEdit.Size = new System.Drawing.Size(589, 20);
            this.TransferDateDateEdit.StyleController = this.dataLayoutControl1;
            this.TransferDateDateEdit.TabIndex = 34;
            this.TransferDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TransferDateDateEdit_Validating);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04216GCSaltTransferEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(131, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 9;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // GritTransferIDTextEdit
            // 
            this.GritTransferIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "GritTransferID", true));
            this.GritTransferIDTextEdit.Location = new System.Drawing.Point(128, 36);
            this.GritTransferIDTextEdit.MenuManager = this.barManager1;
            this.GritTransferIDTextEdit.Name = "GritTransferIDTextEdit";
            this.GritTransferIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GritTransferIDTextEdit, true);
            this.GritTransferIDTextEdit.Size = new System.Drawing.Size(574, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GritTransferIDTextEdit, optionsSpelling9);
            this.GritTransferIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GritTransferIDTextEdit.TabIndex = 4;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 354);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(659, 81);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling10);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 8;
            // 
            // TransferCostTextEdit
            // 
            this.TransferCostTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04216GCSaltTransferEditBindingSource, "TransferCost", true));
            this.TransferCostTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TransferCostTextEdit.Location = new System.Drawing.Point(130, 224);
            this.TransferCostTextEdit.MenuManager = this.barManager1;
            this.TransferCostTextEdit.Name = "TransferCostTextEdit";
            this.TransferCostTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TransferCostTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.TransferCostTextEdit.Properties.Mask.EditMask = "c";
            this.TransferCostTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TransferCostTextEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TransferCostTextEdit.Size = new System.Drawing.Size(233, 20);
            this.TransferCostTextEdit.StyleController = this.dataLayoutControl1;
            this.TransferCostTextEdit.TabIndex = 33;
            // 
            // ItemForGritTransferID
            // 
            this.ItemForGritTransferID.Control = this.GritTransferIDTextEdit;
            this.ItemForGritTransferID.CustomizationFormText = "Salt Transfer ID:";
            this.ItemForGritTransferID.Location = new System.Drawing.Point(0, 24);
            this.ItemForGritTransferID.Name = "ItemForGritTransferID";
            this.ItemForGritTransferID.Size = new System.Drawing.Size(694, 24);
            this.ItemForGritTransferID.Text = "Salt Transfer ID:";
            this.ItemForGritTransferID.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForTransferredByStaffID
            // 
            this.ItemForTransferredByStaffID.Control = this.TransferredByStaffIDTextEdit;
            this.ItemForTransferredByStaffID.CustomizationFormText = "Transferred By ID:";
            this.ItemForTransferredByStaffID.Location = new System.Drawing.Point(0, 72);
            this.ItemForTransferredByStaffID.Name = "ItemForTransferredByStaffID";
            this.ItemForTransferredByStaffID.Size = new System.Drawing.Size(694, 24);
            this.ItemForTransferredByStaffID.Text = "Transferred By ID:";
            this.ItemForTransferredByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFromLocationID
            // 
            this.ItemForFromLocationID.Control = this.FromLocationIDTextEdit;
            this.ItemForFromLocationID.CustomizationFormText = "From Location ID:";
            this.ItemForFromLocationID.Location = new System.Drawing.Point(0, 48);
            this.ItemForFromLocationID.Name = "ItemForFromLocationID";
            this.ItemForFromLocationID.Size = new System.Drawing.Size(694, 24);
            this.ItemForFromLocationID.Text = "From Location ID:";
            this.ItemForFromLocationID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForToLocationID
            // 
            this.ItemForToLocationID.Control = this.ToLocationIDTextEdit;
            this.ItemForToLocationID.CustomizationFormText = "To Location ID:";
            this.ItemForToLocationID.Location = new System.Drawing.Point(0, 72);
            this.ItemForToLocationID.Name = "ItemForToLocationID";
            this.ItemForToLocationID.Size = new System.Drawing.Size(694, 24);
            this.ItemForToLocationID.Text = "To Location ID:";
            this.ItemForToLocationID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFromLocationTypeID
            // 
            this.ItemForFromLocationTypeID.Control = this.FromLocationTypeIDTextEdit;
            this.ItemForFromLocationTypeID.CustomizationFormText = "From Location Type ID:";
            this.ItemForFromLocationTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForFromLocationTypeID.Name = "ItemForFromLocationTypeID";
            this.ItemForFromLocationTypeID.Size = new System.Drawing.Size(694, 24);
            this.ItemForFromLocationTypeID.Text = "From Location Type ID:";
            this.ItemForFromLocationTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForToLocationTypeID
            // 
            this.ItemForToLocationTypeID.Control = this.ToLocationTypeIDTextEdit;
            this.ItemForToLocationTypeID.CustomizationFormText = "To Location Type ID:";
            this.ItemForToLocationTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForToLocationTypeID.Name = "ItemForToLocationTypeID";
            this.ItemForToLocationTypeID.Size = new System.Drawing.Size(694, 24);
            this.ItemForToLocationTypeID.Text = "To Location Type ID:";
            this.ItemForToLocationTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 481);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem1,
            this.emptySpaceItem4,
            this.ItemForTransferDate,
            this.ItemForTransferredByStaffName,
            this.ItemForAmountTransferred,
            this.emptySpaceItem5,
            this.ItemForFromLocationName,
            this.emptySpaceItem6,
            this.ItemForFromLocationType,
            this.ItemForToLocationName,
            this.ItemForToLocationType,
            this.emptySpaceItem2,
            this.emptySpaceItem7,
            this.ItemForAmountTransferredDescriptorID,
            this.ItemForAmountTransferredConverted,
            this.ItemForTransferCost,
            this.ItemForSaltColourID});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(711, 461);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 272);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(711, 179);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(687, 133);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(663, 85);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(663, 85);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(119, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(119, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(119, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(119, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(711, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(319, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(392, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTransferDate
            // 
            this.ItemForTransferDate.Control = this.TransferDateDateEdit;
            this.ItemForTransferDate.CustomizationFormText = "Tranfer Date:";
            this.ItemForTransferDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForTransferDate.Name = "ItemForTransferDate";
            this.ItemForTransferDate.Size = new System.Drawing.Size(711, 24);
            this.ItemForTransferDate.Text = "Tranfer Date:";
            this.ItemForTransferDate.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForTransferredByStaffName
            // 
            this.ItemForTransferredByStaffName.Control = this.TransferredByStaffNameTextEdit;
            this.ItemForTransferredByStaffName.CustomizationFormText = "Transferred By:";
            this.ItemForTransferredByStaffName.Location = new System.Drawing.Point(0, 48);
            this.ItemForTransferredByStaffName.Name = "ItemForTransferredByStaffName";
            this.ItemForTransferredByStaffName.Size = new System.Drawing.Size(711, 24);
            this.ItemForTransferredByStaffName.Text = "Transferred By:";
            this.ItemForTransferredByStaffName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForAmountTransferred
            // 
            this.ItemForAmountTransferred.Control = this.AmountTransferredSpinEdit;
            this.ItemForAmountTransferred.CustomizationFormText = "Amount Transferred:";
            this.ItemForAmountTransferred.Location = new System.Drawing.Point(0, 140);
            this.ItemForAmountTransferred.MaxSize = new System.Drawing.Size(0, 24);
            this.ItemForAmountTransferred.MinSize = new System.Drawing.Size(173, 24);
            this.ItemForAmountTransferred.Name = "ItemForAmountTransferred";
            this.ItemForAmountTransferred.Size = new System.Drawing.Size(355, 24);
            this.ItemForAmountTransferred.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAmountTransferred.Text = "Amount Transferred:";
            this.ItemForAmountTransferred.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 262);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(711, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForFromLocationName
            // 
            this.ItemForFromLocationName.Control = this.FromLocationNameButtonEdit;
            this.ItemForFromLocationName.CustomizationFormText = "From Location:";
            this.ItemForFromLocationName.Location = new System.Drawing.Point(0, 82);
            this.ItemForFromLocationName.Name = "ItemForFromLocationName";
            this.ItemForFromLocationName.Size = new System.Drawing.Size(457, 24);
            this.ItemForFromLocationName.Text = "From Location:";
            this.ItemForFromLocationName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 130);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(711, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForFromLocationType
            // 
            this.ItemForFromLocationType.Control = this.FromLocationTypeTextEdit;
            this.ItemForFromLocationType.CustomizationFormText = "From Location Type:";
            this.ItemForFromLocationType.Location = new System.Drawing.Point(457, 82);
            this.ItemForFromLocationType.Name = "ItemForFromLocationType";
            this.ItemForFromLocationType.Size = new System.Drawing.Size(254, 24);
            this.ItemForFromLocationType.Text = "From Location Type:";
            this.ItemForFromLocationType.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForToLocationName
            // 
            this.ItemForToLocationName.Control = this.ToLocationNameButtonEdit;
            this.ItemForToLocationName.CustomizationFormText = "To Location:";
            this.ItemForToLocationName.Location = new System.Drawing.Point(0, 106);
            this.ItemForToLocationName.Name = "ItemForToLocationName";
            this.ItemForToLocationName.Size = new System.Drawing.Size(457, 24);
            this.ItemForToLocationName.Text = "To Location:";
            this.ItemForToLocationName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForToLocationType
            // 
            this.ItemForToLocationType.Control = this.ToLocationTypeTextEdit;
            this.ItemForToLocationType.CustomizationFormText = "To Location Type:";
            this.ItemForToLocationType.Location = new System.Drawing.Point(457, 106);
            this.ItemForToLocationType.Name = "ItemForToLocationType";
            this.ItemForToLocationType.Size = new System.Drawing.Size(254, 24);
            this.ItemForToLocationType.Text = "To Location Type:";
            this.ItemForToLocationType.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 451);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(711, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(355, 140);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(356, 122);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAmountTransferredDescriptorID
            // 
            this.ItemForAmountTransferredDescriptorID.Control = this.AmountTransferredDescriptorIDGridLookUpEdit;
            this.ItemForAmountTransferredDescriptorID.CustomizationFormText = "Amount Unit Descriptor:";
            this.ItemForAmountTransferredDescriptorID.Location = new System.Drawing.Point(0, 164);
            this.ItemForAmountTransferredDescriptorID.Name = "ItemForAmountTransferredDescriptorID";
            this.ItemForAmountTransferredDescriptorID.Size = new System.Drawing.Size(355, 24);
            this.ItemForAmountTransferredDescriptorID.Text = "Amount Unit Descriptor:";
            this.ItemForAmountTransferredDescriptorID.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForAmountTransferredConverted
            // 
            this.ItemForAmountTransferredConverted.Control = this.AmountTransferredConvertedSpinEdit;
            this.ItemForAmountTransferredConverted.CustomizationFormText = "Amount Converted:";
            this.ItemForAmountTransferredConverted.Location = new System.Drawing.Point(0, 188);
            this.ItemForAmountTransferredConverted.Name = "ItemForAmountTransferredConverted";
            this.ItemForAmountTransferredConverted.Size = new System.Drawing.Size(355, 24);
            this.ItemForAmountTransferredConverted.Text = "Amount Converted:";
            this.ItemForAmountTransferredConverted.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForTransferCost
            // 
            this.ItemForTransferCost.Control = this.TransferCostTextEdit;
            this.ItemForTransferCost.CustomizationFormText = "Transfer Cost:";
            this.ItemForTransferCost.Location = new System.Drawing.Point(0, 212);
            this.ItemForTransferCost.Name = "ItemForTransferCost";
            this.ItemForTransferCost.Size = new System.Drawing.Size(355, 24);
            this.ItemForTransferCost.Text = "Transfer Cost:";
            this.ItemForTransferCost.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForSaltColourID
            // 
            this.ItemForSaltColourID.Control = this.SaltColourIDGridLookUpEdit;
            this.ItemForSaltColourID.CustomizationFormText = "Salt Colour:";
            this.ItemForSaltColourID.Location = new System.Drawing.Point(0, 236);
            this.ItemForSaltColourID.Name = "ItemForSaltColourID";
            this.ItemForSaltColourID.Size = new System.Drawing.Size(355, 26);
            this.ItemForSaltColourID.Text = "Salt Colour:";
            this.ItemForSaltColourID.TextSize = new System.Drawing.Size(115, 13);
            // 
            // sp04216_GC_Salt_Transfer_EditTableAdapter
            // 
            this.sp04216_GC_Salt_Transfer_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter
            // 
            this.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04218_GC_Salt_Colours_List_With_BlankTableAdapter
            // 
            this.sp04218_GC_Salt_Colours_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Salt_Transfer_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(731, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Salt_Transfer_Edit";
            this.Text = "Edit Salt Transfer";
            this.Activated += new System.EventHandler(this.frm_GC_Salt_Transfer_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Salt_Transfer_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Salt_Transfer_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ToLocationTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04216GCSaltTransferEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToLocationNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromLocationTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromLocationNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltColourIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04218GCSaltColoursListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountTransferredConvertedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountTransferredDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04209GCSaltUnitConversionListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountTransferredSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToLocationTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToLocationIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromLocationTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromLocationIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritTransferIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferCostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritTransferID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromLocationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToLocationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromLocationTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToLocationTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountTransferred)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromLocationName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromLocationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToLocationName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToLocationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountTransferredDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountTransferredConverted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltColourID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit GritTransferIDTextEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritTransferID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.SpinEdit TransferCostTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferCost;
        private System.Windows.Forms.BindingSource sp04216GCSaltTransferEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04216_GC_Salt_Transfer_EditTableAdapter sp04216_GC_Salt_Transfer_EditTableAdapter;
        private DevExpress.XtraEditors.DateEdit TransferDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferDate;
        private DevExpress.XtraEditors.TextEdit TransferredByStaffIDTextEdit;
        private DevExpress.XtraEditors.TextEdit TransferredByStaffNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferredByStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferredByStaffName;
        private DevExpress.XtraEditors.TextEdit FromLocationTypeIDTextEdit;
        private DevExpress.XtraEditors.TextEdit FromLocationIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFromLocationID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFromLocationTypeID;
        private DevExpress.XtraEditors.TextEdit ToLocationTypeIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ToLocationIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForToLocationID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForToLocationTypeID;
        private DevExpress.XtraEditors.SpinEdit AmountTransferredSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmountTransferred;
        private DevExpress.XtraEditors.GridLookUpEdit AmountTransferredDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmountTransferredDescriptorID;
        private DevExpress.XtraEditors.SpinEdit AmountTransferredConvertedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmountTransferredConverted;
        private DevExpress.XtraEditors.GridLookUpEdit SaltColourIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSaltColourID;
        private DevExpress.XtraEditors.TextEdit FromLocationTypeTextEdit;
        private DevExpress.XtraEditors.ButtonEdit FromLocationNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFromLocationName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFromLocationType;
        private DevExpress.XtraEditors.TextEdit ToLocationTypeTextEdit;
        private DevExpress.XtraEditors.ButtonEdit ToLocationNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForToLocationName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForToLocationType;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource sp04209GCSaltUnitConversionListWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colConversionValue;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUnitConversionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUnitDescription;
        private System.Windows.Forms.BindingSource sp04218GCSaltColoursListWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04218_GC_Salt_Colours_List_With_BlankTableAdapter sp04218_GC_Salt_Colours_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCode;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

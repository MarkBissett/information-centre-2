﻿namespace WoodPlan5
{
    partial class frm_GC_Callout_Wizard_Add_BlockEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ServiceFailureCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NonStandardSellCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NonStandardCostCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SaltUsedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNonStandardCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNonStandardSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSaltUsed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForServiceFailure = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.SnowOnSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ItemForSnowOnSite = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceFailureCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardSellCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardCostCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltUsedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceFailure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowOnSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowOnSite)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(388, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 244);
            this.barDockControlBottom.Size = new System.Drawing.Size(388, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 218);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(388, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 218);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.SnowOnSiteCheckEdit);
            this.layoutControl1.Controls.Add(this.ServiceFailureCheckEdit);
            this.layoutControl1.Controls.Add(this.NonStandardSellCheckEdit);
            this.layoutControl1.Controls.Add(this.NonStandardCostCheckEdit);
            this.layoutControl1.Controls.Add(this.ClientPONumberTextEdit);
            this.layoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.layoutControl1.Controls.Add(this.SaltUsedSpinEdit);
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1213, 293, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(387, 185);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // ServiceFailureCheckEdit
            // 
            this.ServiceFailureCheckEdit.Location = new System.Drawing.Point(112, 152);
            this.ServiceFailureCheckEdit.MenuManager = this.barManager1;
            this.ServiceFailureCheckEdit.Name = "ServiceFailureCheckEdit";
            this.ServiceFailureCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ServiceFailureCheckEdit.Properties.ValueChecked = 1;
            this.ServiceFailureCheckEdit.Properties.ValueUnchecked = 0;
            this.ServiceFailureCheckEdit.Size = new System.Drawing.Size(263, 19);
            this.ServiceFailureCheckEdit.StyleController = this.layoutControl1;
            this.ServiceFailureCheckEdit.TabIndex = 12;
            // 
            // NonStandardSellCheckEdit
            // 
            this.NonStandardSellCheckEdit.Location = new System.Drawing.Point(112, 129);
            this.NonStandardSellCheckEdit.MenuManager = this.barManager1;
            this.NonStandardSellCheckEdit.Name = "NonStandardSellCheckEdit";
            this.NonStandardSellCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NonStandardSellCheckEdit.Properties.ValueChecked = 1;
            this.NonStandardSellCheckEdit.Properties.ValueUnchecked = 0;
            this.NonStandardSellCheckEdit.Size = new System.Drawing.Size(263, 19);
            this.NonStandardSellCheckEdit.StyleController = this.layoutControl1;
            this.NonStandardSellCheckEdit.TabIndex = 11;
            // 
            // NonStandardCostCheckEdit
            // 
            this.NonStandardCostCheckEdit.Location = new System.Drawing.Point(112, 106);
            this.NonStandardCostCheckEdit.MenuManager = this.barManager1;
            this.NonStandardCostCheckEdit.Name = "NonStandardCostCheckEdit";
            this.NonStandardCostCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NonStandardCostCheckEdit.Properties.ValueChecked = 1;
            this.NonStandardCostCheckEdit.Properties.ValueUnchecked = 0;
            this.NonStandardCostCheckEdit.Size = new System.Drawing.Size(263, 19);
            this.NonStandardCostCheckEdit.StyleController = this.layoutControl1;
            this.NonStandardCostCheckEdit.TabIndex = 10;
            // 
            // ClientPONumberTextEdit
            // 
            this.ClientPONumberTextEdit.Location = new System.Drawing.Point(112, 35);
            this.ClientPONumberTextEdit.MenuManager = this.barManager1;
            this.ClientPONumberTextEdit.Name = "ClientPONumberTextEdit";
            this.ClientPONumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPONumberTextEdit, true);
            this.ClientPONumberTextEdit.Size = new System.Drawing.Size(263, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPONumberTextEdit, optionsSpelling1);
            this.ClientPONumberTextEdit.StyleController = this.layoutControl1;
            this.ClientPONumberTextEdit.TabIndex = 8;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(112, 12);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "(Tick if Reactive, Untick for Proactive)";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(263, 19);
            this.ReactiveCheckEdit.StyleController = this.layoutControl1;
            this.ReactiveCheckEdit.TabIndex = 5;
            // 
            // SaltUsedSpinEdit
            // 
            this.SaltUsedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SaltUsedSpinEdit.Location = new System.Drawing.Point(112, 82);
            this.SaltUsedSpinEdit.MenuManager = this.barManager1;
            this.SaltUsedSpinEdit.Name = "SaltUsedSpinEdit";
            this.SaltUsedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SaltUsedSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.SaltUsedSpinEdit.Properties.Mask.EditMask = "######0.00 25kg Bags";
            this.SaltUsedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SaltUsedSpinEdit.Properties.MaxLength = 50;
            this.SaltUsedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            131072});
            this.SaltUsedSpinEdit.Size = new System.Drawing.Size(263, 20);
            this.SaltUsedSpinEdit.StyleController = this.layoutControl1;
            this.SaltUsedSpinEdit.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForReactive,
            this.ItemForClientPONumber,
            this.ItemForNonStandardCost,
            this.ItemForNonStandardSell,
            this.ItemForSaltUsed,
            this.ItemForServiceFailure,
            this.ItemForSnowOnSite});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(387, 185);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 0);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(367, 23);
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberTextEdit;
            this.ItemForClientPONumber.CustomizationFormText = "Client P.O. Number:";
            this.ItemForClientPONumber.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(367, 24);
            this.ItemForClientPONumber.Text = "Client P.O. Number:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForNonStandardCost
            // 
            this.ItemForNonStandardCost.Control = this.NonStandardCostCheckEdit;
            this.ItemForNonStandardCost.CustomizationFormText = "Non-Standard Cost:";
            this.ItemForNonStandardCost.Location = new System.Drawing.Point(0, 94);
            this.ItemForNonStandardCost.Name = "ItemForNonStandardCost";
            this.ItemForNonStandardCost.Size = new System.Drawing.Size(367, 23);
            this.ItemForNonStandardCost.Text = "Non-Standard Cost:";
            this.ItemForNonStandardCost.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForNonStandardSell
            // 
            this.ItemForNonStandardSell.Control = this.NonStandardSellCheckEdit;
            this.ItemForNonStandardSell.CustomizationFormText = "Non-Standard Sell:";
            this.ItemForNonStandardSell.Location = new System.Drawing.Point(0, 117);
            this.ItemForNonStandardSell.Name = "ItemForNonStandardSell";
            this.ItemForNonStandardSell.Size = new System.Drawing.Size(367, 23);
            this.ItemForNonStandardSell.Text = "Non-Standard Sell:";
            this.ItemForNonStandardSell.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForSaltUsed
            // 
            this.ItemForSaltUsed.Control = this.SaltUsedSpinEdit;
            this.ItemForSaltUsed.CustomizationFormText = "Salt Required:";
            this.ItemForSaltUsed.Location = new System.Drawing.Point(0, 70);
            this.ItemForSaltUsed.Name = "ItemForSaltUsed";
            this.ItemForSaltUsed.Size = new System.Drawing.Size(367, 24);
            this.ItemForSaltUsed.Text = "Salt Required:";
            this.ItemForSaltUsed.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForServiceFailure
            // 
            this.ItemForServiceFailure.Control = this.ServiceFailureCheckEdit;
            this.ItemForServiceFailure.CustomizationFormText = "Service Failure:";
            this.ItemForServiceFailure.Location = new System.Drawing.Point(0, 140);
            this.ItemForServiceFailure.Name = "ItemForServiceFailure";
            this.ItemForServiceFailure.Size = new System.Drawing.Size(367, 25);
            this.ItemForServiceFailure.Text = "Service Failure:";
            this.ItemForServiceFailure.TextSize = new System.Drawing.Size(96, 13);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(168, 214);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(279, 214);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // SnowOnSiteCheckEdit
            // 
            this.SnowOnSiteCheckEdit.Location = new System.Drawing.Point(112, 59);
            this.SnowOnSiteCheckEdit.MenuManager = this.barManager1;
            this.SnowOnSiteCheckEdit.Name = "SnowOnSiteCheckEdit";
            this.SnowOnSiteCheckEdit.Properties.Caption = "(Tick if Yes. On Saving, Salt Required is Doubled)";
            this.SnowOnSiteCheckEdit.Properties.ValueChecked = 1;
            this.SnowOnSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowOnSiteCheckEdit.Size = new System.Drawing.Size(263, 19);
            this.SnowOnSiteCheckEdit.StyleController = this.layoutControl1;
            this.SnowOnSiteCheckEdit.TabIndex = 13;
            // 
            // ItemForSnowOnSite
            // 
            this.ItemForSnowOnSite.Control = this.SnowOnSiteCheckEdit;
            this.ItemForSnowOnSite.CustomizationFormText = "Snow On Site:";
            this.ItemForSnowOnSite.Location = new System.Drawing.Point(0, 47);
            this.ItemForSnowOnSite.Name = "ItemForSnowOnSite";
            this.ItemForSnowOnSite.Size = new System.Drawing.Size(367, 23);
            this.ItemForSnowOnSite.Text = "Snow On Site:";
            this.ItemForSnowOnSite.TextSize = new System.Drawing.Size(96, 13);
            // 
            // frm_GC_Callout_Wizard_Add_BlockEdit
            // 
            this.ClientSize = new System.Drawing.Size(388, 244);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_GC_Callout_Wizard_Add_BlockEdit";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add GRITTING Callout Wizard - Block Edit";
            this.Load += new System.EventHandler(this.frm_GC_Callout_Wizard_Add_BlockEdit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ServiceFailureCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardSellCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardCostCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaltUsedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaltUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceFailure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowOnSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowOnSite)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSaltUsed;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraEditors.TextEdit ClientPONumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraEditors.CheckEdit NonStandardSellCheckEdit;
        private DevExpress.XtraEditors.CheckEdit NonStandardCostCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonStandardCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonStandardSell;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SpinEdit SaltUsedSpinEdit;
        private DevExpress.XtraEditors.CheckEdit ServiceFailureCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceFailure;
        private DevExpress.XtraEditors.CheckEdit SnowOnSiteCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowOnSite;
    }
}

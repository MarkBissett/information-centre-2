﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Callout_Send_No_Proactive_Messages : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_GC_Callout_Send_No_Proactive_Messages()
        {
            InitializeComponent();
        }

        private void frm_GC_Callout_Send_No_Proactive_Messages_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400063;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04289_GC_Teams_For_No_Proactive_Gritting_MessagesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04289_GC_Teams_For_No_Proactive_Gritting_MessagesTableAdapter.Fill(dataSet_GC_DataEntry.sp04289_GC_Teams_For_No_Proactive_Gritting_Messages);
            // Add record selection checkboxes to popup Callout Type grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            
            selection1.SelectAll();  // Tick all rows //

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Teams Available for Sending No Proactive Gritting Messages To");
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0 || selection1.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more teams to text by ticking them before proceeding.", "Send No Proactive Gritting Messages to Teams", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else  // OK to proceed with sending text messages //
            {
                // Check for internet Connectivity to Text Service //
                bool boolTextServiceAvailable = BaseObjects.PingTest.Ping("textanywhere.net");

                if (!boolTextServiceAvailable)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the Text Anywhere service - NO TEXT MESSAGES SENT.\n\nThe problem may be with your internet connection or with the Text Anywhere service. Please check then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // Get Settings from System Settings table //
                string strTextServiceUsername = "";
                string strTextServicePassword = "";
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strTextServiceUsername = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "TextAnywhereUsername").ToString();
                    strTextServicePassword = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "TextAnywherePassword").ToString();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the Text Anywhere service [Missing Connection Credentials] - NO TEXT MESSAGES SENT.\n\nPlease contact Technical Support.", "Get Text Service Login Credentials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (string.IsNullOrEmpty(strTextServiceUsername) || string.IsNullOrEmpty(strTextServicePassword))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the Text Anywhere service [Missing Connection Credentials] - NO TEXT MESSAGES SENT.\n\nPlease contact Technical Support.", "Get Text Service Login Credentials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // If at this point, we are good to send text messages so get user confirmation //
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (selection1.SelectedCount == 1 ? "1 Team" : selection1.SelectedCount.ToString() + " Teams") + " selected for sending a 'No Proactive Gritting Tonight' text message.\n\nProceed?", "Send No Proactive Gritting Messages to Teams", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

                frmProgress fProgress = new frmProgress(0);
                fProgress.UpdateCaption("Sending Text Message(s)...");
                fProgress.Show();
                System.Windows.Forms.Application.DoEvents();
                int intUpdateProgressThreshhold = selection1.SelectedCount / 10;
                int intUpdateProgressTempCount = 0;

                string strTextMessage = "No Proactive Site Gritting is required this evening";
                string strPhoneNumber = "";
                int intTeamID = 0;
                try
                {
                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            strPhoneNumber = Convert.ToString(view.GetRowCellValue(i, "GritMobileTelephoneNumber"));
                            intTeamID = Convert.ToInt32(view.GetRowCellValue(i, "SubContractorID"));
                            Send_Text_Messages(strTextServiceUsername, strTextServicePassword, intTeamID, 4, strTextMessage, strPhoneNumber);

                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while sending the text message(s).\n\nPlease try again. If the problem persists, contact Technical Support.", "Send No Proactive Gritting Messages to Teams", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
            }
            DevExpress.XtraEditors.XtraMessageBox.Show("Text Message(s) Sent Successfully.", "Send No Proactive Gritting Messages to Teams", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
            this.Close();

        }
        private void Send_Text_Messages(string strTextServiceUsername, string strTextServicePassword, int intSubContractorID, int intTextMessageType, string strTextMessage, string strPhoneNumber)
        {
            // Add Text Sent Header to DB and pick up the returned Header ID //
            int intNewHeaderID = 0;
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter InsertTextHeader = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                InsertTextHeader.ChangeConnectionString(strConnectionString);
                intNewHeaderID = Convert.ToInt32(InsertTextHeader.sp04104_GC_Get_Callouts_Create_Text_Header(intSubContractorID, intTextMessageType, strTextMessage, strPhoneNumber));
            }
            catch (Exception)
            {
            }
            // Link Jobs to Text Message and update Job Status to sent //
            /*try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter InsertTextHeaderLink = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                InsertTextHeaderLink.ChangeConnectionString(strConnectionString);
                InsertTextHeaderLink.sp04105_GC_Get_Callouts_Create_Text_Header_CallOuts(intNewHeaderID, strCalloutIDs);
            }
            catch (Exception)
            {
            }*/
            if (!string.IsNullOrEmpty(strPhoneNumber))
            {
                TxtMessage msg = new TxtMessage(strPhoneNumber, strTextMessage, intNewHeaderID, strTextServiceUsername, strTextServicePassword);
                msg.Send(new object());
                if (msg.Status != EnmStatus.Ok)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Problem sending Message to " + strPhoneNumber, "Problem sending text", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // Remove the Sent_Text_Message and Link Callouts from the DB since the text message failed to send //
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }



    }
}

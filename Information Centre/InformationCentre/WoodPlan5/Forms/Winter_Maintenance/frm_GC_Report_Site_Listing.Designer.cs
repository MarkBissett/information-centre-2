namespace WoodPlan5
{
    partial class frm_GC_Report_Site_Listing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                // ***** Following wrapped in a Try Catch Block to avoid erro about comonents = null ???, Mark Bissett [19/11/2008] ***** // 
                try
                {
                    components.Dispose();
                }
                catch
                {
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip23 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem23 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem23 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip24 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem24 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem24 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip25 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem25 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem25 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip26 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem26 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem26 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip27 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem27 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem27 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip28 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem28 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem28 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip29 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem29 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem29 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip30 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem30 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem30 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip31 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem31 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem31 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip32 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem32 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem32 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip33 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem33 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem33 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip34 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem34 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem34 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip35 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem35 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem35 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip36 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem36 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem36 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip37 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem37 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem37 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip38 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem38 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem38 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip39 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem39 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem39 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip40 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem40 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem40 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip41 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem41 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem41 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip42 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem42 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem42 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip43 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem43 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem43 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip44 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem44 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem44 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip45 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem45 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem45 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip46 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem46 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem46 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip47 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem47 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem47 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip48 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem48 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem48 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip49 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem49 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem49 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip50 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem50 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem50 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip51 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem51 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem51 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip52 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem52 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem52 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip53 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem53 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem53 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip54 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem54 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem54 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip55 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem55 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem55 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip56 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem56 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem56 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip57 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem57 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem57 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Report_Site_Listing));
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.printRibbonController1 = new DevExpress.XtraPrinting.Preview.PrintRibbonController(this.components);
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.printPreviewBarItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem5 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem7 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem8 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem13 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem15 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem16 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem17 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem18 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem19 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem20 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem21 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem22 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem23 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem24 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem25 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem26 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem27 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem28 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem29 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem30 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem31 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem32 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem33 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem34 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem35 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem36 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem37 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem38 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem39 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem40 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem41 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem42 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem43 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem44 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem45 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewStaticItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.progressBarEditItem1 = new DevExpress.XtraPrinting.Preview.ProgressBarEditItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.printPreviewBarItem46 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.printPreviewStaticItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.zoomTrackBarEditItem1 = new DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.printPreviewRibbonPage1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.printPreviewRibbonPageGroup1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup2 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup3 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup4 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup5 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup6 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup7 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl3 = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.deFromDate = new DevExpress.XtraEditors.DateEdit();
            this.deToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnShowLinkedDataOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditSnowClearanceCalloutsRates = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSnowClearanceCalloutsExtraCosts = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditGrittingCalloutsImages = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditGrittingCalloutsExtraCosts = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSnowClearanceCallouts = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditGrittingCallouts = new DevExpress.XtraEditors.CheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04295GCReportsSiteListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteFax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMappingWorkspaceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkspaceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHubID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsSnowClearanceSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedGrittingContractCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedSnowClearanceContractCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnLayoutAddNew = new DevExpress.XtraEditors.SimpleButton();
            this.btnLayoutOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp01206_AT_Report_Available_LayoutsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReportLayoutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublishedToWeb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.checkEditActiveSnowClearance = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditActiveGritting = new DevExpress.XtraEditors.CheckEdit();
            this.btnClientFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp03005EPClientListAllBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP = new WoodPlan5.DataSet_EP();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp01206_AT_Report_Available_LayoutsTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp01206_AT_Report_Available_LayoutsTableAdapter();
            this.bbiPublishToWeb = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUnpublishToWeb = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemClientFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditClientFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiReloadDataSupply = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewReport = new DevExpress.XtraBars.BarButtonItem();
            this.sp03005_EP_Client_List_AllTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03005_EP_Client_List_AllTableAdapter();
            this.sp04295_GC_Reports_Site_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04295_GC_Reports_Site_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printRibbonController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl3)).BeginInit();
            this.popupContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSnowClearanceCalloutsRates.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSnowClearanceCalloutsExtraCosts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGrittingCalloutsImages.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGrittingCalloutsExtraCosts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSnowClearanceCallouts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGrittingCallouts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04295GCReportsSiteListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01206_AT_Report_Available_LayoutsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActiveSnowClearance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActiveGritting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03005EPClientListAllBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditClientFilter)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPublishToWeb, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1102, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 718);
            this.barDockControlBottom.Size = new System.Drawing.Size(1102, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 718);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1102, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 718);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiPublishToWeb,
            this.bbiUnpublishToWeb,
            this.bbiReloadDataSupply,
            this.bbiViewReport,
            this.barEditItemClientFilter});
            this.barManager1.MaxItemId = 32;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditClientFilter});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Width = 74;
            // 
            // printRibbonController1
            // 
            this.printRibbonController1.PrintControl = this.printControl1;
            this.printRibbonController1.RibbonControl = this.ribbonControl1;
            this.printRibbonController1.RibbonStatusBar = this.ribbonStatusBar1;
            // 
            // printControl1
            // 
            this.printControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printControl1.IsMetric = true;
            this.printControl1.Location = new System.Drawing.Point(444, 142);
            this.printControl1.Name = "printControl1";
            this.printControl1.PrintingSystem = this.printingSystem1;
            this.printControl1.Size = new System.Drawing.Size(658, 553);
            this.printControl1.TabIndex = 6;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonText = null;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.printPreviewBarItem1,
            this.printPreviewBarItem2,
            this.printPreviewBarItem3,
            this.printPreviewBarItem4,
            this.printPreviewBarItem5,
            this.printPreviewBarItem6,
            this.printPreviewBarItem7,
            this.printPreviewBarItem8,
            this.printPreviewBarItem9,
            this.printPreviewBarItem10,
            this.printPreviewBarItem11,
            this.printPreviewBarItem12,
            this.printPreviewBarItem13,
            this.printPreviewBarItem14,
            this.printPreviewBarItem15,
            this.printPreviewBarItem16,
            this.printPreviewBarItem17,
            this.printPreviewBarItem18,
            this.printPreviewBarItem19,
            this.printPreviewBarItem20,
            this.printPreviewBarItem21,
            this.printPreviewBarItem22,
            this.printPreviewBarItem23,
            this.printPreviewBarItem24,
            this.printPreviewBarItem25,
            this.printPreviewBarItem26,
            this.printPreviewBarItem27,
            this.printPreviewBarItem28,
            this.printPreviewBarItem29,
            this.printPreviewBarItem30,
            this.printPreviewBarItem31,
            this.printPreviewBarItem32,
            this.printPreviewBarItem33,
            this.printPreviewBarItem34,
            this.printPreviewBarItem35,
            this.printPreviewBarItem36,
            this.printPreviewBarItem37,
            this.printPreviewBarItem38,
            this.printPreviewBarItem39,
            this.printPreviewBarItem40,
            this.printPreviewBarItem41,
            this.printPreviewBarItem42,
            this.printPreviewBarItem43,
            this.printPreviewBarItem44,
            this.printPreviewBarItem45,
            this.printPreviewStaticItem1,
            this.barStaticItem1,
            this.progressBarEditItem1,
            this.printPreviewBarItem46,
            this.barButtonItem1,
            this.printPreviewStaticItem2,
            this.zoomTrackBarEditItem1});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 56;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.printPreviewRibbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemZoomTrackBar1});
            this.ribbonControl1.Size = new System.Drawing.Size(1102, 142);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.TransparentEditorsMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // printPreviewBarItem1
            // 
            this.printPreviewBarItem1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem1.Caption = "Bookmarks";
            this.printPreviewBarItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap;
            this.printPreviewBarItem1.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem1.Enabled = false;
            this.printPreviewBarItem1.Id = 0;
            this.printPreviewBarItem1.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_DocumentMap;
            this.printPreviewBarItem1.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_DocumentMapLarge;
            this.printPreviewBarItem1.Name = "printPreviewBarItem1";
            superToolTip4.FixedTooltipWidth = true;
            toolTipTitleItem4.Text = "Document Map";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Open the Document Map, which allows you to navigate through a structural view of " +
    "the document.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip4.MaxWidth = 210;
            this.printPreviewBarItem1.SuperTip = superToolTip4;
            // 
            // printPreviewBarItem2
            // 
            this.printPreviewBarItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem2.Caption = "Parameters";
            this.printPreviewBarItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Parameters;
            this.printPreviewBarItem2.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem2.Enabled = false;
            this.printPreviewBarItem2.Id = 1;
            this.printPreviewBarItem2.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Parameters;
            this.printPreviewBarItem2.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ParametersLarge;
            this.printPreviewBarItem2.Name = "printPreviewBarItem2";
            superToolTip5.FixedTooltipWidth = true;
            toolTipTitleItem5.Text = "Parameters";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Open the Parameters pane, which allows you to enter values for report parameters." +
    "";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            superToolTip5.MaxWidth = 210;
            this.printPreviewBarItem2.SuperTip = superToolTip5;
            // 
            // printPreviewBarItem3
            // 
            this.printPreviewBarItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem3.Caption = "Find";
            this.printPreviewBarItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Find;
            this.printPreviewBarItem3.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem3.Enabled = false;
            this.printPreviewBarItem3.Id = 2;
            this.printPreviewBarItem3.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Find;
            this.printPreviewBarItem3.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.printPreviewBarItem3.Name = "printPreviewBarItem3";
            superToolTip6.FixedTooltipWidth = true;
            toolTipTitleItem6.Text = "Find";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Show the Find dialog to find text in the document.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            superToolTip6.MaxWidth = 210;
            this.printPreviewBarItem3.SuperTip = superToolTip6;
            // 
            // printPreviewBarItem4
            // 
            this.printPreviewBarItem4.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem4.Caption = "Options";
            this.printPreviewBarItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Customize;
            this.printPreviewBarItem4.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem4.Enabled = false;
            this.printPreviewBarItem4.Id = 3;
            this.printPreviewBarItem4.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Customize;
            this.printPreviewBarItem4.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_CustomizeLarge;
            this.printPreviewBarItem4.Name = "printPreviewBarItem4";
            superToolTip7.FixedTooltipWidth = true;
            toolTipTitleItem7.Text = "Options";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Open the Print Options dialog, in which you can change printing options.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            superToolTip7.MaxWidth = 210;
            this.printPreviewBarItem4.SuperTip = superToolTip7;
            // 
            // printPreviewBarItem5
            // 
            this.printPreviewBarItem5.Caption = "Print";
            this.printPreviewBarItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Print;
            this.printPreviewBarItem5.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem5.Enabled = false;
            this.printPreviewBarItem5.Id = 4;
            this.printPreviewBarItem5.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Print;
            this.printPreviewBarItem5.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintLarge;
            this.printPreviewBarItem5.Name = "printPreviewBarItem5";
            superToolTip8.FixedTooltipWidth = true;
            toolTipTitleItem8.Text = "Print (Ctrl+P)";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Select a printer, number of copies and other printing options before printing.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            superToolTip8.MaxWidth = 210;
            this.printPreviewBarItem5.SuperTip = superToolTip8;
            // 
            // printPreviewBarItem6
            // 
            this.printPreviewBarItem6.Caption = "Quick Print";
            this.printPreviewBarItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect;
            this.printPreviewBarItem6.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem6.Enabled = false;
            this.printPreviewBarItem6.Id = 5;
            this.printPreviewBarItem6.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.printPreviewBarItem6.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirectLarge;
            this.printPreviewBarItem6.Name = "printPreviewBarItem6";
            superToolTip9.FixedTooltipWidth = true;
            toolTipTitleItem9.Text = "Quick Print";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Send the document directly to the default printer without making changes.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            superToolTip9.MaxWidth = 210;
            this.printPreviewBarItem6.SuperTip = superToolTip9;
            // 
            // printPreviewBarItem7
            // 
            this.printPreviewBarItem7.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem7.Caption = "Custom Margins...";
            this.printPreviewBarItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup;
            this.printPreviewBarItem7.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem7.Enabled = false;
            this.printPreviewBarItem7.Id = 6;
            this.printPreviewBarItem7.Name = "printPreviewBarItem7";
            superToolTip10.FixedTooltipWidth = true;
            toolTipTitleItem10.Text = "Page Setup";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Show the Page Setup dialog.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            superToolTip10.MaxWidth = 210;
            this.printPreviewBarItem7.SuperTip = superToolTip10;
            // 
            // printPreviewBarItem8
            // 
            this.printPreviewBarItem8.Caption = "Header/Footer";
            this.printPreviewBarItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.EditPageHF;
            this.printPreviewBarItem8.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem8.Enabled = false;
            this.printPreviewBarItem8.Id = 7;
            this.printPreviewBarItem8.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_EditPageHF;
            this.printPreviewBarItem8.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_EditPageHFLarge;
            this.printPreviewBarItem8.Name = "printPreviewBarItem8";
            superToolTip11.FixedTooltipWidth = true;
            toolTipTitleItem11.Text = "Header and Footer";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Edit the header and footer of the document.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            superToolTip11.MaxWidth = 210;
            this.printPreviewBarItem8.SuperTip = superToolTip11;
            // 
            // printPreviewBarItem9
            // 
            this.printPreviewBarItem9.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem9.Caption = "Scale";
            this.printPreviewBarItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Scale;
            this.printPreviewBarItem9.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem9.Enabled = false;
            this.printPreviewBarItem9.Id = 8;
            this.printPreviewBarItem9.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Scale;
            this.printPreviewBarItem9.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ScaleLarge;
            this.printPreviewBarItem9.Name = "printPreviewBarItem9";
            superToolTip12.FixedTooltipWidth = true;
            toolTipTitleItem12.Text = "Scale";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Stretch or shrink the printed output to a percentage of its actual size.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            superToolTip12.MaxWidth = 210;
            this.printPreviewBarItem9.SuperTip = superToolTip12;
            // 
            // printPreviewBarItem10
            // 
            this.printPreviewBarItem10.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem10.Caption = "Pointer";
            this.printPreviewBarItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Pointer;
            this.printPreviewBarItem10.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem10.Down = true;
            this.printPreviewBarItem10.Enabled = false;
            this.printPreviewBarItem10.GroupIndex = 1;
            this.printPreviewBarItem10.Id = 9;
            this.printPreviewBarItem10.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Pointer;
            this.printPreviewBarItem10.Name = "printPreviewBarItem10";
            this.printPreviewBarItem10.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip13.FixedTooltipWidth = true;
            toolTipTitleItem13.Text = "Mouse Pointer";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Show the mouse pointer.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            superToolTip13.MaxWidth = 210;
            this.printPreviewBarItem10.SuperTip = superToolTip13;
            // 
            // printPreviewBarItem11
            // 
            this.printPreviewBarItem11.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem11.Caption = "Hand Tool";
            this.printPreviewBarItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HandTool;
            this.printPreviewBarItem11.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem11.Enabled = false;
            this.printPreviewBarItem11.GroupIndex = 1;
            this.printPreviewBarItem11.Id = 10;
            this.printPreviewBarItem11.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_HandTool;
            this.printPreviewBarItem11.Name = "printPreviewBarItem11";
            this.printPreviewBarItem11.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip14.FixedTooltipWidth = true;
            toolTipTitleItem14.Text = "Hand Tool";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Invoke the Hand tool to manually scroll through pages.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            superToolTip14.MaxWidth = 210;
            this.printPreviewBarItem11.SuperTip = superToolTip14;
            // 
            // printPreviewBarItem12
            // 
            this.printPreviewBarItem12.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem12.Caption = "Magnifier";
            this.printPreviewBarItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Magnifier;
            this.printPreviewBarItem12.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem12.Enabled = false;
            this.printPreviewBarItem12.GroupIndex = 1;
            this.printPreviewBarItem12.Id = 11;
            this.printPreviewBarItem12.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Magnifier;
            this.printPreviewBarItem12.Name = "printPreviewBarItem12";
            this.printPreviewBarItem12.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip15.FixedTooltipWidth = true;
            toolTipTitleItem15.Text = "Magnifier";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Invoke the Magnifier tool.\r\n\r\nClicking once on a document zooms it so that a sing" +
    "le page becomes entirely visible, while clicking another time zooms it to 100% o" +
    "f the normal size.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            superToolTip15.MaxWidth = 210;
            this.printPreviewBarItem12.SuperTip = superToolTip15;
            // 
            // printPreviewBarItem13
            // 
            this.printPreviewBarItem13.Caption = "Zoom Out";
            this.printPreviewBarItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomOut;
            this.printPreviewBarItem13.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem13.Enabled = false;
            this.printPreviewBarItem13.Id = 12;
            this.printPreviewBarItem13.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomOut;
            this.printPreviewBarItem13.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomOutLarge;
            this.printPreviewBarItem13.Name = "printPreviewBarItem13";
            superToolTip16.FixedTooltipWidth = true;
            toolTipTitleItem16.Text = "Zoom Out";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Zoom out to see more of the page at a reduced size.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            superToolTip16.MaxWidth = 210;
            this.printPreviewBarItem13.SuperTip = superToolTip16;
            // 
            // printPreviewBarItem14
            // 
            this.printPreviewBarItem14.Caption = "Zoom In";
            this.printPreviewBarItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomIn;
            this.printPreviewBarItem14.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem14.Enabled = false;
            this.printPreviewBarItem14.Id = 13;
            this.printPreviewBarItem14.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomIn;
            this.printPreviewBarItem14.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomInLarge;
            this.printPreviewBarItem14.Name = "printPreviewBarItem14";
            superToolTip17.FixedTooltipWidth = true;
            toolTipTitleItem17.Text = "Zoom In";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Zoom in to get a close-up view of the document.";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            superToolTip17.MaxWidth = 210;
            this.printPreviewBarItem14.SuperTip = superToolTip17;
            // 
            // printPreviewBarItem15
            // 
            this.printPreviewBarItem15.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem15.Caption = "Zoom";
            this.printPreviewBarItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Zoom;
            this.printPreviewBarItem15.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem15.Enabled = false;
            this.printPreviewBarItem15.Id = 14;
            this.printPreviewBarItem15.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Zoom;
            this.printPreviewBarItem15.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomLarge;
            this.printPreviewBarItem15.Name = "printPreviewBarItem15";
            superToolTip18.FixedTooltipWidth = true;
            toolTipTitleItem18.Text = "Zoom";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Change the zoom level of the document preview.";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            superToolTip18.MaxWidth = 210;
            this.printPreviewBarItem15.SuperTip = superToolTip18;
            // 
            // printPreviewBarItem16
            // 
            this.printPreviewBarItem16.Caption = "First Page";
            this.printPreviewBarItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowFirstPage;
            this.printPreviewBarItem16.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem16.Enabled = false;
            this.printPreviewBarItem16.Id = 15;
            this.printPreviewBarItem16.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowFirstPage;
            this.printPreviewBarItem16.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowFirstPageLarge;
            this.printPreviewBarItem16.Name = "printPreviewBarItem16";
            superToolTip19.FixedTooltipWidth = true;
            toolTipTitleItem19.Text = "First Page (Ctrl+Home)";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Navigate to the first page of the document.";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            superToolTip19.MaxWidth = 210;
            this.printPreviewBarItem16.SuperTip = superToolTip19;
            // 
            // printPreviewBarItem17
            // 
            this.printPreviewBarItem17.Caption = "Previous Page";
            this.printPreviewBarItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowPrevPage;
            this.printPreviewBarItem17.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem17.Enabled = false;
            this.printPreviewBarItem17.Id = 16;
            this.printPreviewBarItem17.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowPrevPage;
            this.printPreviewBarItem17.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowPrevPageLarge;
            this.printPreviewBarItem17.Name = "printPreviewBarItem17";
            superToolTip20.FixedTooltipWidth = true;
            toolTipTitleItem20.Text = "Previous Page (PageUp)";
            toolTipItem20.LeftIndent = 6;
            toolTipItem20.Text = "Navigate to the previous page of the document.";
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem20);
            superToolTip20.MaxWidth = 210;
            this.printPreviewBarItem17.SuperTip = superToolTip20;
            // 
            // printPreviewBarItem18
            // 
            this.printPreviewBarItem18.Caption = "Next  Page ";
            this.printPreviewBarItem18.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowNextPage;
            this.printPreviewBarItem18.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem18.Enabled = false;
            this.printPreviewBarItem18.Id = 17;
            this.printPreviewBarItem18.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowNextPage;
            this.printPreviewBarItem18.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowNextPageLarge;
            this.printPreviewBarItem18.Name = "printPreviewBarItem18";
            superToolTip21.FixedTooltipWidth = true;
            toolTipTitleItem21.Text = "Next Page (PageDown)";
            toolTipItem21.LeftIndent = 6;
            toolTipItem21.Text = "Navigate to the next page of the document.";
            superToolTip21.Items.Add(toolTipTitleItem21);
            superToolTip21.Items.Add(toolTipItem21);
            superToolTip21.MaxWidth = 210;
            this.printPreviewBarItem18.SuperTip = superToolTip21;
            // 
            // printPreviewBarItem19
            // 
            this.printPreviewBarItem19.Caption = "Last  Page ";
            this.printPreviewBarItem19.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowLastPage;
            this.printPreviewBarItem19.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem19.Enabled = false;
            this.printPreviewBarItem19.Id = 18;
            this.printPreviewBarItem19.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowLastPage;
            this.printPreviewBarItem19.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowLastPageLarge;
            this.printPreviewBarItem19.Name = "printPreviewBarItem19";
            superToolTip22.FixedTooltipWidth = true;
            toolTipTitleItem22.Text = "Last Page (Ctrl+End)";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "Navigate to the last page of the document.";
            superToolTip22.Items.Add(toolTipTitleItem22);
            superToolTip22.Items.Add(toolTipItem22);
            superToolTip22.MaxWidth = 210;
            this.printPreviewBarItem19.SuperTip = superToolTip22;
            // 
            // printPreviewBarItem20
            // 
            this.printPreviewBarItem20.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem20.Caption = "Many Pages";
            this.printPreviewBarItem20.Command = DevExpress.XtraPrinting.PrintingSystemCommand.MultiplePages;
            this.printPreviewBarItem20.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem20.Enabled = false;
            this.printPreviewBarItem20.Id = 19;
            this.printPreviewBarItem20.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_MultiplePages;
            this.printPreviewBarItem20.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_MultiplePagesLarge;
            this.printPreviewBarItem20.Name = "printPreviewBarItem20";
            superToolTip23.FixedTooltipWidth = true;
            toolTipTitleItem23.Text = "View Many Pages";
            toolTipItem23.LeftIndent = 6;
            toolTipItem23.Text = "Choose the page layout to arrange the document pages in preview.";
            superToolTip23.Items.Add(toolTipTitleItem23);
            superToolTip23.Items.Add(toolTipItem23);
            superToolTip23.MaxWidth = 210;
            this.printPreviewBarItem20.SuperTip = superToolTip23;
            // 
            // printPreviewBarItem21
            // 
            this.printPreviewBarItem21.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem21.Caption = "Page Color";
            this.printPreviewBarItem21.Command = DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground;
            this.printPreviewBarItem21.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem21.Enabled = false;
            this.printPreviewBarItem21.Id = 20;
            this.printPreviewBarItem21.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FillBackground;
            this.printPreviewBarItem21.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FillBackgroundLarge;
            this.printPreviewBarItem21.Name = "printPreviewBarItem21";
            superToolTip24.FixedTooltipWidth = true;
            toolTipTitleItem24.Text = "Background Color";
            toolTipItem24.LeftIndent = 6;
            toolTipItem24.Text = "Choose a color for the background of the document pages.";
            superToolTip24.Items.Add(toolTipTitleItem24);
            superToolTip24.Items.Add(toolTipItem24);
            superToolTip24.MaxWidth = 210;
            this.printPreviewBarItem21.SuperTip = superToolTip24;
            // 
            // printPreviewBarItem22
            // 
            this.printPreviewBarItem22.Caption = "Watermark";
            this.printPreviewBarItem22.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Watermark;
            this.printPreviewBarItem22.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem22.Enabled = false;
            this.printPreviewBarItem22.Id = 21;
            this.printPreviewBarItem22.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Watermark;
            this.printPreviewBarItem22.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_WatermarkLarge;
            this.printPreviewBarItem22.Name = "printPreviewBarItem22";
            superToolTip25.FixedTooltipWidth = true;
            toolTipTitleItem25.Text = "Watermark";
            toolTipItem25.LeftIndent = 6;
            toolTipItem25.Text = "Insert ghosted text or image behind the content of a page.\r\n\r\nThis is often used " +
    "to indicate that a document is to be treated specially.";
            superToolTip25.Items.Add(toolTipTitleItem25);
            superToolTip25.Items.Add(toolTipItem25);
            superToolTip25.MaxWidth = 210;
            this.printPreviewBarItem22.SuperTip = superToolTip25;
            // 
            // printPreviewBarItem23
            // 
            this.printPreviewBarItem23.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem23.Caption = "Export To";
            this.printPreviewBarItem23.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile;
            this.printPreviewBarItem23.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem23.Enabled = false;
            this.printPreviewBarItem23.Id = 22;
            this.printPreviewBarItem23.Name = "printPreviewBarItem23";
            superToolTip26.FixedTooltipWidth = true;
            toolTipTitleItem26.Text = "Export To...";
            toolTipItem26.LeftIndent = 6;
            toolTipItem26.Text = "Export the current document in one of the available formats, and save it to the f" +
    "ile on a disk.";
            superToolTip26.Items.Add(toolTipTitleItem26);
            superToolTip26.Items.Add(toolTipItem26);
            superToolTip26.MaxWidth = 210;
            this.printPreviewBarItem23.SuperTip = superToolTip26;
            // 
            // printPreviewBarItem24
            // 
            this.printPreviewBarItem24.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem24.Caption = "E-Mail As";
            this.printPreviewBarItem24.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendFile;
            this.printPreviewBarItem24.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem24.Enabled = false;
            this.printPreviewBarItem24.Id = 23;
            this.printPreviewBarItem24.Name = "printPreviewBarItem24";
            superToolTip27.FixedTooltipWidth = true;
            toolTipTitleItem27.Text = "E-Mail As...";
            toolTipItem27.LeftIndent = 6;
            toolTipItem27.Text = "Export the current document in one of the available formats, and attach it to the" +
    " e-mail.";
            superToolTip27.Items.Add(toolTipTitleItem27);
            superToolTip27.Items.Add(toolTipItem27);
            superToolTip27.MaxWidth = 210;
            this.printPreviewBarItem24.SuperTip = superToolTip27;
            // 
            // printPreviewBarItem25
            // 
            this.printPreviewBarItem25.Caption = "Close Print Preview";
            this.printPreviewBarItem25.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview;
            this.printPreviewBarItem25.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem25.Enabled = false;
            this.printPreviewBarItem25.Id = 24;
            this.printPreviewBarItem25.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreview;
            this.printPreviewBarItem25.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreviewLarge;
            this.printPreviewBarItem25.Name = "printPreviewBarItem25";
            superToolTip28.FixedTooltipWidth = true;
            toolTipTitleItem28.Text = "Close Print Preview";
            toolTipItem28.LeftIndent = 6;
            toolTipItem28.Text = "Close Print Preview of the document.";
            superToolTip28.Items.Add(toolTipTitleItem28);
            superToolTip28.Items.Add(toolTipItem28);
            superToolTip28.MaxWidth = 210;
            this.printPreviewBarItem25.SuperTip = superToolTip28;
            // 
            // printPreviewBarItem26
            // 
            this.printPreviewBarItem26.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem26.Caption = "Orientation";
            this.printPreviewBarItem26.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageOrientation;
            this.printPreviewBarItem26.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem26.Enabled = false;
            this.printPreviewBarItem26.Id = 25;
            this.printPreviewBarItem26.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageOrientation;
            this.printPreviewBarItem26.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageOrientationLarge;
            this.printPreviewBarItem26.Name = "printPreviewBarItem26";
            superToolTip29.FixedTooltipWidth = true;
            toolTipTitleItem29.Text = "Page Orientation";
            toolTipItem29.LeftIndent = 6;
            toolTipItem29.Text = "Switch the pages between portrait and landscape layouts.";
            superToolTip29.Items.Add(toolTipTitleItem29);
            superToolTip29.Items.Add(toolTipItem29);
            superToolTip29.MaxWidth = 210;
            this.printPreviewBarItem26.SuperTip = superToolTip29;
            // 
            // printPreviewBarItem27
            // 
            this.printPreviewBarItem27.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem27.Caption = "Size";
            this.printPreviewBarItem27.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PaperSize;
            this.printPreviewBarItem27.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem27.Enabled = false;
            this.printPreviewBarItem27.Id = 26;
            this.printPreviewBarItem27.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PaperSize;
            this.printPreviewBarItem27.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PaperSizeLarge;
            this.printPreviewBarItem27.Name = "printPreviewBarItem27";
            superToolTip30.FixedTooltipWidth = true;
            toolTipTitleItem30.Text = "Page Size";
            toolTipItem30.LeftIndent = 6;
            toolTipItem30.Text = "Choose the paper size of the document.";
            superToolTip30.Items.Add(toolTipTitleItem30);
            superToolTip30.Items.Add(toolTipItem30);
            superToolTip30.MaxWidth = 210;
            this.printPreviewBarItem27.SuperTip = superToolTip30;
            // 
            // printPreviewBarItem28
            // 
            this.printPreviewBarItem28.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem28.Caption = "Margins";
            this.printPreviewBarItem28.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageMargins;
            this.printPreviewBarItem28.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem28.Enabled = false;
            this.printPreviewBarItem28.Id = 27;
            this.printPreviewBarItem28.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageMargins;
            this.printPreviewBarItem28.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageMarginsLarge;
            this.printPreviewBarItem28.Name = "printPreviewBarItem28";
            superToolTip31.FixedTooltipWidth = true;
            toolTipTitleItem31.Text = "Page Margins";
            toolTipItem31.LeftIndent = 6;
            toolTipItem31.Text = "Select the margin sizes for the entire document.\r\n\r\nTo apply specific margin size" +
    "s to the document, click Custom Margins.";
            superToolTip31.Items.Add(toolTipTitleItem31);
            superToolTip31.Items.Add(toolTipItem31);
            superToolTip31.MaxWidth = 210;
            this.printPreviewBarItem28.SuperTip = superToolTip31;
            // 
            // printPreviewBarItem29
            // 
            this.printPreviewBarItem29.Caption = "PDF File";
            this.printPreviewBarItem29.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf;
            this.printPreviewBarItem29.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem29.Description = "Adobe Portable Document Format";
            this.printPreviewBarItem29.Enabled = false;
            this.printPreviewBarItem29.Id = 28;
            this.printPreviewBarItem29.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendPdf;
            this.printPreviewBarItem29.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendPdfLarge;
            this.printPreviewBarItem29.Name = "printPreviewBarItem29";
            superToolTip32.FixedTooltipWidth = true;
            toolTipTitleItem32.Text = "E-Mail As PDF";
            toolTipItem32.LeftIndent = 6;
            toolTipItem32.Text = "Export the document to PDF and attach it to the e-mail.";
            superToolTip32.Items.Add(toolTipTitleItem32);
            superToolTip32.Items.Add(toolTipItem32);
            superToolTip32.MaxWidth = 210;
            this.printPreviewBarItem29.SuperTip = superToolTip32;
            // 
            // printPreviewBarItem30
            // 
            this.printPreviewBarItem30.Caption = "Text File";
            this.printPreviewBarItem30.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt;
            this.printPreviewBarItem30.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem30.Description = "Plain Text";
            this.printPreviewBarItem30.Enabled = false;
            this.printPreviewBarItem30.Id = 29;
            this.printPreviewBarItem30.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendTxt;
            this.printPreviewBarItem30.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendTxtLarge;
            this.printPreviewBarItem30.Name = "printPreviewBarItem30";
            superToolTip33.FixedTooltipWidth = true;
            toolTipTitleItem33.Text = "E-Mail As Text";
            toolTipItem33.LeftIndent = 6;
            toolTipItem33.Text = "Export the document to Text and attach it to the e-mail.";
            superToolTip33.Items.Add(toolTipTitleItem33);
            superToolTip33.Items.Add(toolTipItem33);
            superToolTip33.MaxWidth = 210;
            this.printPreviewBarItem30.SuperTip = superToolTip33;
            // 
            // printPreviewBarItem31
            // 
            this.printPreviewBarItem31.Caption = "CSV File";
            this.printPreviewBarItem31.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv;
            this.printPreviewBarItem31.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem31.Description = "Comma-Separated Values Text";
            this.printPreviewBarItem31.Enabled = false;
            this.printPreviewBarItem31.Id = 30;
            this.printPreviewBarItem31.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendCsv;
            this.printPreviewBarItem31.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendCsvLarge;
            this.printPreviewBarItem31.Name = "printPreviewBarItem31";
            superToolTip34.FixedTooltipWidth = true;
            toolTipTitleItem34.Text = "E-Mail As CSV";
            toolTipItem34.LeftIndent = 6;
            toolTipItem34.Text = "Export the document to CSV and attach it to the e-mail.";
            superToolTip34.Items.Add(toolTipTitleItem34);
            superToolTip34.Items.Add(toolTipItem34);
            superToolTip34.MaxWidth = 210;
            this.printPreviewBarItem31.SuperTip = superToolTip34;
            // 
            // printPreviewBarItem32
            // 
            this.printPreviewBarItem32.Caption = "MHT File";
            this.printPreviewBarItem32.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht;
            this.printPreviewBarItem32.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem32.Description = "Single File Web Page";
            this.printPreviewBarItem32.Enabled = false;
            this.printPreviewBarItem32.Id = 31;
            this.printPreviewBarItem32.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendMht;
            this.printPreviewBarItem32.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendMhtLarge;
            this.printPreviewBarItem32.Name = "printPreviewBarItem32";
            superToolTip35.FixedTooltipWidth = true;
            toolTipTitleItem35.Text = "E-Mail As MHT";
            toolTipItem35.LeftIndent = 6;
            toolTipItem35.Text = "Export the document to MHT and attach it to the e-mail.";
            superToolTip35.Items.Add(toolTipTitleItem35);
            superToolTip35.Items.Add(toolTipItem35);
            superToolTip35.MaxWidth = 210;
            this.printPreviewBarItem32.SuperTip = superToolTip35;
            // 
            // printPreviewBarItem33
            // 
            this.printPreviewBarItem33.Caption = "Excel File";
            this.printPreviewBarItem33.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls;
            this.printPreviewBarItem33.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem33.Description = "Microsoft Excel Workbook";
            this.printPreviewBarItem33.Enabled = false;
            this.printPreviewBarItem33.Id = 32;
            this.printPreviewBarItem33.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendXls;
            this.printPreviewBarItem33.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendXlsLarge;
            this.printPreviewBarItem33.Name = "printPreviewBarItem33";
            superToolTip36.FixedTooltipWidth = true;
            toolTipTitleItem36.Text = "E-Mail As XLS";
            toolTipItem36.LeftIndent = 6;
            toolTipItem36.Text = "Export the document to XLS and attach it to the e-mail.";
            superToolTip36.Items.Add(toolTipTitleItem36);
            superToolTip36.Items.Add(toolTipItem36);
            superToolTip36.MaxWidth = 210;
            this.printPreviewBarItem33.SuperTip = superToolTip36;
            // 
            // printPreviewBarItem34
            // 
            this.printPreviewBarItem34.Caption = "RTF File";
            this.printPreviewBarItem34.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf;
            this.printPreviewBarItem34.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem34.Description = "Rich Text Format";
            this.printPreviewBarItem34.Enabled = false;
            this.printPreviewBarItem34.Id = 33;
            this.printPreviewBarItem34.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendRtf;
            this.printPreviewBarItem34.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendRtfLarge;
            this.printPreviewBarItem34.Name = "printPreviewBarItem34";
            superToolTip37.FixedTooltipWidth = true;
            toolTipTitleItem37.Text = "E-Mail As RTF";
            toolTipItem37.LeftIndent = 6;
            toolTipItem37.Text = "Export the document to RTF and attach it to the e-mail.";
            superToolTip37.Items.Add(toolTipTitleItem37);
            superToolTip37.Items.Add(toolTipItem37);
            superToolTip37.MaxWidth = 210;
            this.printPreviewBarItem34.SuperTip = superToolTip37;
            // 
            // printPreviewBarItem35
            // 
            this.printPreviewBarItem35.Caption = "Image File";
            this.printPreviewBarItem35.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic;
            this.printPreviewBarItem35.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem35.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.printPreviewBarItem35.Enabled = false;
            this.printPreviewBarItem35.Id = 34;
            this.printPreviewBarItem35.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendGraphic;
            this.printPreviewBarItem35.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendGraphicLarge;
            this.printPreviewBarItem35.Name = "printPreviewBarItem35";
            superToolTip38.FixedTooltipWidth = true;
            toolTipTitleItem38.Text = "E-Mail As Image";
            toolTipItem38.LeftIndent = 6;
            toolTipItem38.Text = "Export the document to Image and attach it to the e-mail.";
            superToolTip38.Items.Add(toolTipTitleItem38);
            superToolTip38.Items.Add(toolTipItem38);
            superToolTip38.MaxWidth = 210;
            this.printPreviewBarItem35.SuperTip = superToolTip38;
            // 
            // printPreviewBarItem36
            // 
            this.printPreviewBarItem36.Caption = "PDF File";
            this.printPreviewBarItem36.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            this.printPreviewBarItem36.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem36.Description = "Adobe Portable Document Format";
            this.printPreviewBarItem36.Enabled = false;
            this.printPreviewBarItem36.Id = 35;
            this.printPreviewBarItem36.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportPdf;
            this.printPreviewBarItem36.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportPdfLarge;
            this.printPreviewBarItem36.Name = "printPreviewBarItem36";
            superToolTip39.FixedTooltipWidth = true;
            toolTipTitleItem39.Text = "Export to PDF";
            toolTipItem39.LeftIndent = 6;
            toolTipItem39.Text = "Export the document to PDF and save it to the file on a disk.";
            superToolTip39.Items.Add(toolTipTitleItem39);
            superToolTip39.Items.Add(toolTipItem39);
            superToolTip39.MaxWidth = 210;
            this.printPreviewBarItem36.SuperTip = superToolTip39;
            // 
            // printPreviewBarItem37
            // 
            this.printPreviewBarItem37.Caption = "HTML File";
            this.printPreviewBarItem37.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm;
            this.printPreviewBarItem37.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem37.Description = "Web Page";
            this.printPreviewBarItem37.Enabled = false;
            this.printPreviewBarItem37.Id = 36;
            this.printPreviewBarItem37.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportHtm;
            this.printPreviewBarItem37.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportHtmLarge;
            this.printPreviewBarItem37.Name = "printPreviewBarItem37";
            superToolTip40.FixedTooltipWidth = true;
            toolTipTitleItem40.Text = "Export to HTML";
            toolTipItem40.LeftIndent = 6;
            toolTipItem40.Text = "Export the document to HTML and save it to the file on a disk.";
            superToolTip40.Items.Add(toolTipTitleItem40);
            superToolTip40.Items.Add(toolTipItem40);
            superToolTip40.MaxWidth = 210;
            this.printPreviewBarItem37.SuperTip = superToolTip40;
            // 
            // printPreviewBarItem38
            // 
            this.printPreviewBarItem38.Caption = "Text File";
            this.printPreviewBarItem38.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt;
            this.printPreviewBarItem38.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem38.Description = "Plain Text";
            this.printPreviewBarItem38.Enabled = false;
            this.printPreviewBarItem38.Id = 37;
            this.printPreviewBarItem38.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportTxt;
            this.printPreviewBarItem38.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportTxtLarge;
            this.printPreviewBarItem38.Name = "printPreviewBarItem38";
            superToolTip41.FixedTooltipWidth = true;
            toolTipTitleItem41.Text = "Export to Text";
            toolTipItem41.LeftIndent = 6;
            toolTipItem41.Text = "Export the document to Text and save it to the file on a disk.";
            superToolTip41.Items.Add(toolTipTitleItem41);
            superToolTip41.Items.Add(toolTipItem41);
            superToolTip41.MaxWidth = 210;
            this.printPreviewBarItem38.SuperTip = superToolTip41;
            // 
            // printPreviewBarItem39
            // 
            this.printPreviewBarItem39.Caption = "CSV File";
            this.printPreviewBarItem39.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv;
            this.printPreviewBarItem39.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem39.Description = "Comma-Separated Values Text";
            this.printPreviewBarItem39.Enabled = false;
            this.printPreviewBarItem39.Id = 38;
            this.printPreviewBarItem39.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportCsv;
            this.printPreviewBarItem39.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportCsvLarge;
            this.printPreviewBarItem39.Name = "printPreviewBarItem39";
            superToolTip42.FixedTooltipWidth = true;
            toolTipTitleItem42.Text = "Export to CSV";
            toolTipItem42.LeftIndent = 6;
            toolTipItem42.Text = "Export the document to CSV and save it to the file on a disk.";
            superToolTip42.Items.Add(toolTipTitleItem42);
            superToolTip42.Items.Add(toolTipItem42);
            superToolTip42.MaxWidth = 210;
            this.printPreviewBarItem39.SuperTip = superToolTip42;
            // 
            // printPreviewBarItem40
            // 
            this.printPreviewBarItem40.Caption = "MHT File";
            this.printPreviewBarItem40.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht;
            this.printPreviewBarItem40.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem40.Description = "Single File Web Page";
            this.printPreviewBarItem40.Enabled = false;
            this.printPreviewBarItem40.Id = 39;
            this.printPreviewBarItem40.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportMht;
            this.printPreviewBarItem40.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportMhtLarge;
            this.printPreviewBarItem40.Name = "printPreviewBarItem40";
            superToolTip43.FixedTooltipWidth = true;
            toolTipTitleItem43.Text = "Export to MHT";
            toolTipItem43.LeftIndent = 6;
            toolTipItem43.Text = "Export the document to MHT and save it to the file on a disk.";
            superToolTip43.Items.Add(toolTipTitleItem43);
            superToolTip43.Items.Add(toolTipItem43);
            superToolTip43.MaxWidth = 210;
            this.printPreviewBarItem40.SuperTip = superToolTip43;
            // 
            // printPreviewBarItem41
            // 
            this.printPreviewBarItem41.Caption = "Excel File";
            this.printPreviewBarItem41.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls;
            this.printPreviewBarItem41.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem41.Description = "Microsoft Excel Workbook";
            this.printPreviewBarItem41.Enabled = false;
            this.printPreviewBarItem41.Id = 40;
            this.printPreviewBarItem41.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportXls;
            this.printPreviewBarItem41.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportXlsLarge;
            this.printPreviewBarItem41.Name = "printPreviewBarItem41";
            superToolTip44.FixedTooltipWidth = true;
            toolTipTitleItem44.Text = "Export to XLS";
            toolTipItem44.LeftIndent = 6;
            toolTipItem44.Text = "Export the document to XLS and save it to the file on a disk.";
            superToolTip44.Items.Add(toolTipTitleItem44);
            superToolTip44.Items.Add(toolTipItem44);
            superToolTip44.MaxWidth = 210;
            this.printPreviewBarItem41.SuperTip = superToolTip44;
            // 
            // printPreviewBarItem42
            // 
            this.printPreviewBarItem42.Caption = "RTF File";
            this.printPreviewBarItem42.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf;
            this.printPreviewBarItem42.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem42.Description = "Rich Text Format";
            this.printPreviewBarItem42.Enabled = false;
            this.printPreviewBarItem42.Id = 41;
            this.printPreviewBarItem42.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportRtf;
            this.printPreviewBarItem42.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportRtfLarge;
            this.printPreviewBarItem42.Name = "printPreviewBarItem42";
            superToolTip45.FixedTooltipWidth = true;
            toolTipTitleItem45.Text = "Export to RTF";
            toolTipItem45.LeftIndent = 6;
            toolTipItem45.Text = "Export the document to RTF and save it to the file on a disk.";
            superToolTip45.Items.Add(toolTipTitleItem45);
            superToolTip45.Items.Add(toolTipItem45);
            superToolTip45.MaxWidth = 210;
            this.printPreviewBarItem42.SuperTip = superToolTip45;
            // 
            // printPreviewBarItem43
            // 
            this.printPreviewBarItem43.Caption = "Image File";
            this.printPreviewBarItem43.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic;
            this.printPreviewBarItem43.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem43.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.printPreviewBarItem43.Enabled = false;
            this.printPreviewBarItem43.Id = 42;
            this.printPreviewBarItem43.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportGraphic;
            this.printPreviewBarItem43.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportGraphicLarge;
            this.printPreviewBarItem43.Name = "printPreviewBarItem43";
            superToolTip46.FixedTooltipWidth = true;
            toolTipTitleItem46.Text = "Export to Image";
            toolTipItem46.LeftIndent = 6;
            toolTipItem46.Text = "Export the document to Image and save it to the file on a disk.";
            superToolTip46.Items.Add(toolTipTitleItem46);
            superToolTip46.Items.Add(toolTipItem46);
            superToolTip46.MaxWidth = 210;
            this.printPreviewBarItem43.SuperTip = superToolTip46;
            // 
            // printPreviewBarItem44
            // 
            this.printPreviewBarItem44.Caption = "Open";
            this.printPreviewBarItem44.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Open;
            this.printPreviewBarItem44.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem44.Enabled = false;
            this.printPreviewBarItem44.Id = 43;
            this.printPreviewBarItem44.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Open;
            this.printPreviewBarItem44.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_OpenLarge;
            this.printPreviewBarItem44.Name = "printPreviewBarItem44";
            superToolTip47.FixedTooltipWidth = true;
            toolTipTitleItem47.Text = "Open (Ctrl + O)";
            toolTipItem47.LeftIndent = 6;
            toolTipItem47.Text = "Open a document.";
            superToolTip47.Items.Add(toolTipTitleItem47);
            superToolTip47.Items.Add(toolTipItem47);
            superToolTip47.MaxWidth = 210;
            this.printPreviewBarItem44.SuperTip = superToolTip47;
            // 
            // printPreviewBarItem45
            // 
            this.printPreviewBarItem45.Caption = "Save";
            this.printPreviewBarItem45.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Save;
            this.printPreviewBarItem45.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem45.Enabled = false;
            this.printPreviewBarItem45.Id = 44;
            this.printPreviewBarItem45.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.printPreviewBarItem45.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SaveLarge;
            this.printPreviewBarItem45.Name = "printPreviewBarItem45";
            superToolTip48.FixedTooltipWidth = true;
            toolTipTitleItem48.Text = "Save (Ctrl + S)";
            toolTipItem48.LeftIndent = 6;
            toolTipItem48.Text = "Save the document.";
            superToolTip48.Items.Add(toolTipTitleItem48);
            superToolTip48.Items.Add(toolTipItem48);
            superToolTip48.MaxWidth = 210;
            this.printPreviewBarItem45.SuperTip = superToolTip48;
            // 
            // printPreviewStaticItem1
            // 
            this.printPreviewStaticItem1.Caption = "Nothing";
            this.printPreviewStaticItem1.Id = 49;
            this.printPreviewStaticItem1.LeftIndent = 1;
            this.printPreviewStaticItem1.Name = "printPreviewStaticItem1";
            this.printPreviewStaticItem1.RightIndent = 1;
            this.printPreviewStaticItem1.Type = "PageOfPages";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 50;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // progressBarEditItem1
            // 
            this.progressBarEditItem1.ContextSpecifier = this.printRibbonController1;
            this.progressBarEditItem1.Edit = this.repositoryItemProgressBar1;
            this.progressBarEditItem1.EditHeight = 12;
            this.progressBarEditItem1.EditWidth = 150;
            this.progressBarEditItem1.Id = 51;
            this.progressBarEditItem1.Name = "progressBarEditItem1";
            this.progressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // printPreviewBarItem46
            // 
            this.printPreviewBarItem46.Caption = "Stop";
            this.printPreviewBarItem46.Command = DevExpress.XtraPrinting.PrintingSystemCommand.StopPageBuilding;
            this.printPreviewBarItem46.ContextSpecifier = this.printRibbonController1;
            this.printPreviewBarItem46.Enabled = false;
            this.printPreviewBarItem46.Hint = "Stop";
            this.printPreviewBarItem46.Id = 52;
            this.printPreviewBarItem46.Name = "printPreviewBarItem46";
            this.printPreviewBarItem46.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonItem1.Enabled = false;
            this.barButtonItem1.Id = 53;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // printPreviewStaticItem2
            // 
            this.printPreviewStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.printPreviewStaticItem2.Caption = "100%";
            this.printPreviewStaticItem2.Id = 54;
            this.printPreviewStaticItem2.Name = "printPreviewStaticItem2";
            this.printPreviewStaticItem2.Type = "ZoomFactorText";
            // 
            // zoomTrackBarEditItem1
            // 
            this.zoomTrackBarEditItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.zoomTrackBarEditItem1.ContextSpecifier = this.printRibbonController1;
            this.zoomTrackBarEditItem1.Edit = this.repositoryItemZoomTrackBar1;
            this.zoomTrackBarEditItem1.EditValue = 90;
            this.zoomTrackBarEditItem1.EditWidth = 140;
            this.zoomTrackBarEditItem1.Enabled = false;
            this.zoomTrackBarEditItem1.Id = 55;
            this.zoomTrackBarEditItem1.Name = "zoomTrackBarEditItem1";
            this.zoomTrackBarEditItem1.Range = new int[] {
        10,
        500};
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.AllowFocused = false;
            this.repositoryItemZoomTrackBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemZoomTrackBar1.Maximum = 180;
            this.repositoryItemZoomTrackBar1.Middle = 90;
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            // 
            // printPreviewRibbonPage1
            // 
            this.printPreviewRibbonPage1.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.printPreviewRibbonPageGroup1,
            this.printPreviewRibbonPageGroup2,
            this.printPreviewRibbonPageGroup3,
            this.printPreviewRibbonPageGroup4,
            this.printPreviewRibbonPageGroup5,
            this.printPreviewRibbonPageGroup6,
            this.printPreviewRibbonPageGroup7});
            this.printPreviewRibbonPage1.Name = "printPreviewRibbonPage1";
            this.printPreviewRibbonPage1.Text = "Print Preview";
            // 
            // printPreviewRibbonPageGroup1
            // 
            this.printPreviewRibbonPageGroup1.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup1.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Document;
            this.printPreviewRibbonPageGroup1.ItemLinks.Add(this.printPreviewBarItem44);
            this.printPreviewRibbonPageGroup1.ItemLinks.Add(this.printPreviewBarItem45);
            this.printPreviewRibbonPageGroup1.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Document;
            this.printPreviewRibbonPageGroup1.Name = "printPreviewRibbonPageGroup1";
            this.printPreviewRibbonPageGroup1.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup1.Text = "Document";
            // 
            // printPreviewRibbonPageGroup2
            // 
            this.printPreviewRibbonPageGroup2.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup2.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem5);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem6);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem4);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem2);
            this.printPreviewRibbonPageGroup2.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Print;
            this.printPreviewRibbonPageGroup2.Name = "printPreviewRibbonPageGroup2";
            this.printPreviewRibbonPageGroup2.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup2.Text = "Print";
            // 
            // printPreviewRibbonPageGroup3
            // 
            this.printPreviewRibbonPageGroup3.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup3.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageMargins;
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem8);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem9);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem28);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem26);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem27);
            this.printPreviewRibbonPageGroup3.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.PageSetup;
            this.printPreviewRibbonPageGroup3.Name = "printPreviewRibbonPageGroup3";
            superToolTip49.FixedTooltipWidth = true;
            toolTipTitleItem49.Text = "Page Setup";
            toolTipItem49.Appearance.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageSetupDialog;
            toolTipItem49.Appearance.Options.UseImage = true;
            toolTipItem49.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageSetupDialog;
            toolTipItem49.LeftIndent = 6;
            toolTipItem49.Text = "Show the Page Setup dialog.";
            superToolTip49.Items.Add(toolTipTitleItem49);
            superToolTip49.Items.Add(toolTipItem49);
            superToolTip49.MaxWidth = 318;
            this.printPreviewRibbonPageGroup3.SuperTip = superToolTip49;
            this.printPreviewRibbonPageGroup3.Text = "Page Setup";
            // 
            // printPreviewRibbonPageGroup4
            // 
            this.printPreviewRibbonPageGroup4.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup4.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Find;
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem3);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem1);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem16, true);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem17);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem18);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem19);
            this.printPreviewRibbonPageGroup4.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Navigation;
            this.printPreviewRibbonPageGroup4.Name = "printPreviewRibbonPageGroup4";
            this.printPreviewRibbonPageGroup4.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup4.Text = "Navigation";
            // 
            // printPreviewRibbonPageGroup5
            // 
            this.printPreviewRibbonPageGroup5.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup5.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Zoom;
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem10);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem11);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem12);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem20);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem13);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem15);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem14);
            this.printPreviewRibbonPageGroup5.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Zoom;
            this.printPreviewRibbonPageGroup5.Name = "printPreviewRibbonPageGroup5";
            this.printPreviewRibbonPageGroup5.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup5.Text = "Zoom";
            // 
            // printPreviewRibbonPageGroup6
            // 
            this.printPreviewRibbonPageGroup6.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup6.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Watermark;
            this.printPreviewRibbonPageGroup6.ItemLinks.Add(this.printPreviewBarItem21);
            this.printPreviewRibbonPageGroup6.ItemLinks.Add(this.printPreviewBarItem22);
            this.printPreviewRibbonPageGroup6.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Background;
            this.printPreviewRibbonPageGroup6.Name = "printPreviewRibbonPageGroup6";
            this.printPreviewRibbonPageGroup6.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup6.Text = "Page Background";
            // 
            // printPreviewRibbonPageGroup7
            // 
            this.printPreviewRibbonPageGroup7.ContextSpecifier = this.printRibbonController1;
            this.printPreviewRibbonPageGroup7.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportFile;
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem23);
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem24);
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem25, true);
            this.printPreviewRibbonPageGroup7.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Export;
            this.printPreviewRibbonPageGroup7.Name = "printPreviewRibbonPageGroup7";
            this.printPreviewRibbonPageGroup7.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup7.Text = "Export";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.printPreviewStaticItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem1, true);
            this.ribbonStatusBar1.ItemLinks.Add(this.progressBarEditItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.printPreviewBarItem46);
            this.ribbonStatusBar1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.printPreviewStaticItem2);
            this.ribbonStatusBar1.ItemLinks.Add(this.zoomTrackBarEditItem1);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 695);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1102, 23);
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("ef9f3537-823d-4b91-84cb-b30d9588a2ef");
            this.dockPanel1.Location = new System.Drawing.Point(0, 142);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(444, 200);
            this.dockPanel1.Size = new System.Drawing.Size(444, 553);
            this.dockPanel1.Text = "Data Supply";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.standaloneBarDockControl1);
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(437, 521);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(440, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.Controls.Add(this.popupContainerEdit2);
            this.layoutControl2.Controls.Add(this.gridControl1);
            this.layoutControl2.Controls.Add(this.popupContainerEdit1);
            this.layoutControl2.Location = new System.Drawing.Point(0, 42);
            this.layoutControl2.MenuManager = this.barManager1;
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(807, 163, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(439, 479);
            this.layoutControl2.TabIndex = 5;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Linked Data";
            this.popupContainerEdit2.Location = new System.Drawing.Point(101, 2);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControl3;
            this.popupContainerEdit2.Properties.PopupSizeable = false;
            this.popupContainerEdit2.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit2.Size = new System.Drawing.Size(336, 20);
            this.popupContainerEdit2.StyleController = this.layoutControl2;
            this.popupContainerEdit2.TabIndex = 11;
            this.popupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit2_QueryResultValue);
            // 
            // popupContainerControl3
            // 
            this.popupContainerControl3.Controls.Add(this.groupControl2);
            this.popupContainerControl3.Controls.Add(this.btnShowLinkedDataOK);
            this.popupContainerControl3.Controls.Add(this.groupControl1);
            this.popupContainerControl3.Location = new System.Drawing.Point(765, 403);
            this.popupContainerControl3.Name = "popupContainerControl3";
            this.popupContainerControl3.Size = new System.Drawing.Size(193, 268);
            this.popupContainerControl3.TabIndex = 30;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.deFromDate);
            this.groupControl2.Controls.Add(this.deToDate);
            this.groupControl2.Location = new System.Drawing.Point(3, 163);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(187, 76);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "Date Filter";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(7, 54);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "From:";
            // 
            // deFromDate
            // 
            this.deFromDate.EditValue = null;
            this.deFromDate.Location = new System.Drawing.Point(50, 25);
            this.deFromDate.Name = "deFromDate";
            this.deFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deFromDate.Properties.NullText = "Not Used";
            this.deFromDate.Size = new System.Drawing.Size(130, 20);
            superToolTip50.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem50.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem50.Appearance.Options.UseImage = true;
            toolTipTitleItem50.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem50.Text = "From Date - Information";
            toolTipItem50.LeftIndent = 6;
            toolTipItem50.Text = "All gritting and snow clearance records with a <b>Creation Date</b> which fall be" +
    "tween the From Date and To Date will be returned.\r\n";
            superToolTip50.Items.Add(toolTipTitleItem50);
            superToolTip50.Items.Add(toolTipItem50);
            this.deFromDate.SuperTip = superToolTip50;
            this.deFromDate.TabIndex = 10;
            // 
            // deToDate
            // 
            this.deToDate.EditValue = null;
            this.deToDate.Location = new System.Drawing.Point(50, 51);
            this.deToDate.Name = "deToDate";
            this.deToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deToDate.Properties.NullText = "Not Used";
            this.deToDate.Size = new System.Drawing.Size(130, 20);
            superToolTip51.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem51.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem51.Appearance.Options.UseImage = true;
            toolTipTitleItem51.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem51.Text = "To Date - Information";
            toolTipItem51.LeftIndent = 6;
            toolTipItem51.Text = "All gritting and snow clearance records with a <b>Creation Date</b> which fall be" +
    "tween the From Date and To Date will be returned.\r\n";
            superToolTip51.Items.Add(toolTipTitleItem51);
            superToolTip51.Items.Add(toolTipItem51);
            this.deToDate.SuperTip = superToolTip51;
            this.deToDate.TabIndex = 7;
            // 
            // btnShowLinkedDataOK
            // 
            this.btnShowLinkedDataOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnShowLinkedDataOK.Location = new System.Drawing.Point(3, 242);
            this.btnShowLinkedDataOK.Name = "btnShowLinkedDataOK";
            this.btnShowLinkedDataOK.Size = new System.Drawing.Size(75, 23);
            this.btnShowLinkedDataOK.TabIndex = 2;
            this.btnShowLinkedDataOK.Text = "OK";
            this.btnShowLinkedDataOK.Click += new System.EventHandler(this.btnShowLinkedDataOK_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.checkEditSnowClearanceCalloutsRates);
            this.groupControl1.Controls.Add(this.checkEditSnowClearanceCalloutsExtraCosts);
            this.groupControl1.Controls.Add(this.checkEditGrittingCalloutsImages);
            this.groupControl1.Controls.Add(this.checkEditGrittingCalloutsExtraCosts);
            this.groupControl1.Controls.Add(this.checkEditSnowClearanceCallouts);
            this.groupControl1.Controls.Add(this.checkEditGrittingCallouts);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(187, 155);
            this.groupControl1.TabIndex = 9;
            this.groupControl1.Text = "Show Linked Data";
            // 
            // checkEditSnowClearanceCalloutsRates
            // 
            this.checkEditSnowClearanceCalloutsRates.Enabled = false;
            this.checkEditSnowClearanceCalloutsRates.Location = new System.Drawing.Point(23, 133);
            this.checkEditSnowClearanceCalloutsRates.MenuManager = this.barManager1;
            this.checkEditSnowClearanceCalloutsRates.Name = "checkEditSnowClearanceCalloutsRates";
            this.checkEditSnowClearanceCalloutsRates.Properties.Caption = "Linked Rates:";
            this.checkEditSnowClearanceCalloutsRates.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditSnowClearanceCalloutsRates.Size = new System.Drawing.Size(157, 19);
            superToolTip52.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem52.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem52.Appearance.Options.UseImage = true;
            toolTipTitleItem52.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem52.Text = "Load Linked Data - Information";
            toolTipItem52.LeftIndent = 6;
            toolTipItem52.Text = resources.GetString("toolTipItem52.Text");
            superToolTip52.Items.Add(toolTipTitleItem52);
            superToolTip52.Items.Add(toolTipItem52);
            this.checkEditSnowClearanceCalloutsRates.SuperTip = superToolTip52;
            this.checkEditSnowClearanceCalloutsRates.TabIndex = 13;
            // 
            // checkEditSnowClearanceCalloutsExtraCosts
            // 
            this.checkEditSnowClearanceCalloutsExtraCosts.Enabled = false;
            this.checkEditSnowClearanceCalloutsExtraCosts.Location = new System.Drawing.Point(23, 113);
            this.checkEditSnowClearanceCalloutsExtraCosts.MenuManager = this.barManager1;
            this.checkEditSnowClearanceCalloutsExtraCosts.Name = "checkEditSnowClearanceCalloutsExtraCosts";
            this.checkEditSnowClearanceCalloutsExtraCosts.Properties.Caption = "Linked Extra Costs:";
            this.checkEditSnowClearanceCalloutsExtraCosts.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditSnowClearanceCalloutsExtraCosts.Size = new System.Drawing.Size(157, 19);
            superToolTip53.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem53.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem53.Appearance.Options.UseImage = true;
            toolTipTitleItem53.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem53.Text = "Load Linked Data - Information";
            toolTipItem53.LeftIndent = 6;
            toolTipItem53.Text = resources.GetString("toolTipItem53.Text");
            superToolTip53.Items.Add(toolTipTitleItem53);
            superToolTip53.Items.Add(toolTipItem53);
            this.checkEditSnowClearanceCalloutsExtraCosts.SuperTip = superToolTip53;
            this.checkEditSnowClearanceCalloutsExtraCosts.TabIndex = 12;
            // 
            // checkEditGrittingCalloutsImages
            // 
            this.checkEditGrittingCalloutsImages.Enabled = false;
            this.checkEditGrittingCalloutsImages.Location = new System.Drawing.Point(23, 65);
            this.checkEditGrittingCalloutsImages.MenuManager = this.barManager1;
            this.checkEditGrittingCalloutsImages.Name = "checkEditGrittingCalloutsImages";
            this.checkEditGrittingCalloutsImages.Properties.Caption = "Linked Images:";
            this.checkEditGrittingCalloutsImages.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditGrittingCalloutsImages.Size = new System.Drawing.Size(157, 19);
            superToolTip54.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem54.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem54.Appearance.Options.UseImage = true;
            toolTipTitleItem54.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem54.Text = "Load Linked Data - Information";
            toolTipItem54.LeftIndent = 6;
            toolTipItem54.Text = resources.GetString("toolTipItem54.Text");
            superToolTip54.Items.Add(toolTipTitleItem54);
            superToolTip54.Items.Add(toolTipItem54);
            this.checkEditGrittingCalloutsImages.SuperTip = superToolTip54;
            this.checkEditGrittingCalloutsImages.TabIndex = 11;
            // 
            // checkEditGrittingCalloutsExtraCosts
            // 
            this.checkEditGrittingCalloutsExtraCosts.Enabled = false;
            this.checkEditGrittingCalloutsExtraCosts.Location = new System.Drawing.Point(23, 45);
            this.checkEditGrittingCalloutsExtraCosts.MenuManager = this.barManager1;
            this.checkEditGrittingCalloutsExtraCosts.Name = "checkEditGrittingCalloutsExtraCosts";
            this.checkEditGrittingCalloutsExtraCosts.Properties.Caption = "Linked Extra Costs:";
            this.checkEditGrittingCalloutsExtraCosts.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditGrittingCalloutsExtraCosts.Size = new System.Drawing.Size(157, 19);
            superToolTip55.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem55.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem55.Appearance.Options.UseImage = true;
            toolTipTitleItem55.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem55.Text = "Load Linked Data - Information";
            toolTipItem55.LeftIndent = 6;
            toolTipItem55.Text = resources.GetString("toolTipItem55.Text");
            superToolTip55.Items.Add(toolTipTitleItem55);
            superToolTip55.Items.Add(toolTipItem55);
            this.checkEditGrittingCalloutsExtraCosts.SuperTip = superToolTip55;
            this.checkEditGrittingCalloutsExtraCosts.TabIndex = 10;
            // 
            // checkEditSnowClearanceCallouts
            // 
            this.checkEditSnowClearanceCallouts.Location = new System.Drawing.Point(5, 93);
            this.checkEditSnowClearanceCallouts.MenuManager = this.barManager1;
            this.checkEditSnowClearanceCallouts.Name = "checkEditSnowClearanceCallouts";
            this.checkEditSnowClearanceCallouts.Properties.Caption = "Linked Snow Clearance Callouts:";
            this.checkEditSnowClearanceCallouts.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditSnowClearanceCallouts.Size = new System.Drawing.Size(175, 19);
            superToolTip56.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem56.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem56.Appearance.Options.UseImage = true;
            toolTipTitleItem56.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem56.Text = "Load Linked Data - Information";
            toolTipItem56.LeftIndent = 6;
            toolTipItem56.Text = resources.GetString("toolTipItem56.Text");
            superToolTip56.Items.Add(toolTipTitleItem56);
            superToolTip56.Items.Add(toolTipItem56);
            this.checkEditSnowClearanceCallouts.SuperTip = superToolTip56;
            this.checkEditSnowClearanceCallouts.TabIndex = 9;
            this.checkEditSnowClearanceCallouts.CheckedChanged += new System.EventHandler(this.checkEditSnowClearanceCallouts_CheckedChanged);
            // 
            // checkEditGrittingCallouts
            // 
            this.checkEditGrittingCallouts.Location = new System.Drawing.Point(5, 25);
            this.checkEditGrittingCallouts.MenuManager = this.barManager1;
            this.checkEditGrittingCallouts.Name = "checkEditGrittingCallouts";
            this.checkEditGrittingCallouts.Properties.Caption = "Linked Gritting Callouts:";
            this.checkEditGrittingCallouts.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditGrittingCallouts.Size = new System.Drawing.Size(175, 19);
            superToolTip57.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem57.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem57.Appearance.Options.UseImage = true;
            toolTipTitleItem57.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem57.Text = "Load Linked Data - Information";
            toolTipItem57.LeftIndent = 6;
            toolTipItem57.Text = resources.GetString("toolTipItem57.Text");
            superToolTip57.Items.Add(toolTipTitleItem57);
            superToolTip57.Items.Add(toolTipItem57);
            this.checkEditGrittingCallouts.SuperTip = superToolTip57;
            this.checkEditGrittingCallouts.TabIndex = 8;
            this.checkEditGrittingCallouts.CheckedChanged += new System.EventHandler(this.checkEditGrittingCallouts_CheckedChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp04295GCReportsSiteListBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(2, 50);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit2});
            this.gridControl1.Size = new System.Drawing.Size(435, 427);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04295GCReportsSiteListBindingSource
            // 
            this.sp04295GCReportsSiteListBindingSource.DataMember = "sp04295_GC_Reports_Site_List";
            this.sp04295GCReportsSiteListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.colSiteID,
            this.gridColumn3,
            this.gridColumn4,
            this.colSiteCode,
            this.colSiteName,
            this.colSiteTypeDescription,
            this.colSiteTypeID,
            this.colSiteAddressLine1,
            this.colSiteAddressLine2,
            this.colSiteAddressLine3,
            this.colSiteAddressLine4,
            this.colSiteAddressLine5,
            this.colSitePostcode,
            this.colSiteTelephone,
            this.colSiteMobile,
            this.colSiteFax,
            this.colSiteEmail,
            this.colXCoordinate,
            this.colYCoordinate,
            this.colClientRemarks,
            this.colSiteRemarks,
            this.colContactPerson,
            this.colContactPersonPosition,
            this.colMappingWorkspaceID,
            this.colWorkspaceName,
            this.colCompanyCode,
            this.colCompanyName,
            this.colSiteOrder,
            this.colCompanyID,
            this.colHubID,
            this.colGrittingSite,
            this.colClientsSiteCode,
            this.colClientsSiteID,
            this.colIsSnowClearanceSite,
            this.colLinkedGrittingContractCount,
            this.colLinkedSnowClearanceContractCount});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 513, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Client ID";
            this.gridColumn2.FieldName = "ClientID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 64;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Width = 56;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Client Name";
            this.gridColumn3.FieldName = "ClientName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 227;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Client Code";
            this.gridColumn4.FieldName = "ClientCode";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 1;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 243;
            // 
            // colSiteTypeDescription
            // 
            this.colSiteTypeDescription.Caption = "Site Type";
            this.colSiteTypeDescription.FieldName = "SiteTypeDescription";
            this.colSiteTypeDescription.Name = "colSiteTypeDescription";
            this.colSiteTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSiteTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSiteTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSiteTypeDescription.Visible = true;
            this.colSiteTypeDescription.VisibleIndex = 5;
            this.colSiteTypeDescription.Width = 101;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            this.colSiteTypeID.Width = 80;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 14;
            this.colSiteAddressLine1.Width = 111;
            // 
            // colSiteAddressLine2
            // 
            this.colSiteAddressLine2.Caption = "Address Line 2";
            this.colSiteAddressLine2.FieldName = "SiteAddressLine2";
            this.colSiteAddressLine2.Name = "colSiteAddressLine2";
            this.colSiteAddressLine2.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine2.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine2.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine2.Visible = true;
            this.colSiteAddressLine2.VisibleIndex = 15;
            this.colSiteAddressLine2.Width = 107;
            // 
            // colSiteAddressLine3
            // 
            this.colSiteAddressLine3.Caption = "Address Line 3";
            this.colSiteAddressLine3.FieldName = "SiteAddressLine3";
            this.colSiteAddressLine3.Name = "colSiteAddressLine3";
            this.colSiteAddressLine3.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine3.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine3.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine3.Visible = true;
            this.colSiteAddressLine3.VisibleIndex = 16;
            this.colSiteAddressLine3.Width = 106;
            // 
            // colSiteAddressLine4
            // 
            this.colSiteAddressLine4.Caption = "Address Line 4";
            this.colSiteAddressLine4.FieldName = "SiteAddressLine4";
            this.colSiteAddressLine4.Name = "colSiteAddressLine4";
            this.colSiteAddressLine4.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine4.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine4.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine4.Visible = true;
            this.colSiteAddressLine4.VisibleIndex = 17;
            this.colSiteAddressLine4.Width = 108;
            // 
            // colSiteAddressLine5
            // 
            this.colSiteAddressLine5.Caption = "Address Line 5";
            this.colSiteAddressLine5.FieldName = "SiteAddressLine5";
            this.colSiteAddressLine5.Name = "colSiteAddressLine5";
            this.colSiteAddressLine5.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine5.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine5.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine5.Visible = true;
            this.colSiteAddressLine5.VisibleIndex = 18;
            this.colSiteAddressLine5.Width = 104;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Visible = true;
            this.colSitePostcode.VisibleIndex = 19;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Telephone";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Visible = true;
            this.colSiteTelephone.VisibleIndex = 20;
            this.colSiteTelephone.Width = 92;
            // 
            // colSiteMobile
            // 
            this.colSiteMobile.Caption = "Mobile";
            this.colSiteMobile.FieldName = "SiteMobile";
            this.colSiteMobile.Name = "colSiteMobile";
            this.colSiteMobile.OptionsColumn.AllowEdit = false;
            this.colSiteMobile.OptionsColumn.AllowFocus = false;
            this.colSiteMobile.OptionsColumn.ReadOnly = true;
            this.colSiteMobile.Visible = true;
            this.colSiteMobile.VisibleIndex = 21;
            this.colSiteMobile.Width = 92;
            // 
            // colSiteFax
            // 
            this.colSiteFax.Caption = "Fax";
            this.colSiteFax.FieldName = "SiteFax";
            this.colSiteFax.Name = "colSiteFax";
            this.colSiteFax.OptionsColumn.AllowEdit = false;
            this.colSiteFax.OptionsColumn.AllowFocus = false;
            this.colSiteFax.OptionsColumn.ReadOnly = true;
            this.colSiteFax.Visible = true;
            this.colSiteFax.VisibleIndex = 22;
            this.colSiteFax.Width = 99;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Email";
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.AllowEdit = false;
            this.colSiteEmail.OptionsColumn.AllowFocus = false;
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            this.colSiteEmail.Visible = true;
            this.colSiteEmail.VisibleIndex = 23;
            this.colSiteEmail.Width = 150;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            this.colXCoordinate.Width = 83;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            this.colYCoordinate.Width = 89;
            // 
            // colClientRemarks
            // 
            this.colClientRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colClientRemarks.FieldName = "ClientRemarks";
            this.colClientRemarks.Name = "colClientRemarks";
            this.colClientRemarks.OptionsColumn.ReadOnly = true;
            this.colClientRemarks.Visible = true;
            this.colClientRemarks.VisibleIndex = 25;
            this.colClientRemarks.Width = 100;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colSiteRemarks
            // 
            this.colSiteRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteRemarks.FieldName = "SiteRemarks";
            this.colSiteRemarks.Name = "colSiteRemarks";
            this.colSiteRemarks.OptionsColumn.ReadOnly = true;
            this.colSiteRemarks.Visible = true;
            this.colSiteRemarks.VisibleIndex = 26;
            this.colSiteRemarks.Width = 91;
            // 
            // colContactPerson
            // 
            this.colContactPerson.Caption = "Contact Person";
            this.colContactPerson.FieldName = "ContactPerson";
            this.colContactPerson.Name = "colContactPerson";
            this.colContactPerson.OptionsColumn.AllowEdit = false;
            this.colContactPerson.OptionsColumn.AllowFocus = false;
            this.colContactPerson.OptionsColumn.ReadOnly = true;
            this.colContactPerson.Visible = true;
            this.colContactPerson.VisibleIndex = 11;
            this.colContactPerson.Width = 105;
            // 
            // colContactPersonPosition
            // 
            this.colContactPersonPosition.Caption = "Contact Person Position";
            this.colContactPersonPosition.FieldName = "ContactPersonPosition";
            this.colContactPersonPosition.Name = "colContactPersonPosition";
            this.colContactPersonPosition.OptionsColumn.AllowEdit = false;
            this.colContactPersonPosition.OptionsColumn.AllowFocus = false;
            this.colContactPersonPosition.OptionsColumn.ReadOnly = true;
            this.colContactPersonPosition.Visible = true;
            this.colContactPersonPosition.VisibleIndex = 12;
            this.colContactPersonPosition.Width = 138;
            // 
            // colMappingWorkspaceID
            // 
            this.colMappingWorkspaceID.Caption = "Mapping Workspace ID";
            this.colMappingWorkspaceID.FieldName = "MappingWorkspaceID";
            this.colMappingWorkspaceID.Name = "colMappingWorkspaceID";
            this.colMappingWorkspaceID.OptionsColumn.AllowEdit = false;
            this.colMappingWorkspaceID.OptionsColumn.AllowFocus = false;
            this.colMappingWorkspaceID.OptionsColumn.ReadOnly = true;
            this.colMappingWorkspaceID.Width = 135;
            // 
            // colWorkspaceName
            // 
            this.colWorkspaceName.Caption = "Mapping Workspace";
            this.colWorkspaceName.FieldName = "WorkspaceName";
            this.colWorkspaceName.Name = "colWorkspaceName";
            this.colWorkspaceName.OptionsColumn.AllowEdit = false;
            this.colWorkspaceName.OptionsColumn.AllowFocus = false;
            this.colWorkspaceName.OptionsColumn.ReadOnly = true;
            this.colWorkspaceName.Visible = true;
            this.colWorkspaceName.VisibleIndex = 24;
            this.colWorkspaceName.Width = 165;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 4;
            this.colCompanyName.Width = 120;
            // 
            // colSiteOrder
            // 
            this.colSiteOrder.Caption = "Site Order";
            this.colSiteOrder.FieldName = "SiteOrder";
            this.colSiteOrder.Name = "colSiteOrder";
            this.colSiteOrder.OptionsColumn.AllowEdit = false;
            this.colSiteOrder.OptionsColumn.AllowFocus = false;
            this.colSiteOrder.OptionsColumn.ReadOnly = true;
            this.colSiteOrder.Visible = true;
            this.colSiteOrder.VisibleIndex = 13;
            this.colSiteOrder.Width = 85;
            // 
            // colCompanyID
            // 
            this.colCompanyID.CustomizationCaption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            // 
            // colHubID
            // 
            this.colHubID.CustomizationCaption = "Hub ID";
            this.colHubID.FieldName = "HubID";
            this.colHubID.Name = "colHubID";
            this.colHubID.OptionsColumn.AllowEdit = false;
            this.colHubID.OptionsColumn.AllowFocus = false;
            this.colHubID.OptionsColumn.ReadOnly = true;
            this.colHubID.Visible = true;
            this.colHubID.VisibleIndex = 2;
            // 
            // colGrittingSite
            // 
            this.colGrittingSite.Caption = "Gritting";
            this.colGrittingSite.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colGrittingSite.FieldName = "GrittingSite";
            this.colGrittingSite.Name = "colGrittingSite";
            this.colGrittingSite.OptionsColumn.AllowEdit = false;
            this.colGrittingSite.OptionsColumn.AllowFocus = false;
            this.colGrittingSite.OptionsColumn.ReadOnly = true;
            this.colGrittingSite.Visible = true;
            this.colGrittingSite.VisibleIndex = 7;
            this.colGrittingSite.Width = 57;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Visible = true;
            this.colClientsSiteCode.VisibleIndex = 6;
            this.colClientsSiteCode.Width = 113;
            // 
            // colClientsSiteID
            // 
            this.colClientsSiteID.Caption = "Clients Site ID";
            this.colClientsSiteID.FieldName = "ClientsSiteID";
            this.colClientsSiteID.Name = "colClientsSiteID";
            this.colClientsSiteID.OptionsColumn.AllowEdit = false;
            this.colClientsSiteID.OptionsColumn.AllowFocus = false;
            this.colClientsSiteID.OptionsColumn.ReadOnly = true;
            this.colClientsSiteID.Visible = true;
            this.colClientsSiteID.VisibleIndex = 3;
            this.colClientsSiteID.Width = 88;
            // 
            // colIsSnowClearanceSite
            // 
            this.colIsSnowClearanceSite.Caption = "Snow Clearance";
            this.colIsSnowClearanceSite.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsSnowClearanceSite.FieldName = "IsSnowClearanceSite";
            this.colIsSnowClearanceSite.Name = "colIsSnowClearanceSite";
            this.colIsSnowClearanceSite.OptionsColumn.AllowEdit = false;
            this.colIsSnowClearanceSite.OptionsColumn.AllowFocus = false;
            this.colIsSnowClearanceSite.OptionsColumn.ReadOnly = true;
            this.colIsSnowClearanceSite.Visible = true;
            this.colIsSnowClearanceSite.VisibleIndex = 8;
            this.colIsSnowClearanceSite.Width = 98;
            // 
            // colLinkedGrittingContractCount
            // 
            this.colLinkedGrittingContractCount.Caption = "Active Grit Contract";
            this.colLinkedGrittingContractCount.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colLinkedGrittingContractCount.FieldName = "LinkedGrittingContractCount";
            this.colLinkedGrittingContractCount.Name = "colLinkedGrittingContractCount";
            this.colLinkedGrittingContractCount.OptionsColumn.AllowEdit = false;
            this.colLinkedGrittingContractCount.OptionsColumn.AllowFocus = false;
            this.colLinkedGrittingContractCount.OptionsColumn.ReadOnly = true;
            this.colLinkedGrittingContractCount.Visible = true;
            this.colLinkedGrittingContractCount.VisibleIndex = 9;
            this.colLinkedGrittingContractCount.Width = 116;
            // 
            // colLinkedSnowClearanceContractCount
            // 
            this.colLinkedSnowClearanceContractCount.Caption = "Active Snow Clearance Contract";
            this.colLinkedSnowClearanceContractCount.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colLinkedSnowClearanceContractCount.FieldName = "LinkedSnowClearanceContractCount";
            this.colLinkedSnowClearanceContractCount.Name = "colLinkedSnowClearanceContractCount";
            this.colLinkedSnowClearanceContractCount.OptionsColumn.AllowEdit = false;
            this.colLinkedSnowClearanceContractCount.OptionsColumn.AllowFocus = false;
            this.colLinkedSnowClearanceContractCount.OptionsColumn.ReadOnly = true;
            this.colLinkedSnowClearanceContractCount.Visible = true;
            this.colLinkedSnowClearanceContractCount.VisibleIndex = 10;
            this.colLinkedSnowClearanceContractCount.Width = 176;
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "No Report Layout Selected";
            this.popupContainerEdit1.Location = new System.Drawing.Point(101, 26);
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControl1;
            this.popupContainerEdit1.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit1.Size = new System.Drawing.Size(336, 20);
            this.popupContainerEdit1.StyleController = this.layoutControl2;
            this.popupContainerEdit1.TabIndex = 5;
            this.popupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.btnLayoutAddNew);
            this.popupContainerControl1.Controls.Add(this.btnLayoutOK);
            this.popupContainerControl1.Controls.Add(this.gridControl2);
            this.popupContainerControl1.Location = new System.Drawing.Point(447, 147);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(597, 250);
            this.popupContainerControl1.TabIndex = 10;
            // 
            // btnLayoutAddNew
            // 
            this.btnLayoutAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLayoutAddNew.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLayoutAddNew.ImageOptions.Image")));
            this.btnLayoutAddNew.Location = new System.Drawing.Point(519, 224);
            this.btnLayoutAddNew.Name = "btnLayoutAddNew";
            this.btnLayoutAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnLayoutAddNew.TabIndex = 4;
            this.btnLayoutAddNew.Text = "Add New";
            this.btnLayoutAddNew.Click += new System.EventHandler(this.btnLayoutAddNew_Click);
            // 
            // btnLayoutOK
            // 
            this.btnLayoutOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLayoutOK.Location = new System.Drawing.Point(3, 224);
            this.btnLayoutOK.Name = "btnLayoutOK";
            this.btnLayoutOK.Size = new System.Drawing.Size(75, 23);
            this.btnLayoutOK.TabIndex = 3;
            this.btnLayoutOK.Text = "OK";
            this.btnLayoutOK.Click += new System.EventHandler(this.btnLayoutOK_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp01206_AT_Report_Available_LayoutsBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl2.Size = new System.Drawing.Size(591, 219);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp01206_AT_Report_Available_LayoutsBindingSource
            // 
            this.sp01206_AT_Report_Available_LayoutsBindingSource.DataMember = "sp01206_AT_Report_Available_Layouts";
            this.sp01206_AT_Report_Available_LayoutsBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReportLayoutID,
            this.colReportLayoutName,
            this.colReportTypeName,
            this.colModuleID,
            this.colCreatedBy,
            this.colPublishedToWeb});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReportLayoutName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView2_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView2_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView2_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colReportLayoutID
            // 
            this.colReportLayoutID.Caption = "Report Layout ID";
            this.colReportLayoutID.FieldName = "ReportLayoutID";
            this.colReportLayoutID.Name = "colReportLayoutID";
            this.colReportLayoutID.OptionsColumn.AllowEdit = false;
            this.colReportLayoutID.OptionsColumn.AllowFocus = false;
            this.colReportLayoutID.OptionsColumn.ReadOnly = true;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.Visible = true;
            this.colReportLayoutName.VisibleIndex = 0;
            this.colReportLayoutName.Width = 232;
            // 
            // colReportTypeName
            // 
            this.colReportTypeName.Caption = "Layout Type";
            this.colReportTypeName.FieldName = "ReportTypeName";
            this.colReportTypeName.Name = "colReportTypeName";
            this.colReportTypeName.OptionsColumn.AllowEdit = false;
            this.colReportTypeName.OptionsColumn.AllowFocus = false;
            this.colReportTypeName.OptionsColumn.ReadOnly = true;
            this.colReportTypeName.Visible = true;
            this.colReportTypeName.VisibleIndex = 1;
            this.colReportTypeName.Width = 94;
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.AllowEdit = false;
            this.colModuleID.OptionsColumn.AllowFocus = false;
            this.colModuleID.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.AllowFocus = false;
            this.colCreatedBy.OptionsColumn.ReadOnly = true;
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 2;
            this.colCreatedBy.Width = 136;
            // 
            // colPublishedToWeb
            // 
            this.colPublishedToWeb.Caption = "Published to Web";
            this.colPublishedToWeb.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPublishedToWeb.FieldName = "PublishedToWeb";
            this.colPublishedToWeb.Name = "colPublishedToWeb";
            this.colPublishedToWeb.OptionsColumn.AllowEdit = false;
            this.colPublishedToWeb.OptionsColumn.AllowFocus = false;
            this.colPublishedToWeb.OptionsColumn.ReadOnly = true;
            this.colPublishedToWeb.Visible = true;
            this.colPublishedToWeb.VisibleIndex = 3;
            this.colPublishedToWeb.Width = 104;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(439, 479);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.CustomizationFormText = "Site Grid:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(439, 431);
            this.layoutControlItem1.Text = "Site Grid:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.popupContainerEdit1;
            this.layoutControlItem2.CustomizationFormText = "Layout:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(439, 24);
            this.layoutControlItem2.Text = "Report Layout:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.popupContainerEdit2;
            this.layoutControlItem3.CustomizationFormText = "Linked Data:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(439, 24);
            this.layoutControlItem3.Text = "Report Linked Data:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(96, 13);
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Controls.Add(this.checkEditActiveSnowClearance);
            this.popupContainerControl2.Controls.Add(this.checkEditActiveGritting);
            this.popupContainerControl2.Controls.Add(this.btnClientFilterOK);
            this.popupContainerControl2.Controls.Add(this.gridControl3);
            this.popupContainerControl2.Location = new System.Drawing.Point(447, 403);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(312, 268);
            this.popupContainerControl2.TabIndex = 14;
            // 
            // checkEditActiveSnowClearance
            // 
            this.checkEditActiveSnowClearance.EditValue = true;
            this.checkEditActiveSnowClearance.Location = new System.Drawing.Point(175, 244);
            this.checkEditActiveSnowClearance.MenuManager = this.barManager1;
            this.checkEditActiveSnowClearance.Name = "checkEditActiveSnowClearance";
            this.checkEditActiveSnowClearance.Properties.Caption = "Active Snow Clearance";
            this.checkEditActiveSnowClearance.Size = new System.Drawing.Size(133, 19);
            this.checkEditActiveSnowClearance.TabIndex = 3;
            // 
            // checkEditActiveGritting
            // 
            this.checkEditActiveGritting.EditValue = true;
            this.checkEditActiveGritting.Location = new System.Drawing.Point(81, 244);
            this.checkEditActiveGritting.MenuManager = this.barManager1;
            this.checkEditActiveGritting.Name = "checkEditActiveGritting";
            this.checkEditActiveGritting.Properties.Caption = "Active Gritting";
            this.checkEditActiveGritting.Size = new System.Drawing.Size(88, 19);
            this.checkEditActiveGritting.TabIndex = 2;
            // 
            // btnClientFilterOK
            // 
            this.btnClientFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClientFilterOK.Location = new System.Drawing.Point(3, 242);
            this.btnClientFilterOK.Name = "btnClientFilterOK";
            this.btnClientFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnClientFilterOK.TabIndex = 1;
            this.btnClientFilterOK.Text = "OK";
            this.btnClientFilterOK.Click += new System.EventHandler(this.btnClientFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp03005EPClientListAllBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl3.Size = new System.Drawing.Size(305, 237);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp03005EPClientListAllBindingSource
            // 
            this.sp03005EPClientListAllBindingSource.DataMember = "sp03005_EP_Client_List_All";
            this.sp03005EPClientListAllBindingSource.DataSource = this.dataSet_EP;
            // 
            // dataSet_EP
            // 
            this.dataSet_EP.DataSetName = "DataSet_EP";
            this.dataSet_EP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientCode,
            this.colClientID,
            this.colClientName,
            this.colClientTypeDescription,
            this.colClientTypeID,
            this.gridColumn1});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colClientID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Visible = true;
            this.colClientCode.VisibleIndex = 1;
            this.colClientCode.Width = 108;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 271;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 2;
            this.colClientTypeDescription.Width = 132;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            this.colClientTypeID.Width = 101;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Remarks";
            this.gridColumn1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn1.FieldName = "Remarks";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 3;
            this.gridColumn1.Width = 67;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp01206_AT_Report_Available_LayoutsTableAdapter
            // 
            this.sp01206_AT_Report_Available_LayoutsTableAdapter.ClearBeforeFill = true;
            // 
            // bbiPublishToWeb
            // 
            this.bbiPublishToWeb.Caption = "Toggle Published To Web";
            this.bbiPublishToWeb.Enabled = false;
            this.bbiPublishToWeb.Id = 27;
            this.bbiPublishToWeb.Name = "bbiPublishToWeb";
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Toggle Published To Web - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiPublishToWeb.SuperTip = superToolTip1;
            this.bbiPublishToWeb.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiPublishToWeb.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPublishToWeb_ItemClick);
            // 
            // bbiUnpublishToWeb
            // 
            this.bbiUnpublishToWeb.Caption = "bbiUnpublishToWeb";
            this.bbiUnpublishToWeb.Id = 28;
            this.bbiUnpublishToWeb.Name = "bbiUnpublishToWeb";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(475, 235);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemClientFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadDataSupply),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewReport, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Data Supply Toolbar";
            // 
            // barEditItemClientFilter
            // 
            this.barEditItemClientFilter.Caption = "Clients:";
            this.barEditItemClientFilter.Edit = this.repositoryItemPopupContainerEditClientFilter;
            this.barEditItemClientFilter.EditValue = "All Clients";
            this.barEditItemClientFilter.EditWidth = 149;
            this.barEditItemClientFilter.Id = 31;
            this.barEditItemClientFilter.Name = "barEditItemClientFilter";
            this.barEditItemClientFilter.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditClientFilter
            // 
            this.repositoryItemPopupContainerEditClientFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditClientFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditClientFilter.Name = "repositoryItemPopupContainerEditClientFilter";
            this.repositoryItemPopupContainerEditClientFilter.PopupControl = this.popupContainerControl2;
            this.repositoryItemPopupContainerEditClientFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditClientFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditClientFilter_QueryResultValue);
            // 
            // bbiReloadDataSupply
            // 
            this.bbiReloadDataSupply.Caption = "Reload";
            this.bbiReloadDataSupply.Id = 29;
            this.bbiReloadDataSupply.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiReloadDataSupply.Name = "bbiReloadDataSupply";
            this.bbiReloadDataSupply.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Reload Data - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to reload the Data Supply List using the data supplied in the Filter.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiReloadDataSupply.SuperTip = superToolTip2;
            this.bbiReloadDataSupply.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadDataSupply_ItemClick);
            // 
            // bbiViewReport
            // 
            this.bbiViewReport.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiViewReport.Caption = "View Report";
            this.bbiViewReport.Id = 30;
            this.bbiViewReport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewReport.ImageOptions.Image")));
            this.bbiViewReport.Name = "bbiViewReport";
            this.bbiViewReport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "View Report - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to load the report with the records selected in the Data Supply list.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiViewReport.SuperTip = superToolTip3;
            this.bbiViewReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewReport_ItemClick);
            // 
            // sp03005_EP_Client_List_AllTableAdapter
            // 
            this.sp03005_EP_Client_List_AllTableAdapter.ClearBeforeFill = true;
            // 
            // sp04295_GC_Reports_Site_ListTableAdapter
            // 
            this.sp04295_GC_Reports_Site_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Report_Site_Listing
            // 
            this.ClientSize = new System.Drawing.Size(1102, 718);
            this.Controls.Add(this.popupContainerControl3);
            this.Controls.Add(this.popupContainerControl2);
            this.Controls.Add(this.popupContainerControl1);
            this.Controls.Add(this.printControl1);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ribbonControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Report_Site_Listing";
            this.Text = "Site Listing Reports";
            this.Activated += new System.EventHandler(this.frm_GC_Report_Site_Listing_Activated);
            this.Load += new System.EventHandler(this.frm_GC_Report_Site_Listing_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.ribbonControl1, 0);
            this.Controls.SetChildIndex(this.ribbonStatusBar1, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.Controls.SetChildIndex(this.printControl1, 0);
            this.Controls.SetChildIndex(this.popupContainerControl1, 0);
            this.Controls.SetChildIndex(this.popupContainerControl2, 0);
            this.Controls.SetChildIndex(this.popupContainerControl3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printRibbonController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl3)).EndInit();
            this.popupContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSnowClearanceCalloutsRates.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSnowClearanceCalloutsExtraCosts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGrittingCalloutsImages.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGrittingCalloutsExtraCosts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSnowClearanceCallouts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGrittingCallouts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04295GCReportsSiteListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01206_AT_Report_Available_LayoutsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActiveSnowClearance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActiveGritting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03005EPClientListAllBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditClientFilter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraPrinting.Preview.PrintRibbonController printRibbonController1;
        private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem8;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem9;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem10;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem11;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem12;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem13;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem14;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem15;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem16;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem17;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem18;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem19;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem20;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem21;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem22;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem23;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem24;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem25;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem26;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem27;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem28;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem29;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem30;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem31;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem32;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem33;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem34;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem35;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem36;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem37;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem38;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem39;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem40;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem41;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem42;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem43;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem44;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem45;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage printPreviewRibbonPage1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup7;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraEditors.SimpleButton btnLayoutAddNew;
        private DevExpress.XtraEditors.SimpleButton btnLayoutOK;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_AT dataSet_AT;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraEditors.SimpleButton btnClientFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp01206_AT_Report_Available_LayoutsBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp01206_AT_Report_Available_LayoutsTableAdapter sp01206_AT_Report_Available_LayoutsTableAdapter;
        private DevExpress.XtraEditors.DateEdit deToDate;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraPrinting.Preview.ProgressBarEditItem progressBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem46;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem2;
        private DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem zoomTrackBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEditGrittingCallouts;
        private DevExpress.XtraGrid.Columns.GridColumn colPublishedToWeb;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiPublishToWeb;
        private DevExpress.XtraBars.BarButtonItem bbiUnpublishToWeb;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraEditors.DateEdit deFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiReloadDataSupply;
        private DevExpress.XtraBars.BarButtonItem bbiViewReport;
        private DevExpress.XtraBars.BarEditItem barEditItemClientFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditClientFilter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl3;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnShowLinkedDataOK;
        private DevExpress.XtraEditors.CheckEdit checkEditSnowClearanceCalloutsRates;
        private DevExpress.XtraEditors.CheckEdit checkEditSnowClearanceCalloutsExtraCosts;
        private DevExpress.XtraEditors.CheckEdit checkEditGrittingCalloutsImages;
        private DevExpress.XtraEditors.CheckEdit checkEditGrittingCalloutsExtraCosts;
        private DevExpress.XtraEditors.CheckEdit checkEditSnowClearanceCallouts;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DataSet_EP dataSet_EP;
        private System.Windows.Forms.BindingSource sp03005EPClientListAllBindingSource;
        private DataSet_EPTableAdapters.sp03005_EP_Client_List_AllTableAdapter sp03005_EP_Client_List_AllTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEditActiveGritting;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private System.Windows.Forms.BindingSource sp04295GCReportsSiteListBindingSource;
        private DataSet_GC_ReportsTableAdapters.sp04295_GC_Reports_Site_ListTableAdapter sp04295_GC_Reports_Site_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteFax;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colMappingWorkspaceID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkspaceName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colHubID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingSite;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSnowClearanceSite;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedGrittingContractCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedSnowClearanceContractCount;
        private DevExpress.XtraEditors.CheckEdit checkEditActiveSnowClearance;
    }
}

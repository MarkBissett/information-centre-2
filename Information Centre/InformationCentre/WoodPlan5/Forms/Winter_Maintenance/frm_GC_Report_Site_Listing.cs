using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.LookAndFeel;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using System.IO;  // Used by File.Delete //
using System.Data.SqlClient;  // Used by Generate Map process //
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_GC_Report_Site_Listing : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;

        WaitDialogForm loadingForm;
        private int i_int_SelectedLayoutID = 0;
        
        public string i_str_SavedDirectoryName = "";
        
        private string i_str_SelectedFilename = "";
        private int i_int_FocusedGrid = 1;
        private string i_str_selected_client_ids = "";
        private string i_str_selected_client_names = "";
        private string i_str_selected_linked_records = "";

        private string i_str_selected_records = "";
 
        private int i_intReportType = 9;
        
        BaseObjects.GridCheckMarksSelection selection1;

        rpt_GC_Report_Site_Listing_Blank rptReport;
        #endregion

        public frm_GC_Report_Site_Listing()
        {
            InitializeComponent();
        }

        private void frm_GC_Report_Site_Listing_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 92007;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            printPreviewBarItem44.Enabled = false;  // Switch off Open button //

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp03005_EP_Client_List_AllTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03005_EP_Client_List_AllTableAdapter.Fill(dataSet_EP.sp03005_EP_Client_List_All);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            deFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            deToDate.DateTime = this.GlobalSettings.ViewedEndDate;

            sp01206_AT_Report_Available_LayoutsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 4, i_intReportType, "AmenityTreesReportLayoutLocation");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04295_GC_Reports_Site_ListTableAdapter.Connection.ConnectionString = strConnectionString;      
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Get default layout file directory from System_Settings table //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_SavedDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Report Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_SavedDirectoryName.EndsWith("\\")) i_str_SavedDirectoryName += "\\";  // Add Backslash to end //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            btnLayoutAddNew.Enabled = iBool_AllowEdit;
        }

        public override void PostOpen(object objParameter)
        {
            dockPanel1.Width = 450;
            
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            this.Refresh();

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
        }
        
        private void frm_GC_Report_Site_Listing_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            bbiPublishToWeb.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            bbiPublishToWeb.Enabled = false;
        
            alItems.AddRange(new string[] { "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            if (i_int_FocusedGrid == 99)
            {
                bbiPublishToWeb.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                bbiPublishToWeb.Enabled = true;

                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                    iBool_AllowDelete = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                    iBool_AllowDelete = false;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void bbiReloadDataSupply_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager = splashScreenManager1;
            splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp04295_GC_Reports_Site_ListTableAdapter.Fill(this.dataSet_GC_Reports.sp04295_GC_Reports_Site_List, i_str_selected_client_ids, (checkEditActiveGritting.Checked ? 1 : 0), (checkEditActiveSnowClearance.Checked ? 1 : 0));
            view.EndUpdate();

            splashScreenManager.CloseWaitForm();
        }

        private void bbiViewReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (popupContainerEdit1.Text == "No Report Layout Selected")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, no layout specified.\n\nSelect a layout to use from the Layout list then try again.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (printControl1.PrintingSystem.Document.Name != null)  // Clear any report currently being viewed //
            {
                printControl1.PrintingSystem.ClearContent();
                rptReport = null;
            }

            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;

            i_str_selected_records = "";
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to report on before proceeding!", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                i_str_selected_records += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["SiteID"])) + ',';
            }
            rptReport = new rpt_GC_Report_Site_Listing_Blank(this.GlobalSettings, i_str_selected_records, checkEditGrittingCallouts.Checked, checkEditGrittingCalloutsExtraCosts.Checked, checkEditGrittingCalloutsImages.Checked, checkEditSnowClearanceCallouts.Checked, checkEditSnowClearanceCalloutsExtraCosts.Checked, checkEditSnowClearanceCalloutsRates.Checked, deFromDate.DateTime, deToDate.DateTime, "");

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager = splashScreenManager1;
            splashScreenManager.ShowWaitForm();

            try
            {
                rptReport.LoadLayout(i_str_SavedDirectoryName + i_str_SelectedFilename);
            }
            catch (Exception Ex)
            {
                splashScreenManager.CloseWaitForm();
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            AdjustEventHandlers(rptReport);  // Tie in custom Sorting //

            printControl1.PrintingSystem = rptReport.PrintingSystem;
            printingSystem1.Begin();
            rptReport.CreateDocument();
            printingSystem1.End();
            splashScreenManager.CloseWaitForm();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            DeleteRecord();
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)gridControl2.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
            string strReportLayoutName = Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
            string strReportFileName = Convert.ToString(intReportLayoutID) + ".repx";
            if (intReportLayoutID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout, no layout selected for deletion!\n\nSelect a layout before proceeding.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to delete the selected layout: " + strReportLayoutName + "?\n\nIf you proceed, the layout will no longer be available for use!", "Delete Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                loadingForm = new WaitDialogForm("Deleting Layout...", "Reporting");
                loadingForm.Show();

                // Delete the record then the physical file layout //
                DataSet_ATTableAdapters.QueriesTableAdapter DeleteLayout = new DataSet_ATTableAdapters.QueriesTableAdapter();
                DeleteLayout.ChangeConnectionString(strConnectionString);
                DeleteLayout.sp01213_AT_Report_Layouts_Delete_Layout_Record(intReportLayoutID);


                try
                {
                    File.Delete(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout, it may no longer exist or you may not have the necessary permissions to delete it - contact Technical Support.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                // Reload list of available layouts //
                sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, i_intReportType, "AmenityTreesReportLayoutLocation");          
                popupContainerEdit1.Text = "No Report Layout Selected";
                i_int_SelectedLayoutID = 0;
                loadingForm.Close();
            }

        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            EditRecord();
        }

        private void EditRecord()
        {
            GridView View = (GridView)gridControl2.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(View.GetFocusedRowCellValue("ReportLayoutID"));
            string strReportLayoutName = Convert.ToString(View.GetFocusedRowCellValue("ReportLayoutName"));
            string strReportFileName = Convert.ToString(intReportLayoutID) + ".repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                rptReport = new rpt_GC_Report_Site_Listing_Blank(this.GlobalSettings, i_str_selected_records, checkEditGrittingCallouts.Checked, checkEditGrittingCalloutsExtraCosts.Checked, checkEditGrittingCalloutsImages.Checked, checkEditSnowClearanceCallouts.Checked, checkEditSnowClearanceCalloutsExtraCosts.Checked, checkEditSnowClearanceCalloutsRates.Checked, deFromDate.DateTime, deToDate.DateTime, "");
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                loadingForm = new WaitDialogForm("Loading Report Builder...", "Reporting");
                loadingForm.Show();

                // Open the report in the Report Builder //
                // Create a design form and get its panel.
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);                

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                loadingForm.Close();
                form.ShowDialog();
                panel.CloseReport();
            }
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            if (!iBool_AllowAdd) return;
            AddLayout();
        }
        
        void form_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            /*string strType = "";
            string strFieldName = "";
            GridView view = (GridView)gridControl1.MainView;
            strFieldName = "TreeID";
            strType = "tree";
            view.PostEditor();
            string strSelectedIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, strFieldName)) + ',';
            }
            try
            {
                //Mapping_Functions MapFunctions = new Mapping_Functions();
                //MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, strType);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }*/
         }


        #region Grid View Generic Events

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion

        
        #region GridView 1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Sites Available - Adjust Client filter and click Reload");
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        #endregion


        #region Client Filter Panel

        private void repositoryItemPopupContainerEditClientFilter_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditClients_Get_Selected();
        }

        private void btnClientFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditClients_Get_Selected()
        {
            i_str_selected_client_ids = "";    // Reset any prior values first //
            i_str_selected_client_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "All Clients";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "All Clients";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_client_ids += Convert.ToString(view.GetRowCellValue(i, "ClientID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_client_names = Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_client_names += ", " + Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_client_names;
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 51;
            SetMenuStatus();
        }

        #endregion


        #region PopupContainerLayout

        private void btnLayoutOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void btnLayoutAddNew_Click(object sender, EventArgs e)
        {
            if (!iBool_AllowAdd) return;
            AddLayout();
        }

        private void AddLayout()
        {
            Form frmMain = this.MdiParent;
            frm_AT_Report_Add_Layout fChildForm = new frm_AT_Report_Add_Layout();
            frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intReportType = i_intReportType;
            fChildForm.boolPublishedToWebEnabled = true;
            int intNewLayoutID = 0;
            string strNewLayoutName = "";
            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                return;
            }
            intNewLayoutID = fChildForm.intReturnedValue;
            strNewLayoutName = fChildForm.strReturnedLayoutName;
            int intPublishedToWeb = fChildForm.intPublishedToWeb;

            // Save the new Layout and get back it's ID for the physical filename //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intNewID = 0;
            intNewID = Convert.ToInt32(GetSetting.sp01212_AT_Report_Add_Layouts_Create_Layout_Record(1, i_intReportType, strNewLayoutName, this.GlobalSettings.UserID, intPublishedToWeb));
            if (intNewID <= 0 || i_intReportType <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create new layout, a problem occurred - contact Technical Support.", "Create Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // Load the report - Create a design form and get its panel.
            XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
            //XRDesignFormEx form = new XRDesignFormEx();

            XRDesignPanel panel = form.DesignPanel;
            panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

            rptReport = new rpt_GC_Report_Site_Listing_Blank(this.GlobalSettings, i_str_selected_records, checkEditGrittingCallouts.Checked, checkEditGrittingCalloutsExtraCosts.Checked, checkEditGrittingCalloutsImages.Checked, checkEditSnowClearanceCallouts.Checked, checkEditSnowClearanceCalloutsExtraCosts.Checked, checkEditSnowClearanceCalloutsRates.Checked, deFromDate.DateTime, deToDate.DateTime, "");
            //rptReport.Name = "Tree Listing";  // Set Document Root Node to meaningful text //

            if (intNewLayoutID > 0)
            {
                i_str_SelectedFilename = Convert.ToInt32(intNewLayoutID) + ".repx";
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + i_str_SelectedFilename);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            // Set filename to correct one so that when save fires from the report designer, the correct file is created/updated //
            i_str_SelectedFilename = Convert.ToInt32(intNewID) + ".repx";

            // Add a new command handler to the Report Designer which saves the report in a custom way.
            panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, i_str_SelectedFilename));

            // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
            panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

            loadingForm = new WaitDialogForm("Loading Report Builder...", "Reporting");
            loadingForm.Show();

            // Load the report into the design form and show the form.
            panel.OpenReport(rptReport);
            form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
            form.WindowState = FormWindowState.Maximized;
            loadingForm.Close();

            form.ShowDialog();
            panel.CloseReport();

            // Reload list of available layout and pre-select new one //
            sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, i_intReportType, "AmenityTreesReportLayoutLocation");
            GridView view = (GridView)gridControl2.MainView;
            Int32 intFoundRow = view.LocateByValue(0, view.Columns["ReportLayoutID"], intNewID);
            if (intFoundRow > -1)
            {
                view.FocusedRowHandle = intFoundRow;
            }
            popupContainerEdit1.Text = PopupContainerEdit1_Get_Layout_Selected();  // Set Selected Layout Text //
        }

        private void popupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Layout_Selected();
        }

        private string PopupContainerEdit1_Get_Layout_Selected()
        {
            int[] intRowHandles;
            int intCount = 0;

            GridView view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                i_int_SelectedLayoutID = 0;
                return "No Report Layout Selected";
            }
            else
            {
                i_int_SelectedLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
                i_str_SelectedFilename = Convert.ToString(i_int_SelectedLayoutID) + ".repx";
                return Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
            }

        }

        private void gridView2_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Report Layouts Available", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            if (!iBool_AllowEdit) return;
            EditRecord();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 99;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
        }

        private void gridView2_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //
            GridView view = (GridView)sender;
            if (e.Column.ColumnType != typeof(DateTime))
            {
                GridColumnCollection cols = new GridColumnCollection(view);
                GridColumn column = cols.AddField(e.Column.FieldName);
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[column.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values != null)
                {
                    comboBox.Items.AddRange(values);
                    column.ColumnEdit = comboBox;
                }
                DevExpress.XtraGrid.Filter.FilterCustomDialog dlg = new DevExpress.XtraGrid.Filter.FilterCustomDialog(column, false);
                dlg.ShowDialog();
                e.FilterInfo = null;
                e.Handled = true;
                gridControl2.Refresh();
            }
        }

        private void gridView2_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }

        #endregion


        #region Show Linked Data Panel

        private void popupContainerEdit2_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private void checkEditGrittingCallouts_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                checkEditGrittingCalloutsExtraCosts.Enabled = true;
                checkEditGrittingCalloutsImages.Enabled = true;
            }
            else
            {
                checkEditGrittingCalloutsExtraCosts.Enabled = false;
                checkEditGrittingCalloutsImages.Enabled = false;

                checkEditGrittingCalloutsExtraCosts.Checked = false;
                checkEditGrittingCalloutsImages.Checked = false;
            }
        }

        private void checkEditSnowClearanceCallouts_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                checkEditSnowClearanceCalloutsExtraCosts.Enabled = true;
                checkEditSnowClearanceCalloutsRates.Enabled = true;
            }
            else
            {
                checkEditSnowClearanceCalloutsExtraCosts.Enabled = false;
                checkEditSnowClearanceCalloutsRates.Enabled = false;

                checkEditSnowClearanceCalloutsExtraCosts.Checked = false;
                checkEditSnowClearanceCalloutsRates.Checked = false;
            }
        }

        private void btnShowLinkedDataOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_linked_records = "";  // Reset any prior values first //
            i_str_selected_linked_records += (checkEditGrittingCallouts.Checked ? "Gritting Callouts, " : "");
            i_str_selected_linked_records += (checkEditGrittingCalloutsExtraCosts.Checked ? "Gritting Callout Extra Costs, " : "");
            i_str_selected_linked_records += (checkEditGrittingCalloutsImages.Checked ? "Gritting Callout Images" : "");
            i_str_selected_linked_records += (checkEditSnowClearanceCallouts.Checked ? "Snow Clearance Callouts, " : "");
            i_str_selected_linked_records += (checkEditSnowClearanceCalloutsExtraCosts.Checked ? "Gritting Callout Extra Costs, " : "");
            i_str_selected_linked_records += (checkEditSnowClearanceCalloutsRates.Checked ? "Snow Clearance Callout Rates" : "");
            if (i_str_selected_linked_records.EndsWith(", ")) i_str_selected_linked_records = i_str_selected_linked_records.Remove(i_str_selected_linked_records.Length - 2, 2);
            if (string.IsNullOrEmpty(i_str_selected_linked_records)) i_str_selected_linked_records = "No Linked Data";
            return i_str_selected_linked_records;
        }

        #endregion


        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Report...", "Reporting");
                loadingForm.Show();
                
                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }

        private void bbiPublishToWeb_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView View = (GridView)gridControl2.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(View.GetFocusedRowCellValue("ReportLayoutID"));
            int intPublished = Convert.ToInt32(View.GetFocusedRowCellValue("PublishedToWeb"));
            string strMessage = "";
            if (intReportLayoutID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Toggle Published Status of Layout, no layout selected.\n\nSelect a layout before proceeding.", "Toggle Web Published Status of Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intPublished == 1)
            {
                strMessage = "You are about to hide the selected report layout from the WoodPlan Web Interface.\n\nAre you sure you wish to proceed?";
            }
            else
            {
                strMessage = "You are about to make the selected report layout available to the WoodPlan Web Interface?\n\nAre you sure you wish to proceed?";
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Toggle Web Published Status of Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                loadingForm = new WaitDialogForm("Toggling Published Status...", "Reporting");
                loadingForm.Show();

                // Delete the record then the physical file layout //
                DataSet_ATTableAdapters.QueriesTableAdapter TogglePublishedStatus = new DataSet_ATTableAdapters.QueriesTableAdapter();
                TogglePublishedStatus.ChangeConnectionString(strConnectionString);
                try
                {
                    TogglePublishedStatus.sp01453_AT_Report_Layouts_Toggle_Web_Availibility(intReportLayoutID, (intPublished == 1 ? 0 : 1));
                }
                catch (Exception Ex)
                {
                    loadingForm.Close();
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to toggle published status - an error occurred [" + Ex.Message + "]. Please try again. If the problem persists contact Technical Support.", "Toggle Web Published Status of Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                // Reload list of available layouts to reflect changed Published Status of layout //
                sp01206_AT_Report_Available_LayoutsTableAdapter.Fill(this.dataSet_AT.sp01206_AT_Report_Available_Layouts, 1, i_intReportType, "AmenityTreesReportLayoutLocation");
                loadingForm.Close();
                if (intPublished == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Report Layout set as Unpublished.\n\nThis layout will no longer be available to the WoodPlan Web Interface.\n\nIf you currently have the WoodPlan Web Interface open, you may need to refresh the page to reflect this change.", "Toggle Web Published Status of Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Report Layout set Published.\n\nThis layout is now available to the WoodPlan Web Interface.\n\nIf you currently have the WoodPlan Web Interface open, you may need to refresh the page to reflect this change.", "Toggle Web Published Status of Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }






    }
}


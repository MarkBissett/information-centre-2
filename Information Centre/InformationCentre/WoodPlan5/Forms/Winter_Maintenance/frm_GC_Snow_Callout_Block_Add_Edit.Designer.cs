﻿namespace WoodPlan5
{
    partial class frm_GC_Snow_Callout_Block_Add_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ClientHowSoonIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04165GCClientHowSoonTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Snow_DataEntry = new WoodPlan5.DataSet_GC_Snow_DataEntry();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NonStandardSellCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NonStandardCostCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ClientPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubContractorContactedByStaffIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00226StaffListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisplayName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetworkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SubContractorETADateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GCPONumberSuffixTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForGCPONumberSuffix = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorETA = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNonStandardCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNonStandardSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientHowSoonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorContactedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00226_Staff_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter();
            this.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.HoursWorkedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForHoursWorked = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClientHowSoonIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04165GCClientHowSoonTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardSellCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardCostCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorContactedByStaffIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETADateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETADateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCPONumberSuffixTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCPONumberSuffix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorETA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientHowSoonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorContactedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursWorkedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursWorked)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(626, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 343);
            this.barDockControlBottom.Size = new System.Drawing.Size(626, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 317);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(626, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 317);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 59;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.HoursWorkedSpinEdit);
            this.layoutControl1.Controls.Add(this.ClientHowSoonIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.NonStandardSellCheckEdit);
            this.layoutControl1.Controls.Add(this.NonStandardCostCheckEdit);
            this.layoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.layoutControl1.Controls.Add(this.ClientPONumberTextEdit);
            this.layoutControl1.Controls.Add(this.SubContractorContactedByStaffIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.SubContractorETADateEdit);
            this.layoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.layoutControl1.Controls.Add(this.GCPONumberSuffixTextEdit);
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1259, 293, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(625, 284);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // ClientHowSoonIDGridLookUpEdit
            // 
            this.ClientHowSoonIDGridLookUpEdit.Location = new System.Drawing.Point(171, 131);
            this.ClientHowSoonIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ClientHowSoonIDGridLookUpEdit.Name = "ClientHowSoonIDGridLookUpEdit";
            this.ClientHowSoonIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClientHowSoonIDGridLookUpEdit.Properties.DataSource = this.sp04165GCClientHowSoonTypesWithBlankBindingSource;
            this.ClientHowSoonIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ClientHowSoonIDGridLookUpEdit.Properties.NullText = "";
            this.ClientHowSoonIDGridLookUpEdit.Properties.ValueMember = "Value";
            this.ClientHowSoonIDGridLookUpEdit.Properties.View = this.gridView1;
            this.ClientHowSoonIDGridLookUpEdit.Size = new System.Drawing.Size(442, 20);
            this.ClientHowSoonIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.ClientHowSoonIDGridLookUpEdit.TabIndex = 12;
            // 
            // sp04165GCClientHowSoonTypesWithBlankBindingSource
            // 
            this.sp04165GCClientHowSoonTypesWithBlankBindingSource.DataMember = "sp04165_GC_Client_How_Soon_Types_With_Blank";
            this.sp04165GCClientHowSoonTypesWithBlankBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // dataSet_GC_Snow_DataEntry
            // 
            this.dataSet_GC_Snow_DataEntry.DataSetName = "DataSet_GC_Snow_DataEntry";
            this.dataSet_GC_Snow_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colOrder,
            this.colValue});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colValue;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 210;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // NonStandardSellCheckEdit
            // 
            this.NonStandardSellCheckEdit.Location = new System.Drawing.Point(171, 202);
            this.NonStandardSellCheckEdit.MenuManager = this.barManager1;
            this.NonStandardSellCheckEdit.Name = "NonStandardSellCheckEdit";
            this.NonStandardSellCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NonStandardSellCheckEdit.Properties.ValueChecked = 1;
            this.NonStandardSellCheckEdit.Properties.ValueUnchecked = 0;
            this.NonStandardSellCheckEdit.Size = new System.Drawing.Size(442, 19);
            this.NonStandardSellCheckEdit.StyleController = this.layoutControl1;
            this.NonStandardSellCheckEdit.TabIndex = 11;
            // 
            // NonStandardCostCheckEdit
            // 
            this.NonStandardCostCheckEdit.Location = new System.Drawing.Point(171, 179);
            this.NonStandardCostCheckEdit.MenuManager = this.barManager1;
            this.NonStandardCostCheckEdit.Name = "NonStandardCostCheckEdit";
            this.NonStandardCostCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NonStandardCostCheckEdit.Properties.ValueChecked = 1;
            this.NonStandardCostCheckEdit.Properties.ValueUnchecked = 0;
            this.NonStandardCostCheckEdit.Size = new System.Drawing.Size(442, 19);
            this.NonStandardCostCheckEdit.StyleController = this.layoutControl1;
            this.NonStandardCostCheckEdit.TabIndex = 10;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.Location = new System.Drawing.Point(171, 225);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(442, 47);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling1);
            this.RemarksMemoEdit.StyleController = this.layoutControl1;
            this.RemarksMemoEdit.TabIndex = 9;
            // 
            // ClientPONumberTextEdit
            // 
            this.ClientPONumberTextEdit.Location = new System.Drawing.Point(171, 155);
            this.ClientPONumberTextEdit.MenuManager = this.barManager1;
            this.ClientPONumberTextEdit.Name = "ClientPONumberTextEdit";
            this.ClientPONumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPONumberTextEdit, true);
            this.ClientPONumberTextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPONumberTextEdit, optionsSpelling2);
            this.ClientPONumberTextEdit.StyleController = this.layoutControl1;
            this.ClientPONumberTextEdit.TabIndex = 8;
            // 
            // SubContractorContactedByStaffIDGridLookUpEdit
            // 
            this.SubContractorContactedByStaffIDGridLookUpEdit.Location = new System.Drawing.Point(171, 83);
            this.SubContractorContactedByStaffIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SubContractorContactedByStaffIDGridLookUpEdit.Name = "SubContractorContactedByStaffIDGridLookUpEdit";
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.DisplayMember = "StaffName";
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.NullText = "";
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.SubContractorContactedByStaffIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.SubContractorContactedByStaffIDGridLookUpEdit.Size = new System.Drawing.Size(442, 20);
            this.SubContractorContactedByStaffIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.SubContractorContactedByStaffIDGridLookUpEdit.TabIndex = 7;
            // 
            // sp00226StaffListWithBlankBindingSource
            // 
            this.sp00226StaffListWithBlankBindingSource.DataMember = "sp00226_Staff_List_With_Blank";
            this.sp00226StaffListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActive,
            this.colDisplayName,
            this.colEmail,
            this.colForename,
            this.colNetworkID,
            this.colStaffID,
            this.colStaffName,
            this.colSurname,
            this.colUserType});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colStaffID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDisplayName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 51;
            // 
            // colDisplayName
            // 
            this.colDisplayName.Caption = "Surname: Forename";
            this.colDisplayName.FieldName = "DisplayName";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.OptionsColumn.AllowEdit = false;
            this.colDisplayName.OptionsColumn.AllowFocus = false;
            this.colDisplayName.OptionsColumn.ReadOnly = true;
            this.colDisplayName.Visible = true;
            this.colDisplayName.VisibleIndex = 0;
            this.colDisplayName.Width = 225;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowEdit = false;
            this.colEmail.OptionsColumn.AllowFocus = false;
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Width = 192;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Width = 178;
            // 
            // colNetworkID
            // 
            this.colNetworkID.Caption = "Network ID";
            this.colNetworkID.FieldName = "NetworkID";
            this.colNetworkID.Name = "colNetworkID";
            this.colNetworkID.OptionsColumn.AllowEdit = false;
            this.colNetworkID.OptionsColumn.AllowFocus = false;
            this.colNetworkID.OptionsColumn.ReadOnly = true;
            this.colNetworkID.Width = 180;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Width = 231;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Width = 191;
            // 
            // colUserType
            // 
            this.colUserType.Caption = "User Type";
            this.colUserType.FieldName = "UserType";
            this.colUserType.Name = "colUserType";
            this.colUserType.OptionsColumn.AllowEdit = false;
            this.colUserType.OptionsColumn.AllowFocus = false;
            this.colUserType.OptionsColumn.ReadOnly = true;
            this.colUserType.Width = 223;
            // 
            // SubContractorETADateEdit
            // 
            this.SubContractorETADateEdit.EditValue = null;
            this.SubContractorETADateEdit.Location = new System.Drawing.Point(171, 107);
            this.SubContractorETADateEdit.MenuManager = this.barManager1;
            this.SubContractorETADateEdit.Name = "SubContractorETADateEdit";
            this.SubContractorETADateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubContractorETADateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.SubContractorETADateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SubContractorETADateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SubContractorETADateEdit.Size = new System.Drawing.Size(442, 20);
            this.SubContractorETADateEdit.StyleController = this.layoutControl1;
            this.SubContractorETADateEdit.TabIndex = 6;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(171, 36);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "(Tick if Reactive, Untick for Proactive)";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(442, 19);
            this.ReactiveCheckEdit.StyleController = this.layoutControl1;
            this.ReactiveCheckEdit.TabIndex = 5;
            // 
            // GCPONumberSuffixTextEdit
            // 
            this.GCPONumberSuffixTextEdit.Location = new System.Drawing.Point(171, 12);
            this.GCPONumberSuffixTextEdit.MenuManager = this.barManager1;
            this.GCPONumberSuffixTextEdit.Name = "GCPONumberSuffixTextEdit";
            this.GCPONumberSuffixTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GCPONumberSuffixTextEdit, true);
            this.GCPONumberSuffixTextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GCPONumberSuffixTextEdit, optionsSpelling3);
            this.GCPONumberSuffixTextEdit.StyleController = this.layoutControl1;
            this.GCPONumberSuffixTextEdit.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGCPONumberSuffix,
            this.ItemForReactive,
            this.ItemForSubContractorETA,
            this.ItemForClientPONumber,
            this.ItemForRemarks,
            this.ItemForNonStandardCost,
            this.ItemForNonStandardSell,
            this.ItemForClientHowSoonID,
            this.ItemForSubContractorContactedByStaffID,
            this.ItemForHoursWorked});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(625, 284);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForGCPONumberSuffix
            // 
            this.ItemForGCPONumberSuffix.Control = this.GCPONumberSuffixTextEdit;
            this.ItemForGCPONumberSuffix.CustomizationFormText = "GC P.O. Number Suffix:";
            this.ItemForGCPONumberSuffix.Location = new System.Drawing.Point(0, 0);
            this.ItemForGCPONumberSuffix.Name = "ItemForGCPONumberSuffix";
            this.ItemForGCPONumberSuffix.Size = new System.Drawing.Size(605, 24);
            this.ItemForGCPONumberSuffix.Text = "GC P.O. Number Suffix:";
            this.ItemForGCPONumberSuffix.TextSize = new System.Drawing.Size(155, 13);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 24);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(605, 23);
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(155, 13);
            // 
            // ItemForSubContractorETA
            // 
            this.ItemForSubContractorETA.Control = this.SubContractorETADateEdit;
            this.ItemForSubContractorETA.CustomizationFormText = "Team ETA:";
            this.ItemForSubContractorETA.Location = new System.Drawing.Point(0, 95);
            this.ItemForSubContractorETA.Name = "ItemForSubContractorETA";
            this.ItemForSubContractorETA.Size = new System.Drawing.Size(605, 24);
            this.ItemForSubContractorETA.Text = "Team ETA:";
            this.ItemForSubContractorETA.TextSize = new System.Drawing.Size(155, 13);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberTextEdit;
            this.ItemForClientPONumber.CustomizationFormText = "Client P.O. Number:";
            this.ItemForClientPONumber.Location = new System.Drawing.Point(0, 143);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(605, 24);
            this.ItemForClientPONumber.Text = "Client P.O. Number:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(155, 13);
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 213);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(605, 51);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(155, 13);
            // 
            // ItemForNonStandardCost
            // 
            this.ItemForNonStandardCost.Control = this.NonStandardCostCheckEdit;
            this.ItemForNonStandardCost.CustomizationFormText = "Non-Standard Cost:";
            this.ItemForNonStandardCost.Location = new System.Drawing.Point(0, 167);
            this.ItemForNonStandardCost.Name = "ItemForNonStandardCost";
            this.ItemForNonStandardCost.Size = new System.Drawing.Size(605, 23);
            this.ItemForNonStandardCost.Text = "Non-Standard Cost:";
            this.ItemForNonStandardCost.TextSize = new System.Drawing.Size(155, 13);
            // 
            // ItemForNonStandardSell
            // 
            this.ItemForNonStandardSell.Control = this.NonStandardSellCheckEdit;
            this.ItemForNonStandardSell.CustomizationFormText = "Non-Standard Sell:";
            this.ItemForNonStandardSell.Location = new System.Drawing.Point(0, 190);
            this.ItemForNonStandardSell.Name = "ItemForNonStandardSell";
            this.ItemForNonStandardSell.Size = new System.Drawing.Size(605, 23);
            this.ItemForNonStandardSell.Text = "Non-Standard Sell:";
            this.ItemForNonStandardSell.TextSize = new System.Drawing.Size(155, 13);
            // 
            // ItemForClientHowSoonID
            // 
            this.ItemForClientHowSoonID.Control = this.ClientHowSoonIDGridLookUpEdit;
            this.ItemForClientHowSoonID.CustomizationFormText = "How Soon:";
            this.ItemForClientHowSoonID.Location = new System.Drawing.Point(0, 119);
            this.ItemForClientHowSoonID.Name = "ItemForClientHowSoonID";
            this.ItemForClientHowSoonID.Size = new System.Drawing.Size(605, 24);
            this.ItemForClientHowSoonID.Text = "How Soon:";
            this.ItemForClientHowSoonID.TextSize = new System.Drawing.Size(155, 13);
            // 
            // ItemForSubContractorContactedByStaffID
            // 
            this.ItemForSubContractorContactedByStaffID.Control = this.SubContractorContactedByStaffIDGridLookUpEdit;
            this.ItemForSubContractorContactedByStaffID.CustomizationFormText = "Team Contacted By Staff Name:";
            this.ItemForSubContractorContactedByStaffID.Location = new System.Drawing.Point(0, 71);
            this.ItemForSubContractorContactedByStaffID.Name = "ItemForSubContractorContactedByStaffID";
            this.ItemForSubContractorContactedByStaffID.Size = new System.Drawing.Size(605, 24);
            this.ItemForSubContractorContactedByStaffID.Text = "Team Contacted By Staff Name:";
            this.ItemForSubContractorContactedByStaffID.TextSize = new System.Drawing.Size(155, 13);
            // 
            // sp00226_Staff_List_With_BlankTableAdapter
            // 
            this.sp00226_Staff_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter
            // 
            this.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(406, 313);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(517, 313);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // HoursWorkedSpinEdit
            // 
            this.HoursWorkedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.HoursWorkedSpinEdit.Location = new System.Drawing.Point(171, 59);
            this.HoursWorkedSpinEdit.MenuManager = this.barManager1;
            this.HoursWorkedSpinEdit.Name = "HoursWorkedSpinEdit";
            this.HoursWorkedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.HoursWorkedSpinEdit.Properties.Mask.EditMask = "f2";
            this.HoursWorkedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HoursWorkedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.HoursWorkedSpinEdit.Size = new System.Drawing.Size(442, 20);
            this.HoursWorkedSpinEdit.StyleController = this.layoutControl1;
            this.HoursWorkedSpinEdit.TabIndex = 13;
            // 
            // ItemForHoursWorked
            // 
            this.ItemForHoursWorked.Control = this.HoursWorkedSpinEdit;
            this.ItemForHoursWorked.CustomizationFormText = "Hours Worked:";
            this.ItemForHoursWorked.Location = new System.Drawing.Point(0, 47);
            this.ItemForHoursWorked.Name = "ItemForHoursWorked";
            this.ItemForHoursWorked.Size = new System.Drawing.Size(605, 24);
            this.ItemForHoursWorked.Text = "Hours Worked:";
            this.ItemForHoursWorked.TextSize = new System.Drawing.Size(155, 13);
            // 
            // frm_GC_Snow_Callout_Block_Add_Edit
            // 
            this.ClientSize = new System.Drawing.Size(626, 343);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_GC_Snow_Callout_Block_Add_Edit";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Snow Clearance Callout Wizard - Block Edit";
            this.Load += new System.EventHandler(this.frm_GC_Snow_Callout_Block_Add_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ClientHowSoonIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04165GCClientHowSoonTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardSellCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonStandardCostCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorContactedByStaffIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETADateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorETADateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCPONumberSuffixTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCPONumberSuffix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorETA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNonStandardSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientHowSoonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorContactedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursWorkedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursWorked)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraEditors.TextEdit GCPONumberSuffixTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGCPONumberSuffix;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraEditors.DateEdit SubContractorETADateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorETA;
        private DevExpress.XtraEditors.GridLookUpEdit SubContractorContactedByStaffIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorContactedByStaffID;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp00226StaffListWithBlankBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter sp00226_Staff_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colNetworkID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType;
        private DevExpress.XtraEditors.TextEdit ClientPONumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.CheckEdit NonStandardSellCheckEdit;
        private DevExpress.XtraEditors.CheckEdit NonStandardCostCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonStandardCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNonStandardSell;
        private DevExpress.XtraEditors.GridLookUpEdit ClientHowSoonIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientHowSoonID;
        private DataSet_GC_Snow_DataEntry dataSet_GC_Snow_DataEntry;
        private System.Windows.Forms.BindingSource sp04165GCClientHowSoonTypesWithBlankBindingSource;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SpinEdit HoursWorkedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHoursWorked;
    }
}

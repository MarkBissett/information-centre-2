using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_GC_Select_Snow_Site_Contract : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedValue = "";
        public int intSelectedID = 0;
        GridHitInfo downHitInfo = null;

        #endregion

        // Important Note: Ensure all Lists are set to Single Selection //
        public frm_GC_Select_Snow_Site_Contract()
        {
            InitializeComponent();
        }

        private void frm_GC_Select_Snow_Site_Contract_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400030;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04059_GC_Gritting_Sites_List_SimpleTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04138_GC_Snow_Clearance_Site_Contract_For_SitesTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                this.sp03042_EP_Client_Manager_List_SimpleTableAdapter.Connection.ConnectionString = strConnectionString;
                this.sp03042_EP_Client_Manager_List_SimpleTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03042_EP_Client_Manager_List_Simple);
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the clients list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        bool internalRowFocusing;

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Sites Available For Selection - Select a Client to see Related Sites";
                    break;
                case "gridView3":
                    message = "No Site Snow Clearance Contracts Available For Selection - Select a Site to see Related Site Snow Clearance Contracts";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                case "gridView2":
                    LoadLinkedData2();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ClientID"])) + ',';
            }

            //Populate Linked Map Links //
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_GC_DataEntry.sp04059_GC_Gritting_Sites_List_Simple.Clear();
            }
            else
            {
                try
                {
                    sp04059_GC_Gritting_Sites_List_SimpleTableAdapter.Fill(dataSet_GC_DataEntry.sp04059_GC_Gritting_Sites_List_Simple, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related sites.\n\nTry selecting a client again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void LoadLinkedData2()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["SiteID"])) + ',';
            }

            //Populate Linked Contracts Links //
            gridControl3.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_GC_Snow_Core.sp04138_GC_Snow_Clearance_Site_Contract_For_Sites.Clear();
            }
            else
            {
                try
                {
                    sp04138_GC_Snow_Clearance_Site_Contract_For_SitesTableAdapter.Fill(dataSet_GC_Snow_Core.sp04138_GC_Snow_Clearance_Site_Contract_For_Sites, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), null, null, (OnlyShowActiveCheckEdit.Checked ? 1 : 0));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Site Gritting Contracts.\n\nTry selecting a Site again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl3.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            LoadLinkedData2();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl3.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedValue = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteName"))) ? "Unknown Site" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteName"))) + "  [" +
                    (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "StartDate"))) ? "No Start" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "StartDate"))) + " - " +
                    (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "EndDate"))) ? "No End" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "EndDate"))) + "]  Active = " +
                    (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "Active")) == 0 ? "No" : "Yes");
                intSelectedID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SnowClearanceSiteContractID"));
            }
        }


    
    
    
    
    }
}


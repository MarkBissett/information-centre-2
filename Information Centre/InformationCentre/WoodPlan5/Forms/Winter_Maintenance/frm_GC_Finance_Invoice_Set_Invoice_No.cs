﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Finance_Invoice_Set_Invoice_No : WoodPlan5.frmBase_Modal
    {

        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strClientInvoiceDetails = null;
        public string strInvoiceNumber = null;
        public int? intSentToAccounts = null;
        public int? intSentToClientMethodID = null;
        public DateTime? dtDateTimeSent = null;
        public int? intAudited = null;
        public int? intOnHoldReasonID = null;
        public string strRemarks = null;

        #endregion
        
        public frm_GC_Finance_Invoice_Set_Invoice_No()
        {
            InitializeComponent();
        }

        private void frm_GC_Finance_Invoice_Set_Invoice_No_Load(object sender, EventArgs e)
        {
            this.FormID = 400060;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            sp04371_WM_Sent_To_Client_MethodsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04371_WM_Sent_To_Client_MethodsTableAdapter.Fill(dataSet_GC_Core.sp04371_WM_Sent_To_Client_Methods, 1);

            sp04372_WM_Sent_To_Client_On_Hold_ReasonsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04372_WM_Sent_To_Client_On_Hold_ReasonsTableAdapter.Fill(dataSet_GC_Core.sp04372_WM_Sent_To_Client_On_Hold_Reasons, 1);

            ClientInvoiceLineTextEdit.EditValue = strClientInvoiceDetails;
            InvoiceNumberTextEdit.EditValue = strInvoiceNumber;
            SentToAccountsCheckEdit.EditValue = intSentToAccounts;
            SentToClientMethodIDGridLookUpEdit.EditValue = intSentToClientMethodID;
            DateTimeSentDateEdit.EditValue = dtDateTimeSent;
            AuditedCheckEdit.EditValue = intAudited;
            OnHoldReasonIDGridLookUpEdit.EditValue = intOnHoldReasonID;
            RemarksMemoEdit.EditValue = strRemarks;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (InvoiceNumberTextEdit.EditValue != null) strInvoiceNumber = InvoiceNumberTextEdit.EditValue.ToString();
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
            if (SentToAccountsCheckEdit.EditValue != null) intSentToAccounts = Convert.ToInt32(SentToAccountsCheckEdit.EditValue);
            if (SentToClientMethodIDGridLookUpEdit.EditValue != null) intSentToClientMethodID = Convert.ToInt32(SentToClientMethodIDGridLookUpEdit.EditValue);
            if (DateTimeSentDateEdit.EditValue != null) dtDateTimeSent = Convert.ToDateTime(DateTimeSentDateEdit.EditValue);
            if (AuditedCheckEdit.EditValue != null) intAudited = Convert.ToInt32(AuditedCheckEdit.EditValue);
            if (OnHoldReasonIDGridLookUpEdit.EditValue != null) intOnHoldReasonID = Convert.ToInt32(OnHoldReasonIDGridLookUpEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


    }
}

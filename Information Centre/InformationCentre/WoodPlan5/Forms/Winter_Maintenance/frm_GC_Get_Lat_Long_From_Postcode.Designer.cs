﻿namespace WoodPlan5
{
    partial class frm_GC_Get_Lat_Long_From_Postcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04353GCFindPostcodesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEasting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNorthing = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGridRef = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCounty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrict = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWardCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConstituency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIntroduced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTerminated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.textEditPostcode = new DevExpress.XtraEditors.TextEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp04353_GC_Find_PostcodesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04353_GC_Find_PostcodesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04353GCFindPostcodesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPostcode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(539, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 368);
            this.barDockControlBottom.Size = new System.Drawing.Size(539, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 342);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(539, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 342);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp04353GCFindPostcodesBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(1, 85);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDate});
            this.gridControl1.Size = new System.Drawing.Size(538, 247);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04353GCFindPostcodesBindingSource
            // 
            this.sp04353GCFindPostcodesBindingSource.DataMember = "sp04353_GC_Find_Postcodes";
            this.sp04353GCFindPostcodesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPostcode,
            this.colLatitude,
            this.colLongitude,
            this.colEasting,
            this.colNorthing,
            this.colGridRef,
            this.colCounty,
            this.colDistrict,
            this.colWard,
            this.colDistrictCode,
            this.colWardCode,
            this.colCountry,
            this.colCountyCode,
            this.colConstituency,
            this.colIntroduced,
            this.colTerminated});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPostcode, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.FixedWidth = true;
            this.colPostcode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 0;
            this.colPostcode.Width = 83;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.FixedWidth = true;
            this.colLatitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatitude.Visible = true;
            this.colLatitude.VisibleIndex = 1;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.FixedWidth = true;
            this.colLongitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLongitude.Visible = true;
            this.colLongitude.VisibleIndex = 2;
            // 
            // colEasting
            // 
            this.colEasting.Caption = "Easting";
            this.colEasting.FieldName = "Easting";
            this.colEasting.Name = "colEasting";
            this.colEasting.OptionsColumn.AllowEdit = false;
            this.colEasting.OptionsColumn.AllowFocus = false;
            this.colEasting.OptionsColumn.FixedWidth = true;
            this.colEasting.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEasting.Visible = true;
            this.colEasting.VisibleIndex = 3;
            // 
            // colNorthing
            // 
            this.colNorthing.Caption = "Northing";
            this.colNorthing.FieldName = "Northing";
            this.colNorthing.Name = "colNorthing";
            this.colNorthing.OptionsColumn.AllowEdit = false;
            this.colNorthing.OptionsColumn.AllowFocus = false;
            this.colNorthing.OptionsColumn.FixedWidth = true;
            this.colNorthing.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNorthing.Visible = true;
            this.colNorthing.VisibleIndex = 4;
            // 
            // colGridRef
            // 
            this.colGridRef.Caption = "Grid Reference";
            this.colGridRef.FieldName = "GridRef";
            this.colGridRef.Name = "colGridRef";
            this.colGridRef.OptionsColumn.AllowEdit = false;
            this.colGridRef.OptionsColumn.AllowFocus = false;
            this.colGridRef.OptionsColumn.FixedWidth = true;
            this.colGridRef.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGridRef.Visible = true;
            this.colGridRef.VisibleIndex = 5;
            this.colGridRef.Width = 102;
            // 
            // colCounty
            // 
            this.colCounty.Caption = "County";
            this.colCounty.FieldName = "County";
            this.colCounty.Name = "colCounty";
            this.colCounty.OptionsColumn.AllowEdit = false;
            this.colCounty.OptionsColumn.AllowFocus = false;
            this.colCounty.OptionsColumn.FixedWidth = true;
            this.colCounty.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCounty.Visible = true;
            this.colCounty.VisibleIndex = 6;
            // 
            // colDistrict
            // 
            this.colDistrict.Caption = "District";
            this.colDistrict.FieldName = "District";
            this.colDistrict.Name = "colDistrict";
            this.colDistrict.OptionsColumn.AllowEdit = false;
            this.colDistrict.OptionsColumn.AllowFocus = false;
            this.colDistrict.OptionsColumn.FixedWidth = true;
            this.colDistrict.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistrict.Visible = true;
            this.colDistrict.VisibleIndex = 7;
            // 
            // colWard
            // 
            this.colWard.Caption = "Ward";
            this.colWard.FieldName = "Ward";
            this.colWard.Name = "colWard";
            this.colWard.OptionsColumn.AllowEdit = false;
            this.colWard.OptionsColumn.AllowFocus = false;
            this.colWard.OptionsColumn.FixedWidth = true;
            this.colWard.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWard.Visible = true;
            this.colWard.VisibleIndex = 8;
            // 
            // colDistrictCode
            // 
            this.colDistrictCode.Caption = "District Code";
            this.colDistrictCode.FieldName = "DistrictCode";
            this.colDistrictCode.Name = "colDistrictCode";
            this.colDistrictCode.OptionsColumn.AllowEdit = false;
            this.colDistrictCode.OptionsColumn.AllowFocus = false;
            this.colDistrictCode.OptionsColumn.FixedWidth = true;
            this.colDistrictCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistrictCode.Visible = true;
            this.colDistrictCode.VisibleIndex = 11;
            this.colDistrictCode.Width = 88;
            // 
            // colWardCode
            // 
            this.colWardCode.Caption = "Ward Code";
            this.colWardCode.FieldName = "WardCode";
            this.colWardCode.Name = "colWardCode";
            this.colWardCode.OptionsColumn.AllowEdit = false;
            this.colWardCode.OptionsColumn.AllowFocus = false;
            this.colWardCode.OptionsColumn.FixedWidth = true;
            this.colWardCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWardCode.Visible = true;
            this.colWardCode.VisibleIndex = 12;
            // 
            // colCountry
            // 
            this.colCountry.Caption = "Country";
            this.colCountry.FieldName = "Country";
            this.colCountry.Name = "colCountry";
            this.colCountry.OptionsColumn.AllowEdit = false;
            this.colCountry.OptionsColumn.AllowFocus = false;
            this.colCountry.OptionsColumn.FixedWidth = true;
            this.colCountry.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCountry.Visible = true;
            this.colCountry.VisibleIndex = 9;
            // 
            // colCountyCode
            // 
            this.colCountyCode.Caption = "County Code";
            this.colCountyCode.FieldName = "CountyCode";
            this.colCountyCode.Name = "colCountyCode";
            this.colCountyCode.OptionsColumn.AllowEdit = false;
            this.colCountyCode.OptionsColumn.AllowFocus = false;
            this.colCountyCode.OptionsColumn.FixedWidth = true;
            this.colCountyCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCountyCode.Visible = true;
            this.colCountyCode.VisibleIndex = 10;
            this.colCountyCode.Width = 85;
            // 
            // colConstituency
            // 
            this.colConstituency.Caption = "Constituency";
            this.colConstituency.FieldName = "Constituency";
            this.colConstituency.Name = "colConstituency";
            this.colConstituency.OptionsColumn.AllowEdit = false;
            this.colConstituency.OptionsColumn.AllowFocus = false;
            this.colConstituency.OptionsColumn.FixedWidth = true;
            this.colConstituency.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colConstituency.Visible = true;
            this.colConstituency.VisibleIndex = 13;
            this.colConstituency.Width = 91;
            // 
            // colIntroduced
            // 
            this.colIntroduced.Caption = "Introduced";
            this.colIntroduced.ColumnEdit = this.repositoryItemTextEditDate;
            this.colIntroduced.FieldName = "Introduced";
            this.colIntroduced.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colIntroduced.Name = "colIntroduced";
            this.colIntroduced.OptionsColumn.AllowEdit = false;
            this.colIntroduced.OptionsColumn.AllowFocus = false;
            this.colIntroduced.OptionsColumn.FixedWidth = true;
            this.colIntroduced.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colIntroduced.Visible = true;
            this.colIntroduced.VisibleIndex = 14;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "d";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colTerminated
            // 
            this.colTerminated.Caption = "Terminated";
            this.colTerminated.ColumnEdit = this.repositoryItemTextEditDate;
            this.colTerminated.FieldName = "Terminated";
            this.colTerminated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTerminated.Name = "colTerminated";
            this.colTerminated.OptionsColumn.AllowEdit = false;
            this.colTerminated.OptionsColumn.AllowFocus = false;
            this.colTerminated.OptionsColumn.FixedWidth = true;
            this.colTerminated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colTerminated.Visible = true;
            this.colTerminated.VisibleIndex = 15;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 13);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Postcode:";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.textEditPostcode);
            this.groupControl1.Location = new System.Drawing.Point(1, 27);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(538, 52);
            this.groupControl1.TabIndex = 8;
            this.groupControl1.Text = "Enter Postcode and click Search (for partial searches surround value with %)";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(205, 23);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(163, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search for Latitude \\ Longitude";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // textEditPostcode
            // 
            this.textEditPostcode.Location = new System.Drawing.Point(59, 26);
            this.textEditPostcode.MenuManager = this.barManager1;
            this.textEditPostcode.Name = "textEditPostcode";
            this.textEditPostcode.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditPostcode, true);
            this.textEditPostcode.Size = new System.Drawing.Size(140, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditPostcode, optionsSpelling1);
            this.textEditPostcode.TabIndex = 6;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(374, 339);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(457, 339);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp04353_GC_Find_PostcodesTableAdapter
            // 
            this.sp04353_GC_Find_PostcodesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Get_Lat_Long_From_Postcode
            // 
            this.ClientSize = new System.Drawing.Size(539, 368);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.gridControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_GC_Get_Lat_Long_From_Postcode";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Get Latitude \\ Longitude From Postcode";
            this.Load += new System.EventHandler(this.frm_GC_Get_Lat_Long_From_Postcode_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04353GCFindPostcodesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPostcode.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.TextEdit textEditPostcode;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private System.Windows.Forms.BindingSource sp04353GCFindPostcodesBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colEasting;
        private DevExpress.XtraGrid.Columns.GridColumn colNorthing;
        private DevExpress.XtraGrid.Columns.GridColumn colGridRef;
        private DevExpress.XtraGrid.Columns.GridColumn colCounty;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrict;
        private DevExpress.XtraGrid.Columns.GridColumn colWard;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictCode;
        private DevExpress.XtraGrid.Columns.GridColumn colWardCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCountry;
        private DevExpress.XtraGrid.Columns.GridColumn colCountyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colConstituency;
        private DevExpress.XtraGrid.Columns.GridColumn colIntroduced;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTerminated;
        private DataSet_GC_CoreTableAdapters.sp04353_GC_Find_PostcodesTableAdapter sp04353_GC_Find_PostcodesTableAdapter;
    }
}

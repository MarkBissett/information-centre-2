﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

using DevExpress.XtraGauges;
using DevExpress.XtraGauges.Win;
using DevExpress.XtraGauges.Win.Base;
using DevExpress.XtraGauges.Core;
using DevExpress.XtraGauges.Core.Base;
using DevExpress.XtraGauges.Core.Drawing;
using DevExpress.XtraGauges.Core.Model;
using DevExpress.XtraGauges.Core.Primitive;
using DevExpress.XtraGauges.Base;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Dashboard : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";

        string i_str_selected_Company_ids = "";
        string i_str_selected_Company_names = "";
        BaseObjects.GridCheckMarksSelection selection2;
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        ArrayList ArrayListGauges; 
        IGauge gauge = null;

        #endregion

        public frm_GC_Dashboard()
        {
            InitializeComponent();
        }

        private void frm_GC_Dashboard_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4019;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            // Add record selection checkboxes to popup Company Filter grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            sp04237_GC_Company_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04237_GC_Company_Filter_ListTableAdapter.Fill(dataSet_GC_Reports.sp04237_GC_Company_Filter_List);
            gridControl5.ForceInitialize();

            sp04285_GC_Gritting_DashboardTableAdapter.Connection.ConnectionString = strConnectionString;

            SetDatesAndTimes();

            ArrayListGauges = new ArrayList();
            ArrayListGauges.Add(gaugeControl1);
            ArrayListGauges.Add(gaugeControl2);
            ArrayListGauges.Add(gaugeControl3);
            ArrayListGauges.Add(gaugeControl4);
            ArrayListGauges.Add(gaugeControl5);
            ArrayListGauges.Add(gaugeControl7);
            ArrayListGauges.Add(gaugeControl8);
            ArrayListGauges.Add(gaugeControl11);
            ArrayListGauges.Add(gaugeControl12);
            ArrayListGauges.Add(gaugeControl13);
            ArrayListGauges.Add(gaugeControlJobAcceptance);
            ArrayListGauges.Add(gaugeControlJobCompletion);
            ArrayListGauges.Add(gaugeControlCurrentTime);
            Attach_MouseUp_To_Gauges(ArrayListGauges);  // Attach Enter Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //

            //StyleShader shader = new StyleShader();
            //shader.StyleColor1 = Color.Green;
            //arcScaleNeedleComponent1.Shader = shader;

            SqlDataAdapter sdaSeasonSpan = new SqlDataAdapter();
            DataSet dsSeasonSpan = new DataSet("NewDataSet");
            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            try
            {
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp04286_GC_Gritting_Dashboard_Get_Season_Duration", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                dsSeasonSpan.Clear();  // Remove old values first //
                sdaSeasonSpan = new SqlDataAdapter(cmd);
                sdaSeasonSpan.Fill(dsSeasonSpan, "Table");
                cmd = null;
                if (dsSeasonSpan.Tables[0].Rows.Count == 1)
                {
                    DataRow dr = dsSeasonSpan.Tables[0].Rows[0];
                    dtStart = Convert.ToDateTime(dr["dtFromDate"]);
                    dtEnd = Convert.ToDateTime(dr["dtToDate"]);
                }
            }
            catch (Exception)
            {
            }
            if (string.IsNullOrEmpty(dtStart.ToString())) dtStart = this.GlobalSettings.LiveStartDate;
            if (string.IsNullOrEmpty(dtEnd.ToString())) dtEnd = this.GlobalSettings.LiveEndDate;
            dateEditFromDateSeason.DateTime = Convert.ToDateTime(dtStart);
            dateEditToDateSeason.DateTime = Convert.ToDateTime(dtEnd);
            timer1.Enabled = true;
            timer2.Enabled = true;
        }

        private void Attach_MouseUp_To_Gauges(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GaugeControl)item).MouseUp += new System.Windows.Forms.MouseEventHandler(Gauge_MouseUp);
            }
        }

        private void Detach_MouseUp_From_Gauges(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GaugeControl)item).MouseUp -= new System.Windows.Forms.MouseEventHandler(Gauge_MouseUp);
            }
        }

        private void Gauge_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                GaugeControl gaugeControl = (GaugeControl)sender;
                gauge = GetGaugeAtPoint(gaugeControl, e.Location);
                if (gauge == null) return;
                pmGaugeStyle.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }


        private void SetDatesAndTimes()
        {
            dateEditFromDateJobAcceptance.DateTime = DateTime.Today;
            dateEditToDateJobAcceptance.DateTime = DateTime.Today.AddDays(1).AddMilliseconds(-1);  // Should give todays date + 23:59:59.999 //

            dateEditFromDateReactiveJobs.DateTime = DateTime.Now.AddDays(-1);  // Should give 24 hour span from yesteday up until current date time //
            dateEditToDateReactiveJobs.DateTime = DateTime.Now;

            dateEditFromDateLastNight.DateTime = DateTime.Today.AddDays(-1);  // Should give 24 hour span from start of yesteday up until midnight yesterday //
            dateEditToDateLastNight.DateTime = DateTime.Today.AddMilliseconds(-1);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            SetMenuStatus();
            LoadLastSavedUserScreenSettings();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        private void frm_GC_Dashboard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_GC_Dashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
            timer2.Stop();
            Detach_MouseUp_From_Gauges(ArrayListGauges);  // Detach Mouse Up Event from All Guages... Attached on Form Load Event //
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            /*GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Clients //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }*/

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
            /*
            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }*/
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Company Filter //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("CompanyFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["CompanyID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    barEditItemCompanyFilter.EditValue = PopupContainerEditCompanies_Get_Selected();
                }
            }
            Load_Data();
        }


        private void Load_Data()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            
            timer1.Stop();  // Halt timer while the data is loaded just in case it takes a while and it ovelaps into the tick cycle //
            timer2.Stop();  // Halt timer while the data is loaded just in case it takes a while and it ovelaps into the tick cycle //
            DateTime dtDateEditFromDateJobAcceptance = dateEditFromDateJobAcceptance.DateTime;
            DateTime dtDateEditToDateJobAcceptance = dateEditToDateJobAcceptance.DateTime;
            DateTime dtDateEditFromDateReactiveJobs = dateEditFromDateReactiveJobs.DateTime;
            DateTime dtDateEditToDateReactiveJobs = dateEditToDateReactiveJobs.DateTime;
            DateTime dtDateEditFromDateLastNight = dateEditFromDateLastNight.DateTime;
            DateTime dtDateEditToDateLastNight = dateEditToDateLastNight.DateTime;
            DateTime dtDateEditFromDateSeason = dateEditFromDateSeason.DateTime;
            DateTime dtDateEditToDateSeason = dateEditToDateSeason.DateTime;
            DateTime dtCurrentDateTime = DateTime.Now;
            TimeSpan tsCurrentTime = dtCurrentDateTime.TimeOfDay;
            TimeSpan ts9AM = new TimeSpan(9, 0, 0);
            TimeSpan ts3PM = new TimeSpan(15, 0, 0);
            TimeSpan ts7PM = new TimeSpan(19, 0, 0);
            try
            {
                sp04285_GC_Gritting_DashboardTableAdapter.Fill(this.dataSet_GC_Core.sp04285_GC_Gritting_Dashboard, dtDateEditFromDateJobAcceptance, dtDateEditToDateJobAcceptance, dtDateEditFromDateLastNight, dtDateEditToDateLastNight, dtDateEditFromDateReactiveJobs, dtDateEditToDateReactiveJobs, dtDateEditFromDateSeason, dtDateEditToDateSeason, i_str_selected_Company_ids, dtCurrentDateTime);

                if (this.dataSet_GC_Core.sp04285_GC_Gritting_Dashboard.Rows.Count <= 0) return;
                DataRow dr = this.dataSet_GC_Core.sp04285_GC_Gritting_Dashboard.Rows[0];

                circularGauge1.BeginUpdate();
                arcScaleComponentCompletionTotal.MaxValue = Convert.ToInt32(dr["CalloutsCompletedProactiveTotal"]);
                arcScaleComponentCompletion.Value = (float)Convert.ToDecimal(dr["CalloutsCompletedProactivePercentage"]);
                arcScaleComponentCompletionNo.Value = (float)100.00 - (float)Convert.ToDecimal(dr["CalloutsCompletedProactivePercentage"]);
                circularGauge1.EndUpdate();

                digitalGaugeCompletion.BeginUpdate();
                digitalGaugeCompletion.Text = dr["CalloutsCompletedProactive"].ToString() + "/" + dr["CalloutsCompletedProactiveTotal"].ToString();
                digitalGaugeCompletion.EndUpdate();

                circularGauge2.BeginUpdate();
                arcScaleComponentAcceptedTotal.MaxValue = Convert.ToInt32(dr["CalloutsAcceptedProactiveTotal"]);
                arcScaleComponentAccepted.Value = (float)Convert.ToDecimal(dr["CalloutsAcceptedProactivePercentage"]);
                arcScaleComponentAcceptedNo.Value = (float)100.00 - (float)Convert.ToDecimal(dr["CalloutsAcceptedProactivePercentage"]);
                circularGauge2.EndUpdate();

                digitalGaugeAcceptance.BeginUpdate();
                digitalGaugeAcceptance.Text = dr["CalloutsAcceptedProactive"].ToString() + "/" + dr["CalloutsAcceptedProactiveTotal"].ToString();
                digitalGaugeAcceptance.EndUpdate();

                digitalGaugeReactiveTotal.BeginUpdate();
                digitalGaugeReactiveTotal.Text = dr["TotalReactiveCallouts24Hours"].ToString();
                digitalGaugeReactiveTotal.EndUpdate();

                digitalGaugeReactiveRejected.BeginUpdate();
                digitalGaugeReactiveRejected.Text = dr["ReactiveCalloutsRejected24Hours"].ToString();
                digitalGaugeReactiveRejected.EndUpdate();

                digitalGaugeReactiveAccepted.BeginUpdate();
                digitalGaugeReactiveAccepted.Text = dr["ReactiveCalloutsCompleted24Hours"].ToString();
                digitalGaugeReactiveAccepted.EndUpdate();

                digitalGaugeReactiveOutstanding.BeginUpdate();
                digitalGaugeReactiveOutstanding.Text = dr["ReactiveCalloutsOutstanding24Hours"].ToString();
                digitalGaugeReactiveOutstanding.EndUpdate();

                digitalGaugeSeason.BeginUpdate();
                digitalGaugeSeason.Text = dr["TotalSeasonCallouts"].ToString();
                digitalGaugeSeason.EndUpdate();

                digitalGaugeYesterday.BeginUpdate();
                digitalGaugeYesterday.Text = dr["TotalCalloutsLastNight"].ToString();
                digitalGaugeYesterday.EndUpdate();

                if (Convert.ToInt32(dr["WeatherImported"]) > 0)
                {
                    stateIndicatorComponent2.StateIndex = 3;  // Green //
                }
                else if (tsCurrentTime < ts9AM)  // Amber //
                {
                    stateIndicatorComponent2.StateIndex = 2;  // Amber //
                }
                else // Red //
                {
                    stateIndicatorComponent2.StateIndex = 1;  // Red //
                }

                if (Convert.ToInt32(dr["ClientReportsSent"]) > 0)
                {
                    stateIndicatorComponent1.StateIndex = 3;  // Green //
                }
                else if (tsCurrentTime < ts9AM)  // Amber //
                {
                    stateIndicatorComponent1.StateIndex = 2;  // Amber //
                }
                else // Red //
                {
                    stateIndicatorComponent1.StateIndex = 1;  // Red //
                }


                if (Convert.ToInt32(dr["AllTeamsTexted"]) > 0)
                {
                    stateIndicatorComponent3.StateIndex = 3;  // Green //
                }
                else if (tsCurrentTime < ts3PM)  // Amber //
                {
                    stateIndicatorComponent3.StateIndex = 2;  // Amber //
                }
                else // Red //
                {
                    stateIndicatorComponent3.StateIndex = 1;  // Red //
                }

                if (Convert.ToInt32(dr["AllTeamsChased"]) > 0)
                {
                    stateIndicatorComponent4.StateIndex = 3;  // Green //
                }
                else if (tsCurrentTime < ts7PM)  // Amber //
                {
                    stateIndicatorComponent4.StateIndex = 2;  // Amber //
                }
                else // Red //
                {
                    stateIndicatorComponent4.StateIndex = 1;  // Red //
                }

            }
            catch (Exception)
            {
            }
            timer2.Start();  // Restart timer now data refresh is complete //
            timer1.Start();  // Restart timer now data refresh is complete //
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }


        #region Date Filters

        private void btnDateFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void dateEditFromDateJobAcceptance_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDateJobAcceptance.EditValue = null;
                }
            }
        }

        private void dateEditToDateJobAcceptance_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDateJobAcceptance.EditValue = null;
                }
            }
        }

        private void dateEditFromDateReactiveJobs_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDateReactiveJobs.EditValue = null;
                }
            }
        }

        private void dateEditToDateReactiveJobs_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDateReactiveJobs.EditValue = null;
                }
            }
        }

        private void dateEditFromDateLastNight_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDateLastNight.EditValue = null;
                }
            }
        }

        private void dateEditToDateLastNight_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDateLastNight.EditValue = null;
                }
            }
        }

        private void dateEditFromDateSeason_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDateSeason.EditValue = null;
                }
            }
        }

        private void dateEditToDateSeason_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDateSeason.EditValue = null;
                }
            }
        }

        #endregion


        #region Company Filter Panel

        private void repositoryItemPopupContainerEditCompanyFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCompanies_Get_Selected();
        }

        private void btnCompanyFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditCompanies_Get_Selected()
        {
            i_str_selected_Company_ids = "";    // Reset any prior values first //
            i_str_selected_Company_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Company_ids += Convert.ToString(view.GetRowCellValue(i, "CompanyID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Company_names = Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Company_names += ", " + Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Company_names;
        }

        #endregion


        private void timer1_Tick(object sender, EventArgs e)
        {
            // Main form Timer for gauges (every 60 seconds) //
            if (checkEditRecalculateOnTimer.Checked) SetDatesAndTimes();
            Load_Data();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            // Digital clock timer (every second) //
            digitalGaugeCurrentTime.Text = DateTime.Now.ToString("HH:mm:ss");
        }

        private void bbiReload_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (checkEditRecalculateOnTimer.Checked) SetDatesAndTimes();
            Load_Data();
        }

        int CalcSectionIndex(RectangleF bounds, int sectionCount, Point pt)
        {
            if (sectionCount > 0)
            {
                float sectionHeight = bounds.Height / (float)sectionCount;
                for (int i = 0; i < sectionCount; i++)
                {
                    RectangleF section = new RectangleF(
                        bounds.X, bounds.Y + sectionHeight * (float)i, bounds.Width, sectionHeight);
                    if (section.Contains(pt)) return i + 1;
                }
            }
            return -1;
        }

        private void gaugeControl1_MouseClick(object sender, MouseEventArgs e)
        {
            // Forecast Imported //
            if (!iBool_AllowEdit) return;
            BasePrimitiveHitInfo hi = ((IGaugeContainer)gaugeControl1).CalcHitInfo(e.Location);
            if (hi.Element == stateIndicatorComponent2)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to update a Team Metric.\n\nAre you sure you wish to proceed?", "Update Team Metric", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                int index = stateIndicatorComponent2.StateIndex;
                if (index == 0 || index == 1 || index == 2)  // Gray, Red or Amber //
                {
                    index = 3;  // Green //
                }
                else  // Calculate what it should be //
                {
                    TimeSpan tsCurrentTime = DateTime.Now.TimeOfDay;
                    TimeSpan tsDue = new TimeSpan(9, 0, 0);
                    index = (tsCurrentTime < tsDue ? 2 : 1);
                }
                try
                {
                    DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter StoreValue = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                    StoreValue.ChangeConnectionString(strConnectionString);
                    int intSuccess = Convert.ToInt32(StoreValue.sp04287_GC_Gritting_Dashboard_Update_Team_Metric("Forecast", index, DateTime.Now));
                    //if (intSuccess == 0)
                    //{
                    //    DevExpress.XtraEditors.XtraMessageBox.Show("The current Forecast has not been imported into the system yet. Metric Changes cannot be stored until this has been done.\n\nPlease try again once you have imported today's weather forecast.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //    return;
                    //}
                    //else
                    //{
                        stateIndicatorComponent2.StateIndex = index;
                        DevExpress.XtraEditors.XtraMessageBox.Show("Metric Updated Successfully.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //}
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to Update the database with the Team Metrics Change.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void gaugeControl5_MouseClick(object sender, MouseEventArgs e)
        {
            // Client Reports Sent //
            if (!iBool_AllowEdit) return;
            BasePrimitiveHitInfo hi = ((IGaugeContainer)gaugeControl5).CalcHitInfo(e.Location);
            if (hi.Element == stateIndicatorComponent1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to update a Team Metric.\n\nAre you sure you wish to proceed?", "Update Team Metric", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                int index = stateIndicatorComponent1.StateIndex;
                if (index == 0 || index == 1 || index == 2)  // Gray, Red or Amber //
                {
                    index = 3;  // Green //
                }
                else  // Calculate what it should be //
                {
                    TimeSpan tsCurrentTime = DateTime.Now.TimeOfDay;
                    TimeSpan tsDue = new TimeSpan(9, 0, 0);
                    index = (tsCurrentTime < tsDue ? 2 : 1);
                }
                try
                {
                    DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter StoreValue = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                    StoreValue.ChangeConnectionString(strConnectionString);
                    int intSuccess = Convert.ToInt32(StoreValue.sp04287_GC_Gritting_Dashboard_Update_Team_Metric("ClientReportsSent", index, DateTime.Now));
                    if (intSuccess == 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The current Forecast has not been imported into the system yet. Metric Changes cannot be stored until this has been done.\n\nPlease try again once you have imported today's weather forecast.\n\nTIP: The Weather Imported Metric can be clicked to create a Forecast Import to allow you to record the other Metrics.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        stateIndicatorComponent1.StateIndex = index;
                        DevExpress.XtraEditors.XtraMessageBox.Show("Metric Updated Successfully.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to Update the database with the Team Metrics Change.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void gaugeControl4_MouseClick(object sender, MouseEventArgs e)
        {
            // Text 100% of Teams //
            if (!iBool_AllowEdit) return;
            BasePrimitiveHitInfo hi = ((IGaugeContainer)gaugeControl4).CalcHitInfo(e.Location);
            if (hi.Element == stateIndicatorComponent3)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to update a Team Metric.\n\nAre you sure you wish to proceed?", "Update Team Metric", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                int index = stateIndicatorComponent3.StateIndex;
                if (index == 0 || index == 1 || index == 2)  // Gray, Red or Amber //
                {
                    index = 3;  // Green //
                }
                else  // Calculate what it should be //
                {
                    TimeSpan tsCurrentTime = DateTime.Now.TimeOfDay;
                    TimeSpan tsDue = new TimeSpan(15, 0, 0);
                    index = (tsCurrentTime < tsDue ? 2 : 1);
                }
                try
                {
                    DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter StoreValue = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                    StoreValue.ChangeConnectionString(strConnectionString);
                    int intSuccess = Convert.ToInt32(StoreValue.sp04287_GC_Gritting_Dashboard_Update_Team_Metric("AllTeamsTexted", index, DateTime.Now));
                    if (intSuccess == 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The current Forecast has not been imported into the system yet. Metric Changes cannot be stored until this has been done.\n\nPlease try again once you have imported today's weather forecast.\n\nTIP: The Weather Imported Metric can be clicked to create a Forecast Import to allow you to record the other Metrics.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        stateIndicatorComponent3.StateIndex = index;
                        DevExpress.XtraEditors.XtraMessageBox.Show("Metric Updated Successfully.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to Update the database with the Team Metrics Change.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void gaugeControl3_MouseClick(object sender, MouseEventArgs e)
        {
            // Chase Teams //
            if (!iBool_AllowEdit) return;
            BasePrimitiveHitInfo hi = ((IGaugeContainer)gaugeControl3).CalcHitInfo(e.Location);
            if (hi.Element == stateIndicatorComponent4)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to update a Team Metric.\n\nAre you sure you wish to proceed?", "Update Team Metric", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                int index = stateIndicatorComponent4.StateIndex;
                if (index == 0 || index == 1 || index == 2)  // Gray, Red or Amber //
                {
                    index = 3;  // Green //
                }
                else  // Calculate what it should be //
                {
                    TimeSpan tsCurrentTime = DateTime.Now.TimeOfDay;
                    TimeSpan tsDue = new TimeSpan(19, 0, 0);
                    index = (tsCurrentTime < tsDue ? 2 : 1);
                }
                try
                {
                    DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter StoreValue = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                    StoreValue.ChangeConnectionString(strConnectionString);
                    int intSuccess = Convert.ToInt32(StoreValue.sp04287_GC_Gritting_Dashboard_Update_Team_Metric("AllTeamsChased", index, DateTime.Now));
                    if (intSuccess == 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The current Forecast has not been imported into the system yet. Metric Changes cannot be stored until this has been done.\n\nPlease try again once you have imported today's weather forecast.\n\nTIP: The Weather Imported Metric can be clicked to create a Forecast Import to allow you to record the other Metrics.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        stateIndicatorComponent4.StateIndex = index;
                        DevExpress.XtraEditors.XtraMessageBox.Show("Metric Updated Successfully.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to Update the database with the Team Metrics Change.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Update Team Metric", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }



        IGauge GetGaugeAtPoint(GaugeControl gc, Point p)
        {
            foreach (IGauge gauge in gc.Gauges)
            {
                if (gauge.Bounds.Contains(p))
                {
                    return gauge;
                }
            }
            return null;
        }

        private void bbiStyleChooser_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (gauge == null) return;
            StyleChooser.Show(gauge);
            if (gauge.Name == "circularGauge2")
            {
                ResetGaugeControlJobAcceptanceInnerScaleStyling();
            }
            else if (gauge.Name == "circularGauge1")
            {
                ResetGaugeControlJobCompletedInnerScaleStyling();
            }
        }

        private void bbiStyleManager_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (gauge == null) return;
            StyleManager.Show(gauge.Container);
            if (gauge.Name == "circularGauge2")
            {
                ResetGaugeControlJobAcceptanceInnerScaleStyling();
            }
            else if (gauge.Name == "circularGauge1")
            {
                ResetGaugeControlJobCompletedInnerScaleStyling();
            }
        }


        private void ResetGaugeControlJobAcceptanceInnerScaleStyling()
        {
            circularGauge2.BeginUpdate();
            arcScaleComponentAcceptedNo.RadiusX = 70;// arcScaleComponentAcceptedNo.RadiusX - 52;
            arcScaleComponentAcceptedNo.RadiusY = 70;// arcScaleComponentAcceptedNo.RadiusY - 52;

            arcScaleNeedleComponentAcceptedNo.ShapeType = NeedleShapeType.CircularFull_ClockSecond;
            circularGauge2.EndUpdate();
        }

        private void ResetGaugeControlJobCompletedInnerScaleStyling()
        {
            circularGauge1.BeginUpdate();
            arcScaleComponentCompletionNo.RadiusX = 70;// arcScaleComponentAcceptedNo.RadiusX - 52;
            arcScaleComponentCompletionNo.RadiusY = 70;// arcScaleComponentAcceptedNo.RadiusY - 52;

            arcScaleNeedleComponentCompletionNo.ShapeType = NeedleShapeType.CircularFull_ClockSecond;
            circularGauge1.EndUpdate();
        }



    }
}

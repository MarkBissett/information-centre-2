﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Reports;

namespace WoodPlan5
{
    public partial class frm_GC_Snow_Authorise_Callouts : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
 
        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        Boolean iBoolPreventChildDataLoading = false;

        public string i_str_PDFDirectoryName = "";

        Bitmap bmpBlank = null;
        Bitmap bmpAlert = null;

        #endregion

        public frm_GC_Snow_Authorise_Callouts()
        {
            InitializeComponent();
        }

        private void frm_GC_Snow_Authorise_Callouts_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4011;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            dateEditFromDate.DateTime = DateTime.Today.AddDays(-7);  // Last week only //
            dateEditToDate.DateTime = DateTime.Today.AddDays(1).AddSeconds(-1);  // Should give todays date + 23:59:59 //
            sp04197_GC_Snow_Callout_Manager_For_AuthorisationTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "SnowClearanceCallOutID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Load_Data() - performed by LoadLastSavedUserScreenSettings() //
            
            bmpBlank = new Bitmap(16, 16);
            bmpAlert = new Bitmap(imageCollection1.Images[3], 16, 16);
                          
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_PDFDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "SnowClearanceTeamPOPDFPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Saved Team Purchase Order PDFs (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Saved Team Purchase Order PDF Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_PDFDirectoryName.EndsWith("\\")) i_str_PDFDirectoryName += "\\";  // Add Backslash to end //
            
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }

        private void frm_GC_Snow_Authorise_Callouts_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
            }
            SetMenuStatus();
        }

        private void frm_GC_Snow_Authorise_Callouts_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShowQueries", CheckEditShowQueries.EditValue.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShowDontPay", CheckEditShowDontPay.EditValue.ToString());
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            switch (i_int_FocusedGrid)
            {
                case 1:    // Gritting Callouts //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        /*if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                            bbiBlockAdd.Enabled = true;
                        }*/
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        /*if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }*/
                    }
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys != Keys.Control)
                {
                    // Show Querys //
                    string strShowQueries = default_screen_settings.RetrieveSetting("ShowQueries");
                    if (!string.IsNullOrEmpty(strShowQueries)) CheckEditShowQueries.EditValue = Convert.ToInt32(strShowQueries);

                    // Show Dont Pay //
                    string strShowDontPay = default_screen_settings.RetrieveSetting("ShowDontPay");
                    if (!string.IsNullOrEmpty(strShowDontPay)) CheckEditShowDontPay.EditValue = Convert.ToInt32(strShowDontPay);
                }
            }
            Load_Data();
        }

        private void bbiLoadData_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            WaitDialogForm loading = new WaitDialogForm("Loading Snow Clearance Callouts...", "Snow Clearance Callout Authorisation Manager");
            if (fProgress == null)
            {
                loading.Show();
            }
            else
            {
                loading.Hide();
                loading.Dispose();
            }

            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            int intShowQuery = Convert.ToInt32(CheckEditShowQueries.EditValue);
            int intShowDoNotPay = Convert.ToInt32(CheckEditShowDontPay.EditValue);
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            gridControl1.BeginUpdate();
            sp04197_GC_Snow_Callout_Manager_For_AuthorisationTableAdapter.Fill(dataSet_GC_Snow_DataEntry.sp04197_GC_Snow_Callout_Manager_For_Authorisation, intShowQuery, intShowDoNotPay, dtFromDate, dtToDate);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SnowClearanceCallOutID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
            if (fProgress == null)
            {
                loading.Close();
                loading.Dispose();
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
         }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Snow Clearance Callouts Available - Try adjusting the Date Range");
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "TeamPOFileName":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "TeamPOFileName").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "TeamPOFileName":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("TeamPOFileName").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "TeamPOFileName").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No PDF File Linked - unable to proceed.", "View Linked PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(i_str_PDFDirectoryName + strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + i_str_PDFDirectoryName + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        //Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Alert" && e.IsGetData)
            {
                e.Value = bmpBlank;
                GridView view = (GridView)sender;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                if (Convert.ToInt32(view.GetRowCellValue(rHandle, "DoNotPaySubContractor")) == 1) e.Value = bmpAlert;
            }
        }

        #endregion


        #region DateRange

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }
        
        #endregion


        #region Authorise

        private void bbiAuthorise1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Authorise_Callout(1);
        }

        private void bbiAuthorise2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Authorise_Callout(1);
        }

        private void bbiUnAuthorise_ItemClick(object sender, ItemClickEventArgs e)
        {
            Authorise_Callout(0);
        }

        private void Authorise_Callout(int intAuthorise)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intAuthorise == 1 ? "Authorise" : "Unauthorise") + " before proceeding.", "Authorise\\Unauthorise Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            if (intAuthorise == 1)
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // De-select any selected record currently set as Authorised as this process will set selected records to this value //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 160) view.UnselectRow(intRowHandles[i]);

                    int intTimesheetSubmitted = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "TimesheetSubmitted"));
                    if (intTimesheetSubmitted <= 0) view.UnselectRow(intRowHandles[i]);

                    string strTeamInvoiceNumber = view.GetRowCellValue(intRowHandles[i], "TeamInvoiceNumber").ToString();
                    if (string.IsNullOrEmpty(strTeamInvoiceNumber)) view.UnselectRow(intRowHandles[i]);

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            else // De-select any records with a status of 120 (Work Completed) as this process will set selected records to this value //
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Authorised as they have already been authorised //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 120) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }

            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intAuthorise == 1 ? "Authorise" : "Unauthorise") + " before proceeding.\n\nNote: This process only updates callouts which don't already have this status.\nIn addition, callouts must have their Timesheet Submitted tickbox ticked and a valid Team Invoice Number.\n\nAny callouts not matching these criteria are automatically de-selected.", "Authorise\\Unauthorise Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to update so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " selected for " + (intAuthorise == 1 ? "Authorisation" : "Unauthorisation") + ".\n\nProceed?", "Authorise\\Unauthorise Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption((intAuthorise == 1 ? "Authorising" : "Unauthorising") + " Callouts...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;
            
            string strCalloutIDs = "";
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                strCalloutIDs += view.GetRowCellValue(intRowHandles[i], "SnowClearanceCallOutID").ToString() + ",";
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            try
            {
                DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter AuthoriseCallouts = new DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter();
                AuthoriseCallouts.ChangeConnectionString(strConnectionString);
                AuthoriseCallouts.sp04198_GC_Snow_Authorise_Callouts(strCalloutIDs, intAuthorise);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while " + (intAuthorise == 1 ? "Authorising" : "Unauthorising") + " the selected callouts - [" + ex.Message + "]\n\nTry running the process again, if the problem persists please contact Technical Support.", "Authorise\\Unauthorise Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " updated successfully.", "Authorise\\Unauthorise Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        #endregion


        #region Query

        private void bbiQuery1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Query_Callout(1);
        }

        private void bbiQuery2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Query_Callout(1);
        }

        private void bbiQueryClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            Query_Callout(0);
        }

        private void Query_Callout(int intQuery)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intQuery == 1 ? "Query" : "Clear the Query of") + " before proceeding.", "Query\\Clear Query Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            if (intQuery == 1)
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Queried as this process will set selected records to this value //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 170) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            else // Deselect any records with a status of 170 (Work Queried) as this process will set selected records to this value //
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Authorised as they have already been authorised //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 120) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }

            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intQuery == 1 ? "Query" : "Clear the Query of") + " before proceeding.\n\nNote: This process only updates callouts which don't already have this status. Any other callouts are automatically de-selected.", "Query\\Clear Query Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to update so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " selected for " + (intQuery == 1 ? "Querying" : "Clearing the Query") + ".\n\nProceed?", "Query\\Clear Query Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption((intQuery == 1 ? "Querying" : "Clearing Query") + " Callouts...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            string strCalloutIDs = "";
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                strCalloutIDs += view.GetRowCellValue(intRowHandles[i], "SnowClearanceCallOutID").ToString() + ",";
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            try
            {
                DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter QueryCallouts = new DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter();
                QueryCallouts.ChangeConnectionString(strConnectionString);
                QueryCallouts.sp04199_GC_Snow_Query_Callouts(strCalloutIDs, intQuery);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while " + (intQuery == 1 ? "Querying" : "Clearing the Query of") + " the selected callouts - [" + ex.Message + "]\n\nTry running the process again, if the problem persists please contact Technical Support.", "Query\\Clear Query Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " updated successfully.", "Query\\Clear Query Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        #endregion


        #region Don't Pay

        private void bbiDontPay1_ItemClick(object sender, ItemClickEventArgs e)
        {
            DontPay_Callout(1);
        }

        private void bbiDontPay2_ItemClick(object sender, ItemClickEventArgs e)
        {
            DontPay_Callout(1);
        }

        private void bbiDoPay_ItemClick(object sender, ItemClickEventArgs e)
        {
            DontPay_Callout(0);
        }

        private void DontPay_Callout(int intDontPay)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intDontPay == 1 ? "set as Don't Pay" : "Clear Don't Pay Flag") + " before proceeding.", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            if (intDontPay == 1)
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Queried as this process will set selected records to this value //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 170) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            else // Deselect any records with a status of 170 (Work Queried) as this process will set selected records to this value //
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Authorised as they have already been authorised //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 120) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }

            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intDontPay == 1 ? "set as Don't Pay" : "Clear Don't Pay Flag") + " before proceeding.\n\nNote: This process only updates callouts which don't already have this status. Any other callouts are automatically de-selected.", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to update so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " selected for " + (intDontPay == 1 ? "setting as Don't Pay" : "Clearing Don't Pay Flag") + ".\n\nProceed?", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            string strDontPayTeamReason = "";
            int intDontInvoiceClient = 0;
            string strDontInvoiceClientReason = "";
            int intClearDontChargeClient = 0;

            if (intDontPay == 1)
            {
                // Open Data entry screen so user can enter further info...//
                frm_GC_Team_Self_Billing_Invoice_Dont_Pay fChildForm = new frm_GC_Team_Self_Billing_Invoice_Dont_Pay();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() != DialogResult.OK) return;
                strDontPayTeamReason = fChildForm.strDontPayTeamReason;
                intDontInvoiceClient = fChildForm.intDontInvoiceClient; ;
                strDontInvoiceClientReason = fChildForm.strDontInvoiceClientReason;
            }
            else  // Clearing Don't Pay Flag so see if user wants to clear any don't pay Client setings //
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are Clearing the Don't Pay Flag - do you also wish to clear any Don't Charge Client details?", "Clear Don't Pay Flag on Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) intClearDontChargeClient = 1;
            }

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption((intDontPay == 1 ? "Setting Don't Pay" : "Clearing Don't Pay") + " Callouts...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            string strCalloutIDs = "";
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                strCalloutIDs += view.GetRowCellValue(intRowHandles[i], "SnowClearanceCallOutID").ToString() + ",";
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            try
            {
                DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter DontPayCallouts = new DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter();
                DontPayCallouts.ChangeConnectionString(strConnectionString);
                DontPayCallouts.sp04200_GC_Snow_Dont_Pay_Callouts(strCalloutIDs, intDontPay, strDontPayTeamReason, intDontInvoiceClient, strDontInvoiceClientReason, intClearDontChargeClient);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while " + (intDontPay == 1 ? "setting as Don't Pay" : "Clearing Don't Pay Flag") + " on the selected callouts - [" + ex.Message + "]\n\nTry running the process again, if the problem persists please contact Technical Support.", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " updated successfully.", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion


        #region Set Team Invoice Number

        private void bbiTeamInvoiceNo1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Team_Invoice_Number(1);
        }

        private void bbiTeamInvcoiceNo2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Team_Invoice_Number(1);
        }

        private void bbiTeamInvoiceNoClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            Team_Invoice_Number(0);
        }

        private void Team_Invoice_Number(int intSetInvoiceNumber)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intSetInvoiceNumber == 1 ? "set the Invoice Number" : "Clear the Invoice Number") + " before proceeding.", "Set\\Clear Invoice Number on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            if (intSetInvoiceNumber == 1)
            {
                // Do nothing //
            }
            else
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // De-select any selected record currently set as Completed as invoice cannot be removed unless the record is marked as not completed first //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 160) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }

            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intSetInvoiceNumber == 1 ? "set the Invoice Number" : "Clear the Invoice Number") + " before proceeding.\n\nNote: This process only updates callouts which don't already have this status. Any other callouts are automatically de-selected.", "Set\\Clear Invoice Number on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to update so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " selected for " + (intSetInvoiceNumber == 1 ? "setting the Invoice Number" : "Clearing the Invoice Number") + ".\n\nProceed?", "Set\\Clear Invoice Number on Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            string strTeamInvoiceNumber = "";
  
            if (intSetInvoiceNumber == 1)
            {
                // Open Data entry screen so user can enter further info...//
                frm_GC_Snow_Authorise_Callouts_Set_Invoice_No fChildForm = new frm_GC_Snow_Authorise_Callouts_Set_Invoice_No();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() != DialogResult.OK) return;
                strTeamInvoiceNumber = fChildForm.strTeamInvoiceNumber;
            }

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption((intSetInvoiceNumber == 1 ? "Setting Invoice Number" : "Clearing Invoice Number") + "...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            string strCalloutIDs = "";
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                strCalloutIDs += view.GetRowCellValue(intRowHandles[i], "SnowClearanceCallOutID").ToString() + ",";
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            try
            {
                DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter DontPayCallouts = new DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter();
                DontPayCallouts.ChangeConnectionString(strConnectionString);
                DontPayCallouts.sp04202_GC_Snow_Callout_Set_Team_Invoice_No(strCalloutIDs, strTeamInvoiceNumber);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while " + (intSetInvoiceNumber == 1 ? "setting the Invoice Number" : "Clearing the Invoice Number") + " on the selected callouts - [" + ex.Message + "]\n\nTry running the process again, if the problem persists please contact Technical Support.", "Set\\Clear Invoice Number on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " updated successfully.", "Set\\Clear Invoice Number on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        #endregion


        #region Timesheet Submitted

        private void bbiTimesheetSubmitted_ItemClick(object sender, ItemClickEventArgs e)
        {
            Timesheet_Submitted(1);
        }

        private void TimesheetSubmitted2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Timesheet_Submitted(1);
        }

        private void TimesheetSubmittedClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            Timesheet_Submitted(0);
        }
        
        private void Timesheet_Submitted(int intSubmitted)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intSubmitted == 1 ? "mark as Timesheet Submitted" : "clear Timesheet Submitted") + " before proceeding.", "Set\\Clear Timesheet Submitted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intTimesheetSubmitted = 0;
            if (intSubmitted == 1)
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // De-select any selected record with flag set //
                {
                    intTimesheetSubmitted = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "TimesheetSubmitted"));
                    if (intTimesheetSubmitted > 0) view.UnselectRow(intRowHandles[i]);

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            else // De-select any records with flag set to dalse as this process will set the flag to false //
            {
                for (int i = 0; i < intRowHandles.Length; i++)
                {
                    intTimesheetSubmitted = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "TimesheetSubmitted"));
                    if (intTimesheetSubmitted == 0) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intSubmitted == 1 ? "mark as Timesheet Submitted" : "clear Timesheet Submitted") + " before proceeding.\n\nNote: This process only updates callouts which don't already have this status.", "Set\\Clear Timesheet Submitted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to update so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " selected for " + (intSubmitted == 1 ? "mark as Timesheet Submitted" : "clear Timesheet Submitted") + ".\n\nProceed?", "Set\\Clear Timesheet Submitted", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption((intSubmitted == 1 ? "Mark Timesheet Submitted" : "Clear Timesheet Submitted") + "...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            string strCalloutIDs = "";
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                strCalloutIDs += view.GetRowCellValue(intRowHandles[i], "SnowClearanceCallOutID").ToString() + ",";
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            try
            {
                DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter ToggleTimesheetSubmitted = new DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter();
                ToggleTimesheetSubmitted.ChangeConnectionString(strConnectionString);
                ToggleTimesheetSubmitted.sp04201_GC_Snow_Callout_Toggle_Timesheet_Submitted(strCalloutIDs, intSubmitted);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while " + (intSubmitted == 1 ? "Marking Timesheet Submitted" : "Clearing Timesheet Submitted") + " for the selected callouts - [" + ex.Message + "]\n\nTry running the process again, if the problem persists please contact Technical Support.", "Set\\Clear Timesheet Submitted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " updated successfully.", "Set\\Clear Timesheet Submitted", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        

        #endregion


        private void barManager1_HighlightedLinkChanged(object sender, HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo { Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip };

            toolTipController1.ShowHint(Info);
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            if (!iBool_AllowEdit) return;
            view = (GridView)gridControl1.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ',';
            }
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            frm_GC_Snow_Callout_Edit fChildForm = new frm_GC_Snow_Callout_Edit();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = strRecordIDs;
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = intCount;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.fProgress = fProgress;
            fChildForm.Show();

            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            if (!iBool_AllowEdit) return;
            view = (GridView)gridControl1.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ',';
            }
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            frm_GC_Snow_Callout_Edit fChildForm = new frm_GC_Snow_Callout_Edit();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = strRecordIDs;
            fChildForm.strFormMode = "blockedit";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = intCount;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.fProgress = fProgress;
            fChildForm.Show();

            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

 



    }
}
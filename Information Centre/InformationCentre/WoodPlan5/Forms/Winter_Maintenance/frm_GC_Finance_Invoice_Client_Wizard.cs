﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Finance_Invoice_Client_Wizard : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        BaseObjects.GridCheckMarksSelection selection4;

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        string i_str_selected_Company_ids = "";
        string i_str_selected_Company_names = "";

        #endregion

        public frm_GC_Finance_Invoice_Client_Wizard()
        {
            InitializeComponent();
        }

        private void frm_GC_Finance_Invoice_Client_Wizard_Load(object sender, EventArgs e)
        {
            this.FormID = 400061;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;  // Hide Tab Page headers so it doesn't look like a pageframe //
            xtraTabControl1.SelectedTabPage = this.xtraTabPage1;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04254_GC_Finance_Clients_ListsTableAdapter.Connection.ConnectionString = strConnectionString;
            // Add record selection checkboxes to popup Callout Type grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.Fixed = FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            RefreshGridViewState1 = new RefreshGridState(gridView1, "ClientID");

            sp04255_GC_Finance_Grit_Callouts_For_Client_InvoiceTableAdapter.Connection.ConnectionString = strConnectionString;
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.Fixed = FixedStyle.Left;
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            RefreshGridViewState2 = new RefreshGridState(gridView2, "GrittingCalloutID");

            sp04256_GC_Finance_Snow_Callout_Manager_For_Client_InvoiceTableAdapter.Connection.ConnectionString = strConnectionString;
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection3.CheckMarkColumn.Fixed = FixedStyle.Left;
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            RefreshGridViewState3 = new RefreshGridState(gridView4, "SnowClearanceCalloutID");

            // Add record selection checkboxes to popup Company Filter grid control //
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;
            sp04237_GC_Company_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04237_GC_Company_Filter_ListTableAdapter.Fill(dataSet_GC_Reports.sp04237_GC_Company_Filter_List);
            gridControl3.ForceInitialize();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            barEditItemFromDate.EditValue = this.GlobalSettings.LiveStartDate;
            barEditItemToDate.EditValue = this.GlobalSettings.LiveEndDate;

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();
            ConfigureFormAccordingToMode();
            Load_Clients();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }


        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_GC_Finance_Invoice_Client_Wizard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Company Filter //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("CompanyFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl3.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["CompanyID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    barEditItemCompanyFilter.EditValue = PopupContainerEditCompanies_Get_Selected();
                }
            }
        }

        private void frm_GC_Finance_Invoice_Client_Wizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        private void bbiReloadClientGrid_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Clients();
        }

        private void bbiReloadJobs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Jobs();
        }


        #region Navigation Buttons

        private void btnNext1_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
        }

        private void btnPrevious2_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
        }

        private void bntNext2_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            if (selection1.SelectedCount <= 0)
            {
                XtraMessageBox.Show("Select at least 1 client to invoice before proceeding.", "Invoice Client Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            Load_Jobs();
            xtraTabControl1.SelectedTabPage = this.xtraTabPage3;
            if (!splitContainerControl1.Collapsed)
            {
                splitContainerControl1.SplitterPosition = splitContainerControl1.Height / 2;
            }
        }

        private void btnPrevious3_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
        }

        private void btnNext3_Click(object sender, EventArgs e)
        {
            GridView view2 = (GridView)gridControl2.MainView;
            if (!ReferenceEquals(view2.ActiveFilter.Criteria, null)) view2.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view2.FindFilterText)) view2.ApplyFindFilter("");  // Clear any Find in place //         
            
            GridView view4 = (GridView)gridControl4.MainView;
            if (!ReferenceEquals(view4.ActiveFilter.Criteria, null)) view4.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view4.FindFilterText)) view4.ApplyFindFilter("");  // Clear any Find in place //         

            if (selection2.SelectedCount <= 0 && selection3.SelectedCount <= 0)
            {
                XtraMessageBox.Show("Select at least one job from the Gritting and \\ or Snow Clearance Job list before proceeding!", "Invoice Client Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            xtraTabControl1.SelectedTabPage = this.xtraTabPage4;
        }

        private void btnPrevious4_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPage3;
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Creating Invoice Lines...");
            fProgress.Show();
            Application.DoEvents();

            string strGrittingIDs = "";
            if (selection2.SelectedCount > 0)
            {
                GridView view = (GridView)gridControl2.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strGrittingIDs += view.GetRowCellValue(i, "GrittingCallOutID").ToString() + ",";
                    }
                }
            }
            string strSnowClearanceIDs = "";
            if (selection3.SelectedCount > 0)
            {
                GridView view = (GridView)gridControl4.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSnowClearanceIDs += view.GetRowCellValue(i, "SnowClearanceCallOutID").ToString() + ",";
                    }
                }
            }

            string strNewIDs = "";
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter AddRecords = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                AddRecords.ChangeConnectionString(strConnectionString);
                strNewIDs = AddRecords.sp04257_GC_Finance_Create_Client_Invoice_Lines(DateTime.Now, this.GlobalSettings.UserID, strGrittingIDs, strSnowClearanceIDs).ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to generate Client Invoice Lines - message = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Get Linked Callout Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
          
            // Notify any open instances of Finance Invoice Jobs Manager they will need to refresh their data on activating //
            frm_GC_Finance_Invoice_Jobs fParentForm;
            foreach (Form frmChild in this.ParentForm.MdiChildren)
            {
                if (frmChild.Name == "frm_GC_Finance_Invoice_Jobs")
                {
                    fParentForm = (frm_GC_Finance_Invoice_Jobs)frmChild;
                    fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", (checkEdit3.Checked ? "spreadsheets": ""));
                }
            }

            fProgress.SetProgressValue(100);
            fProgress.UpdateCaption("Changes Saved");
            Application.DoEvents();
            System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();

            this.Close();
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Gritting Callouts available for Client Invoicing";
                    break;
                case "gridView4":
                    message = "No Snow Clearance Callouts available for Client Invoicing";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "NextInvoiceDate")
            {
                if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "NextInvoiceDate").ToString())) return;
                DateTime dtToday = DateTime.Today;
                TimeSpan ts1 = dtToday - Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "NextInvoiceDate"));
                if (ts1.Days == 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBF, 0xC0, 0xFF, 0xC0); ;
                    e.Appearance.BackColor2 = Color.FromArgb(0xBE, 0x90, 0xEE, 0x90);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (ts1.Days > 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }
        
        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "UninvoicedGrittingCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "UninvoicedGrittingCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "UninvoicedSnowClearanceCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "UninvoicedSnowClearanceCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }
        
        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "UninvoicedGrittingCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("UninvoicedGrittingCount")) == 0) e.Cancel = true;
                    break;
                case "UninvoicedSnowClearanceCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("UninvoicedSnowClearanceCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strClientIDs = view.GetRowCellValue(view.FocusedRowHandle, "ClientID").ToString() + ",";
            DateTime FromDate = (string.IsNullOrEmpty(barEditItemFromDate.EditValue.ToString()) ? Convert.ToDateTime("2011-01-01") : Convert.ToDateTime(barEditItemFromDate.EditValue));
            DateTime ToDate = (string.IsNullOrEmpty(barEditItemToDate.EditValue.ToString()) ? Convert.ToDateTime("2011-01-01") : Convert.ToDateTime(barEditItemToDate.EditValue));
            switch (view.FocusedColumn.FieldName)
            {
               case "UninvoicedGrittingCount":
                    {
                        DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetData = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                        GetData.ChangeConnectionString(strConnectionString);
                        string strRecordIDs = GetData.sp04077_GC_DrillDown_Get_Linked_Callouts(strClientIDs, "client_invoicing","", FromDate, ToDate).ToString();
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "client_invoicing_gritting");
                    }
                    break;
                case "UninvoicedSnowClearanceCount":
                    {
                        DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter GetData = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
                        GetData.ChangeConnectionString(strConnectionString);
                        string strRecordIDs = GetData.sp04196_GC_Snow_DrillDown_Get_Linked_Callouts(strClientIDs, "client_invoicing","", FromDate, ToDate).ToString();
                       Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "client_invoicing_snow_clearance");
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {

        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView4

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView4_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {

        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Company Filter Panel

        private void repositoryItemPopupContainerEditCompanyFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCompanies_Get_Selected();
        }

        private void btnCompanyFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditCompanies_Get_Selected()
        {
            i_str_selected_Company_ids = "";    // Reset any prior values first //
            i_str_selected_Company_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Company_ids += Convert.ToString(view.GetRowCellValue(i, "CompanyID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Company_names = Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Company_names += ", " + Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Company_names;
        }

        #endregion


        private void Load_Clients()
        {
            WaitDialogForm loading = new WaitDialogForm("Loading Clients...", "Invoice Client Wizard");
            if (fProgress == null)
            {
                loading.Show();
            }
            else
            {
                loading.Hide();
                loading.Dispose();
            }
            DateTime dtFromDate = Convert.ToDateTime(barEditItemFromDate.EditValue);
            DateTime dtToDate = Convert.ToDateTime(barEditItemToDate.EditValue);

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            try
            {
                sp04254_GC_Finance_Clients_ListsTableAdapter.Fill(dataSet_GC_Core.sp04254_GC_Finance_Clients_Lists, DateTime.Today, i_str_selected_Company_ids, dtFromDate, dtToDate);
            }
            catch (Exception)
            {
            }
            view.EndUpdate();
            if (fProgress == null)
            {
                loading.Close();
                loading.Dispose();
            }
        }

        private void Load_Jobs()
        {
            WaitDialogForm loading = new WaitDialogForm("Loading Selected Client Details...", "Invoice Client Wizard");
            loading.Show();

            string strClientIDs = "";
            GridView view = (GridView)gridControl1.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strClientIDs += view.GetRowCellValue(i, "ClientID").ToString() + ",";
                }
            }
            gridControl2.MainView.BeginUpdate();
            try
            {
                sp04255_GC_Finance_Grit_Callouts_For_Client_InvoiceTableAdapter.Fill(dataSet_GC_Core.sp04255_GC_Finance_Grit_Callouts_For_Client_Invoice, strClientIDs, i_str_selected_Company_ids);
            }
            catch (Exception)
            {
            }
            gridControl2.MainView.EndUpdate();
            gridControl4.MainView.BeginUpdate();
            try
            {
                sp04256_GC_Finance_Snow_Callout_Manager_For_Client_InvoiceTableAdapter.Fill(dataSet_GC_Core.sp04256_GC_Finance_Snow_Callout_Manager_For_Client_Invoice, strClientIDs, i_str_selected_Company_ids);
            }
            catch (Exception)
            {
            }
            gridControl4.MainView.EndUpdate();
            loading.Close();
            loading.Dispose();
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more gritting callouts to remove from this Client Invoicing Run by clicking on them then try again.", "Remove Gritting Job from Client Invoice Run", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + (intRowHandles.Length == 1 ? " Gritting Callout" : " Gritting Callouts") + " selected for removal from this Client invoicing Run!\n\nProceed?", "Remove Gritting Job from Client Invoice Run", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(0);
                            fProgress.UpdateCaption("Removing...");
                            fProgress.Show();
                            Application.DoEvents();
                            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                            int intUpdateProgressTempCount = 0;

                            gridControl2.BeginUpdate();
                            for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                            {
                                view.DeleteRow(intRowHandles[intRowHandle]);
                                intUpdateProgressTempCount++;
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    fProgress.UpdateProgress(10);
                                }
                            }
                            gridControl2.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                        }
                    }
                    break;
                case 3:
                    {
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more snow clearance callouts to remove from this Client Invoicing Run by clicking on them then try again.", "Remove Snow Clearance Job from Client Invoice Run", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + (intRowHandles.Length == 1 ? " Snow Clearance Callout" : " Snow Clearance Callouts") + " selected for removal from this Client invoicing Run!\n\nProceed?", "Remove Snow Clearance Job from Client Invoice Run", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(0);
                            fProgress.UpdateCaption("Removing...");
                            fProgress.Show();
                            Application.DoEvents();
                            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                            int intUpdateProgressTempCount = 0;

                            gridControl4.BeginUpdate();
                            for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                            {
                                view.DeleteRow(intRowHandles[intRowHandle]);
                                intUpdateProgressTempCount++;
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    fProgress.UpdateProgress(10);
                                }
                            }
                            gridControl4.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                        }
                    }
                    break;
            }
        }

        private void bbiRotateSplit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            splitContainerControl1.Horizontal = !splitContainerControl1.Horizontal;
            if (!splitContainerControl1.Horizontal)
            {
                splitContainerControl1.Panel1.CaptionLocation = Locations.Left;
                splitContainerControl1.Panel2.CaptionLocation = Locations.Left;
                if (!splitContainerControl1.Collapsed)
                {
                    splitContainerControl1.SplitterPosition = splitContainerControl1.Height / 2;
                }
            }
            else
            {
                splitContainerControl1.Panel1.CaptionLocation = Locations.Top;
                splitContainerControl1.Panel2.CaptionLocation = Locations.Top;
                if (!splitContainerControl1.Collapsed)
                {
                    splitContainerControl1.SplitterPosition = splitContainerControl1.Width / 2;
                }
            }
        }

        private void repositoryItemDateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    barEditItemFromDate.EditValue = null;
                }
            }
        }

        private void repositoryItemDateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    barEditItemToDate.EditValue = null;
                }
            }
        }




 







    }
}
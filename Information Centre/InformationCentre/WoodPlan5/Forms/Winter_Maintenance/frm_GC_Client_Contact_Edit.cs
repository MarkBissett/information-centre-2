using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_GC_Client_Contact_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intTypeID = 0;

        public int intPassedInClientID = 0;
        public string strPassedInClientName = "";
        public int intPassedInClientContactPersonID = 0;
        public string strPassedInPersonName = "";

        #endregion

        public frm_GC_Client_Contact_Edit()
        {
            InitializeComponent();
        }

        private void frm_GC_Client_Contact_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 400003;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;


            sp04010_GC_Contacts_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04010_GC_Contacts_TypesTableAdapter.Fill(dataSet_GC_DataEntry.sp04010_GC_Contacts_Types);      

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            
            sp04011_GC_Linked_Client_Contact_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Populate Main Dataset //
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientID"] = intPassedInClientID;
                        drNewRow["ClientName"] = strPassedInClientName;
                        drNewRow["ContactDetails"] = "";
                        drNewRow["ContactTypeID"] = (intTypeID != 0 ? intTypeID : 0);
                        drNewRow["IncludeForGrittingEmail"] = 0;
                        drNewRow["IncludeForReports"] = 0;
                        drNewRow["WoodPlanWebActionDoneDate"] = 0;
                        drNewRow["WoodPlanWebWorkOrderDoneDate"] = 0;
                        drNewRow["WoodPlanWebFileManagerAdmin"] = 0;
                        drNewRow["ClientContactPersonID"] = intPassedInClientContactPersonID;
                        drNewRow["PersonName"] = strPassedInPersonName;
                        this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow = this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientID"] = intPassedInClientID;
                        drNewRow["ClientName"] = strPassedInClientName;
                        drNewRow["ContactDetails"] = "";
                        drNewRow["ContactTypeID"] = (intTypeID != 0 ? intTypeID : 0);
                        drNewRow["IncludeForGrittingEmail"] = 0;
                        drNewRow["IncludeForReports"] = 0;
                        drNewRow["WoodPlanWebActionDoneDate"] = 0;
                        drNewRow["WoodPlanWebWorkOrderDoneDate"] = 0;
                        drNewRow["WoodPlanWebFileManagerAdmin"] = 0;
                        drNewRow["ClientContactPersonID"] = intPassedInClientContactPersonID;
                        drNewRow["PersonName"] = strPassedInPersonName;
                        this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ContactDetails"] = "";
                        this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows.Add(drNewRow);
                        this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp04011_GC_Linked_Client_Contact_EditTableAdapter.Fill(this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit, strRecordIDs, strFormMode, 0);     
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enabled Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Client Contact", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientNameButtonEdit.Focus();

                        ContactTypeIDGridLookUpEdit.Properties.ReadOnly = false;
                        IncludeForGrittingEmailCheckEdit.Properties.ReadOnly = false;
                        IncludeForReportsCheckEdit.Properties.ReadOnly = false;

                        WebSiteUserNameTextEdit.Properties.ReadOnly = false;
                        WebSitePasswordTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientNameButtonEdit.Focus();

                        ContactTypeIDGridLookUpEdit.Properties.ReadOnly = true;
                        IncludeForGrittingEmailCheckEdit.Properties.ReadOnly = true;
                        IncludeForReportsCheckEdit.Properties.ReadOnly = true;

                        WebSiteUserNameTextEdit.Properties.ReadOnly = true;
                        WebSitePasswordTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientNameButtonEdit.Focus();

                        ContactTypeIDGridLookUpEdit.Properties.ReadOnly = false;
                        IncludeForGrittingEmailCheckEdit.Properties.ReadOnly = false;
                        IncludeForReportsCheckEdit.Properties.ReadOnly = false;

                        WebSiteUserNameTextEdit.Properties.ReadOnly = false;
                        WebSitePasswordTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientNameButtonEdit.Focus();

                        ContactTypeIDGridLookUpEdit.Properties.ReadOnly = true;
                        IncludeForGrittingEmailCheckEdit.Properties.ReadOnly = true;
                        IncludeForReportsCheckEdit.Properties.ReadOnly = true;

                        WebSiteUserNameTextEdit.Properties.ReadOnly = true;
                        WebSitePasswordTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_GC_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_GC_Client_Contact_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_GC_Client_Contact_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp04011GCLinkedClientContactEditBindingSource.EndEdit();
            try
            {
                this.sp04011_GC_Linked_Client_Contact_EditTableAdapter.Update(dataSet_GC_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp04011GCLinkedClientContactEditBindingSource.Current;
                if (currentRow != null) 
                {
                    strNewIDs = Convert.ToInt32(currentRow["ClientContactID"]) + ";";
                    // Switch mode to Edit so than any subsequent changes update this record //
                    this.strFormMode = "edit";
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
            frm_Core_Client_Manager fParentForm;
            foreach (Form frmChild in this.ParentForm.MdiChildren)
            {
                if (frmChild.Name == "frm_Core_Client_Manager")
                {
                    fParentForm = (frm_Core_Client_Manager)frmChild;
                    fParentForm.UpdateFormRefreshStatus(3, "", "", strNewIDs, "", "", "", "", "");
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    SetIncludeForGrittingCheckEditEnabledStatus();
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        public void SetIncludeForGrittingCheckEditEnabledStatus()
        {
            DataRowView currentRow = (DataRowView)sp04011GCLinkedClientContactEditBindingSource.Current;
            if (currentRow != null)
            {
                string strType = currentRow["ContactTypeID"].ToString();
                IncludeForGrittingEmailCheckEdit.Properties.ReadOnly = (strType == "4" ? false : true);
                IncludeForGrittingEmailCheckEdit.Enabled = (strType == "4" ? true : false);
                IncludeForReportsCheckEdit.Properties.ReadOnly = (strType == "4" ? false : true);
                IncludeForReportsCheckEdit.Enabled = (strType == "4" ? true : false);
            }
        }
        #endregion


        #region Editors

        private void ClientNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;

            var currentRowView = (DataRowView)sp04011GCLinkedClientContactEditBindingSource.Current;
            var currentRow = (DataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")
            {
                int intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                var fChildForm = new frm_EP_Select_Client();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalClientID = intClientID;
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intClientID != fChildForm.intSelectedClientID && fChildForm.intSelectedClientID > 0)
                    {
                        currentRow.ClientID = fChildForm.intSelectedClientID;
                        currentRow.ClientName = fChildForm.strSelectedClientName;
                        currentRow.ClientContactPersonID = 0;
                        currentRow.PersonName = "";
                        sp04011GCLinkedClientContactEditBindingSource.EndEdit();
                    }
                }
            }
        }
        private void ClientNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "");
            }
        }
         
        private void PersonNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;

            var currentRowView = (DataRowView)sp04011GCLinkedClientContactEditBindingSource.Current;
            var currentRow = (DataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")
            {
                int intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                int intClientContactPersonID = (string.IsNullOrEmpty(currentRow.ClientContactPersonID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContactPersonID));
                var fChildForm = new frm_Core_Select_Client_Contact_Person();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalParentSelectedID = intClientID;
                fChildForm.intOriginalChildSelectedID = intClientContactPersonID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intClientID != fChildForm.intSelectedParentID && fChildForm.intSelectedParentID > 0)
                    {
                        currentRow.ClientID = fChildForm.intSelectedParentID;
                        currentRow.ClientName = fChildForm.strSelectedParentDescription;
                    }
                    if (intClientContactPersonID != fChildForm.intSelectedChildID && fChildForm.intSelectedChildID > 0)
                    {
                        currentRow.ClientContactPersonID = fChildForm.intSelectedChildID;
                        currentRow.PersonName = fChildForm.strSelectedPersonName;
                    }
                    sp04011GCLinkedClientContactEditBindingSource.EndEdit();
                }
            }
            if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.ClientContactPersonID = 0;
                currentRow.PersonName = "";
                sp04011GCLinkedClientContactEditBindingSource.EndEdit();
            }
       }
        private void PersonNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            //ButtonEdit be = (ButtonEdit)sender;
            //if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            //{
            //    dxErrorProvider1.SetError(PersonNameButtonEdit, "Select a value.");
            //    e.Cancel = true;  // Show stop icon as field is invalid //
            //    return;
            //}
            //else
            //{
            //    dxErrorProvider1.SetError(PersonNameButtonEdit, "");
            //}
        }
        
        private void ContactTypeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                IncludeForGrittingEmailCheckEdit.Properties.ReadOnly = false;
                IncludeForGrittingEmailCheckEdit.Enabled = true;
                IncludeForReportsCheckEdit.Properties.ReadOnly = false;
                IncludeForReportsCheckEdit.Enabled = true;
            }
            else if (Convert.ToInt32(glue.EditValue) == 4)
            {
                IncludeForGrittingEmailCheckEdit.Properties.ReadOnly = false;
                IncludeForGrittingEmailCheckEdit.Enabled = true;
                IncludeForReportsCheckEdit.Properties.ReadOnly = false;
                IncludeForReportsCheckEdit.Enabled = true;
            }
            else
            {
                IncludeForGrittingEmailCheckEdit.Properties.ReadOnly = true;
                IncludeForGrittingEmailCheckEdit.Enabled = false;
                IncludeForReportsCheckEdit.Properties.ReadOnly = true;
                IncludeForReportsCheckEdit.Enabled = false;
                DataRowView currentRow = (DataRowView)sp04011GCLinkedClientContactEditBindingSource.Current;
                if (currentRow != null) 
                {
                    currentRow["IncludeForGrittingEmail"] = 0;  // Clear Tickbox //
                    currentRow["IncludeForReports"] = 0;  // Clear Tickbox //
                }
                IncludeForGrittingEmailCheckEdit.Checked = false;
                IncludeForReportsCheckEdit.Checked = false;
            }
        }
        private void ContactTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ContactTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContactTypeIDGridLookUpEdit, "");
            }
        }
        
        private void ClientContactTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ClientContactTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ClientContactTextEdit, "");
            }
        }
        
        #endregion





    


    }
}


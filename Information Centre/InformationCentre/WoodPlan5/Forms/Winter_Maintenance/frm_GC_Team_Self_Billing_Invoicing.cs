﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.UserDesigner;

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Reports;

namespace WoodPlan5
{
    public partial class frm_GC_Team_Self_Billing_Invoicing : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_EditLayoutEnabled = false;
 
        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs4 = "";

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        Boolean iBoolPreventChildDataLoading = false;

        public string i_str_SavedDirectoryName = "";
        public string i_str_PDFDirectoryName = "";
        WaitDialogForm loadingForm;
        rpt_GC_Gritting_Team_Self_Billing_Invoice rptReport;

        string i_str_selected_Company_ids = "";
        string i_str_selected_Company_names = "";
        BaseObjects.GridCheckMarksSelection selection1;

        Bitmap bmpBlank = null;
        Bitmap bmpAlert = null;

        #endregion

        public frm_GC_Team_Self_Billing_Invoicing()
        {
            InitializeComponent();
        }

        private void frm_GC_Team_Self_Billing_Invoicing_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4008;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04121_GC_Callouts_For_InvoicingTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "GrittingCallOutID");
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04272_GC_Callouts_For_Invoice_HeadersTableAdapter.Connection.ConnectionString = strConnectionString;

            sp04119_GC_Authorisation_Teams_Self_BillingTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "SubContractorGritInformationID");
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04120_GC_Authorisation_Teams_Non_Self_BillingTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "SubContractorGritInformationID");
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //


            beiFromDate.EditValue = DateTime.Today.AddDays(-7);  // Last week only //
            beiToDate.EditValue = DateTime.Today.AddDays(1).AddSeconds(-1);  // Should give todays date + 23:59:59 //
            sp04131_GC_Team_Self_Billing_InvoicesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView4, "GrittingInvoiceID");

            // Add record selection checkboxes to popup Company Filter grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            sp04237_GC_Company_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04237_GC_Company_Filter_ListTableAdapter.Fill(dataSet_GC_Reports.sp04237_GC_Company_Filter_List);
            gridControl5.ForceInitialize();

            bmpBlank = new Bitmap(16, 16);
            bmpAlert = new Bitmap(imageCollection1.Images[3], 16, 16);
            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);  // Initialise the form for later use //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Load_Data() - performed by LoadLastSavedUserScreenSettings() //
    
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_SavedDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Self-Billing Invoice Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Self-Billing Invoice Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_SavedDirectoryName.EndsWith("\\")) i_str_SavedDirectoryName += "\\";  // Add Backslash to end //
            
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_PDFDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingSelfBillingInvoicePDFPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Saved PDF Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Saved PDF Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_PDFDirectoryName.EndsWith("\\")) i_str_PDFDirectoryName += "\\";  // Add Backslash to end //
            
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }

        private void frm_GC_Team_Self_Billing_Invoicing_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Linked_Records();
                }
            }
            SetMenuStatus();
        }

        private void frm_GC_Team_Self_Billing_Invoicing_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                try
                {
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShowQueries", barEditItemShowQueries.EditValue.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShowDontPay", barEditItemShowDontPay.EditValue.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
                catch (Exception)
                {
                }
            }

        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();

            DevExpress.XtraEditors.XtraMessageBox.Show("Please ensure that your team know not to edit any callouts when generating the team self-billing invoices.", "IMPORTANT INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Edit Layout Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_EditLayoutEnabled = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            switch (i_int_FocusedGrid)
            {
                case 4:    // Historical Self-BIlling Invoices //
                    {
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
            
            // Set Enabled Status of Main Toolbar Buttons //
            bbiEditLayout.Enabled = iBool_EditLayoutEnabled;
           
            // Set enabled status of GridView4 navigator custom buttons //
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowDelete)
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            bbiRegeneratePDF.Enabled = (intRowHandles.Length > 0);
            bbiEmail.Enabled = (intRowHandles.Length > 0);
        }

        private void bbiReloadCalloutData_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Linked_Records();
        }

        private void bbiReloadTeamData_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Data();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys != Keys.Control)
                {
                    // Show Querys //
                    string strShowQueries = default_screen_settings.RetrieveSetting("ShowQueries");
                    if (!string.IsNullOrEmpty(strShowQueries)) barEditItemShowQueries.EditValue = Convert.ToInt32(strShowQueries);

                    // Show Dont Pay //
                    string strShowDontPay = default_screen_settings.RetrieveSetting("ShowDontPay");
                    if (!string.IsNullOrEmpty(strShowDontPay)) barEditItemShowDontPay.EditValue = Convert.ToInt32(strShowDontPay);
                }
            }
            // Company Filter //
            int intFoundRow = 0;
            string strItemFilter = default_screen_settings.RetrieveSetting("CompanyFilter");
            if (!string.IsNullOrEmpty(strItemFilter))
            {
                Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                GridView viewFilter = (GridView)gridControl5.MainView;
                viewFilter.BeginUpdate();
                intFoundRow = 0;
                foreach (string strElement in arrayItems)
                {
                    if (strElement == "") break;
                    intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["CompanyID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        viewFilter.MakeRowVisible(intFoundRow, false);
                    }
                }
                viewFilter.EndUpdate();
                barEditItemCompanyFilter.EditValue = PopupContainerEditCompanies_Get_Selected();
            }

            Load_Data();
            Load_Invoice_Data();
        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            WaitDialogForm loading = new WaitDialogForm("Loading Teams...", "Callout Authorisation Manager");
            if (fProgress == null)
            {
                loading.Show();
            }
            else
            {
                loading.Hide();
                loading.Dispose();
            }
            DateTime dtToday = DateTime.Today;

            iBoolPreventChildDataLoading = true; // Prevent child grid loading until both parent grids loaded //
 
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            this.RefreshGridViewState1.SaveViewInfo();
            sp04119_GC_Authorisation_Teams_Self_BillingTableAdapter.Fill(this.dataSet_GC_Core.sp04119_GC_Authorisation_Teams_Self_Billing, dtToday);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

            view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            this.RefreshGridViewState2.SaveViewInfo();
            sp04120_GC_Authorisation_Teams_Non_Self_BillingTableAdapter.Fill(this.dataSet_GC_Core.sp04120_GC_Authorisation_Teams_Non_Self_Billing, dtToday);
            this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

            if (fProgress == null)
            {
                loading.Close();
                loading.Dispose();
            }

            Load_Linked_Records();
            iBoolPreventChildDataLoading = false;
        }

        private void Load_Linked_Records()
        {
            int intShowQuery = Convert.ToInt32(barEditItemShowQueries.EditValue);
            int intShowDoNotPay = Convert.ToInt32(barEditItemShowDontPay.EditValue);
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount1 = intRowHandles.Length;
            string strSelectedIDs1 = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs1 += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["SubContractorID"])) + ',';
            }

            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            int intCount2 = intRowHandles.Length;
            string strSelectedIDs2 = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs2 += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["SubContractorID"])) + ',';
            }
            //Populate Child Grid //
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount1 + intCount2 == 0)
            {
                this.dataSet_GC_Core.sp04121_GC_Callouts_For_Invoicing.Clear();
            }
            else
            {
                sp04121_GC_Callouts_For_InvoicingTableAdapter.Fill(dataSet_GC_Core.sp04121_GC_Callouts_For_Invoicing, strSelectedIDs1 + strSelectedIDs2, intShowQuery, intShowDoNotPay, i_str_selected_Company_ids);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl3.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["GrittingCallOutID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Self-Billing Invoice Teams Available";
                    break;
                case "gridView2":
                    message = "No Non Self-Billing Invoice Teams Available";
                    break;
                case "gridView3":
                    message = "No Non Self-Billing Invoice Teams Available";
                    break;
                case "gridView4":
                    message = "No Historical Self Billing Invoices Available";
                    break;
                case "gridView6":
                    message = "No Linked Callouts Available - Select one or more Invoices";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    if (!iBoolPreventChildDataLoading) Load_Linked_Records();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView2":
                    if (!iBoolPreventChildDataLoading) Load_Linked_Records();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView4":
                    Load_Callouts_Linked_To_Invoice();
                    view = (GridView)gridControl6.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        //Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "NextInvoiceDate")
            {
                if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "NextInvoiceDate").ToString())) return;
                DateTime dtToday = DateTime.Today;
                TimeSpan ts1 = dtToday - Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "NextInvoiceDate"));
                if (ts1.Days == 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(192, 255, 192); ;
                    e.Appearance.BackColor2 = Color.LightGreen;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (ts1.Days > 0)
                {
                    e.Appearance.BackColor = Color.LightCoral;
                    e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView2;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        //Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }


        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                int[] intRowHandles = view.GetSelectedRows();
                bbiSelectTeamsOnSelectedCallouts.Enabled = intRowHandles.Length > 0;
                pmCalloutsList.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView3;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        //Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Alert" && e.IsGetData)
            {
                e.Value = bmpBlank;
                GridView view = (GridView)sender;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                if (Convert.ToInt32(view.GetRowCellValue(rHandle, "DoNotPaySubContractor")) == 1) e.Value = bmpAlert;
            }
        }

        #endregion


        #region Authorise

        private void bbiAuthorise1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Authorise_Callout(1);
        }

        private void bbiAuthorise2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Authorise_Callout(1);
        }

        private void bbiUnAuthorise_ItemClick(object sender, ItemClickEventArgs e)
        {
            Authorise_Callout(0);
        }

        private void Authorise_Callout(int intAuthorise)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intAuthorise == 1 ? "Authorise" : "Unauthorise") + " before proceeding.", "Authorise\\Unauthorise Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            if (intAuthorise == 1)
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Authorised as this process will set selected records to this value //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 160) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            else // Deselect any records with a status of 120 (Work Completed) as this process will set selected records to this value //
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Authorised as they have already been authorised //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 120) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }

            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intAuthorise == 1 ? "Authorise" : "Unauthorise") + " before proceeding.\n\nNote: This process only updates callouts which don't already have this status. Any other callouts are automatically de-selected.", "Authorise\\Unauthorise Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to update so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " selected for " + (intAuthorise == 1 ? "Authorisation" : "Unauthorisation") + ".\n\nProceed?", "Authorise\\Unauthorise Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption((intAuthorise == 1 ? "Authorising" : "Unauthorising") + " Callouts...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;
            
            string strCalloutIDs = "";
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                strCalloutIDs += view.GetRowCellValue(intRowHandles[i], "GrittingCallOutID").ToString() + ",";
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            try
            {
                DataSet_GC_CoreTableAdapters.QueriesTableAdapter AuthoriseCallouts = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                AuthoriseCallouts.ChangeConnectionString(strConnectionString);
                AuthoriseCallouts.sp04122_GC_Authorise_Callouts(strCalloutIDs, intAuthorise);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while " + (intAuthorise == 1 ? "Authorising" : "Unauthorising") + " the selected gritting callouts - [" + ex.Message + "]\n\nTry running the process again, if the problem persists please contact Technical Support.", "Authorise\\Unauthorise Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " updated successfully.", "Authorise\\Unauthorise Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        #endregion


        #region Query

        private void bbiQuery1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Query_Callout(1);
        }

        private void bbiQuery2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Query_Callout(1);
        }

        private void bbiQueryClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            Query_Callout(0);
        }

        private void Query_Callout(int intQuery)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intQuery == 1 ? "Query" : "Clear the Query of") + " before proceeding.", "Query\\Clear Query Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            if (intQuery == 1)
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Queried as this process will set selected records to this value //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 170) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            else // Deselect any records with a status of 170 (Work Queried) as this process will set selected records to this value //
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Authorised as they have already been authorised //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 120) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }

            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intQuery == 1 ? "Query" : "Clear the Query of") + " before proceeding.\n\nNote: This process only updates callouts which don't already have this status. Any other callouts are automatically de-selected.", "Query\\Clear Query Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to update so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " selected for " + (intQuery == 1 ? "Querying" : "Clearing the Query") + ".\n\nProceed?", "Query\\Clear Query Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption((intQuery == 1 ? "Querying" : "Clearing Query") + " Callouts...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            string strCalloutIDs = "";
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                strCalloutIDs += view.GetRowCellValue(intRowHandles[i], "GrittingCallOutID").ToString() + ",";
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            try
            {
                DataSet_GC_CoreTableAdapters.QueriesTableAdapter QueryCallouts = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                QueryCallouts.ChangeConnectionString(strConnectionString);
                QueryCallouts.sp04123_GC_Query_Callouts(strCalloutIDs, intQuery);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while " + (intQuery == 1 ? "Querying" : "Clearing the Query of") + " the selected gritting callouts - [" + ex.Message + "]\n\nTry running the process again, if the problem persists please contact Technical Support.", "Query\\Clear Query Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " updated successfully.", "Query\\Clear Query Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        #endregion


        #region Don't Pay

        private void bbiDontPay1_ItemClick(object sender, ItemClickEventArgs e)
        {
            DontPay_Callout(1);
        }

        private void bbiDontPay2_ItemClick(object sender, ItemClickEventArgs e)
        {
            DontPay_Callout(1);
        }

        private void bbiDoPay_ItemClick(object sender, ItemClickEventArgs e)
        {
            DontPay_Callout(0);
        }

        private void DontPay_Callout(int intDontPay)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intDontPay == 1 ? "set as Don't Pay" : "Clear Don't Pay Flag") + " before proceeding.", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            if (intDontPay == 1)
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Queried as this process will set selected records to this value //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 170) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            else // Deselect any records with a status of 170 (Work Queried) as this process will set selected records to this value //
            {
                for (int i = 0; i < intRowHandles.Length; i++)  // Deselect any selected record currently set as Authorised as they have already been authorised //
                {
                    intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                    if (intJobStatusID == 120) view.UnselectRow(intRowHandles[i]);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }

            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to " + (intDontPay == 1 ? "set as Don't Pay" : "Clear Don't Pay Flag") + " before proceeding.\n\nNote: This process only updates callouts which don't already have this status. Any other callouts are automatically de-selected.", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to update so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " selected for " + (intDontPay == 1 ? "setting as Don't Pay" : "Clearing Don't Pay Flag") + ".\n\nProceed?", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            string strDontPayTeamReason = "";
            int intDontInvoiceClient = 0;
            string strDontInvoiceClientReason = "";
            int intClearDontChargeClient = 0;

            if (intDontPay == 1)
            {
                // Open Data entry screen so user can enter further info...//
                frm_GC_Team_Self_Billing_Invoice_Dont_Pay fChildForm = new frm_GC_Team_Self_Billing_Invoice_Dont_Pay();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() != DialogResult.OK) return;
                strDontPayTeamReason = fChildForm.strDontPayTeamReason;
                intDontInvoiceClient = fChildForm.intDontInvoiceClient; ;
                strDontInvoiceClientReason = fChildForm.strDontInvoiceClientReason;
            }
            else  // Clearing Don't Pay Flag so see if user wants to clear any don't pay Client setings //
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are Clearing the Don't Pay Flag - do you also wish to clear any Don't Charge Client details?", "Clear Don't Pay Flag on Callouts", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) intClearDontChargeClient = 1;
            }

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption((intDontPay == 1 ? "Setting Don't Pay" : "Clearing Don't Pay") + " Callouts...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            string strCalloutIDs = "";
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                strCalloutIDs += view.GetRowCellValue(intRowHandles[i], "GrittingCallOutID").ToString() + ",";
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            try
            {
                DataSet_GC_CoreTableAdapters.QueriesTableAdapter DontPayCallouts = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                DontPayCallouts.ChangeConnectionString(strConnectionString);
                DontPayCallouts.sp04124_GC_Dont_Pay_Callouts(strCalloutIDs, intDontPay, strDontPayTeamReason, intDontInvoiceClient, strDontInvoiceClientReason, intClearDontChargeClient);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while " + (intDontPay == 1 ? "setting as Don't Pay" : "Clearing Don't Pay Flag") + " on the selected gritting callouts - [" + ex.Message + "]\n\nTry running the process again, if the problem persists please contact Technical Support.", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Callout" : intRowHandles.Length.ToString() + " Callouts") + " updated successfully.", "Set\\Clear Don't Pay Flag on Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion


        #region Invoice

        private void bbiInvoice1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Invoice_Teams();
        }

        private void bbiInvoice2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Invoice_Teams();
        }

        private void bbiInvoiceSelectRecords_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all Teams with at least one job ready for invoicing and Next Invoice date <= todays date //
            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            DateTime dtToday = DateTime.Today;

            for (int i = 0; i < view.DataRowCount; i++)
            {
                
                if (!string.IsNullOrWhiteSpace(view.GetRowCellValue(i, "NextInvoiceDate").ToString()) && Convert.ToDateTime(view.GetRowCellValue(i, "NextInvoiceDate")) <= dtToday && Convert.ToInt32(view.GetRowCellValue(i, "AuthorisedJobCount")) > 0) view.SelectRow(i);

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void Invoice_Teams()
        {
            // First... Check selected jobs are all appropriate - Status of 50 (Ready to send) - if not, de-select them //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Teams to create invoices for before proceeding.", "Create Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check for internet Connectivity //
            bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
            if (!boolNoInternet) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send emails.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Selected Teams...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.SelectedRowsCount / 10;
            if (intUpdateProgressThreshhold == 0) intUpdateProgressThreshhold = 10 / view.SelectedRowsCount;
            int intUpdateProgressTempCount = 0;

            DateTime dtToday = DateTime.Today;
            DateTime dtTodayWithTime = DateTime.Now;
            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < intRowHandles.Length; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "AuthorisedJobCount")) <= 0) view.UnselectRow(i);  // De-select team as it is not ready to be invoiced [no callouts] //
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Teams to create invoices for before proceeding.\n\nNote: This process de-selects any selected teams which have no jobs ready for invoicing.", "Create Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get DB Settings for generated PDF Self-Billing Invoices //
            string strEmailBodyFile = "";
            string strEmailFrom = "";
            string strEmailSubjectLine = "";
            string strCCToEmailAddress = "";
            string strPDFFolderPath = "";
            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp04126_GC_Team_Self_Billing_Invoice_System_Settings", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
            DataSet dsSettings = new DataSet("NewDataSet");
            try
            {
                sdaSettings.Fill(dsSettings, "Table");
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Self Billing Invoice Folder and Email Settings' (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dsSettings.Tables[0].Rows.Count != 1)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Self Billing Invoice Folder and Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DataRow dr1 = dsSettings.Tables[0].Rows[0];
            strEmailBodyFile = dr1["BodyFileName"].ToString();
            strEmailFrom = dr1["EmailFrom"].ToString();
            strEmailSubjectLine = dr1["SubjectLine"].ToString();
            strCCToEmailAddress = dr1["CCToName"].ToString();
            strPDFFolderPath = dr1["PDFFolder"].ToString();
            strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
            strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
            strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
            strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);
            
            if (string.IsNullOrEmpty(strEmailBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strPDFFolderPath) || string.IsNullOrEmpty(strSMTPMailServerAddress))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the folder and email settings (Email Layout File, From Email Address, PDF File Folder and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strPDFFolderPath.EndsWith("\\")) strPDFFolderPath+="\\";  // Add Backslash to end //

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            // If at this point, we are good to generate self-billing invoices so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Team" : intRowHandles.Length.ToString() + " Teams") + " selected for creating self-billing invoices.\n\nProceed?", "Create Team Invoices", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            i_str_AddedRecordIDs4 = "";  // Used to hold new records so they can be highlighted in the Historical Invoices page //

            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            if (intUpdateProgressThreshhold == 0) intUpdateProgressThreshhold = 10 / view.SelectedRowsCount;
            intUpdateProgressTempCount = 0;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Creating Invoices...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

                      
            int intSubContractorIDs = 0;
            int intInvoiceHeaderID = 0;
            string strPDFName = "";
            string strEmailPassword = "";

            DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetEmailAddress = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            GetEmailAddress.ChangeConnectionString(strConnectionString);
            string strEmailAddresses = "";
            char[] delimiters = new char[] { ',' }; 
            try
            {
                string strBody = System.IO.File.ReadAllText(strEmailBodyFile);
                foreach (int intRowHandle in intRowHandles)
                {
                    intSubContractorIDs = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SubContractorID"));
                    strEmailPassword = view.GetRowCellValue(intRowHandle, "EmailPassword").ToString();
                    strPDFName = view.GetRowCellValue(intRowHandle, "SubContractorID").ToString().PadLeft(8, '0') + "_" + dtTodayWithTime.ToString("yyyy-MM-dd_HH_mm_ss") + ".PDF";

                    DataSet_GC_CoreTableAdapters.QueriesTableAdapter InsertInvoiceHeader = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                    InsertInvoiceHeader.ChangeConnectionString(strConnectionString);
                    intInvoiceHeaderID = Convert.ToInt32(InsertInvoiceHeader.sp04125_GC_Create_Invoice_Header(intSubContractorIDs, dtToday, strPDFName));
                    i_str_AddedRecordIDs4 += intInvoiceHeaderID.ToString() + ";";

                    // Header now created and callouts linked so create invoice report and save it to PDF //
                    SaveReport(intInvoiceHeaderID, strEmailPassword, strPDFFolderPath + strPDFName);
                    int intCounter = 0;  // Check if PDF file has completed writting //
                    do
                    {
                        System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                        intCounter++;
                        if (intCounter >= 60) break;
                    } while (!File.Exists(strPDFFolderPath + @strPDFName));
                    if (intCounter >= 60)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    /*string strReportFileName = "SelfBillingInvoiceLayout1.repx";
                    rpt_GC_Gritting_Team_Self_Billing_Invoice rptReport = new rpt_GC_Gritting_Team_Self_Billing_Invoice(this.GlobalSettings, intInvoiceHeaderID);
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);

                    // Set security options of report so when it is exported, it can't be edited and is password protected //
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsPassword = "GroundControlXX";
                    if (!string.IsNullOrEmpty(strEmailPassword)) rptReport.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = strEmailPassword;

                    strPDFName = strPDFFolderPath + strPDFName;  // Put path onto start of filename //
                    rptReport.ExportToPdf(strPDFName);
                    */

                    // PDF now created so email to team... //
                    strEmailAddresses = GetEmailAddress.sp04130_GC_Team_Email_Addressed_For_Self_Billing_Email(intSubContractorIDs).ToString();
                    if (!String.IsNullOrEmpty(strEmailAddresses))
                    {
                        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                        msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                        string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (strEmailTo.Length > 0)
                        {
                            foreach (string strEmailAddress in strEmailTo)
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                            }
                        }
                        else
                        {
                            msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                        }
                        msg.Subject = strEmailSubjectLine;
                        if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                        msg.Priority = System.Net.Mail.MailPriority.High;
                        msg.IsBodyHtml = true;

                        System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                        System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                        // Create a new attachment //
                        System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strPDFFolderPath + strPDFName); //create the attachment
                        msg.Attachments.Add(mailAttachment);
                       
                        //create the LinkedResource (embedded image)
                        System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                        logo.ContentId = "companylogo";
                        //add the LinkedResource to the appropriate view
                        htmlView.LinkedResources.Add(logo);

                        //create the LinkedResource (embedded image)
                        System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                        logo2.ContentId = "companyfooter";
                        //add the LinkedResource to the appropriate view
                        htmlView.LinkedResources.Add(logo2);

                        msg.AlternateViews.Add(plainView);
                        msg.AlternateViews.Add(htmlView);

                        object userState = msg;
                        System.Net.Mail.SmtpClient emailClient = null;
                        if (intSMTPMailServerPort != 0)
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                        }
                        else
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                        }
                        if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                        {
                            System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                            emailClient.UseDefaultCredentials = false;
                            emailClient.Credentials = basicCredential;
                        }
                        emailClient.SendAsync(msg, userState);
                    }

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the self-billing invoice(s).\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            Load_Data();  // Refresh grid to reflect record's updated status //
            Load_Invoice_Data();  // Refresh list of historical invoice (this process will also select the new invoices) //
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            // Success message in here //
        }
        private void SaveReport(int intInvoiceHeaderID, string strEmailPassword, string strPDFName)
        {
            // Header now created and callouts linked so create invoice report and save it to PDF //
            string strReportFileName = "SelfBillingInvoiceLayout1.repx";
            rpt_GC_Gritting_Team_Self_Billing_Invoice rptReport = new rpt_GC_Gritting_Team_Self_Billing_Invoice(this.GlobalSettings, intInvoiceHeaderID);
            rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);

            // Set security options of report so when it is exported, it can't be edited and is password protected //
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsPassword = "GroundControlXX";
            if (!string.IsNullOrEmpty(strEmailPassword)) rptReport.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = strEmailPassword;

            //strPDFName = strPDFFolderPath + strPDFName;  // Put path onto start of filename //
            rptReport.ExportToPdf(strPDFName);
        }

        #endregion


        #region Company Filter Panel

        private void repositoryItemPopupContainerEditCompanyFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCompanies_Get_Selected();
        }

        private void btnCompanyFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditCompanies_Get_Selected()
        {
            i_str_selected_Company_ids = "";    // Reset any prior values first //
            i_str_selected_Company_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_Company_ids = "";
                return "All Companies";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Company_ids += Convert.ToString(view.GetRowCellValue(i, "CompanyID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Company_names = Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Company_names += ", " + Convert.ToString(view.GetRowCellValue(i, "CompanyName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Company_names;
        }

        #endregion


        private void barManager1_HighlightedLinkChanged(object sender, HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo { Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip };

            toolTipController1.ShowHint(Info);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // Create a new report.                
                rpt_GC_Gritting_Team_Self_Billing_Invoice report = new rpt_GC_Gritting_Team_Self_Billing_Invoice(this.GlobalSettings, 1);



                // Create a new memory stream and export the report into it as PDF.
                /*MemoryStream mem = new MemoryStream();
                report.ExportToPdf(mem);
                
                // Create a new attachment and put the PDF report into it.
                mem.Seek(0, System.IO.SeekOrigin.Begin);
                Attachment att = new Attachment(mem, "TestReport.pdf", "application/pdf");

                // Create a new message and attach the PDF report to it.
                MailMessage mail = new MailMessage();
                mail.Attachments.Add(att);

                // Specify sender and recipient options for the e-mail message.
                mail.From = new MailAddress("someone@somewhere.com", "Someone");
                mail.To.Add(new MailAddress(report.ExportOptions.Email.RecipientAddress,
                    report.ExportOptions.Email.RecipientName));

                // Specify other e-mail options.
                mail.Subject = report.ExportOptions.Email.Subject;
                mail.Body = "This is a test e-mail message sent by an application.";

                // Send the e-mail message via the specified SMTP server.
                SmtpClient smtp = new SmtpClient("smtp.somewhere.com");
                smtp.Send(mail);

                // Close the memory stream.
                mem.Close();
                mem.Flush();*/
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error sending a report.\n" + ex.ToString());
            }
        }

        private void bbiEditLayout_ItemClick(object sender, ItemClickEventArgs e)
        {
            string strReportFileName = "SelfBillingInvoiceLayout1.repx";
 
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                rpt_GC_Gritting_Team_Self_Billing_Invoice rptReport = null;
                frm_GC_Team_Self_Billing_Invoice_Test_Layout fChildForm = new frm_GC_Team_Self_Billing_Invoice_Test_Layout();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    rptReport = new rpt_GC_Gritting_Team_Self_Billing_Invoice(this.GlobalSettings, fChildForm.intInvoiceHeader);

                }
                else
                {
                    rptReport = new rpt_GC_Gritting_Team_Self_Billing_Invoice(this.GlobalSettings, 0);
                }
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (Control.ModifierKeys == Keys.Control)  // Open report for preview so we can see result of any changes //
                {
                    rptReport.ShowRibbonPreview();
                    return;
                }


                loadingForm = new WaitDialogForm("Loading Report Builder...", "Reporting");
                loadingForm.Show();

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                loadingForm.Close();
                form.ShowDialog();
                panel.CloseReport();
            }
        }

        private void bbiSelectTeamsOnSelectedCallouts_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            string strSelfBillingTeamIDs = ",";
            string strNonSelfBillingTeamIDs = ",";
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to base Team Selection on before proceeding.", "Select Teams Based on Selected Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Get unique list of Teams from selected Callouts //
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["SelfBillingInvoice"])) == 1)
                {
                    if (!strSelfBillingTeamIDs.Contains(',' + view.GetRowCellValue(intRowHandle, view.Columns["SubContractorID"]).ToString() + ',')) strSelfBillingTeamIDs += view.GetRowCellValue(intRowHandle, view.Columns["SubContractorID"]).ToString() + ',';
                }
                else
                {
                    if (!strNonSelfBillingTeamIDs.Contains(',' + view.GetRowCellValue(intRowHandle, view.Columns["SubContractorID"]).ToString() + ',')) strNonSelfBillingTeamIDs += view.GetRowCellValue(intRowHandle, view.Columns["SubContractorID"]).ToString() + ',';
                }
            }
            char[] delimiters = new char[] { ',' };
            int intID = 0;
            int intHandle = 0;

            // Sellf Billing First //
            GridView viewParent = (GridView)gridControl1.MainView;
            viewParent.BeginUpdate();
            viewParent.BeginSelection();
            viewParent.ClearSelection();
            string[] strArray = strSelfBillingTeamIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (string strElement in strArray)
            {
                intID = Convert.ToInt32(strElement);
                intHandle = viewParent.LocateByValue(0, viewParent.Columns["SubContractorID"], intID);
                if (intHandle != GridControl.InvalidRowHandle)
                {
                    viewParent.SelectRow(intHandle);
                    viewParent.MakeRowVisible(intHandle, false);
                }
            }
            viewParent.EndSelection();
            viewParent.EndUpdate();
            
            // Non-self Billing Second //
            viewParent = (GridView)gridControl2.MainView;
            viewParent.BeginUpdate();
            viewParent.BeginSelection();
            viewParent.ClearSelection();
            strArray = strNonSelfBillingTeamIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (string strElement in strArray)
            {
                intID = Convert.ToInt32(strElement);
                intHandle = viewParent.LocateByValue(0, viewParent.Columns["SubContractorID"], intID);
                if (intHandle != GridControl.InvalidRowHandle)
                {
                    viewParent.SelectRow(intHandle);
                    viewParent.MakeRowVisible(intHandle, false);
                }
            }
            viewParent.EndSelection();
            viewParent.EndUpdate();

        }


        #region Historical Self-Billing Invoices Page

        private void repositoryItemDateEditFrom_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Invoice Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    beiFromDate.EditValue = null;
                }
            }
        }

        private void repositoryItemDateEditTo_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Invoice Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    beiToDate.EditValue = null;
                }
            }
        }

        private void bbiLoadData_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Invoice_Data();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
        }

        private void Load_Invoice_Data()
        {
            if (!this.splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager.ShowWaitForm();
            }

            DateTime dtFromDate = Convert.ToDateTime(beiFromDate.EditValue);
            if (dtFromDate < new DateTime(1900, 1, 1)) dtFromDate = new DateTime(1900, 1, 1);
            DateTime dtToDate = Convert.ToDateTime(beiToDate.EditValue);
            if (dtToDate < new DateTime(1900, 1, 1)) dtToDate = new DateTime(1900, 1, 1);
            GridView view = (GridView)gridControl4.MainView;
            this.RefreshGridViewState4.SaveViewInfo();  // Store expanded groups and selected rows //
            view.BeginUpdate();
            try
            {
                sp04131_GC_Team_Self_Billing_InvoicesTableAdapter.Fill(dataSet_GC_Core.sp04131_GC_Team_Self_Billing_Invoices, "", dtFromDate, dtToDate);
                this.RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
                view.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.splashScreenManager.IsSplashFormVisible) this.splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Self-Billing Invoice list.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.EndUpdate();
            if (this.splashScreenManager.IsSplashFormVisible) this.splashScreenManager.CloseWaitForm();

            if (i_str_AddedRecordIDs4 != "")
            {
                // Highlight any recently added new rows //
                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                 strArray = i_str_AddedRecordIDs4.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["GrittingInvoiceID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs4 = "";
            }
        }

        private void Load_Callouts_Linked_To_Invoice()
        {
            if (splitContainerControl4.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grid is invisible //

            if (!this.splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager.ShowWaitForm();
            }

            GridView view = (GridView)gridControl4.MainView;
            string strGrittingInvoiceIDs = "";
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            foreach (int intRowHandle in intRowHandles)
            {
                strGrittingInvoiceIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GrittingInvoiceID"])) + ',';
            }
            view = (GridView)gridControl6.MainView;
            view.BeginUpdate();
            try
            {
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04272_GC_Callouts_For_Invoice_Headers.Clear();
                }
                else
                {
                    sp04272_GC_Callouts_For_Invoice_HeadersTableAdapter.Fill(dataSet_GC_Core.sp04272_GC_Callouts_For_Invoice_Headers, strGrittingInvoiceIDs);
                }
            }
            catch (Exception)
            {
                view.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.splashScreenManager.IsSplashFormVisible) this.splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Callouts linked to the selected Invoices.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.EndUpdate();
            if (this.splashScreenManager.IsSplashFormVisible) this.splashScreenManager.CloseWaitForm();
        }


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 4;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView4_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {

        }

        private void gridView4_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedFileName":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "LinkedFileName").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView4_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedFileName":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("LinkedFileName").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "LinkedFileName").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No PDF File Linked - unable to proceed.", "View Linked PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                {
                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = Path.Combine(i_str_PDFDirectoryName + strFile);
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                else
                {
                    System.Diagnostics.Process.Start(i_str_PDFDirectoryName + strFile);
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + i_str_PDFDirectoryName + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView4;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }
 
        #endregion


        #region GridView6

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 6;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView6_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {

        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView6;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void splitContainerControl4_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) Load_Callouts_Linked_To_Invoice();
        }

        private void bbiRegeneratePDF_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl4.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Teams to create recreate invoices for before proceeding.", "Recreate Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Selected Team Invoice Headers...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            if (intUpdateProgressThreshhold == 0) intUpdateProgressThreshhold = 10 / view.SelectedRowsCount;
            int intUpdateProgressTempCount = 0;

            // Get DB Settings for generated PDF Self-Billing Invoices //
            string strEmailBodyFile = "";
            string strEmailFrom = "";
            string strEmailSubjectLine = "";
            string strCCToEmailAddress = "";
            string strPDFFolderPath = "";
            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp04126_GC_Team_Self_Billing_Invoice_System_Settings", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
            DataSet dsSettings = new DataSet("NewDataSet");
            try
            {
                sdaSettings.Fill(dsSettings, "Table");
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Self Billing Invoice Folder and Email Settings' (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dsSettings.Tables[0].Rows.Count != 1)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Self Billing Invoice Folder and Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            try
            {
                DataRow dr1 = dsSettings.Tables[0].Rows[0];
                strEmailBodyFile = dr1["BodyFileName"].ToString();
                strEmailFrom = dr1["EmailFrom"].ToString();
                strEmailSubjectLine = dr1["SubjectLine"].ToString();
                strCCToEmailAddress = dr1["CCToName"].ToString();
                strPDFFolderPath = dr1["PDFFolder"].ToString();
                strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
                strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
                strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
                strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
                if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
                int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

                if (string.IsNullOrEmpty(strEmailBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strPDFFolderPath) || string.IsNullOrEmpty(strSMTPMailServerAddress))
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the folder and email settings (Email Layout File, From Email Address, PDF File Folder and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!strPDFFolderPath.EndsWith("\\")) strPDFFolderPath += "\\";  // Add Backslash to end //

                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                // If at this point, we are good to generate self-billing invoices so get user confirmation //
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Team Self-Billing Invoice" : intRowHandles.Length.ToString() + " Team  Self-Billing Invoices") + " selected for recreating.\n\nProceed?", "Recreate Team Invoices", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

                i_str_AddedRecordIDs4 = "";  // Used to hold new records so they can be highlighted in the Historical Invoices page //

                intUpdateProgressThreshhold = intRowHandles.Length / 10;
                if (intUpdateProgressThreshhold == 0) intUpdateProgressThreshhold = 10 / view.SelectedRowsCount;
                intUpdateProgressTempCount = 0;

                fProgress = new frmProgress(0);
                fProgress.UpdateCaption("Recreating Invoices...");
                fProgress.Show();
                System.Windows.Forms.Application.DoEvents();


                int intSubContractorIDs = 0;
                int intInvoiceHeaderID = 0;
                string strPDFName = "";
                string strEmailPassword = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    intSubContractorIDs = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SubContractorID"));
                    strEmailPassword = view.GetRowCellValue(intRowHandle, "EmailPassword").ToString();
                    strPDFName = view.GetRowCellValue(intRowHandle, "LinkedFileName").ToString();  // Use the original name //
                    intInvoiceHeaderID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "GrittingInvoiceID"));

                    // Header now created and callouts linked so create invoice report and save it to PDF //
                    SaveReport(intInvoiceHeaderID, strEmailPassword, strPDFFolderPath + strPDFName);
                    int intCounter = 0;  // Check if PDF file has completed writting //
                    do
                    {
                        System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                        intCounter++;
                        if (intCounter >= 60) break;
                    } while (!File.Exists(strPDFFolderPath + @strPDFName));
                    if (intCounter >= 60)
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to recreate the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Recreate Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while recreating the self-billing invoice(s).\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Recreate Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void bbiEmail_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl4.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Invoices to re-send before proceeding.", "Re-send Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            // Check for internet Connectivity //
            bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
            if (!boolNoInternet) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send emails.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Selected Invoices...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            int intUpdateProgressThreshhold = view.SelectedRowsCount / 10;
            if (intUpdateProgressThreshhold == 0) intUpdateProgressThreshhold = 10 / view.SelectedRowsCount;
            int intUpdateProgressTempCount = 0;

            // Get DB Settings for generated PDF Self-Billing Invoices //
            string strEmailBodyFile = "";
            string strEmailFrom = "";
            string strEmailSubjectLine = "";
            string strCCToEmailAddress = "";
            string strPDFFolderPath = "";
            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp04126_GC_Team_Self_Billing_Invoice_System_Settings", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
            DataSet dsSettings = new DataSet("NewDataSet");
            try
            {
                sdaSettings.Fill(dsSettings, "Table");
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Self Billing Invoice Folder and Email Settings' (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dsSettings.Tables[0].Rows.Count != 1)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Self Billing Invoice Folder and Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DataRow dr1 = dsSettings.Tables[0].Rows[0];
            strEmailBodyFile = dr1["BodyFileName"].ToString();
            strEmailFrom = dr1["EmailFrom"].ToString();
            strEmailSubjectLine = dr1["SubjectLine"].ToString();
            strCCToEmailAddress = dr1["CCToName"].ToString();
            strPDFFolderPath = dr1["PDFFolder"].ToString();
            strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
            strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
            strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
            strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

            if (string.IsNullOrEmpty(strEmailBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strPDFFolderPath) || string.IsNullOrEmpty(strSMTPMailServerAddress))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the folder and email settings (Email Layout File, From Email Address, PDF File Folder and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strPDFFolderPath.EndsWith("\\")) strPDFFolderPath += "\\";  // Add Backslash to end //

            view.BeginUpdate();
            view.BeginSelection();
            string strPDFName = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strPDFName = view.GetRowCellValue(intRowHandle, "LinkedFileName").ToString();
                if (!File.Exists(strPDFFolderPath + strPDFName)) view.UnselectRow(intRowHandle);  // De-select invoice as it is not stored. //
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Teams to re-send invoices to before proceeding.\n\nNote: This process de-selects any selected invoices where the saved invoice PDf file is missing.", "Re-send Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // If at this point, we are good to re-send self-billing invoices so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Team Invoice" : intRowHandles.Length.ToString() + " Team Invoices") + " selected for re-sending.\n\nProceed?", "Re-send Team Invoices", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Re-sending Invoices...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            intUpdateProgressThreshhold = view.SelectedRowsCount / 10;
            if (intUpdateProgressThreshhold == 0) intUpdateProgressThreshhold = 10 / view.SelectedRowsCount;
            intUpdateProgressTempCount = 0;

            DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetEmailAddress = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            GetEmailAddress.ChangeConnectionString(strConnectionString);
            string strEmailAddresses = "";
            char[] delimiters = new char[] { ',' };
            try
            {
                intUpdateProgressTempCount++;
                int intSubContractorIDs = 0;
                string strBody = System.IO.File.ReadAllText(strEmailBodyFile);
                foreach (int intRowHandle in intRowHandles)
                {
                    intSubContractorIDs = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SubContractorID"));
                    strPDFName = view.GetRowCellValue(intRowHandle, "LinkedFileName").ToString();
                    strEmailAddresses = GetEmailAddress.sp04130_GC_Team_Email_Addressed_For_Self_Billing_Email(intSubContractorIDs).ToString();
                    if (!String.IsNullOrEmpty(strEmailAddresses))
                    {
                        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                        msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                        string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (strEmailTo.Length > 0)
                        {
                            foreach (string strEmailAddress in strEmailTo)
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                            }
                        }
                        else
                        {
                            msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                        }
                        msg.Subject = strEmailSubjectLine;
                        if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                        msg.Priority = System.Net.Mail.MailPriority.High;
                        msg.IsBodyHtml = true;

                        System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                        System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                        // Create a new attachment //
                        System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strPDFFolderPath + strPDFName); // create the attachment //
                        msg.Attachments.Add(mailAttachment);
                       
                        // create the LinkedResource (embedded image) //
                        System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                        logo.ContentId = "companylogo";
                        //add the LinkedResource to the appropriate view
                        htmlView.LinkedResources.Add(logo);

                        // create the LinkedResource (embedded image) //
                        System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                        logo2.ContentId = "companyfooter";
                        //add the LinkedResource to the appropriate view
                        htmlView.LinkedResources.Add(logo2);

                        msg.AlternateViews.Add(plainView);
                        msg.AlternateViews.Add(htmlView);

                        object userState = msg;
                        System.Net.Mail.SmtpClient emailClient = null;
                        if (intSMTPMailServerPort != 0)
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                        }
                        else
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                        }
                        if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                        {
                            System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                            emailClient.UseDefaultCredentials = false;
                            emailClient.Credentials = basicCredential;
                        }
                        emailClient.SendAsync(msg, userState);
                    }

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the self-billing invoice(s).\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Team Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        #endregion


        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 4:
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl4.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Self-Billing Invoices to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Self-Billing Invoice" : Convert.ToString(intRowHandles.Length) + " Self-Billing Invoices") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Self-Billing Invoice" : "these Self-Billing Invoices") + " will no longer be available for selection!\n\nAny Gritting Callouts on the invoice will be unlinked and available for inclusion on a new invoice.\n\nNote that any linked PDF files will not be deleted - these must be manually deleted.";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingInvoiceID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //


                        DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        RemoveRecords.sp03002_EP_Client_Delete("gc_gritting_self_billing_invoice", strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        Load_Data();  // Load Team list as Last Invoiced Date and Calculated Next Invoice Date may have changed. NOTE: this will also load callout records if required //
                        Load_Invoice_Data();
                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
        }



        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Report...", "Reporting");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }



 


    }
}
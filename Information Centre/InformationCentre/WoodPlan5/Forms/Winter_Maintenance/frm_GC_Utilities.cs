﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Utilities : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        private ExtendedGridMenu egmMenu;  // Used to extend Grid Column Menu //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        private string strDefaultImagePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        #endregion

        public frm_GC_Utilities()
        {
            InitializeComponent();
        }

        private void frm_GC_Utilities_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4018;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultImagePath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutPictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Images path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Saved Images Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strDefaultImagePath.EndsWith("\\")) strDefaultImagePath += "\\";  // Add Backslash to end //
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //


            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingSavedCalloutSignatureFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Signatures path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Saved Signatures Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strDefaultSignaturePath.EndsWith("\\")) strDefaultSignaturePath += "\\";  // Add Backslash to end //
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            SetMenuStatus();
        }

        private void frm_GC_Utilities_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            /*GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Clients //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }*/

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
            /*
            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }*/
        }

        private void btnImportImages_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to import Gritting Images and Client Signatures recorded on PDAs.\n\nAre you sure you wish to proceed?", "Import Gritting Images and Client Signatures", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;


            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Importing Images and Signatures...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            DateTime dtNow = DateTime.Now;

            SqlDataAdapter sdaImages = new SqlDataAdapter();
            DataSet dsImages = new DataSet("NewDataSet");
            try  // Load image records into memory //
            {
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp04281_GC_Import_Images_Load", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                dsImages.Clear();  // Remove any old values first //
                sdaImages = new SqlDataAdapter(cmd);
                sdaImages.Fill(dsImages, "Table");
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Importing Gritting Images and Client Signatures.\n\nMessage = [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Import Gritting Images and Client Signatures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (dsImages.Tables[0].Rows.Count <= 0)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("No Gritting Images or Client Signatures to Import.", "Import Gritting Images and Client Signatures", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;  // Nothing to process so abort //
            }

            bool boolErrorsFound = false;
            string strImagesIDsToBeDeleted = "";
            int intUpdateProgressThreshhold = dsImages.Tables[0].Rows.Count / 10;
            int intUpdateProgressTempCount = 0;

            try
            {
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SQlConn.Open();
                foreach (DataRow row in dsImages.Tables[0].Rows)
                {
                    // Load binary images into memory //
                    Byte[] data = new Byte[0];
                    data = (Byte[])(row["CapturedImage"]);
                    MemoryStream mem = new MemoryStream(data);
                    Image img = Image.FromStream(mem);
                    int intImageType = Convert.ToInt32(row["ImageType"]);
                    int intCalloutID = Convert.ToInt32(row["GrittingCalloutID"]);
                    string strImageDescription = row["Description"].ToString();
                    string strFullFileName = "";
                    string strFileName = "";
                    if (intImageType == 1)  // Gritting Image //
                    {
                        strFileName = "IMG_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "_" + row["GrittingImageID"].ToString() + ".gif";
                        strFullFileName = strDefaultImagePath + strFileName;
                    }
                    else  // Signature Image //
                    {
                        strFileName = "SIG_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "_" + row["GrittingImageID"].ToString() + ".gif";
                        strFullFileName = strDefaultSignaturePath + strFileName;
                    }

                    img.Save(strFullFileName, System.Drawing.Imaging.ImageFormat.Gif);  // Save Image to Physical File //

                    // Create Link Record in Image table //
                    SqlCommand cmd = null;
                    cmd = new SqlCommand("sp04282_GC_Import_Images_Create_Link", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutID", intCalloutID));
                    cmd.Parameters.Add(new SqlParameter("@PicturePath", strFileName));
                    cmd.Parameters.Add(new SqlParameter("@DateTimeTaken", dtNow));
                    cmd.Parameters.Add(new SqlParameter("@Remarks", strImageDescription));
                    cmd.ExecuteNonQuery();
                    cmd = null;

                    // Store Original Image ID so they can be cleared in one go at the end  //
                    strImagesIDsToBeDeleted += row["GrittingImageID"].ToString() + ",";

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        if (fProgress != null) fProgress.UpdateProgress(10);
                    }
                }
                SQlConn.Close();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Importing Gritting Images and Client Signatures.\n\nMessage = [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Import Gritting Images and Client Signatures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                boolErrorsFound = true;
            }
            finally
            {
                if (!string.IsNullOrEmpty(strImagesIDsToBeDeleted))
                {
                    try
                    {
                        // Remove original binary images to keep DB size down //
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SQlConn.Open();
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp04283_GC_Import_Images_Delete", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@GrittingImageIDs", strImagesIDsToBeDeleted));
                        cmd.ExecuteNonQuery();
                        cmd = null;
                        SQlConn.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                }
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                if (!boolErrorsFound)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Gritting Image and Client Signature Import Completed Successfully.\n\nThe Linked Images can be viewed from the Gritting Callout Manager screen.", "Import Gritting Images and Client Signatures", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        #region Test Code To Insert Images into SQL DB

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                //Read Image Bytes into a byte array
                byte[] imageData = ReadFile(@"C:\Mark_Development\Dev_Images\add_32.png");

                //Initialize SQL Server Connection
                SqlConnection CN = new SqlConnection(strConnectionString);

                //Set insert query
                int intID = 160;
                int intTypeID = 1;
                byte bDescription = 0;
                string qry = "insert into tblGrittingImages (GrittingCalloutID,ImageType,capturedImage, Description) values(@intID, @intTypeID, @ImageData, @Description)";

                //Initialize SqlCommand object for insert.
                SqlCommand SqlCom = new SqlCommand(qry, CN);

                //We are passing Original Image Path and Image byte data as sql parameters.
                SqlCom.Parameters.Add(new SqlParameter("@intID", intID));
                SqlCom.Parameters.Add(new SqlParameter("@intTypeID", intTypeID));
                SqlCom.Parameters.Add(new SqlParameter("@ImageData", (object)imageData));
                SqlCom.Parameters.Add(new SqlParameter("@Description", bDescription));

                //Open connection and execute insert query.
                CN.Open();
                SqlCom.ExecuteNonQuery();
                CN.Close();

                //Close form and return to list or images.
                this.Close();
            }
            catch (Exception Ex)
            {
            }
        }
        //Open file in to a filestream and read data in a byte array.
        byte[] ReadFile(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes to read from file.
            //In this case we want to read entire file. So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);
            return data;
        }

        #endregion



        private void btnRebuildIndexes_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to rebuild indexes on the Gritting and Snow Clearance Callout Tables.\n\nOther users may notice a momentary drop in performance while the process runs if you continue.\n\nAre you sure you wish to proceed?", "Rebuild Indexes", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Rebuilding Indexes...");
                fProgress.Show();
                Application.DoEvents();

                DataSet_GC_CoreTableAdapters.QueriesTableAdapter RebuildIndexes = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
                RebuildIndexes.ChangeConnectionString(strConnectionString);
                if (fProgress != null) fProgress.UpdateProgress(30); // Update Progress Bar //

                try
                {
                    RebuildIndexes.sp04288_GC_Rebuild_Callout_Table_Date_Indexes();
                }
                catch (Exception Ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while rebuilding the indexes... Message [" + Ex.Message + "].\n\nTry again. If the problem persists, contact Technical Support.", "Rebuild Indexes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (fProgress != null)
                {
                    fProgress.UpdateProgress(50); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Indexes Rebuilt Successfully", "Rebuild Indexes", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
 
        }


    }
}

﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;

// Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;

// Required by GridViewFiltering //
using System.Collections.Generic;

// Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Snow_Choose_Team : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;

        public int intOriginalTeamID = 0;
        public string strPassedInSiteSnowClearanceContractIDs = "";
        public int intPreferredSubContractorID = 0;
        public string strSelectedTeamName = "";
        public int intSubContractorID = 0;
        public string strCaller = "";
        public int intRecordCount = 0;
        public int intIsVatRegistered = 0;
        public decimal decDefaultHours = (decimal)0.00;
        public bool boolRemoveBlankRow = false; // Used to remove the dummy blank row if required [used when this screen is called by the Callout Manager scren //
        
        private string strPostcode = "";
        private int intDistance = 0;

        #endregion

        public frm_GC_Snow_Choose_Team()
        {
            InitializeComponent();
        }

        private void frm_GC_Snow_Choose_Team_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show(); // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400040; // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp04168_GC_Preferred_Snow_Clear_Teams_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04185_GC_Outstanding_Snow_And_Grit_Callouts_For_TeamTableAdapter.Connection.ConnectionString = strConnectionString;

            LoadData();
            gridControl1.ForceInitialize();
            if (boolRemoveBlankRow)
            {
                GridView view = (GridView)gridControl1.MainView;
                int intRowHandle = view.LocateByValue(0, view.Columns["SubContractorID"], 0);
                if (intRowHandle != GridControl.InvalidRowHandle)
                {
                    view.DeleteRow(intRowHandle);
                }
            }

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            int SiteContractID = 0;
            char[] delimiters = new char[] { ',' };
            string[] strArray = strPassedInSiteSnowClearanceContractIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length == 1)
            {
                SiteContractID = Convert.ToInt32(strArray[0]);
                try
                {
                    DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter GetPostcode = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
                    GetPostcode.ChangeConnectionString(strConnectionString);
                    strPostcode = GetPostcode.sp04204_GC_Get_Postcode("sitesnowclearancecontract", SiteContractID).ToString().ToUpper().Replace(" ", "");
                    strPostcode = (strPostcode.Length == 7 ? strPostcode.Substring(0, 4) : (strPostcode.Length == 6 ? strPostcode.Substring(0, 3) : strPostcode.Substring(0, 2)));
                }
                catch (Exception)
                {
                }
            }

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100); // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents(); // Allow Form time to repaint itself //

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow(); // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0; // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren(); // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0; // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1; // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren(); // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1; // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            if (string.IsNullOrEmpty(strPostcode))
            {
                barEditItemSearchRadius.Enabled = false;
            }
            else
            {
                barEditItemSearchRadius.Enabled = true;
                radiusLabel.Text = "Postcode: " + strPostcode + " - Seach Radius: 0 miles.";
            }
        }


        bool internalRowFocusing;

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Preferred Site Snow Clearance Teams - Click the Load All Teams button and Select from the All Teams list";
                    break;
                case "gridView2":
                    message = "No Outstanding Jobs for Today for Selected Team";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    if (splitContainerControl2.PanelVisibility == SplitPanelVisibility.Both) LoadLinkedData();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedTeamName == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp04168_GC_Preferred_Snow_Clear_Teams_With_BlankTableAdapter.Fill(dataSet_GC_Snow_Core.sp04168_GC_Preferred_Snow_Clear_Teams_With_Blank, (checkEdit1.Checked ? strPassedInSiteSnowClearanceContractIDs : (checkEdit3.Checked ? "ALL CONTRACTORS" : "")), strPostcode, intDistance);
            if (boolRemoveBlankRow)
            {
                int intRowHandle = view.LocateByValue(0, view.Columns["SubContractorID"], 0);
                if (intRowHandle != GridControl.InvalidRowHandle)
                {
                    view.DeleteRow(intRowHandle);
                }
            }
            view.EndUpdate();
        }

        private void LoadLinkedData()
        {
            GridView view = (GridView)gridControl1.MainView;
            gridControl2.MainView.BeginUpdate();
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                DateTime dtStartDate = DateTime.Today;
                DateTime dtEndDate = DateTime.Today.AddDays(1).AddSeconds(-1); // Should give todays date + 23:59:59 //
                sp04185_GC_Outstanding_Snow_And_Grit_Callouts_For_TeamTableAdapter.Fill(dataSet_GC_Snow_Core.sp04185_GC_Outstanding_Snow_And_Grit_Callouts_For_Team, Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SubContractorID")), dtStartDate, dtEndDate);
                view = (GridView)gridControl2.MainView;
                view.ExpandAllGroups();
            }
            else
            {
                this.dataSet_GC_Snow_Core.sp04185_GC_Outstanding_Snow_And_Grit_Callouts_For_Team.Clear();
            }
            gridControl2.MainView.EndUpdate();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedTeamName = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "TeamName"));
                intPreferredSubContractorID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PreferredSubContractorID"));
                intSubContractorID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SubContractorID"));
                intIsVatRegistered = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "IsVatRegistered"));
                decDefaultHours = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "DefaultHours")); 
            }
        }

        private void repositoryItemCheckEditShowOutstandingJobs_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                splitContainerControl2.PanelVisibility = SplitPanelVisibility.Both;
                LoadLinkedData();
            }
            else
            {
                splitContainerControl2.PanelVisibility = SplitPanelVisibility.Panel1;
            }
        }


        #region Search Radius

        private void trackBarControlRadius_EditValueChanged(object sender, EventArgs e)
        {
            TrackBarControl tbc = (TrackBarControl)sender;
            if (tbc.Value == 0)
            {
                radiusLabel.Text = "Disabled";
                intDistance = 0;
            }
            else
            {
                radiusLabel.Text = "Postcode: " + strPostcode + " - Seach Radius: " + tbc.Value.ToString() + " miles.";
                intDistance = tbc.Value;
            }
        }

        private void btnOKSearchRadius_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditSearchRadius_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = popupContainerControlSearchRadius_Get_Selected();
        }

        private string popupContainerControlSearchRadius_Get_Selected()
        {
            if (intDistance > 0)
            {
                return strPostcode + ": " + intDistance.ToString() + " miles";
            }
            else
            {
                return "Disabled";
            }
        }

        private void trackBarControlRadius_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        #endregion

 

    }
}



using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.UserDesigner;

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_GC_Snow_Callout_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        bool iBool_ReassignJobsButtonEnabled = false;
        bool iBool_AuthoriseButtonEnabled = false;
        bool iBool_PayButtonEnabled = false;
        bool iBool_AddWizardButtonEnabled = false;
        bool iBool_SendButtonEnabled = false;
        bool iBool_EditPOLayout = false;
        bool iBool_ViewPOLayout = false;
        bool iBool_EnableGridColumnChooser = false;

        string istr_FurtherAction = "";  // Set by Block Add Snow Clearance Wizard only //

        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState12;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState6;
        public RefreshGridState RefreshGridViewState5;
        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs12 = "";
        string i_str_AddedRecordIDs6 = "";
        string i_str_AddedRecordIDs5 = "";
        
        string i_str_selected_CallOutType_ids = "";
        string i_str_selected_CallOutType_names = "";
        BaseObjects.GridCheckMarksSelection selection1;
        
        private DateTime i_dtStart = DateTime.MinValue;  // Used if Panel is Date Range //
        private DateTime i_dtEnd = DateTime.MaxValue;  // Used if Panel is Date Range //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        public string strPassedInDrillDownIDs = "";

        public string i_str_SavedDirectoryName = "";
        public string i_str_PDFDirectoryName = "";
        WaitDialogForm loadingForm;
        rpt_GC_Snow_Clearance_Team_Purchase_Order rptReport;

        // Following Used to merge updated changes back into the original dataset without reloading the whole dataset. IMPORTANT NOTE: backgroundWorker1 added as object to form and events set on it. //
        private string strEditedVisitIDs = "";
        bool boolProcessRunning = false;
        private int intErrorValue = 0;
        private string strErrorMessage = "";
        SqlDataAdapter sdaDataMerge = null;
        DataSet dsDataMerge = null;
        SqlCommand cmd = null;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        #endregion

        public frm_GC_Snow_Callout_Manager()
        {
            InitializeComponent();
        }

        private void frm_GC_Snow_Callout_Manager_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4010;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();
 
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04160_GC_Snow_Callout_ManagerTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "SnowClearanceCallOutID");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Add record selection checkboxes to popup Callout Type grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04001_GC_Job_CallOut_StatusesTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp04001_GC_Job_CallOut_StatusesTableAdapter.Fill(dataSet_GC_Core.sp04001_GC_Job_CallOut_Statuses);
            gridControl3.ForceInitialize();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            i_dtStart = DateTime.Today;
            i_dtEnd = DateTime.Today.AddDays(1).AddSeconds(-1);  // Should give todays date + 23:59:59 //
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutPictureFilesFolderInternal").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Callout Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Callout Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }


            sp04161_GC_Snow_Clearance_Callout_Linked_Extra_CostsTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "ExtraCostID");

            sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState3 = new RefreshGridState(gridView4, "SnowClearanceCallOutRateID");

            sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState12 = new RefreshGridState(gridView15, "CRMID");

            sp04340_GC_Service_Areas_Linked_To_Snow_CalloutTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState6 = new RefreshGridState(gridView6, "SnowClearanceServicedSiteID");

            sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewState5 = new RefreshGridState(gridView5, "PictureID");


            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionStringREADONLY);
                i_str_SavedDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Self-Billing Invoice Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Self-Billing Invoice Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_SavedDirectoryName.EndsWith("\\")) i_str_SavedDirectoryName += "\\";  // Add Backslash to end //
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionStringREADONLY);
                i_str_PDFDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "SnowClearanceTeamPOPDFPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Saved PDF Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Saved PDF Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_PDFDirectoryName.EndsWith("\\")) i_str_PDFDirectoryName += "\\";  // Add Backslash to end //
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
         

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                tabbedControlGroupFilterType.SelectedTabPageIndex = 1;
                VisitIDsMemoEdit.EditValue = strPassedInDrillDownIDs;
                //popupContainerEdit2.Text = "Custom Filter";
                Load_Data();  // Load records //
            }

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_GC_Snow_Callout_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    if (!string.IsNullOrEmpty(istr_FurtherAction))
                    {
                        GridView view = (GridView)gridControl1.MainView;
                        if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                        if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear and Find in place //         
                    }
                    Load_Data();
                    if (!string.IsNullOrEmpty(istr_FurtherAction))
                    {
                        string strAction = istr_FurtherAction;
                        istr_FurtherAction = "";
                        if (strAction == "edit")
                        {
                            Edit_Record();
                        }
                        else if (strAction == "blockedit")
                        {
                            Block_Edit();
                        }
                        else if (strAction == "send")
                        {
                            Send_TeamPOs();
                        }
                    }
                }
                else if (UpdateRefreshStatus == 2)
                {
                    Load_Data();  // Require this to repopulate main list to show updated total costs //
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_GC_Snow_Callout_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ColourCode", (checkEditColourCode.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CallOutFilter", i_str_selected_CallOutType_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }


        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Colour Code //
                string strColourCode = default_screen_settings.RetrieveSetting("ColourCode");
                if (!string.IsNullOrEmpty(strColourCode)) checkEditColourCode.Checked = (strColourCode == "0" ? false : true);

                // Gritting Callout Filter //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("CallOutFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl3.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["Value"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit2.Text = PopupContainerEdit1_Get_Selected();
                }

                Load_Data();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                Point location = dockPanelFilters.PointToScreen(Point.Empty);

                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);

            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (boolProcessRunning) return;  // Abort since process already running //

            dataSet_GC_Snow_Core.sp04160_GC_Snow_Callout_Manager.Clear(); // Clear all existing data since the data picked up by the merge may be a smaller dataset due to any new filters in place //

            backgroundWorker1.RunWorkerAsync();
            boolProcessRunning = true;
            SetLoadingVisibility(true);
        }

        private void Load_Data()
        {
            if (boolProcessRunning) return;  // Abort since process already running //
            backgroundWorker1.RunWorkerAsync();
            boolProcessRunning = true;
            SetLoadingVisibility(true);
        }

        private void Populate_Visits()
        {
            intErrorValue = 0;  // Reset error flag //
            strErrorMessage = "";
            sdaDataMerge = new SqlDataAdapter();
            dsDataMerge = new DataSet("NewDataSet");

            if (i_str_selected_CallOutType_ids == null) i_str_selected_CallOutType_ids = "";

            string strTempDrilldownIDs = "";
            if (UpdateRefreshStatus > 0)
            {
                UpdateRefreshStatus = 0;
                // Merge just changed rows back in to dataset - no need to reload the full dataset //
                strTempDrilldownIDs = (string.IsNullOrWhiteSpace(i_str_AddedRecordIDs1) ? strEditedVisitIDs : i_str_AddedRecordIDs1.Replace(';', ','));
                if (string.IsNullOrEmpty(strTempDrilldownIDs))
                {
                    backgroundWorker1.CancelAsync();  // Nothing to load so abort cleanly //
                    return;
                }
            }
            else if (tabbedControlGroupFilterType.SelectedTabPageIndex == 1 && !string.IsNullOrWhiteSpace(VisitIDsMemoEdit.EditValue.ToString()))
            {
                strTempDrilldownIDs = VisitIDsMemoEdit.EditValue.ToString();
            }
            try
            {
                using (var conn = new SqlConnection(strConnectionStringREADONLY))
                {
                    // Load data into memory - Background worked will merge this on RunWorkerCompleted event //
                    conn.Open();
                    cmd = new SqlCommand("sp04160_GC_Snow_Callout_Manager", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DrillDownIDs", strTempDrilldownIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", i_dtStart));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", i_dtEnd));
                    cmd.Parameters.Add(new SqlParameter("@CalloutStatuses", i_str_selected_CallOutType_ids));
                    sdaDataMerge = new SqlDataAdapter(cmd);
                    sdaDataMerge.Fill(dsDataMerge, "Table");
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == -2)  // SQL Server Time-Out //
                {
                    intErrorValue = -2;
                    return;
                }
                else  // Other SQL Server issue //
                {
                    intErrorValue = -3;
                    strErrorMessage = ex.Message.ToString();
                    return;
                }
            }
            catch (Exception ex)  // Some other issue //
            {
                intErrorValue = -1;
                strErrorMessage = ex.Message.ToString();
                return;
            }
        }


        private void LoadLinkedRecords()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["SnowClearanceCallOutID"])) + ',';
            }

            //Populate Linked Documents //
            gridControl2.MainView.BeginUpdate();
            this.RefreshGridViewState2.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_GC_Snow_Core.sp04161_GC_Snow_Clearance_Callout_Linked_Extra_Costs.Clear();
            }
            else
            {
                sp04161_GC_Snow_Clearance_Callout_Linked_Extra_CostsTableAdapter.Fill(dataSet_GC_Snow_Core.sp04161_GC_Snow_Clearance_Callout_Linked_Extra_Costs, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl2.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl2.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ExtraCostID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }

            //Populate Linked Rates //
            gridControl4.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_GC_Snow_Core.sp04162_GC_Snow_Clearance_Callout_Linked_Rates.Clear();
            }
            else
            {
                sp04162_GC_Snow_Clearance_Callout_Linked_RatesTableAdapter.Fill(dataSet_GC_Snow_Core.sp04162_GC_Snow_Clearance_Callout_Linked_Rates, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl4.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl4.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SnowClearanceCallOutRateID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }

            if (xtraTabPage3.PageVisible)
            {
                // Populate CRM Contacts //
                gridControl15.MainView.BeginUpdate();
                this.RefreshGridViewState12.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.woodPlanDataSet.sp05089_CRM_Contacts_Linked_To_Record.Clear();
                }
                else
                {
                    sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.Fill(woodPlanDataSet.sp05089_CRM_Contacts_Linked_To_Record, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 25);
                    this.RefreshGridViewState12.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl15.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs12 != "")
                {
                    strArray = i_str_AddedRecordIDs12.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl15.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["CRMID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs12 = "";
                }
            }

            if (xtraTabPage4.PageVisible)
            {
                // Populate Linked Service Areas //
                gridControl6.MainView.BeginUpdate();
                this.RefreshGridViewState6.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04337_GC_Service_Areas_Linked_To_Gritting_Callout.Clear();
                }
                else
                {

                    sp04340_GC_Service_Areas_Linked_To_Snow_CalloutTableAdapter.Fill(dataSet_GC_Snow_Core.sp04340_GC_Service_Areas_Linked_To_Snow_Callout, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState6.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl6.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs6 != "")
                {
                    strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl6.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SnowClearanceServicedSiteID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs6 = "";
                }
            }

            if (xtraTabPage5.PageVisible)
            {
                // Populate Linked Pictures //
                gridControl5.MainView.BeginUpdate();
                this.RefreshGridViewState5.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04092_GC_Gritting_Callout_Pictures_For_Callout.Clear();
                }
                else
                {
                    sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter.Fill(dataSet_GC_Core.sp04092_GC_Gritting_Callout_Pictures_For_Callout, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), strDefaultPath, 1);
                    this.RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl5.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs5 != "")
                {
                    strArray = i_str_AddedRecordIDs5.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl5.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["PictureID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs5 = "";
                }
            }

        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3, string strFurtherAction, string strNewIDs12, string strNewIDs6, string strNewIDs5)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
            istr_FurtherAction = strFurtherAction;
            if (strNewIDs12 != "") i_str_AddedRecordIDs12 = strNewIDs12;
            if (strNewIDs6 != "") i_str_AddedRecordIDs6 = strNewIDs6;
            if (strNewIDs5 != "") i_str_AddedRecordIDs5 = strNewIDs5;
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Reassign Jobs Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_ReassignJobsButtonEnabled = true;
                        }
                        break;
                    case 2:  // Authorise Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_AuthoriseButtonEnabled = true;
                        }
                        break;
                    case 3:  // Pay Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_PayButtonEnabled = true;
                        }
                        break;
                    case 4:  // Send Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_SendButtonEnabled = true;
                        }
                        break;
                    case 5:  // Add Wizard Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_AddWizardButtonEnabled = true;
                        }
                        break;
                    case 6:  // Edit Team Purchase Order Layout Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_EditPOLayout = true;
                        }
                        break;
                    case 7:  // View Team Purchase Order Layout Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_ViewPOLayout = true;
                        }
                        break;
                    case 8:  // Show Column Chooser for GridControl 1 //
                        if (sfpPermissions.blRead)
                        {
                            iBool_EnableGridColumnChooser = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            switch (i_int_FocusedGrid)
            {
                case 1:    // Gritting Callouts //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                            bbiBlockAdd.Enabled = true;
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case 2:    // Linked Extra Costs //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            GridView viewParent = (GridView)gridControl1.MainView;
                            int[] intRowHandlesParent;
                            intRowHandlesParent = viewParent.GetSelectedRows();
                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case 3:    // Linked Rates //
                    {
                        bsiAdd.Enabled = false;
                        bbiSingleAdd.Enabled = false;
                        bbiBlockAdd.Enabled = false;
                        bsiEdit.Enabled = false;
                        bbiSingleEdit.Enabled = false;
                        bbiBlockEdit.Enabled = false;
                        bbiDelete.Enabled = false;
                     }
                    break;

                case 15:  // CRM Contact //
                    view = (GridView)gridControl15.MainView;
                    intRowHandles = view.GetSelectedRows();
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        //if (intRowHandlesParent.Length >= 2)
                        //{
                        //    alItems.Add("iBlockAdd");
                        //    bbiBlockAdd.Enabled = true;
                        //}
                        //alItems.Add("iBlockAdd");
                        //bbiBlockAdd.Enabled = true;
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case 6:    // Linked Service Areas //
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            GridView viewParent = (GridView)gridControl1.MainView;
                            int[] intRowHandlesParent;
                            intRowHandlesParent = viewParent.GetSelectedRows();
                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case 5:    // Linked Pictures //
                    {
                        view = (GridView)gridControl5.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            GridView viewParent = (GridView)gridControl1.MainView;
                            int[] intRowHandlesParent;
                            intRowHandlesParent = viewParent.GetSelectedRows();
                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of CRM Contact Grid navigator custom buttons //
            view = (GridView)gridControl15.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl15.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl15.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl15.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl15.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl15.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl15.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set Enabled Status of Main Toolbar Buttons //
            bbiReassignJobs.Enabled = iBool_ReassignJobsButtonEnabled;
            bbiAuthorise.Enabled = iBool_AuthoriseButtonEnabled;
            bbiPay.Enabled = iBool_PayButtonEnabled;
            bsiSendJobs.Enabled = iBool_SendButtonEnabled;
            bbiAddJobs.Enabled = iBool_AddWizardButtonEnabled;
            bbiEditTeamPOLayout.Enabled = iBool_EditPOLayout;

            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length == 1 && iBool_ViewPOLayout)
            {
                bbiPreviewTeamPO.Enabled = true;
            }
            else
            {
                bbiPreviewTeamPO.Enabled = false;
            }

            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView6 navigator custom buttons //
            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView5 navigator custom buttons //
            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl5.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl5.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Callout //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                        frm_GC_Snow_Callout_Edit fChildForm = new frm_GC_Snow_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Linked Extra Costs //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Extra_Cost_Edit fChildForm2 = new frm_GC_Snow_Callout_Extra_Cost_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SnowClearanceCallOutID"));

                            string strSelectedValue = "Site: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName")))
                                                        + "   Team: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName"))) ? "No Team" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName")))
                                                        + "   Callout Time: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime"))) ? "No Callout Time" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime")));
                            fChildForm2.strLinkedToRecordDescription = strSelectedValue;
                        }

                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 15:     // CRM Contacts //
                    {
                        if (!iBool_AllowAdd) return;

                        // Check if only one parent tender selected - if yes, pass it to child screen otherwise pass none //
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //

                        frm_Core_CRM_Contact_Edit fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        int intClientID = (intRowHandles.Length == 1 ? Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")) : 0);
                        string strClientName = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "ClientName")) ?? "Unknown Client";
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SnowClearanceCallOutID"));

                            string strSelectedValue = "Site: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName")))
                                                        + "   Team: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName"))) ? "No Team" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName")))
                                                        + "   Callout Time: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime"))) ? "No Callout Time" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime")));
                            fChildForm.strLinkedToRecordDesc = strSelectedValue;
                        }
                        fChildForm.intClientID = intClientID;
                        fChildForm.strClientName = strClientName;
                        fChildForm.intRecordTypeID = 25;  // Snow Callout //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 6:     // Linked Service Areas //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Serviced_Area_Edit fChildForm2 = new frm_GC_Snow_Callout_Serviced_Area_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SnowClearanceCallOutID"));

                            string strSelectedValue = "Site: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName")))
                                                        + "   Team: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName"))) ? "No Team" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName")))
                                                        + "   Callout Time: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime"))) ? "No Callout Time" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime")));
                            fChildForm2.strLinkedToRecordDescription = strSelectedValue;
                        }

                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 5:     // Linked Pictures //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl5.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Picture_Edit fChildForm2 = new frm_GC_Callout_Picture_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SnowClearanceCallOutID"));

                            string strSelectedValue = "Site: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName"))) ? "Unknown Site" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SiteName")))
                                                        + "   Team: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName"))) ? "No Team" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubContractorName")))
                                                        + "   Callout Time: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime"))) ? "No Callout Time" : Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CallOutDateTime")));
                            fChildForm2.strLinkedToRecordDescription = strSelectedValue;
                        }

                        fChildForm2.intLinkedRecordTypeID = 1;  // 0 = Gritting, 1 = Snow //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Gritting Callout //
                    {
                        if (!iBool_AllowAdd) return;
                        Add_Callouts_Via_Wizard();
                        break;
                    }
                case 2:     // Related Documents Link //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Extra_Cost_Edit fChildForm2 = new frm_GC_Snow_Callout_Extra_Cost_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 6:     // Linked Site Service Areas //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Serviced_Area_Edit fChildForm2 = new frm_GC_Snow_Callout_Serviced_Area_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 5:     // Related Picture //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Picture_Edit fChildForm2 = new frm_GC_Callout_Picture_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intLinkedRecordTypeID = 1;  // 0 = Gritting, 1 = Snow //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ',');
                        }
                        strEditedVisitIDs = sb.ToString();  // Put IDs into the instance var so we can merge changes if any are made without reloading the whole screen //

                        frm_GC_Snow_Callout_Edit fChildForm = new frm_GC_Snow_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strEditedVisitIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Linked Extra Costs //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ExtraCostID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Extra_Cost_Edit fChildForm2 = new frm_GC_Snow_Callout_Extra_Cost_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 15:  // Linked CRM Contacts //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl15.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_Core_CRM_Contact_Edit fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 6:     // Linked Service Areas //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceServicedSiteID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Serviced_Area_Edit fChildForm2 = new frm_GC_Snow_Callout_Serviced_Area_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 5:     // Linked Pictures //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl5.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Picture_Edit fChildForm2 = new frm_GC_Callout_Picture_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Callout //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID")) + ',');
                        }
                        strEditedVisitIDs = sb.ToString();  // Put IDs into the instance var so we can merge changes if any are made without reloading the whole screen //

                        frm_GC_Snow_Callout_Edit fChildForm = new frm_GC_Snow_Callout_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strEditedVisitIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Linked Extra Costs //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ExtraCostID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Extra_Cost_Edit fChildForm2 = new frm_GC_Snow_Callout_Extra_Cost_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 15:  // Linked CRM Contact //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl15.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_Core_CRM_Contact_Edit fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 6:     // Linked Site Service Areas //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceServicedSiteID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Callout_Serviced_Area_Edit fChildForm2 = new frm_GC_Snow_Callout_Serviced_Area_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 5:     // Linked Pictures //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl5.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Callout_Picture_Edit fChildForm2 = new frm_GC_Callout_Picture_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Callouts //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Callout" : Convert.ToString(intRowHandles.Length) + " Callouts") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Callout" : "these Callouts") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            Array.Reverse(intRowHandles);  // Reverse the order so the last record is deleted first //
                            StringBuilder sb = new StringBuilder();
                            foreach (int intRowHandle in intRowHandles)
                            {
                                sb.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SnowClearanceCallOutID")) + ",");
                                view.DeleteRow(intRowHandle);
                            }

                            using (var RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp03002_EP_Client_Delete("gc_snow_clearance_callout", sb.ToString());  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            view.DeleteSelectedRows();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);                           
                        }
                    }
                    break;
                case 2:  // Linked Extra Costs //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more extra costs to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Extra Cost" : Convert.ToString(intRowHandles.Length) + " Extra Costs") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Extra Cost" : "these Extra Costs") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ExtraCostID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp03002_EP_Client_Delete("gc_snow_clearance_callout_extra_cost", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            //LoadLinkedRecords();
                            Load_Data();  // ***** Load Top level as the total costs for the parent callout(s) will have been updated ***** //

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 15:  // Linked CRM Contacts //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl15.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked CRM Contacts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked CRM Contact" : Convert.ToString(intRowHandles.Length) + " Linked CRM Contacts") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Linked CRM Contact" : "these Linked CRM Contacts") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ",";
                            }
                            DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp01000_Core_Delete("customer_contact", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager.IsSplashFormVisible)
                            {
                                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 6:  // Linked Site Service Areas //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Site Service Areas to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Site Service Area" : Convert.ToString(intRowHandles.Length) + " Linked Site Service Areas") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Linked Site Service Area" : "these Linked Site Service Areas") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceServicedSiteID")) + ",";
                            }
                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp03002_EP_Client_Delete("gc_snow_clearance_callout_serviced_site", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager.IsSplashFormVisible)
                            {
                                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 5:  // Linked Pictures //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl5.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Picture" : Convert.ToString(intRowHandles.Length) + " Pictures") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Picture" : "these Pictures") + " will no longer be available for selection!\n\nNote: The physical picture files will still exist within the Linked Pictures Folder.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp03002_EP_Client_Delete("gc_gritting_callout_picture", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Callouts Available";
                    break;
                case "gridView2":
                    message = "No Linked Extra Costs Available - Select one or more Callouts to view Linked Extra Costs";
                    break;
                case "gridView4":
                    message = "No Linked Rates Available - Select one or more Callouts to view Linked Rates";
                    break;
                case "gridView15":
                    message = "No CRM Contacts - Select one or more Snow Callouts to view Linked CRM Contacts";
                    break;
                case "gridView6":
                    message = "No Linked Service Area Available - Select one or more Callouts to view Linked Service Areas";
                    break;
                case "gridView5":
                    message = "No Linked Pictures Available - Select one or more Callouts to view Linked Pictures";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedRecords();
                    view = (GridView)gridControl2.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl4.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl15.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl6.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl5.MainView;
                    view.ExpandAllGroups();
                    Set_Selected_Count();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("reload_selected".Equals(e.Button.Tag))
                    {
                        Reload_Selected_Records_Only();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "SubContractorName")
            {
                switch (view.GetRowCellValue(e.RowHandle, "SubContractorName").ToString())
                {
                    case "":
                        //e.Appearance.BackColor = Color.LightCoral;
                        //e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "TeamPOFileName":
                        if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "TeamPOFileName").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            // Makes sure grid is focused if not previously focused and user right-clicks on the grid //
            GridView view = sender as GridView;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && !view.GridControl.IsFocused) view.GridControl.Select();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            GridView view = (GridView)sender;
            ExtendedGridMenu egmMenu = new ExtendedGridMenu(view);

            // Disable Column Chooser Customize //
            if (e.MenuType == GridMenuType.Column)
            {
                DXMenuItem miCustomize = GetItemByStringId(e.Menu, GridStringId.MenuColumnColumnCustomization);
                if (miCustomize != null) miCustomize.Enabled = iBool_EnableGridColumnChooser;
            }

            egmMenu.PopupMenuShowing(sender, e);
        }
        private DXMenuItem GetItemByStringId(DXPopupMenu menu, GridStringId id)
        {
            foreach (DXMenuItem item in menu.Items)
            {
                if (item.Caption == GridLocalizer.Active.GetLocalizedString(id)) return item;
            }
            return null;
        }

        private void Set_Selected_Count()
        {
            GridView view = (GridView)gridControl1.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            if (intSelectedCount == 0)
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Callout Selected" : "<color=red>" + intSelectedCount.ToString() + " Callouts</Color> Selected");
                }
                bsiSelectedCount.Caption = strText;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "TeamPOFileName":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("TeamPOFileName").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditTeamPOFile_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strPath = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "SnowClearanceTeamPOPDFPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Team Purchase Order PDFs (from the System Configuration Screen).\n\nPlease try again. If the problem persists, contact Technical Support.", "Get Linked Team Purchase Order PDF Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!strPath.EndsWith("\\")) strPath += "\\";
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "TeamPOFileName").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("NoTeam Purchase Order PDF Linked - unable to proceed.", "View Team Purchase Order PDF", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strPath + strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Team Purchase Order PDF: " + strPath + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Team Purchase Order PDF", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void Reload_Selected_Records_Only()
        {
            strEditedVisitIDs = Get_Selected_IDs((GridView)gridControl1.MainView, "SnowClearanceCallOutID");
            if (string.IsNullOrWhiteSpace(strEditedVisitIDs)) return;
            UpdateRefreshStatus = 1;
            //Set_DictionaryDataRefreshes_All_Value(dictionaryDataRefreshesMain, true);
            Load_Data();
        }
        private string Get_Selected_IDs(GridView view, string ColumnName)
        {
            int intRowCount = view.DataRowCount;
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0) return "";
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellValue(intRowHandle, ColumnName).ToString() + ",");
            }
            return sb.ToString();
        }
       
        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {
            // Makes sure grid is focused if not previously focused and user right-clicks on the grid //
            GridView view = sender as GridView;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && !view.GridControl.IsFocused) view.GridControl.Select();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView4_MouseDown(object sender, MouseEventArgs e)
        {
            // Makes sure grid is focused if not previously focused and user right-clicks on the grid //
            GridView view = sender as GridView;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && !view.GridControl.IsFocused) view.GridControl.Select();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView15 - CRM Contacts

        private void gridView15_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView15_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 15;
            SetMenuStatus();
        }

        private void gridView15_MouseDown(object sender, MouseEventArgs e)
        {
            // Makes sure grid is focused if not previously focused and user right-clicks on the grid //
            GridView view = sender as GridView;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && !view.GridControl.IsFocused) view.GridControl.Select();
        }

        private void gridView15_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 15;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl15_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView6

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseDown(object sender, MouseEventArgs e)
        {
            // Makes sure grid is focused if not previously focused and user right-clicks on the grid //
            GridView view = sender as GridView;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && !view.GridControl.IsFocused) view.GridControl.Select();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 6;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView5

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_MouseDown(object sender, MouseEventArgs e)
        {
            // Makes sure grid is focused if not previously focused and user right-clicks on the grid //
            GridView view = sender as GridView;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && !view.GridControl.IsFocused) view.GridControl.Select();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 5;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView5_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "PicturePath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "PicturePath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView5_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "PicturePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("PicturePath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
 
        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Callout Status Filter Panel

        private void btnGritCalloutFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 3;
            //SetMenuStatus();
        }

        private void popupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_CallOutType_ids = "";    // Reset any prior values first //
            i_str_selected_CallOutType_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CallOutType_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CallOutType_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CallOutType_names;
        }

        #endregion


        #region DateTime

        private void dateEditFromDate_EditValueChanged(object sender, EventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (de.DateTime >= DateTime.MinValue && de.DateTime <= DateTime.MaxValue)
            {
                i_dtStart = (de.DateTime >= DateTime.MinValue && de.DateTime <= DateTime.MaxValue ? de.DateTime : DateTime.MinValue);
            }
        }

        private void dateEditToDate_EditValueChanged(object sender, EventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (de.DateTime >= DateTime.MinValue && de.DateTime <= DateTime.MaxValue)
            {
                i_dtEnd = (de.DateTime >= DateTime.MinValue && de.DateTime <= DateTime.MaxValue ? de.DateTime : DateTime.MaxValue);
            }
        }

        #endregion


        #region Toolbar Buttons

        private void bbiSelectSetReadyToSend_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 10 (Started - To Be Completed - No Authorisation Required) or 30 (Started - To Be Completed - Authorised) //
            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "SnowClearanceSiteContractID")) == 0) continue;
                if (Convert.ToInt32(view.GetRowCellValue(i, "SubContractorID")) == 0) continue;
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 10) view.SelectRow(i);

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void SetJobStatus(int intStatus)
        {
            // First... Check selected jobs are all appropriate - Status of 10 - if not, de-select them //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to set as 'Ready to Send' before proceeding.", "Set Callouts as 'Ready to Send'", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Selected Jobs...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.SelectedRowsCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < intRowHandles.Length; i++)
            {
                if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID")) == 10))
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [Wrong Status] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "SnowClearanceSiteContractID")) == 0)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [Missing SiteGrittingContractID] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "SubContractorID")) == 0)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [Missing SubContractorID] //
                    continue;
                }
                if (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[i], "ClientPONumber").ToString()))
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [Missing Client PO] //
                    continue;
                }
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to set as 'Ready to Send' before proceeding.\n\nNote: This process de-selects any selected jobs which are not ready to set as 'Ready to Send'...\n\nJobs must have a Site, Team, Client P.O. Number and appropriate Status.", "Set Callouts as 'Ready to Send'", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to change status so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Job" : intRowHandles.Length.ToString() + " Jobs") + " selected for setting as 'Ready to Send'.\n\nProceed?", "set Callout(s) as 'Ready to Send'", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Changing Job Status...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            string strJobIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strJobIDs += view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID").ToString() + ",";

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            try
            {
                DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter SetStatus = new DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter();
                SetStatus.ChangeConnectionString(strConnectionString);
                SetStatus.sp04182_GC_Snow_Set_Job_Status(strJobIDs, intStatus);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while changing the status of the selected  Callouts.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Set Callouts as 'Ready To Send'", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();  // Refresh grid to reflect record's updated status //

            DevExpress.XtraEditors.XtraMessageBox.Show("Callout Statuses Changed Successfully.", "Set Callouts as 'Ready to Send'", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void bbiSetReadyToSend1_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetJobStatus(50);  // 50: Completed - Ready To Send //
        }

        private void bbiSetReadyToSend2_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetJobStatus(50);  // 50: Completed - Ready To Send //
        }


        private void bbiSelectJobsReadyToSend_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 50 (Ready to send) //
            GridView view = (GridView)gridControl1.MainView;
            view.ClearSelection();

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 50 && Convert.ToDecimal(view.GetRowCellValue(i, "HoursWorked")) > (decimal)0.00) view.SelectRow(i);

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

        }

        private void bbiSendJobs1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Send_TeamPOs();
        }
        private void bbiSendJobs2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Send_TeamPOs();
        }
        private void Send_TeamPOs()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more callouts to send before proceeding.", "Send Team Purchase Orders", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check for internet Connectivity //
            bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
            if (!boolNoInternet) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send emails.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check for connectiviety to TextAanywhere service and get the username and password for the service... //
            bool boolTextServiceAvailable = true;
            bool boolTextServiceLoginDetailsAvailable = true;
            boolTextServiceAvailable = BaseObjects.PingTest.Ping("textanywhere.net");  // Check for internet Connectivity to Text Service //            
            if (!boolTextServiceAvailable)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the Text Anywhere service - No Linked Text Messages between snow and gritting teams can be sent.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strTextServiceUsername = "";
            string strTextServicePassword = "";
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            strTextServiceUsername = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "TextAnywhereUsername").ToString();
            strTextServicePassword = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "TextAnywherePassword").ToString();
            if (string.IsNullOrEmpty(strTextServiceUsername) || string.IsNullOrEmpty(strTextServicePassword)) boolTextServiceLoginDetailsAvailable = false;
            if (!boolTextServiceLoginDetailsAvailable)
            {
                 DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the Text Anywhere service [Missing Connection Credentials] - No Linked Text Messages between snow and gritting teams can be sent.", "Get Text Service Login Credentials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Selected Callouts...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.SelectedRowsCount / 10;
            int intUpdateProgressTempCount = 0;

            DateTime dtToday = DateTime.Today;
            DateTime dtTodayWithTime = DateTime.Now;
            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < intRowHandles.Length; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) != 50 || Convert.ToDecimal(view.GetRowCellValue(i, "HoursWorked")) <= (decimal)0.00) view.UnselectRow(i); // De-select callout as it is not ready to be sent [wrong status] or not HoursWorked //
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more callouts to send before proceeding.\n\nNote: This process de-selects any selected callouts which are not ready to be sent [callouts must have a Job Status of 'Ready to Send' and an Hours Worked greater than zero].", "Send Team Purchase Orders", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get DB Settings for generated PDF Team Purchase Orders //
            string strEmailBodyFile = "";
            string strEmailFrom = "";
            string strEmailSubjectLine = "";
            string strCCToEmailAddress = "";
            string strPDFFolderPath = "";
            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp04193_GC_Snow_Clearance_Team_PO_System_Settings", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
            DataSet dsSettings = new DataSet("NewDataSet");
            try
            {
                sdaSettings.Fill(dsSettings, "Table");
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Team PO PDF Folder and Email Settings' (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dsSettings.Tables[0].Rows.Count != 1)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Team PO PDF Folder and Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DataRow dr1 = dsSettings.Tables[0].Rows[0];
            strEmailBodyFile = dr1["BodyFileName"].ToString();
            strEmailFrom = dr1["EmailFrom"].ToString();
            strEmailSubjectLine = dr1["SubjectLine"].ToString();
            strCCToEmailAddress = dr1["CCToName"].ToString();
            strPDFFolderPath = dr1["PDFFolder"].ToString();
            strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
            strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
            strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
            strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

            if (string.IsNullOrEmpty(strEmailBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strPDFFolderPath) || string.IsNullOrEmpty(strSMTPMailServerAddress))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the folder and email settings (Email Layout File, From Email Address, PDF File Folder and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strPDFFolderPath.EndsWith("\\")) strPDFFolderPath += "\\";  // Add Backslash to end //

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            // If at this point, we are good to generate Team POs so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Snow Clearance Callout" : intRowHandles.Length.ToString() + " Snow Clearance Callouts") + " selected for emailing to Teams.\n\nProceed?", "Create Team Purchase Orders", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Creating Team POs...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            int intSentCount = 0;
            int intSubContractorID = 0;
            int intSnowClearanceCallOutID = 0;
            string strPDFName = "";
            string strPDFNameJustName = "";
            string strEmailPassword = "";

            string strSitesRequiringGrit = "";
            int intSiteID = 0;
            string strFoundMatchDetails = "";
            int intMatchingRecordID = 0;
            string strMatchingTeamName = "";
            int intMatchingTeamID = 0;
            string strMatchingTeamMobile = "";
            string strLinkedTextMessage1 = "";
            string strLinkedTextMessage2 = "";
            string strTuncatedSiteID = "";
            string strTuncatedClientName = "";
            string strTuncatedSiteName = "";
            string strTuncatedSubContractorName = "";
            string strTruncatedPhoneNumber = "";
            DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter GetLinkedCallout = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
            GetLinkedCallout.ChangeConnectionString(strConnectionString);
            DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter StoreLinks = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
            StoreLinks.ChangeConnectionString(strConnectionString);

            DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter GetEmailAddress = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
            GetEmailAddress.ChangeConnectionString(strConnectionString);
            string strEmailAddresses = "";
            char[] delimiters = new char[] { ',' };
            char[] delimiters2 = new char[] { '|' };
            try
            {
                string strBody = System.IO.File.ReadAllText(strEmailBodyFile);
                foreach (int intRowHandle in intRowHandles)
                {
                    intSubContractorID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SubContractorID"));
                    intSnowClearanceCallOutID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID"));
                    strPDFName = "Team_PO_" + intSnowClearanceCallOutID.ToString().PadLeft(8, '0') + "_" + dtTodayWithTime.ToString("yyyy-MM-dd_HH_mm_ss") + ".PDF";

                    // PDF now created so email to team... //
                    strEmailAddresses = GetEmailAddress.sp04195_GC_Team_Email_Addresses_For_Team_PO_Email(intSubContractorID).ToString();
                    if (!String.IsNullOrEmpty(strEmailAddresses))
                    {
                        string[] strEmailAddressAndPassword = strEmailAddresses.Split(delimiters2);
                        if (strEmailAddressAndPassword.Length > 0)
                        {
                            strEmailAddresses = strEmailAddressAndPassword[strEmailAddressAndPassword.Length - 1];  // remove the email password from the front of the email address for use later //
                            strEmailPassword = (strEmailAddressAndPassword.Length > 1 ? strEmailAddressAndPassword[0] : "");
                        }
                    }

                    // Create Team Purchase Order report and save it to PDF //
                    string strReportFileName = "SnowClearanceTeamPurchaseOrderLayout1.repx";
                    rpt_GC_Snow_Clearance_Team_Purchase_Order rptReport = new rpt_GC_Snow_Clearance_Team_Purchase_Order(this.GlobalSettings, intSnowClearanceCallOutID);
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);

                    // Set security options of report so when it is exported, it can't be edited and is password protected //
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
                    rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsPassword = "GroundControlXX";
                    if (!string.IsNullOrEmpty(strEmailPassword)) rptReport.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = strEmailPassword;

                    strPDFNameJustName = strPDFName;  // Need this for later when the filename is written into the callout record //
                    strPDFName = strPDFFolderPath + strPDFName;  // Put path onto start of filename //
                    rptReport.ExportToPdf(strPDFName);

                    if (!String.IsNullOrEmpty(strEmailAddresses))
                    {
                        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                        msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                        string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (strEmailTo.Length > 0)
                        {
                            foreach (string strEmailAddress in strEmailTo)
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                            }
                        }
                        else
                        {
                            msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                        }
                        msg.Subject = strEmailSubjectLine;
                        if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                        msg.Priority = System.Net.Mail.MailPriority.High;
                        msg.IsBodyHtml = true;

                        System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                        System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                        // Create a new attachment //
                        System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strPDFName); //create the attachment
                        msg.Attachments.Add(mailAttachment);

                        //create the LinkedResource (embedded image)
                        System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                        logo.ContentId = "companylogo";
                        //add the LinkedResource to the appropriate view
                        htmlView.LinkedResources.Add(logo);

                        //create the LinkedResource (embedded image)
                        System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                        logo2.ContentId = "companyfooter";
                        //add the LinkedResource to the appropriate view
                        htmlView.LinkedResources.Add(logo2);

                        msg.AlternateViews.Add(plainView);
                        msg.AlternateViews.Add(htmlView);

                        object userState = msg;
                        System.Net.Mail.SmtpClient emailClient = null;
                        if (intSMTPMailServerPort != 0)
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                        }
                        else
                        {
                            emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                        }
                        if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                        {
                            System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                            emailClient.UseDefaultCredentials = false;
                            emailClient.Credentials = basicCredential;
                        }
                        emailClient.SendAsync(msg, userState);

                        // Update DB (write generated PDF report filename into callout and change callout status to sent) //
                        DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter UpdateCallout = new DataSet_GC_Snow_DataEntryTableAdapters.QueriesTableAdapter();
                        UpdateCallout.ChangeConnectionString(strConnectionString);
                        UpdateCallout.sp04194_GC_Update_Callout_With_Team_PO_Name(intSnowClearanceCallOutID, strPDFNameJustName);
                        intSentCount++;

                        #region SendLinkTexts
                        // Check if a gritting callout is available if yes, send linking text messages between snow and grit teams, otherwise add SiteID to list of required Grits //
                        if (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "GritMobileTelephoneNumber").ToString()))
                        {
                            intSiteID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteID"));
                            strFoundMatchDetails = GetLinkedCallout.sp04233_GC_Get_Linked_Callout_Record(intSiteID, 1).ToString();
                            string[] strMatchingRecordDetails = strFoundMatchDetails.Split(delimiters2);  // | delimeter //
                            if (strMatchingRecordDetails.Length == 4)
                            {
                                intMatchingRecordID = Convert.ToInt32(strMatchingRecordDetails[0]);
                                strMatchingTeamName = strMatchingRecordDetails[1];
                                strMatchingTeamMobile = strMatchingRecordDetails[2];
                                intMatchingTeamID = Convert.ToInt32(strMatchingRecordDetails[3]);

                                if (intMatchingRecordID == 0)  // No Matching Record so we need to store this site so we can open the Gritting Callout Wizard with this ID later //
                                {
                                    strSitesRequiringGrit += intSiteID.ToString() + ",";
                                }
                                else
                                {
                                    // Check to ensure the Grit and Snow Clear are linked to different teams - if they match we don't need any text messages //
                                    if (intMatchingTeamID != intSubContractorID)
                                    {
                                        if (strMatchingTeamMobile != "0")
                                        {
                                            string[] strSplitSnowTel = view.GetRowCellValue(intRowHandle, "GritMobileTelephoneNumber").ToString().Split(delimiters);  // Snow Tel No //
                                            string[] strSplitGritTel = strMatchingTeamMobile.Split(delimiters);  // Grit Tel No //
                                            if (strSplitGritTel.Length > 0 && strSplitSnowTel.Length > 0)
                                            {
                                                strTuncatedSiteID = (intSiteID.ToString().Length > 6 ? intSiteID.ToString().Substring(0, 6) : intSiteID.ToString());
                                                strTuncatedClientName = (view.GetRowCellValue(intRowHandle, "ClientName").ToString().Length > 10 ? view.GetRowCellValue(intRowHandle, "ClientName").ToString().Substring(0, 10) : view.GetRowCellValue(intRowHandle, "ClientName").ToString());
                                                strTuncatedSiteName = (view.GetRowCellValue(intRowHandle, "SiteName").ToString().Length > 10 ? view.GetRowCellValue(intRowHandle, "SiteName").ToString().Substring(0, 10) : view.GetRowCellValue(intRowHandle, "SiteName").ToString());
                                                strTuncatedSubContractorName = (view.GetRowCellValue(intRowHandle, "SubContractorName").ToString().Length > 20 ? view.GetRowCellValue(intRowHandle, "SubContractorName").ToString().Substring(0, 20) : view.GetRowCellValue(intRowHandle, "SubContractorName").ToString());
                                                for (int i = 0; i < strSplitGritTel.Length; i++)  // Send Snow team text message for each recorded gritting mobile tel no //
                                                {
                                                    strLinkedTextMessage1 = "Gritting after snow clearance at " +
                                                                               strTuncatedSiteID + "-" + strTuncatedClientName + "-" + strTuncatedSiteName + ". " +
                                                                               "Contact " + strMatchingTeamName + " on (" + strSplitGritTel[i] + ") to confirm time to attend.";

                                                    Send_Link_Text_Messages(strTextServiceUsername, strTextServicePassword, intSubContractorID, 2, strLinkedTextMessage1, view.GetRowCellValue(intRowHandle, "GritMobileTelephoneNumber").ToString());  // Potential multiple comma seperated recipients //
                                                }

                                                for (int j = 0; j < strSplitSnowTel.Length; j++)    // Send Gritting team text message for each recorded snow clearance mobile tel no //
                                                {
                                                    strTruncatedPhoneNumber = (strSplitSnowTel[j].Length > 20 ? strSplitSnowTel[j].Substring(0, 20) : strSplitSnowTel[j]);
                                                    strLinkedTextMessage2 = "SNOW Clearance scheduled at " +
                                                                                strTuncatedSiteID + "-" + strTuncatedClientName + "-" + strTuncatedSiteName + ". " +
                                                                               "Contact " + strTuncatedSubContractorName + " on (" + strTruncatedPhoneNumber + ") ) to confirm when GRITTING required at site.";
                                                    Send_Link_Text_Messages(strTextServiceUsername, strTextServicePassword, intMatchingTeamID, 3, strLinkedTextMessage2, strMatchingTeamMobile);  // Potential multiple comma seperated recipients //
                                                }
                                            }
                                        }
                                    }
                                    // Update both grit and snow clearance records (Link IDs) with each others ID //
                                     StoreLinks.sp04234_GC_Update_Callouts_With_Linked_Record_IDs(Convert.ToInt32(intMatchingRecordID), intSnowClearanceCallOutID);  // Grit ID, Snow Clearance ID //
                                }
                            }
                        }
                        #endregion
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Team Purchase Order.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Create Team Purchase Orders", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();  // Refresh grid to reflect record's updated status //
            System.Windows.Forms.Application.DoEvents();
            if (intSentCount > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Team Purchase Order" : intRowHandles.Length.ToString() + " Team Purchase Orders") + " Sent Successfully.", "Create Team Purchase Orders", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Team Purchase Orders Sent.\n\nNote that if the Team has no Email address [with Self-Billing flag Ticked] linked to them in the Team Contacts screens, Team Purchase Orders can't be sent to them.", "Create Team Purchase Orders", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!string.IsNullOrEmpty(strSitesRequiringGrit))  // New Gritting Callouts are required to link with one or more of the Sent snow clearance records //
            {
                frm_GC_Callout_Manager frmGrittingCallouts = null;
                try
                {
                    foreach (frmBase frmChild in this.MdiParent.MdiChildren)
                    {
                        if (frmChild.FormID == 4007)
                        {
                            frmChild.Activate();
                            frmGrittingCallouts = (frm_GC_Callout_Manager)frmChild;
                            frmGrittingCallouts.i_str_SiteIDsToLink = strSitesRequiringGrit;  // Pass SiteIDs in so they can be passed through to the Wizard when it is opened //
                            frmGrittingCallouts.bbiAddJobs.PerformClick();  // Click Wizard button //           
                            return;
                        }
                    }
                }
                catch (Exception)
                {
                }
                // If we are here then the form was NOT already open, so open it //
                frmGrittingCallouts = new frm_GC_Callout_Manager();
                fProgress = new frmProgress(10);
                this.AddOwnedForm(fProgress);
                fProgress.Show();  // ***** Closed in PostOpen event ***** //
                Application.DoEvents();
                frmGrittingCallouts.fProgress = fProgress;
                frmGrittingCallouts.MdiParent = this.MdiParent;
                frmGrittingCallouts.GlobalSettings = this.GlobalSettings;
                frmGrittingCallouts.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(frmGrittingCallouts, new object[] { null });
                frmGrittingCallouts.i_str_SiteIDsToLink = strSitesRequiringGrit;  // Pass SiteIDs in so they can be passed through to the Wizard when it is opened //
                frmGrittingCallouts.bbiAddJobs.PerformClick();  // Click Wizard button //

            }
        }
        private void Send_Link_Text_Messages(string strTextServiceUsername, string strTextServicePassword, int intSubContractorID, int intTextMessageType, string strTextMessage, string strPhoneNumber)
        {
            // Add Text Sent Header to DB and pick up the returned Header ID //
            int intNewHeaderID = 0;
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter InsertTextHeader = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                InsertTextHeader.ChangeConnectionString(strConnectionString);
                intNewHeaderID = Convert.ToInt32(InsertTextHeader.sp04104_GC_Get_Callouts_Create_Text_Header(intSubContractorID, intTextMessageType, strTextMessage, strPhoneNumber));
            }
            catch (Exception)
            {
            }
            // Link Jobs to Text Message and update Job Status to sent //
            /*try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter InsertTextHeaderLink = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                InsertTextHeaderLink.ChangeConnectionString(strConnectionString);
                InsertTextHeaderLink.sp04105_GC_Get_Callouts_Create_Text_Header_CallOuts(intNewHeaderID, strCalloutIDs);
            }
            catch (Exception)
            {
            }*/
            if (!string.IsNullOrEmpty(strPhoneNumber))
            {
                TxtMessage msg = new TxtMessage(strPhoneNumber, strTextMessage, intNewHeaderID, strTextServiceUsername, strTextServicePassword);
                msg.Send(new object());
                if (msg.Status != EnmStatus.Ok)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Problem sending Message to " + strPhoneNumber, "Problem sending text", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // Remove the Sent_Text_Message and Link Callouts from the DB since the text message failed to send //
                }
            }
        }


        private void bbiAuthorise_ItemClick(object sender, ItemClickEventArgs e)
        {
            frm_GC_Snow_Authorise_Callouts frmInstance = new frm_GC_Snow_Authorise_Callouts();

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            System.Windows.Forms.Application.DoEvents();

            frmInstance.MdiParent = this.MdiParent;
            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.fProgress = fProgress;
            frmInstance.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmInstance, new object[] { null }); 
        }


        private void bbiReassignJobs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to Reassign before proceeding.", "Reassign Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            for (int i = 0; i < intRowHandles.Length; i++)
            {

                intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                if (!(intJobStatusID < 70 || intJobStatusID == 90)) view.UnselectRow(intRowHandles[i]);
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to Reassign before proceeding.\n\nNote: This process only allows jobs not yet sent or jobs sent but rejected by the team to be re-assigned. Any other jobs are automatically de-selected.", "Reassign Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Now allow the user to change the selected contractor - open screen to allow the user to select the new team //
            string strRecordIDs = ",";  // Put leading comma in so we can easily detect if it contains a value within the coming loop //
            string strSnowClearanceCalloutIDs = "";
            int intSiteGrittingContractID = 0;
            int intRecordCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                intSiteGrittingContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SnowClearanceSiteContractID"));
                if (intSiteGrittingContractID > 0 && !strRecordIDs.Contains("," + intSiteGrittingContractID.ToString() + ","))
                {
                    strRecordIDs += intSiteGrittingContractID.ToString() + ",";
                    strSnowClearanceCalloutIDs += view.GetRowCellValue(intRowHandle, "SnowClearanceCallOutID").ToString() + ",";
                    intRecordCount++;
                }
            }
            strRecordIDs = strRecordIDs.Remove(0, 1);  // Remove leading ',' added earlier //
            frm_GC_Snow_Choose_Team fChildForm = new frm_GC_Snow_Choose_Team();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInSiteSnowClearanceContractIDs = strRecordIDs;
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = intRecordCount;
            fChildForm.boolRemoveBlankRow = true;
            if (fChildForm.ShowDialog() != DialogResult.OK) return; // User Aborted //
            try
            {
                DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter UpdateDatabase = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
                UpdateDatabase.ChangeConnectionString(strConnectionString);
                UpdateDatabase.sp04183_GC_Snow_Callout_Reassign(strSnowClearanceCalloutIDs, fChildForm.intSubContractorID);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to Reassign the selected Callouts.\n\nPlease try again. If the problem persists, contact Technical Support.", "Reassign Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Load_Data();  // Reload to see updated records //
            DevExpress.XtraEditors.XtraMessageBox.Show((intRecordCount == 1 ? "1 Callout" : intRecordCount.ToString() + " Callouts") + " Reassigned Successfully\n\n***** IMPORTANT: REMEMBER TO SEND THESE CALLOUTS *****", "Reassign Callouts", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        private void bbiPay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_GC_Finance_Invoice_Jobs frmInstance = new frm_GC_Finance_Invoice_Jobs();

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            System.Windows.Forms.Application.DoEvents();

            frmInstance.MdiParent = this.MdiParent;
            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.fProgress = fProgress;
            frmInstance.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmInstance, new object[] { null });
        }

        private void bbiViewSiteOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view on the map before proceeding.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = ",";  // Used to stop the same location being picked up more than once //
            double dStartLat = (double)0.00;
            double dStartLong = (double)0.00;
            List<Mapping_Objects> mapObjectsList = new List<Mapping_Objects>();
            foreach (int intRowHandle in intRowHandles)
            {
                if (!strSelectedIDs.Contains(view.GetRowCellValue(intRowHandle, "SiteID").ToString() + ","))
                {
                    dStartLat = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "SiteLocationX").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLocationX")) : (double)0.00);
                    dStartLong = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "SiteLocationY").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLocationY")) : (double)0.00);
                    if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
                    {
                        Mapping_Objects mapObject = new Mapping_Objects();
                        mapObject.doubleLat = dStartLat;
                        mapObject.doubleLong = dStartLong;
                        mapObject.stringToolTip = "Client: " + view.GetRowCellValue(intRowHandle, "ClientName").ToString() + "\nSite: " + view.GetRowCellValue(intRowHandle, "SiteName").ToString();
                        mapObject.stringID = "Site|" + view.GetRowCellValue(intRowHandle, "SiteID").ToString();
                        mapObjectsList.Add(mapObject);
                        strSelectedIDs += view.GetRowCellValue(intRowHandle, "SiteID").ToString() + ",";
                    }
                }
            }
            if (mapObjectsList.Count == 0)  // No objects added due to missing coordinates //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view on the map before proceeding.\n\nTip: Records must have coordinates stored against them before they can be viewed on the map.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                Mapping_Functions mapFunctions = new Mapping_Functions();
                mapFunctions.View_Object_In_Internet_Mapping(this.GlobalSettings, this.strConnectionString, this, mapObjectsList);
            }
        }

        private void bbiViewJobOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view on the map before proceeding.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            double dStartLat = (double)0.00;
            double dStartLong = (double)0.00;
            List<Mapping_Objects> mapObjectsList = new List<Mapping_Objects>();
            foreach (int intRowHandle in intRowHandles)
            {
                dStartLat = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "StartLatitude").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "StartLatitude")) : (double)0.00);
                dStartLong = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "StartLongitude").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "StartLongitude")) : (double)0.00);
                if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
                {
                    Mapping_Objects mapObject = new Mapping_Objects();
                    mapObject.doubleLat = dStartLat;
                    mapObject.doubleLong = dStartLong;
                    mapObject.stringToolTip = "Job Start Position \n\nJob ID: " + view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + "\nClient: " + view.GetRowCellValue(intRowHandle, "ClientName").ToString() + "\nSite: " + view.GetRowCellValue(intRowHandle, "SiteName").ToString();
                    mapObject.stringID = "Job Start|" + view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString();
                    mapObjectsList.Add(mapObject);
                }
                dStartLat = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "FinishedLatitude").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "FinishedLatitude")) : (double)0.00);
                dStartLong = (!string.IsNullOrEmpty(view.GetRowCellValue(intRowHandle, "FinishedLongitude").ToString()) ? Convert.ToDouble(view.GetRowCellValue(intRowHandle, "FinishedLongitude")) : (double)0.00);
                if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
                {
                    Mapping_Objects mapObject = new Mapping_Objects();
                    mapObject.doubleLat = dStartLat;
                    mapObject.doubleLong = dStartLong;
                    mapObject.stringToolTip = "Job End Position \n\nJob ID: " + view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + "\nClient: " + view.GetRowCellValue(intRowHandle, "ClientName").ToString() + "\nSite: " + view.GetRowCellValue(intRowHandle, "SiteName").ToString();
                    mapObject.stringID = "Job End|" + view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString();
                    mapObjectsList.Add(mapObject);
                }
            }
            if (mapObjectsList.Count == 0)  // No objects added due to missing coordinates //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view on the map before proceeding.\n\nTip: Records must have coordinates stored against them before they can be viewed on the map.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                Mapping_Functions mapFunctions = new Mapping_Functions();
                mapFunctions.View_Object_In_Internet_Mapping(this.GlobalSettings, this.strConnectionString, this, mapObjectsList);
            }*/
        }


        private void bbiAddJobs_ItemClick(object sender, ItemClickEventArgs e)
        {
            Add_Callouts_Via_Wizard();
        }

        private void Add_Callouts_Via_Wizard()
        {
            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
            frm_GC_Snow_Callout_Block_Add fChildForm = new frm_GC_Snow_Callout_Block_Add();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strFormMode = "edit";
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.fProgress = fProgress;
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }


        private void bbiPreviewTeamPO_ItemClick(object sender, ItemClickEventArgs e)
        {
            string strReportFileName = "SnowClearanceTeamPurchaseOrderLayout1.repx";

            rpt_GC_Snow_Clearance_Team_Purchase_Order rptReport = null;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to preview Snow Clearance Team Purchase Order - select a Snow Clearance Callout record to preview before proceeding!", "Load Snow Clearance Team Purchase Order Preview", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            rptReport = new rpt_GC_Snow_Clearance_Team_Purchase_Order(this.GlobalSettings, Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SnowClearanceCallOutID")));
            try
            {
                rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                rptReport.ShowRibbonPreview();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void bbiEditTeamPOLayout_ItemClick(object sender, ItemClickEventArgs e)
        {
            string strReportFileName = "SnowClearanceTeamPurchaseOrderLayout1.repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                rpt_GC_Snow_Clearance_Team_Purchase_Order rptReport = null;
                GridView view = (GridView)gridControl1.MainView;
                int[] intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length <= 0)
                {
                    rptReport = new rpt_GC_Snow_Clearance_Team_Purchase_Order(this.GlobalSettings, 0);  // No Data to load with designer //
                }
                else
                {
                    rptReport = new rpt_GC_Snow_Clearance_Team_Purchase_Order(this.GlobalSettings, Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SnowClearanceCallOutID")));
                }
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                loadingForm = new WaitDialogForm("Loading Report Builder...", "Reporting");
                loadingForm.Show();

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                loadingForm.Close();
                form.ShowDialog();
                panel.CloseReport();
            }
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Load_Data();
            if (boolProcessRunning)
            {
                //MessageBox.Show("Process already running.");
                return;
            }
            backgroundWorker1.RunWorkerAsync();
            boolProcessRunning = true;
            SetLoadingVisibility(true);
        }

        #endregion


        private void btnFormatData_Click(object sender, EventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Formatting data...");

            VisitIDsMemoEdit.EditValue = BaseObjects.ExtensionFunctions.FormatStringToCSV(VisitIDsMemoEdit.EditValue.ToString(), true);

            if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
        }


        private void checkEditColourCode_CheckedChanged(object sender, EventArgs e)
        {
            SetGridFormatConditions();
        }

        private void SetGridFormatConditions()
        {
            CheckEdit ce = (CheckEdit)checkEditColourCode;
            if (ce.Checked)
            {
                StyleFormatCondition condition1 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition1.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                condition1.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition1.Appearance.Options.UseBackColor = true;
                condition1.Appearance.ForeColor = Color.Black;
                condition1.ApplyToRow = true;
                condition1.Condition = FormatConditionEnum.Expression;
                condition1.Expression = "[JobStatusID] == 20";
                gridView1.FormatConditions.Add(condition1);

                StyleFormatCondition condition2 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition2.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                condition2.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition2.Appearance.Options.UseBackColor = true;
                condition2.Appearance.ForeColor = Color.Black;
                condition2.ApplyToRow = true;
                condition2.Condition = FormatConditionEnum.Expression;
                condition2.Expression = "[JobStatusID] == 40";
                gridView1.FormatConditions.Add(condition2);

                StyleFormatCondition condition3 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition3.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                condition3.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition3.Appearance.Options.UseBackColor = true;
                condition3.Appearance.ForeColor = Color.Black;
                condition3.ApplyToRow = true;
                condition3.Condition = FormatConditionEnum.Expression;
                condition3.Expression = "[JobStatusID] == 60";
                gridView1.FormatConditions.Add(condition3);


                StyleFormatCondition condition4 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition4.Appearance.BackColor = Color.FromArgb(255, 255, 128);
                condition4.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition4.Appearance.Options.UseBackColor = true;
                condition4.Appearance.ForeColor = Color.Black;
                condition4.ApplyToRow = true;
                condition4.Condition = FormatConditionEnum.Expression;
                condition4.Expression = "[JobStatusID] == 10";
                gridView1.FormatConditions.Add(condition4);

                StyleFormatCondition condition5 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition5.Appearance.BackColor = Color.FromArgb(255, 255, 128);
                condition5.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition5.Appearance.Options.UseBackColor = true;
                condition5.Appearance.ForeColor = Color.Black;
                condition5.ApplyToRow = true;
                condition5.Condition = FormatConditionEnum.Expression;
                condition5.Expression = "[JobStatusID] == 30";
                gridView1.FormatConditions.Add(condition5);


                StyleFormatCondition condition6 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition6.Appearance.BackColor = Color.FromArgb(192, 255, 192);
                condition6.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition6.Appearance.Options.UseBackColor = true;
                condition6.Appearance.ForeColor = Color.Black;
                condition6.ApplyToRow = true;
                condition6.Condition = FormatConditionEnum.Expression;
                condition6.Expression = "[JobStatusID] == 50";
                gridView1.FormatConditions.Add(condition6);
            }
            else
            {
                gridView1.FormatConditions.Clear();
            }

        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Callout Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) LoadLinkedRecords();
        }


        private void barManager1_HighlightedLinkChanged(object sender, DevExpress.XtraBars.HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo { Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip };

            toolTipController1.ShowHint(Info);
        }
 

        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Report...", "Reporting");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //
            }
        }

        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }

        private void bciFilterVisitsSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (bciFilterVisitsSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Callout records to filter by before proceeding.", "Filter Selected Callout Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControl1.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControl1.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControl1.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_GC_Snow_Core.sp04160_GC_Snow_Callout_Manager.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControl1.EndUpdate();
        }


        #region Background Worker

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Populate_Visits();
            if (backgroundWorker1.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            e.Result = "Success";
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            strEditedVisitIDs = "";  // Clear - it will be re-populated if we need to do a partial reload by the caller //
            UpdateRefreshStatus = 0; // Clear - it will be re-populated if we need to do a partial reload by the caller //

            if (e.Cancelled)
            {
                sdaDataMerge = null;
                dsDataMerge = null;
                SetLoadingVisibility(false);
                boolProcessRunning = false;
                return;
            }
            else if (e.Error != null)
            {
                MessageBox.Show("An Error Occurred While Attempting to Load Data.\n\nError: " + (e.Error as Exception).ToString());
            }
            else
            {
                if (intErrorValue < 0)
                {
                    sdaDataMerge = null;
                    dsDataMerge = null;
                    SetLoadingVisibility(false);
                    boolProcessRunning = false;
                    switch (intErrorValue)
                    {
                        case -2:
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Callouts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            break;
                        case -3:
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Callouts Data.\n\nMessage = [" + strErrorMessage + "].\n\nIf the problem persists, contact Technical Support.", "Load Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            break;
                        case -1:
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Callouts Data.\n\nMessage = [" + strErrorMessage + "].\n\nIf the problem persists, contact Technical Support.", "Load Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            break;
                        default:
                            break;
                    }
                    return;
                }
                try
                {
                    beiLoadingProgress.EditValue = "Merging Data...";
                    Application.DoEvents();   // Allow Form time to repaint itself so updated caption is made visible form line above //
                    dataSet_GC_Snow_Core.sp04160_GC_Snow_Callout_Manager.Merge(dsDataMerge.Tables[0]);

                    sdaDataMerge = null;
                    dsDataMerge = null;

                    GridView view = (GridView)gridControl1.MainView;
                    if (tabbedControlGroupFilterType.SelectedTabPageIndex == 1 && strPassedInDrillDownIDs != "") view.ExpandAllGroups();  // Drilldown so expand all groups //

                    // Highlight any recently added new rows //
                    if (i_str_AddedRecordIDs1 != "")
                    {
                        char[] delimiters = new char[] { ';' };
                        string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        int intID = 0;
                        int intRowHandle = 0;
                        view.BeginSelection();
                        view.ClearSelection(); // Clear any current selection so just the new record is selected //
                        foreach (string strElement in strArray)
                        {
                            intID = Convert.ToInt32(strElement);
                            intRowHandle = view.LocateByValue(0, view.Columns["SnowClearanceCallOutID"], intID);
                            if (intRowHandle != GridControl.InvalidRowHandle)
                            {
                                view.SelectRow(intRowHandle);
                                view.MakeRowVisible(intRowHandle, false);
                                view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);  // Tick the new record so it is picked up by the calendar if it is active //
                            }
                        }
                        i_str_AddedRecordIDs1 = "";
                        view.EndSelection();
                    }
                    SetLoadingVisibility(false);
                    boolProcessRunning = false;
                    LoadLinkedRecords();  // Refresh any children //
                }
                catch (Exception) { }
            }
        }

        private void bbiLoadingCancel_ItemClick(object sender, ItemClickEventArgs e)
        {
            //notify background worker we want to cancel the operation.
            //this code doesn't actually cancel or kill the thread that is executing the job.
            try
            {
                cmd.Cancel();  // Kill the SQL Command //
                cmd = null;
                backgroundWorker1.CancelAsync();  // Tell the thread it is cancelled - the line 2 above does the cancel since there is no loop in the DoWork to check for the cancelled status of the thread //
            }
            catch (Exception) { }
        }

        private void SetLoadingVisibility(Boolean boolVisible)
        {
            bbiRefresh.Visibility = (boolVisible ? BarItemVisibility.Never : BarItemVisibility.Always);
            beiLoadingProgress.Visibility = (boolVisible ? BarItemVisibility.Always : BarItemVisibility.Never);
            bbiLoadingCancel.Visibility = (boolVisible ? BarItemVisibility.Always : BarItemVisibility.Never);
            if (!boolVisible) beiLoadingProgress.EditValue = "Loading Data...";
        }



        #endregion


    }
}


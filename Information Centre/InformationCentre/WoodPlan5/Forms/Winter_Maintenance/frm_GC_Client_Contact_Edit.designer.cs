namespace WoodPlan5
{
    partial class frm_GC_Client_Contact_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Client_Contact_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ClientNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.sp04011GCLinkedClientContactEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.PersonNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ClientContactPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WoodPlanWebFileManagerAdminCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WoodPlanWebWorkOrderDoneDateCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WoodPlanWebActionDoneDateCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WebSitePasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WebSiteUserNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IncludeForReportsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IncludeForGrittingEmailCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ClientContactIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ContactTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04010GCContactsTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientContactTextEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForClientContactID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientContactPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPersonName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactTypeIDGridLookUpEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientContact = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForWebSiteUserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForWoodPlanWebActionDoneDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWoodPlanWebWorkOrderDoneDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWoodPlanWebFileManagerAdmin = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWebSitePassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIncludeForGrittingEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIncludeForReports = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.sp04011_GC_Linked_Client_Contact_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04011_GC_Linked_Client_Contact_EditTableAdapter();
            this.sp04010_GC_Contacts_TypesTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04010_GC_Contacts_TypesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04011GCLinkedClientContactEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebFileManagerAdminCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebActionDoneDateCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSitePasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSiteUserNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeForReportsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeForGrittingEmailCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04010GCContactsTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactTypeIDGridLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSiteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebActionDoneDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebWorkOrderDoneDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebFileManagerAdmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSitePassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncludeForGrittingEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncludeForReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(571, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 576);
            this.barDockControlBottom.Size = new System.Drawing.Size(571, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 550);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(571, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 550);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colValue
            // 
            this.colValue.Caption = "Contact Type ID";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            this.colValue.Width = 101;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(571, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 576);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(571, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 550);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(571, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 550);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ClientNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.PersonNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContactPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WoodPlanWebFileManagerAdminCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WoodPlanWebWorkOrderDoneDateCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WoodPlanWebActionDoneDateCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WebSitePasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WebSiteUserNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.IncludeForReportsCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IncludeForGrittingEmailCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.ClientContactIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContactTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp04011GCLinkedClientContactEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientContactID,
            this.ItemForClientContactPersonID,
            this.ItemForClientID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1292, 310, 606, 544);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(571, 550);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ClientNameButtonEdit
            // 
            this.ClientNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "ClientName", true));
            this.ClientNameButtonEdit.EditValue = "";
            this.ClientNameButtonEdit.Location = new System.Drawing.Point(148, 30);
            this.ClientNameButtonEdit.MenuManager = this.barManager1;
            this.ClientNameButtonEdit.Name = "ClientNameButtonEdit";
            this.ClientNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientNameButtonEdit.Size = new System.Drawing.Size(397, 20);
            this.ClientNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameButtonEdit.TabIndex = 13;
            this.ClientNameButtonEdit.TabStop = false;
            this.ClientNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientNameButtonEdit_ButtonClick);
            this.ClientNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientNameButtonEdit_Validating);
            // 
            // sp04011GCLinkedClientContactEditBindingSource
            // 
            this.sp04011GCLinkedClientContactEditBindingSource.DataMember = "sp04011_GC_Linked_Client_Contact_Edit";
            this.sp04011GCLinkedClientContactEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PersonNameButtonEdit
            // 
            this.PersonNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "PersonName", true));
            this.PersonNameButtonEdit.EditValue = "";
            this.PersonNameButtonEdit.Location = new System.Drawing.Point(148, 54);
            this.PersonNameButtonEdit.MenuManager = this.barManager1;
            this.PersonNameButtonEdit.Name = "PersonNameButtonEdit";
            this.PersonNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to open the Select Client Contact Person screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to Clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.PersonNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.PersonNameButtonEdit.Size = new System.Drawing.Size(397, 20);
            this.PersonNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.PersonNameButtonEdit.TabIndex = 13;
            this.PersonNameButtonEdit.TabStop = false;
            this.PersonNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PersonNameButtonEdit_ButtonClick);
            this.PersonNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PersonNameButtonEdit_Validating);
            // 
            // ClientContactPersonIDTextEdit
            // 
            this.ClientContactPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "ClientContactPersonID", true));
            this.ClientContactPersonIDTextEdit.Location = new System.Drawing.Point(148, 231);
            this.ClientContactPersonIDTextEdit.MenuManager = this.barManager1;
            this.ClientContactPersonIDTextEdit.Name = "ClientContactPersonIDTextEdit";
            this.ClientContactPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContactPersonIDTextEdit, true);
            this.ClientContactPersonIDTextEdit.Size = new System.Drawing.Size(473, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContactPersonIDTextEdit, optionsSpelling1);
            this.ClientContactPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContactPersonIDTextEdit.TabIndex = 21;
            // 
            // WoodPlanWebFileManagerAdminCheckEdit
            // 
            this.WoodPlanWebFileManagerAdminCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "WoodPlanWebFileManagerAdmin", true));
            this.WoodPlanWebFileManagerAdminCheckEdit.Location = new System.Drawing.Point(184, 471);
            this.WoodPlanWebFileManagerAdminCheckEdit.MenuManager = this.barManager1;
            this.WoodPlanWebFileManagerAdminCheckEdit.Name = "WoodPlanWebFileManagerAdminCheckEdit";
            this.WoodPlanWebFileManagerAdminCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.WoodPlanWebFileManagerAdminCheckEdit.Properties.ValueChecked = 1;
            this.WoodPlanWebFileManagerAdminCheckEdit.Properties.ValueUnchecked = 0;
            this.WoodPlanWebFileManagerAdminCheckEdit.Size = new System.Drawing.Size(104, 19);
            this.WoodPlanWebFileManagerAdminCheckEdit.StyleController = this.dataLayoutControl1;
            this.WoodPlanWebFileManagerAdminCheckEdit.TabIndex = 20;
            // 
            // WoodPlanWebWorkOrderDoneDateCheckEdit
            // 
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "WoodPlanWebWorkOrderDoneDate", true));
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Location = new System.Drawing.Point(184, 448);
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.MenuManager = this.barManager1;
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Name = "WoodPlanWebWorkOrderDoneDateCheckEdit";
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties.ValueChecked = 1;
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties.ValueUnchecked = 0;
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Size = new System.Drawing.Size(104, 19);
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.StyleController = this.dataLayoutControl1;
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.TabIndex = 19;
            // 
            // WoodPlanWebActionDoneDateCheckEdit
            // 
            this.WoodPlanWebActionDoneDateCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "WoodPlanWebActionDoneDate", true));
            this.WoodPlanWebActionDoneDateCheckEdit.Location = new System.Drawing.Point(184, 425);
            this.WoodPlanWebActionDoneDateCheckEdit.MenuManager = this.barManager1;
            this.WoodPlanWebActionDoneDateCheckEdit.Name = "WoodPlanWebActionDoneDateCheckEdit";
            this.WoodPlanWebActionDoneDateCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.WoodPlanWebActionDoneDateCheckEdit.Properties.ValueChecked = 1;
            this.WoodPlanWebActionDoneDateCheckEdit.Properties.ValueUnchecked = 0;
            this.WoodPlanWebActionDoneDateCheckEdit.Size = new System.Drawing.Size(104, 19);
            this.WoodPlanWebActionDoneDateCheckEdit.StyleController = this.dataLayoutControl1;
            this.WoodPlanWebActionDoneDateCheckEdit.TabIndex = 18;
            // 
            // WebSitePasswordTextEdit
            // 
            this.WebSitePasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "WebSitePassword", true));
            this.WebSitePasswordTextEdit.Location = new System.Drawing.Point(172, 357);
            this.WebSitePasswordTextEdit.MenuManager = this.barManager1;
            this.WebSitePasswordTextEdit.Name = "WebSitePasswordTextEdit";
            this.WebSitePasswordTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WebSitePasswordTextEdit, true);
            this.WebSitePasswordTextEdit.Size = new System.Drawing.Size(271, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WebSitePasswordTextEdit, optionsSpelling2);
            this.WebSitePasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.WebSitePasswordTextEdit.TabIndex = 17;
            // 
            // WebSiteUserNameTextEdit
            // 
            this.WebSiteUserNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "WebSiteUserName", true));
            this.WebSiteUserNameTextEdit.Location = new System.Drawing.Point(172, 333);
            this.WebSiteUserNameTextEdit.MenuManager = this.barManager1;
            this.WebSiteUserNameTextEdit.Name = "WebSiteUserNameTextEdit";
            this.WebSiteUserNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WebSiteUserNameTextEdit, true);
            this.WebSiteUserNameTextEdit.Size = new System.Drawing.Size(271, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WebSiteUserNameTextEdit, optionsSpelling3);
            this.WebSiteUserNameTextEdit.StyleController = this.dataLayoutControl1;
            this.WebSiteUserNameTextEdit.TabIndex = 16;
            // 
            // IncludeForReportsCheckEdit
            // 
            this.IncludeForReportsCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "IncludeForReports", true));
            this.IncludeForReportsCheckEdit.Location = new System.Drawing.Point(148, 230);
            this.IncludeForReportsCheckEdit.MenuManager = this.barManager1;
            this.IncludeForReportsCheckEdit.Name = "IncludeForReportsCheckEdit";
            this.IncludeForReportsCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IncludeForReportsCheckEdit.Properties.ValueChecked = 1;
            this.IncludeForReportsCheckEdit.Properties.ValueUnchecked = 0;
            this.IncludeForReportsCheckEdit.Size = new System.Drawing.Size(97, 19);
            this.IncludeForReportsCheckEdit.StyleController = this.dataLayoutControl1;
            this.IncludeForReportsCheckEdit.TabIndex = 15;
            // 
            // IncludeForGrittingEmailCheckEdit
            // 
            this.IncludeForGrittingEmailCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "IncludeForGrittingEmail", true));
            this.IncludeForGrittingEmailCheckEdit.Location = new System.Drawing.Point(148, 207);
            this.IncludeForGrittingEmailCheckEdit.MenuManager = this.barManager1;
            this.IncludeForGrittingEmailCheckEdit.Name = "IncludeForGrittingEmailCheckEdit";
            this.IncludeForGrittingEmailCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IncludeForGrittingEmailCheckEdit.Properties.ValueChecked = 1;
            this.IncludeForGrittingEmailCheckEdit.Properties.ValueUnchecked = 0;
            this.IncludeForGrittingEmailCheckEdit.Size = new System.Drawing.Size(97, 19);
            this.IncludeForGrittingEmailCheckEdit.StyleController = this.dataLayoutControl1;
            this.IncludeForGrittingEmailCheckEdit.TabIndex = 14;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04011GCLinkedClientContactEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(148, 7);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(166, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 7;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ClientContactIDTextEdit
            // 
            this.ClientContactIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "ClientContactID", true));
            this.ClientContactIDTextEdit.Location = new System.Drawing.Point(109, 31);
            this.ClientContactIDTextEdit.MenuManager = this.barManager1;
            this.ClientContactIDTextEdit.Name = "ClientContactIDTextEdit";
            this.ClientContactIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContactIDTextEdit, true);
            this.ClientContactIDTextEdit.Size = new System.Drawing.Size(512, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContactIDTextEdit, optionsSpelling4);
            this.ClientContactIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContactIDTextEdit.TabIndex = 8;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(31, 333);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(509, 169);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling5);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // ContactTypeIDGridLookUpEdit
            // 
            this.ContactTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "ContactTypeID", true));
            this.ContactTypeIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ContactTypeIDGridLookUpEdit.Location = new System.Drawing.Point(148, 78);
            this.ContactTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContactTypeIDGridLookUpEdit.Name = "ContactTypeIDGridLookUpEdit";
            this.ContactTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContactTypeIDGridLookUpEdit.Properties.DataSource = this.sp04010GCContactsTypesBindingSource;
            this.ContactTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ContactTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ContactTypeIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.ContactTypeIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.ContactTypeIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.ContactTypeIDGridLookUpEdit.Properties.ValueMember = "Value";
            this.ContactTypeIDGridLookUpEdit.Size = new System.Drawing.Size(397, 20);
            this.ContactTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContactTypeIDGridLookUpEdit.TabIndex = 9;
            this.ContactTypeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ContactTypeIDGridLookUpEdit_EditValueChanged);
            this.ContactTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContactTypeIDGridLookUpEdit_Validating);
            // 
            // sp04010GCContactsTypesBindingSource
            // 
            this.sp04010GCContactsTypesBindingSource.DataMember = "sp04010_GC_Contacts_Types";
            this.sp04010GCContactsTypesBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colValue,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colValue;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "0";
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Contact Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 227;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // ClientContactTextEdit
            // 
            this.ClientContactTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "ContactDetails", true));
            this.ClientContactTextEdit.Location = new System.Drawing.Point(148, 102);
            this.ClientContactTextEdit.MenuManager = this.barManager1;
            this.ClientContactTextEdit.Name = "ClientContactTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContactTextEdit, true);
            this.ClientContactTextEdit.Size = new System.Drawing.Size(416, 101);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContactTextEdit, optionsSpelling6);
            this.ClientContactTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContactTextEdit.TabIndex = 10;
            this.ClientContactTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientContactTextEdit_Validating);
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04011GCLinkedClientContactEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.EditValue = "";
            this.ClientIDTextEdit.Location = new System.Drawing.Point(148, 30);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(456, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling7);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 13;
            // 
            // ItemForClientContactID
            // 
            this.ItemForClientContactID.Control = this.ClientContactIDTextEdit;
            this.ItemForClientContactID.CustomizationFormText = "Client Contact ID:";
            this.ItemForClientContactID.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientContactID.Name = "ItemForClientContactID";
            this.ItemForClientContactID.Size = new System.Drawing.Size(618, 24);
            this.ItemForClientContactID.Text = "Client Contact ID:";
            this.ItemForClientContactID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientContactPersonID
            // 
            this.ItemForClientContactPersonID.Control = this.ClientContactPersonIDTextEdit;
            this.ItemForClientContactPersonID.Location = new System.Drawing.Point(0, 175);
            this.ItemForClientContactPersonID.Name = "ItemForClientContactPersonID";
            this.ItemForClientContactPersonID.Size = new System.Drawing.Size(618, 24);
            this.ItemForClientContactPersonID.Text = "Client Contact Person ID:";
            this.ItemForClientContactPersonID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(601, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(138, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(571, 550);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.emptySpaceItem10,
            this.ItemForPersonName,
            this.ItemForContactTypeIDGridLookUpEdit,
            this.ItemForClientContact});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(561, 200);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(141, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(170, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(141, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(141, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(141, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(311, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(250, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ClientNameButtonEdit;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(542, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(542, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(542, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Client Name:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(542, 23);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(19, 72);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPersonName
            // 
            this.ItemForPersonName.Control = this.PersonNameButtonEdit;
            this.ItemForPersonName.Location = new System.Drawing.Point(0, 47);
            this.ItemForPersonName.Name = "ItemForPersonName";
            this.ItemForPersonName.Size = new System.Drawing.Size(542, 24);
            this.ItemForPersonName.Text = "Contact Person:";
            this.ItemForPersonName.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForContactTypeIDGridLookUpEdit
            // 
            this.ItemForContactTypeIDGridLookUpEdit.AllowHide = false;
            this.ItemForContactTypeIDGridLookUpEdit.Control = this.ContactTypeIDGridLookUpEdit;
            this.ItemForContactTypeIDGridLookUpEdit.CustomizationFormText = "Contact Type:";
            this.ItemForContactTypeIDGridLookUpEdit.Location = new System.Drawing.Point(0, 71);
            this.ItemForContactTypeIDGridLookUpEdit.Name = "ItemForContactTypeIDGridLookUpEdit";
            this.ItemForContactTypeIDGridLookUpEdit.Size = new System.Drawing.Size(542, 24);
            this.ItemForContactTypeIDGridLookUpEdit.Text = "Contact Type:";
            this.ItemForContactTypeIDGridLookUpEdit.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForClientContact
            // 
            this.ItemForClientContact.AllowHide = false;
            this.ItemForClientContact.Control = this.ClientContactTextEdit;
            this.ItemForClientContact.CustomizationFormText = "Contact:";
            this.ItemForClientContact.Location = new System.Drawing.Point(0, 95);
            this.ItemForClientContact.MaxSize = new System.Drawing.Size(0, 105);
            this.ItemForClientContact.MinSize = new System.Drawing.Size(116, 105);
            this.ItemForClientContact.Name = "ItemForClientContact";
            this.ItemForClientContact.Size = new System.Drawing.Size(561, 105);
            this.ItemForClientContact.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientContact.Text = "Contact Details:";
            this.ItemForClientContact.TextSize = new System.Drawing.Size(138, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "item0";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.ItemForIncludeForGrittingEmail,
            this.emptySpaceItem6,
            this.ItemForIncludeForReports});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 200);
            this.layoutControlGroup3.Name = "item0";
            this.layoutControlGroup3.Size = new System.Drawing.Size(561, 340);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 56);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(561, 267);
            this.layoutControlGroup5.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(537, 221);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Web";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWebSiteUserName,
            this.layoutControlGroup7,
            this.emptySpaceItem5,
            this.emptySpaceItem9,
            this.ItemForWebSitePassword});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(513, 173);
            this.layoutControlGroup6.Text = "Web";
            // 
            // ItemForWebSiteUserName
            // 
            this.ItemForWebSiteUserName.Control = this.WebSiteUserNameTextEdit;
            this.ItemForWebSiteUserName.CustomizationFormText = "Website User Name:";
            this.ItemForWebSiteUserName.Location = new System.Drawing.Point(0, 0);
            this.ItemForWebSiteUserName.MaxSize = new System.Drawing.Size(416, 24);
            this.ItemForWebSiteUserName.MinSize = new System.Drawing.Size(416, 24);
            this.ItemForWebSiteUserName.Name = "ItemForWebSiteUserName";
            this.ItemForWebSiteUserName.Size = new System.Drawing.Size(416, 24);
            this.ItemForWebSiteUserName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWebSiteUserName.Text = "Website User Name:";
            this.ItemForWebSiteUserName.TextSize = new System.Drawing.Size(138, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "WoodPlan Web Access";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWoodPlanWebActionDoneDate,
            this.emptySpaceItem7,
            this.ItemForWoodPlanWebWorkOrderDoneDate,
            this.ItemForWoodPlanWebFileManagerAdmin});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 58);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(513, 115);
            this.layoutControlGroup7.Text = "WoodPlan Web Access";
            // 
            // ItemForWoodPlanWebActionDoneDate
            // 
            this.ItemForWoodPlanWebActionDoneDate.Control = this.WoodPlanWebActionDoneDateCheckEdit;
            this.ItemForWoodPlanWebActionDoneDate.CustomizationFormText = "Set Actions Completed:";
            this.ItemForWoodPlanWebActionDoneDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForWoodPlanWebActionDoneDate.MaxSize = new System.Drawing.Size(249, 23);
            this.ItemForWoodPlanWebActionDoneDate.MinSize = new System.Drawing.Size(249, 23);
            this.ItemForWoodPlanWebActionDoneDate.Name = "ItemForWoodPlanWebActionDoneDate";
            this.ItemForWoodPlanWebActionDoneDate.Size = new System.Drawing.Size(249, 23);
            this.ItemForWoodPlanWebActionDoneDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWoodPlanWebActionDoneDate.Text = "Set Actions Completed:";
            this.ItemForWoodPlanWebActionDoneDate.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(249, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(240, 69);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWoodPlanWebWorkOrderDoneDate
            // 
            this.ItemForWoodPlanWebWorkOrderDoneDate.Control = this.WoodPlanWebWorkOrderDoneDateCheckEdit;
            this.ItemForWoodPlanWebWorkOrderDoneDate.CustomizationFormText = "Set Work Order Completed:";
            this.ItemForWoodPlanWebWorkOrderDoneDate.Location = new System.Drawing.Point(0, 23);
            this.ItemForWoodPlanWebWorkOrderDoneDate.Name = "ItemForWoodPlanWebWorkOrderDoneDate";
            this.ItemForWoodPlanWebWorkOrderDoneDate.Size = new System.Drawing.Size(249, 23);
            this.ItemForWoodPlanWebWorkOrderDoneDate.Text = "Set Work Orders Completed:";
            this.ItemForWoodPlanWebWorkOrderDoneDate.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForWoodPlanWebFileManagerAdmin
            // 
            this.ItemForWoodPlanWebFileManagerAdmin.Control = this.WoodPlanWebFileManagerAdminCheckEdit;
            this.ItemForWoodPlanWebFileManagerAdmin.CustomizationFormText = "File Manager Administrator:";
            this.ItemForWoodPlanWebFileManagerAdmin.Location = new System.Drawing.Point(0, 46);
            this.ItemForWoodPlanWebFileManagerAdmin.Name = "ItemForWoodPlanWebFileManagerAdmin";
            this.ItemForWoodPlanWebFileManagerAdmin.Size = new System.Drawing.Size(249, 23);
            this.ItemForWoodPlanWebFileManagerAdmin.Text = "File Manager Administrator:";
            this.ItemForWoodPlanWebFileManagerAdmin.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(513, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(416, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(97, 48);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWebSitePassword
            // 
            this.ItemForWebSitePassword.Control = this.WebSitePasswordTextEdit;
            this.ItemForWebSitePassword.CustomizationFormText = "Website Password:";
            this.ItemForWebSitePassword.Location = new System.Drawing.Point(0, 24);
            this.ItemForWebSitePassword.Name = "ItemForWebSitePassword";
            this.ItemForWebSitePassword.Size = new System.Drawing.Size(416, 24);
            this.ItemForWebSitePassword.Text = "Website Password:";
            this.ItemForWebSitePassword.TextSize = new System.Drawing.Size(138, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(513, 173);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(513, 173);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 46);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(561, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 323);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(561, 17);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIncludeForGrittingEmail
            // 
            this.ItemForIncludeForGrittingEmail.Control = this.IncludeForGrittingEmailCheckEdit;
            this.ItemForIncludeForGrittingEmail.CustomizationFormText = "Include For Gritting Emails:";
            this.ItemForIncludeForGrittingEmail.Location = new System.Drawing.Point(0, 0);
            this.ItemForIncludeForGrittingEmail.MaxSize = new System.Drawing.Size(242, 23);
            this.ItemForIncludeForGrittingEmail.MinSize = new System.Drawing.Size(242, 23);
            this.ItemForIncludeForGrittingEmail.Name = "ItemForIncludeForGrittingEmail";
            this.ItemForIncludeForGrittingEmail.Size = new System.Drawing.Size(242, 23);
            this.ItemForIncludeForGrittingEmail.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIncludeForGrittingEmail.Text = "Include For Gritting Emails:";
            this.ItemForIncludeForGrittingEmail.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(242, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(319, 46);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIncludeForReports
            // 
            this.ItemForIncludeForReports.Control = this.IncludeForReportsCheckEdit;
            this.ItemForIncludeForReports.CustomizationFormText = "Include for Sent Reports:";
            this.ItemForIncludeForReports.Location = new System.Drawing.Point(0, 23);
            this.ItemForIncludeForReports.Name = "ItemForIncludeForReports";
            this.ItemForIncludeForReports.Size = new System.Drawing.Size(242, 23);
            this.ItemForIncludeForReports.Text = "Include for Sent Reports:";
            this.ItemForIncludeForReports.TextSize = new System.Drawing.Size(138, 13);
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp04011_GC_Linked_Client_Contact_EditTableAdapter
            // 
            this.sp04011_GC_Linked_Client_Contact_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04010_GC_Contacts_TypesTableAdapter
            // 
            this.sp04010_GC_Contacts_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Client_Contact_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(571, 606);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Client_Contact_Edit";
            this.Text = "Edit Client Contact";
            this.Activated += new System.EventHandler(this.frm_GC_Client_Contact_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Client_Contact_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Client_Contact_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04011GCLinkedClientContactEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebFileManagerAdminCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebActionDoneDateCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSitePasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSiteUserNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeForReportsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeForGrittingEmailCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04010GCContactsTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactTypeIDGridLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSiteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebActionDoneDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebWorkOrderDoneDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebFileManagerAdmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSitePassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncludeForGrittingEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncludeForReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit ClientContactIDTextEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContactID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactTypeIDGridLookUpEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContact;
        private DevExpress.XtraEditors.GridLookUpEdit ContactTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.BindingSource sp04011GCLinkedClientContactEditBindingSource;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DataSet_GC_DataEntryTableAdapters.sp04011_GC_Linked_Client_Contact_EditTableAdapter sp04011_GC_Linked_Client_Contact_EditTableAdapter;
        private System.Windows.Forms.BindingSource sp04010GCContactsTypesBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04010_GC_Contacts_TypesTableAdapter sp04010_GC_Contacts_TypesTableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraEditors.CheckEdit IncludeForGrittingEmailCheckEdit;
        private DevExpress.XtraEditors.MemoEdit ClientContactTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncludeForGrittingEmail;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.CheckEdit IncludeForReportsCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncludeForReports;
        private DevExpress.XtraEditors.TextEdit WebSitePasswordTextEdit;
        private DevExpress.XtraEditors.TextEdit WebSiteUserNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWebSiteUserName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWebSitePassword;
        private DevExpress.XtraEditors.CheckEdit WoodPlanWebWorkOrderDoneDateCheckEdit;
        private DevExpress.XtraEditors.CheckEdit WoodPlanWebActionDoneDateCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWoodPlanWebActionDoneDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWoodPlanWebWorkOrderDoneDate;
        private DevExpress.XtraEditors.CheckEdit WoodPlanWebFileManagerAdminCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWoodPlanWebFileManagerAdmin;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.TextEdit ClientContactPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContactPersonID;
        private DevExpress.XtraEditors.ButtonEdit PersonNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonName;
        private DevExpress.XtraEditors.ButtonEdit ClientNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
    }
}

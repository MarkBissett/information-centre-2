namespace WoodPlan5
{
    partial class frm_GC_Client_Invoice_Group_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Client_Invoice_Group_Edit));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.AcceptsEmailInvoiceCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.sp04249GCClientInvoiceGroupEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.CareOfEmailMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.CareOfPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CareOfAddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CareOfAddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CareOfAddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CareOfAddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CareOfAddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IncludeSnowClearanceCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IncludeGrittingCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03009EPClientListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.InvoiceGroupIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForInvoiceGroupID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCareOfAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCareOfAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCareOfAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCareOfAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCareOfAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCareOfPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCareOfEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAcceptsEmailInvoice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIncludeGritting = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIncludeSnowClearance = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04249_GC_Client_Invoice_Group_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04249_GC_Client_Invoice_Group_EditTableAdapter();
            this.sp03009_EP_Client_List_With_BlankTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03009_EP_Client_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptsEmailInvoiceCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04249GCClientInvoiceGroupEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfEmailMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeSnowClearanceCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeGrittingCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03009EPClientListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceGroupIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceGroupID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptsEmailInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncludeGritting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncludeSnowClearance)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 553);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 527);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 527);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Width = 74;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 553);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 527);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 527);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.AcceptsEmailInvoiceCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.CareOfEmailMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.CareOfPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CareOfAddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.CareOfAddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.CareOfAddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.CareOfAddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.CareOfAddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.IncludeSnowClearanceCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IncludeGrittingCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.InvoiceGroupIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.DataSource = this.sp04249GCClientInvoiceGroupEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForInvoiceGroupID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(972, 343, 464, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 527);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // AcceptsEmailInvoiceCheckEdit
            // 
            this.AcceptsEmailInvoiceCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "AcceptsEmailInvoice", true));
            this.AcceptsEmailInvoiceCheckEdit.Location = new System.Drawing.Point(189, 439);
            this.AcceptsEmailInvoiceCheckEdit.MenuManager = this.barManager1;
            this.AcceptsEmailInvoiceCheckEdit.Name = "AcceptsEmailInvoiceCheckEdit";
            this.AcceptsEmailInvoiceCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.AcceptsEmailInvoiceCheckEdit.Properties.ValueChecked = 1;
            this.AcceptsEmailInvoiceCheckEdit.Properties.ValueUnchecked = 0;
            this.AcceptsEmailInvoiceCheckEdit.Size = new System.Drawing.Size(403, 19);
            this.AcceptsEmailInvoiceCheckEdit.StyleController = this.dataLayoutControl1;
            this.AcceptsEmailInvoiceCheckEdit.TabIndex = 36;
            // 
            // sp04249GCClientInvoiceGroupEditBindingSource
            // 
            this.sp04249GCClientInvoiceGroupEditBindingSource.DataMember = "sp04249_GC_Client_Invoice_Group_Edit";
            this.sp04249GCClientInvoiceGroupEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // CareOfEmailMemoEdit
            // 
            this.CareOfEmailMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "CareOfEmail", true));
            this.CareOfEmailMemoEdit.Location = new System.Drawing.Point(189, 378);
            this.CareOfEmailMemoEdit.MenuManager = this.barManager1;
            this.CareOfEmailMemoEdit.Name = "CareOfEmailMemoEdit";
            this.CareOfEmailMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CareOfEmailMemoEdit, true);
            this.CareOfEmailMemoEdit.Size = new System.Drawing.Size(403, 57);
            this.scSpellChecker.SetSpellCheckerOptions(this.CareOfEmailMemoEdit, optionsSpelling1);
            this.CareOfEmailMemoEdit.StyleController = this.dataLayoutControl1;
            this.CareOfEmailMemoEdit.TabIndex = 21;
            this.CareOfEmailMemoEdit.ToolTip = "Comma separate email addresses if more then one required.";
            this.CareOfEmailMemoEdit.Validating += new System.ComponentModel.CancelEventHandler(this.CareOfEmailMemoEdit_Validating);
            // 
            // CareOfPostcodeTextEdit
            // 
            this.CareOfPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "CareOfPostcode", true));
            this.CareOfPostcodeTextEdit.Location = new System.Drawing.Point(189, 354);
            this.CareOfPostcodeTextEdit.MenuManager = this.barManager1;
            this.CareOfPostcodeTextEdit.Name = "CareOfPostcodeTextEdit";
            this.CareOfPostcodeTextEdit.Properties.MaxLength = 290;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CareOfPostcodeTextEdit, true);
            this.CareOfPostcodeTextEdit.Size = new System.Drawing.Size(403, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CareOfPostcodeTextEdit, optionsSpelling2);
            this.CareOfPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.CareOfPostcodeTextEdit.TabIndex = 35;
            // 
            // CareOfAddressLine5TextEdit
            // 
            this.CareOfAddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "CareOfAddressLine5", true));
            this.CareOfAddressLine5TextEdit.Location = new System.Drawing.Point(189, 330);
            this.CareOfAddressLine5TextEdit.MenuManager = this.barManager1;
            this.CareOfAddressLine5TextEdit.Name = "CareOfAddressLine5TextEdit";
            this.CareOfAddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CareOfAddressLine5TextEdit, true);
            this.CareOfAddressLine5TextEdit.Size = new System.Drawing.Size(403, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CareOfAddressLine5TextEdit, optionsSpelling3);
            this.CareOfAddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.CareOfAddressLine5TextEdit.TabIndex = 34;
            // 
            // CareOfAddressLine4TextEdit
            // 
            this.CareOfAddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "CareOfAddressLine4", true));
            this.CareOfAddressLine4TextEdit.Location = new System.Drawing.Point(189, 306);
            this.CareOfAddressLine4TextEdit.MenuManager = this.barManager1;
            this.CareOfAddressLine4TextEdit.Name = "CareOfAddressLine4TextEdit";
            this.CareOfAddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CareOfAddressLine4TextEdit, true);
            this.CareOfAddressLine4TextEdit.Size = new System.Drawing.Size(403, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CareOfAddressLine4TextEdit, optionsSpelling4);
            this.CareOfAddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.CareOfAddressLine4TextEdit.TabIndex = 34;
            // 
            // CareOfAddressLine3TextEdit
            // 
            this.CareOfAddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "CareOfAddressLine3", true));
            this.CareOfAddressLine3TextEdit.Location = new System.Drawing.Point(189, 282);
            this.CareOfAddressLine3TextEdit.MenuManager = this.barManager1;
            this.CareOfAddressLine3TextEdit.Name = "CareOfAddressLine3TextEdit";
            this.CareOfAddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CareOfAddressLine3TextEdit, true);
            this.CareOfAddressLine3TextEdit.Size = new System.Drawing.Size(403, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CareOfAddressLine3TextEdit, optionsSpelling5);
            this.CareOfAddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.CareOfAddressLine3TextEdit.TabIndex = 34;
            // 
            // CareOfAddressLine2TextEdit
            // 
            this.CareOfAddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "CareOfAddressLine2", true));
            this.CareOfAddressLine2TextEdit.Location = new System.Drawing.Point(189, 258);
            this.CareOfAddressLine2TextEdit.MenuManager = this.barManager1;
            this.CareOfAddressLine2TextEdit.Name = "CareOfAddressLine2TextEdit";
            this.CareOfAddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CareOfAddressLine2TextEdit, true);
            this.CareOfAddressLine2TextEdit.Size = new System.Drawing.Size(403, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CareOfAddressLine2TextEdit, optionsSpelling6);
            this.CareOfAddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.CareOfAddressLine2TextEdit.TabIndex = 33;
            // 
            // CareOfAddressLine1TextEdit
            // 
            this.CareOfAddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "CareOfAddressLine1", true));
            this.CareOfAddressLine1TextEdit.Location = new System.Drawing.Point(189, 234);
            this.CareOfAddressLine1TextEdit.MenuManager = this.barManager1;
            this.CareOfAddressLine1TextEdit.Name = "CareOfAddressLine1TextEdit";
            this.CareOfAddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CareOfAddressLine1TextEdit, true);
            this.CareOfAddressLine1TextEdit.Size = new System.Drawing.Size(403, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CareOfAddressLine1TextEdit, optionsSpelling7);
            this.CareOfAddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.CareOfAddressLine1TextEdit.TabIndex = 32;
            // 
            // IncludeSnowClearanceCheckEdit
            // 
            this.IncludeSnowClearanceCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "IncludeSnowClearance", true));
            this.IncludeSnowClearanceCheckEdit.Location = new System.Drawing.Point(165, 131);
            this.IncludeSnowClearanceCheckEdit.MenuManager = this.barManager1;
            this.IncludeSnowClearanceCheckEdit.Name = "IncludeSnowClearanceCheckEdit";
            this.IncludeSnowClearanceCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IncludeSnowClearanceCheckEdit.Properties.ValueChecked = 1;
            this.IncludeSnowClearanceCheckEdit.Properties.ValueUnchecked = 0;
            this.IncludeSnowClearanceCheckEdit.Size = new System.Drawing.Size(99, 19);
            this.IncludeSnowClearanceCheckEdit.StyleController = this.dataLayoutControl1;
            this.IncludeSnowClearanceCheckEdit.TabIndex = 31;
            // 
            // IncludeGrittingCheckEdit
            // 
            this.IncludeGrittingCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "IncludeGritting", true));
            this.IncludeGrittingCheckEdit.Location = new System.Drawing.Point(165, 108);
            this.IncludeGrittingCheckEdit.MenuManager = this.barManager1;
            this.IncludeGrittingCheckEdit.Name = "IncludeGrittingCheckEdit";
            this.IncludeGrittingCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IncludeGrittingCheckEdit.Properties.ValueChecked = 1;
            this.IncludeGrittingCheckEdit.Properties.ValueUnchecked = 0;
            this.IncludeGrittingCheckEdit.Size = new System.Drawing.Size(99, 19);
            this.IncludeGrittingCheckEdit.StyleController = this.dataLayoutControl1;
            this.IncludeGrittingCheckEdit.TabIndex = 30;
            // 
            // ActiveCheckEdit
            // 
            this.ActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "Active", true));
            this.ActiveCheckEdit.Location = new System.Drawing.Point(165, 85);
            this.ActiveCheckEdit.MenuManager = this.barManager1;
            this.ActiveCheckEdit.Name = "ActiveCheckEdit";
            this.ActiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ActiveCheckEdit.Properties.ValueChecked = 1;
            this.ActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveCheckEdit.Size = new System.Drawing.Size(99, 19);
            this.ActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveCheckEdit.TabIndex = 29;
            // 
            // ClientIDGridLookUpEdit
            // 
            this.ClientIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "ClientID", true));
            this.ClientIDGridLookUpEdit.Location = new System.Drawing.Point(165, 35);
            this.ClientIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ClientIDGridLookUpEdit.Name = "ClientIDGridLookUpEdit";
            this.ClientIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Edit Underlying Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Reload Underlying Data", "reload", null, true)});
            this.ClientIDGridLookUpEdit.Properties.DataSource = this.sp03009EPClientListWithBlankBindingSource;
            this.ClientIDGridLookUpEdit.Properties.DisplayMember = "ClientName";
            this.ClientIDGridLookUpEdit.Properties.NullText = "";
            this.ClientIDGridLookUpEdit.Properties.ValueMember = "ClientID";
            this.ClientIDGridLookUpEdit.Properties.View = this.gridView1;
            this.ClientIDGridLookUpEdit.Size = new System.Drawing.Size(451, 22);
            this.ClientIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDGridLookUpEdit.TabIndex = 28;
            this.ClientIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientIDGridLookUpEdit_ButtonClick);
            this.ClientIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientIDGridLookUpEdit_Validating);
            // 
            // sp03009EPClientListWithBlankBindingSource
            // 
            this.sp03009EPClientListWithBlankBindingSource.DataMember = "sp03009_EP_Client_List_With_Blank";
            this.sp03009EPClientListWithBlankBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientCode,
            this.colClientID,
            this.colClientName,
            this.colClientTypeDescription,
            this.colClientTypeID,
            this.colRemarks});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colClientID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Visible = true;
            this.colClientCode.VisibleIndex = 1;
            this.colClientCode.Width = 108;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 271;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 2;
            this.colClientTypeDescription.Width = 132;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            this.colClientTypeID.Width = 101;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            this.colRemarks.Width = 67;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04249GCClientInvoiceGroupEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(166, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(177, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 9;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // InvoiceGroupIDTextEdit
            // 
            this.InvoiceGroupIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "InvoiceGroupID", true));
            this.InvoiceGroupIDTextEdit.Location = new System.Drawing.Point(101, 36);
            this.InvoiceGroupIDTextEdit.MenuManager = this.barManager1;
            this.InvoiceGroupIDTextEdit.Name = "InvoiceGroupIDTextEdit";
            this.InvoiceGroupIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.InvoiceGroupIDTextEdit, true);
            this.InvoiceGroupIDTextEdit.Size = new System.Drawing.Size(515, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.InvoiceGroupIDTextEdit, optionsSpelling8);
            this.InvoiceGroupIDTextEdit.StyleController = this.dataLayoutControl1;
            this.InvoiceGroupIDTextEdit.TabIndex = 4;
            // 
            // DescriptionTextEdit
            // 
            this.DescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "Description", true));
            this.DescriptionTextEdit.Location = new System.Drawing.Point(165, 61);
            this.DescriptionTextEdit.MenuManager = this.barManager1;
            this.DescriptionTextEdit.Name = "DescriptionTextEdit";
            this.DescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DescriptionTextEdit, true);
            this.DescriptionTextEdit.Size = new System.Drawing.Size(451, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DescriptionTextEdit, optionsSpelling9);
            this.DescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.DescriptionTextEdit.TabIndex = 6;
            this.DescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DescriptionTextEdit_Validating);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04249GCClientInvoiceGroupEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 234);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(556, 257);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling10);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 8;
            // 
            // ItemForInvoiceGroupID
            // 
            this.ItemForInvoiceGroupID.Control = this.InvoiceGroupIDTextEdit;
            this.ItemForInvoiceGroupID.CustomizationFormText = "Invoice Group ID:";
            this.ItemForInvoiceGroupID.Location = new System.Drawing.Point(0, 24);
            this.ItemForInvoiceGroupID.Name = "ItemForInvoiceGroupID";
            this.ItemForInvoiceGroupID.Size = new System.Drawing.Size(608, 24);
            this.ItemForInvoiceGroupID.Text = "Invoice Group ID:";
            this.ItemForInvoiceGroupID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 527);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.ItemForDescription,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.ItemForClientID,
            this.ItemForActive,
            this.emptySpaceItem5,
            this.ItemForIncludeGritting,
            this.ItemForIncludeSnowClearance});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 507);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 152);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(608, 355);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 309);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImage = global::WoodPlan5.Properties.Resources.Home_16x16;
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCareOfAddressLine1,
            this.emptySpaceItem2,
            this.ItemForCareOfAddressLine2,
            this.ItemForCareOfAddressLine3,
            this.ItemForCareOfAddressLine4,
            this.ItemForCareOfAddressLine5,
            this.ItemForCareOfPostcode,
            this.ItemForCareOfEmail,
            this.ItemForAcceptsEmailInvoice});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(560, 261);
            this.layoutControlGroup5.Text = "Care Of Address";
            // 
            // ItemForCareOfAddressLine1
            // 
            this.ItemForCareOfAddressLine1.Control = this.CareOfAddressLine1TextEdit;
            this.ItemForCareOfAddressLine1.Location = new System.Drawing.Point(0, 0);
            this.ItemForCareOfAddressLine1.Name = "ItemForCareOfAddressLine1";
            this.ItemForCareOfAddressLine1.Size = new System.Drawing.Size(560, 24);
            this.ItemForCareOfAddressLine1.Text = "Address Line 1:";
            this.ItemForCareOfAddressLine1.TextSize = new System.Drawing.Size(150, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 228);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(560, 33);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCareOfAddressLine2
            // 
            this.ItemForCareOfAddressLine2.Control = this.CareOfAddressLine2TextEdit;
            this.ItemForCareOfAddressLine2.Location = new System.Drawing.Point(0, 24);
            this.ItemForCareOfAddressLine2.Name = "ItemForCareOfAddressLine2";
            this.ItemForCareOfAddressLine2.Size = new System.Drawing.Size(560, 24);
            this.ItemForCareOfAddressLine2.Text = "Address Line 2:";
            this.ItemForCareOfAddressLine2.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForCareOfAddressLine3
            // 
            this.ItemForCareOfAddressLine3.Control = this.CareOfAddressLine3TextEdit;
            this.ItemForCareOfAddressLine3.Location = new System.Drawing.Point(0, 48);
            this.ItemForCareOfAddressLine3.Name = "ItemForCareOfAddressLine3";
            this.ItemForCareOfAddressLine3.Size = new System.Drawing.Size(560, 24);
            this.ItemForCareOfAddressLine3.Text = "Address Line 3:";
            this.ItemForCareOfAddressLine3.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForCareOfAddressLine4
            // 
            this.ItemForCareOfAddressLine4.Control = this.CareOfAddressLine4TextEdit;
            this.ItemForCareOfAddressLine4.Location = new System.Drawing.Point(0, 72);
            this.ItemForCareOfAddressLine4.Name = "ItemForCareOfAddressLine4";
            this.ItemForCareOfAddressLine4.Size = new System.Drawing.Size(560, 24);
            this.ItemForCareOfAddressLine4.Text = "Address Line 4:";
            this.ItemForCareOfAddressLine4.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForCareOfAddressLine5
            // 
            this.ItemForCareOfAddressLine5.Control = this.CareOfAddressLine5TextEdit;
            this.ItemForCareOfAddressLine5.Location = new System.Drawing.Point(0, 96);
            this.ItemForCareOfAddressLine5.Name = "ItemForCareOfAddressLine5";
            this.ItemForCareOfAddressLine5.Size = new System.Drawing.Size(560, 24);
            this.ItemForCareOfAddressLine5.Text = "Address Line 5:";
            this.ItemForCareOfAddressLine5.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForCareOfPostcode
            // 
            this.ItemForCareOfPostcode.Control = this.CareOfPostcodeTextEdit;
            this.ItemForCareOfPostcode.Location = new System.Drawing.Point(0, 120);
            this.ItemForCareOfPostcode.Name = "ItemForCareOfPostcode";
            this.ItemForCareOfPostcode.Size = new System.Drawing.Size(560, 24);
            this.ItemForCareOfPostcode.Text = "Postcode:";
            this.ItemForCareOfPostcode.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForCareOfEmail
            // 
            this.ItemForCareOfEmail.Control = this.CareOfEmailMemoEdit;
            this.ItemForCareOfEmail.Location = new System.Drawing.Point(0, 144);
            this.ItemForCareOfEmail.MaxSize = new System.Drawing.Size(0, 61);
            this.ItemForCareOfEmail.MinSize = new System.Drawing.Size(167, 61);
            this.ItemForCareOfEmail.Name = "ItemForCareOfEmail";
            this.ItemForCareOfEmail.Size = new System.Drawing.Size(560, 61);
            this.ItemForCareOfEmail.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCareOfEmail.Text = "Email Address:";
            this.ItemForCareOfEmail.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForAcceptsEmailInvoice
            // 
            this.ItemForAcceptsEmailInvoice.Control = this.AcceptsEmailInvoiceCheckEdit;
            this.ItemForAcceptsEmailInvoice.Location = new System.Drawing.Point(0, 205);
            this.ItemForAcceptsEmailInvoice.Name = "ItemForAcceptsEmailInvoice";
            this.ItemForAcceptsEmailInvoice.Size = new System.Drawing.Size(560, 23);
            this.ItemForAcceptsEmailInvoice.Text = "Accepts Email Invoice:";
            this.ItemForAcceptsEmailInvoice.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(560, 261);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(560, 261);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.AllowHide = false;
            this.ItemForDescription.Control = this.DescriptionTextEdit;
            this.ItemForDescription.CustomizationFormText = "Group Description:";
            this.ItemForDescription.Location = new System.Drawing.Point(0, 49);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(608, 24);
            this.ItemForDescription.Text = "Group Description:";
            this.ItemForDescription.TextSize = new System.Drawing.Size(150, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 142);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(154, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(181, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(154, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(154, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(154, 23);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(335, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(273, 23);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDGridLookUpEdit;
            this.ItemForClientID.CustomizationFormText = "Client:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(608, 26);
            this.ItemForClientID.Text = "Client:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForActive
            // 
            this.ItemForActive.Control = this.ActiveCheckEdit;
            this.ItemForActive.CustomizationFormText = "Active:";
            this.ItemForActive.Location = new System.Drawing.Point(0, 73);
            this.ItemForActive.MaxSize = new System.Drawing.Size(256, 23);
            this.ItemForActive.MinSize = new System.Drawing.Size(256, 23);
            this.ItemForActive.Name = "ItemForActive";
            this.ItemForActive.Size = new System.Drawing.Size(256, 23);
            this.ItemForActive.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActive.Text = "Active:";
            this.ItemForActive.TextSize = new System.Drawing.Size(150, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(256, 73);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(352, 69);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIncludeGritting
            // 
            this.ItemForIncludeGritting.Control = this.IncludeGrittingCheckEdit;
            this.ItemForIncludeGritting.CustomizationFormText = "Gritting Jobs Included:";
            this.ItemForIncludeGritting.Location = new System.Drawing.Point(0, 96);
            this.ItemForIncludeGritting.MaxSize = new System.Drawing.Size(256, 23);
            this.ItemForIncludeGritting.MinSize = new System.Drawing.Size(256, 23);
            this.ItemForIncludeGritting.Name = "ItemForIncludeGritting";
            this.ItemForIncludeGritting.Size = new System.Drawing.Size(256, 23);
            this.ItemForIncludeGritting.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIncludeGritting.Text = "Gritting Jobs Included:";
            this.ItemForIncludeGritting.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForIncludeSnowClearance
            // 
            this.ItemForIncludeSnowClearance.Control = this.IncludeSnowClearanceCheckEdit;
            this.ItemForIncludeSnowClearance.CustomizationFormText = "Snow Clearance Jobs Included:";
            this.ItemForIncludeSnowClearance.Location = new System.Drawing.Point(0, 119);
            this.ItemForIncludeSnowClearance.MaxSize = new System.Drawing.Size(256, 23);
            this.ItemForIncludeSnowClearance.MinSize = new System.Drawing.Size(256, 23);
            this.ItemForIncludeSnowClearance.Name = "ItemForIncludeSnowClearance";
            this.ItemForIncludeSnowClearance.Size = new System.Drawing.Size(256, 23);
            this.ItemForIncludeSnowClearance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIncludeSnowClearance.Text = "Snow Clearance Jobs Included:";
            this.ItemForIncludeSnowClearance.TextSize = new System.Drawing.Size(150, 13);
            // 
            // sp04249_GC_Client_Invoice_Group_EditTableAdapter
            // 
            this.sp04249_GC_Client_Invoice_Group_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp03009_EP_Client_List_With_BlankTableAdapter
            // 
            this.sp03009_EP_Client_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Client_Invoice_Group_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 583);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Client_Invoice_Group_Edit";
            this.Text = "Edit Client Invoice Group";
            this.Activated += new System.EventHandler(this.frm_GC_Client_Invoice_Group_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Client_Invoice_Group_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Client_Invoice_Group_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AcceptsEmailInvoiceCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04249GCClientInvoiceGroupEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfEmailMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CareOfAddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeSnowClearanceCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludeGrittingCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03009EPClientListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceGroupIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceGroupID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCareOfEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptsEmailInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncludeGritting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncludeSnowClearance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit InvoiceGroupIDTextEdit;
        private DevExpress.XtraEditors.TextEdit DescriptionTextEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvoiceGroupID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private System.Windows.Forms.BindingSource sp04249GCClientInvoiceGroupEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04249_GC_Client_Invoice_Group_EditTableAdapter sp04249_GC_Client_Invoice_Group_EditTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit ClientIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private System.Windows.Forms.BindingSource sp03009EPClientListWithBlankBindingSource;
        private DataSet_EP_DataEntryTableAdapters.sp03009_EP_Client_List_With_BlankTableAdapter sp03009_EP_Client_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.CheckEdit IncludeSnowClearanceCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IncludeGrittingCheckEdit;
        private DevExpress.XtraEditors.CheckEdit ActiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncludeGritting;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncludeSnowClearance;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.TextEdit CareOfAddressLine1TextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCareOfAddressLine1;
        private DevExpress.XtraEditors.TextEdit CareOfPostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit CareOfAddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit CareOfAddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit CareOfAddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit CareOfAddressLine2TextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCareOfAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCareOfAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCareOfAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCareOfAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCareOfPostcode;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.MemoEdit CareOfEmailMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCareOfEmail;
        private DevExpress.XtraEditors.CheckEdit AcceptsEmailInvoiceCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAcceptsEmailInvoice;
    }
}

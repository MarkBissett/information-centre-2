namespace WoodPlan5
{
    partial class frm_GC_Select_Salt_Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Select_Salt_Order));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04222GCSaltOrderSelectListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGritOrderHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colGritSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlacedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedDeliveryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountConverted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25KGBags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOtherCostVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOtherCostExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOtherCostIncVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCostExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCostExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCostIncVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCostIncVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGCPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSupplierName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderLineCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp04222_GC_Salt_Order_Select_ListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04222_GC_Salt_Order_Select_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04222GCSaltOrderSelectListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KGBags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 432);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 406);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 406);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.MaxItemId = 31;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp04222GCSaltOrderSelectListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEdit25KGBags,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditDate,
            this.repositoryItemTextEditPercentage});
            this.gridControl1.Size = new System.Drawing.Size(628, 329);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04222GCSaltOrderSelectListBindingSource
            // 
            this.sp04222GCSaltOrderSelectListBindingSource.DataMember = "sp04222_GC_Salt_Order_Select_List";
            this.sp04222GCSaltOrderSelectListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGritOrderHeaderID,
            this.colOrderDate,
            this.colGritSupplierID,
            this.colPlacedByStaffID,
            this.colEstimatedDeliveryDate,
            this.colTotalAmountConverted,
            this.colOtherCostVatRate,
            this.colOtherCostExVat,
            this.colOtherCostIncVat,
            this.colTotalCostExVat,
            this.colUnitCostExVat,
            this.colTotalCostIncVat,
            this.colUnitCostIncVat,
            this.colRemarks,
            this.colGCPONumber,
            this.colOrderStatusID,
            this.colGritSupplierName,
            this.colOrderedByStaffName,
            this.colOrderLineCount,
            this.colOrderStatusDescription});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colGritOrderHeaderID
            // 
            this.colGritOrderHeaderID.Caption = "Order Header ID";
            this.colGritOrderHeaderID.FieldName = "GritOrderHeaderID";
            this.colGritOrderHeaderID.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colGritOrderHeaderID.Name = "colGritOrderHeaderID";
            this.colGritOrderHeaderID.OptionsColumn.AllowEdit = false;
            this.colGritOrderHeaderID.OptionsColumn.AllowFocus = false;
            this.colGritOrderHeaderID.OptionsColumn.ReadOnly = true;
            this.colGritOrderHeaderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colGritOrderHeaderID.Width = 101;
            // 
            // colOrderDate
            // 
            this.colOrderDate.Caption = "Order Date";
            this.colOrderDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colOrderDate.FieldName = "OrderDate";
            this.colOrderDate.Name = "colOrderDate";
            this.colOrderDate.OptionsColumn.AllowEdit = false;
            this.colOrderDate.OptionsColumn.AllowFocus = false;
            this.colOrderDate.OptionsColumn.ReadOnly = true;
            this.colOrderDate.Visible = true;
            this.colOrderDate.VisibleIndex = 1;
            this.colOrderDate.Width = 112;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "dd/MM/yyyy HH:ss";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colGritSupplierID
            // 
            this.colGritSupplierID.Caption = "Supplier ID";
            this.colGritSupplierID.FieldName = "GritSupplierID";
            this.colGritSupplierID.Name = "colGritSupplierID";
            this.colGritSupplierID.OptionsColumn.AllowEdit = false;
            this.colGritSupplierID.OptionsColumn.AllowFocus = false;
            this.colGritSupplierID.OptionsColumn.ReadOnly = true;
            // 
            // colPlacedByStaffID
            // 
            this.colPlacedByStaffID.Caption = "Placed By Staff ID";
            this.colPlacedByStaffID.FieldName = "PlacedByStaffID";
            this.colPlacedByStaffID.Name = "colPlacedByStaffID";
            this.colPlacedByStaffID.OptionsColumn.AllowEdit = false;
            this.colPlacedByStaffID.OptionsColumn.AllowFocus = false;
            this.colPlacedByStaffID.OptionsColumn.ReadOnly = true;
            this.colPlacedByStaffID.Width = 108;
            // 
            // colEstimatedDeliveryDate
            // 
            this.colEstimatedDeliveryDate.Caption = "Estimated Delivery Date";
            this.colEstimatedDeliveryDate.FieldName = "EstimatedDeliveryDate";
            this.colEstimatedDeliveryDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEstimatedDeliveryDate.Name = "colEstimatedDeliveryDate";
            this.colEstimatedDeliveryDate.OptionsColumn.AllowEdit = false;
            this.colEstimatedDeliveryDate.OptionsColumn.AllowFocus = false;
            this.colEstimatedDeliveryDate.OptionsColumn.ReadOnly = true;
            this.colEstimatedDeliveryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEstimatedDeliveryDate.Visible = true;
            this.colEstimatedDeliveryDate.VisibleIndex = 4;
            this.colEstimatedDeliveryDate.Width = 136;
            // 
            // colTotalAmountConverted
            // 
            this.colTotalAmountConverted.Caption = "Amount Converted";
            this.colTotalAmountConverted.ColumnEdit = this.repositoryItemTextEdit25KGBags;
            this.colTotalAmountConverted.FieldName = "TotalAmountConverted";
            this.colTotalAmountConverted.Name = "colTotalAmountConverted";
            this.colTotalAmountConverted.OptionsColumn.AllowEdit = false;
            this.colTotalAmountConverted.OptionsColumn.AllowFocus = false;
            this.colTotalAmountConverted.OptionsColumn.ReadOnly = true;
            this.colTotalAmountConverted.Visible = true;
            this.colTotalAmountConverted.VisibleIndex = 5;
            this.colTotalAmountConverted.Width = 112;
            // 
            // repositoryItemTextEdit25KGBags
            // 
            this.repositoryItemTextEdit25KGBags.AutoHeight = false;
            this.repositoryItemTextEdit25KGBags.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25KGBags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KGBags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KGBags.Name = "repositoryItemTextEdit25KGBags";
            // 
            // colOtherCostVatRate
            // 
            this.colOtherCostVatRate.Caption = "Other Cost VAT Rate";
            this.colOtherCostVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colOtherCostVatRate.FieldName = "OtherCostVatRate";
            this.colOtherCostVatRate.Name = "colOtherCostVatRate";
            this.colOtherCostVatRate.OptionsColumn.AllowEdit = false;
            this.colOtherCostVatRate.OptionsColumn.AllowFocus = false;
            this.colOtherCostVatRate.OptionsColumn.ReadOnly = true;
            this.colOtherCostVatRate.Visible = true;
            this.colOtherCostVatRate.VisibleIndex = 6;
            this.colOtherCostVatRate.Width = 122;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colOtherCostExVat
            // 
            this.colOtherCostExVat.Caption = "Other Cost Ex VAT";
            this.colOtherCostExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherCostExVat.FieldName = "OtherCostExVat";
            this.colOtherCostExVat.Name = "colOtherCostExVat";
            this.colOtherCostExVat.OptionsColumn.AllowEdit = false;
            this.colOtherCostExVat.OptionsColumn.AllowFocus = false;
            this.colOtherCostExVat.OptionsColumn.ReadOnly = true;
            this.colOtherCostExVat.Visible = true;
            this.colOtherCostExVat.VisibleIndex = 7;
            this.colOtherCostExVat.Width = 111;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colOtherCostIncVat
            // 
            this.colOtherCostIncVat.Caption = "Other Cost inc VAT";
            this.colOtherCostIncVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherCostIncVat.FieldName = "OtherCostIncVat";
            this.colOtherCostIncVat.Name = "colOtherCostIncVat";
            this.colOtherCostIncVat.OptionsColumn.AllowEdit = false;
            this.colOtherCostIncVat.OptionsColumn.AllowFocus = false;
            this.colOtherCostIncVat.OptionsColumn.ReadOnly = true;
            this.colOtherCostIncVat.Visible = true;
            this.colOtherCostIncVat.VisibleIndex = 8;
            this.colOtherCostIncVat.Width = 112;
            // 
            // colTotalCostExVat
            // 
            this.colTotalCostExVat.Caption = "Total Cost Ex VAT";
            this.colTotalCostExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCostExVat.FieldName = "TotalCostExVat";
            this.colTotalCostExVat.Name = "colTotalCostExVat";
            this.colTotalCostExVat.OptionsColumn.AllowEdit = false;
            this.colTotalCostExVat.OptionsColumn.AllowFocus = false;
            this.colTotalCostExVat.OptionsColumn.ReadOnly = true;
            this.colTotalCostExVat.Visible = true;
            this.colTotalCostExVat.VisibleIndex = 9;
            this.colTotalCostExVat.Width = 107;
            // 
            // colUnitCostExVat
            // 
            this.colUnitCostExVat.Caption = "Unit Cost Ex VAT";
            this.colUnitCostExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colUnitCostExVat.FieldName = "UnitCostExVat";
            this.colUnitCostExVat.Name = "colUnitCostExVat";
            this.colUnitCostExVat.OptionsColumn.AllowEdit = false;
            this.colUnitCostExVat.OptionsColumn.AllowFocus = false;
            this.colUnitCostExVat.OptionsColumn.ReadOnly = true;
            this.colUnitCostExVat.Width = 102;
            // 
            // colTotalCostIncVat
            // 
            this.colTotalCostIncVat.Caption = "Total Cost Inc VAT";
            this.colTotalCostIncVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCostIncVat.FieldName = "TotalCostIncVat";
            this.colTotalCostIncVat.Name = "colTotalCostIncVat";
            this.colTotalCostIncVat.OptionsColumn.AllowEdit = false;
            this.colTotalCostIncVat.OptionsColumn.AllowFocus = false;
            this.colTotalCostIncVat.OptionsColumn.ReadOnly = true;
            this.colTotalCostIncVat.Visible = true;
            this.colTotalCostIncVat.VisibleIndex = 10;
            this.colTotalCostIncVat.Width = 110;
            // 
            // colUnitCostIncVat
            // 
            this.colUnitCostIncVat.Caption = "Unit Cost Inc VAT";
            this.colUnitCostIncVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colUnitCostIncVat.FieldName = "UnitCostIncVat";
            this.colUnitCostIncVat.Name = "colUnitCostIncVat";
            this.colUnitCostIncVat.OptionsColumn.AllowEdit = false;
            this.colUnitCostIncVat.OptionsColumn.AllowFocus = false;
            this.colUnitCostIncVat.OptionsColumn.ReadOnly = true;
            this.colUnitCostIncVat.Width = 105;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGCPONumber
            // 
            this.colGCPONumber.Caption = "GC PO Number";
            this.colGCPONumber.FieldName = "GCPONumber";
            this.colGCPONumber.Name = "colGCPONumber";
            this.colGCPONumber.OptionsColumn.AllowEdit = false;
            this.colGCPONumber.OptionsColumn.AllowFocus = false;
            this.colGCPONumber.OptionsColumn.ReadOnly = true;
            this.colGCPONumber.Visible = true;
            this.colGCPONumber.VisibleIndex = 2;
            this.colGCPONumber.Width = 92;
            // 
            // colOrderStatusID
            // 
            this.colOrderStatusID.Caption = "Order Status ID";
            this.colOrderStatusID.FieldName = "OrderStatusID";
            this.colOrderStatusID.Name = "colOrderStatusID";
            this.colOrderStatusID.OptionsColumn.AllowEdit = false;
            this.colOrderStatusID.OptionsColumn.AllowFocus = false;
            this.colOrderStatusID.OptionsColumn.ReadOnly = true;
            this.colOrderStatusID.Width = 97;
            // 
            // colGritSupplierName
            // 
            this.colGritSupplierName.Caption = "Supplier Name";
            this.colGritSupplierName.FieldName = "GritSupplierName";
            this.colGritSupplierName.Name = "colGritSupplierName";
            this.colGritSupplierName.OptionsColumn.AllowEdit = false;
            this.colGritSupplierName.OptionsColumn.AllowFocus = false;
            this.colGritSupplierName.OptionsColumn.ReadOnly = true;
            this.colGritSupplierName.Visible = true;
            this.colGritSupplierName.VisibleIndex = 3;
            this.colGritSupplierName.Width = 181;
            // 
            // colOrderedByStaffName
            // 
            this.colOrderedByStaffName.Caption = "Ordered By Staff";
            this.colOrderedByStaffName.FieldName = "OrderedByStaffName";
            this.colOrderedByStaffName.Name = "colOrderedByStaffName";
            this.colOrderedByStaffName.OptionsColumn.AllowEdit = false;
            this.colOrderedByStaffName.OptionsColumn.AllowFocus = false;
            this.colOrderedByStaffName.OptionsColumn.ReadOnly = true;
            this.colOrderedByStaffName.Visible = true;
            this.colOrderedByStaffName.VisibleIndex = 12;
            this.colOrderedByStaffName.Width = 103;
            // 
            // colOrderLineCount
            // 
            this.colOrderLineCount.Caption = "Order Line Count";
            this.colOrderLineCount.FieldName = "OrderLineCount";
            this.colOrderLineCount.Name = "colOrderLineCount";
            this.colOrderLineCount.OptionsColumn.AllowEdit = false;
            this.colOrderLineCount.OptionsColumn.AllowFocus = false;
            this.colOrderLineCount.OptionsColumn.ReadOnly = true;
            this.colOrderLineCount.Visible = true;
            this.colOrderLineCount.VisibleIndex = 11;
            this.colOrderLineCount.Width = 103;
            // 
            // colOrderStatusDescription
            // 
            this.colOrderStatusDescription.Caption = "Order Status";
            this.colOrderStatusDescription.FieldName = "OrderStatusDescription";
            this.colOrderStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colOrderStatusDescription.Name = "colOrderStatusDescription";
            this.colOrderStatusDescription.OptionsColumn.AllowEdit = false;
            this.colOrderStatusDescription.OptionsColumn.AllowFocus = false;
            this.colOrderStatusDescription.OptionsColumn.ReadOnly = true;
            this.colOrderStatusDescription.Visible = true;
            this.colOrderStatusDescription.VisibleIndex = 0;
            this.colOrderStatusDescription.Width = 192;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(461, 401);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(542, 401);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupSizeable = false;
            this.repositoryItemPopupContainerEdit1.ShowPopupCloseButton = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 29);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(628, 366);
            this.splitContainerControl1.SplitterPosition = 31;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnLoad);
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(464, 222, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(628, 31);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(560, 4);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(64, 22);
            this.btnLoad.StyleController = this.layoutControl1;
            this.btnLoad.TabIndex = 13;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(268, 4);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all tr" +
    "ansfers will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, true)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(146, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 12;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(61, 4);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all tr" +
    "ansfers will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, true)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(146, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 11;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 31);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDate;
            this.layoutControlItem1.CustomizationFormText = "From Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(207, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(207, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(207, 27);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "From Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDate;
            this.layoutControlItem2.CustomizationFormText = "To Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(207, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(207, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(207, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(207, 27);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "To Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnLoad;
            this.layoutControlItem3.CustomizationFormText = "Load Button";
            this.layoutControlItem3.Location = new System.Drawing.Point(556, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(68, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(68, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(68, 27);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Load Button";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(414, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(142, 27);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp04222_GC_Salt_Order_Select_ListTableAdapter
            // 
            this.sp04222_GC_Salt_Order_Select_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Select_Salt_Order
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(628, 432);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_GC_Select_Salt_Order";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Salt Order";
            this.Load += new System.EventHandler(this.frm_GC_Select_Salt_Order_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04222GCSaltOrderSelectListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KGBags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DataSet_GC_CoreTableAdapters.sp04206_GC_Salt_Supplier_Select_ListTableAdapter sp04206_GC_Salt_Supplier_Select_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KGBags;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private System.Windows.Forms.BindingSource sp04222GCSaltOrderSelectListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOrderHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colPlacedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedDeliveryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountConverted;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCostVatRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCostExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCostIncVat;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCostExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitCostExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCostIncVat;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitCostIncVat;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colGCPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSupplierName;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderLineCount;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderStatusDescription;
        private DataSet_GC_CoreTableAdapters.sp04222_GC_Salt_Order_Select_ListTableAdapter sp04222_GC_Salt_Order_Select_ListTableAdapter;
    }
}

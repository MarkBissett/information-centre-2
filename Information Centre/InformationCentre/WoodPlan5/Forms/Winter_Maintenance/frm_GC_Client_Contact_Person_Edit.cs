using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_GC_Client_Contact_Person_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public bool ibool_Adding_FromTender = false;
        public frm_OM_Tender_Edit frm_Tender_Edit_Screen_To_Update = null;  // Only used if called from the Edit_Tender screen so we can update the correct instance //
        public bool AllowClientChange = true;

        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;
        
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        int i_int_FocusedGrid = 3;

        #endregion

        public frm_GC_Client_Contact_Person_Edit()
        {
            InitializeComponent();
        }

        private void frm_GC_Client_Contact_Person_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 400110;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp03009_EP_Client_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03009_EP_Client_List_With_BlankTableAdapter.Fill(dataSet_EP_DataEntry.sp03009_EP_Client_List_With_Blank);
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04010_GC_Contacts_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04010_GC_Contacts_TypesTableAdapter.Fill(dataSet_GC_DataEntry.sp04010_GC_Contacts_Types);      

            sp05092_GC_Client_Contact_Person_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            
            sp04011_GC_Linked_Client_Contact_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "ClientContactID");
          
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Populate Main Dataset //
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientID"] = intLinkedToRecordID;
                        drNewRow["PersonName"] = "";
                        this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow = this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientID"] = intLinkedToRecordID;
                        drNewRow["PersonName"] = "";
                        this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["PersonName"] = "";
                        this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.Rows.Add(drNewRow);
                        this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp05092_GC_Client_Contact_Person_EditTableAdapter.Fill(this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit, strRecordIDs, strFormMode);     
                    }
                    catch (Exception)
                    {
                    }
                    Load_Linked_Contact_Details();
                    break;
            }
            if (ibool_Adding_FromTender)  // Hide save button plus empty spaces plus linked contacts //
            {
                emptySpaceItemTop.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                ItemForSaveButton.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                emptySpaceItemRight.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                emptySpaceItemBottom.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlGroupContactDetails.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
            else  // Show save button plus empty spaces plus linked contacts //
            {
                emptySpaceItemTop.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                ItemForSaveButton.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                emptySpaceItemRight.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                emptySpaceItemBottom.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layoutControlGroupContactDetails.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }

            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            locusEffectsProvider1 = new LocusEffectsProvider();
            locusEffectsProvider1.Initialize();
            locusEffectsProvider1.FramesPerSecond = 30;

            m_customArrowLocusEffect1 = new ArrowLocusEffect();
            m_customArrowLocusEffect1.Name = "CustomeArrow1";
            m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
            m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
            m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
            m_customArrowLocusEffect1.MovementCycles = 20;
            m_customArrowLocusEffect1.MovementAmplitude = 200;
            m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
            m_customArrowLocusEffect1.LeadInTime = 0; //msec
            m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
            m_customArrowLocusEffect1.AnimationTime = 2000; //msec
            locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enabled Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Client Contact Person", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        if (AllowClientChange)  // Set to False if called by the OM Edit Tender screen (when adding a new site) //
                        {
                            ClientGridLookupEdit.Properties.ReadOnly = false;
                            ClientGridLookupEdit.Properties.Buttons[0].Enabled = true;
                            ClientGridLookupEdit.Properties.Buttons[1].Enabled = true;
                            ClientGridLookupEdit.Properties.Buttons[2].Enabled = true;
                            ClientGridLookupEdit.Focus();
                        }
                        else
                        {
                            ClientGridLookupEdit.Properties.ReadOnly = true;
                            ClientGridLookupEdit.Properties.Buttons[0].Enabled = false;
                            ClientGridLookupEdit.Properties.Buttons[1].Enabled = false;
                            ClientGridLookupEdit.Properties.Buttons[2].Enabled = false;
                            ClientGridLookupEdit.Focus();
                        }
                        PersonNameTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    gridControl3.Enabled = true;
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientGridLookupEdit.Focus();

                        PersonNameTextEdit.Properties.ReadOnly = true;
                     }
                    catch (Exception)
                    {
                    }
                    gridControl3.Enabled = false;
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientGridLookupEdit.Focus();

                        PersonNameTextEdit.Properties.ReadOnly = false;
                     }
                    catch (Exception)
                    {
                    }
                    gridControl3.Enabled = true;
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientGridLookupEdit.Focus();

                        PersonNameTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    gridControl3.Enabled = false;
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            btnSave.Enabled = false;
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            
            ibool_ignoreValidation = true;
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            GridView view = (GridView)gridControl3.MainView;
            view.CloseEditor();
            this.sp04011GCLinkedClientContactEditBindingSource.EndEdit();  // Force any pending save from Grid to underlying table adapter //

            
            DataSet dsChanges = this.dataSet_EP_DataEntry.GetChanges();
            DataSet dsChanges2 = this.dataSet_GC_DataEntry.GetChanges();
            if (dsChanges != null || dsChanges2 != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                btnSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                btnSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            if (strFormMode == "blockedit")
            {
                gridControl3.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            if (i_int_FocusedGrid == 3)  // Linked Contact Detail records //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "blockedit");
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "blockedit" && intRowHandles.Length > 0);
        }

        private void frm_GC_Client_Contact_Person_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_GC_Client_Contact_Person_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            GridView view = (GridView)gridControl3.MainView;
            view.PostEditor();
            int intErrors = CheckRows(view);
            if (intErrors > 0)
            {
                string strMessage = (intErrors == 1 ? "1 Contact Detail record" : intErrors.ToString() + " Contact Detail records") + " with errors - correct the errors before proceeding.\n\nColumns with invalid values have warning icons in them.";
                XtraMessageBox.Show(strMessage, "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Warning, DefaultBoolean.True);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp05092GCClientContactPersonEditBindingSource.EndEdit();
            try
            {
                this.sp05092_GC_Client_Contact_Person_EditTableAdapter.Update(dataSet_EP_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {              
                DataRowView currentRowView = (DataRowView)sp05092GCClientContactPersonEditBindingSource.Current;
                var currentRow = (DataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_EditRow)currentRowView.Row;
                this.strFormMode = "edit";
                if (currentRow != null) 
                {
                    strNewIDs = Convert.ToInt32(currentRow["ClientContactPersonID"]) + ";";
                    currentRow.strMode = "edit";  // Switch mode to Edit so than any subsequent changes update this record //
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //

                    // Pass new ID to any records in the linked grid //
                    int intClientContactPersonID = Convert.ToInt32(currentRow["ClientContactPersonID"]);
                    int intClientID = Convert.ToInt32(currentRow["ClientID"]);
                    view = (GridView)gridControl3.MainView;
                    view.BeginUpdate();
                    foreach (DataRow dr in dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows)
                    {
                        dr["ClientContactPersonID"] = intClientContactPersonID;
                        dr["ClientID"] = intClientID;
                    }
                    this.sp04011GCLinkedClientContactEditBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                    view.EndUpdate();

                    // See if there is a specific Edit Tender screen open we need to send this record's values to (only happens if the Edit Tender screen opened this screen) //
                    if (frm_Tender_Edit_Screen_To_Update != null)
                    {
                        try
                        {
                            frm_Tender_Edit_Screen_To_Update.SetTenderClientContactPersonFromAddScreen(currentRow.ClientContactPersonID, currentRow.PersonName, currentRow.Title, currentRow.Position);
                        }
                        catch (Exception) { }
                    }
                }
            }
            if (this.strFormMode.ToLower() == "add" || this.strFormMode.ToLower() == "edit")
            {
                try  // Attempt to save any changes to Linked Contact Details... //
                {
                    sp04011_GC_Linked_Client_Contact_EditTableAdapter.Update(dataSet_GC_DataEntry);  // Insert, Update and Delete queries defined in Table Adapter //
                }
                catch (System.Exception ex)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                    XtraMessageBox.Show("An error occurred while saving the linked Contact Detail changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return "Error";
                }
            }
            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            frm_Core_Client_Manager fParentForm;
            foreach (Form frmChild in this.ParentForm.MdiChildren)
            {
                if (frmChild.Name == "frm_Core_Client_Manager")
                {
                    fParentForm = (frm_Core_Client_Manager)frmChild;
                    fParentForm.UpdateFormRefreshStatus(3, "", "", "", "", "", "", "", strNewIDs);
                }
            }

            Load_Linked_Contact_Details();
            FilterGrids();

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            int intRecordNewContact = 0;
            int intRecordModifiedContact = 0;
            int intRecordDeletedContact = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_EP_DataEntry.sp05092_GC_Client_Contact_Person_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            for (int i = 0; i < this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNewContact++;
                        break;
                    case DataRowState.Modified:
                        intRecordModifiedContact++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeletedContact++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intRecordNewContact > 0 || intRecordModifiedContact > 0 || intRecordDeletedContact > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New Contact Person record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated Contact Person record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted Contact Person record(s)\n";
                if (intRecordNewContact > 0) strMessage += Convert.ToString(intRecordNewContact) + " New Contact Detail record(s)\n";
                if (intRecordModifiedContact > 0) strMessage += Convert.ToString(intRecordModifiedContact) + " Updated Contact Detail record(s)\n";
                if (intRecordDeletedContact > 0) strMessage += Convert.ToString(intRecordDeletedContact) + " Deleted Contact Detail record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        private void Load_Linked_Contact_Details()
        {
            gridControl3.BeginUpdate();
            try
            {
                sp04011_GC_Linked_Client_Contact_EditTableAdapter.Fill(dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit, strRecordIDs, strFormMode, 1);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl3.EndUpdate();
        }



        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                GridView view = (GridView)gridControl3.MainView;
                view.PostEditor();
                int intErrors = CheckRows(view);
                if (intErrors > 0)
                {
                    string strMessage = (intErrors == 1 ? "1 Contact Detail record" : intErrors.ToString() + " Contact Detail records") + " with errors - correct the errors before proceeding.\n\nColumns with invalid values have warning icons in them.";
                    XtraMessageBox.Show(strMessage, "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Warning, DefaultBoolean.True);
                    return;
                }
                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    FilterGrids();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void ClientGridLookupEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 3001, "Clients");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp03009_EP_Client_List_With_BlankTableAdapter.Fill(dataSet_EP_DataEntry.sp03009_EP_Client_List_With_Blank);
                }
            }
        }
        private void ClientGridLookupEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ClientGridLookupEdit_Validating(object sender, CancelEventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ClientGridLookupEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ClientGridLookupEdit, "");
                if (this.strFormMode.ToLower() == "edit")
                {
                    int intClientID = Convert.ToInt32(glue.EditValue);
                    GridView view = (GridView)gridControl3.MainView;
                    view.BeginDataUpdate();
                    for (int i = 0; i < view.RowCount; i++)  // Using RowCount - not DataRowCount - since we don't want any rows filtered out on a different client //
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(i, "ClientID")) != intClientID) view.SetRowCellValue(i, "ClientID", intClientID);
                    }
                    view.PostEditor();
                    view.EndDataUpdate();
                }

            }
        }

        private void PersonNameTextEdit_EnabledChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void PersonNameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(PersonNameTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(PersonNameTextEdit, "");
            }
        }

        #endregion


        #region GridControl3

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView3_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            string message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "LiLinked Contact Details NOT Shown When Block Adding and Block Editing" : "No Linked Contact Details Available - Click the Add button to Create Linked Contacts");
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
            }
        }

        private void gridView3_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView3_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            Generic_EditChanged(sender, new EventArgs());

            SetMenuStatus();
        }

        private void gridView3_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intFocusedRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intFocusedRow);
            if (dr == null) return;
            switch (view.FocusedColumn.Name)
            {
                case "colContactDetails":
                    {
                        string strValue = e.Value.ToString();
                        if (string.IsNullOrWhiteSpace(strValue))
                        {
                            dr.SetColumnError("ContactDetails", String.Format("{0} should contain a value.", view.Columns["ContactDetails"].Caption));
                        }
                        else
                        {
                            dr.SetColumnError("ContactDetails", "");  // Clear Error Text //
                        }
                    }
                    break;
                case "colContactTypeID":
                    {
                        int intValue = 0;
                        if (e.Value.ToString() != null)
                        {
                            int.TryParse(e.Value.ToString(), out intValue);
                        }
                        if (intValue <= 0)
                        {
                            dr.SetColumnError("ContactTypeID", String.Format("Select a value from {0}.", view.Columns["ContactTypeID"].Caption));
                        }
                        else
                        {
                            dr.SetColumnError("ContactTypeID", "");  // Clear Error Text //
                        }
                    }
                    break;
            }
        }

        private int CheckRows(GridView view)
        {
            int intInvalidRowCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intInvalidRowCount += CheckRow(view, i);
            }
            return intInvalidRowCount;
        }
        private int CheckRow(GridView view, int RowHandle)
        {
            try
            {
                DataRow dr = view.GetDataRow(RowHandle);
                bool boolInvalidRow = false;
                string strValue = null;
                int? intValue = null;
                if (view.GetRowCellValue(RowHandle, "ContactDetails") != DBNull.Value) strValue = view.GetRowCellValue(RowHandle, "ContactDetails").ToString();
                if (view.GetRowCellValue(RowHandle, "ContactTypeID") != DBNull.Value) intValue = Convert.ToInt32(view.GetRowCellValue(RowHandle, "ContactTypeID"));
                
                if (string.IsNullOrWhiteSpace(strValue))
                {
                    dr.SetColumnError("ContactDetails", String.Format("{0} should contain a value.", view.Columns["ContactDetails"].Caption));
                    boolInvalidRow = true;
                }
                else
                {
                    dr.SetColumnError("ContactDetails", "");  // Clear Error Text //
                }
                if (intValue <= 0)
                {
                    dr.SetColumnError("ContactTypeID", String.Format("Select a value from {0}.", view.Columns["ContactTypeID"].Caption));
                    boolInvalidRow = true;
                }
                else
                {
                    dr.SetColumnError("ContactTypeID", "");  // Clear Error Text //
                }

                return (boolInvalidRow ? 1 : 0);
            }
            catch (Exception)
            {
            }
            return 0;
        }


        #endregion


        private void Add_Record()
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp05092GCClientContactPersonEditBindingSource.Current;
            if (currentRow == null) return;
            
            int intClientContactPersonID = 0;
            if (!string.IsNullOrEmpty(currentRow["ClientContactPersonID"].ToString())) intClientContactPersonID = Convert.ToInt32(currentRow["ClientContactPersonID"]);

            int intClientID = 0;
            if (!string.IsNullOrEmpty(currentRow["ClientID"].ToString())) intClientID = Convert.ToInt32(currentRow["ClientID"]);

            if (intClientContactPersonID <= 0 || intClientID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Save the Client Contact Person record first before adding linked Contact Details.", "Add Linked Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                // Use locus effect to highlight the correct button for the user to click //
                try
                {
                    Control c = btnSave;
                    dataLayoutControl1.GetItemByControl(c);
                    DevExpress.XtraLayout.LayoutControlItem item = dataLayoutControl1.GetItemByControl(c);
                    if (item != null)
                    {
                        dataLayoutControl1.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutControl1.FocusHelper.FocusElement(item, true);
                        btnSave.Focus();  // Set active control first //
                        Rectangle rect = Rectangle.Empty;
                        if (dataLayoutControl1.ActiveControl.Parent is BaseEdit)
                        {
                            rect = dataLayoutControl1.RectangleToScreen(dataLayoutControl1.ActiveControl.Parent.Bounds);
                        }
                        else
                        {
                            rect = dataLayoutControl1.RectangleToScreen(dataLayoutControl1.ActiveControl.Bounds);
                        }
                        if (rect.Width > 0 && rect.Height > 0)
                        {
                            System.Drawing.Point point = new System.Drawing.Point(rect.X + 5, (rect.Height / 2) + rect.Y);
                            locusEffectsProvider1.ShowLocusEffect(this, point, m_customArrowLocusEffect1.Name);
                        }
                    }
                }
                catch (Exception)
                {
                }
                return;
            }
            switch (i_int_FocusedGrid)
            {
                case 3:  // Contact Detail record //
                    {
                        try
                        {
                            DataRow drNewRow;
                            drNewRow = dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["ClientContactPersonID"] = intClientContactPersonID;
                            drNewRow["ClientID"] = intClientID;
                            drNewRow["ContactDetails"] = "";
                            drNewRow["ContactTypeID"] = 0;
                            drNewRow["IncludeForReports"] = 0;
                            dataSet_GC_DataEntry.sp04011_GC_Linked_Client_Contact_Edit.Rows.Add(drNewRow);
                            gridView3.FocusedRowHandle = gridView3.DataRowCount - 1;
                            SetChangesPendingLabel();
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new Contract Detail record - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Linked Contact Detail record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 3:  // Contact Detail record //
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Contact Detail records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Contact Detail record" : Convert.ToString(intRowHandles.Length) + " Linked Contact Detail records") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Linked Contact Detail record" : "these Linked Contact Detail records") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            view.BeginUpdate();
                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }
                            view.EndUpdate();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Linked Contact Detail record(s) deleted.\n\nImportant Note: Deleted Linked Contact Detail records are only physically removed from the Contact record on saving changes.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SetChangesPendingLabel();
                        }
                    }
                    break;
            }
        }

        private void FilterGrids()
        {
            string strClientContactPersonID = "";
            DataRowView currentRow = (DataRowView)sp05092GCClientContactPersonEditBindingSource.Current;
            if (currentRow != null)
            {
                strClientContactPersonID = (currentRow["ClientContactPersonID"] == null ? "0" : currentRow["ClientContactPersonID"].ToString());
            }
            if (strClientContactPersonID == "-1") return;

            GridView view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[ClientContactPersonID] = " + Convert.ToString(strClientContactPersonID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
            FilterGrids();  // Required just in case this is a save for the first time (ID of record will change) //
        }






    }
}


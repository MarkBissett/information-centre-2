namespace WoodPlan5
{
    partial class frm_GC_Salt_Order_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Salt_Order_Edit));
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUnitConversionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.GritSupplierIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp04224GCSaltOrderEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.ConversionValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GCPONumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.GritOrderHeaderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PlacedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.UnitCostIncVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.UnitCostExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.OtherCostIncVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TotalCostIncVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TotalCostExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TotalAmountConvertedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04227GCSaltOrderLineEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGritOrderLineID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOrderHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliverToLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliverToLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedirectFromLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedirectFromLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedirectedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSaltColourOrdered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditSaltColourOrdered = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp04218GCSaltColoursListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountOrdered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colAmountOrderedDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp04209GCSaltUnitConversionListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colConversionValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUnitDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountOrderedConverted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25KgBags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedDeliveryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colActualDeliveryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountDelivered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountDeliveredDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountDeliveredConverted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltColourDelivered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colUnitCostExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colTotalCostExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCostIncVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCostIncVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountOrderedConversionFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountDeliveredConversionFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliverToLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditDeliverToLocationName = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colDeliverToLocationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedirectFromLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditRedirectFromLocationName = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colRedirectFromLocationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.OtherCostExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.OtherCostVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.OrderDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EstimatedDeliveryDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.strStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OrderStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04226GCGritOrderStatusListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOrderStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.GritSupplierNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForGritOrderHeaderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPlacedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForConversionValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritSupplierID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForOtherCostExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOtherCostVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOtherCostIncVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTotalCostExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalCostIncVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForUnitCostExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitCostIncVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalAmountConverted = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritSupplierName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForGCPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOrderDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOrderStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedDeliveryDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04224_GC_Salt_Order_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04224_GC_Salt_Order_EditTableAdapter();
            this.sp04226_GC_Grit_Order_Status_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04226_GC_Grit_Order_Status_List_With_BlankTableAdapter();
            this.sp04227_GC_Salt_Order_Line_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04227_GC_Salt_Order_Line_EditTableAdapter();
            this.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter();
            this.sp04218_GC_Salt_Colours_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04218_GC_Salt_Colours_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GritSupplierIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04224GCSaltOrderEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConversionValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCPONumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOrderHeaderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlacedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitCostIncVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitCostExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostIncVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostIncVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalAmountConvertedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04227GCSaltOrderLineEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSaltColourOrdered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04218GCSaltColoursListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAmountOrderedDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04209GCSaltUnitConversionListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditDeliverToLocationName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditRedirectFromLocationName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedDeliveryDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedDeliveryDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04226GCGritOrderStatusListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritSupplierNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOrderHeaderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlacedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConversionValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSupplierID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCostExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCostVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCostIncVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostIncVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitCostExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitCostIncVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalAmountConverted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSupplierName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedDeliveryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrStaffName)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(956, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            this.barDockControlBottom.Size = new System.Drawing.Size(956, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(956, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colItemID
            // 
            this.colItemID.Caption = "Colour ID";
            this.colItemID.FieldName = "ItemID";
            this.colItemID.Name = "colItemID";
            this.colItemID.OptionsColumn.AllowEdit = false;
            this.colItemID.OptionsColumn.AllowFocus = false;
            this.colItemID.OptionsColumn.ReadOnly = true;
            // 
            // colSaltUnitConversionID
            // 
            this.colSaltUnitConversionID.Caption = "Unit Description ID";
            this.colSaltUnitConversionID.FieldName = "SaltUnitConversionID";
            this.colSaltUnitConversionID.Name = "colSaltUnitConversionID";
            this.colSaltUnitConversionID.OptionsColumn.AllowEdit = false;
            this.colSaltUnitConversionID.OptionsColumn.AllowFocus = false;
            this.colSaltUnitConversionID.OptionsColumn.ReadOnly = true;
            this.colSaltUnitConversionID.Width = 110;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Unit Description ID";
            this.gridColumn2.FieldName = "SaltUnitConversionID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 110;
            // 
            // colOrderStatusID
            // 
            this.colOrderStatusID.Caption = "Order Status ID";
            this.colOrderStatusID.FieldName = "OrderStatusID";
            this.colOrderStatusID.Name = "colOrderStatusID";
            this.colOrderStatusID.OptionsColumn.AllowEdit = false;
            this.colOrderStatusID.OptionsColumn.AllowFocus = false;
            this.colOrderStatusID.OptionsColumn.ReadOnly = true;
            this.colOrderStatusID.Width = 97;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(956, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Size = new System.Drawing.Size(956, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(956, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.GritSupplierIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ConversionValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GCPONumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.GritOrderHeaderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PlacedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.UnitCostIncVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.UnitCostExVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.OtherCostIncVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostIncVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostExVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalAmountConvertedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.OtherCostExVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.OtherCostVatRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.OrderDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedDeliveryDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.strStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OrderStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.GritSupplierNameButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp04224GCSaltOrderEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGritOrderHeaderID,
            this.ItemForPlacedByStaffID,
            this.ItemForConversionValue,
            this.ItemForGritSupplierID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(644, 205, 260, 392);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(956, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // GritSupplierIDTextEdit
            // 
            this.GritSupplierIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "GritSupplierID", true));
            this.GritSupplierIDTextEdit.Location = new System.Drawing.Point(138, 108);
            this.GritSupplierIDTextEdit.MenuManager = this.barManager1;
            this.GritSupplierIDTextEdit.Name = "GritSupplierIDTextEdit";
            this.GritSupplierIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GritSupplierIDTextEdit, true);
            this.GritSupplierIDTextEdit.Size = new System.Drawing.Size(789, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GritSupplierIDTextEdit, optionsSpelling1);
            this.GritSupplierIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GritSupplierIDTextEdit.TabIndex = 132;
            // 
            // sp04224GCSaltOrderEditBindingSource
            // 
            this.sp04224GCSaltOrderEditBindingSource.DataMember = "sp04224_GC_Salt_Order_Edit";
            this.sp04224GCSaltOrderEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ConversionValueSpinEdit
            // 
            this.ConversionValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "ConversionValue", true));
            this.ConversionValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ConversionValueSpinEdit.Location = new System.Drawing.Point(138, 60);
            this.ConversionValueSpinEdit.MenuManager = this.barManager1;
            this.ConversionValueSpinEdit.Name = "ConversionValueSpinEdit";
            this.ConversionValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ConversionValueSpinEdit.Properties.Mask.EditMask = "f2";
            this.ConversionValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ConversionValueSpinEdit.Properties.ReadOnly = true;
            this.ConversionValueSpinEdit.Size = new System.Drawing.Size(789, 20);
            this.ConversionValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.ConversionValueSpinEdit.TabIndex = 131;
            // 
            // GCPONumberButtonEdit
            // 
            this.GCPONumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "GCPONumber", true));
            this.GCPONumberButtonEdit.Location = new System.Drawing.Point(137, -159);
            this.GCPONumberButtonEdit.MenuManager = this.barManager1;
            this.GCPONumberButtonEdit.Name = "GCPONumberButtonEdit";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Sequence Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem5.Image")));
            toolTipTitleItem5.Text = "Number Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.GCPONumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Sequence", superToolTip4, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "Number", superToolTip5, true)});
            this.GCPONumberButtonEdit.Size = new System.Drawing.Size(330, 20);
            this.GCPONumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.GCPONumberButtonEdit.TabIndex = 130;
            this.GCPONumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.GCPONumberButtonEdit_ButtonClick);
            // 
            // GritOrderHeaderIDTextEdit
            // 
            this.GritOrderHeaderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "GritOrderHeaderID", true));
            this.GritOrderHeaderIDTextEdit.Location = new System.Drawing.Point(124, 36);
            this.GritOrderHeaderIDTextEdit.MenuManager = this.barManager1;
            this.GritOrderHeaderIDTextEdit.Name = "GritOrderHeaderIDTextEdit";
            this.GritOrderHeaderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GritOrderHeaderIDTextEdit, true);
            this.GritOrderHeaderIDTextEdit.Size = new System.Drawing.Size(803, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GritOrderHeaderIDTextEdit, optionsSpelling2);
            this.GritOrderHeaderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GritOrderHeaderIDTextEdit.TabIndex = 117;
            // 
            // PlacedByStaffIDTextEdit
            // 
            this.PlacedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "PlacedByStaffID", true));
            this.PlacedByStaffIDTextEdit.Location = new System.Drawing.Point(124, 108);
            this.PlacedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.PlacedByStaffIDTextEdit.Name = "PlacedByStaffIDTextEdit";
            this.PlacedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PlacedByStaffIDTextEdit, true);
            this.PlacedByStaffIDTextEdit.Size = new System.Drawing.Size(803, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PlacedByStaffIDTextEdit, optionsSpelling3);
            this.PlacedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PlacedByStaffIDTextEdit.TabIndex = 113;
            // 
            // UnitCostIncVatSpinEdit
            // 
            this.UnitCostIncVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "UnitCostIncVat", true));
            this.UnitCostIncVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitCostIncVatSpinEdit.Location = new System.Drawing.Point(610, 75);
            this.UnitCostIncVatSpinEdit.MenuManager = this.barManager1;
            this.UnitCostIncVatSpinEdit.Name = "UnitCostIncVatSpinEdit";
            this.UnitCostIncVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.UnitCostIncVatSpinEdit.Properties.Mask.EditMask = "c";
            this.UnitCostIncVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitCostIncVatSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.UnitCostIncVatSpinEdit.Properties.ReadOnly = true;
            this.UnitCostIncVatSpinEdit.Size = new System.Drawing.Size(281, 20);
            this.UnitCostIncVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.UnitCostIncVatSpinEdit.TabIndex = 129;
            // 
            // UnitCostExVatSpinEdit
            // 
            this.UnitCostExVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "UnitCostExVat", true));
            this.UnitCostExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitCostExVatSpinEdit.Location = new System.Drawing.Point(610, 51);
            this.UnitCostExVatSpinEdit.MenuManager = this.barManager1;
            this.UnitCostExVatSpinEdit.Name = "UnitCostExVatSpinEdit";
            this.UnitCostExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.UnitCostExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.UnitCostExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitCostExVatSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.UnitCostExVatSpinEdit.Properties.ReadOnly = true;
            this.UnitCostExVatSpinEdit.Size = new System.Drawing.Size(281, 20);
            this.UnitCostExVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.UnitCostExVatSpinEdit.TabIndex = 128;
            // 
            // OtherCostIncVatSpinEdit
            // 
            this.OtherCostIncVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "OtherCostIncVat", true));
            this.OtherCostIncVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OtherCostIncVatSpinEdit.Location = new System.Drawing.Point(173, 75);
            this.OtherCostIncVatSpinEdit.MenuManager = this.barManager1;
            this.OtherCostIncVatSpinEdit.Name = "OtherCostIncVatSpinEdit";
            this.OtherCostIncVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OtherCostIncVatSpinEdit.Properties.Mask.EditMask = "c";
            this.OtherCostIncVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OtherCostIncVatSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.OtherCostIncVatSpinEdit.Size = new System.Drawing.Size(278, 20);
            this.OtherCostIncVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.OtherCostIncVatSpinEdit.TabIndex = 127;
            this.OtherCostIncVatSpinEdit.EditValueChanged += new System.EventHandler(this.OtherCostIncVatSpinEdit_EditValueChanged);
            this.OtherCostIncVatSpinEdit.Validated += new System.EventHandler(this.OtherCostIncVatSpinEdit_Validated);
            // 
            // TotalCostIncVatSpinEdit
            // 
            this.TotalCostIncVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "TotalCostIncVat", true));
            this.TotalCostIncVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalCostIncVatSpinEdit.Location = new System.Drawing.Point(610, 123);
            this.TotalCostIncVatSpinEdit.MenuManager = this.barManager1;
            this.TotalCostIncVatSpinEdit.Name = "TotalCostIncVatSpinEdit";
            this.TotalCostIncVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalCostIncVatSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalCostIncVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostIncVatSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TotalCostIncVatSpinEdit.Properties.ReadOnly = true;
            this.TotalCostIncVatSpinEdit.Size = new System.Drawing.Size(281, 20);
            this.TotalCostIncVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostIncVatSpinEdit.TabIndex = 69;
            // 
            // TotalCostExVatSpinEdit
            // 
            this.TotalCostExVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "TotalCostExVat", true));
            this.TotalCostExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalCostExVatSpinEdit.Location = new System.Drawing.Point(610, 99);
            this.TotalCostExVatSpinEdit.MenuManager = this.barManager1;
            this.TotalCostExVatSpinEdit.Name = "TotalCostExVatSpinEdit";
            this.TotalCostExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalCostExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalCostExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostExVatSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TotalCostExVatSpinEdit.Properties.ReadOnly = true;
            this.TotalCostExVatSpinEdit.Size = new System.Drawing.Size(281, 20);
            this.TotalCostExVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostExVatSpinEdit.TabIndex = 68;
            // 
            // TotalAmountConvertedSpinEdit
            // 
            this.TotalAmountConvertedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "TotalAmountConverted", true));
            this.TotalAmountConvertedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalAmountConvertedSpinEdit.Location = new System.Drawing.Point(610, 27);
            this.TotalAmountConvertedSpinEdit.MenuManager = this.barManager1;
            this.TotalAmountConvertedSpinEdit.Name = "TotalAmountConvertedSpinEdit";
            this.TotalAmountConvertedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalAmountConvertedSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.TotalAmountConvertedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalAmountConvertedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TotalAmountConvertedSpinEdit.Properties.ReadOnly = true;
            this.TotalAmountConvertedSpinEdit.Size = new System.Drawing.Size(281, 20);
            this.TotalAmountConvertedSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalAmountConvertedSpinEdit.TabIndex = 126;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp04227GCSaltOrderLineEditBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Order Line", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Delete Order Line", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(24, 247);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemSpinEdit2DP,
            this.repositoryItemGridLookUpEditSaltColourOrdered,
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID,
            this.repositoryItemTextEdit25KgBags,
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID,
            this.repositoryItemSpinEditPercentage,
            this.repositoryItemSpinEditCurrency,
            this.repositoryItemButtonEditDeliverToLocationName,
            this.repositoryItemButtonEditRedirectFromLocationName,
            this.repositoryItemDateEdit1});
            this.gridControl1.Size = new System.Drawing.Size(891, 200);
            this.gridControl1.TabIndex = 122;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04227GCSaltOrderLineEditBindingSource
            // 
            this.sp04227GCSaltOrderLineEditBindingSource.DataMember = "sp04227_GC_Salt_Order_Line_Edit";
            this.sp04227GCSaltOrderLineEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGritOrderLineID,
            this.colGritOrderHeaderID,
            this.colDeliverToLocationID,
            this.colDeliverToLocationTypeID,
            this.colRedirectFromLocationID,
            this.colRedirectFromLocationTypeID,
            this.colRedirectedReason,
            this.colSaltColourOrdered,
            this.colAmountOrdered,
            this.colAmountOrderedDescriptorID,
            this.colAmountOrderedConverted,
            this.colExpectedDeliveryDate,
            this.colActualDeliveryDate,
            this.colAmountDelivered,
            this.colAmountDeliveredDescriptorID,
            this.colAmountDeliveredConverted,
            this.colSaltColourDelivered,
            this.colVatRate,
            this.colUnitCostExVat,
            this.colTotalCostExVat,
            this.colUnitCostIncVat,
            this.colTotalCostIncVat,
            this.colRemarks,
            this.colAmountOrderedConversionFactor,
            this.colAmountDeliveredConversionFactor,
            this.colDeliverToLocationName,
            this.colDeliverToLocationType,
            this.colRedirectFromLocationName,
            this.colRedirectFromLocationType,
            this.colGCPONumber,
            this.colOrderDate});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView1.OptionsFilter.AllowMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colGritOrderLineID
            // 
            this.colGritOrderLineID.Caption = "Order Line ID";
            this.colGritOrderLineID.FieldName = "GritOrderLineID";
            this.colGritOrderLineID.Name = "colGritOrderLineID";
            this.colGritOrderLineID.OptionsColumn.AllowEdit = false;
            this.colGritOrderLineID.OptionsColumn.AllowFocus = false;
            this.colGritOrderLineID.OptionsColumn.ReadOnly = true;
            // 
            // colGritOrderHeaderID
            // 
            this.colGritOrderHeaderID.Caption = "Order Header ID";
            this.colGritOrderHeaderID.FieldName = "GritOrderHeaderID";
            this.colGritOrderHeaderID.Name = "colGritOrderHeaderID";
            this.colGritOrderHeaderID.OptionsColumn.AllowEdit = false;
            this.colGritOrderHeaderID.OptionsColumn.AllowFocus = false;
            this.colGritOrderHeaderID.OptionsColumn.ReadOnly = true;
            this.colGritOrderHeaderID.Width = 91;
            // 
            // colDeliverToLocationID
            // 
            this.colDeliverToLocationID.Caption = "Deliver To Location ID";
            this.colDeliverToLocationID.FieldName = "DeliverToLocationID";
            this.colDeliverToLocationID.Name = "colDeliverToLocationID";
            this.colDeliverToLocationID.OptionsColumn.AllowEdit = false;
            this.colDeliverToLocationID.OptionsColumn.AllowFocus = false;
            this.colDeliverToLocationID.OptionsColumn.ReadOnly = true;
            this.colDeliverToLocationID.Width = 116;
            // 
            // colDeliverToLocationTypeID
            // 
            this.colDeliverToLocationTypeID.Caption = "Deliver To Location Type ID";
            this.colDeliverToLocationTypeID.FieldName = "DeliverToLocationTypeID";
            this.colDeliverToLocationTypeID.Name = "colDeliverToLocationTypeID";
            this.colDeliverToLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colDeliverToLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colDeliverToLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colDeliverToLocationTypeID.Width = 143;
            // 
            // colRedirectFromLocationID
            // 
            this.colRedirectFromLocationID.Caption = "Redirect From Location ID";
            this.colRedirectFromLocationID.FieldName = "RedirectFromLocationID";
            this.colRedirectFromLocationID.Name = "colRedirectFromLocationID";
            this.colRedirectFromLocationID.OptionsColumn.AllowEdit = false;
            this.colRedirectFromLocationID.OptionsColumn.AllowFocus = false;
            this.colRedirectFromLocationID.OptionsColumn.ReadOnly = true;
            this.colRedirectFromLocationID.Width = 135;
            // 
            // colRedirectFromLocationTypeID
            // 
            this.colRedirectFromLocationTypeID.Caption = "Redirect From Location Type ID";
            this.colRedirectFromLocationTypeID.FieldName = "RedirectFromLocationTypeID";
            this.colRedirectFromLocationTypeID.Name = "colRedirectFromLocationTypeID";
            this.colRedirectFromLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colRedirectFromLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colRedirectFromLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colRedirectFromLocationTypeID.Width = 162;
            // 
            // colRedirectedReason
            // 
            this.colRedirectedReason.Caption = "Redirected Reason";
            this.colRedirectedReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRedirectedReason.FieldName = "RedirectedReason";
            this.colRedirectedReason.Name = "colRedirectedReason";
            this.colRedirectedReason.Visible = true;
            this.colRedirectedReason.VisibleIndex = 17;
            this.colRedirectedReason.Width = 102;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            this.repositoryItemMemoExEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemMemoExEdit1_EditValueChanged);
            // 
            // colSaltColourOrdered
            // 
            this.colSaltColourOrdered.Caption = "Ordered Salt Colour";
            this.colSaltColourOrdered.ColumnEdit = this.repositoryItemGridLookUpEditSaltColourOrdered;
            this.colSaltColourOrdered.FieldName = "SaltColourOrdered";
            this.colSaltColourOrdered.Name = "colSaltColourOrdered";
            this.colSaltColourOrdered.Visible = true;
            this.colSaltColourOrdered.VisibleIndex = 1;
            this.colSaltColourOrdered.Width = 106;
            // 
            // repositoryItemGridLookUpEditSaltColourOrdered
            // 
            this.repositoryItemGridLookUpEditSaltColourOrdered.AutoHeight = false;
            this.repositoryItemGridLookUpEditSaltColourOrdered.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditSaltColourOrdered.DataSource = this.sp04218GCSaltColoursListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditSaltColourOrdered.DisplayMember = "ItemDescription";
            this.repositoryItemGridLookUpEditSaltColourOrdered.Name = "repositoryItemGridLookUpEditSaltColourOrdered";
            this.repositoryItemGridLookUpEditSaltColourOrdered.NullText = "";
            this.repositoryItemGridLookUpEditSaltColourOrdered.ValueMember = "ItemID";
            this.repositoryItemGridLookUpEditSaltColourOrdered.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // sp04218GCSaltColoursListWithBlankBindingSource
            // 
            this.sp04218GCSaltColoursListWithBlankBindingSource.DataMember = "sp04218_GC_Salt_Colours_List_With_Blank";
            this.sp04218GCSaltColoursListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemDescription,
            this.colItemCode,
            this.colItemID,
            this.colOrder});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colItemID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Salt Colour";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 167;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Salt Code";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.OptionsColumn.AllowFocus = false;
            this.colItemCode.OptionsColumn.ReadOnly = true;
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 1;
            this.colItemCode.Width = 93;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Colour Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.Width = 123;
            // 
            // colAmountOrdered
            // 
            this.colAmountOrdered.Caption = "Ordered Amount";
            this.colAmountOrdered.ColumnEdit = this.repositoryItemSpinEdit2DP;
            this.colAmountOrdered.FieldName = "AmountOrdered";
            this.colAmountOrdered.Name = "colAmountOrdered";
            this.colAmountOrdered.Visible = true;
            this.colAmountOrdered.VisibleIndex = 2;
            this.colAmountOrdered.Width = 91;
            // 
            // repositoryItemSpinEdit2DP
            // 
            this.repositoryItemSpinEdit2DP.AutoHeight = false;
            this.repositoryItemSpinEdit2DP.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2DP.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit2DP.Name = "repositoryItemSpinEdit2DP";
            this.repositoryItemSpinEdit2DP.ValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2dp_ValueChanged);
            this.repositoryItemSpinEdit2DP.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2dp_EditValueChanged);
            // 
            // colAmountOrderedDescriptorID
            // 
            this.colAmountOrderedDescriptorID.Caption = "Ordered Unit Descriptor";
            this.colAmountOrderedDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditAmountOrderedDescriptorID;
            this.colAmountOrderedDescriptorID.FieldName = "AmountOrderedDescriptorID";
            this.colAmountOrderedDescriptorID.Name = "colAmountOrderedDescriptorID";
            this.colAmountOrderedDescriptorID.Visible = true;
            this.colAmountOrderedDescriptorID.VisibleIndex = 3;
            this.colAmountOrderedDescriptorID.Width = 125;
            // 
            // repositoryItemGridLookUpEditAmountOrderedDescriptorID
            // 
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.AutoHeight = false;
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.DataSource = this.sp04209GCSaltUnitConversionListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.DisplayMember = "SaltUnitDescription";
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.Name = "repositoryItemGridLookUpEditAmountOrderedDescriptorID";
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.NullText = "";
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2});
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.ValueMember = "SaltUnitConversionID";
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.View = this.gridView4;
            this.repositoryItemGridLookUpEditAmountOrderedDescriptorID.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditAmountOrderedDescriptorID_EditValueChanged);
            // 
            // sp04209GCSaltUnitConversionListWithBlankBindingSource
            // 
            this.sp04209GCSaltUnitConversionListWithBlankBindingSource.DataMember = "sp04209_GC_Salt_Unit_Conversion_List_With_Blank";
            this.sp04209GCSaltUnitConversionListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colConversionValue,
            this.colSaltUnitConversionID,
            this.colSaltUnitDescription});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colSaltUnitConversionID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSaltUnitDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colConversionValue
            // 
            this.colConversionValue.Caption = "Conversion Value";
            this.colConversionValue.ColumnEdit = this.repositoryItemTextEdit2;
            this.colConversionValue.FieldName = "ConversionValue";
            this.colConversionValue.Name = "colConversionValue";
            this.colConversionValue.OptionsColumn.AllowEdit = false;
            this.colConversionValue.OptionsColumn.AllowFocus = false;
            this.colConversionValue.OptionsColumn.ReadOnly = true;
            this.colConversionValue.Visible = true;
            this.colConversionValue.VisibleIndex = 1;
            this.colConversionValue.Width = 130;
            // 
            // colSaltUnitDescription
            // 
            this.colSaltUnitDescription.Caption = "Unit Description";
            this.colSaltUnitDescription.FieldName = "SaltUnitDescription";
            this.colSaltUnitDescription.Name = "colSaltUnitDescription";
            this.colSaltUnitDescription.OptionsColumn.AllowEdit = false;
            this.colSaltUnitDescription.OptionsColumn.AllowFocus = false;
            this.colSaltUnitDescription.OptionsColumn.ReadOnly = true;
            this.colSaltUnitDescription.Visible = true;
            this.colSaltUnitDescription.VisibleIndex = 0;
            this.colSaltUnitDescription.Width = 174;
            // 
            // colAmountOrderedConverted
            // 
            this.colAmountOrderedConverted.Caption = "Ordered Amount Converted";
            this.colAmountOrderedConverted.ColumnEdit = this.repositoryItemTextEdit25KgBags;
            this.colAmountOrderedConverted.FieldName = "AmountOrderedConverted";
            this.colAmountOrderedConverted.Name = "colAmountOrderedConverted";
            this.colAmountOrderedConverted.OptionsColumn.AllowEdit = false;
            this.colAmountOrderedConverted.OptionsColumn.AllowFocus = false;
            this.colAmountOrderedConverted.OptionsColumn.ReadOnly = true;
            this.colAmountOrderedConverted.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AmountOrderedConverted", "Total: {0:######0.00 25 Kg Bags}")});
            this.colAmountOrderedConverted.Visible = true;
            this.colAmountOrderedConverted.VisibleIndex = 4;
            this.colAmountOrderedConverted.Width = 145;
            // 
            // repositoryItemTextEdit25KgBags
            // 
            this.repositoryItemTextEdit25KgBags.AutoHeight = false;
            this.repositoryItemTextEdit25KgBags.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25KgBags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KgBags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KgBags.Name = "repositoryItemTextEdit25KgBags";
            // 
            // colExpectedDeliveryDate
            // 
            this.colExpectedDeliveryDate.Caption = "Expected Delivery Date";
            this.colExpectedDeliveryDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colExpectedDeliveryDate.FieldName = "ExpectedDeliveryDate";
            this.colExpectedDeliveryDate.Name = "colExpectedDeliveryDate";
            this.colExpectedDeliveryDate.Visible = true;
            this.colExpectedDeliveryDate.VisibleIndex = 5;
            this.colExpectedDeliveryDate.Width = 124;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colActualDeliveryDate
            // 
            this.colActualDeliveryDate.Caption = "Actual Delivery Date";
            this.colActualDeliveryDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colActualDeliveryDate.FieldName = "ActualDeliveryDate";
            this.colActualDeliveryDate.Name = "colActualDeliveryDate";
            this.colActualDeliveryDate.Visible = true;
            this.colActualDeliveryDate.VisibleIndex = 11;
            this.colActualDeliveryDate.Width = 109;
            // 
            // colAmountDelivered
            // 
            this.colAmountDelivered.Caption = "Delivered Amount";
            this.colAmountDelivered.ColumnEdit = this.repositoryItemSpinEdit2DP;
            this.colAmountDelivered.FieldName = "AmountDelivered";
            this.colAmountDelivered.Name = "colAmountDelivered";
            this.colAmountDelivered.Visible = true;
            this.colAmountDelivered.VisibleIndex = 13;
            this.colAmountDelivered.Width = 96;
            // 
            // colAmountDeliveredDescriptorID
            // 
            this.colAmountDeliveredDescriptorID.Caption = "Delivered Unit Descriptor";
            this.colAmountDeliveredDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID;
            this.colAmountDeliveredDescriptorID.FieldName = "AmountDeliveredDescriptorID";
            this.colAmountDeliveredDescriptorID.Name = "colAmountDeliveredDescriptorID";
            this.colAmountDeliveredDescriptorID.Visible = true;
            this.colAmountDeliveredDescriptorID.VisibleIndex = 14;
            this.colAmountDeliveredDescriptorID.Width = 130;
            // 
            // repositoryItemGridLookUpEditAmountDeliveredDescriptorID
            // 
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.AutoHeight = false;
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.DataSource = this.sp04209GCSaltUnitConversionListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.DisplayMember = "SaltUnitDescription";
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.Name = "repositoryItemGridLookUpEditAmountDeliveredDescriptorID";
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.NullText = "";
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit4});
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.ValueMember = "SaltUnitConversionID";
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.View = this.gridView5;
            this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID_EditValueChanged);
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn2;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Conversion Value";
            this.gridColumn1.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn1.FieldName = "ConversionValue";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 130;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Unit Description";
            this.gridColumn3.FieldName = "SaltUnitDescription";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 174;
            // 
            // colAmountDeliveredConverted
            // 
            this.colAmountDeliveredConverted.Caption = "Delivered Amount Converted";
            this.colAmountDeliveredConverted.ColumnEdit = this.repositoryItemTextEdit25KgBags;
            this.colAmountDeliveredConverted.FieldName = "AmountDeliveredConverted";
            this.colAmountDeliveredConverted.Name = "colAmountDeliveredConverted";
            this.colAmountDeliveredConverted.OptionsColumn.AllowEdit = false;
            this.colAmountDeliveredConverted.OptionsColumn.AllowFocus = false;
            this.colAmountDeliveredConverted.OptionsColumn.ReadOnly = true;
            this.colAmountDeliveredConverted.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AmountDeliveredConverted", "Total: {0:######0.00 25 Kg Bags}")});
            this.colAmountDeliveredConverted.Visible = true;
            this.colAmountDeliveredConverted.VisibleIndex = 15;
            this.colAmountDeliveredConverted.Width = 150;
            // 
            // colSaltColourDelivered
            // 
            this.colSaltColourDelivered.Caption = "Delivered Salt Colour";
            this.colSaltColourDelivered.ColumnEdit = this.repositoryItemGridLookUpEditSaltColourOrdered;
            this.colSaltColourDelivered.FieldName = "SaltColourDelivered";
            this.colSaltColourDelivered.Name = "colSaltColourDelivered";
            this.colSaltColourDelivered.Visible = true;
            this.colSaltColourDelivered.VisibleIndex = 12;
            this.colSaltColourDelivered.Width = 111;
            // 
            // colVatRate
            // 
            this.colVatRate.Caption = "VAT Rate";
            this.colVatRate.ColumnEdit = this.repositoryItemSpinEditPercentage;
            this.colVatRate.FieldName = "VatRate";
            this.colVatRate.Name = "colVatRate";
            this.colVatRate.Visible = true;
            this.colVatRate.VisibleIndex = 6;
            // 
            // repositoryItemSpinEditPercentage
            // 
            this.repositoryItemSpinEditPercentage.AutoHeight = false;
            this.repositoryItemSpinEditPercentage.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditPercentage.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercentage.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            131072});
            this.repositoryItemSpinEditPercentage.Name = "repositoryItemSpinEditPercentage";
            this.repositoryItemSpinEditPercentage.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditPercentage_EditValueChanged);
            // 
            // colUnitCostExVat
            // 
            this.colUnitCostExVat.Caption = "Unit Cost Ex VAT";
            this.colUnitCostExVat.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colUnitCostExVat.FieldName = "UnitCostExVat";
            this.colUnitCostExVat.Name = "colUnitCostExVat";
            this.colUnitCostExVat.Visible = true;
            this.colUnitCostExVat.VisibleIndex = 7;
            this.colUnitCostExVat.Width = 92;
            // 
            // repositoryItemSpinEditCurrency
            // 
            this.repositoryItemSpinEditCurrency.AutoHeight = false;
            this.repositoryItemSpinEditCurrency.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditCurrency.Mask.EditMask = "c";
            this.repositoryItemSpinEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCurrency.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEditCurrency.Name = "repositoryItemSpinEditCurrency";
            this.repositoryItemSpinEditCurrency.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditCurrency_EditValueChanged);
            // 
            // colTotalCostExVat
            // 
            this.colTotalCostExVat.Caption = "Total Cost Ex VAT";
            this.colTotalCostExVat.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colTotalCostExVat.FieldName = "TotalCostExVat";
            this.colTotalCostExVat.Name = "colTotalCostExVat";
            this.colTotalCostExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCostExVat", "Total: {0:c2}")});
            this.colTotalCostExVat.Visible = true;
            this.colTotalCostExVat.VisibleIndex = 9;
            this.colTotalCostExVat.Width = 97;
            // 
            // colUnitCostIncVat
            // 
            this.colUnitCostIncVat.Caption = "Unit Cost Inc VAT";
            this.colUnitCostIncVat.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colUnitCostIncVat.FieldName = "UnitCostIncVat";
            this.colUnitCostIncVat.Name = "colUnitCostIncVat";
            this.colUnitCostIncVat.Visible = true;
            this.colUnitCostIncVat.VisibleIndex = 8;
            this.colUnitCostIncVat.Width = 95;
            // 
            // colTotalCostIncVat
            // 
            this.colTotalCostIncVat.Caption = "Total Cost Inc VAT";
            this.colTotalCostIncVat.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colTotalCostIncVat.FieldName = "TotalCostIncVat";
            this.colTotalCostIncVat.Name = "colTotalCostIncVat";
            this.colTotalCostIncVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCostIncVat", "Total: {0:c2}")});
            this.colTotalCostIncVat.Visible = true;
            this.colTotalCostIncVat.VisibleIndex = 10;
            this.colTotalCostIncVat.Width = 100;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 18;
            // 
            // colAmountOrderedConversionFactor
            // 
            this.colAmountOrderedConversionFactor.Caption = "Ordered Amount Conversion Factor";
            this.colAmountOrderedConversionFactor.ColumnEdit = this.repositoryItemSpinEdit2DP;
            this.colAmountOrderedConversionFactor.FieldName = "AmountOrderedConversionFactor";
            this.colAmountOrderedConversionFactor.Name = "colAmountOrderedConversionFactor";
            this.colAmountOrderedConversionFactor.OptionsColumn.AllowEdit = false;
            this.colAmountOrderedConversionFactor.OptionsColumn.AllowFocus = false;
            this.colAmountOrderedConversionFactor.OptionsColumn.ReadOnly = true;
            this.colAmountOrderedConversionFactor.Width = 182;
            // 
            // colAmountDeliveredConversionFactor
            // 
            this.colAmountDeliveredConversionFactor.Caption = "Delivered Amount Conversion Factor";
            this.colAmountDeliveredConversionFactor.ColumnEdit = this.repositoryItemSpinEdit2DP;
            this.colAmountDeliveredConversionFactor.FieldName = "AmountDeliveredConversionFactor";
            this.colAmountDeliveredConversionFactor.Name = "colAmountDeliveredConversionFactor";
            this.colAmountDeliveredConversionFactor.OptionsColumn.AllowEdit = false;
            this.colAmountDeliveredConversionFactor.OptionsColumn.AllowFocus = false;
            this.colAmountDeliveredConversionFactor.OptionsColumn.ReadOnly = true;
            this.colAmountDeliveredConversionFactor.Width = 187;
            // 
            // colDeliverToLocationName
            // 
            this.colDeliverToLocationName.Caption = "Deliver To Location";
            this.colDeliverToLocationName.ColumnEdit = this.repositoryItemButtonEditDeliverToLocationName;
            this.colDeliverToLocationName.FieldName = "DeliverToLocationName";
            this.colDeliverToLocationName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDeliverToLocationName.Name = "colDeliverToLocationName";
            this.colDeliverToLocationName.Visible = true;
            this.colDeliverToLocationName.VisibleIndex = 0;
            this.colDeliverToLocationName.Width = 185;
            // 
            // repositoryItemButtonEditDeliverToLocationName
            // 
            this.repositoryItemButtonEditDeliverToLocationName.AutoHeight = false;
            this.repositoryItemButtonEditDeliverToLocationName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "choose", null, true)});
            this.repositoryItemButtonEditDeliverToLocationName.Name = "repositoryItemButtonEditDeliverToLocationName";
            this.repositoryItemButtonEditDeliverToLocationName.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditDeliverToLocationName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditDeliverToLocationName_ButtonClick);
            // 
            // colDeliverToLocationType
            // 
            this.colDeliverToLocationType.Caption = "Deliver To Location Type";
            this.colDeliverToLocationType.FieldName = "DeliverToLocationType";
            this.colDeliverToLocationType.Name = "colDeliverToLocationType";
            this.colDeliverToLocationType.Width = 129;
            // 
            // colRedirectFromLocationName
            // 
            this.colRedirectFromLocationName.Caption = "Redirect From Name";
            this.colRedirectFromLocationName.ColumnEdit = this.repositoryItemButtonEditRedirectFromLocationName;
            this.colRedirectFromLocationName.FieldName = "RedirectFromLocationName";
            this.colRedirectFromLocationName.Name = "colRedirectFromLocationName";
            this.colRedirectFromLocationName.Visible = true;
            this.colRedirectFromLocationName.VisibleIndex = 16;
            this.colRedirectFromLocationName.Width = 190;
            // 
            // repositoryItemButtonEditRedirectFromLocationName
            // 
            this.repositoryItemButtonEditRedirectFromLocationName.AutoHeight = false;
            this.repositoryItemButtonEditRedirectFromLocationName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "choose", null, true)});
            this.repositoryItemButtonEditRedirectFromLocationName.Name = "repositoryItemButtonEditRedirectFromLocationName";
            this.repositoryItemButtonEditRedirectFromLocationName.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditRedirectFromLocationName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditRedirectFromLocationName_ButtonClick);
            // 
            // colRedirectFromLocationType
            // 
            this.colRedirectFromLocationType.Caption = "Redeirect From Type";
            this.colRedirectFromLocationType.FieldName = "RedirectFromLocationType";
            this.colRedirectFromLocationType.Name = "colRedirectFromLocationType";
            this.colRedirectFromLocationType.OptionsColumn.AllowEdit = false;
            this.colRedirectFromLocationType.OptionsColumn.AllowFocus = false;
            this.colRedirectFromLocationType.OptionsColumn.ReadOnly = true;
            this.colRedirectFromLocationType.Width = 111;
            // 
            // colGCPONumber
            // 
            this.colGCPONumber.Caption = "GC PO Number";
            this.colGCPONumber.FieldName = "GCPONumber";
            this.colGCPONumber.Name = "colGCPONumber";
            this.colGCPONumber.OptionsColumn.AllowEdit = false;
            this.colGCPONumber.OptionsColumn.AllowFocus = false;
            this.colGCPONumber.OptionsColumn.ReadOnly = true;
            this.colGCPONumber.Width = 82;
            // 
            // colOrderDate
            // 
            this.colOrderDate.Caption = "Order Date";
            this.colOrderDate.FieldName = "OrderDate";
            this.colOrderDate.Name = "colOrderDate";
            this.colOrderDate.OptionsColumn.AllowEdit = false;
            this.colOrderDate.OptionsColumn.AllowFocus = false;
            this.colOrderDate.OptionsColumn.ReadOnly = true;
            // 
            // OtherCostExVatSpinEdit
            // 
            this.OtherCostExVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "OtherCostExVat", true));
            this.OtherCostExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OtherCostExVatSpinEdit.Location = new System.Drawing.Point(173, 27);
            this.OtherCostExVatSpinEdit.MenuManager = this.barManager1;
            this.OtherCostExVatSpinEdit.Name = "OtherCostExVatSpinEdit";
            this.OtherCostExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OtherCostExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.OtherCostExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OtherCostExVatSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.OtherCostExVatSpinEdit.Size = new System.Drawing.Size(278, 20);
            this.OtherCostExVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.OtherCostExVatSpinEdit.TabIndex = 62;
            this.OtherCostExVatSpinEdit.EditValueChanged += new System.EventHandler(this.OtherCostExVatSpinEdit_EditValueChanged);
            this.OtherCostExVatSpinEdit.Validated += new System.EventHandler(this.OtherCostExVatSpinEdit_Validated);
            // 
            // OtherCostVatRateSpinEdit
            // 
            this.OtherCostVatRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "OtherCostVatRate", true));
            this.OtherCostVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OtherCostVatRateSpinEdit.Location = new System.Drawing.Point(173, 51);
            this.OtherCostVatRateSpinEdit.MenuManager = this.barManager1;
            this.OtherCostVatRateSpinEdit.Name = "OtherCostVatRateSpinEdit";
            this.OtherCostVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OtherCostVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.OtherCostVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OtherCostVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.OtherCostVatRateSpinEdit.Size = new System.Drawing.Size(278, 20);
            this.OtherCostVatRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.OtherCostVatRateSpinEdit.TabIndex = 64;
            this.OtherCostVatRateSpinEdit.EditValueChanged += new System.EventHandler(this.OtherCostVatRateSpinEdit_EditValueChanged);
            this.OtherCostVatRateSpinEdit.Validated += new System.EventHandler(this.OtherCostVatRateSpinEdit_Validated);
            // 
            // OrderDateDateEdit
            // 
            this.OrderDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "OrderDate", true));
            this.OrderDateDateEdit.EditValue = null;
            this.OrderDateDateEdit.Location = new System.Drawing.Point(596, -159);
            this.OrderDateDateEdit.MenuManager = this.barManager1;
            this.OrderDateDateEdit.Name = "OrderDateDateEdit";
            this.OrderDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OrderDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OrderDateDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.OrderDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OrderDateDateEdit.Size = new System.Drawing.Size(331, 20);
            this.OrderDateDateEdit.StyleController = this.dataLayoutControl1;
            this.OrderDateDateEdit.TabIndex = 45;
            // 
            // EstimatedDeliveryDateDateEdit
            // 
            this.EstimatedDeliveryDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "EstimatedDeliveryDate", true));
            this.EstimatedDeliveryDateDateEdit.EditValue = null;
            this.EstimatedDeliveryDateDateEdit.Location = new System.Drawing.Point(137, -111);
            this.EstimatedDeliveryDateDateEdit.MenuManager = this.barManager1;
            this.EstimatedDeliveryDateDateEdit.Name = "EstimatedDeliveryDateDateEdit";
            this.EstimatedDeliveryDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EstimatedDeliveryDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedDeliveryDateDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.EstimatedDeliveryDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedDeliveryDateDateEdit.Size = new System.Drawing.Size(330, 20);
            this.EstimatedDeliveryDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedDeliveryDateDateEdit.TabIndex = 106;
            // 
            // strStaffNameTextEdit
            // 
            this.strStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "strStaffName", true));
            this.strStaffNameTextEdit.Location = new System.Drawing.Point(596, -111);
            this.strStaffNameTextEdit.MenuManager = this.barManager1;
            this.strStaffNameTextEdit.Name = "strStaffNameTextEdit";
            this.strStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strStaffNameTextEdit, true);
            this.strStaffNameTextEdit.Size = new System.Drawing.Size(331, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strStaffNameTextEdit, optionsSpelling4);
            this.strStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.strStaffNameTextEdit.TabIndex = 105;
            // 
            // OrderStatusIDGridLookUpEdit
            // 
            this.OrderStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "OrderStatusID", true));
            this.OrderStatusIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OrderStatusIDGridLookUpEdit.Location = new System.Drawing.Point(596, -135);
            this.OrderStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.OrderStatusIDGridLookUpEdit.Name = "OrderStatusIDGridLookUpEdit";
            this.OrderStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OrderStatusIDGridLookUpEdit.Properties.DataSource = this.sp04226GCGritOrderStatusListWithBlankBindingSource;
            this.OrderStatusIDGridLookUpEdit.Properties.DisplayMember = "OrderStatusDescription";
            this.OrderStatusIDGridLookUpEdit.Properties.NullText = "";
            this.OrderStatusIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.OrderStatusIDGridLookUpEdit.Properties.ValueMember = "OrderStatusID";
            this.OrderStatusIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.OrderStatusIDGridLookUpEdit.Size = new System.Drawing.Size(331, 20);
            this.OrderStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.OrderStatusIDGridLookUpEdit.TabIndex = 103;
            this.OrderStatusIDGridLookUpEdit.Enter += new System.EventHandler(this.OrderStatusIDGridLookUpEdit_Enter);
            this.OrderStatusIDGridLookUpEdit.Leave += new System.EventHandler(this.OrderStatusIDGridLookUpEdit_Leave);
            this.OrderStatusIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.OrderStatusIDGridLookUpEdit_Validating);
            // 
            // sp04226GCGritOrderStatusListWithBlankBindingSource
            // 
            this.sp04226GCGritOrderStatusListWithBlankBindingSource.DataMember = "sp04226_GC_Grit_Order_Status_List_With_Blank";
            this.sp04226GCGritOrderStatusListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOrderStatusID,
            this.colOrderStatusDescription,
            this.colOrderStatusOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colOrderStatusID;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowFilterEditor = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrderStatusOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colOrderStatusDescription
            // 
            this.colOrderStatusDescription.Caption = "Order Status";
            this.colOrderStatusDescription.FieldName = "OrderStatusDescription";
            this.colOrderStatusDescription.Name = "colOrderStatusDescription";
            this.colOrderStatusDescription.OptionsColumn.AllowEdit = false;
            this.colOrderStatusDescription.OptionsColumn.AllowFocus = false;
            this.colOrderStatusDescription.OptionsColumn.ReadOnly = true;
            this.colOrderStatusDescription.Visible = true;
            this.colOrderStatusDescription.VisibleIndex = 0;
            this.colOrderStatusDescription.Width = 253;
            // 
            // colOrderStatusOrder
            // 
            this.colOrderStatusOrder.Caption = "Sort Order";
            this.colOrderStatusOrder.FieldName = "OrderStatusOrder";
            this.colOrderStatusOrder.Name = "colOrderStatusOrder";
            this.colOrderStatusOrder.OptionsColumn.AllowEdit = false;
            this.colOrderStatusOrder.OptionsColumn.AllowFocus = false;
            this.colOrderStatusOrder.OptionsColumn.ReadOnly = true;
            this.colOrderStatusOrder.Width = 85;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04224GCSaltOrderEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(138, -183);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 9;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, -7);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(867, 182);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling5);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 8;
            // 
            // GritSupplierNameButtonEdit
            // 
            this.GritSupplierNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04224GCSaltOrderEditBindingSource, "GritSupplierName", true));
            this.GritSupplierNameButtonEdit.Location = new System.Drawing.Point(137, -135);
            this.GritSupplierNameButtonEdit.MenuManager = this.barManager1;
            this.GritSupplierNameButtonEdit.Name = "GritSupplierNameButtonEdit";
            this.GritSupplierNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "view", null, true)});
            this.GritSupplierNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.GritSupplierNameButtonEdit.Size = new System.Drawing.Size(330, 20);
            this.GritSupplierNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.GritSupplierNameButtonEdit.TabIndex = 33;
            this.GritSupplierNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.GritSupplierNameButtonEdit_ButtonClick);
            this.GritSupplierNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.GritSupplierIDButtonEdit_Validating);
            // 
            // ItemForGritOrderHeaderID
            // 
            this.ItemForGritOrderHeaderID.Control = this.GritOrderHeaderIDTextEdit;
            this.ItemForGritOrderHeaderID.CustomizationFormText = "Grit Order Header ID:";
            this.ItemForGritOrderHeaderID.Location = new System.Drawing.Point(0, 24);
            this.ItemForGritOrderHeaderID.Name = "ItemForGritOrderHeaderID";
            this.ItemForGritOrderHeaderID.Size = new System.Drawing.Size(919, 24);
            this.ItemForGritOrderHeaderID.Text = "Grit Order Header ID:";
            this.ItemForGritOrderHeaderID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPlacedByStaffID
            // 
            this.ItemForPlacedByStaffID.Control = this.PlacedByStaffIDTextEdit;
            this.ItemForPlacedByStaffID.CustomizationFormText = "Recorded By Staff ID:";
            this.ItemForPlacedByStaffID.Location = new System.Drawing.Point(0, 96);
            this.ItemForPlacedByStaffID.Name = "ItemForPlacedByStaffID";
            this.ItemForPlacedByStaffID.Size = new System.Drawing.Size(919, 24);
            this.ItemForPlacedByStaffID.Text = "Recorded By Staff ID:";
            this.ItemForPlacedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForConversionValue
            // 
            this.ItemForConversionValue.Control = this.ConversionValueSpinEdit;
            this.ItemForConversionValue.CustomizationFormText = "Conversion Factor:";
            this.ItemForConversionValue.Location = new System.Drawing.Point(0, 48);
            this.ItemForConversionValue.Name = "ItemForConversionValue";
            this.ItemForConversionValue.Size = new System.Drawing.Size(919, 24);
            this.ItemForConversionValue.Text = "Conversion Factor:";
            this.ItemForConversionValue.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForGritSupplierID
            // 
            this.ItemForGritSupplierID.Control = this.GritSupplierIDTextEdit;
            this.ItemForGritSupplierID.CustomizationFormText = "Supplier ID:";
            this.ItemForGritSupplierID.Location = new System.Drawing.Point(0, 96);
            this.ItemForGritSupplierID.Name = "ItemForGritSupplierID";
            this.ItemForGritSupplierID.Size = new System.Drawing.Size(919, 24);
            this.ItemForGritSupplierID.Text = "Supplier ID:";
            this.ItemForGritSupplierID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -195);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(939, 676);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.ItemForGritSupplierName,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItem1,
            this.emptySpaceItem12,
            this.emptySpaceItem2,
            this.layoutControlGroup15,
            this.emptySpaceItem13,
            this.ItemForGCPONumber,
            this.ItemForOrderDate,
            this.ItemForOrderStatusID,
            this.ItemForEstimatedDeliveryDate,
            this.ItemForstrStaffName});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(919, 656);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 106);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(919, 280);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(895, 234);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Costs";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup10,
            this.layoutControlGroup11,
            this.splitterItem2,
            this.emptySpaceItem8});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(871, 186);
            this.layoutControlGroup6.Text = "Totals \\ Costs";
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Other Cost Totals";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.ItemForOtherCostExVat,
            this.ItemForOtherCostVatRate,
            this.ItemForOtherCostIncVat});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(431, 176);
            this.layoutControlGroup10.Text = "Other Costs";
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(407, 58);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForOtherCostExVat
            // 
            this.ItemForOtherCostExVat.Control = this.OtherCostExVatSpinEdit;
            this.ItemForOtherCostExVat.CustomizationFormText = "Other Cost Ex VAT:";
            this.ItemForOtherCostExVat.Location = new System.Drawing.Point(0, 0);
            this.ItemForOtherCostExVat.Name = "ItemForOtherCostExVat";
            this.ItemForOtherCostExVat.Size = new System.Drawing.Size(407, 24);
            this.ItemForOtherCostExVat.Text = "Other Cost Ex VAT:";
            this.ItemForOtherCostExVat.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForOtherCostVatRate
            // 
            this.ItemForOtherCostVatRate.Control = this.OtherCostVatRateSpinEdit;
            this.ItemForOtherCostVatRate.CustomizationFormText = "Other Cost VAT Rate:";
            this.ItemForOtherCostVatRate.Location = new System.Drawing.Point(0, 24);
            this.ItemForOtherCostVatRate.Name = "ItemForOtherCostVatRate";
            this.ItemForOtherCostVatRate.Size = new System.Drawing.Size(407, 24);
            this.ItemForOtherCostVatRate.Text = "Other Cost VAT Rate:";
            this.ItemForOtherCostVatRate.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForOtherCostIncVat
            // 
            this.ItemForOtherCostIncVat.Control = this.OtherCostIncVatSpinEdit;
            this.ItemForOtherCostIncVat.CustomizationFormText = "Other Cost Inc VAT:";
            this.ItemForOtherCostIncVat.Location = new System.Drawing.Point(0, 48);
            this.ItemForOtherCostIncVat.Name = "ItemForOtherCostIncVat";
            this.ItemForOtherCostIncVat.Size = new System.Drawing.Size(407, 24);
            this.ItemForOtherCostIncVat.Text = "Other Cost Inc VAT:";
            this.ItemForOtherCostIncVat.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Callout Totals";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTotalCostExVat,
            this.ItemForTotalCostIncVat,
            this.emptySpaceItem7,
            this.ItemForUnitCostExVat,
            this.ItemForUnitCostIncVat,
            this.ItemForTotalAmountConverted});
            this.layoutControlGroup11.Location = new System.Drawing.Point(437, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(434, 176);
            this.layoutControlGroup11.Text = "Order Totals";
            // 
            // ItemForTotalCostExVat
            // 
            this.ItemForTotalCostExVat.Control = this.TotalCostExVatSpinEdit;
            this.ItemForTotalCostExVat.CustomizationFormText = "Total Cost Ex VAT:";
            this.ItemForTotalCostExVat.Location = new System.Drawing.Point(0, 72);
            this.ItemForTotalCostExVat.Name = "ItemForTotalCostExVat";
            this.ItemForTotalCostExVat.Size = new System.Drawing.Size(410, 24);
            this.ItemForTotalCostExVat.Text = "Total Cost Ex VAT:";
            this.ItemForTotalCostExVat.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForTotalCostIncVat
            // 
            this.ItemForTotalCostIncVat.Control = this.TotalCostIncVatSpinEdit;
            this.ItemForTotalCostIncVat.CustomizationFormText = "Total Sell:";
            this.ItemForTotalCostIncVat.Location = new System.Drawing.Point(0, 96);
            this.ItemForTotalCostIncVat.Name = "ItemForTotalCostIncVat";
            this.ItemForTotalCostIncVat.Size = new System.Drawing.Size(410, 24);
            this.ItemForTotalCostIncVat.Text = "Total Cost Inc VAT:";
            this.ItemForTotalCostIncVat.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(410, 10);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForUnitCostExVat
            // 
            this.ItemForUnitCostExVat.Control = this.UnitCostExVatSpinEdit;
            this.ItemForUnitCostExVat.CustomizationFormText = "Unit Cost Ex VAT:";
            this.ItemForUnitCostExVat.Location = new System.Drawing.Point(0, 24);
            this.ItemForUnitCostExVat.Name = "ItemForUnitCostExVat";
            this.ItemForUnitCostExVat.Size = new System.Drawing.Size(410, 24);
            this.ItemForUnitCostExVat.Text = "Unit Cost Ex VAT:";
            this.ItemForUnitCostExVat.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForUnitCostIncVat
            // 
            this.ItemForUnitCostIncVat.Control = this.UnitCostIncVatSpinEdit;
            this.ItemForUnitCostIncVat.CustomizationFormText = "Unit Cost Inc VAT:";
            this.ItemForUnitCostIncVat.Location = new System.Drawing.Point(0, 48);
            this.ItemForUnitCostIncVat.Name = "ItemForUnitCostIncVat";
            this.ItemForUnitCostIncVat.Size = new System.Drawing.Size(410, 24);
            this.ItemForUnitCostIncVat.Text = "Unit Cost Inc VAT:";
            this.ItemForUnitCostIncVat.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForTotalAmountConverted
            // 
            this.ItemForTotalAmountConverted.Control = this.TotalAmountConvertedSpinEdit;
            this.ItemForTotalAmountConverted.CustomizationFormText = "Total Amount Converted:";
            this.ItemForTotalAmountConverted.Location = new System.Drawing.Point(0, 0);
            this.ItemForTotalAmountConverted.Name = "ItemForTotalAmountConverted";
            this.ItemForTotalAmountConverted.Size = new System.Drawing.Size(410, 24);
            this.ItemForTotalAmountConverted.Text = "Total Amount Converted:";
            this.ItemForTotalAmountConverted.TextSize = new System.Drawing.Size(122, 13);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(431, 0);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(6, 176);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 176);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(871, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(871, 186);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(871, 186);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForGritSupplierName
            // 
            this.ItemForGritSupplierName.Control = this.GritSupplierNameButtonEdit;
            this.ItemForGritSupplierName.CustomizationFormText = "Supplier:";
            this.ItemForGritSupplierName.Location = new System.Drawing.Point(0, 48);
            this.ItemForGritSupplierName.Name = "ItemForGritSupplierName";
            this.ItemForGritSupplierName.Size = new System.Drawing.Size(459, 24);
            this.ItemForGritSupplierName.Text = "Supplier:";
            this.ItemForGritSupplierName.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(126, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(126, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(126, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(326, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(593, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "Navigator";
            this.layoutControlItem1.Location = new System.Drawing.Point(126, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem1.Text = "Navigator";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(919, 10);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 646);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(919, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.CustomizationFormText = "Linked Rates";
            this.layoutControlGroup15.ExpandButtonVisible = true;
            this.layoutControlGroup15.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 396);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(919, 250);
            this.layoutControlGroup15.Text = "Order Lines";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Rates";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 204);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(229, 204);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(895, 204);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Rates";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 386);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(919, 10);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForGCPONumber
            // 
            this.ItemForGCPONumber.Control = this.GCPONumberButtonEdit;
            this.ItemForGCPONumber.CustomizationFormText = "GC PO Number:";
            this.ItemForGCPONumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForGCPONumber.Name = "ItemForGCPONumber";
            this.ItemForGCPONumber.Size = new System.Drawing.Size(459, 24);
            this.ItemForGCPONumber.Text = "GC PO Number:";
            this.ItemForGCPONumber.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForOrderDate
            // 
            this.ItemForOrderDate.Control = this.OrderDateDateEdit;
            this.ItemForOrderDate.CustomizationFormText = "Order Date:";
            this.ItemForOrderDate.Location = new System.Drawing.Point(459, 24);
            this.ItemForOrderDate.Name = "ItemForOrderDate";
            this.ItemForOrderDate.Size = new System.Drawing.Size(460, 24);
            this.ItemForOrderDate.Text = "Order Date:";
            this.ItemForOrderDate.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForOrderStatusID
            // 
            this.ItemForOrderStatusID.Control = this.OrderStatusIDGridLookUpEdit;
            this.ItemForOrderStatusID.CustomizationFormText = "Order Status:";
            this.ItemForOrderStatusID.Location = new System.Drawing.Point(459, 48);
            this.ItemForOrderStatusID.Name = "ItemForOrderStatusID";
            this.ItemForOrderStatusID.Size = new System.Drawing.Size(460, 24);
            this.ItemForOrderStatusID.Text = "Order Status:";
            this.ItemForOrderStatusID.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForEstimatedDeliveryDate
            // 
            this.ItemForEstimatedDeliveryDate.Control = this.EstimatedDeliveryDateDateEdit;
            this.ItemForEstimatedDeliveryDate.CustomizationFormText = "Estimated Delivery Date:";
            this.ItemForEstimatedDeliveryDate.Location = new System.Drawing.Point(0, 72);
            this.ItemForEstimatedDeliveryDate.Name = "ItemForEstimatedDeliveryDate";
            this.ItemForEstimatedDeliveryDate.Size = new System.Drawing.Size(459, 24);
            this.ItemForEstimatedDeliveryDate.Text = "Estimated Delivery Date:";
            this.ItemForEstimatedDeliveryDate.TextSize = new System.Drawing.Size(122, 13);
            // 
            // ItemForstrStaffName
            // 
            this.ItemForstrStaffName.Control = this.strStaffNameTextEdit;
            this.ItemForstrStaffName.CustomizationFormText = "Recorded By Name:";
            this.ItemForstrStaffName.Location = new System.Drawing.Point(459, 72);
            this.ItemForstrStaffName.Name = "ItemForstrStaffName";
            this.ItemForstrStaffName.Size = new System.Drawing.Size(460, 24);
            this.ItemForstrStaffName.Text = "Recorded By Name:";
            this.ItemForstrStaffName.TextSize = new System.Drawing.Size(122, 13);
            // 
            // sp04224_GC_Salt_Order_EditTableAdapter
            // 
            this.sp04224_GC_Salt_Order_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04226_GC_Grit_Order_Status_List_With_BlankTableAdapter
            // 
            this.sp04226_GC_Grit_Order_Status_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04227_GC_Salt_Order_Line_EditTableAdapter
            // 
            this.sp04227_GC_Salt_Order_Line_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter
            // 
            this.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04218_GC_Salt_Colours_List_With_BlankTableAdapter
            // 
            this.sp04218_GC_Salt_Colours_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Salt_Order_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(956, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Salt_Order_Edit";
            this.Text = "Edit Salt Order";
            this.Activated += new System.EventHandler(this.frm_GC_Salt_Order_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Salt_Order_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Salt_Order_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GritSupplierIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04224GCSaltOrderEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConversionValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCPONumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOrderHeaderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlacedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitCostIncVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitCostExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostIncVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostIncVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalAmountConvertedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04227GCSaltOrderLineEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSaltColourOrdered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04218GCSaltColoursListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAmountOrderedDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04209GCSaltUnitConversionListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAmountDeliveredDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditDeliverToLocationName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditRedirectFromLocationName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherCostVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedDeliveryDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedDeliveryDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04226GCGritOrderStatusListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritSupplierNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOrderHeaderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlacedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConversionValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSupplierID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCostExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCostVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOtherCostIncVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostIncVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitCostExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitCostIncVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalAmountConverted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritSupplierName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedDeliveryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrStaffName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritSupplierName;
        private DevExpress.XtraEditors.GridLookUpEdit OrderStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOrderStatusID;
        private DevExpress.XtraEditors.TextEdit strStaffNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrStaffName;
        private DevExpress.XtraEditors.DateEdit EstimatedDeliveryDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedDeliveryDate;
        private DevExpress.XtraEditors.DateEdit OrderDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOrderDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraEditors.TextEdit PlacedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlacedByStaffID;
        private DevExpress.XtraEditors.TextEdit GritOrderHeaderIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritOrderHeaderID;
        private DevExpress.XtraEditors.SpinEdit OtherCostVatRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOtherCostVatRate;
        private DevExpress.XtraEditors.SpinEdit OtherCostExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOtherCostExVat;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraEditors.SpinEdit TotalCostExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCostExVat;
        private DevExpress.XtraEditors.SpinEdit TotalCostIncVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCostIncVat;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2DP;
        private System.Windows.Forms.BindingSource sp04224GCSaltOrderEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04224_GC_Salt_Order_EditTableAdapter sp04224_GC_Salt_Order_EditTableAdapter;
        private System.Windows.Forms.BindingSource sp04226GCGritOrderStatusListWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderStatusOrder;
        private DataSet_GC_DataEntryTableAdapters.sp04226_GC_Grit_Order_Status_List_With_BlankTableAdapter sp04226_GC_Grit_Order_Status_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.SpinEdit TotalAmountConvertedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalAmountConverted;
        private DevExpress.XtraEditors.SpinEdit OtherCostIncVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOtherCostIncVat;
        private DevExpress.XtraEditors.SpinEdit UnitCostExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitCostExVat;
        private DevExpress.XtraEditors.SpinEdit UnitCostIncVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitCostIncVat;
        private DevExpress.XtraEditors.ButtonEdit GCPONumberButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGCPONumber;
        private DevExpress.XtraEditors.SpinEdit ConversionValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForConversionValue;
        private System.Windows.Forms.BindingSource sp04227GCSaltOrderLineEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOrderLineID;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOrderHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliverToLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliverToLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectFromLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectFromLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltColourOrdered;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditSaltColourOrdered;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountOrdered;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountOrderedDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountOrderedConverted;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDeliveryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDeliveryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountDelivered;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountDeliveredDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountDeliveredConverted;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltColourDelivered;
        private DevExpress.XtraGrid.Columns.GridColumn colVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitCostExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCostExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitCostIncVat;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCostIncVat;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountOrderedConversionFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountDeliveredConversionFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliverToLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliverToLocationType;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectFromLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectFromLocationType;
        private DevExpress.XtraGrid.Columns.GridColumn colGCPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderDate;
        private DataSet_GC_DataEntryTableAdapters.sp04227_GC_Salt_Order_Line_EditTableAdapter sp04227_GC_Salt_Order_Line_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditAmountOrderedDescriptorID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KgBags;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditAmountDeliveredDescriptorID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditDeliverToLocationName;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditRedirectFromLocationName;
        private System.Windows.Forms.BindingSource sp04209GCSaltUnitConversionListWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter sp04209_GC_Salt_Unit_Conversion_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colConversionValue;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUnitConversionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUnitDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private System.Windows.Forms.BindingSource sp04218GCSaltColoursListWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04218_GC_Salt_Colours_List_With_BlankTableAdapter sp04218_GC_Salt_Colours_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCode;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.ButtonEdit GritSupplierNameButtonEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.TextEdit GritSupplierIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritSupplierID;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

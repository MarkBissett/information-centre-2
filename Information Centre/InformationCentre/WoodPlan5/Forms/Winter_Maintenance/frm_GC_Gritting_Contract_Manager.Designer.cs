namespace WoodPlan5
{
    partial class frm_GC_Gritting_Contract_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Gritting_Contract_Manager));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.popupContainerEditCompanies = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlCompanies = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCompanyFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp04237GCCompanyFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerEdit3 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlSites = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp04057GCSiteFilterDropDownJustGritingSitesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK3 = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlShowTabPages = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp04022CoreDummyTabPageListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTabPageName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnOK2 = new DevExpress.XtraEditors.SimpleButton();
            this.LoadContractorsBtn = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlLinkedGrittingCalloutsFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04001GCJobCallOutStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnGritCalloutFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEditLinkedRecordType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ShowActiveOnlyCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04037GCSiteGrittingContractManagerListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteGrittingContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractManagerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientProactivePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colClientReactivePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientChargedForSalt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSaltPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedRecordCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colGritOnMonday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnTuesday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnWednesday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnThursday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnFriday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnSaturday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOnSunday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForecastTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingActivationCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingActivationCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingForecastTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumTemperature = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultGritAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTeamProactiveRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamReactiveRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientEveningRateModifier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGrittingNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultNoAccessRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrioritySite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBandingEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBandingStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedOverridesBanding = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsFloatingSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingCompletionEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingTimeOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMinutes = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSiteOpenTime1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSiteClosedTime1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteOpenTime2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteClosedTime2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingCompletionEmailLinkedPictures = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnnualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessRestrictions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnnual_Contract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstPreferredContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDontInvoiceClientReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumPictureCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeasonPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeasonPeriodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActiveClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultProactiveRate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultReactiveRate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp04042GCPrefferedGrittingTeamsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPreferredSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGrittingContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreferrenceOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultProactiveRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDefaultReactiveRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteGrittingContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentCostConverted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditVatRate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPDADefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobileTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemHyperLinkEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp04047GCPrefferedGrittingTeamExtraCostsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colExtraCostID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreferredSubContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCostTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGrittingContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamAddressLine11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreferrenceOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGrittingContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChargedPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colNumberOfUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSnowOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingTimeToDeduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextHours2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.dataSet_EP = new WoodPlan5.DataSet_EP();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp04022_Core_Dummy_TabPageListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter();
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter();
            this.sp04037_GC_Site_Gritting_Contract_Manager_ListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04037_GC_Site_Gritting_Contract_Manager_ListTableAdapter();
            this.sp04042_GC_Preffered_Gritting_TeamsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04042_GC_Preffered_Gritting_TeamsTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp04047_GC_Preffered_Gritting_Team_Extra_CostsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04047_GC_Preffered_Gritting_Team_Extra_CostsTableAdapter();
            this.sp04057_GC_Site_Filter_DropDown_Just_Griting_SitesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04057_GC_Site_Filter_DropDown_Just_Griting_SitesTableAdapter();
            this.sp04237_GC_Company_Filter_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending7 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCompanies.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).BeginInit();
            this.popupContainerControlCompanies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSites)).BeginInit();
            this.popupContainerControlSites.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04057GCSiteFilterDropDownJustGritingSitesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).BeginInit();
            this.popupContainerControlShowTabPages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).BeginInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLinkedRecordType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowActiveOnlyCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04037GCSiteGrittingContractManagerListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04042GCPrefferedGrittingTeamsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04047GCPrefferedGrittingTeamExtraCostsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextHours2DP)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1155, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 691);
            this.barDockControlBottom.Size = new System.Drawing.Size(1155, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 691);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1155, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 691);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 6;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Gritting Contracts";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1155, 691);
            this.splitContainerControl1.SplitterPosition = 286;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlCompanies);
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlSites);
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlShowTabPages);
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlLinkedGrittingCalloutsFilter);
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1151, 375);
            this.splitContainerControl2.SplitterPosition = 30;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.popupContainerEditCompanies);
            this.layoutControl1.Controls.Add(this.popupContainerEdit3);
            this.layoutControl1.Controls.Add(this.popupContainerEdit1);
            this.layoutControl1.Controls.Add(this.LoadContractorsBtn);
            this.layoutControl1.Controls.Add(this.popupContainerEdit2);
            this.layoutControl1.Controls.Add(this.comboBoxEditLinkedRecordType);
            this.layoutControl1.Controls.Add(this.ShowActiveOnlyCheckEdit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(454, 215, 250, 350);
            this.layoutControl1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1151, 30);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // popupContainerEditCompanies
            // 
            this.popupContainerEditCompanies.EditValue = "No Company Filter";
            this.popupContainerEditCompanies.Location = new System.Drawing.Point(981, 4);
            this.popupContainerEditCompanies.MenuManager = this.barManager1;
            this.popupContainerEditCompanies.Name = "popupContainerEditCompanies";
            this.popupContainerEditCompanies.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditCompanies.Properties.PopupControl = this.popupContainerControlCompanies;
            this.popupContainerEditCompanies.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditCompanies.Size = new System.Drawing.Size(68, 20);
            this.popupContainerEditCompanies.StyleController = this.layoutControl1;
            this.popupContainerEditCompanies.TabIndex = 14;
            this.popupContainerEditCompanies.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditCompanies_QueryResultValue);
            // 
            // popupContainerControlCompanies
            // 
            this.popupContainerControlCompanies.Controls.Add(this.btnCompanyFilterOK);
            this.popupContainerControlCompanies.Controls.Add(this.gridControl6);
            this.popupContainerControlCompanies.Location = new System.Drawing.Point(856, 12);
            this.popupContainerControlCompanies.Name = "popupContainerControlCompanies";
            this.popupContainerControlCompanies.Size = new System.Drawing.Size(189, 163);
            this.popupContainerControlCompanies.TabIndex = 16;
            // 
            // btnCompanyFilterOK
            // 
            this.btnCompanyFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCompanyFilterOK.Location = new System.Drawing.Point(3, 138);
            this.btnCompanyFilterOK.Name = "btnCompanyFilterOK";
            this.btnCompanyFilterOK.Size = new System.Drawing.Size(77, 22);
            this.btnCompanyFilterOK.TabIndex = 18;
            this.btnCompanyFilterOK.Text = "OK";
            this.btnCompanyFilterOK.Click += new System.EventHandler(this.btnCompanyFilterOK_Click);
            // 
            // gridControl6
            // 
            this.gridControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl6.DataSource = this.sp04237GCCompanyFilterListBindingSource;
            this.gridControl6.Location = new System.Drawing.Point(3, 3);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.Size = new System.Drawing.Size(183, 133);
            this.gridControl6.TabIndex = 1;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp04237GCCompanyFilterListBindingSource
            // 
            this.sp04237GCCompanyFilterListBindingSource.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7,
            this.colCompanyCode,
            this.colCompanyOrder});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Company ID";
            this.gridColumn6.FieldName = "CompanyID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 80;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Company Name";
            this.gridColumn7.FieldName = "CompanyName";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            this.gridColumn7.Width = 152;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyOrder
            // 
            this.colCompanyOrder.Caption = "Order";
            this.colCompanyOrder.FieldName = "CompanyOrder";
            this.colCompanyOrder.Name = "colCompanyOrder";
            this.colCompanyOrder.OptionsColumn.AllowEdit = false;
            this.colCompanyOrder.OptionsColumn.AllowFocus = false;
            this.colCompanyOrder.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerEdit3
            // 
            this.popupContainerEdit3.EditValue = "No Site Filter";
            this.popupContainerEdit3.Location = new System.Drawing.Point(283, 4);
            this.popupContainerEdit3.MenuManager = this.barManager1;
            this.popupContainerEdit3.Name = "popupContainerEdit3";
            this.popupContainerEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit3.Properties.PopupControl = this.popupContainerControlSites;
            this.popupContainerEdit3.Size = new System.Drawing.Size(177, 20);
            this.popupContainerEdit3.StyleController = this.layoutControl1;
            this.popupContainerEdit3.TabIndex = 12;
            this.popupContainerEdit3.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit3_QueryResultValue);
            // 
            // popupContainerControlSites
            // 
            this.popupContainerControlSites.Controls.Add(this.gridControl8);
            this.popupContainerControlSites.Controls.Add(this.btnOK3);
            this.popupContainerControlSites.Location = new System.Drawing.Point(3, 59);
            this.popupContainerControlSites.Name = "popupContainerControlSites";
            this.popupContainerControlSites.Size = new System.Drawing.Size(251, 245);
            this.popupContainerControlSites.TabIndex = 7;
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp04057GCSiteFilterDropDownJustGritingSitesBindingSource;
            this.gridControl8.Location = new System.Drawing.Point(4, 3);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.Size = new System.Drawing.Size(245, 215);
            this.gridControl8.TabIndex = 1;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp04057GCSiteFilterDropDownJustGritingSitesBindingSource
            // 
            this.sp04057GCSiteFilterDropDownJustGritingSitesBindingSource.DataMember = "sp04057_GC_Site_Filter_DropDown_Just_Griting_Sites";
            this.sp04057GCSiteFilterDropDownJustGritingSitesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.colClientCode,
            this.colSiteCode,
            this.gridColumn4,
            this.colSiteTypeDescription,
            this.colSiteTypeID,
            this.colContactPerson,
            this.gridColumn5,
            this.colXCoordinate,
            this.colYCoordinate});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 1;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Client ID";
            this.gridColumn1.FieldName = "ClientID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Site ID";
            this.gridColumn2.FieldName = "SiteID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Client Name";
            this.gridColumn3.FieldName = "ClientName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 130;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Visible = true;
            this.colClientCode.VisibleIndex = 5;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Site Name";
            this.gridColumn4.FieldName = "SiteName";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 244;
            // 
            // colSiteTypeDescription
            // 
            this.colSiteTypeDescription.Caption = "Site Type";
            this.colSiteTypeDescription.FieldName = "SiteTypeDescription";
            this.colSiteTypeDescription.Name = "colSiteTypeDescription";
            this.colSiteTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSiteTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSiteTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSiteTypeDescription.Visible = true;
            this.colSiteTypeDescription.VisibleIndex = 2;
            this.colSiteTypeDescription.Width = 129;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colContactPerson
            // 
            this.colContactPerson.Caption = "Contact Person";
            this.colContactPerson.FieldName = "ContactPerson";
            this.colContactPerson.Name = "colContactPerson";
            this.colContactPerson.OptionsColumn.AllowEdit = false;
            this.colContactPerson.OptionsColumn.AllowFocus = false;
            this.colContactPerson.OptionsColumn.ReadOnly = true;
            this.colContactPerson.Visible = true;
            this.colContactPerson.VisibleIndex = 3;
            this.colContactPerson.Width = 137;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Address Line 1";
            this.gridColumn5.FieldName = "SiteAddressLine1";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 152;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // btnOK3
            // 
            this.btnOK3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK3.Location = new System.Drawing.Point(3, 220);
            this.btnOK3.Name = "btnOK3";
            this.btnOK3.Size = new System.Drawing.Size(75, 23);
            this.btnOK3.TabIndex = 0;
            this.btnOK3.Text = "OK";
            this.btnOK3.Click += new System.EventHandler(this.btnOK3_Click);
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "No Related Data";
            this.popupContainerEdit1.Location = new System.Drawing.Point(118, 4);
            this.popupContainerEdit1.MenuManager = this.barManager1;
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControlShowTabPages;
            this.popupContainerEdit1.Size = new System.Drawing.Size(131, 20);
            this.popupContainerEdit1.StyleController = this.layoutControl1;
            this.popupContainerEdit1.TabIndex = 11;
            this.popupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            // 
            // popupContainerControlShowTabPages
            // 
            this.popupContainerControlShowTabPages.Controls.Add(this.gridControl4);
            this.popupContainerControlShowTabPages.Controls.Add(this.btnOK2);
            this.popupContainerControlShowTabPages.Location = new System.Drawing.Point(257, 62);
            this.popupContainerControlShowTabPages.Name = "popupContainerControlShowTabPages";
            this.popupContainerControlShowTabPages.Size = new System.Drawing.Size(217, 245);
            this.popupContainerControlShowTabPages.TabIndex = 6;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp04022CoreDummyTabPageListBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 3);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit7});
            this.gridControl4.Size = new System.Drawing.Size(211, 215);
            this.gridControl4.TabIndex = 1;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp04022CoreDummyTabPageListBindingSource
            // 
            this.sp04022CoreDummyTabPageListBindingSource.DataMember = "sp04022_Core_Dummy_TabPageList";
            this.sp04022CoreDummyTabPageListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTabPageName});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colTabPageName
            // 
            this.colTabPageName.Caption = "Linked Records";
            this.colTabPageName.FieldName = "TabPageName";
            this.colTabPageName.Name = "colTabPageName";
            this.colTabPageName.OptionsColumn.AllowEdit = false;
            this.colTabPageName.OptionsColumn.AllowFocus = false;
            this.colTabPageName.OptionsColumn.ReadOnly = true;
            this.colTabPageName.Visible = true;
            this.colTabPageName.VisibleIndex = 0;
            this.colTabPageName.Width = 179;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Caption = "Check";
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // btnOK2
            // 
            this.btnOK2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK2.Location = new System.Drawing.Point(3, 220);
            this.btnOK2.Name = "btnOK2";
            this.btnOK2.Size = new System.Drawing.Size(75, 23);
            this.btnOK2.TabIndex = 0;
            this.btnOK2.Text = "OK";
            this.btnOK2.Click += new System.EventHandler(this.btnOK2_Click);
            // 
            // LoadContractorsBtn
            // 
            this.LoadContractorsBtn.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.LoadContractorsBtn.Location = new System.Drawing.Point(1053, 4);
            this.LoadContractorsBtn.Name = "LoadContractorsBtn";
            this.LoadContractorsBtn.Size = new System.Drawing.Size(94, 22);
            this.LoadContractorsBtn.StyleController = this.layoutControl1;
            this.LoadContractorsBtn.TabIndex = 10;
            this.LoadContractorsBtn.Text = "Refresh";
            this.LoadContractorsBtn.Click += new System.EventHandler(this.LoadContractorsBtn_Click);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Callout Status Filter";
            this.popupContainerEdit2.Location = new System.Drawing.Point(863, 4);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControlLinkedGrittingCalloutsFilter;
            this.popupContainerEdit2.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit2.Size = new System.Drawing.Size(62, 20);
            this.popupContainerEdit2.StyleController = this.layoutControl1;
            this.popupContainerEdit2.TabIndex = 7;
            this.popupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit2_QueryResultValue);
            // 
            // popupContainerControlLinkedGrittingCalloutsFilter
            // 
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.layoutControl2);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.btnGritCalloutFilterOK);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Location = new System.Drawing.Point(477, 62);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Name = "popupContainerControlLinkedGrittingCalloutsFilter";
            this.popupContainerControlLinkedGrittingCalloutsFilter.Size = new System.Drawing.Size(340, 245);
            this.popupContainerControlLinkedGrittingCalloutsFilter.TabIndex = 5;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.Controls.Add(this.gridControl5);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1185, 135, 250, 350);
            this.layoutControl2.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(340, 220);
            this.layoutControl2.TabIndex = 12;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp04001GCJobCallOutStatusesBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(12, 32);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(316, 152);
            this.gridControl5.TabIndex = 4;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04001GCJobCallOutStatusesBindingSource
            // 
            this.sp04001GCJobCallOutStatusesBindingSource.DataMember = "sp04001_GC_Job_CallOut_Statuses";
            this.sp04001GCJobCallOutStatusesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colValue,
            this.colOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Callout Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 263;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(216, 188);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all ca" +
    "llouts will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(112, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 11;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(69, 188);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all ca" +
    "llouts will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(98, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(340, 220);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Linked Gritting Callouts - Data Filter";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(336, 216);
            this.layoutControlGroup3.Text = "Linked Gritting Callouts - Data Filter";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl5;
            this.layoutControlItem3.CustomizationFormText = "Callout Job Status Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(320, 156);
            this.layoutControlItem3.Text = "Callout Job Status Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateEditFromDate;
            this.layoutControlItem4.CustomizationFormText = "From Date:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(159, 24);
            this.layoutControlItem4.Text = "From Date:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dateEditToDate;
            this.layoutControlItem5.CustomizationFormText = "To Date:";
            this.layoutControlItem5.Location = new System.Drawing.Point(159, 156);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem5.Text = "To Date:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(42, 13);
            // 
            // btnGritCalloutFilterOK
            // 
            this.btnGritCalloutFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGritCalloutFilterOK.Location = new System.Drawing.Point(3, 220);
            this.btnGritCalloutFilterOK.Name = "btnGritCalloutFilterOK";
            this.btnGritCalloutFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnGritCalloutFilterOK.TabIndex = 12;
            this.btnGritCalloutFilterOK.Text = "OK";
            this.btnGritCalloutFilterOK.Click += new System.EventHandler(this.btnGritCalloutFilterOK_Click);
            // 
            // comboBoxEditLinkedRecordType
            // 
            this.comboBoxEditLinkedRecordType.Location = new System.Drawing.Point(680, 4);
            this.comboBoxEditLinkedRecordType.MenuManager = this.barManager1;
            this.comboBoxEditLinkedRecordType.Name = "comboBoxEditLinkedRecordType";
            this.comboBoxEditLinkedRecordType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditLinkedRecordType.Properties.Items.AddRange(new object[] {
            "No Linked Records",
            "Gritting Callouts"});
            this.comboBoxEditLinkedRecordType.Size = new System.Drawing.Size(105, 20);
            this.comboBoxEditLinkedRecordType.StyleController = this.layoutControl1;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem3.Text = "Linked Record Type - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I control the values calculated and shown in the Linked Records hyperlink column " +
    "of the Site grid.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.comboBoxEditLinkedRecordType.SuperTip = superToolTip3;
            this.comboBoxEditLinkedRecordType.TabIndex = 9;
            this.comboBoxEditLinkedRecordType.SelectedValueChanged += new System.EventHandler(this.comboBoxEditLinkedRecordType_SelectedValueChanged);
            // 
            // ShowActiveOnlyCheckEdit
            // 
            this.ShowActiveOnlyCheckEdit.Location = new System.Drawing.Point(464, 4);
            this.ShowActiveOnlyCheckEdit.MenuManager = this.barManager1;
            this.ShowActiveOnlyCheckEdit.Name = "ShowActiveOnlyCheckEdit";
            this.ShowActiveOnlyCheckEdit.Properties.Caption = "Show Active Only:";
            this.ShowActiveOnlyCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ShowActiveOnlyCheckEdit.Properties.ValueChecked = 1;
            this.ShowActiveOnlyCheckEdit.Properties.ValueUnchecked = 0;
            this.ShowActiveOnlyCheckEdit.Size = new System.Drawing.Size(111, 19);
            this.ShowActiveOnlyCheckEdit.StyleController = this.layoutControl1;
            this.ShowActiveOnlyCheckEdit.TabIndex = 13;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem10});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1151, 30);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.comboBoxEditLinkedRecordType;
            this.layoutControlItem1.CustomizationFormText = "Linked Record Type:";
            this.layoutControlItem1.Location = new System.Drawing.Point(575, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(210, 26);
            this.layoutControlItem1.Text = "Linked Record Type:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.popupContainerEdit2;
            this.layoutControlItem2.CustomizationFormText = "Callout Status:";
            this.layoutControlItem2.Location = new System.Drawing.Point(785, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(140, 26);
            this.layoutControlItem2.Text = "Callout Status:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.LoadContractorsBtn;
            this.layoutControlItem6.CustomizationFormText = "Load Contractors Button";
            this.layoutControlItem6.Location = new System.Drawing.Point(1049, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(98, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(98, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(98, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.popupContainerEdit1;
            this.layoutControlItem7.CustomizationFormText = "Linked Data To Show:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(249, 26);
            this.layoutControlItem7.Text = "Related Data To Show:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.ShowActiveOnlyCheckEdit;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(460, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(115, 0);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(115, 23);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(115, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.popupContainerEdit3;
            this.layoutControlItem8.CustomizationFormText = "Sites:";
            this.layoutControlItem8.Location = new System.Drawing.Point(249, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(211, 26);
            this.layoutControlItem8.Text = "Sites:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(27, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.popupContainerEditCompanies;
            this.layoutControlItem10.CustomizationFormText = "Company:";
            this.layoutControlItem10.Location = new System.Drawing.Point(925, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(124, 26);
            this.layoutControlItem10.Text = "Company:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(49, 13);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp04037GCSiteGrittingContractManagerListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditMinutes,
            this.repositoryItemTextEditTime});
            this.gridControl1.Size = new System.Drawing.Size(1151, 339);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04037GCSiteGrittingContractManagerListBindingSource
            // 
            this.sp04037GCSiteGrittingContractManagerListBindingSource.DataMember = "sp04037_GC_Site_Gritting_Contract_Manager_List";
            this.sp04037GCSiteGrittingContractManagerListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteGrittingContractID,
            this.colSiteID,
            this.colSiteName,
            this.colSiteAddressLine1,
            this.colSiteXCoordinate,
            this.colSiteYCoordinate,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colClientID,
            this.colClientName,
            this.colContractManagerID,
            this.colContractManagerName,
            this.colStartDate1,
            this.colEndDate1,
            this.colActive1,
            this.colArea,
            this.colProactive,
            this.colReactive,
            this.colClientProactivePrice,
            this.colClientReactivePrice,
            this.colClientChargedForSalt,
            this.colClientSaltPrice,
            this.colRemarks,
            this.colLinkedRecordCount,
            this.colGritOnMonday,
            this.colGritOnTuesday,
            this.colGritOnWednesday,
            this.colGritOnThursday,
            this.colGritOnFriday,
            this.colGritOnSaturday,
            this.colGritOnSunday,
            this.colForecastTypeID,
            this.colGrittingActivationCode,
            this.colGrittingActivationCodeID,
            this.colGrittingForecastTypeName,
            this.colMinimumTemperature,
            this.colDefaultGritAmount,
            this.colTeamProactiveRate,
            this.colTeamReactiveRate,
            this.colSiteAddressLine2,
            this.colSiteAddressLine3,
            this.colSiteAddressLine4,
            this.colSiteAddressLine5,
            this.colSitePostcode,
            this.colClientEveningRateModifier,
            this.colCompanyName,
            this.colSiteGrittingNotes,
            this.colDefaultNoAccessRate,
            this.colPrioritySite,
            this.colBandingEnd,
            this.colBandingStart,
            this.colRedOverridesBanding,
            this.colIsFloatingSite,
            this.colGrittingCompletionEmail,
            this.colGrittingTimeOnSite,
            this.colSiteOpenTime1,
            this.colSiteClosedTime1,
            this.colSiteOpenTime2,
            this.colSiteClosedTime2,
            this.colGrittingCompletionEmailLinkedPictures,
            this.colAnnualCost,
            this.colAccessRestrictions,
            this.colAnnual_Contract,
            this.colSiteCode1,
            this.colClientsSiteCode,
            this.colFirstPreferredContractor,
            this.colInvoiceClient,
            this.colDontInvoiceClientReason,
            this.colMinimumPictureCount,
            this.colSeasonPeriod,
            this.colSeasonPeriodID,
            this.colActiveClientPONumber,
            this.colDefaultProactiveRate1,
            this.colDefaultReactiveRate1});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive1;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSiteGrittingContractID
            // 
            this.colSiteGrittingContractID.Caption = "Site Gritting Contract ID";
            this.colSiteGrittingContractID.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID.Name = "colSiteGrittingContractID";
            this.colSiteGrittingContractID.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID.Width = 136;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 150;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Site Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 47;
            this.colSiteAddressLine1.Width = 121;
            // 
            // colSiteXCoordinate
            // 
            this.colSiteXCoordinate.Caption = "Site X Coordinate";
            this.colSiteXCoordinate.FieldName = "SiteXCoordinate";
            this.colSiteXCoordinate.Name = "colSiteXCoordinate";
            this.colSiteXCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteXCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteXCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteXCoordinate.Width = 104;
            // 
            // colSiteYCoordinate
            // 
            this.colSiteYCoordinate.Caption = "Site Y Coordinate";
            this.colSiteYCoordinate.FieldName = "SiteYCoordinate";
            this.colSiteYCoordinate.Name = "colSiteYCoordinate";
            this.colSiteYCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteYCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteYCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteYCoordinate.Width = 104;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Location X";
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 91;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Location Y";
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 91;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 145;
            // 
            // colContractManagerID
            // 
            this.colContractManagerID.Caption = "Contract Manager ID";
            this.colContractManagerID.FieldName = "ContractManagerID";
            this.colContractManagerID.Name = "colContractManagerID";
            this.colContractManagerID.OptionsColumn.AllowEdit = false;
            this.colContractManagerID.OptionsColumn.AllowFocus = false;
            this.colContractManagerID.OptionsColumn.ReadOnly = true;
            this.colContractManagerID.Width = 122;
            // 
            // colContractManagerName
            // 
            this.colContractManagerName.Caption = "Contract Manager";
            this.colContractManagerName.FieldName = "ContractManagerName";
            this.colContractManagerName.Name = "colContractManagerName";
            this.colContractManagerName.OptionsColumn.AllowEdit = false;
            this.colContractManagerName.OptionsColumn.AllowFocus = false;
            this.colContractManagerName.OptionsColumn.ReadOnly = true;
            this.colContractManagerName.Visible = true;
            this.colContractManagerName.VisibleIndex = 8;
            this.colContractManagerName.Width = 108;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 3;
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 4;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area";
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            this.colArea.Visible = true;
            this.colArea.VisibleIndex = 43;
            // 
            // colProactive
            // 
            this.colProactive.Caption = "Proactive";
            this.colProactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colProactive.FieldName = "Proactive";
            this.colProactive.Name = "colProactive";
            this.colProactive.OptionsColumn.AllowEdit = false;
            this.colProactive.OptionsColumn.AllowFocus = false;
            this.colProactive.OptionsColumn.ReadOnly = true;
            this.colProactive.Visible = true;
            this.colProactive.VisibleIndex = 9;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 10;
            // 
            // colClientProactivePrice
            // 
            this.colClientProactivePrice.Caption = "Client Proactive Price";
            this.colClientProactivePrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colClientProactivePrice.FieldName = "ClientProactivePrice";
            this.colClientProactivePrice.Name = "colClientProactivePrice";
            this.colClientProactivePrice.OptionsColumn.AllowEdit = false;
            this.colClientProactivePrice.OptionsColumn.AllowFocus = false;
            this.colClientProactivePrice.OptionsColumn.ReadOnly = true;
            this.colClientProactivePrice.Visible = true;
            this.colClientProactivePrice.VisibleIndex = 36;
            this.colClientProactivePrice.Width = 122;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colClientReactivePrice
            // 
            this.colClientReactivePrice.Caption = "Client Reactive Price";
            this.colClientReactivePrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colClientReactivePrice.FieldName = "ClientReactivePrice";
            this.colClientReactivePrice.Name = "colClientReactivePrice";
            this.colClientReactivePrice.OptionsColumn.AllowEdit = false;
            this.colClientReactivePrice.OptionsColumn.AllowFocus = false;
            this.colClientReactivePrice.OptionsColumn.ReadOnly = true;
            this.colClientReactivePrice.Visible = true;
            this.colClientReactivePrice.VisibleIndex = 37;
            this.colClientReactivePrice.Width = 119;
            // 
            // colClientChargedForSalt
            // 
            this.colClientChargedForSalt.Caption = "Client Charged for Salt";
            this.colClientChargedForSalt.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colClientChargedForSalt.FieldName = "ClientChargedForSalt";
            this.colClientChargedForSalt.Name = "colClientChargedForSalt";
            this.colClientChargedForSalt.OptionsColumn.AllowEdit = false;
            this.colClientChargedForSalt.OptionsColumn.AllowFocus = false;
            this.colClientChargedForSalt.OptionsColumn.ReadOnly = true;
            this.colClientChargedForSalt.Visible = true;
            this.colClientChargedForSalt.VisibleIndex = 42;
            this.colClientChargedForSalt.Width = 130;
            // 
            // colClientSaltPrice
            // 
            this.colClientSaltPrice.Caption = "Client Salt Price";
            this.colClientSaltPrice.ColumnEdit = this.repositoryItemTextEdit1;
            this.colClientSaltPrice.FieldName = "ClientSaltPrice";
            this.colClientSaltPrice.Name = "colClientSaltPrice";
            this.colClientSaltPrice.OptionsColumn.AllowEdit = false;
            this.colClientSaltPrice.OptionsColumn.AllowFocus = false;
            this.colClientSaltPrice.OptionsColumn.ReadOnly = true;
            this.colClientSaltPrice.Visible = true;
            this.colClientSaltPrice.VisibleIndex = 26;
            this.colClientSaltPrice.Width = 95;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 46;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLinkedRecordCount
            // 
            this.colLinkedRecordCount.Caption = "Linked Record Count";
            this.colLinkedRecordCount.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colLinkedRecordCount.FieldName = "LinkedRecordCount";
            this.colLinkedRecordCount.Name = "colLinkedRecordCount";
            this.colLinkedRecordCount.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordCount.Visible = true;
            this.colLinkedRecordCount.VisibleIndex = 45;
            this.colLinkedRecordCount.Width = 120;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colGritOnMonday
            // 
            this.colGritOnMonday.Caption = "Monday Grit";
            this.colGritOnMonday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnMonday.FieldName = "GritOnMonday";
            this.colGritOnMonday.Name = "colGritOnMonday";
            this.colGritOnMonday.OptionsColumn.AllowEdit = false;
            this.colGritOnMonday.OptionsColumn.AllowFocus = false;
            this.colGritOnMonday.OptionsColumn.ReadOnly = true;
            this.colGritOnMonday.Visible = true;
            this.colGritOnMonday.VisibleIndex = 29;
            this.colGritOnMonday.Width = 79;
            // 
            // colGritOnTuesday
            // 
            this.colGritOnTuesday.Caption = "Tuesday Grit";
            this.colGritOnTuesday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnTuesday.FieldName = "GritOnTuesday";
            this.colGritOnTuesday.Name = "colGritOnTuesday";
            this.colGritOnTuesday.OptionsColumn.AllowEdit = false;
            this.colGritOnTuesday.OptionsColumn.AllowFocus = false;
            this.colGritOnTuesday.OptionsColumn.ReadOnly = true;
            this.colGritOnTuesday.Visible = true;
            this.colGritOnTuesday.VisibleIndex = 30;
            this.colGritOnTuesday.Width = 82;
            // 
            // colGritOnWednesday
            // 
            this.colGritOnWednesday.Caption = "Wednesday Grit";
            this.colGritOnWednesday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnWednesday.FieldName = "GritOnWednesday";
            this.colGritOnWednesday.Name = "colGritOnWednesday";
            this.colGritOnWednesday.OptionsColumn.AllowEdit = false;
            this.colGritOnWednesday.OptionsColumn.AllowFocus = false;
            this.colGritOnWednesday.OptionsColumn.ReadOnly = true;
            this.colGritOnWednesday.Visible = true;
            this.colGritOnWednesday.VisibleIndex = 31;
            this.colGritOnWednesday.Width = 98;
            // 
            // colGritOnThursday
            // 
            this.colGritOnThursday.Caption = "Thursday Grit";
            this.colGritOnThursday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnThursday.FieldName = "GritOnThursday";
            this.colGritOnThursday.Name = "colGritOnThursday";
            this.colGritOnThursday.OptionsColumn.AllowEdit = false;
            this.colGritOnThursday.OptionsColumn.AllowFocus = false;
            this.colGritOnThursday.OptionsColumn.ReadOnly = true;
            this.colGritOnThursday.Visible = true;
            this.colGritOnThursday.VisibleIndex = 32;
            this.colGritOnThursday.Width = 86;
            // 
            // colGritOnFriday
            // 
            this.colGritOnFriday.Caption = "Friday Grit";
            this.colGritOnFriday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnFriday.FieldName = "GritOnFriday";
            this.colGritOnFriday.Name = "colGritOnFriday";
            this.colGritOnFriday.OptionsColumn.AllowEdit = false;
            this.colGritOnFriday.OptionsColumn.AllowFocus = false;
            this.colGritOnFriday.OptionsColumn.ReadOnly = true;
            this.colGritOnFriday.Visible = true;
            this.colGritOnFriday.VisibleIndex = 33;
            // 
            // colGritOnSaturday
            // 
            this.colGritOnSaturday.Caption = "Saturday Grit";
            this.colGritOnSaturday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnSaturday.FieldName = "GritOnSaturday";
            this.colGritOnSaturday.Name = "colGritOnSaturday";
            this.colGritOnSaturday.OptionsColumn.AllowEdit = false;
            this.colGritOnSaturday.OptionsColumn.AllowFocus = false;
            this.colGritOnSaturday.OptionsColumn.ReadOnly = true;
            this.colGritOnSaturday.Visible = true;
            this.colGritOnSaturday.VisibleIndex = 35;
            this.colGritOnSaturday.Width = 85;
            // 
            // colGritOnSunday
            // 
            this.colGritOnSunday.Caption = "Sunday Grit";
            this.colGritOnSunday.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGritOnSunday.FieldName = "GritOnSunday";
            this.colGritOnSunday.Name = "colGritOnSunday";
            this.colGritOnSunday.OptionsColumn.AllowEdit = false;
            this.colGritOnSunday.OptionsColumn.AllowFocus = false;
            this.colGritOnSunday.OptionsColumn.ReadOnly = true;
            this.colGritOnSunday.Visible = true;
            this.colGritOnSunday.VisibleIndex = 34;
            this.colGritOnSunday.Width = 77;
            // 
            // colForecastTypeID
            // 
            this.colForecastTypeID.Caption = "Forecast Type ID";
            this.colForecastTypeID.FieldName = "ForecastTypeID";
            this.colForecastTypeID.Name = "colForecastTypeID";
            this.colForecastTypeID.OptionsColumn.AllowEdit = false;
            this.colForecastTypeID.OptionsColumn.AllowFocus = false;
            this.colForecastTypeID.OptionsColumn.ReadOnly = true;
            this.colForecastTypeID.Width = 107;
            // 
            // colGrittingActivationCode
            // 
            this.colGrittingActivationCode.Caption = "Weather Activation Point";
            this.colGrittingActivationCode.FieldName = "GrittingActivationCode";
            this.colGrittingActivationCode.Name = "colGrittingActivationCode";
            this.colGrittingActivationCode.OptionsColumn.AllowEdit = false;
            this.colGrittingActivationCode.OptionsColumn.AllowFocus = false;
            this.colGrittingActivationCode.OptionsColumn.ReadOnly = true;
            this.colGrittingActivationCode.Visible = true;
            this.colGrittingActivationCode.VisibleIndex = 17;
            this.colGrittingActivationCode.Width = 146;
            // 
            // colGrittingActivationCodeID
            // 
            this.colGrittingActivationCodeID.Caption = "Weather Activation Point ID";
            this.colGrittingActivationCodeID.FieldName = "GrittingActivationCodeID";
            this.colGrittingActivationCodeID.Name = "colGrittingActivationCodeID";
            this.colGrittingActivationCodeID.OptionsColumn.AllowEdit = false;
            this.colGrittingActivationCodeID.OptionsColumn.AllowFocus = false;
            this.colGrittingActivationCodeID.OptionsColumn.ReadOnly = true;
            this.colGrittingActivationCodeID.Width = 163;
            // 
            // colGrittingForecastTypeName
            // 
            this.colGrittingForecastTypeName.Caption = "Forecast Type";
            this.colGrittingForecastTypeName.FieldName = "GrittingForecastTypeName";
            this.colGrittingForecastTypeName.Name = "colGrittingForecastTypeName";
            this.colGrittingForecastTypeName.OptionsColumn.AllowEdit = false;
            this.colGrittingForecastTypeName.OptionsColumn.AllowFocus = false;
            this.colGrittingForecastTypeName.OptionsColumn.ReadOnly = true;
            this.colGrittingForecastTypeName.Visible = true;
            this.colGrittingForecastTypeName.VisibleIndex = 19;
            this.colGrittingForecastTypeName.Width = 128;
            // 
            // colMinimumTemperature
            // 
            this.colMinimumTemperature.Caption = "Min Temperature";
            this.colMinimumTemperature.FieldName = "MinimumTemperature";
            this.colMinimumTemperature.Name = "colMinimumTemperature";
            this.colMinimumTemperature.OptionsColumn.AllowEdit = false;
            this.colMinimumTemperature.OptionsColumn.AllowFocus = false;
            this.colMinimumTemperature.OptionsColumn.ReadOnly = true;
            this.colMinimumTemperature.Visible = true;
            this.colMinimumTemperature.VisibleIndex = 18;
            this.colMinimumTemperature.Width = 102;
            // 
            // colDefaultGritAmount
            // 
            this.colDefaultGritAmount.Caption = "Default Grit Amount";
            this.colDefaultGritAmount.ColumnEdit = this.repositoryItemTextEdit4;
            this.colDefaultGritAmount.FieldName = "DefaultGritAmount";
            this.colDefaultGritAmount.Name = "colDefaultGritAmount";
            this.colDefaultGritAmount.OptionsColumn.AllowEdit = false;
            this.colDefaultGritAmount.OptionsColumn.AllowFocus = false;
            this.colDefaultGritAmount.OptionsColumn.ReadOnly = true;
            this.colDefaultGritAmount.Visible = true;
            this.colDefaultGritAmount.VisibleIndex = 44;
            this.colDefaultGritAmount.Width = 116;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "######0.00  25kg Bags";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // colTeamProactiveRate
            // 
            this.colTeamProactiveRate.Caption = "Team Proactive Rate";
            this.colTeamProactiveRate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colTeamProactiveRate.FieldName = "TeamProactiveRate";
            this.colTeamProactiveRate.Name = "colTeamProactiveRate";
            this.colTeamProactiveRate.OptionsColumn.AllowEdit = false;
            this.colTeamProactiveRate.OptionsColumn.AllowFocus = false;
            this.colTeamProactiveRate.OptionsColumn.ReadOnly = true;
            this.colTeamProactiveRate.Visible = true;
            this.colTeamProactiveRate.VisibleIndex = 39;
            this.colTeamProactiveRate.Width = 121;
            // 
            // colTeamReactiveRate
            // 
            this.colTeamReactiveRate.Caption = "Team Reactive Rate";
            this.colTeamReactiveRate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colTeamReactiveRate.FieldName = "TeamReactiveRate";
            this.colTeamReactiveRate.Name = "colTeamReactiveRate";
            this.colTeamReactiveRate.OptionsColumn.AllowEdit = false;
            this.colTeamReactiveRate.OptionsColumn.AllowFocus = false;
            this.colTeamReactiveRate.OptionsColumn.ReadOnly = true;
            this.colTeamReactiveRate.Visible = true;
            this.colTeamReactiveRate.VisibleIndex = 40;
            this.colTeamReactiveRate.Width = 118;
            // 
            // colSiteAddressLine2
            // 
            this.colSiteAddressLine2.Caption = "Site Address Line 2";
            this.colSiteAddressLine2.FieldName = "SiteAddressLine2";
            this.colSiteAddressLine2.Name = "colSiteAddressLine2";
            this.colSiteAddressLine2.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine2.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine2.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine2.Visible = true;
            this.colSiteAddressLine2.VisibleIndex = 48;
            this.colSiteAddressLine2.Width = 112;
            // 
            // colSiteAddressLine3
            // 
            this.colSiteAddressLine3.Caption = "Site Address Line 3";
            this.colSiteAddressLine3.FieldName = "SiteAddressLine3";
            this.colSiteAddressLine3.Name = "colSiteAddressLine3";
            this.colSiteAddressLine3.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine3.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine3.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine3.Visible = true;
            this.colSiteAddressLine3.VisibleIndex = 49;
            this.colSiteAddressLine3.Width = 112;
            // 
            // colSiteAddressLine4
            // 
            this.colSiteAddressLine4.Caption = "Site Address Line 4";
            this.colSiteAddressLine4.FieldName = "SiteAddressLine4";
            this.colSiteAddressLine4.Name = "colSiteAddressLine4";
            this.colSiteAddressLine4.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine4.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine4.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine4.Visible = true;
            this.colSiteAddressLine4.VisibleIndex = 50;
            this.colSiteAddressLine4.Width = 112;
            // 
            // colSiteAddressLine5
            // 
            this.colSiteAddressLine5.Caption = "Site Address Line 5";
            this.colSiteAddressLine5.FieldName = "SiteAddressLine5";
            this.colSiteAddressLine5.Name = "colSiteAddressLine5";
            this.colSiteAddressLine5.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine5.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine5.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine5.Visible = true;
            this.colSiteAddressLine5.VisibleIndex = 51;
            this.colSiteAddressLine5.Width = 112;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Visible = true;
            this.colSitePostcode.VisibleIndex = 52;
            this.colSitePostcode.Width = 86;
            // 
            // colClientEveningRateModifier
            // 
            this.colClientEveningRateModifier.Caption = "Client Evening Rate Modifier";
            this.colClientEveningRateModifier.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colClientEveningRateModifier.FieldName = "ClientEveningRateModifier";
            this.colClientEveningRateModifier.Name = "colClientEveningRateModifier";
            this.colClientEveningRateModifier.OptionsColumn.AllowEdit = false;
            this.colClientEveningRateModifier.OptionsColumn.AllowFocus = false;
            this.colClientEveningRateModifier.OptionsColumn.ReadOnly = true;
            this.colClientEveningRateModifier.Visible = true;
            this.colClientEveningRateModifier.VisibleIndex = 38;
            this.colClientEveningRateModifier.Width = 156;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 7;
            this.colCompanyName.Width = 95;
            // 
            // colSiteGrittingNotes
            // 
            this.colSiteGrittingNotes.Caption = "Site Gritting Notes";
            this.colSiteGrittingNotes.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteGrittingNotes.FieldName = "SiteGrittingNotes";
            this.colSiteGrittingNotes.Name = "colSiteGrittingNotes";
            this.colSiteGrittingNotes.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingNotes.Visible = true;
            this.colSiteGrittingNotes.VisibleIndex = 58;
            this.colSiteGrittingNotes.Width = 108;
            // 
            // colDefaultNoAccessRate
            // 
            this.colDefaultNoAccessRate.Caption = "Team No Access Rate";
            this.colDefaultNoAccessRate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDefaultNoAccessRate.FieldName = "DefaultNoAccessRate";
            this.colDefaultNoAccessRate.Name = "colDefaultNoAccessRate";
            this.colDefaultNoAccessRate.OptionsColumn.AllowEdit = false;
            this.colDefaultNoAccessRate.OptionsColumn.AllowFocus = false;
            this.colDefaultNoAccessRate.OptionsColumn.ReadOnly = true;
            this.colDefaultNoAccessRate.Visible = true;
            this.colDefaultNoAccessRate.VisibleIndex = 41;
            this.colDefaultNoAccessRate.Width = 125;
            // 
            // colPrioritySite
            // 
            this.colPrioritySite.Caption = "Priority Site";
            this.colPrioritySite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPrioritySite.FieldName = "PrioritySite";
            this.colPrioritySite.Name = "colPrioritySite";
            this.colPrioritySite.OptionsColumn.AllowEdit = false;
            this.colPrioritySite.OptionsColumn.AllowFocus = false;
            this.colPrioritySite.OptionsColumn.ReadOnly = true;
            this.colPrioritySite.Visible = true;
            this.colPrioritySite.VisibleIndex = 12;
            this.colPrioritySite.Width = 76;
            // 
            // colBandingEnd
            // 
            this.colBandingEnd.Caption = "Banding Start";
            this.colBandingEnd.FieldName = "BandingEnd";
            this.colBandingEnd.Name = "colBandingEnd";
            this.colBandingEnd.OptionsColumn.AllowEdit = false;
            this.colBandingEnd.OptionsColumn.AllowFocus = false;
            this.colBandingEnd.OptionsColumn.ReadOnly = true;
            this.colBandingEnd.Visible = true;
            this.colBandingEnd.VisibleIndex = 20;
            this.colBandingEnd.Width = 86;
            // 
            // colBandingStart
            // 
            this.colBandingStart.Caption = "Banding End";
            this.colBandingStart.FieldName = "BandingStart";
            this.colBandingStart.Name = "colBandingStart";
            this.colBandingStart.OptionsColumn.AllowEdit = false;
            this.colBandingStart.OptionsColumn.AllowFocus = false;
            this.colBandingStart.OptionsColumn.ReadOnly = true;
            this.colBandingStart.Visible = true;
            this.colBandingStart.VisibleIndex = 21;
            this.colBandingStart.Width = 80;
            // 
            // colRedOverridesBanding
            // 
            this.colRedOverridesBanding.Caption = "Red Overrides Banding";
            this.colRedOverridesBanding.FieldName = "RedOverridesBanding";
            this.colRedOverridesBanding.Name = "colRedOverridesBanding";
            this.colRedOverridesBanding.OptionsColumn.AllowEdit = false;
            this.colRedOverridesBanding.OptionsColumn.AllowFocus = false;
            this.colRedOverridesBanding.OptionsColumn.ReadOnly = true;
            this.colRedOverridesBanding.Visible = true;
            this.colRedOverridesBanding.VisibleIndex = 22;
            this.colRedOverridesBanding.Width = 131;
            // 
            // colIsFloatingSite
            // 
            this.colIsFloatingSite.Caption = "Is Floating Site";
            this.colIsFloatingSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsFloatingSite.FieldName = "IsFloatingSite";
            this.colIsFloatingSite.Name = "colIsFloatingSite";
            this.colIsFloatingSite.OptionsColumn.AllowEdit = false;
            this.colIsFloatingSite.OptionsColumn.AllowFocus = false;
            this.colIsFloatingSite.OptionsColumn.ReadOnly = true;
            this.colIsFloatingSite.Visible = true;
            this.colIsFloatingSite.VisibleIndex = 13;
            this.colIsFloatingSite.Width = 92;
            // 
            // colGrittingCompletionEmail
            // 
            this.colGrittingCompletionEmail.Caption = "Gritting Completion Email";
            this.colGrittingCompletionEmail.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGrittingCompletionEmail.FieldName = "GrittingCompletionEmail";
            this.colGrittingCompletionEmail.Name = "colGrittingCompletionEmail";
            this.colGrittingCompletionEmail.OptionsColumn.AllowEdit = false;
            this.colGrittingCompletionEmail.OptionsColumn.AllowFocus = false;
            this.colGrittingCompletionEmail.OptionsColumn.ReadOnly = true;
            this.colGrittingCompletionEmail.Visible = true;
            this.colGrittingCompletionEmail.VisibleIndex = 23;
            this.colGrittingCompletionEmail.Width = 139;
            // 
            // colGrittingTimeOnSite
            // 
            this.colGrittingTimeOnSite.Caption = "Gritting Time on Site";
            this.colGrittingTimeOnSite.ColumnEdit = this.repositoryItemTextEditMinutes;
            this.colGrittingTimeOnSite.FieldName = "GrittingTimeOnSite";
            this.colGrittingTimeOnSite.Name = "colGrittingTimeOnSite";
            this.colGrittingTimeOnSite.OptionsColumn.AllowEdit = false;
            this.colGrittingTimeOnSite.OptionsColumn.AllowFocus = false;
            this.colGrittingTimeOnSite.OptionsColumn.ReadOnly = true;
            this.colGrittingTimeOnSite.Visible = true;
            this.colGrittingTimeOnSite.VisibleIndex = 53;
            this.colGrittingTimeOnSite.Width = 117;
            // 
            // repositoryItemTextEditMinutes
            // 
            this.repositoryItemTextEditMinutes.AutoHeight = false;
            this.repositoryItemTextEditMinutes.Mask.EditMask = "#####0.00 Minutes";
            this.repositoryItemTextEditMinutes.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMinutes.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMinutes.Name = "repositoryItemTextEditMinutes";
            // 
            // colSiteOpenTime1
            // 
            this.colSiteOpenTime1.Caption = "Site Open 1";
            this.colSiteOpenTime1.ColumnEdit = this.repositoryItemTextEditTime;
            this.colSiteOpenTime1.FieldName = "SiteOpenTime1";
            this.colSiteOpenTime1.Name = "colSiteOpenTime1";
            this.colSiteOpenTime1.OptionsColumn.AllowEdit = false;
            this.colSiteOpenTime1.OptionsColumn.AllowFocus = false;
            this.colSiteOpenTime1.OptionsColumn.ReadOnly = true;
            this.colSiteOpenTime1.Visible = true;
            this.colSiteOpenTime1.VisibleIndex = 54;
            this.colSiteOpenTime1.Width = 77;
            // 
            // repositoryItemTextEditTime
            // 
            this.repositoryItemTextEditTime.AutoHeight = false;
            this.repositoryItemTextEditTime.Mask.EditMask = "t";
            this.repositoryItemTextEditTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditTime.Name = "repositoryItemTextEditTime";
            // 
            // colSiteClosedTime1
            // 
            this.colSiteClosedTime1.Caption = "Site Closed 1";
            this.colSiteClosedTime1.ColumnEdit = this.repositoryItemTextEditTime;
            this.colSiteClosedTime1.FieldName = "SiteClosedTime1";
            this.colSiteClosedTime1.Name = "colSiteClosedTime1";
            this.colSiteClosedTime1.OptionsColumn.AllowEdit = false;
            this.colSiteClosedTime1.OptionsColumn.AllowFocus = false;
            this.colSiteClosedTime1.OptionsColumn.ReadOnly = true;
            this.colSiteClosedTime1.Visible = true;
            this.colSiteClosedTime1.VisibleIndex = 55;
            this.colSiteClosedTime1.Width = 83;
            // 
            // colSiteOpenTime2
            // 
            this.colSiteOpenTime2.Caption = "Site Open 2";
            this.colSiteOpenTime2.ColumnEdit = this.repositoryItemTextEditTime;
            this.colSiteOpenTime2.FieldName = "SiteOpenTime2";
            this.colSiteOpenTime2.Name = "colSiteOpenTime2";
            this.colSiteOpenTime2.OptionsColumn.AllowEdit = false;
            this.colSiteOpenTime2.OptionsColumn.AllowFocus = false;
            this.colSiteOpenTime2.OptionsColumn.ReadOnly = true;
            this.colSiteOpenTime2.Visible = true;
            this.colSiteOpenTime2.VisibleIndex = 56;
            this.colSiteOpenTime2.Width = 77;
            // 
            // colSiteClosedTime2
            // 
            this.colSiteClosedTime2.Caption = "Site Closed 2";
            this.colSiteClosedTime2.ColumnEdit = this.repositoryItemTextEditTime;
            this.colSiteClosedTime2.FieldName = "SiteClosedTime2";
            this.colSiteClosedTime2.Name = "colSiteClosedTime2";
            this.colSiteClosedTime2.OptionsColumn.AllowEdit = false;
            this.colSiteClosedTime2.OptionsColumn.AllowFocus = false;
            this.colSiteClosedTime2.OptionsColumn.ReadOnly = true;
            this.colSiteClosedTime2.Visible = true;
            this.colSiteClosedTime2.VisibleIndex = 57;
            this.colSiteClosedTime2.Width = 83;
            // 
            // colGrittingCompletionEmailLinkedPictures
            // 
            this.colGrittingCompletionEmailLinkedPictures.Caption = "Completion Email - Linked Pictures";
            this.colGrittingCompletionEmailLinkedPictures.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGrittingCompletionEmailLinkedPictures.FieldName = "GrittingCompletionEmailLinkedPictures";
            this.colGrittingCompletionEmailLinkedPictures.Name = "colGrittingCompletionEmailLinkedPictures";
            this.colGrittingCompletionEmailLinkedPictures.OptionsColumn.AllowEdit = false;
            this.colGrittingCompletionEmailLinkedPictures.OptionsColumn.AllowFocus = false;
            this.colGrittingCompletionEmailLinkedPictures.OptionsColumn.ReadOnly = true;
            this.colGrittingCompletionEmailLinkedPictures.Visible = true;
            this.colGrittingCompletionEmailLinkedPictures.VisibleIndex = 24;
            this.colGrittingCompletionEmailLinkedPictures.Width = 182;
            // 
            // colAnnualCost
            // 
            this.colAnnualCost.Caption = "Annual Cost";
            this.colAnnualCost.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAnnualCost.FieldName = "AnnualCost";
            this.colAnnualCost.Name = "colAnnualCost";
            this.colAnnualCost.OptionsColumn.AllowEdit = false;
            this.colAnnualCost.OptionsColumn.AllowFocus = false;
            this.colAnnualCost.OptionsColumn.ReadOnly = true;
            this.colAnnualCost.Visible = true;
            this.colAnnualCost.VisibleIndex = 25;
            this.colAnnualCost.Width = 83;
            // 
            // colAccessRestrictions
            // 
            this.colAccessRestrictions.Caption = "Access Restrictions";
            this.colAccessRestrictions.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAccessRestrictions.FieldName = "AccessRestrictions";
            this.colAccessRestrictions.Name = "colAccessRestrictions";
            this.colAccessRestrictions.OptionsColumn.AllowEdit = false;
            this.colAccessRestrictions.OptionsColumn.AllowFocus = false;
            this.colAccessRestrictions.OptionsColumn.ReadOnly = true;
            this.colAccessRestrictions.Visible = true;
            this.colAccessRestrictions.VisibleIndex = 59;
            this.colAccessRestrictions.Width = 111;
            // 
            // colAnnual_Contract
            // 
            this.colAnnual_Contract.Caption = "Annual Contract";
            this.colAnnual_Contract.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAnnual_Contract.FieldName = "Annual_Contract";
            this.colAnnual_Contract.Name = "colAnnual_Contract";
            this.colAnnual_Contract.OptionsColumn.AllowEdit = false;
            this.colAnnual_Contract.OptionsColumn.AllowFocus = false;
            this.colAnnual_Contract.OptionsColumn.ReadOnly = true;
            this.colAnnual_Contract.Visible = true;
            this.colAnnual_Contract.VisibleIndex = 11;
            this.colAnnual_Contract.Width = 97;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "Site Code";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            this.colSiteCode1.Visible = true;
            this.colSiteCode1.VisibleIndex = 1;
            this.colSiteCode1.Width = 65;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Visible = true;
            this.colClientsSiteCode.VisibleIndex = 2;
            this.colClientsSiteCode.Width = 100;
            // 
            // colFirstPreferredContractor
            // 
            this.colFirstPreferredContractor.Caption = "First Preferred Team";
            this.colFirstPreferredContractor.FieldName = "FirstPreferredContractor";
            this.colFirstPreferredContractor.Name = "colFirstPreferredContractor";
            this.colFirstPreferredContractor.OptionsColumn.AllowEdit = false;
            this.colFirstPreferredContractor.OptionsColumn.AllowFocus = false;
            this.colFirstPreferredContractor.OptionsColumn.ReadOnly = true;
            this.colFirstPreferredContractor.Visible = true;
            this.colFirstPreferredContractor.VisibleIndex = 14;
            this.colFirstPreferredContractor.Width = 118;
            // 
            // colInvoiceClient
            // 
            this.colInvoiceClient.Caption = "Invoice Client";
            this.colInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colInvoiceClient.FieldName = "InvoiceClient";
            this.colInvoiceClient.Name = "colInvoiceClient";
            this.colInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colInvoiceClient.Visible = true;
            this.colInvoiceClient.VisibleIndex = 27;
            this.colInvoiceClient.Width = 84;
            // 
            // colDontInvoiceClientReason
            // 
            this.colDontInvoiceClientReason.Caption = "Don\'t Invoice Site Reason";
            this.colDontInvoiceClientReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDontInvoiceClientReason.FieldName = "DontInvoiceClientReason";
            this.colDontInvoiceClientReason.Name = "colDontInvoiceClientReason";
            this.colDontInvoiceClientReason.OptionsColumn.ReadOnly = true;
            this.colDontInvoiceClientReason.Visible = true;
            this.colDontInvoiceClientReason.VisibleIndex = 28;
            this.colDontInvoiceClientReason.Width = 142;
            // 
            // colMinimumPictureCount
            // 
            this.colMinimumPictureCount.Caption = "Min Picture Count";
            this.colMinimumPictureCount.FieldName = "MinimumPictureCount";
            this.colMinimumPictureCount.Name = "colMinimumPictureCount";
            this.colMinimumPictureCount.OptionsColumn.AllowEdit = false;
            this.colMinimumPictureCount.OptionsColumn.AllowFocus = false;
            this.colMinimumPictureCount.OptionsColumn.ReadOnly = true;
            this.colMinimumPictureCount.Visible = true;
            this.colMinimumPictureCount.VisibleIndex = 60;
            this.colMinimumPictureCount.Width = 103;
            // 
            // colSeasonPeriod
            // 
            this.colSeasonPeriod.Caption = "Season Period";
            this.colSeasonPeriod.FieldName = "SeasonPeriod";
            this.colSeasonPeriod.Name = "colSeasonPeriod";
            this.colSeasonPeriod.Visible = true;
            this.colSeasonPeriod.VisibleIndex = 5;
            this.colSeasonPeriod.Width = 87;
            // 
            // colSeasonPeriodID
            // 
            this.colSeasonPeriodID.Caption = "Season Period ID";
            this.colSeasonPeriodID.FieldName = "SeasonPeriodID";
            this.colSeasonPeriodID.Name = "colSeasonPeriodID";
            this.colSeasonPeriodID.OptionsColumn.AllowEdit = false;
            this.colSeasonPeriodID.OptionsColumn.AllowFocus = false;
            this.colSeasonPeriodID.OptionsColumn.ReadOnly = true;
            this.colSeasonPeriodID.Width = 101;
            // 
            // colActiveClientPONumber
            // 
            this.colActiveClientPONumber.Caption = "Active Client PO #";
            this.colActiveClientPONumber.FieldName = "ActiveClientPONumber";
            this.colActiveClientPONumber.Name = "colActiveClientPONumber";
            this.colActiveClientPONumber.OptionsColumn.AllowEdit = false;
            this.colActiveClientPONumber.OptionsColumn.AllowFocus = false;
            this.colActiveClientPONumber.OptionsColumn.ReadOnly = true;
            this.colActiveClientPONumber.Visible = true;
            this.colActiveClientPONumber.VisibleIndex = 61;
            this.colActiveClientPONumber.Width = 107;
            // 
            // colDefaultProactiveRate1
            // 
            this.colDefaultProactiveRate1.Caption = "First Team Proactive Rate";
            this.colDefaultProactiveRate1.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDefaultProactiveRate1.FieldName = "DefaultProactiveRate";
            this.colDefaultProactiveRate1.Name = "colDefaultProactiveRate1";
            this.colDefaultProactiveRate1.OptionsColumn.AllowEdit = false;
            this.colDefaultProactiveRate1.OptionsColumn.AllowFocus = false;
            this.colDefaultProactiveRate1.OptionsColumn.ReadOnly = true;
            this.colDefaultProactiveRate1.Visible = true;
            this.colDefaultProactiveRate1.VisibleIndex = 15;
            this.colDefaultProactiveRate1.Width = 143;
            // 
            // colDefaultReactiveRate1
            // 
            this.colDefaultReactiveRate1.Caption = "First Team Reactive Rate";
            this.colDefaultReactiveRate1.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDefaultReactiveRate1.FieldName = "DefaultReactiveRate";
            this.colDefaultReactiveRate1.Name = "colDefaultReactiveRate1";
            this.colDefaultReactiveRate1.OptionsColumn.AllowEdit = false;
            this.colDefaultReactiveRate1.OptionsColumn.AllowFocus = false;
            this.colDefaultReactiveRate1.OptionsColumn.ReadOnly = true;
            this.colDefaultReactiveRate1.Visible = true;
            this.colDefaultReactiveRate1.VisibleIndex = 16;
            this.colDefaultReactiveRate1.Width = 140;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1155, 286);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1150, 260);
            this.xtraTabPage1.Text = "Preferred Teams";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl3.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl3.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControl2);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "Preferred Teams";
            this.splitContainerControl3.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl3.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl3.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl3.Panel2.Controls.Add(this.gridControl7);
            this.splitContainerControl3.Panel2.ShowCaption = true;
            this.splitContainerControl3.Panel2.Text = "Preferred Team - Default Extra Costs";
            this.splitContainerControl3.Size = new System.Drawing.Size(1150, 260);
            this.splitContainerControl3.SplitterPosition = 587;
            this.splitContainerControl3.TabIndex = 1;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp04042GCPrefferedGrittingTeamsBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Move Item Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Move Item Down", "down"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Preserve Sort Order", "set_order")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckEdit4,
            this.repositoryItemHyperLinkEdit4,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditVatRate});
            this.gridControl2.Size = new System.Drawing.Size(532, 257);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp04042GCPrefferedGrittingTeamsBindingSource
            // 
            this.sp04042GCPrefferedGrittingTeamsBindingSource.DataMember = "sp04042_GC_Preffered_Gritting_Teams";
            this.sp04042GCPrefferedGrittingTeamsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPreferredSubContractorID,
            this.colSiteGrittingContractID1,
            this.colSiteName1,
            this.colClientName1,
            this.colSubContractorID1,
            this.colTeamName,
            this.colTeamAddressLine1,
            this.colPreferrenceOrder,
            this.colDefaultProactiveRate,
            this.colDefaultReactiveRate,
            this.colRemarks1,
            this.colSiteGrittingContractDescription1,
            this.colCurrentCostConverted,
            this.colSaltVatRate,
            this.colPDADefault,
            this.colMobileTel,
            this.colTelephone1,
            this.colTelephone2,
            this.colWebSite});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteGrittingContractDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPreferrenceOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView2_CustomRowCellEdit);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView2_ShowingEditor);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colPreferredSubContractorID
            // 
            this.colPreferredSubContractorID.Caption = "Preferred Team ID";
            this.colPreferredSubContractorID.FieldName = "PreferredSubContractorID";
            this.colPreferredSubContractorID.Name = "colPreferredSubContractorID";
            this.colPreferredSubContractorID.OptionsColumn.AllowEdit = false;
            this.colPreferredSubContractorID.OptionsColumn.AllowFocus = false;
            this.colPreferredSubContractorID.OptionsColumn.ReadOnly = true;
            this.colPreferredSubContractorID.Width = 110;
            // 
            // colSiteGrittingContractID1
            // 
            this.colSiteGrittingContractID1.Caption = "Site Gritting Contract ID";
            this.colSiteGrittingContractID1.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID1.Name = "colSiteGrittingContractID1";
            this.colSiteGrittingContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID1.Width = 136;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 8;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 231;
            // 
            // colSubContractorID1
            // 
            this.colSubContractorID1.Caption = "Team ID";
            this.colSubContractorID1.FieldName = "SubContractorID";
            this.colSubContractorID1.Name = "colSubContractorID1";
            this.colSubContractorID1.OptionsColumn.AllowEdit = false;
            this.colSubContractorID1.OptionsColumn.AllowFocus = false;
            this.colSubContractorID1.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName
            // 
            this.colTeamName.Caption = "Team Name";
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.Visible = true;
            this.colTeamName.VisibleIndex = 0;
            this.colTeamName.Width = 243;
            // 
            // colTeamAddressLine1
            // 
            this.colTeamAddressLine1.Caption = "Team Address Line 1";
            this.colTeamAddressLine1.FieldName = "TeamAddressLine1";
            this.colTeamAddressLine1.Name = "colTeamAddressLine1";
            this.colTeamAddressLine1.OptionsColumn.AllowEdit = false;
            this.colTeamAddressLine1.OptionsColumn.AllowFocus = false;
            this.colTeamAddressLine1.OptionsColumn.ReadOnly = true;
            this.colTeamAddressLine1.Visible = true;
            this.colTeamAddressLine1.VisibleIndex = 1;
            this.colTeamAddressLine1.Width = 134;
            // 
            // colPreferrenceOrder
            // 
            this.colPreferrenceOrder.Caption = "Order";
            this.colPreferrenceOrder.FieldName = "PreferrenceOrder";
            this.colPreferrenceOrder.Name = "colPreferrenceOrder";
            this.colPreferrenceOrder.OptionsColumn.AllowEdit = false;
            this.colPreferrenceOrder.OptionsColumn.AllowFocus = false;
            this.colPreferrenceOrder.OptionsColumn.ReadOnly = true;
            this.colPreferrenceOrder.Visible = true;
            this.colPreferrenceOrder.VisibleIndex = 2;
            this.colPreferrenceOrder.Width = 60;
            // 
            // colDefaultProactiveRate
            // 
            this.colDefaultProactiveRate.Caption = "Default Proactive Rate";
            this.colDefaultProactiveRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colDefaultProactiveRate.FieldName = "DefaultProactiveRate";
            this.colDefaultProactiveRate.Name = "colDefaultProactiveRate";
            this.colDefaultProactiveRate.OptionsColumn.AllowEdit = false;
            this.colDefaultProactiveRate.OptionsColumn.AllowFocus = false;
            this.colDefaultProactiveRate.OptionsColumn.ReadOnly = true;
            this.colDefaultProactiveRate.Visible = true;
            this.colDefaultProactiveRate.VisibleIndex = 4;
            this.colDefaultProactiveRate.Width = 130;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colDefaultReactiveRate
            // 
            this.colDefaultReactiveRate.Caption = "Default Reactive Rate";
            this.colDefaultReactiveRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colDefaultReactiveRate.FieldName = "DefaultReactiveRate";
            this.colDefaultReactiveRate.Name = "colDefaultReactiveRate";
            this.colDefaultReactiveRate.OptionsColumn.AllowEdit = false;
            this.colDefaultReactiveRate.OptionsColumn.AllowFocus = false;
            this.colDefaultReactiveRate.OptionsColumn.ReadOnly = true;
            this.colDefaultReactiveRate.Visible = true;
            this.colDefaultReactiveRate.VisibleIndex = 5;
            this.colDefaultReactiveRate.Width = 127;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colSiteGrittingContractDescription1
            // 
            this.colSiteGrittingContractDescription1.Caption = "Site Gritting Contract";
            this.colSiteGrittingContractDescription1.FieldName = "SiteGrittingContractDescription";
            this.colSiteGrittingContractDescription1.Name = "colSiteGrittingContractDescription1";
            this.colSiteGrittingContractDescription1.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractDescription1.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractDescription1.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractDescription1.Width = 379;
            // 
            // colCurrentCostConverted
            // 
            this.colCurrentCostConverted.Caption = "Salt Cost per 25 Kg Bag";
            this.colCurrentCostConverted.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCurrentCostConverted.FieldName = "CurrentCostConverted";
            this.colCurrentCostConverted.Name = "colCurrentCostConverted";
            this.colCurrentCostConverted.OptionsColumn.AllowEdit = false;
            this.colCurrentCostConverted.OptionsColumn.AllowFocus = false;
            this.colCurrentCostConverted.OptionsColumn.ReadOnly = true;
            this.colCurrentCostConverted.Visible = true;
            this.colCurrentCostConverted.VisibleIndex = 6;
            this.colCurrentCostConverted.Width = 134;
            // 
            // colSaltVatRate
            // 
            this.colSaltVatRate.Caption = "Salt VAT Rate";
            this.colSaltVatRate.ColumnEdit = this.repositoryItemTextEditVatRate;
            this.colSaltVatRate.FieldName = "SaltVatRate";
            this.colSaltVatRate.Name = "colSaltVatRate";
            this.colSaltVatRate.OptionsColumn.AllowEdit = false;
            this.colSaltVatRate.OptionsColumn.AllowFocus = false;
            this.colSaltVatRate.OptionsColumn.ReadOnly = true;
            this.colSaltVatRate.Visible = true;
            this.colSaltVatRate.VisibleIndex = 7;
            this.colSaltVatRate.Width = 87;
            // 
            // repositoryItemTextEditVatRate
            // 
            this.repositoryItemTextEditVatRate.AutoHeight = false;
            this.repositoryItemTextEditVatRate.Mask.EditMask = "P";
            this.repositoryItemTextEditVatRate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditVatRate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditVatRate.Name = "repositoryItemTextEditVatRate";
            // 
            // colPDADefault
            // 
            this.colPDADefault.Caption = "Default Device Code";
            this.colPDADefault.FieldName = "PDADefault";
            this.colPDADefault.Name = "colPDADefault";
            this.colPDADefault.OptionsColumn.AllowEdit = false;
            this.colPDADefault.OptionsColumn.AllowFocus = false;
            this.colPDADefault.OptionsColumn.ReadOnly = true;
            this.colPDADefault.Visible = true;
            this.colPDADefault.VisibleIndex = 3;
            this.colPDADefault.Width = 107;
            // 
            // colMobileTel
            // 
            this.colMobileTel.Caption = "Mobile Tel";
            this.colMobileTel.FieldName = "MobileTel";
            this.colMobileTel.Name = "colMobileTel";
            this.colMobileTel.OptionsColumn.AllowEdit = false;
            this.colMobileTel.OptionsColumn.AllowFocus = false;
            this.colMobileTel.OptionsColumn.ReadOnly = true;
            this.colMobileTel.Visible = true;
            this.colMobileTel.VisibleIndex = 9;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Visible = true;
            this.colTelephone1.VisibleIndex = 10;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Visible = true;
            this.colTelephone2.VisibleIndex = 11;
            this.colTelephone2.Width = 80;
            // 
            // colWebSite
            // 
            this.colWebSite.Caption = "Website";
            this.colWebSite.FieldName = "WebSite";
            this.colWebSite.Name = "colWebSite";
            this.colWebSite.OptionsColumn.AllowEdit = false;
            this.colWebSite.OptionsColumn.AllowFocus = false;
            this.colWebSite.OptionsColumn.ReadOnly = true;
            this.colWebSite.Visible = true;
            this.colWebSite.VisibleIndex = 12;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // repositoryItemHyperLinkEdit4
            // 
            this.repositoryItemHyperLinkEdit4.AutoHeight = false;
            this.repositoryItemHyperLinkEdit4.Name = "repositoryItemHyperLinkEdit4";
            this.repositoryItemHyperLinkEdit4.SingleClick = true;
            this.repositoryItemHyperLinkEdit4.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit4_OpenLink);
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp04047GCPrefferedGrittingTeamExtraCostsBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEdit3,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEdit2DP2,
            this.repositoryItemTextHours2DP});
            this.gridControl7.Size = new System.Drawing.Size(562, 257);
            this.gridControl7.TabIndex = 0;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp04047GCPrefferedGrittingTeamExtraCostsBindingSource
            // 
            this.sp04047GCPrefferedGrittingTeamExtraCostsBindingSource.DataMember = "sp04047_GC_Preffered_Gritting_Team_Extra_Costs";
            this.sp04047GCPrefferedGrittingTeamExtraCostsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colExtraCostID,
            this.colPreferredSubContractorID1,
            this.colCostTypeID,
            this.colDescription2,
            this.colCost,
            this.colSell,
            this.colVatRate,
            this.colRemarks2,
            this.colCostTypeDescription,
            this.colSiteGrittingContractID2,
            this.colSiteName2,
            this.colClientName2,
            this.colSubContractorID,
            this.colTeamName1,
            this.colTeamAddressLine11,
            this.colPreferrenceOrder1,
            this.colActive,
            this.colEndDate,
            this.colStartDate,
            this.colSiteGrittingContractDescription,
            this.colChargedPerHour,
            this.colNumberOfUnits,
            this.colSnowOnSite,
            this.colGrittingTimeToDeduct});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 3;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsLayout.StoreFormatRules = true;
            this.gridView7.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteGrittingContractDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.DoubleClick += new System.EventHandler(this.gridView7_DoubleClick);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colExtraCostID
            // 
            this.colExtraCostID.Caption = "Extra Cost ID";
            this.colExtraCostID.FieldName = "ExtraCostID";
            this.colExtraCostID.Name = "colExtraCostID";
            this.colExtraCostID.OptionsColumn.AllowEdit = false;
            this.colExtraCostID.OptionsColumn.AllowFocus = false;
            this.colExtraCostID.OptionsColumn.ReadOnly = true;
            this.colExtraCostID.Width = 86;
            // 
            // colPreferredSubContractorID1
            // 
            this.colPreferredSubContractorID1.Caption = "Preferred Team ID";
            this.colPreferredSubContractorID1.FieldName = "PreferredSubContractorID";
            this.colPreferredSubContractorID1.Name = "colPreferredSubContractorID1";
            this.colPreferredSubContractorID1.OptionsColumn.AllowEdit = false;
            this.colPreferredSubContractorID1.OptionsColumn.AllowFocus = false;
            this.colPreferredSubContractorID1.OptionsColumn.ReadOnly = true;
            this.colPreferredSubContractorID1.Width = 110;
            // 
            // colCostTypeID
            // 
            this.colCostTypeID.Caption = "Cost Type ID";
            this.colCostTypeID.FieldName = "CostTypeID";
            this.colCostTypeID.Name = "colCostTypeID";
            this.colCostTypeID.OptionsColumn.AllowEdit = false;
            this.colCostTypeID.OptionsColumn.AllowFocus = false;
            this.colCostTypeID.OptionsColumn.ReadOnly = true;
            this.colCostTypeID.Width = 84;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Cost Description";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 182;
            // 
            // colCost
            // 
            this.colCost.Caption = "Cost";
            this.colCost.ColumnEdit = this.repositoryItemTextEdit3;
            this.colCost.FieldName = "Cost";
            this.colCost.Name = "colCost";
            this.colCost.OptionsColumn.AllowEdit = false;
            this.colCost.OptionsColumn.AllowFocus = false;
            this.colCost.OptionsColumn.ReadOnly = true;
            this.colCost.Visible = true;
            this.colCost.VisibleIndex = 4;
            this.colCost.Width = 53;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "c";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colSell
            // 
            this.colSell.Caption = "Cost To Client";
            this.colSell.ColumnEdit = this.repositoryItemTextEdit3;
            this.colSell.FieldName = "Sell";
            this.colSell.Name = "colSell";
            this.colSell.OptionsColumn.AllowEdit = false;
            this.colSell.OptionsColumn.AllowFocus = false;
            this.colSell.OptionsColumn.ReadOnly = true;
            this.colSell.Visible = true;
            this.colSell.VisibleIndex = 6;
            this.colSell.Width = 88;
            // 
            // colVatRate
            // 
            this.colVatRate.Caption = "VAT Rate";
            this.colVatRate.FieldName = "VatRate";
            this.colVatRate.Name = "colVatRate";
            this.colVatRate.OptionsColumn.AllowEdit = false;
            this.colVatRate.OptionsColumn.AllowFocus = false;
            this.colVatRate.OptionsColumn.ReadOnly = true;
            this.colVatRate.Visible = true;
            this.colVatRate.VisibleIndex = 7;
            this.colVatRate.Width = 65;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colCostTypeDescription
            // 
            this.colCostTypeDescription.Caption = "Cost Type";
            this.colCostTypeDescription.FieldName = "CostTypeDescription";
            this.colCostTypeDescription.Name = "colCostTypeDescription";
            this.colCostTypeDescription.OptionsColumn.AllowEdit = false;
            this.colCostTypeDescription.OptionsColumn.AllowFocus = false;
            this.colCostTypeDescription.OptionsColumn.ReadOnly = true;
            this.colCostTypeDescription.Visible = true;
            this.colCostTypeDescription.VisibleIndex = 1;
            this.colCostTypeDescription.Width = 99;
            // 
            // colSiteGrittingContractID2
            // 
            this.colSiteGrittingContractID2.Caption = "Site Gritting Contract ID";
            this.colSiteGrittingContractID2.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID2.Name = "colSiteGrittingContractID2";
            this.colSiteGrittingContractID2.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID2.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID2.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID2.Width = 136;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Width = 122;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 117;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName1
            // 
            this.colTeamName1.Caption = "Team Name";
            this.colTeamName1.FieldName = "TeamName";
            this.colTeamName1.Name = "colTeamName1";
            this.colTeamName1.OptionsColumn.AllowEdit = false;
            this.colTeamName1.OptionsColumn.AllowFocus = false;
            this.colTeamName1.OptionsColumn.ReadOnly = true;
            this.colTeamName1.Width = 137;
            // 
            // colTeamAddressLine11
            // 
            this.colTeamAddressLine11.Caption = "Address Line 1";
            this.colTeamAddressLine11.FieldName = "TeamAddressLine1";
            this.colTeamAddressLine11.Name = "colTeamAddressLine11";
            this.colTeamAddressLine11.OptionsColumn.AllowEdit = false;
            this.colTeamAddressLine11.OptionsColumn.AllowFocus = false;
            this.colTeamAddressLine11.OptionsColumn.ReadOnly = true;
            this.colTeamAddressLine11.Width = 91;
            // 
            // colPreferrenceOrder1
            // 
            this.colPreferrenceOrder1.Caption = "Preferrence Order";
            this.colPreferrenceOrder1.FieldName = "PreferrenceOrder";
            this.colPreferrenceOrder1.Name = "colPreferrenceOrder1";
            this.colPreferrenceOrder1.OptionsColumn.AllowEdit = false;
            this.colPreferrenceOrder1.OptionsColumn.AllowFocus = false;
            this.colPreferrenceOrder1.OptionsColumn.ReadOnly = true;
            this.colPreferrenceOrder1.Width = 109;
            // 
            // colActive
            // 
            this.colActive.Caption = "Contract Active";
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Width = 96;
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "Contract Start Date";
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Width = 116;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Contract End Date";
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Width = 110;
            // 
            // colSiteGrittingContractDescription
            // 
            this.colSiteGrittingContractDescription.Caption = "Site Contract";
            this.colSiteGrittingContractDescription.FieldName = "SiteGrittingContractDescription";
            this.colSiteGrittingContractDescription.Name = "colSiteGrittingContractDescription";
            this.colSiteGrittingContractDescription.Width = 193;
            // 
            // colChargedPerHour
            // 
            this.colChargedPerHour.Caption = "Charged Per Hour";
            this.colChargedPerHour.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colChargedPerHour.FieldName = "ChargedPerHour";
            this.colChargedPerHour.Name = "colChargedPerHour";
            this.colChargedPerHour.OptionsColumn.AllowEdit = false;
            this.colChargedPerHour.OptionsColumn.AllowFocus = false;
            this.colChargedPerHour.OptionsColumn.ReadOnly = true;
            this.colChargedPerHour.Visible = true;
            this.colChargedPerHour.VisibleIndex = 3;
            this.colChargedPerHour.Width = 107;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colNumberOfUnits
            // 
            this.colNumberOfUnits.Caption = "Number of Units";
            this.colNumberOfUnits.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colNumberOfUnits.FieldName = "NumberOfUnits";
            this.colNumberOfUnits.Name = "colNumberOfUnits";
            this.colNumberOfUnits.OptionsColumn.AllowEdit = false;
            this.colNumberOfUnits.OptionsColumn.AllowFocus = false;
            this.colNumberOfUnits.OptionsColumn.ReadOnly = true;
            this.colNumberOfUnits.Visible = true;
            this.colNumberOfUnits.VisibleIndex = 5;
            this.colNumberOfUnits.Width = 98;
            // 
            // repositoryItemTextEdit2DP2
            // 
            this.repositoryItemTextEdit2DP2.AutoHeight = false;
            this.repositoryItemTextEdit2DP2.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP2.Name = "repositoryItemTextEdit2DP2";
            // 
            // colSnowOnSite
            // 
            this.colSnowOnSite.Caption = "Only When Snow On Site";
            this.colSnowOnSite.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSnowOnSite.FieldName = "SnowOnSite";
            this.colSnowOnSite.Name = "colSnowOnSite";
            this.colSnowOnSite.OptionsColumn.AllowEdit = false;
            this.colSnowOnSite.OptionsColumn.AllowFocus = false;
            this.colSnowOnSite.OptionsColumn.ReadOnly = true;
            this.colSnowOnSite.Visible = true;
            this.colSnowOnSite.VisibleIndex = 2;
            this.colSnowOnSite.Width = 141;
            // 
            // colGrittingTimeToDeduct
            // 
            this.colGrittingTimeToDeduct.Caption = "Gritting Time To Deduct";
            this.colGrittingTimeToDeduct.ColumnEdit = this.repositoryItemTextHours2DP;
            this.colGrittingTimeToDeduct.FieldName = "GrittingTimeToDeduct";
            this.colGrittingTimeToDeduct.Name = "colGrittingTimeToDeduct";
            this.colGrittingTimeToDeduct.OptionsColumn.AllowEdit = false;
            this.colGrittingTimeToDeduct.OptionsColumn.AllowFocus = false;
            this.colGrittingTimeToDeduct.OptionsColumn.ReadOnly = true;
            this.colGrittingTimeToDeduct.Visible = true;
            this.colGrittingTimeToDeduct.VisibleIndex = 8;
            this.colGrittingTimeToDeduct.Width = 133;
            // 
            // repositoryItemTextHours2DP
            // 
            this.repositoryItemTextHours2DP.AutoHeight = false;
            this.repositoryItemTextHours2DP.Mask.EditMask = "##0.00 Hours";
            this.repositoryItemTextHours2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextHours2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextHours2DP.Name = "repositoryItemTextHours2DP";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1150, 260);
            this.xtraTabPage2.Text = "Linked Documents";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl3.Size = new System.Drawing.Size(1150, 260);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView3_ShowingEditor);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // dataSet_EP
            // 
            this.dataSet_EP.DataSetName = "DataSet_EP";
            this.dataSet_EP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            // 
            // sp04022_Core_Dummy_TabPageListTableAdapter
            // 
            this.sp04022_Core_Dummy_TabPageListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04001_GC_Job_CallOut_StatusesTableAdapter
            // 
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04037_GC_Site_Gritting_Contract_Manager_ListTableAdapter
            // 
            this.sp04037_GC_Site_Gritting_Contract_Manager_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04042_GC_Preffered_Gritting_TeamsTableAdapter
            // 
            this.sp04042_GC_Preffered_Gritting_TeamsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04047_GC_Preffered_Gritting_Team_Extra_CostsTableAdapter
            // 
            this.sp04047_GC_Preffered_Gritting_Team_Extra_CostsTableAdapter.ClearBeforeFill = true;
            // 
            // sp04057_GC_Site_Filter_DropDown_Just_Griting_SitesTableAdapter
            // 
            this.sp04057_GC_Site_Filter_DropDown_Just_Griting_SitesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04237_GC_Company_Filter_ListTableAdapter
            // 
            this.sp04237_GC_Company_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending7
            // 
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending7.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending7.GridControl = this.gridControl7;
            // 
            // frm_GC_Gritting_Contract_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1155, 691);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Gritting_Contract_Manager";
            this.Text = "Gritting Contract Manager";
            this.Activated += new System.EventHandler(this.frm_GC_Gritting_Contract_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Gritting_Contract_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Gritting_Contract_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCompanies.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).EndInit();
            this.popupContainerControlCompanies.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSites)).EndInit();
            this.popupContainerControlSites.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04057GCSiteFilterDropDownJustGritingSitesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).EndInit();
            this.popupContainerControlShowTabPages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).EndInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLinkedRecordType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowActiveOnlyCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04037GCSiteGrittingContractManagerListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04042GCPrefferedGrittingTeamsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04047GCPrefferedGrittingTeamExtraCostsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextHours2DP)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DataSet_AT dataSet_AT;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditLinkedRecordType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLinkedGrittingCalloutsFilter;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnGritCalloutFilterOK;
        private DevExpress.XtraEditors.SimpleButton LoadContractorsBtn;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlShowTabPages;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SimpleButton btnOK2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DataSet_GC_Core dataSet_GC_Core;
        private System.Windows.Forms.BindingSource sp04022CoreDummyTabPageListBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter sp04022_Core_Dummy_TabPageListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTabPageName;
        private System.Windows.Forms.BindingSource sp04001GCJobCallOutStatusesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter sp04001_GC_Job_CallOut_StatusesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSites;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.SimpleButton btnOK3;
        private System.Windows.Forms.BindingSource sp04037GCSiteGrittingContractManagerListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractManagerID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraGrid.Columns.GridColumn colProactive;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colClientProactivePrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientReactivePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colClientChargedForSalt;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSaltPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordCount;
        private DataSet_GC_CoreTableAdapters.sp04037_GC_Site_Gritting_Contract_Manager_ListTableAdapter sp04037_GC_Site_Gritting_Contract_Manager_ListTableAdapter;
        private DataSet_EP dataSet_EP;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPerson;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private System.Windows.Forms.BindingSource sp04042GCPrefferedGrittingTeamsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPreferredSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colPreferrenceOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultProactiveRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultReactiveRate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DataSet_GC_CoreTableAdapters.sp04042_GC_Preffered_Gritting_TeamsTableAdapter sp04042_GC_Preffered_Gritting_TeamsTableAdapter;
        private DevExpress.XtraEditors.CheckEdit ShowActiveOnlyCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnMonday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnTuesday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnWednesday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnThursday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnFriday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnSaturday;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOnSunday;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp04047GCPrefferedGrittingTeamExtraCostsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraCostID;
        private DevExpress.XtraGrid.Columns.GridColumn colPreferredSubContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSell;
        private DevExpress.XtraGrid.Columns.GridColumn colVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamAddressLine11;
        private DevExpress.XtraGrid.Columns.GridColumn colPreferrenceOrder1;
        private DataSet_GC_CoreTableAdapters.sp04047_GC_Preffered_Gritting_Team_Extra_CostsTableAdapter sp04047_GC_Preffered_Gritting_Team_Extra_CostsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingActivationCode;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingActivationCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingForecastTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumTemperature;
        private System.Windows.Forms.BindingSource sp04057GCSiteFilterDropDownJustGritingSitesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04057_GC_Site_Filter_DropDown_Just_Griting_SitesTableAdapter sp04057_GC_Site_Filter_DropDown_Just_Griting_SitesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultGritAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamProactiveRate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamReactiveRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientEveningRateModifier;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colChargedPerHour;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOfUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP2;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingTimeToDeduct;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextHours2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCompanies;
        private DevExpress.XtraEditors.SimpleButton btnCompanyFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOrder;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditCompanies;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource;
        private DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter sp04237_GC_Company_Filter_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentCostConverted;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltVatRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultNoAccessRate;
        private DevExpress.XtraGrid.Columns.GridColumn colPrioritySite;
        private DevExpress.XtraGrid.Columns.GridColumn colBandingEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colBandingStart;
        private DevExpress.XtraGrid.Columns.GridColumn colRedOverridesBanding;
        private DevExpress.XtraGrid.Columns.GridColumn colPDADefault;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTel;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colWebSite;
        private DevExpress.XtraGrid.Columns.GridColumn colIsFloatingSite;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCompletionEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingTimeOnSite;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMinutes;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteOpenTime1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteClosedTime1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteOpenTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteClosedTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCompletionEmailLinkedPictures;
        private DevExpress.XtraGrid.Columns.GridColumn colAnnualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessRestrictions;
        private DevExpress.XtraGrid.Columns.GridColumn colAnnual_Contract;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending7;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstPreferredContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDontInvoiceClientReason;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumPictureCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSeasonPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colSeasonPeriodID;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultProactiveRate1;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultReactiveRate1;
    }
}

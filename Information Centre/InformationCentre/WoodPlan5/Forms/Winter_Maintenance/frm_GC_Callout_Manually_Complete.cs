﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Callout_Manually_Complete : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public DateTime? dtStartDate = null;
        public DateTime? dtCompletedDate = null;
        public decimal? decSaltUsed = null;
        public string strComments = null;

        #endregion

        public frm_GC_Callout_Manually_Complete()
        {
            InitializeComponent();
        }

        private void frm_GC_Callout_Manually_Complete_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400057;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            dateEditFromDate.EditValue = null;
            dateEditToDate.EditValue = null;
            spinEdit1.EditValue = null;
            memoEdit1.EditValue = "Information for this visit has been populated via our automated text messaging service and gritting database.";

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dateEditFromDate.EditValue != null) dtStartDate = Convert.ToDateTime(dateEditFromDate.EditValue);
            if (dateEditToDate.EditValue != null) dtCompletedDate = Convert.ToDateTime(dateEditToDate.EditValue);
            if (spinEdit1.EditValue != null) decSaltUsed = Convert.ToDecimal(spinEdit1.EditValue);
            if (memoEdit1.EditValue != null) strComments = memoEdit1.EditValue.ToString();

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void dateEditFromDate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: If your proceed, on clicking OK, any Start date on the jobs being updated will be left as is.\n\nAre you sure you wish to proceed?", "Clear Date", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }



    }
}

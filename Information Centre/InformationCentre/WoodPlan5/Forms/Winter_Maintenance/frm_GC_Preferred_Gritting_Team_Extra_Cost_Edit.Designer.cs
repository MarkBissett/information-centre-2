namespace WoodPlan5
{
    partial class frm_GC_Preferred_Gritting_Team_Extra_Cost_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Preferred_Gritting_Team_Extra_Cost_Edit));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.GrittingTimeToDeductSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.SnowOnSiteOnlyCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NumberOfUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ChargedPerHourCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.VatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PreferredSubContractorDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CostTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00227PicklistListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ExtraCostIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PreferredSubContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForPreferredSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFoExtraCostID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChargedPerHour = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNumberOfUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowOnSiteOnly = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingTimeToDeduct = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCostTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPreferredSubContractorDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04049_GC_Preffered_Gritting_Teams_Extra_Cost_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04049_GC_Preffered_Gritting_Teams_Extra_Cost_EditTableAdapter();
            this.sp00227_Picklist_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00227_Picklist_List_With_BlankTableAdapter();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingTimeToDeductSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowOnSiteOnlyCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargedPerHourCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreferredSubContractorDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00227PicklistListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraCostIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreferredSubContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPreferredSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFoExtraCostID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargedPerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNumberOfUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowOnSiteOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingTimeToDeduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPreferredSubContractorDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(678, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 502);
            this.barDockControlBottom.Size = new System.Drawing.Size(678, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(678, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colID
            // 
            this.colID.Caption = "Item ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(678, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 502);
            this.barDockControl2.Size = new System.Drawing.Size(678, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(678, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 476);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.GrittingTimeToDeductSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowOnSiteOnlyCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.NumberOfUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ChargedPerHourCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.VatRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PreferredSubContractorDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.CostTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.ExtraCostIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PreferredSubContractorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.DataSource = this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPreferredSubContractorID,
            this.ItemFoExtraCostID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1242, 305, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(678, 476);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // GrittingTimeToDeductSpinEdit
            // 
            this.GrittingTimeToDeductSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "GrittingTimeToDeduct", true));
            this.GrittingTimeToDeductSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.GrittingTimeToDeductSpinEdit.Location = new System.Drawing.Point(175, 334);
            this.GrittingTimeToDeductSpinEdit.MenuManager = this.barManager1;
            this.GrittingTimeToDeductSpinEdit.Name = "GrittingTimeToDeductSpinEdit";
            this.GrittingTimeToDeductSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.GrittingTimeToDeductSpinEdit.Properties.Mask.EditMask = "##0.00 Hours";
            this.GrittingTimeToDeductSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.GrittingTimeToDeductSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.GrittingTimeToDeductSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.GrittingTimeToDeductSpinEdit.StyleController = this.dataLayoutControl1;
            this.GrittingTimeToDeductSpinEdit.TabIndex = 37;
            // 
            // sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource
            // 
            this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource.DataMember = "sp04049_GC_Preffered_Gritting_Teams_Extra_Cost_Edit";
            this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SnowOnSiteOnlyCheckEdit
            // 
            this.SnowOnSiteOnlyCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "SnowOnSiteOnly", true));
            this.SnowOnSiteOnlyCheckEdit.Location = new System.Drawing.Point(175, 311);
            this.SnowOnSiteOnlyCheckEdit.MenuManager = this.barManager1;
            this.SnowOnSiteOnlyCheckEdit.Name = "SnowOnSiteOnlyCheckEdit";
            this.SnowOnSiteOnlyCheckEdit.Properties.Caption = "(Tick if this record should only be added When Snow On Site is ticked on Gritting" +
    " Callouts)";
            this.SnowOnSiteOnlyCheckEdit.Properties.ValueChecked = 1;
            this.SnowOnSiteOnlyCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowOnSiteOnlyCheckEdit.Size = new System.Drawing.Size(457, 19);
            this.SnowOnSiteOnlyCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowOnSiteOnlyCheckEdit.TabIndex = 36;
            // 
            // NumberOfUnitsSpinEdit
            // 
            this.NumberOfUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "NumberOfUnits", true));
            this.NumberOfUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NumberOfUnitsSpinEdit.Location = new System.Drawing.Point(175, 287);
            this.NumberOfUnitsSpinEdit.MenuManager = this.barManager1;
            this.NumberOfUnitsSpinEdit.Name = "NumberOfUnitsSpinEdit";
            this.NumberOfUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.NumberOfUnitsSpinEdit.Properties.Mask.EditMask = "f2";
            this.NumberOfUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.NumberOfUnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.NumberOfUnitsSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.NumberOfUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.NumberOfUnitsSpinEdit.TabIndex = 35;
            // 
            // ChargedPerHourCheckEdit
            // 
            this.ChargedPerHourCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "ChargedPerHour", true));
            this.ChargedPerHourCheckEdit.Location = new System.Drawing.Point(175, 192);
            this.ChargedPerHourCheckEdit.MenuManager = this.barManager1;
            this.ChargedPerHourCheckEdit.Name = "ChargedPerHourCheckEdit";
            this.ChargedPerHourCheckEdit.Properties.Caption = "(Tick if Unit Cost is Hourly Rate, Untick if Unit Cost is Job Cost)";
            this.ChargedPerHourCheckEdit.Properties.ValueChecked = 1;
            this.ChargedPerHourCheckEdit.Properties.ValueUnchecked = 0;
            this.ChargedPerHourCheckEdit.Size = new System.Drawing.Size(467, 19);
            this.ChargedPerHourCheckEdit.StyleController = this.dataLayoutControl1;
            this.ChargedPerHourCheckEdit.TabIndex = 34;
            // 
            // VatRateSpinEdit
            // 
            this.VatRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "VatRate", true));
            this.VatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VatRateSpinEdit.Location = new System.Drawing.Point(175, 263);
            this.VatRateSpinEdit.MenuManager = this.barManager1;
            this.VatRateSpinEdit.Name = "VatRateSpinEdit";
            this.VatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.VatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.VatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.VatRateSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.VatRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.VatRateSpinEdit.TabIndex = 33;
            // 
            // SellSpinEdit
            // 
            this.SellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "Sell", true));
            this.SellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellSpinEdit.Location = new System.Drawing.Point(175, 239);
            this.SellSpinEdit.MenuManager = this.barManager1;
            this.SellSpinEdit.Name = "SellSpinEdit";
            this.SellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SellSpinEdit.Properties.Mask.EditMask = "c";
            this.SellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.SellSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.SellSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellSpinEdit.TabIndex = 32;
            // 
            // CostSpinEdit
            // 
            this.CostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "Cost", true));
            this.CostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostSpinEdit.Location = new System.Drawing.Point(175, 215);
            this.CostSpinEdit.MenuManager = this.barManager1;
            this.CostSpinEdit.Name = "CostSpinEdit";
            this.CostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CostSpinEdit.Properties.Mask.EditMask = "c";
            this.CostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.CostSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.CostSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostSpinEdit.TabIndex = 31;
            // 
            // DescriptionTextEdit
            // 
            this.DescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "Description", true));
            this.DescriptionTextEdit.Location = new System.Drawing.Point(151, 86);
            this.DescriptionTextEdit.MenuManager = this.barManager1;
            this.DescriptionTextEdit.Name = "DescriptionTextEdit";
            this.DescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DescriptionTextEdit, true);
            this.DescriptionTextEdit.Size = new System.Drawing.Size(515, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DescriptionTextEdit, optionsSpelling1);
            this.DescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.DescriptionTextEdit.TabIndex = 30;
            this.DescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DescriptionTextEdit_Validating);
            // 
            // PreferredSubContractorDescriptionButtonEdit
            // 
            this.PreferredSubContractorDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "PreferredSubContractorDescription", true));
            this.PreferredSubContractorDescriptionButtonEdit.Location = new System.Drawing.Point(151, 36);
            this.PreferredSubContractorDescriptionButtonEdit.MenuManager = this.barManager1;
            this.PreferredSubContractorDescriptionButtonEdit.Name = "PreferredSubContractorDescriptionButtonEdit";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "View Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>open</b> the <b>Parent Tree</b> record for the current Inspection." +
    "";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.PreferredSubContractorDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to Select Parent Tree", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "view", superToolTip4, true)});
            this.PreferredSubContractorDescriptionButtonEdit.Properties.ReadOnly = true;
            this.PreferredSubContractorDescriptionButtonEdit.Size = new System.Drawing.Size(515, 20);
            this.PreferredSubContractorDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.PreferredSubContractorDescriptionButtonEdit.TabIndex = 29;
            this.PreferredSubContractorDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PreferredSubContractorDescriptionButtonEdit_ButtonClick);
            this.PreferredSubContractorDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PreferredSubContractorDescriptionButtonEdit_Validating);
            // 
            // CostTypeIDGridLookUpEdit
            // 
            this.CostTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "CostTypeID", true));
            this.CostTypeIDGridLookUpEdit.Location = new System.Drawing.Point(151, 60);
            this.CostTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CostTypeIDGridLookUpEdit.Name = "CostTypeIDGridLookUpEdit";
            this.CostTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Edit Underlying Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Reload Underlying Data", "reload", null, true)});
            this.CostTypeIDGridLookUpEdit.Properties.DataSource = this.sp00227PicklistListWithBlankBindingSource;
            this.CostTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.CostTypeIDGridLookUpEdit.Properties.NullText = "";
            this.CostTypeIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemMemoExEdit1});
            this.CostTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.CostTypeIDGridLookUpEdit.Properties.View = this.gridView1;
            this.CostTypeIDGridLookUpEdit.Size = new System.Drawing.Size(515, 22);
            this.CostTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CostTypeIDGridLookUpEdit.TabIndex = 19;
            this.CostTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CostTypeIDGridLookUpEdit_ButtonClick);
            this.CostTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.CostTypeIDGridLookUpEdit_Validating);
            // 
            // sp00227PicklistListWithBlankBindingSource
            // 
            this.sp00227PicklistListWithBlankBindingSource.DataMember = "sp00227_Picklist_List_With_Blank";
            this.sp00227PicklistListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colintOrder});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colintOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 303;
            // 
            // colintOrder
            // 
            this.colintOrder.Caption = "Order";
            this.colintOrder.FieldName = "intOrder";
            this.colintOrder.Name = "colintOrder";
            this.colintOrder.OptionsColumn.AllowEdit = false;
            this.colintOrder.OptionsColumn.AllowFocus = false;
            this.colintOrder.OptionsColumn.ReadOnly = true;
            this.colintOrder.Width = 149;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(153, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(192, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ExtraCostIDSpinEdit
            // 
            this.ExtraCostIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "ExtraCostID", true));
            this.ExtraCostIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ExtraCostIDSpinEdit.Location = new System.Drawing.Point(129, 86);
            this.ExtraCostIDSpinEdit.MenuManager = this.barManager1;
            this.ExtraCostIDSpinEdit.Name = "ExtraCostIDSpinEdit";
            this.ExtraCostIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ExtraCostIDSpinEdit.Properties.ReadOnly = true;
            this.ExtraCostIDSpinEdit.Size = new System.Drawing.Size(183, 20);
            this.ExtraCostIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.ExtraCostIDSpinEdit.TabIndex = 4;
            // 
            // PreferredSubContractorIDTextEdit
            // 
            this.PreferredSubContractorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "PreferredSubContractorID", true));
            this.PreferredSubContractorIDTextEdit.Location = new System.Drawing.Point(129, 86);
            this.PreferredSubContractorIDTextEdit.MenuManager = this.barManager1;
            this.PreferredSubContractorIDTextEdit.Name = "PreferredSubContractorIDTextEdit";
            this.PreferredSubContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PreferredSubContractorIDTextEdit, true);
            this.PreferredSubContractorIDTextEdit.Size = new System.Drawing.Size(487, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PreferredSubContractorIDTextEdit, optionsSpelling2);
            this.PreferredSubContractorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PreferredSubContractorIDTextEdit.TabIndex = 16;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 192);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(606, 176);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling3);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // ItemForPreferredSubContractorID
            // 
            this.ItemForPreferredSubContractorID.Control = this.PreferredSubContractorIDTextEdit;
            this.ItemForPreferredSubContractorID.CustomizationFormText = "Preferred Team ID:";
            this.ItemForPreferredSubContractorID.Location = new System.Drawing.Point(0, 74);
            this.ItemForPreferredSubContractorID.Name = "ItemForPreferredSubContractorID";
            this.ItemForPreferredSubContractorID.Size = new System.Drawing.Size(608, 24);
            this.ItemForPreferredSubContractorID.Text = "Preferred Team ID:";
            this.ItemForPreferredSubContractorID.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemFoExtraCostID
            // 
            this.ItemFoExtraCostID.Control = this.ExtraCostIDSpinEdit;
            this.ItemFoExtraCostID.CustomizationFormText = "Extra Cost ID:";
            this.ItemFoExtraCostID.Location = new System.Drawing.Point(0, 74);
            this.ItemFoExtraCostID.Name = "ItemFoExtraCostID";
            this.ItemFoExtraCostID.Size = new System.Drawing.Size(304, 24);
            this.ItemFoExtraCostID.Text = "Extra Cost ID:";
            this.ItemFoExtraCostID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(678, 476);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroup4,
            this.emptySpaceItem2,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.ItemForCostTypeID,
            this.ItemForPreferredSubContractorDescription,
            this.ItemForDescription});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(658, 456);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 384);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(658, 72);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 110);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(658, 274);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(634, 228);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.ItemForCost,
            this.ItemForChargedPerHour,
            this.ItemForSnowOnSiteOnly,
            this.ItemForGrittingTimeToDeduct,
            this.emptySpaceItem6,
            this.ItemForSell,
            this.layoutControlItem2,
            this.ItemForNumberOfUnits,
            this.emptySpaceItem7,
            this.emptySpaceItem8});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(610, 180);
            this.layoutControlGroup5.Text = "Details";
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 166);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(610, 14);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCost
            // 
            this.ItemForCost.Control = this.CostSpinEdit;
            this.ItemForCost.CustomizationFormText = "Cost:";
            this.ItemForCost.Location = new System.Drawing.Point(0, 23);
            this.ItemForCost.Name = "ItemForCost";
            this.ItemForCost.Size = new System.Drawing.Size(305, 24);
            this.ItemForCost.Text = "Cost:";
            this.ItemForCost.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSell
            // 
            this.ItemForSell.Control = this.SellSpinEdit;
            this.ItemForSell.CustomizationFormText = "Cost To Client:";
            this.ItemForSell.Location = new System.Drawing.Point(0, 47);
            this.ItemForSell.Name = "ItemForSell";
            this.ItemForSell.Size = new System.Drawing.Size(305, 24);
            this.ItemForSell.Text = "Cost To Client:";
            this.ItemForSell.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.VatRateSpinEdit;
            this.layoutControlItem2.CustomizationFormText = "VAT Rate:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 71);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(305, 24);
            this.layoutControlItem2.Text = "VAT Rate:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForChargedPerHour
            // 
            this.ItemForChargedPerHour.Control = this.ChargedPerHourCheckEdit;
            this.ItemForChargedPerHour.CustomizationFormText = "Charged Per Hour:";
            this.ItemForChargedPerHour.Location = new System.Drawing.Point(0, 0);
            this.ItemForChargedPerHour.Name = "ItemForChargedPerHour";
            this.ItemForChargedPerHour.Size = new System.Drawing.Size(610, 23);
            this.ItemForChargedPerHour.Text = "Charged Per Hour:";
            this.ItemForChargedPerHour.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForNumberOfUnits
            // 
            this.ItemForNumberOfUnits.Control = this.NumberOfUnitsSpinEdit;
            this.ItemForNumberOfUnits.CustomizationFormText = "Default Number of Units:";
            this.ItemForNumberOfUnits.Location = new System.Drawing.Point(0, 95);
            this.ItemForNumberOfUnits.Name = "ItemForNumberOfUnits";
            this.ItemForNumberOfUnits.Size = new System.Drawing.Size(305, 24);
            this.ItemForNumberOfUnits.Text = "Default Number of Units:";
            this.ItemForNumberOfUnits.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForSnowOnSiteOnly
            // 
            this.ItemForSnowOnSiteOnly.Control = this.SnowOnSiteOnlyCheckEdit;
            this.ItemForSnowOnSiteOnly.CustomizationFormText = "Applies When Snow On Site:";
            this.ItemForSnowOnSiteOnly.Location = new System.Drawing.Point(0, 119);
            this.ItemForSnowOnSiteOnly.Name = "ItemForSnowOnSiteOnly";
            this.ItemForSnowOnSiteOnly.Size = new System.Drawing.Size(600, 23);
            this.ItemForSnowOnSiteOnly.Text = "Applies When Snow On Site:";
            this.ItemForSnowOnSiteOnly.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForGrittingTimeToDeduct
            // 
            this.ItemForGrittingTimeToDeduct.Control = this.GrittingTimeToDeductSpinEdit;
            this.ItemForGrittingTimeToDeduct.CustomizationFormText = "Gritting Time To Deduct:";
            this.ItemForGrittingTimeToDeduct.Location = new System.Drawing.Point(0, 142);
            this.ItemForGrittingTimeToDeduct.Name = "ItemForGrittingTimeToDeduct";
            this.ItemForGrittingTimeToDeduct.Size = new System.Drawing.Size(305, 24);
            this.ItemForGrittingTimeToDeduct.Text = "Gritting Time To Deduct:";
            this.ItemForGrittingTimeToDeduct.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(610, 180);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(610, 180);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(658, 12);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(337, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(321, 24);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(141, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(196, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(141, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(141, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(141, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCostTypeID
            // 
            this.ItemForCostTypeID.Control = this.CostTypeIDGridLookUpEdit;
            this.ItemForCostTypeID.CustomizationFormText = "Cost Type:";
            this.ItemForCostTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForCostTypeID.Name = "ItemForCostTypeID";
            this.ItemForCostTypeID.Size = new System.Drawing.Size(658, 26);
            this.ItemForCostTypeID.Text = "Cost Type:";
            this.ItemForCostTypeID.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForPreferredSubContractorDescription
            // 
            this.ItemForPreferredSubContractorDescription.Control = this.PreferredSubContractorDescriptionButtonEdit;
            this.ItemForPreferredSubContractorDescription.CustomizationFormText = "Preferred Gritting Team:";
            this.ItemForPreferredSubContractorDescription.Location = new System.Drawing.Point(0, 24);
            this.ItemForPreferredSubContractorDescription.Name = "ItemForPreferredSubContractorDescription";
            this.ItemForPreferredSubContractorDescription.Size = new System.Drawing.Size(658, 24);
            this.ItemForPreferredSubContractorDescription.Text = "Preferred Gritting Team:";
            this.ItemForPreferredSubContractorDescription.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.Control = this.DescriptionTextEdit;
            this.ItemForDescription.CustomizationFormText = "Cost Description:";
            this.ItemForDescription.Location = new System.Drawing.Point(0, 74);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(658, 24);
            this.ItemForDescription.Text = "Cost Description:";
            this.ItemForDescription.TextSize = new System.Drawing.Size(136, 13);
            // 
            // sp04049_GC_Preffered_Gritting_Teams_Extra_Cost_EditTableAdapter
            // 
            this.sp04049_GC_Preffered_Gritting_Teams_Extra_Cost_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp00227_Picklist_List_With_BlankTableAdapter
            // 
            this.sp00227_Picklist_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(305, 23);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(305, 96);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(305, 142);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(305, 24);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(600, 119);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(10, 23);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_GC_Preferred_Gritting_Team_Extra_Cost_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(678, 532);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Preferred_Gritting_Team_Extra_Cost_Edit";
            this.Text = "Edit Preferred Gritting Team - Extra Costs";
            this.Activated += new System.EventHandler(this.frm_GC_Preferred_Gritting_Team_Extra_Cost_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Preferred_Gritting_Team_Extra_Cost_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Preferred_Gritting_Team_Extra_Cost_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GrittingTimeToDeductSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowOnSiteOnlyCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChargedPerHourCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreferredSubContractorDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00227PicklistListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraCostIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreferredSubContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPreferredSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFoExtraCostID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChargedPerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNumberOfUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowOnSiteOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingTimeToDeduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPreferredSubContractorDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SpinEdit ExtraCostIDSpinEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemFoExtraCostID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPreferredSubContractorID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.GridLookUpEdit CostTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit PreferredSubContractorIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit PreferredSubContractorDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPreferredSubContractorDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp04049GCPrefferedGrittingTeamsExtraCostEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04049_GC_Preffered_Gritting_Teams_Extra_Cost_EditTableAdapter sp04049_GC_Preffered_Gritting_Teams_Extra_Cost_EditTableAdapter;
        private System.Windows.Forms.BindingSource sp00227PicklistListWithBlankBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private WoodPlanDataSetTableAdapters.sp00227_Picklist_List_With_BlankTableAdapter sp00227_Picklist_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colintOrder;
        private DevExpress.XtraEditors.TextEdit DescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;
        private DevExpress.XtraEditors.SpinEdit SellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSell;
        private DevExpress.XtraEditors.SpinEdit VatRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CheckEdit ChargedPerHourCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChargedPerHour;
        private DevExpress.XtraEditors.SpinEdit NumberOfUnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNumberOfUnits;
        private DevExpress.XtraEditors.CheckEdit SnowOnSiteOnlyCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowOnSiteOnly;
        private DevExpress.XtraEditors.SpinEdit GrittingTimeToDeductSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingTimeToDeduct;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
    }
}

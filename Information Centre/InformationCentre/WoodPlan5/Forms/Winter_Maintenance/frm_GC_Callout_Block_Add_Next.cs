﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WoodPlan5
{
    public partial class frm_GC_Callout_Block_Add_Next : DevExpress.XtraEditors.XtraForm
    {
        
        #region Instance Variables...
        
        public int intRecordCount = 0;
        public string strReturnedValue = "Close";
        #endregion

        public frm_GC_Callout_Block_Add_Next()
        {
            InitializeComponent();
        }

        private void GC_Callout_Block_Add_Next_Load(object sender, EventArgs e)
        {
            labelControl1.Text = "Block Add Completed Successfully. <b>" + intRecordCount.ToString() + " " + (intRecordCount == 1 ? "record" : "records") + " created</b>.";
            btnBlockEdit.Enabled = (intRecordCount > 1 ? true : false);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            strReturnedValue = "Close";
            this.Close();

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            strReturnedValue = "Edit";
            this.Close();

        }

        private void btnBlockEdit_Click(object sender, EventArgs e)
        {
            strReturnedValue = "BlockEdit";
            this.Close();
        }
    }
}
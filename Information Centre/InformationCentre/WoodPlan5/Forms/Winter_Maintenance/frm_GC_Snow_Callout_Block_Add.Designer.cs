﻿namespace WoodPlan5
{
    partial class frm_GC_Snow_Callout_Block_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Snow_Callout_Block_Add));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnNext1 = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollectionWizardButtons = new DevExpress.Utils.ImageCollection(this.components);
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.popupContainerControlCompanies = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCompanyFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04237GCCompanyFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlLinkedGrittingCalloutsFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04001GCJobCallOutStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnGritCalloutFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnPrevious2 = new DevExpress.XtraEditors.SimpleButton();
            this.bntNext2 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04178GCSnowCalloutBlockAddSelectSitesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Snow_DataEntry = new WoodPlan5.DataSet_GC_Snow_DataEntry();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteFax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLastCalloutDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedRecordCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumberRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOIDDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp04180GCSnowCalloutBlockAddTemplateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSnowClearanceCallOutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCPONumberSuffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditVC50 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditTeam = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRecordedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorETA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colSubContractorContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00226StaffListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisplayName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetworkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOIDDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditClientPO = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientHowSoonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditHowSoon = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp04165GCClientHowSoonTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumberRequired1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoursWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.btnNext3 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrevious3 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControlNumberOfJobsReadyToSend = new DevExpress.XtraEditors.LabelControl();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrevious4 = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.sp04178_GC_Snow_Callout_Block_Add_Select_SitesTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04178_GC_Snow_Callout_Block_Add_Select_SitesTableAdapter();
            this.sp00226_Staff_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter();
            this.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter();
            this.pmBlockEdit = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiBlockEditTeam = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockEditClientPO = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockEditOtherValues = new DevExpress.XtraBars.BarButtonItem();
            this.sp04180_GC_Snow_Callout_Block_Add_TemplateTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04180_GC_Snow_Callout_Block_Add_TemplateTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemLinkedRecordType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barEditItemCalloutStatusFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemCompanyFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditCompanyFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiReloadGrid = new DevExpress.XtraBars.BarButtonItem();
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp04237_GC_Company_Filter_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionWizardButtons)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).BeginInit();
            this.popupContainerControlCompanies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).BeginInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04178GCSnowCalloutBlockAddSelectSitesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04180GCSnowCalloutBlockAddTemplateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVC50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditClientPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditHowSoon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04165GCClientHowSoonTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmBlockEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(959, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 511);
            this.barDockControlBottom.Size = new System.Drawing.Size(959, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 511);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(959, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 511);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiBlockEditTeam,
            this.bbiBlockEditClientPO,
            this.bbiBlockEditOtherValues,
            this.barEditItemLinkedRecordType,
            this.barEditItemCalloutStatusFilter,
            this.bbiReloadGrid,
            this.barEditItemCompanyFilter});
            this.barManager1.MaxItemId = 35;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemPopupContainerEditCompanyFilter});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 59;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.wizard_image;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(205, 454);
            this.pictureEdit1.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(676, 33);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Welcome to the <b>Add Snow Clearance Callout Wizard</b>";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(959, 511);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.panelControl1);
            this.xtraTabPage1.Controls.Add(this.btnNext1);
            this.xtraTabPage1.Controls.Add(this.labelControl2);
            this.xtraTabPage1.Controls.Add(this.pictureEdit1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(932, 506);
            this.xtraTabPage1.Text = "Welcome";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.pictureEdit6);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Location = new System.Drawing.Point(213, 8);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(708, 67);
            this.panelControl1.TabIndex = 17;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit6.EditValue = ((object)(resources.GetObject("pictureEdit6.EditValue")));
            this.pictureEdit6.Location = new System.Drawing.Point(637, 3);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ShowMenu = false;
            this.pictureEdit6.Size = new System.Drawing.Size(67, 60);
            this.pictureEdit6.TabIndex = 18;
            // 
            // labelControl9
            // 
            this.labelControl9.AllowHtmlString = true;
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(9, 48);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(389, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "This wizard will allow you to create a snow clearance callout for one or more Sit" +
    "es";
            // 
            // btnNext1
            // 
            this.btnNext1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext1.ImageOptions.ImageIndex = 1;
            this.btnNext1.ImageOptions.ImageList = this.imageCollectionWizardButtons;
            this.btnNext1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnNext1.Location = new System.Drawing.Point(829, 462);
            this.btnNext1.Name = "btnNext1";
            this.btnNext1.Size = new System.Drawing.Size(92, 36);
            this.btnNext1.TabIndex = 7;
            this.btnNext1.Text = "Next";
            this.btnNext1.Click += new System.EventHandler(this.btnNext1_Click);
            // 
            // imageCollectionWizardButtons
            // 
            this.imageCollectionWizardButtons.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollectionWizardButtons.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionWizardButtons.ImageStream")));
            this.imageCollectionWizardButtons.InsertImage(global::WoodPlan5.Properties.Resources.backward_32x32, "backward_32x32", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollectionWizardButtons.Images.SetKeyName(0, "backward_32x32");
            this.imageCollectionWizardButtons.InsertImage(global::WoodPlan5.Properties.Resources.forward_32x32, "forward_32x32", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollectionWizardButtons.Images.SetKeyName(1, "forward_32x32");
            this.imageCollectionWizardButtons.InsertImage(global::WoodPlan5.Properties.Resources.apply_32x32, "apply_32x32", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollectionWizardButtons.Images.SetKeyName(2, "apply_32x32");
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(218, 139);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(169, 19);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Click <b>Next</b> To Continue";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.popupContainerControlCompanies);
            this.xtraTabPage2.Controls.Add(this.popupContainerControlLinkedGrittingCalloutsFilter);
            this.xtraTabPage2.Controls.Add(this.standaloneBarDockControl1);
            this.xtraTabPage2.Controls.Add(this.panelControl3);
            this.xtraTabPage2.Controls.Add(this.btnPrevious2);
            this.xtraTabPage2.Controls.Add(this.bntNext2);
            this.xtraTabPage2.Controls.Add(this.gridControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(932, 506);
            this.xtraTabPage2.Text = "Step 1";
            // 
            // popupContainerControlCompanies
            // 
            this.popupContainerControlCompanies.Controls.Add(this.btnCompanyFilterOK);
            this.popupContainerControlCompanies.Controls.Add(this.gridControl3);
            this.popupContainerControlCompanies.Location = new System.Drawing.Point(405, 239);
            this.popupContainerControlCompanies.Name = "popupContainerControlCompanies";
            this.popupContainerControlCompanies.Size = new System.Drawing.Size(193, 163);
            this.popupContainerControlCompanies.TabIndex = 17;
            // 
            // btnCompanyFilterOK
            // 
            this.btnCompanyFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCompanyFilterOK.Location = new System.Drawing.Point(3, 138);
            this.btnCompanyFilterOK.Name = "btnCompanyFilterOK";
            this.btnCompanyFilterOK.Size = new System.Drawing.Size(77, 22);
            this.btnCompanyFilterOK.TabIndex = 18;
            this.btnCompanyFilterOK.Text = "OK";
            this.btnCompanyFilterOK.Click += new System.EventHandler(this.btnCompanyFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp04237GCCompanyFilterListBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView4;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(187, 133);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp04237GCCompanyFilterListBindingSource
            // 
            this.sp04237GCCompanyFilterListBindingSource.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.colCompanyCode,
            this.colCompanyOrder});
            this.gridView4.GridControl = this.gridControl3;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Company ID";
            this.gridColumn5.FieldName = "CompanyID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 80;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Company Name";
            this.gridColumn6.FieldName = "CompanyName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 152;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyOrder
            // 
            this.colCompanyOrder.Caption = "Order";
            this.colCompanyOrder.FieldName = "CompanyOrder";
            this.colCompanyOrder.Name = "colCompanyOrder";
            this.colCompanyOrder.OptionsColumn.AllowEdit = false;
            this.colCompanyOrder.OptionsColumn.AllowFocus = false;
            this.colCompanyOrder.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerControlLinkedGrittingCalloutsFilter
            // 
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.layoutControl2);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.btnGritCalloutFilterOK);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Location = new System.Drawing.Point(49, 182);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Name = "popupContainerControlLinkedGrittingCalloutsFilter";
            this.popupContainerControlLinkedGrittingCalloutsFilter.Size = new System.Drawing.Size(340, 245);
            this.popupContainerControlLinkedGrittingCalloutsFilter.TabIndex = 12;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.Controls.Add(this.gridControl5);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1185, 135, 250, 350);
            this.layoutControl2.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(340, 220);
            this.layoutControl2.TabIndex = 12;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp04001GCJobCallOutStatusesBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(12, 32);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(316, 152);
            this.gridControl5.TabIndex = 4;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04001GCJobCallOutStatusesBindingSource
            // 
            this.sp04001GCJobCallOutStatusesBindingSource.DataMember = "sp04001_GC_Job_CallOut_Statuses";
            this.sp04001GCJobCallOutStatusesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Callout Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 263;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Value";
            this.gridColumn2.FieldName = "Value";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(216, 188);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all ca" +
    "llouts will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(112, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 11;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(69, 188);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all ca" +
    "llouts will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(98, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(340, 220);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Linked Snow Clearance Callouts - Data Filter";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(336, 216);
            this.layoutControlGroup3.Text = "Linked Snow Clearance Callouts - Data Filter";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl5;
            this.layoutControlItem3.CustomizationFormText = "Callout Job Status Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(320, 156);
            this.layoutControlItem3.Text = "Callout Job Status Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateEditFromDate;
            this.layoutControlItem4.CustomizationFormText = "From Date:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(159, 24);
            this.layoutControlItem4.Text = "From Date:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dateEditToDate;
            this.layoutControlItem5.CustomizationFormText = "To Date:";
            this.layoutControlItem5.Location = new System.Drawing.Point(159, 156);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem5.Text = "To Date:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(42, 13);
            // 
            // btnGritCalloutFilterOK
            // 
            this.btnGritCalloutFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGritCalloutFilterOK.Location = new System.Drawing.Point(3, 220);
            this.btnGritCalloutFilterOK.Name = "btnGritCalloutFilterOK";
            this.btnGritCalloutFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnGritCalloutFilterOK.TabIndex = 12;
            this.btnGritCalloutFilterOK.Text = "OK";
            this.btnGritCalloutFilterOK.Click += new System.EventHandler(this.btnGritCalloutFilterOK_Click);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(11, 79);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(910, 26);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Controls.Add(this.labelControl3);
            this.panelControl3.Controls.Add(this.pictureEdit3);
            this.panelControl3.Controls.Add(this.labelControl5);
            this.panelControl3.Location = new System.Drawing.Point(11, 8);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(910, 65);
            this.panelControl3.TabIndex = 10;
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(5, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(317, 16);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "<b>Step 1:</b> Select one or more Sites to create Callouts for";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit3.EditValue = ((object)(resources.GetObject("pictureEdit3.EditValue")));
            this.pictureEdit3.Location = new System.Drawing.Point(839, 3);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Size = new System.Drawing.Size(67, 60);
            this.pictureEdit3.TabIndex = 8;
            // 
            // labelControl5
            // 
            this.labelControl5.AllowHtmlString = true;
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Location = new System.Drawing.Point(57, 27);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(362, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Select the sites to create callouts for by ticking them. Once done click Next.";
            // 
            // btnPrevious2
            // 
            this.btnPrevious2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrevious2.ImageOptions.ImageIndex = 0;
            this.btnPrevious2.ImageOptions.ImageList = this.imageCollectionWizardButtons;
            this.btnPrevious2.Location = new System.Drawing.Point(11, 462);
            this.btnPrevious2.Name = "btnPrevious2";
            this.btnPrevious2.Size = new System.Drawing.Size(92, 36);
            this.btnPrevious2.TabIndex = 9;
            this.btnPrevious2.Text = "Previous";
            this.btnPrevious2.Click += new System.EventHandler(this.btnPrevious2_Click);
            // 
            // bntNext2
            // 
            this.bntNext2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntNext2.ImageOptions.ImageIndex = 1;
            this.bntNext2.ImageOptions.ImageList = this.imageCollectionWizardButtons;
            this.bntNext2.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.bntNext2.Location = new System.Drawing.Point(829, 462);
            this.bntNext2.Name = "bntNext2";
            this.bntNext2.Size = new System.Drawing.Size(92, 36);
            this.bntNext2.TabIndex = 8;
            this.bntNext2.Text = "Next";
            this.bntNext2.Click += new System.EventHandler(this.bntNext2_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl1.BackgroundImage")));
            this.gridControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gridControl1.DataSource = this.sp04178GCSnowCalloutBlockAddSelectSitesBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(11, 106);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemCheckEdit1,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(910, 347);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04178GCSnowCalloutBlockAddSelectSitesBindingSource
            // 
            this.sp04178GCSnowCalloutBlockAddSelectSitesBindingSource.DataMember = "sp04178_GC_Snow_Callout_Block_Add_Select_Sites";
            this.sp04178GCSnowCalloutBlockAddSelectSitesBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // dataSet_GC_Snow_DataEntry
            // 
            this.dataSet_GC_Snow_DataEntry.DataSetName = "DataSet_GC_Snow_DataEntry";
            this.dataSet_GC_Snow_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colSiteID,
            this.colClientName,
            this.colClientCode,
            this.colSiteCode,
            this.colSiteName,
            this.colSiteTypeDescription,
            this.colSiteTypeID,
            this.colContactPerson,
            this.colContactPersonPosition,
            this.colSiteAddressLine1,
            this.colSiteAddressLine2,
            this.colSiteAddressLine3,
            this.colSiteAddressLine4,
            this.colSiteAddressLine5,
            this.colSitePostcode,
            this.colSiteTelephone,
            this.colSiteMobile,
            this.colSiteFax,
            this.colSiteEmail,
            this.colXCoordinate,
            this.colYCoordinate,
            this.colClientRemarks,
            this.colSiteRemarks,
            this.colSnowClearanceSiteContractID,
            this.colActive,
            this.colLastCalloutDateTime,
            this.colLinkedRecordCount,
            this.colClientsSiteCode,
            this.colClientsSiteID,
            this.colClientPONumberRequired,
            this.colClientPOID1,
            this.colClientPOIDDescription1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 239;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Width = 105;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 4;
            this.colSiteCode.Width = 95;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 185;
            // 
            // colSiteTypeDescription
            // 
            this.colSiteTypeDescription.Caption = "Site Type";
            this.colSiteTypeDescription.FieldName = "SiteTypeDescription";
            this.colSiteTypeDescription.Name = "colSiteTypeDescription";
            this.colSiteTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSiteTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSiteTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSiteTypeDescription.Visible = true;
            this.colSiteTypeDescription.VisibleIndex = 1;
            this.colSiteTypeDescription.Width = 127;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            this.colSiteTypeID.Width = 80;
            // 
            // colContactPerson
            // 
            this.colContactPerson.Caption = "Contact Person";
            this.colContactPerson.FieldName = "ContactPerson";
            this.colContactPerson.Name = "colContactPerson";
            this.colContactPerson.OptionsColumn.AllowEdit = false;
            this.colContactPerson.OptionsColumn.AllowFocus = false;
            this.colContactPerson.OptionsColumn.ReadOnly = true;
            this.colContactPerson.Visible = true;
            this.colContactPerson.VisibleIndex = 7;
            this.colContactPerson.Width = 144;
            // 
            // colContactPersonPosition
            // 
            this.colContactPersonPosition.Caption = "Contact Person Position";
            this.colContactPersonPosition.FieldName = "ContactPersonPosition";
            this.colContactPersonPosition.Name = "colContactPersonPosition";
            this.colContactPersonPosition.OptionsColumn.AllowEdit = false;
            this.colContactPersonPosition.OptionsColumn.AllowFocus = false;
            this.colContactPersonPosition.OptionsColumn.ReadOnly = true;
            this.colContactPersonPosition.Visible = true;
            this.colContactPersonPosition.VisibleIndex = 8;
            this.colContactPersonPosition.Width = 135;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Site Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 9;
            this.colSiteAddressLine1.Width = 112;
            // 
            // colSiteAddressLine2
            // 
            this.colSiteAddressLine2.Caption = "Site Address Line 2";
            this.colSiteAddressLine2.FieldName = "SiteAddressLine2";
            this.colSiteAddressLine2.Name = "colSiteAddressLine2";
            this.colSiteAddressLine2.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine2.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine2.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine2.Width = 112;
            // 
            // colSiteAddressLine3
            // 
            this.colSiteAddressLine3.Caption = "Site Address Line 3";
            this.colSiteAddressLine3.FieldName = "SiteAddressLine3";
            this.colSiteAddressLine3.Name = "colSiteAddressLine3";
            this.colSiteAddressLine3.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine3.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine3.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine3.Width = 112;
            // 
            // colSiteAddressLine4
            // 
            this.colSiteAddressLine4.Caption = "Site Address Line 4";
            this.colSiteAddressLine4.FieldName = "SiteAddressLine4";
            this.colSiteAddressLine4.Name = "colSiteAddressLine4";
            this.colSiteAddressLine4.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine4.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine4.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine4.Width = 112;
            // 
            // colSiteAddressLine5
            // 
            this.colSiteAddressLine5.Caption = "Site Address Line 5";
            this.colSiteAddressLine5.FieldName = "SiteAddressLine5";
            this.colSiteAddressLine5.Name = "colSiteAddressLine5";
            this.colSiteAddressLine5.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine5.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine5.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine5.Width = 112;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Width = 86;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Site Telephone ";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Visible = true;
            this.colSiteTelephone.VisibleIndex = 10;
            this.colSiteTelephone.Width = 95;
            // 
            // colSiteMobile
            // 
            this.colSiteMobile.Caption = "Site Mobile";
            this.colSiteMobile.FieldName = "SiteMobile";
            this.colSiteMobile.Name = "colSiteMobile";
            this.colSiteMobile.OptionsColumn.AllowEdit = false;
            this.colSiteMobile.OptionsColumn.AllowFocus = false;
            this.colSiteMobile.OptionsColumn.ReadOnly = true;
            this.colSiteMobile.Visible = true;
            this.colSiteMobile.VisibleIndex = 11;
            this.colSiteMobile.Width = 90;
            // 
            // colSiteFax
            // 
            this.colSiteFax.Caption = "Site Fax";
            this.colSiteFax.FieldName = "SiteFax";
            this.colSiteFax.Name = "colSiteFax";
            this.colSiteFax.OptionsColumn.AllowEdit = false;
            this.colSiteFax.OptionsColumn.AllowFocus = false;
            this.colSiteFax.OptionsColumn.ReadOnly = true;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Site Email";
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.AllowEdit = false;
            this.colSiteEmail.OptionsColumn.AllowFocus = false;
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            this.colSiteEmail.Visible = true;
            this.colSiteEmail.VisibleIndex = 12;
            this.colSiteEmail.Width = 151;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "Site X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            this.colXCoordinate.Width = 104;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Site Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            this.colYCoordinate.Width = 104;
            // 
            // colClientRemarks
            // 
            this.colClientRemarks.Caption = "Client Remarks";
            this.colClientRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colClientRemarks.FieldName = "ClientRemarks";
            this.colClientRemarks.Name = "colClientRemarks";
            this.colClientRemarks.OptionsColumn.ReadOnly = true;
            this.colClientRemarks.Visible = true;
            this.colClientRemarks.VisibleIndex = 15;
            this.colClientRemarks.Width = 92;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colSiteRemarks
            // 
            this.colSiteRemarks.Caption = "Site Remarks";
            this.colSiteRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteRemarks.FieldName = "SiteRemarks";
            this.colSiteRemarks.Name = "colSiteRemarks";
            this.colSiteRemarks.OptionsColumn.ReadOnly = true;
            this.colSiteRemarks.Visible = true;
            this.colSiteRemarks.VisibleIndex = 13;
            this.colSiteRemarks.Width = 83;
            // 
            // colSnowClearanceSiteContractID
            // 
            this.colSnowClearanceSiteContractID.Caption = "Snow Clearance Contract ID";
            this.colSnowClearanceSiteContractID.FieldName = "SnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.Name = "colSnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSiteContractID.Width = 157;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colLastCalloutDateTime
            // 
            this.colLastCalloutDateTime.Caption = " Last Callout";
            this.colLastCalloutDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colLastCalloutDateTime.FieldName = "LastCalloutDateTime";
            this.colLastCalloutDateTime.Name = "colLastCalloutDateTime";
            this.colLastCalloutDateTime.OptionsColumn.AllowEdit = false;
            this.colLastCalloutDateTime.OptionsColumn.AllowFocus = false;
            this.colLastCalloutDateTime.OptionsColumn.ReadOnly = true;
            this.colLastCalloutDateTime.Visible = true;
            this.colLastCalloutDateTime.VisibleIndex = 2;
            this.colLastCalloutDateTime.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colLinkedRecordCount
            // 
            this.colLinkedRecordCount.Caption = "Linked Record Count";
            this.colLinkedRecordCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedRecordCount.FieldName = "LinkedRecordCount";
            this.colLinkedRecordCount.Name = "colLinkedRecordCount";
            this.colLinkedRecordCount.Visible = true;
            this.colLinkedRecordCount.VisibleIndex = 3;
            this.colLinkedRecordCount.Width = 120;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Visible = true;
            this.colClientsSiteCode.VisibleIndex = 5;
            this.colClientsSiteCode.Width = 102;
            // 
            // colClientsSiteID
            // 
            this.colClientsSiteID.Caption = "Clients Site ID";
            this.colClientsSiteID.FieldName = "ClientsSiteID";
            this.colClientsSiteID.Name = "colClientsSiteID";
            this.colClientsSiteID.OptionsColumn.AllowEdit = false;
            this.colClientsSiteID.OptionsColumn.AllowFocus = false;
            this.colClientsSiteID.OptionsColumn.ReadOnly = true;
            this.colClientsSiteID.Width = 88;
            // 
            // colClientPONumberRequired
            // 
            this.colClientPONumberRequired.Caption = "Client PO Required";
            this.colClientPONumberRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colClientPONumberRequired.FieldName = "ClientPONumberRequired";
            this.colClientPONumberRequired.Name = "colClientPONumberRequired";
            this.colClientPONumberRequired.OptionsColumn.AllowEdit = false;
            this.colClientPONumberRequired.OptionsColumn.ReadOnly = true;
            this.colClientPONumberRequired.Visible = true;
            this.colClientPONumberRequired.VisibleIndex = 14;
            this.colClientPONumberRequired.Width = 111;
            // 
            // colClientPOID1
            // 
            this.colClientPOID1.Caption = "Default Client PO ID";
            this.colClientPOID1.FieldName = "ClientPOID";
            this.colClientPOID1.Name = "colClientPOID1";
            this.colClientPOID1.OptionsColumn.AllowEdit = false;
            this.colClientPOID1.OptionsColumn.AllowFocus = false;
            this.colClientPOID1.OptionsColumn.ReadOnly = true;
            this.colClientPOID1.Width = 120;
            // 
            // colClientPOIDDescription1
            // 
            this.colClientPOIDDescription1.Caption = "Default Client PO";
            this.colClientPOIDDescription1.FieldName = "ClientPOIDDescription";
            this.colClientPOIDDescription1.Name = "colClientPOIDDescription1";
            this.colClientPOIDDescription1.OptionsColumn.AllowEdit = false;
            this.colClientPOIDDescription1.OptionsColumn.AllowFocus = false;
            this.colClientPOIDDescription1.OptionsColumn.ReadOnly = true;
            this.colClientPOIDDescription1.Visible = true;
            this.colClientPOIDDescription1.VisibleIndex = 6;
            this.colClientPOIDDescription1.Width = 106;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl2);
            this.xtraTabPage3.Controls.Add(this.panelControl4);
            this.xtraTabPage3.Controls.Add(this.btnNext3);
            this.xtraTabPage3.Controls.Add(this.btnPrevious3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(932, 506);
            this.xtraTabPage3.Text = "Step 2";
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl2.BackgroundImage")));
            this.gridControl2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gridControl2.DataSource = this.sp04180GCSnowCalloutBlockAddTemplateBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Callout(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(11, 80);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemGridLookUpEditHowSoon,
            this.repositoryItemButtonEditTeam,
            this.repositoryItemButtonEditClientPO,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemTextEditVC50,
            this.repositoryItemDateEdit1,
            this.repositoryItemSpinEdit2DP});
            this.gridControl2.Size = new System.Drawing.Size(910, 373);
            this.gridControl2.TabIndex = 16;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp04180GCSnowCalloutBlockAddTemplateBindingSource
            // 
            this.sp04180GCSnowCalloutBlockAddTemplateBindingSource.DataMember = "sp04180_GC_Snow_Callout_Block_Add_Template";
            this.sp04180GCSnowCalloutBlockAddTemplateBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSnowClearanceCallOutID,
            this.colSnowClearanceSiteContractID1,
            this.colGCPONumberSuffix,
            this.colClientName1,
            this.colSiteName1,
            this.colSubContractorID,
            this.colSubContractorName,
            this.colReactive,
            this.colJobStatusID,
            this.colCallOutDateTime,
            this.colRecordedByStaffID,
            this.colRecordedByName,
            this.colSubContractorETA,
            this.colSubContractorContactedByStaffID,
            this.colAccessComments,
            this.colClientPOID,
            this.colClientPOIDDescription,
            this.colClientPONumber,
            this.colLabourVatRate,
            this.colRemarks,
            this.colNonStandardCost,
            this.colNonStandardSell,
            this.colClientHowSoonID,
            this.colSiteID1,
            this.colSiteAddressLine11,
            this.colSiteAddressLine21,
            this.colSiteAddressLine31,
            this.colSiteAddressLine41,
            this.colSiteAddressLine51,
            this.colSitePostcode1,
            this.colSiteCode1,
            this.colClientsSiteCode1,
            this.colClientsSiteID1,
            this.colClientPONumberRequired1,
            this.colHoursWorked});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            this.gridView2.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView2_ValidatingEditor);
            // 
            // colSnowClearanceCallOutID
            // 
            this.colSnowClearanceCallOutID.Caption = "GC PO ID";
            this.colSnowClearanceCallOutID.FieldName = "SnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.Name = "colSnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutID.Visible = true;
            this.colSnowClearanceCallOutID.VisibleIndex = 11;
            this.colSnowClearanceCallOutID.Width = 66;
            // 
            // colSnowClearanceSiteContractID1
            // 
            this.colSnowClearanceSiteContractID1.Caption = "Contract ID";
            this.colSnowClearanceSiteContractID1.FieldName = "SnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID1.Name = "colSnowClearanceSiteContractID1";
            this.colSnowClearanceSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSiteContractID1.Width = 77;
            // 
            // colGCPONumberSuffix
            // 
            this.colGCPONumberSuffix.Caption = "GC PO Suffix";
            this.colGCPONumberSuffix.ColumnEdit = this.repositoryItemTextEditVC50;
            this.colGCPONumberSuffix.FieldName = "GCPONumberSuffix";
            this.colGCPONumberSuffix.Name = "colGCPONumberSuffix";
            this.colGCPONumberSuffix.Visible = true;
            this.colGCPONumberSuffix.VisibleIndex = 12;
            this.colGCPONumberSuffix.Width = 83;
            // 
            // repositoryItemTextEditVC50
            // 
            this.repositoryItemTextEditVC50.AutoHeight = false;
            this.repositoryItemTextEditVC50.MaxLength = 50;
            this.repositoryItemTextEditVC50.Name = "repositoryItemTextEditVC50";
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 0;
            this.colClientName1.Width = 123;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 1;
            this.colSiteName1.Width = 128;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Team Name";
            this.colSubContractorName.ColumnEdit = this.repositoryItemButtonEditTeam;
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 2;
            this.colSubContractorName.Width = 177;
            // 
            // repositoryItemButtonEditTeam
            // 
            this.repositoryItemButtonEditTeam.AutoHeight = false;
            this.repositoryItemButtonEditTeam.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Team screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to View selected Team Details", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditTeam.Name = "repositoryItemButtonEditTeam";
            this.repositoryItemButtonEditTeam.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditTeam.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditTeam_ButtonClick);
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 9;
            this.colReactive.Width = 63;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 101;
            // 
            // colCallOutDateTime
            // 
            this.colCallOutDateTime.Caption = "Callout Date\\Time";
            this.colCallOutDateTime.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colCallOutDateTime.FieldName = "CallOutDateTime";
            this.colCallOutDateTime.Name = "colCallOutDateTime";
            this.colCallOutDateTime.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime.Visible = true;
            this.colCallOutDateTime.VisibleIndex = 13;
            this.colCallOutDateTime.Width = 106;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colRecordedByStaffID
            // 
            this.colRecordedByStaffID.Caption = "Recorded By Staff ID";
            this.colRecordedByStaffID.FieldName = "RecordedByStaffID";
            this.colRecordedByStaffID.Name = "colRecordedByStaffID";
            this.colRecordedByStaffID.OptionsColumn.AllowEdit = false;
            this.colRecordedByStaffID.OptionsColumn.AllowFocus = false;
            this.colRecordedByStaffID.OptionsColumn.ReadOnly = true;
            this.colRecordedByStaffID.Width = 123;
            // 
            // colRecordedByName
            // 
            this.colRecordedByName.Caption = "Recorded By Name";
            this.colRecordedByName.FieldName = "RecordedByName";
            this.colRecordedByName.Name = "colRecordedByName";
            this.colRecordedByName.OptionsColumn.AllowEdit = false;
            this.colRecordedByName.OptionsColumn.AllowFocus = false;
            this.colRecordedByName.OptionsColumn.ReadOnly = true;
            this.colRecordedByName.Visible = true;
            this.colRecordedByName.VisibleIndex = 14;
            this.colRecordedByName.Width = 112;
            // 
            // colSubContractorETA
            // 
            this.colSubContractorETA.Caption = "Team ETA";
            this.colSubContractorETA.ColumnEdit = this.repositoryItemDateEdit1;
            this.colSubContractorETA.FieldName = "SubContractorETA";
            this.colSubContractorETA.Name = "colSubContractorETA";
            this.colSubContractorETA.Visible = true;
            this.colSubContractorETA.VisibleIndex = 7;
            this.colSubContractorETA.Width = 119;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.repositoryItemDateEdit1.MinValue = new System.DateTime(2011, 1, 1, 0, 0, 0, 0);
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colSubContractorContactedByStaffID
            // 
            this.colSubContractorContactedByStaffID.Caption = "Team Contacted By";
            this.colSubContractorContactedByStaffID.ColumnEdit = this.repositoryItemGridLookUpEdit1;
            this.colSubContractorContactedByStaffID.FieldName = "SubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.Name = "colSubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.Visible = true;
            this.colSubContractorContactedByStaffID.VisibleIndex = 10;
            this.colSubContractorContactedByStaffID.Width = 119;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.repositoryItemGridLookUpEdit1.DisplayMember = "DisplayName";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.PopupView = this.gridView3;
            this.repositoryItemGridLookUpEdit1.ValueMember = "StaffID";
            // 
            // sp00226StaffListWithBlankBindingSource
            // 
            this.sp00226StaffListWithBlankBindingSource.DataMember = "sp00226_Staff_List_With_Blank";
            this.sp00226StaffListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.colDisplayName,
            this.colEmail,
            this.colForename,
            this.colNetworkID,
            this.colStaffID,
            this.colStaffName,
            this.colSurname,
            this.colUserType});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colStaffID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDisplayName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Active";
            this.gridColumn1.FieldName = "Active";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 51;
            // 
            // colDisplayName
            // 
            this.colDisplayName.Caption = "Surname: Forename";
            this.colDisplayName.FieldName = "DisplayName";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.OptionsColumn.AllowEdit = false;
            this.colDisplayName.OptionsColumn.AllowFocus = false;
            this.colDisplayName.OptionsColumn.ReadOnly = true;
            this.colDisplayName.Visible = true;
            this.colDisplayName.VisibleIndex = 0;
            this.colDisplayName.Width = 225;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowEdit = false;
            this.colEmail.OptionsColumn.AllowFocus = false;
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Width = 192;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Width = 178;
            // 
            // colNetworkID
            // 
            this.colNetworkID.Caption = "Network ID";
            this.colNetworkID.FieldName = "NetworkID";
            this.colNetworkID.Name = "colNetworkID";
            this.colNetworkID.OptionsColumn.AllowEdit = false;
            this.colNetworkID.OptionsColumn.AllowFocus = false;
            this.colNetworkID.OptionsColumn.ReadOnly = true;
            this.colNetworkID.Width = 180;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Width = 231;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Width = 191;
            // 
            // colUserType
            // 
            this.colUserType.Caption = "User Type";
            this.colUserType.FieldName = "UserType";
            this.colUserType.Name = "colUserType";
            this.colUserType.OptionsColumn.AllowEdit = false;
            this.colUserType.OptionsColumn.AllowFocus = false;
            this.colUserType.OptionsColumn.ReadOnly = true;
            this.colUserType.Width = 223;
            // 
            // colAccessComments
            // 
            this.colAccessComments.Caption = "Access Comments";
            this.colAccessComments.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colAccessComments.FieldName = "AccessComments";
            this.colAccessComments.Name = "colAccessComments";
            this.colAccessComments.Visible = true;
            this.colAccessComments.VisibleIndex = 8;
            this.colAccessComments.Width = 107;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Linked Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 112;
            // 
            // colClientPOIDDescription
            // 
            this.colClientPOIDDescription.Caption = "Linked Client PO";
            this.colClientPOIDDescription.ColumnEdit = this.repositoryItemButtonEditClientPO;
            this.colClientPOIDDescription.FieldName = "ClientPOIDDescription";
            this.colClientPOIDDescription.Name = "colClientPOIDDescription";
            this.colClientPOIDDescription.Visible = true;
            this.colClientPOIDDescription.VisibleIndex = 4;
            this.colClientPOIDDescription.Width = 137;
            // 
            // repositoryItemButtonEditClientPO
            // 
            this.repositoryItemButtonEditClientPO.AutoHeight = false;
            this.repositoryItemButtonEditClientPO.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click to Open Choose Client PO screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Click to View selected Client PO details", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditClientPO.Name = "repositoryItemButtonEditClientPO";
            this.repositoryItemButtonEditClientPO.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditClientPO.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditClientPO_ButtonClick);
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO Number";
            this.colClientPONumber.ColumnEdit = this.repositoryItemTextEditVC50;
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 5;
            this.colClientPONumber.Width = 105;
            // 
            // colLabourVatRate
            // 
            this.colLabourVatRate.Caption = "Labour VAT Rate";
            this.colLabourVatRate.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colLabourVatRate.FieldName = "LabourVatRate";
            this.colLabourVatRate.Name = "colLabourVatRate";
            this.colLabourVatRate.OptionsColumn.AllowEdit = false;
            this.colLabourVatRate.OptionsColumn.AllowFocus = false;
            this.colLabourVatRate.OptionsColumn.ReadOnly = true;
            this.colLabourVatRate.Visible = true;
            this.colLabourVatRate.VisibleIndex = 15;
            this.colLabourVatRate.Width = 102;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Callout Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 16;
            this.colRemarks.Width = 98;
            // 
            // colNonStandardCost
            // 
            this.colNonStandardCost.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNonStandardCost.FieldName = "NonStandardCost";
            this.colNonStandardCost.Name = "colNonStandardCost";
            this.colNonStandardCost.Visible = true;
            this.colNonStandardCost.VisibleIndex = 17;
            this.colNonStandardCost.Width = 112;
            // 
            // colNonStandardSell
            // 
            this.colNonStandardSell.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNonStandardSell.FieldName = "NonStandardSell";
            this.colNonStandardSell.Name = "colNonStandardSell";
            this.colNonStandardSell.Visible = true;
            this.colNonStandardSell.VisibleIndex = 18;
            this.colNonStandardSell.Width = 106;
            // 
            // colClientHowSoonID
            // 
            this.colClientHowSoonID.ColumnEdit = this.repositoryItemGridLookUpEditHowSoon;
            this.colClientHowSoonID.FieldName = "ClientHowSoonID";
            this.colClientHowSoonID.Name = "colClientHowSoonID";
            this.colClientHowSoonID.Visible = true;
            this.colClientHowSoonID.VisibleIndex = 6;
            this.colClientHowSoonID.Width = 113;
            // 
            // repositoryItemGridLookUpEditHowSoon
            // 
            this.repositoryItemGridLookUpEditHowSoon.AutoHeight = false;
            this.repositoryItemGridLookUpEditHowSoon.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditHowSoon.DataSource = this.sp04165GCClientHowSoonTypesWithBlankBindingSource;
            this.repositoryItemGridLookUpEditHowSoon.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditHowSoon.Name = "repositoryItemGridLookUpEditHowSoon";
            this.repositoryItemGridLookUpEditHowSoon.NullText = "";
            this.repositoryItemGridLookUpEditHowSoon.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditHowSoon.ValueMember = "Value";
            // 
            // sp04165GCClientHowSoonTypesWithBlankBindingSource
            // 
            this.sp04165GCClientHowSoonTypesWithBlankBindingSource.DataMember = "sp04165_GC_Client_How_Soon_Types_With_Blank";
            this.sp04165GCClientHowSoonTypesWithBlankBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colOrder,
            this.colValue});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colValue;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 210;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteAddressLine11
            // 
            this.colSiteAddressLine11.Caption = "Site Address Line 1";
            this.colSiteAddressLine11.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine11.Name = "colSiteAddressLine11";
            this.colSiteAddressLine11.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine11.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine11.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine11.Visible = true;
            this.colSiteAddressLine11.VisibleIndex = 21;
            this.colSiteAddressLine11.Width = 112;
            // 
            // colSiteAddressLine21
            // 
            this.colSiteAddressLine21.Caption = "Site Address Line 2";
            this.colSiteAddressLine21.FieldName = "SiteAddressLine2";
            this.colSiteAddressLine21.Name = "colSiteAddressLine21";
            this.colSiteAddressLine21.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine21.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine21.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine21.Visible = true;
            this.colSiteAddressLine21.VisibleIndex = 22;
            this.colSiteAddressLine21.Width = 112;
            // 
            // colSiteAddressLine31
            // 
            this.colSiteAddressLine31.Caption = "Site Address Line 3";
            this.colSiteAddressLine31.FieldName = "SiteAddressLine3";
            this.colSiteAddressLine31.Name = "colSiteAddressLine31";
            this.colSiteAddressLine31.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine31.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine31.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine31.Visible = true;
            this.colSiteAddressLine31.VisibleIndex = 23;
            this.colSiteAddressLine31.Width = 112;
            // 
            // colSiteAddressLine41
            // 
            this.colSiteAddressLine41.Caption = "Site Address Line 4";
            this.colSiteAddressLine41.FieldName = "SiteAddressLine4";
            this.colSiteAddressLine41.Name = "colSiteAddressLine41";
            this.colSiteAddressLine41.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine41.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine41.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine41.Visible = true;
            this.colSiteAddressLine41.VisibleIndex = 24;
            this.colSiteAddressLine41.Width = 112;
            // 
            // colSiteAddressLine51
            // 
            this.colSiteAddressLine51.Caption = "Site Address Line 5";
            this.colSiteAddressLine51.FieldName = "SiteAddressLine5";
            this.colSiteAddressLine51.Name = "colSiteAddressLine51";
            this.colSiteAddressLine51.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine51.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine51.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine51.Visible = true;
            this.colSiteAddressLine51.VisibleIndex = 25;
            this.colSiteAddressLine51.Width = 112;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Site Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            this.colSitePostcode1.Visible = true;
            this.colSitePostcode1.VisibleIndex = 26;
            this.colSitePostcode1.Width = 86;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "Site Code";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            this.colSiteCode1.Visible = true;
            this.colSiteCode1.VisibleIndex = 19;
            // 
            // colClientsSiteCode1
            // 
            this.colClientsSiteCode1.Caption = "Clients Site Code";
            this.colClientsSiteCode1.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode1.Name = "colClientsSiteCode1";
            this.colClientsSiteCode1.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode1.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode1.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode1.Visible = true;
            this.colClientsSiteCode1.VisibleIndex = 20;
            this.colClientsSiteCode1.Width = 102;
            // 
            // colClientsSiteID1
            // 
            this.colClientsSiteID1.Caption = "Clients Site ID";
            this.colClientsSiteID1.FieldName = "ClientsSiteID";
            this.colClientsSiteID1.Name = "colClientsSiteID1";
            this.colClientsSiteID1.OptionsColumn.AllowEdit = false;
            this.colClientsSiteID1.OptionsColumn.AllowFocus = false;
            this.colClientsSiteID1.OptionsColumn.ReadOnly = true;
            this.colClientsSiteID1.Width = 88;
            // 
            // colClientPONumberRequired1
            // 
            this.colClientPONumberRequired1.Caption = "Client PO Number Required";
            this.colClientPONumberRequired1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colClientPONumberRequired1.FieldName = "ClientPONumberRequired";
            this.colClientPONumberRequired1.Name = "colClientPONumberRequired1";
            this.colClientPONumberRequired1.OptionsColumn.AllowEdit = false;
            this.colClientPONumberRequired1.OptionsColumn.AllowFocus = false;
            this.colClientPONumberRequired1.OptionsColumn.ReadOnly = true;
            this.colClientPONumberRequired1.Width = 151;
            // 
            // colHoursWorked
            // 
            this.colHoursWorked.Caption = "Default Hours Worked";
            this.colHoursWorked.ColumnEdit = this.repositoryItemSpinEdit2DP;
            this.colHoursWorked.FieldName = "HoursWorked";
            this.colHoursWorked.Name = "colHoursWorked";
            this.colHoursWorked.Visible = true;
            this.colHoursWorked.VisibleIndex = 3;
            this.colHoursWorked.Width = 127;
            // 
            // repositoryItemSpinEdit2DP
            // 
            this.repositoryItemSpinEdit2DP.AutoHeight = false;
            this.repositoryItemSpinEdit2DP.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2DP.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2DP.Name = "repositoryItemSpinEdit2DP";
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.pictureEdit5);
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Location = new System.Drawing.Point(11, 8);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(910, 65);
            this.panelControl4.TabIndex = 15;
            // 
            // labelControl7
            // 
            this.labelControl7.AllowHtmlString = true;
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(5, 5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(300, 16);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "<b>Step 2:</b> Select the Preferred Teams for the Callouts\r\n";
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit5.EditValue = ((object)(resources.GetObject("pictureEdit5.EditValue")));
            this.pictureEdit5.Location = new System.Drawing.Point(839, 3);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ShowMenu = false;
            this.pictureEdit5.Size = new System.Drawing.Size(67, 60);
            this.pictureEdit5.TabIndex = 8;
            // 
            // labelControl8
            // 
            this.labelControl8.AllowHtmlString = true;
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Location = new System.Drawing.Point(57, 27);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(1150, 13);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = resources.GetString("labelControl8.Text");
            // 
            // btnNext3
            // 
            this.btnNext3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext3.ImageOptions.ImageIndex = 1;
            this.btnNext3.ImageOptions.ImageList = this.imageCollectionWizardButtons;
            this.btnNext3.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnNext3.Location = new System.Drawing.Point(829, 462);
            this.btnNext3.Name = "btnNext3";
            this.btnNext3.Size = new System.Drawing.Size(92, 36);
            this.btnNext3.TabIndex = 11;
            this.btnNext3.Text = "Next";
            this.btnNext3.Click += new System.EventHandler(this.btnNext3_Click);
            // 
            // btnPrevious3
            // 
            this.btnPrevious3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrevious3.ImageOptions.ImageIndex = 0;
            this.btnPrevious3.ImageOptions.ImageList = this.imageCollectionWizardButtons;
            this.btnPrevious3.Location = new System.Drawing.Point(11, 462);
            this.btnPrevious3.Name = "btnPrevious3";
            this.btnPrevious3.Size = new System.Drawing.Size(92, 36);
            this.btnPrevious3.TabIndex = 10;
            this.btnPrevious3.Text = "Previous";
            this.btnPrevious3.Click += new System.EventHandler(this.btnPrevious3_Click);
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.panelControl2);
            this.xtraTabPage4.Controls.Add(this.groupControl1);
            this.xtraTabPage4.Controls.Add(this.labelControl10);
            this.xtraTabPage4.Controls.Add(this.btnFinish);
            this.xtraTabPage4.Controls.Add(this.btnPrevious4);
            this.xtraTabPage4.Controls.Add(this.pictureEdit2);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(932, 506);
            this.xtraTabPage4.Text = "Finished";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.pictureEdit4);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Location = new System.Drawing.Point(213, 8);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(708, 82);
            this.panelControl2.TabIndex = 18;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.EditValue = ((object)(resources.GetObject("pictureEdit4.EditValue")));
            this.pictureEdit4.Location = new System.Drawing.Point(637, 3);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Size = new System.Drawing.Size(67, 60);
            this.pictureEdit4.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.AllowHtmlString = true;
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(5, 5);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(269, 33);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Completing the Wizard";
            // 
            // labelControl12
            // 
            this.labelControl12.AllowHtmlString = true;
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Location = new System.Drawing.Point(9, 48);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(381, 28);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "You have successfully completed the Wizard.\r\nOn clicking <b>Finish</b>, a callout" +
    " record will be created for each selected site.";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControlNumberOfJobsReadyToSend);
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.checkEdit4);
            this.groupControl1.Controls.Add(this.checkEdit3);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(220, 155);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(477, 144);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Available Choices:";
            // 
            // labelControlNumberOfJobsReadyToSend
            // 
            this.labelControlNumberOfJobsReadyToSend.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlNumberOfJobsReadyToSend.Appearance.ImageIndex = 0;
            this.labelControlNumberOfJobsReadyToSend.Appearance.ImageList = this.imageCollection2;
            this.labelControlNumberOfJobsReadyToSend.Appearance.Options.UseImageAlign = true;
            this.labelControlNumberOfJobsReadyToSend.Appearance.Options.UseImageIndex = true;
            this.labelControlNumberOfJobsReadyToSend.Appearance.Options.UseImageList = true;
            this.labelControlNumberOfJobsReadyToSend.Appearance.Options.UseTextOptions = true;
            this.labelControlNumberOfJobsReadyToSend.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControlNumberOfJobsReadyToSend.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlNumberOfJobsReadyToSend.Location = new System.Drawing.Point(23, 123);
            this.labelControlNumberOfJobsReadyToSend.Name = "labelControlNumberOfJobsReadyToSend";
            this.labelControlNumberOfJobsReadyToSend.Size = new System.Drawing.Size(272, 16);
            this.labelControlNumberOfJobsReadyToSend.TabIndex = 19;
            this.labelControlNumberOfJobsReadyToSend.Text = "        X of X New Callouts Ready To Send";
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(0, "info_16.png");
            this.imageCollection2.Images.SetKeyName(1, "attention_16.png");
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(6, 101);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit2.Properties.Caption = "Create the Callouts then <b>mark as Complete</b> and <b>Send</b>";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(466, 19);
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem3.Text = "Create the Callouts then mark as Complete and Send - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Only callouts with a selected Team and Hours Worked greater than 0.00 can be mark" +
    "ed as complete and sent. Any other new callouts with a Team will not be sent.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.checkEdit2.SuperTip = superToolTip3;
            this.checkEdit2.TabIndex = 4;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(6, 76);
            this.checkEdit4.MenuManager = this.barManager1;
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit4.Properties.Caption = "Create the Callouts then <b>Open</b> them for <b>block editing</b>";
            this.checkEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit4.Properties.RadioGroupIndex = 1;
            this.checkEdit4.Size = new System.Drawing.Size(466, 19);
            this.checkEdit4.TabIndex = 3;
            this.checkEdit4.TabStop = false;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(6, 51);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit3.Properties.Caption = "Create the Callouts then <b>Open</b> them for <b>further Editing</b>";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(466, 19);
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit1.Properties.Caption = "Create the Callouts then <b>return</b> to the <b>Callout Manager</b>";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(466, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(220, 127);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(154, 13);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "What would you like to do next?";
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnFinish.Appearance.Options.UseFont = true;
            this.btnFinish.ImageOptions.ImageIndex = 2;
            this.btnFinish.ImageOptions.ImageList = this.imageCollectionWizardButtons;
            this.btnFinish.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnFinish.Location = new System.Drawing.Point(829, 462);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(92, 36);
            this.btnFinish.TabIndex = 12;
            this.btnFinish.Text = "Finish";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnPrevious4
            // 
            this.btnPrevious4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrevious4.ImageOptions.ImageIndex = 0;
            this.btnPrevious4.ImageOptions.ImageList = this.imageCollectionWizardButtons;
            this.btnPrevious4.Location = new System.Drawing.Point(11, 462);
            this.btnPrevious4.Name = "btnPrevious4";
            this.btnPrevious4.Size = new System.Drawing.Size(92, 36);
            this.btnPrevious4.TabIndex = 11;
            this.btnPrevious4.Text = "Previous";
            this.btnPrevious4.Click += new System.EventHandler(this.btnPrevious4_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.wizard_finish1;
            this.pictureEdit2.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(205, 454);
            this.pictureEdit2.TabIndex = 5;
            // 
            // sp04178_GC_Snow_Callout_Block_Add_Select_SitesTableAdapter
            // 
            this.sp04178_GC_Snow_Callout_Block_Add_Select_SitesTableAdapter.ClearBeforeFill = true;
            // 
            // sp00226_Staff_List_With_BlankTableAdapter
            // 
            this.sp00226_Staff_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter
            // 
            this.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // pmBlockEdit
            // 
            this.pmBlockEdit.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEditTeam),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEditClientPO, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEditOtherValues, true)});
            this.pmBlockEdit.Manager = this.barManager1;
            this.pmBlockEdit.MenuCaption = "Block Edit";
            this.pmBlockEdit.Name = "pmBlockEdit";
            this.pmBlockEdit.ShowCaption = true;
            // 
            // bbiBlockEditTeam
            // 
            this.bbiBlockEditTeam.Caption = "Block Edit Selected Team";
            this.bbiBlockEditTeam.Id = 27;
            this.bbiBlockEditTeam.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditTeam.ImageOptions.Image")));
            this.bbiBlockEditTeam.Name = "bbiBlockEditTeam";
            this.bbiBlockEditTeam.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockEditTeam_ItemClick);
            // 
            // bbiBlockEditClientPO
            // 
            this.bbiBlockEditClientPO.Caption = "Block Edit Linked Client PO";
            this.bbiBlockEditClientPO.Id = 28;
            this.bbiBlockEditClientPO.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditClientPO.ImageOptions.Image")));
            this.bbiBlockEditClientPO.Name = "bbiBlockEditClientPO";
            this.bbiBlockEditClientPO.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockEditClientPO_ItemClick);
            // 
            // bbiBlockEditOtherValues
            // 
            this.bbiBlockEditOtherValues.Caption = "Block Edit Other Values";
            this.bbiBlockEditOtherValues.Id = 29;
            this.bbiBlockEditOtherValues.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditOtherValues.ImageOptions.Image")));
            this.bbiBlockEditOtherValues.Name = "bbiBlockEditOtherValues";
            this.bbiBlockEditOtherValues.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockEditOtherValues_ItemClick);
            // 
            // sp04180_GC_Snow_Callout_Block_Add_TemplateTableAdapter
            // 
            this.sp04180_GC_Snow_Callout_Block_Add_TemplateTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(561, 280);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemLinkedRecordType),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemCalloutStatusFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemCompanyFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadGrid)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Drill Down Filter Bar";
            // 
            // barEditItemLinkedRecordType
            // 
            this.barEditItemLinkedRecordType.Caption = "Linked Record Type";
            this.barEditItemLinkedRecordType.Edit = this.repositoryItemComboBox1;
            this.barEditItemLinkedRecordType.EditValue = "No Linked Records";
            this.barEditItemLinkedRecordType.EditWidth = 138;
            this.barEditItemLinkedRecordType.Id = 31;
            this.barEditItemLinkedRecordType.Name = "barEditItemLinkedRecordType";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "No Linked Records",
            "Snow Clearance Callouts"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.SelectedValueChanged += new System.EventHandler(this.repositoryItemComboBox1_SelectedValueChanged);
            // 
            // barEditItemCalloutStatusFilter
            // 
            this.barEditItemCalloutStatusFilter.Caption = "Callout Status Filter";
            this.barEditItemCalloutStatusFilter.Edit = this.repositoryItemPopupContainerEdit1;
            this.barEditItemCalloutStatusFilter.EditValue = "No Callout Status Filter";
            this.barEditItemCalloutStatusFilter.EditWidth = 341;
            this.barEditItemCalloutStatusFilter.Id = 32;
            this.barEditItemCalloutStatusFilter.Name = "barEditItemCalloutStatusFilter";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControlLinkedGrittingCalloutsFilter;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit1_QueryResultValue);
            // 
            // barEditItemCompanyFilter
            // 
            this.barEditItemCompanyFilter.Caption = "Company Filter";
            this.barEditItemCompanyFilter.Edit = this.repositoryItemPopupContainerEditCompanyFilter;
            this.barEditItemCompanyFilter.EditValue = "No Company Filter";
            this.barEditItemCompanyFilter.EditWidth = 112;
            this.barEditItemCompanyFilter.Id = 34;
            this.barEditItemCompanyFilter.Name = "barEditItemCompanyFilter";
            // 
            // repositoryItemPopupContainerEditCompanyFilter
            // 
            this.repositoryItemPopupContainerEditCompanyFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditCompanyFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditCompanyFilter.Name = "repositoryItemPopupContainerEditCompanyFilter";
            this.repositoryItemPopupContainerEditCompanyFilter.PopupControl = this.popupContainerControlCompanies;
            this.repositoryItemPopupContainerEditCompanyFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditCompanyFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditCompanyFilter_QueryResultValue);
            // 
            // bbiReloadGrid
            // 
            this.bbiReloadGrid.Caption = "Refresh Grid";
            this.bbiReloadGrid.Id = 33;
            this.bbiReloadGrid.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.bbiReloadGrid.Name = "bbiReloadGrid";
            this.bbiReloadGrid.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiReloadGrid.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadGrid_ItemClick);
            // 
            // sp04001_GC_Job_CallOut_StatusesTableAdapter
            // 
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // sp04237_GC_Company_Filter_ListTableAdapter
            // 
            this.sp04237_GC_Company_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Snow_Callout_Block_Add
            // 
            this.ClientSize = new System.Drawing.Size(959, 511);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Snow_Callout_Block_Add";
            this.Text = "Add SNOW CLEARANCE Callout Wizard";
            this.Activated += new System.EventHandler(this.frm_GC_Snow_Callout_Block_Add_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Snow_Callout_Block_Add_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Snow_Callout_Block_Add_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionWizardButtons)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).EndInit();
            this.popupContainerControlCompanies.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).EndInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04178GCSnowCalloutBlockAddSelectSitesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04180GCSnowCalloutBlockAddTemplateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVC50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditClientPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditHowSoon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04165GCClientHowSoonTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmBlockEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SimpleButton btnNext1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.SimpleButton btnPrevious2;
        private DevExpress.XtraEditors.SimpleButton bntNext2;
        private DevExpress.XtraEditors.SimpleButton btnNext3;
        private DevExpress.XtraEditors.SimpleButton btnPrevious3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.SimpleButton btnPrevious4;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource sp04178GCSnowCalloutBlockAddSelectSitesBindingSource;
        private DataSet_GC_Snow_DataEntry dataSet_GC_Snow_DataEntry;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteFax;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastCalloutDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04178_GC_Snow_Callout_Block_Add_Select_SitesTableAdapter sp04178_GC_Snow_Callout_Block_Add_Select_SitesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutID;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colGCPONumberSuffix;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorETA;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessComments;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOIDDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardSell;
        private DevExpress.XtraGrid.Columns.GridColumn colClientHowSoonID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditHowSoon;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditTeam;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditClientPO;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp00226StaffListWithBlankBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter sp00226_Staff_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colNetworkID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType;
        private System.Windows.Forms.BindingSource sp04165GCClientHowSoonTypesWithBlankBindingSource;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter sp04165_GC_Client_How_Soon_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraBars.BarButtonItem bbiBlockEditTeam;
        private DevExpress.XtraBars.BarButtonItem bbiBlockEditClientPO;
        private DevExpress.XtraBars.PopupMenu pmBlockEdit;
        private System.Windows.Forms.BindingSource sp04180GCSnowCalloutBlockAddTemplateBindingSource;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04180_GC_Snow_Callout_Block_Add_TemplateTableAdapter sp04180_GC_Snow_Callout_Block_Add_TemplateTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditVC50;
        private DevExpress.XtraBars.BarButtonItem bbiBlockEditOtherValues;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemLinkedRecordType;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem barEditItemCalloutStatusFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLinkedGrittingCalloutsFilter;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnGritCalloutFilterOK;
        private DevExpress.XtraBars.BarButtonItem bbiReloadGrid;
        private DataSet_GC_Core dataSet_GC_Core;
        private System.Windows.Forms.BindingSource sp04001GCJobCallOutStatusesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter sp04001_GC_Job_CallOut_StatusesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine11;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine21;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine31;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine41;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine51;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteID1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumberRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumberRequired1;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursWorked;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2DP;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraEditors.LabelControl labelControlNumberOfJobsReadyToSend;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCompanies;
        private DevExpress.XtraEditors.SimpleButton btnCompanyFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOrder;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource;
        private DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter sp04237_GC_Company_Filter_ListTableAdapter;
        private DevExpress.XtraBars.BarEditItem barEditItemCompanyFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditCompanyFilter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOIDDescription1;
        private DevExpress.Utils.ImageCollection imageCollectionWizardButtons;
    }
}

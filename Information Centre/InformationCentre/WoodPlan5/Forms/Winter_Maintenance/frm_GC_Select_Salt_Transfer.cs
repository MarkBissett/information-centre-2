using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Select_Salt_Transfer : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedValue = "";
        public int intSelectedID = 0;
        GridHitInfo downHitInfo = null;
        
        #endregion
    
        public frm_GC_Select_Salt_Transfer()
        {
            InitializeComponent();
        }

        private void frm_GC_Select_Salt_Transfer_Load(object sender, EventArgs e)
        {
            
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 400052;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            dateEditFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditToDate.DateTime = this.GlobalSettings.ViewedEndDate;

            sp04215_GC_Salt_Transfer_Select_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            LoadData();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

             if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void LoadData()
        {
            WaitDialogForm loading = new WaitDialogForm("Loading Transfers...", "Select Salt Transfer");
            if (fProgress == null)
            {
                loading.Show();
            }
            else
            {
                loading.Hide();
                loading.Dispose();
            }
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp04215_GC_Salt_Transfer_Select_ListTableAdapter.Fill(dataSet_GC_Core.sp04215_GC_Salt_Transfer_Select_List, dtFromDate, dtToDate);
            view.EndUpdate();
            if (fProgress == null)
            {
                loading.Close();
                loading.Dispose();
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            LoadData();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Records Available For Selection");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedID = Convert.ToInt32(gridView1.GetRowCellValue(view.FocusedRowHandle, "GritTransferID"));
                string strDate = Convert.ToDateTime(gridView1.GetRowCellValue(view.FocusedRowHandle, "TransferDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                string strFrom = gridView1.GetRowCellValue(view.FocusedRowHandle, "FromLocationName").ToString() ?? "Unknown Location";
                string strTo = gridView1.GetRowCellValue(view.FocusedRowHandle, "ToLocationName").ToString() ?? "Unknown Location";
		        strSelectedValue = "Date: " + strDate + " - From: " + strFrom + " To: " + strTo;
            }
        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear) dateEditFromDate.EditValue = null;
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear) dateEditToDate.EditValue = null;
        }



    }
}


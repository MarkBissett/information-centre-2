namespace WoodPlan5
{
    partial class frm_GC_Salt_Order_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Salt_Order_Manager));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04221GCSaltOrderManagerListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGritOrderHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colGritSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlacedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedDeliveryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountConverted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25kgbags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOtherCostVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOtherCostExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOtherCostIncVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCostExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCostExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCostIncVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCostIncVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGCPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSupplierName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderLineCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemainingDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlLinkedGrittingCalloutsFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04220GCSaltOrderStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnGritCalloutFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04223GCSaltOrderLinesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGritOrderLineID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritOrderHeaderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliverToLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliverToLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedirectFromLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedirectFromLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedirectedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSaltColourOrdered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountOrdered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAmountOrderedDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountOrderedConverted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25KgBag2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedDeliveryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualDeliveryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountDelivered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountDeliveredDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountDeliveredConverted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltColourDelivered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditVat2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colUnitCostExVat1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalCostExVat1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitCostIncVat1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCostIncVat1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltColourOrdered1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltColourDelivered1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountOrderedUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountOrderedConversionFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountDeliveredUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountDeliveredConversionFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliverToLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeliverToLocationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedirectFromLocationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedirectFromLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCPONumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemainingDays1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CustomFilterTextEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04220_GC_Salt_Order_StatusesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04220_GC_Salt_Order_StatusesTableAdapter();
            this.sp04221_GC_Salt_Order_Manager_ListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04221_GC_Salt_Order_Manager_ListTableAdapter();
            this.sp04223_GC_Salt_Order_LinesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04223_GC_Salt_Order_LinesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04221GCSaltOrderManagerListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgbags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).BeginInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04220GCSaltOrderStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04223GCSaltOrderLinesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBag2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVat2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomFilterTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(839, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(839, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(839, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp04221GCSaltOrderManagerListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEdit25kgbags,
            this.repositoryItemTextEditDateTime});
            this.gridControl1.Size = new System.Drawing.Size(835, 291);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04221GCSaltOrderManagerListBindingSource
            // 
            this.sp04221GCSaltOrderManagerListBindingSource.DataMember = "sp04221_GC_Salt_Order_Manager_List";
            this.sp04221GCSaltOrderManagerListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGritOrderHeaderID,
            this.colOrderDate,
            this.colGritSupplierID,
            this.colPlacedByStaffID,
            this.colEstimatedDeliveryDate,
            this.colTotalAmountConverted,
            this.colOtherCostVatRate,
            this.colOtherCostExVat,
            this.colOtherCostIncVat,
            this.colTotalCostExVat,
            this.colUnitCostExVat,
            this.colTotalCostIncVat,
            this.colUnitCostIncVat,
            this.colRemarks,
            this.colGCPONumber,
            this.colOrderStatusID,
            this.colGritSupplierName,
            this.colOrderedByStaffName,
            this.colOrderLineCount,
            this.colOrderStatusDescription,
            this.colRemainingDays});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colGritOrderHeaderID
            // 
            this.colGritOrderHeaderID.Caption = "Order Header ID";
            this.colGritOrderHeaderID.FieldName = "GritOrderHeaderID";
            this.colGritOrderHeaderID.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colGritOrderHeaderID.Name = "colGritOrderHeaderID";
            this.colGritOrderHeaderID.OptionsColumn.AllowEdit = false;
            this.colGritOrderHeaderID.OptionsColumn.AllowFocus = false;
            this.colGritOrderHeaderID.OptionsColumn.ReadOnly = true;
            this.colGritOrderHeaderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colGritOrderHeaderID.Width = 101;
            // 
            // colOrderDate
            // 
            this.colOrderDate.Caption = "Order Date";
            this.colOrderDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colOrderDate.FieldName = "OrderDate";
            this.colOrderDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colOrderDate.Name = "colOrderDate";
            this.colOrderDate.OptionsColumn.AllowEdit = false;
            this.colOrderDate.OptionsColumn.AllowFocus = false;
            this.colOrderDate.OptionsColumn.ReadOnly = true;
            this.colOrderDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colOrderDate.Visible = true;
            this.colOrderDate.VisibleIndex = 1;
            this.colOrderDate.Width = 98;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colGritSupplierID
            // 
            this.colGritSupplierID.Caption = "Supplier ID";
            this.colGritSupplierID.FieldName = "GritSupplierID";
            this.colGritSupplierID.Name = "colGritSupplierID";
            this.colGritSupplierID.OptionsColumn.AllowEdit = false;
            this.colGritSupplierID.OptionsColumn.AllowFocus = false;
            this.colGritSupplierID.OptionsColumn.ReadOnly = true;
            // 
            // colPlacedByStaffID
            // 
            this.colPlacedByStaffID.Caption = "Placed By Staff ID";
            this.colPlacedByStaffID.FieldName = "PlacedByStaffID";
            this.colPlacedByStaffID.Name = "colPlacedByStaffID";
            this.colPlacedByStaffID.OptionsColumn.AllowEdit = false;
            this.colPlacedByStaffID.OptionsColumn.AllowFocus = false;
            this.colPlacedByStaffID.OptionsColumn.ReadOnly = true;
            this.colPlacedByStaffID.Width = 108;
            // 
            // colEstimatedDeliveryDate
            // 
            this.colEstimatedDeliveryDate.Caption = "Estimated Delivery Date";
            this.colEstimatedDeliveryDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEstimatedDeliveryDate.FieldName = "EstimatedDeliveryDate";
            this.colEstimatedDeliveryDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEstimatedDeliveryDate.Name = "colEstimatedDeliveryDate";
            this.colEstimatedDeliveryDate.OptionsColumn.AllowEdit = false;
            this.colEstimatedDeliveryDate.OptionsColumn.AllowFocus = false;
            this.colEstimatedDeliveryDate.OptionsColumn.ReadOnly = true;
            this.colEstimatedDeliveryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEstimatedDeliveryDate.Visible = true;
            this.colEstimatedDeliveryDate.VisibleIndex = 4;
            this.colEstimatedDeliveryDate.Width = 136;
            // 
            // colTotalAmountConverted
            // 
            this.colTotalAmountConverted.Caption = "Amount Converted";
            this.colTotalAmountConverted.ColumnEdit = this.repositoryItemTextEdit25kgbags;
            this.colTotalAmountConverted.FieldName = "TotalAmountConverted";
            this.colTotalAmountConverted.Name = "colTotalAmountConverted";
            this.colTotalAmountConverted.OptionsColumn.AllowEdit = false;
            this.colTotalAmountConverted.OptionsColumn.AllowFocus = false;
            this.colTotalAmountConverted.OptionsColumn.ReadOnly = true;
            this.colTotalAmountConverted.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountConverted", "Total: {0:######0.00 25 Kg Bags}")});
            this.colTotalAmountConverted.Visible = true;
            this.colTotalAmountConverted.VisibleIndex = 6;
            this.colTotalAmountConverted.Width = 145;
            // 
            // repositoryItemTextEdit25kgbags
            // 
            this.repositoryItemTextEdit25kgbags.AutoHeight = false;
            this.repositoryItemTextEdit25kgbags.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25kgbags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25kgbags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25kgbags.Name = "repositoryItemTextEdit25kgbags";
            // 
            // colOtherCostVatRate
            // 
            this.colOtherCostVatRate.Caption = "Other Cost VAT Rate";
            this.colOtherCostVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colOtherCostVatRate.FieldName = "OtherCostVatRate";
            this.colOtherCostVatRate.Name = "colOtherCostVatRate";
            this.colOtherCostVatRate.OptionsColumn.AllowEdit = false;
            this.colOtherCostVatRate.OptionsColumn.AllowFocus = false;
            this.colOtherCostVatRate.OptionsColumn.ReadOnly = true;
            this.colOtherCostVatRate.Visible = true;
            this.colOtherCostVatRate.VisibleIndex = 7;
            this.colOtherCostVatRate.Width = 122;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colOtherCostExVat
            // 
            this.colOtherCostExVat.Caption = "Other Cost Ex VAT";
            this.colOtherCostExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherCostExVat.FieldName = "OtherCostExVat";
            this.colOtherCostExVat.Name = "colOtherCostExVat";
            this.colOtherCostExVat.OptionsColumn.AllowEdit = false;
            this.colOtherCostExVat.OptionsColumn.AllowFocus = false;
            this.colOtherCostExVat.OptionsColumn.ReadOnly = true;
            this.colOtherCostExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OtherCostExVat", "Total: {0:c2}")});
            this.colOtherCostExVat.Visible = true;
            this.colOtherCostExVat.VisibleIndex = 8;
            this.colOtherCostExVat.Width = 111;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colOtherCostIncVat
            // 
            this.colOtherCostIncVat.Caption = "Other Cost inc VAT";
            this.colOtherCostIncVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherCostIncVat.FieldName = "OtherCostIncVat";
            this.colOtherCostIncVat.Name = "colOtherCostIncVat";
            this.colOtherCostIncVat.OptionsColumn.AllowEdit = false;
            this.colOtherCostIncVat.OptionsColumn.AllowFocus = false;
            this.colOtherCostIncVat.OptionsColumn.ReadOnly = true;
            this.colOtherCostIncVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OtherCostIncVat", "Total: {0:c2}")});
            this.colOtherCostIncVat.Visible = true;
            this.colOtherCostIncVat.VisibleIndex = 9;
            this.colOtherCostIncVat.Width = 112;
            // 
            // colTotalCostExVat
            // 
            this.colTotalCostExVat.Caption = "Total Cost Ex VAT";
            this.colTotalCostExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCostExVat.FieldName = "TotalCostExVat";
            this.colTotalCostExVat.Name = "colTotalCostExVat";
            this.colTotalCostExVat.OptionsColumn.AllowEdit = false;
            this.colTotalCostExVat.OptionsColumn.AllowFocus = false;
            this.colTotalCostExVat.OptionsColumn.ReadOnly = true;
            this.colTotalCostExVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCostExVat", "Total: {0:c2}")});
            this.colTotalCostExVat.Visible = true;
            this.colTotalCostExVat.VisibleIndex = 10;
            this.colTotalCostExVat.Width = 107;
            // 
            // colUnitCostExVat
            // 
            this.colUnitCostExVat.Caption = "Unit Cost Ex VAT";
            this.colUnitCostExVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colUnitCostExVat.FieldName = "UnitCostExVat";
            this.colUnitCostExVat.Name = "colUnitCostExVat";
            this.colUnitCostExVat.OptionsColumn.AllowEdit = false;
            this.colUnitCostExVat.OptionsColumn.AllowFocus = false;
            this.colUnitCostExVat.OptionsColumn.ReadOnly = true;
            this.colUnitCostExVat.Width = 102;
            // 
            // colTotalCostIncVat
            // 
            this.colTotalCostIncVat.Caption = "Total Cost Inc VAT";
            this.colTotalCostIncVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCostIncVat.FieldName = "TotalCostIncVat";
            this.colTotalCostIncVat.Name = "colTotalCostIncVat";
            this.colTotalCostIncVat.OptionsColumn.AllowEdit = false;
            this.colTotalCostIncVat.OptionsColumn.AllowFocus = false;
            this.colTotalCostIncVat.OptionsColumn.ReadOnly = true;
            this.colTotalCostIncVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCostIncVat", "Total: {0:c2}")});
            this.colTotalCostIncVat.Visible = true;
            this.colTotalCostIncVat.VisibleIndex = 11;
            this.colTotalCostIncVat.Width = 110;
            // 
            // colUnitCostIncVat
            // 
            this.colUnitCostIncVat.Caption = "Unit Cost Inc VAT";
            this.colUnitCostIncVat.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colUnitCostIncVat.FieldName = "UnitCostIncVat";
            this.colUnitCostIncVat.Name = "colUnitCostIncVat";
            this.colUnitCostIncVat.OptionsColumn.AllowEdit = false;
            this.colUnitCostIncVat.OptionsColumn.AllowFocus = false;
            this.colUnitCostIncVat.OptionsColumn.ReadOnly = true;
            this.colUnitCostIncVat.Width = 105;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGCPONumber
            // 
            this.colGCPONumber.Caption = "GC PO Number";
            this.colGCPONumber.FieldName = "GCPONumber";
            this.colGCPONumber.Name = "colGCPONumber";
            this.colGCPONumber.OptionsColumn.AllowEdit = false;
            this.colGCPONumber.OptionsColumn.AllowFocus = false;
            this.colGCPONumber.OptionsColumn.ReadOnly = true;
            this.colGCPONumber.Visible = true;
            this.colGCPONumber.VisibleIndex = 2;
            this.colGCPONumber.Width = 92;
            // 
            // colOrderStatusID
            // 
            this.colOrderStatusID.Caption = "Order Status ID";
            this.colOrderStatusID.FieldName = "OrderStatusID";
            this.colOrderStatusID.Name = "colOrderStatusID";
            this.colOrderStatusID.OptionsColumn.AllowEdit = false;
            this.colOrderStatusID.OptionsColumn.AllowFocus = false;
            this.colOrderStatusID.OptionsColumn.ReadOnly = true;
            this.colOrderStatusID.Width = 97;
            // 
            // colGritSupplierName
            // 
            this.colGritSupplierName.Caption = "Supplier Name";
            this.colGritSupplierName.FieldName = "GritSupplierName";
            this.colGritSupplierName.Name = "colGritSupplierName";
            this.colGritSupplierName.OptionsColumn.AllowEdit = false;
            this.colGritSupplierName.OptionsColumn.AllowFocus = false;
            this.colGritSupplierName.OptionsColumn.ReadOnly = true;
            this.colGritSupplierName.Visible = true;
            this.colGritSupplierName.VisibleIndex = 3;
            this.colGritSupplierName.Width = 181;
            // 
            // colOrderedByStaffName
            // 
            this.colOrderedByStaffName.Caption = "Ordered By Staff";
            this.colOrderedByStaffName.FieldName = "OrderedByStaffName";
            this.colOrderedByStaffName.Name = "colOrderedByStaffName";
            this.colOrderedByStaffName.OptionsColumn.AllowEdit = false;
            this.colOrderedByStaffName.OptionsColumn.AllowFocus = false;
            this.colOrderedByStaffName.OptionsColumn.ReadOnly = true;
            this.colOrderedByStaffName.Visible = true;
            this.colOrderedByStaffName.VisibleIndex = 13;
            this.colOrderedByStaffName.Width = 103;
            // 
            // colOrderLineCount
            // 
            this.colOrderLineCount.Caption = "Order Line Count";
            this.colOrderLineCount.FieldName = "OrderLineCount";
            this.colOrderLineCount.Name = "colOrderLineCount";
            this.colOrderLineCount.OptionsColumn.AllowEdit = false;
            this.colOrderLineCount.OptionsColumn.AllowFocus = false;
            this.colOrderLineCount.OptionsColumn.ReadOnly = true;
            this.colOrderLineCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrderLineCount", "Total: {0}")});
            this.colOrderLineCount.Visible = true;
            this.colOrderLineCount.VisibleIndex = 12;
            this.colOrderLineCount.Width = 103;
            // 
            // colOrderStatusDescription
            // 
            this.colOrderStatusDescription.Caption = "Order Status";
            this.colOrderStatusDescription.FieldName = "OrderStatusDescription";
            this.colOrderStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colOrderStatusDescription.Name = "colOrderStatusDescription";
            this.colOrderStatusDescription.OptionsColumn.AllowEdit = false;
            this.colOrderStatusDescription.OptionsColumn.AllowFocus = false;
            this.colOrderStatusDescription.OptionsColumn.ReadOnly = true;
            this.colOrderStatusDescription.Visible = true;
            this.colOrderStatusDescription.VisibleIndex = 0;
            this.colOrderStatusDescription.Width = 192;
            // 
            // colRemainingDays
            // 
            this.colRemainingDays.Caption = "Days Until Due";
            this.colRemainingDays.FieldName = "RemainingDays";
            this.colRemainingDays.Name = "colRemainingDays";
            this.colRemainingDays.OptionsColumn.AllowEdit = false;
            this.colRemainingDays.OptionsColumn.AllowFocus = false;
            this.colRemainingDays.OptionsColumn.ReadOnly = true;
            this.colRemainingDays.Visible = true;
            this.colRemainingDays.VisibleIndex = 5;
            this.colRemainingDays.Width = 91;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlLinkedGrittingCalloutsFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Salt Orders";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Linked Documents";
            this.splitContainerControl1.Size = new System.Drawing.Size(839, 501);
            this.splitContainerControl1.SplitterPosition = 315;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // popupContainerControlLinkedGrittingCalloutsFilter
            // 
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.layoutControl2);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.btnGritCalloutFilterOK);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Location = new System.Drawing.Point(95, 27);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Name = "popupContainerControlLinkedGrittingCalloutsFilter";
            this.popupContainerControlLinkedGrittingCalloutsFilter.Size = new System.Drawing.Size(340, 245);
            this.popupContainerControlLinkedGrittingCalloutsFilter.TabIndex = 5;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.Controls.Add(this.gridControl5);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1185, 135, 250, 350);
            this.layoutControl2.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(340, 220);
            this.layoutControl2.TabIndex = 12;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp04220GCSaltOrderStatusesBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(12, 32);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(316, 152);
            this.gridControl5.TabIndex = 4;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04220GCSaltOrderStatusesBindingSource
            // 
            this.sp04220GCSaltOrderStatusesBindingSource.DataMember = "sp04220_GC_Salt_Order_Statuses";
            this.sp04220GCSaltOrderStatusesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colValue,
            this.colOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Order Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 263;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(216, 188);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all or" +
    "ders will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(112, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 11;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(69, 188);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all or" +
    "ders will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(98, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(340, 220);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Linked Gritting Callouts - Data Filter";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(336, 216);
            this.layoutControlGroup3.Text = "Order - Data Filter";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl5;
            this.layoutControlItem3.CustomizationFormText = "Callout Job Status Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(320, 156);
            this.layoutControlItem3.Text = "Callout Job Status Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateEditFromDate;
            this.layoutControlItem4.CustomizationFormText = "From Date:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(159, 24);
            this.layoutControlItem4.Text = "From Date:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dateEditToDate;
            this.layoutControlItem5.CustomizationFormText = "To Date:";
            this.layoutControlItem5.Location = new System.Drawing.Point(159, 156);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem5.Text = "To Date:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(42, 13);
            // 
            // btnGritCalloutFilterOK
            // 
            this.btnGritCalloutFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGritCalloutFilterOK.Location = new System.Drawing.Point(3, 220);
            this.btnGritCalloutFilterOK.Name = "btnGritCalloutFilterOK";
            this.btnGritCalloutFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnGritCalloutFilterOK.TabIndex = 12;
            this.btnGritCalloutFilterOK.Text = "OK";
            this.btnGritCalloutFilterOK.Click += new System.EventHandler(this.btnGritCalloutFilterOK_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(839, 180);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(834, 154);
            this.xtraTabPage1.Text = "Order Lines";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp04223GCSaltOrderLinesBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditVat2,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEdit25KgBag2});
            this.gridControl3.Size = new System.Drawing.Size(834, 154);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp04223GCSaltOrderLinesBindingSource
            // 
            this.sp04223GCSaltOrderLinesBindingSource.DataMember = "sp04223_GC_Salt_Order_Lines";
            this.sp04223GCSaltOrderLinesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGritOrderLineID,
            this.colGritOrderHeaderID1,
            this.colDeliverToLocationID,
            this.colDeliverToLocationTypeID,
            this.colRedirectFromLocationID,
            this.colRedirectFromLocationTypeID,
            this.colRedirectedReason,
            this.colSaltColourOrdered,
            this.colAmountOrdered,
            this.colAmountOrderedDescriptorID,
            this.colAmountOrderedConverted,
            this.colExpectedDeliveryDate,
            this.colActualDeliveryDate,
            this.colAmountDelivered,
            this.colAmountDeliveredDescriptorID,
            this.colAmountDeliveredConverted,
            this.colSaltColourDelivered,
            this.colVatRate,
            this.colUnitCostExVat1,
            this.colTotalCostExVat1,
            this.colUnitCostIncVat1,
            this.colTotalCostIncVat1,
            this.colRemarks1,
            this.colSaltColourOrdered1,
            this.colSaltColourDelivered1,
            this.colAmountOrderedUnitDescriptor,
            this.colAmountOrderedConversionFactor,
            this.colAmountDeliveredUnitDescriptor,
            this.colAmountDeliveredConversionFactor,
            this.colDeliverToLocationName,
            this.colDeliverToLocationType,
            this.colRedirectFromLocationType,
            this.colRedirectFromLocationName,
            this.colGCPONumber1,
            this.colOrderDate1,
            this.colRemainingDays1});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGCPONumber1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colGritOrderLineID
            // 
            this.colGritOrderLineID.Caption = "Order Line ID";
            this.colGritOrderLineID.FieldName = "GritOrderLineID";
            this.colGritOrderLineID.Name = "colGritOrderLineID";
            this.colGritOrderLineID.OptionsColumn.AllowEdit = false;
            this.colGritOrderLineID.OptionsColumn.AllowFocus = false;
            this.colGritOrderLineID.OptionsColumn.ReadOnly = true;
            this.colGritOrderLineID.Width = 85;
            // 
            // colGritOrderHeaderID1
            // 
            this.colGritOrderHeaderID1.Caption = "Order Header ID";
            this.colGritOrderHeaderID1.FieldName = "GritOrderHeaderID";
            this.colGritOrderHeaderID1.Name = "colGritOrderHeaderID1";
            this.colGritOrderHeaderID1.OptionsColumn.AllowEdit = false;
            this.colGritOrderHeaderID1.OptionsColumn.AllowFocus = false;
            this.colGritOrderHeaderID1.OptionsColumn.ReadOnly = true;
            this.colGritOrderHeaderID1.Width = 101;
            // 
            // colDeliverToLocationID
            // 
            this.colDeliverToLocationID.Caption = "Deliver To Location ID";
            this.colDeliverToLocationID.FieldName = "DeliverToLocationID";
            this.colDeliverToLocationID.Name = "colDeliverToLocationID";
            this.colDeliverToLocationID.OptionsColumn.AllowEdit = false;
            this.colDeliverToLocationID.OptionsColumn.AllowFocus = false;
            this.colDeliverToLocationID.OptionsColumn.ReadOnly = true;
            this.colDeliverToLocationID.Width = 126;
            // 
            // colDeliverToLocationTypeID
            // 
            this.colDeliverToLocationTypeID.Caption = "Deliver To Location Type ID";
            this.colDeliverToLocationTypeID.FieldName = "DeliverToLocationTypeID";
            this.colDeliverToLocationTypeID.Name = "colDeliverToLocationTypeID";
            this.colDeliverToLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colDeliverToLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colDeliverToLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colDeliverToLocationTypeID.Width = 153;
            // 
            // colRedirectFromLocationID
            // 
            this.colRedirectFromLocationID.Caption = "Redirect From Location ID";
            this.colRedirectFromLocationID.FieldName = "RedirectFromLocationID";
            this.colRedirectFromLocationID.Name = "colRedirectFromLocationID";
            this.colRedirectFromLocationID.OptionsColumn.AllowEdit = false;
            this.colRedirectFromLocationID.OptionsColumn.AllowFocus = false;
            this.colRedirectFromLocationID.OptionsColumn.ReadOnly = true;
            this.colRedirectFromLocationID.Width = 145;
            // 
            // colRedirectFromLocationTypeID
            // 
            this.colRedirectFromLocationTypeID.Caption = "Redirect From Location Type ID";
            this.colRedirectFromLocationTypeID.FieldName = "RedirectFromLocationTypeID";
            this.colRedirectFromLocationTypeID.Name = "colRedirectFromLocationTypeID";
            this.colRedirectFromLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colRedirectFromLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colRedirectFromLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colRedirectFromLocationTypeID.Width = 172;
            // 
            // colRedirectedReason
            // 
            this.colRedirectedReason.Caption = "Redirected Reason";
            this.colRedirectedReason.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRedirectedReason.FieldName = "RedirectedReason";
            this.colRedirectedReason.Name = "colRedirectedReason";
            this.colRedirectedReason.OptionsColumn.AllowEdit = false;
            this.colRedirectedReason.OptionsColumn.AllowFocus = false;
            this.colRedirectedReason.OptionsColumn.ReadOnly = true;
            this.colRedirectedReason.Visible = true;
            this.colRedirectedReason.VisibleIndex = 18;
            this.colRedirectedReason.Width = 112;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colSaltColourOrdered
            // 
            this.colSaltColourOrdered.Caption = "Ordered Salt Colour ID";
            this.colSaltColourOrdered.FieldName = "SaltColourOrdered";
            this.colSaltColourOrdered.Name = "colSaltColourOrdered";
            this.colSaltColourOrdered.OptionsColumn.AllowEdit = false;
            this.colSaltColourOrdered.OptionsColumn.AllowFocus = false;
            this.colSaltColourOrdered.OptionsColumn.ReadOnly = true;
            this.colSaltColourOrdered.Width = 130;
            // 
            // colAmountOrdered
            // 
            this.colAmountOrdered.Caption = "Ordered Amount";
            this.colAmountOrdered.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colAmountOrdered.FieldName = "AmountOrdered";
            this.colAmountOrdered.Name = "colAmountOrdered";
            this.colAmountOrdered.OptionsColumn.AllowEdit = false;
            this.colAmountOrdered.OptionsColumn.AllowFocus = false;
            this.colAmountOrdered.OptionsColumn.ReadOnly = true;
            this.colAmountOrdered.Visible = true;
            this.colAmountOrdered.VisibleIndex = 4;
            this.colAmountOrdered.Width = 101;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colAmountOrderedDescriptorID
            // 
            this.colAmountOrderedDescriptorID.Caption = "Ordered Amount Descriptor ID";
            this.colAmountOrderedDescriptorID.FieldName = "AmountOrderedDescriptorID";
            this.colAmountOrderedDescriptorID.Name = "colAmountOrderedDescriptorID";
            this.colAmountOrderedDescriptorID.OptionsColumn.AllowEdit = false;
            this.colAmountOrderedDescriptorID.OptionsColumn.AllowFocus = false;
            this.colAmountOrderedDescriptorID.OptionsColumn.ReadOnly = true;
            this.colAmountOrderedDescriptorID.Width = 167;
            // 
            // colAmountOrderedConverted
            // 
            this.colAmountOrderedConverted.Caption = "Ordered Amount Converted";
            this.colAmountOrderedConverted.ColumnEdit = this.repositoryItemTextEdit25KgBag2;
            this.colAmountOrderedConverted.FieldName = "AmountOrderedConverted";
            this.colAmountOrderedConverted.Name = "colAmountOrderedConverted";
            this.colAmountOrderedConverted.OptionsColumn.AllowEdit = false;
            this.colAmountOrderedConverted.OptionsColumn.AllowFocus = false;
            this.colAmountOrderedConverted.OptionsColumn.ReadOnly = true;
            this.colAmountOrderedConverted.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AmountOrderedConverted", "Total: {0:######0.00 25 Kg Bags}")});
            this.colAmountOrderedConverted.Visible = true;
            this.colAmountOrderedConverted.VisibleIndex = 6;
            this.colAmountOrderedConverted.Width = 155;
            // 
            // repositoryItemTextEdit25KgBag2
            // 
            this.repositoryItemTextEdit25KgBag2.AutoHeight = false;
            this.repositoryItemTextEdit25KgBag2.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25KgBag2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KgBag2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KgBag2.Name = "repositoryItemTextEdit25KgBag2";
            // 
            // colExpectedDeliveryDate
            // 
            this.colExpectedDeliveryDate.Caption = "Expected Delivery Date";
            this.colExpectedDeliveryDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colExpectedDeliveryDate.FieldName = "ExpectedDeliveryDate";
            this.colExpectedDeliveryDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedDeliveryDate.Name = "colExpectedDeliveryDate";
            this.colExpectedDeliveryDate.OptionsColumn.AllowEdit = false;
            this.colExpectedDeliveryDate.OptionsColumn.AllowFocus = false;
            this.colExpectedDeliveryDate.OptionsColumn.ReadOnly = true;
            this.colExpectedDeliveryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedDeliveryDate.Visible = true;
            this.colExpectedDeliveryDate.VisibleIndex = 1;
            this.colExpectedDeliveryDate.Width = 134;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colActualDeliveryDate
            // 
            this.colActualDeliveryDate.Caption = "Actual Delivery Date";
            this.colActualDeliveryDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colActualDeliveryDate.FieldName = "ActualDeliveryDate";
            this.colActualDeliveryDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActualDeliveryDate.Name = "colActualDeliveryDate";
            this.colActualDeliveryDate.OptionsColumn.AllowEdit = false;
            this.colActualDeliveryDate.OptionsColumn.AllowFocus = false;
            this.colActualDeliveryDate.OptionsColumn.ReadOnly = true;
            this.colActualDeliveryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActualDeliveryDate.Visible = true;
            this.colActualDeliveryDate.VisibleIndex = 13;
            this.colActualDeliveryDate.Width = 119;
            // 
            // colAmountDelivered
            // 
            this.colAmountDelivered.Caption = "Delivered Amount";
            this.colAmountDelivered.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colAmountDelivered.FieldName = "AmountDelivered";
            this.colAmountDelivered.Name = "colAmountDelivered";
            this.colAmountDelivered.OptionsColumn.AllowEdit = false;
            this.colAmountDelivered.OptionsColumn.AllowFocus = false;
            this.colAmountDelivered.OptionsColumn.ReadOnly = true;
            this.colAmountDelivered.Visible = true;
            this.colAmountDelivered.VisibleIndex = 14;
            this.colAmountDelivered.Width = 106;
            // 
            // colAmountDeliveredDescriptorID
            // 
            this.colAmountDeliveredDescriptorID.Caption = "Delivered Amount Descriptor ID";
            this.colAmountDeliveredDescriptorID.FieldName = "AmountDeliveredDescriptorID";
            this.colAmountDeliveredDescriptorID.Name = "colAmountDeliveredDescriptorID";
            this.colAmountDeliveredDescriptorID.OptionsColumn.AllowEdit = false;
            this.colAmountDeliveredDescriptorID.OptionsColumn.AllowFocus = false;
            this.colAmountDeliveredDescriptorID.OptionsColumn.ReadOnly = true;
            this.colAmountDeliveredDescriptorID.Width = 172;
            // 
            // colAmountDeliveredConverted
            // 
            this.colAmountDeliveredConverted.Caption = "Delivered Amount Converted";
            this.colAmountDeliveredConverted.ColumnEdit = this.repositoryItemTextEdit25KgBag2;
            this.colAmountDeliveredConverted.FieldName = "AmountDeliveredConverted";
            this.colAmountDeliveredConverted.Name = "colAmountDeliveredConverted";
            this.colAmountDeliveredConverted.OptionsColumn.AllowEdit = false;
            this.colAmountDeliveredConverted.OptionsColumn.AllowFocus = false;
            this.colAmountDeliveredConverted.OptionsColumn.ReadOnly = true;
            this.colAmountDeliveredConverted.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AmountDeliveredConverted", "Total: {0:######0.00 25 Kg Bags}")});
            this.colAmountDeliveredConverted.Visible = true;
            this.colAmountDeliveredConverted.VisibleIndex = 16;
            this.colAmountDeliveredConverted.Width = 160;
            // 
            // colSaltColourDelivered
            // 
            this.colSaltColourDelivered.Caption = "Delivered Salt Colour ID";
            this.colSaltColourDelivered.FieldName = "SaltColourDelivered";
            this.colSaltColourDelivered.Name = "colSaltColourDelivered";
            this.colSaltColourDelivered.OptionsColumn.AllowEdit = false;
            this.colSaltColourDelivered.OptionsColumn.AllowFocus = false;
            this.colSaltColourDelivered.OptionsColumn.ReadOnly = true;
            this.colSaltColourDelivered.Width = 135;
            // 
            // colVatRate
            // 
            this.colVatRate.Caption = "VAT Rate";
            this.colVatRate.ColumnEdit = this.repositoryItemTextEditVat2;
            this.colVatRate.FieldName = "VatRate";
            this.colVatRate.Name = "colVatRate";
            this.colVatRate.OptionsColumn.AllowEdit = false;
            this.colVatRate.OptionsColumn.AllowFocus = false;
            this.colVatRate.OptionsColumn.ReadOnly = true;
            this.colVatRate.Visible = true;
            this.colVatRate.VisibleIndex = 7;
            // 
            // repositoryItemTextEditVat2
            // 
            this.repositoryItemTextEditVat2.AutoHeight = false;
            this.repositoryItemTextEditVat2.Mask.EditMask = "P";
            this.repositoryItemTextEditVat2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditVat2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditVat2.Name = "repositoryItemTextEditVat2";
            // 
            // colUnitCostExVat1
            // 
            this.colUnitCostExVat1.Caption = "Unit Cost Ex VAT";
            this.colUnitCostExVat1.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colUnitCostExVat1.FieldName = "UnitCostExVat";
            this.colUnitCostExVat1.Name = "colUnitCostExVat1";
            this.colUnitCostExVat1.OptionsColumn.AllowEdit = false;
            this.colUnitCostExVat1.OptionsColumn.AllowFocus = false;
            this.colUnitCostExVat1.OptionsColumn.ReadOnly = true;
            this.colUnitCostExVat1.Visible = true;
            this.colUnitCostExVat1.VisibleIndex = 8;
            this.colUnitCostExVat1.Width = 102;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colTotalCostExVat1
            // 
            this.colTotalCostExVat1.Caption = "Total Cost Ex VAT";
            this.colTotalCostExVat1.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colTotalCostExVat1.FieldName = "TotalCostExVat";
            this.colTotalCostExVat1.Name = "colTotalCostExVat1";
            this.colTotalCostExVat1.OptionsColumn.AllowEdit = false;
            this.colTotalCostExVat1.OptionsColumn.AllowFocus = false;
            this.colTotalCostExVat1.OptionsColumn.ReadOnly = true;
            this.colTotalCostExVat1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCostExVat", "Total: {0:c2}")});
            this.colTotalCostExVat1.Visible = true;
            this.colTotalCostExVat1.VisibleIndex = 10;
            this.colTotalCostExVat1.Width = 107;
            // 
            // colUnitCostIncVat1
            // 
            this.colUnitCostIncVat1.Caption = "Unit Cost Inc VAT";
            this.colUnitCostIncVat1.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colUnitCostIncVat1.FieldName = "UnitCostIncVat";
            this.colUnitCostIncVat1.Name = "colUnitCostIncVat1";
            this.colUnitCostIncVat1.OptionsColumn.AllowEdit = false;
            this.colUnitCostIncVat1.OptionsColumn.AllowFocus = false;
            this.colUnitCostIncVat1.OptionsColumn.ReadOnly = true;
            this.colUnitCostIncVat1.Visible = true;
            this.colUnitCostIncVat1.VisibleIndex = 9;
            this.colUnitCostIncVat1.Width = 105;
            // 
            // colTotalCostIncVat1
            // 
            this.colTotalCostIncVat1.Caption = "Total Cost Inc VAT";
            this.colTotalCostIncVat1.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colTotalCostIncVat1.FieldName = "TotalCostIncVat";
            this.colTotalCostIncVat1.Name = "colTotalCostIncVat1";
            this.colTotalCostIncVat1.OptionsColumn.AllowEdit = false;
            this.colTotalCostIncVat1.OptionsColumn.AllowFocus = false;
            this.colTotalCostIncVat1.OptionsColumn.ReadOnly = true;
            this.colTotalCostIncVat1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCostIncVat", "Total: {0:c2}")});
            this.colTotalCostIncVat1.Visible = true;
            this.colTotalCostIncVat1.VisibleIndex = 11;
            this.colTotalCostIncVat1.Width = 110;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.AllowEdit = false;
            this.colRemarks1.OptionsColumn.AllowFocus = false;
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 17;
            // 
            // colSaltColourOrdered1
            // 
            this.colSaltColourOrdered1.Caption = "Ordered Salt Colour";
            this.colSaltColourOrdered1.FieldName = "SaltColourOrdered1";
            this.colSaltColourOrdered1.Name = "colSaltColourOrdered1";
            this.colSaltColourOrdered1.OptionsColumn.AllowEdit = false;
            this.colSaltColourOrdered1.OptionsColumn.AllowFocus = false;
            this.colSaltColourOrdered1.OptionsColumn.ReadOnly = true;
            this.colSaltColourOrdered1.Visible = true;
            this.colSaltColourOrdered1.VisibleIndex = 3;
            this.colSaltColourOrdered1.Width = 116;
            // 
            // colSaltColourDelivered1
            // 
            this.colSaltColourDelivered1.Caption = "Delivered Salt Colour";
            this.colSaltColourDelivered1.FieldName = "SaltColourDelivered1";
            this.colSaltColourDelivered1.Name = "colSaltColourDelivered1";
            this.colSaltColourDelivered1.OptionsColumn.AllowEdit = false;
            this.colSaltColourDelivered1.OptionsColumn.AllowFocus = false;
            this.colSaltColourDelivered1.OptionsColumn.ReadOnly = true;
            this.colSaltColourDelivered1.Visible = true;
            this.colSaltColourDelivered1.VisibleIndex = 12;
            this.colSaltColourDelivered1.Width = 121;
            // 
            // colAmountOrderedUnitDescriptor
            // 
            this.colAmountOrderedUnitDescriptor.Caption = "Ordered Amount Unit Descriptor";
            this.colAmountOrderedUnitDescriptor.FieldName = "AmountOrderedUnitDescriptor";
            this.colAmountOrderedUnitDescriptor.Name = "colAmountOrderedUnitDescriptor";
            this.colAmountOrderedUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colAmountOrderedUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colAmountOrderedUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colAmountOrderedUnitDescriptor.Visible = true;
            this.colAmountOrderedUnitDescriptor.VisibleIndex = 5;
            this.colAmountOrderedUnitDescriptor.Width = 175;
            // 
            // colAmountOrderedConversionFactor
            // 
            this.colAmountOrderedConversionFactor.Caption = "Ordered Amount Conversion Factor";
            this.colAmountOrderedConversionFactor.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colAmountOrderedConversionFactor.FieldName = "AmountOrderedConversionFactor";
            this.colAmountOrderedConversionFactor.Name = "colAmountOrderedConversionFactor";
            this.colAmountOrderedConversionFactor.OptionsColumn.AllowEdit = false;
            this.colAmountOrderedConversionFactor.OptionsColumn.AllowFocus = false;
            this.colAmountOrderedConversionFactor.OptionsColumn.ReadOnly = true;
            this.colAmountOrderedConversionFactor.Width = 192;
            // 
            // colAmountDeliveredUnitDescriptor
            // 
            this.colAmountDeliveredUnitDescriptor.Caption = "Delivered Amount Unit Descriptor";
            this.colAmountDeliveredUnitDescriptor.FieldName = "AmountDeliveredUnitDescriptor";
            this.colAmountDeliveredUnitDescriptor.Name = "colAmountDeliveredUnitDescriptor";
            this.colAmountDeliveredUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colAmountDeliveredUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colAmountDeliveredUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colAmountDeliveredUnitDescriptor.Visible = true;
            this.colAmountDeliveredUnitDescriptor.VisibleIndex = 15;
            this.colAmountDeliveredUnitDescriptor.Width = 180;
            // 
            // colAmountDeliveredConversionFactor
            // 
            this.colAmountDeliveredConversionFactor.Caption = "Delivered Amount Conversion Factor";
            this.colAmountDeliveredConversionFactor.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colAmountDeliveredConversionFactor.FieldName = "AmountDeliveredConversionFactor";
            this.colAmountDeliveredConversionFactor.Name = "colAmountDeliveredConversionFactor";
            this.colAmountDeliveredConversionFactor.OptionsColumn.AllowEdit = false;
            this.colAmountDeliveredConversionFactor.OptionsColumn.AllowFocus = false;
            this.colAmountDeliveredConversionFactor.OptionsColumn.ReadOnly = true;
            this.colAmountDeliveredConversionFactor.Width = 197;
            // 
            // colDeliverToLocationName
            // 
            this.colDeliverToLocationName.Caption = "Deliver To Location Name";
            this.colDeliverToLocationName.FieldName = "DeliverToLocationName";
            this.colDeliverToLocationName.Name = "colDeliverToLocationName";
            this.colDeliverToLocationName.OptionsColumn.AllowEdit = false;
            this.colDeliverToLocationName.OptionsColumn.AllowFocus = false;
            this.colDeliverToLocationName.OptionsColumn.ReadOnly = true;
            this.colDeliverToLocationName.Visible = true;
            this.colDeliverToLocationName.VisibleIndex = 0;
            this.colDeliverToLocationName.Width = 173;
            // 
            // colDeliverToLocationType
            // 
            this.colDeliverToLocationType.Caption = "Deliver To Location Type";
            this.colDeliverToLocationType.FieldName = "DeliverToLocationType";
            this.colDeliverToLocationType.Name = "colDeliverToLocationType";
            this.colDeliverToLocationType.OptionsColumn.AllowEdit = false;
            this.colDeliverToLocationType.OptionsColumn.AllowFocus = false;
            this.colDeliverToLocationType.OptionsColumn.ReadOnly = true;
            this.colDeliverToLocationType.Width = 139;
            // 
            // colRedirectFromLocationType
            // 
            this.colRedirectFromLocationType.Caption = "Redirect From Location Type";
            this.colRedirectFromLocationType.FieldName = "RedirectFromLocationType";
            this.colRedirectFromLocationType.Name = "colRedirectFromLocationType";
            this.colRedirectFromLocationType.OptionsColumn.AllowEdit = false;
            this.colRedirectFromLocationType.OptionsColumn.AllowFocus = false;
            this.colRedirectFromLocationType.OptionsColumn.ReadOnly = true;
            this.colRedirectFromLocationType.Visible = true;
            this.colRedirectFromLocationType.VisibleIndex = 19;
            this.colRedirectFromLocationType.Width = 158;
            // 
            // colRedirectFromLocationName
            // 
            this.colRedirectFromLocationName.Caption = "Redirect From Location";
            this.colRedirectFromLocationName.FieldName = "RedirectFromLocationName";
            this.colRedirectFromLocationName.Name = "colRedirectFromLocationName";
            this.colRedirectFromLocationName.OptionsColumn.AllowEdit = false;
            this.colRedirectFromLocationName.OptionsColumn.AllowFocus = false;
            this.colRedirectFromLocationName.OptionsColumn.ReadOnly = true;
            this.colRedirectFromLocationName.Visible = true;
            this.colRedirectFromLocationName.VisibleIndex = 20;
            this.colRedirectFromLocationName.Width = 131;
            // 
            // colGCPONumber1
            // 
            this.colGCPONumber1.Caption = "GC PO Number";
            this.colGCPONumber1.FieldName = "GCPONumber";
            this.colGCPONumber1.Name = "colGCPONumber1";
            this.colGCPONumber1.OptionsColumn.AllowEdit = false;
            this.colGCPONumber1.OptionsColumn.AllowFocus = false;
            this.colGCPONumber1.OptionsColumn.ReadOnly = true;
            this.colGCPONumber1.Width = 92;
            // 
            // colOrderDate1
            // 
            this.colOrderDate1.Caption = "Order Date";
            this.colOrderDate1.FieldName = "OrderDate";
            this.colOrderDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colOrderDate1.Name = "colOrderDate1";
            this.colOrderDate1.OptionsColumn.AllowEdit = false;
            this.colOrderDate1.OptionsColumn.AllowFocus = false;
            this.colOrderDate1.OptionsColumn.ReadOnly = true;
            this.colOrderDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            // 
            // colRemainingDays1
            // 
            this.colRemainingDays1.Caption = "Days Until Due";
            this.colRemainingDays1.FieldName = "RemainingDays";
            this.colRemainingDays1.Name = "colRemainingDays1";
            this.colRemainingDays1.OptionsColumn.AllowEdit = false;
            this.colRemainingDays1.OptionsColumn.AllowFocus = false;
            this.colRemainingDays1.OptionsColumn.ReadOnly = true;
            this.colRemainingDays1.Visible = true;
            this.colRemainingDays1.VisibleIndex = 2;
            this.colRemainingDays1.Width = 91;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(834, 154);
            this.xtraTabPage2.Text = "Linked Documents";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl2.Size = new System.Drawing.Size(834, 154);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(839, 537);
            this.splitContainerControl2.SplitterPosition = 30;
            this.splitContainerControl2.TabIndex = 6;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.CustomFilterTextEdit);
            this.layoutControl1.Controls.Add(this.btnLoad);
            this.layoutControl1.Controls.Add(this.popupContainerEdit2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(839, 30);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // CustomFilterTextEdit
            // 
            this.CustomFilterTextEdit.EditValue = "Normal";
            this.CustomFilterTextEdit.Location = new System.Drawing.Point(660, 4);
            this.CustomFilterTextEdit.MenuManager = this.barManager1;
            this.CustomFilterTextEdit.Name = "CustomFilterTextEdit";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem3.Text = "Clear Custom Filter - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = resources.GetString("toolTipItem3.Text");
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.CustomFilterTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "clear", superToolTip3, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CustomFilterTextEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CustomFilterTextEdit.Size = new System.Drawing.Size(109, 20);
            this.CustomFilterTextEdit.StyleController = this.layoutControl1;
            this.CustomFilterTextEdit.TabIndex = 13;
            this.CustomFilterTextEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CustomFilterTextEdit_ButtonClick);
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnLoad.Location = new System.Drawing.Point(773, 4);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(62, 22);
            this.btnLoad.StyleController = this.layoutControl1;
            this.btnLoad.TabIndex = 8;
            this.btnLoad.Text = "Load";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Order Status Filter";
            this.popupContainerEdit2.Location = new System.Drawing.Point(73, 4);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControlLinkedGrittingCalloutsFilter;
            this.popupContainerEdit2.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit2.Size = new System.Drawing.Size(514, 20);
            this.popupContainerEdit2.StyleController = this.layoutControl1;
            this.popupContainerEdit2.TabIndex = 7;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem6});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(839, 30);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.popupContainerEdit2;
            this.layoutControlItem1.CustomizationFormText = "Callout Status:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(587, 26);
            this.layoutControlItem1.Text = "Order Status:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(66, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnLoad;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(769, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(66, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(66, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(66, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.CustomFilterTextEdit;
            this.layoutControlItem6.CustomizationFormText = "Status:";
            this.layoutControlItem6.Location = new System.Drawing.Point(587, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(182, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(182, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(182, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Status:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(66, 13);
            // 
            // sp04220_GC_Salt_Order_StatusesTableAdapter
            // 
            this.sp04220_GC_Salt_Order_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04221_GC_Salt_Order_Manager_ListTableAdapter
            // 
            this.sp04221_GC_Salt_Order_Manager_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04223_GC_Salt_Order_LinesTableAdapter
            // 
            this.sp04223_GC_Salt_Order_LinesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Salt_Order_Manager
            // 
            this.ClientSize = new System.Drawing.Size(839, 537);
            this.Controls.Add(this.splitContainerControl2);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Salt_Order_Manager";
            this.Text = "Salt Order Manager";
            this.Activated += new System.EventHandler(this.frm_GC_Salt_Order_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Salt_Order_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Salt_Order_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04221GCSaltOrderManagerListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgbags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).EndInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04220GCSaltOrderStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04223GCSaltOrderLinesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBag2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVat2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustomFilterTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLinkedGrittingCalloutsFilter;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnGritCalloutFilterOK;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25kgbags;
        private System.Windows.Forms.BindingSource sp04220GCSaltOrderStatusesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04220_GC_Salt_Order_StatusesTableAdapter sp04220_GC_Salt_Order_StatusesTableAdapter;
        private System.Windows.Forms.BindingSource sp04221GCSaltOrderManagerListBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04221_GC_Salt_Order_Manager_ListTableAdapter sp04221_GC_Salt_Order_Manager_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOrderHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colPlacedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedDeliveryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountConverted;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCostVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCostExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCostIncVat;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCostExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitCostExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCostIncVat;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitCostIncVat;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colGCPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSupplierName;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderLineCount;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderStatusDescription;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp04223GCSaltOrderLinesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOrderLineID;
        private DevExpress.XtraGrid.Columns.GridColumn colGritOrderHeaderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliverToLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliverToLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectFromLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectFromLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectedReason;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltColourOrdered;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountOrdered;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountOrderedDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountOrderedConverted;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDeliveryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDeliveryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountDelivered;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountDeliveredDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountDeliveredConverted;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltColourDelivered;
        private DevExpress.XtraGrid.Columns.GridColumn colVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitCostExVat1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCostExVat1;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitCostIncVat1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCostIncVat1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltColourOrdered1;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltColourDelivered1;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountOrderedUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountOrderedConversionFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountDeliveredUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountDeliveredConversionFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliverToLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colDeliverToLocationType;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectFromLocationType;
        private DataSet_GC_CoreTableAdapters.sp04223_GC_Salt_Order_LinesTableAdapter sp04223_GC_Salt_Order_LinesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KgBag2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditVat2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraGrid.Columns.GridColumn colRedirectFromLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colGCPONumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemainingDays;
        private DevExpress.XtraGrid.Columns.GridColumn colRemainingDays1;
        private DevExpress.XtraEditors.ButtonEdit CustomFilterTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}

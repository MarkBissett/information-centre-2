﻿namespace WoodPlan5
{
    partial class frm_GC_Finance_Invoice_Jobs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Finance_Invoice_Jobs));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlParameters = new DevExpress.XtraEditors.PopupContainerControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEditIncludeCompleted = new DevExpress.XtraEditors.CheckEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp04237GCCompanyFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOK2 = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04258GCFinanceClientInvoiceLinesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientInvoiceLineID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colRaisedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFinanceInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colSnowClearanceJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentToAccounts = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colSentToClientMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentToClientMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeSent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAudited = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04259GCFinanceGritCalloutsForClientInvoiceLineBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGrittingCallOutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGrittingContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorIsDirectLabour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colOriginalSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContarctorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colImportedWeatherForecastID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextSentTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorReceivedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorRespondedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorETA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitAborted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbortedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishedLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishedLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25KgBags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSaltCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSaltSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercent = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHoursWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTeamHourlyRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamCharge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractorReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSourceLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSourceLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteWeather = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTemperature = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamPresent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceFailure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritJobTransferMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarkup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClientReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritMobileTelephoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceLineID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceLineDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp04260GCFinanceSnowCalloutManagerForClientInvoiceLineBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSnowClearanceCallOutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCPONumberSuffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorContactedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientHowSoon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccessAbortedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamPOFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimesheetSubmitted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceLineID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceLineDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04265GCFinanceTeamJobsForExportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colUniqueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteWeather1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTemperature1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowOnSite1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colVisitAborted1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccess1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUsed1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25KgBags2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSaltSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOtherSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiClientInvoiceWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClientSpreadsheets = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemParameters = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditParameters = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiReloadData = new DevExpress.XtraBars.BarButtonItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp04237_GC_Company_Filter_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter();
            this.sp04258_GC_Finance_Client_Invoice_LinesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04258_GC_Finance_Client_Invoice_LinesTableAdapter();
            this.sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_LineTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_LineTableAdapter();
            this.sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_LineTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_LineTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiFinanceTeamSpreadsheets = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPaidComplete = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemParameters2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditCompanyFilter2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiReloadData2 = new DevExpress.XtraBars.BarButtonItem();
            this.sp04265_GC_Finance_Team_Jobs_For_ExportTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04265_GC_Finance_Team_Jobs_For_ExportTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlParameters)).BeginInit();
            this.popupContainerControlParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIncludeCompleted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04258GCFinanceClientInvoiceLinesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04259GCFinanceGritCalloutsForClientInvoiceLineBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04260GCFinanceSnowCalloutManagerForClientInvoiceLineBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04265GCFinanceTeamJobsForExportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1046, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 542);
            this.barDockControlBottom.Size = new System.Drawing.Size(1046, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 542);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1046, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 542);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemParameters,
            this.bbiReloadData,
            this.bbiClientInvoiceWizard,
            this.bbiClientSpreadsheets,
            this.bbiReloadData2,
            this.barEditItemParameters2,
            this.bbiFinanceTeamSpreadsheets,
            this.bbiPaidComplete});
            this.barManager1.MaxItemId = 35;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditParameters,
            this.repositoryItemPopupContainerEditCompanyFilter2});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1046, 542);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1041, 516);
            this.xtraTabPage1.Text = "Client Billing";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlParameters);
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Saved Invoice Lines";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Invoice Line Contents";
            this.splitContainerControl1.Size = new System.Drawing.Size(1041, 516);
            this.splitContainerControl1.SplitterPosition = 540;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // popupContainerControlParameters
            // 
            this.popupContainerControlParameters.Controls.Add(this.layoutControl2);
            this.popupContainerControlParameters.Controls.Add(this.btnOK2);
            this.popupContainerControlParameters.Location = new System.Drawing.Point(9, 159);
            this.popupContainerControlParameters.Name = "popupContainerControlParameters";
            this.popupContainerControlParameters.Size = new System.Drawing.Size(272, 302);
            this.popupContainerControlParameters.TabIndex = 15;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.Controls.Add(this.checkEditIncludeCompleted);
            this.layoutControl2.Controls.Add(this.gridControl2);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(997, 103, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(272, 277);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // checkEditIncludeCompleted
            // 
            this.checkEditIncludeCompleted.Location = new System.Drawing.Point(103, 55);
            this.checkEditIncludeCompleted.MenuManager = this.barManager1;
            this.checkEditIncludeCompleted.Name = "checkEditIncludeCompleted";
            this.checkEditIncludeCompleted.Properties.Caption = "(Tick if Yes)";
            this.checkEditIncludeCompleted.Properties.ValueChecked = 1;
            this.checkEditIncludeCompleted.Properties.ValueUnchecked = 0;
            this.checkEditIncludeCompleted.Size = new System.Drawing.Size(162, 19);
            this.checkEditIncludeCompleted.StyleController = this.layoutControl2;
            this.checkEditIncludeCompleted.TabIndex = 15;
            this.checkEditIncludeCompleted.ToolTip = "Client Billing Only  [Not used by Team Billing]";
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp04237GCCompanyFilterListBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(15, 118);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(242, 144);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp04237GCCompanyFilterListBindingSource
            // 
            this.sp04237GCCompanyFilterListBindingSource.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompanyID,
            this.colCompanyName,
            this.colCompanyCode,
            this.colCompanyOrder});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCompanyID
            // 
            this.colCompanyID.Caption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 80;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 0;
            this.colCompanyName.Width = 183;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyOrder
            // 
            this.colCompanyOrder.Caption = "Order";
            this.colCompanyOrder.FieldName = "CompanyOrder";
            this.colCompanyOrder.Name = "colCompanyOrder";
            this.colCompanyOrder.OptionsColumn.AllowEdit = false;
            this.colCompanyOrder.OptionsColumn.AllowFocus = false;
            this.colCompanyOrder.OptionsColumn.ReadOnly = true;
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Location = new System.Drawing.Point(103, 7);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem3.Text = "Clear Date - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Jo" +
    "bs will be loaded.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip3, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(162, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 14;
            this.dateEditFromDate.ToolTip = "Client Billing Only  [Not used by Team Billing]";
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(103, 31);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Clear Date - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip4, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(162, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 12;
            this.dateEditToDate.ToolTip = "Client Billing Only  [Not used by Team Billing]";
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.layoutControlItem3});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(272, 277);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDate;
            this.layoutControlItem1.CustomizationFormText = "From Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem1.Text = "From Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDate;
            this.layoutControlItem2.CustomizationFormText = "To Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(112, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "To Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Company Filter";
            this.layoutControlGroup1.ExpandButtonVisible = true;
            this.layoutControlGroup1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 81);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(262, 186);
            this.layoutControlGroup1.Text = "Company Filter";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl2;
            this.layoutControlItem4.CustomizationFormText = "Company Filter:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(246, 148);
            this.layoutControlItem4.Text = "Company Filter:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(262, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkEditIncludeCompleted;
            this.layoutControlItem3.CustomizationFormText = "Include Completed:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(262, 23);
            this.layoutControlItem3.Text = "Include Completed:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(93, 13);
            // 
            // btnOK2
            // 
            this.btnOK2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK2.Location = new System.Drawing.Point(3, 277);
            this.btnOK2.Name = "btnOK2";
            this.btnOK2.Size = new System.Drawing.Size(50, 22);
            this.btnOK2.TabIndex = 18;
            this.btnOK2.Text = "OK";
            this.btnOK2.Click += new System.EventHandler(this.btnOK2_Click);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(536, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp04258GCFinanceClientInvoiceLinesBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Set Finance Invoice Number for Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 42);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditCurrency,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemDateEdit1,
            this.repositoryItemCheckEdit4});
            this.gridControl1.Size = new System.Drawing.Size(536, 450);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04258GCFinanceClientInvoiceLinesBindingSource
            // 
            this.sp04258GCFinanceClientInvoiceLinesBindingSource.DataMember = "sp04258_GC_Finance_Client_Invoice_Lines";
            this.sp04258GCFinanceClientInvoiceLinesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientInvoiceLineID,
            this.colInvoiceGroupID,
            this.colDateRaised,
            this.colRaisedByStaffID,
            this.colRaisedByStaffName,
            this.colLineValue,
            this.colFinanceInvoiceNumber,
            this.colRemarks,
            this.colClientName,
            this.colClientCode,
            this.colGrittingJobCount,
            this.colSnowClearanceJobCount,
            this.colClientID1,
            this.colSentToAccounts,
            this.colSentToClientMethodID,
            this.colSentToClientMethod,
            this.colDateTimeSent,
            this.colAudited,
            this.colOnHoldReasonID,
            this.colOnHoldReason});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colClientInvoiceLineID
            // 
            this.colClientInvoiceLineID.Caption = "Client Invoice Line ID";
            this.colClientInvoiceLineID.FieldName = "ClientInvoiceLineID";
            this.colClientInvoiceLineID.Name = "colClientInvoiceLineID";
            this.colClientInvoiceLineID.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceLineID.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceLineID.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceLineID.Width = 122;
            // 
            // colInvoiceGroupID
            // 
            this.colInvoiceGroupID.Caption = "Invoice Group ID";
            this.colInvoiceGroupID.FieldName = "InvoiceGroupID";
            this.colInvoiceGroupID.Name = "colInvoiceGroupID";
            this.colInvoiceGroupID.OptionsColumn.AllowEdit = false;
            this.colInvoiceGroupID.OptionsColumn.AllowFocus = false;
            this.colInvoiceGroupID.OptionsColumn.ReadOnly = true;
            this.colInvoiceGroupID.Width = 102;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemDateEdit1;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 0;
            this.colDateRaised.Width = 120;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colRaisedByStaffID
            // 
            this.colRaisedByStaffID.Caption = "Raised By Staff ID";
            this.colRaisedByStaffID.FieldName = "RaisedByStaffID";
            this.colRaisedByStaffID.Name = "colRaisedByStaffID";
            this.colRaisedByStaffID.OptionsColumn.AllowEdit = false;
            this.colRaisedByStaffID.OptionsColumn.AllowFocus = false;
            this.colRaisedByStaffID.OptionsColumn.ReadOnly = true;
            this.colRaisedByStaffID.Width = 109;
            // 
            // colRaisedByStaffName
            // 
            this.colRaisedByStaffName.Caption = "Raised By Staff Name";
            this.colRaisedByStaffName.FieldName = "RaisedByStaffName";
            this.colRaisedByStaffName.Name = "colRaisedByStaffName";
            this.colRaisedByStaffName.OptionsColumn.AllowEdit = false;
            this.colRaisedByStaffName.OptionsColumn.AllowFocus = false;
            this.colRaisedByStaffName.OptionsColumn.ReadOnly = true;
            this.colRaisedByStaffName.Visible = true;
            this.colRaisedByStaffName.VisibleIndex = 5;
            this.colRaisedByStaffName.Width = 125;
            // 
            // colLineValue
            // 
            this.colLineValue.Caption = "Line Value";
            this.colLineValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colLineValue.FieldName = "LineValue";
            this.colLineValue.Name = "colLineValue";
            this.colLineValue.OptionsColumn.AllowEdit = false;
            this.colLineValue.OptionsColumn.AllowFocus = false;
            this.colLineValue.OptionsColumn.ReadOnly = true;
            this.colLineValue.Visible = true;
            this.colLineValue.VisibleIndex = 1;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colFinanceInvoiceNumber
            // 
            this.colFinanceInvoiceNumber.Caption = "Finance Invoice Number";
            this.colFinanceInvoiceNumber.FieldName = "FinanceInvoiceNumber";
            this.colFinanceInvoiceNumber.Name = "colFinanceInvoiceNumber";
            this.colFinanceInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colFinanceInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colFinanceInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colFinanceInvoiceNumber.Visible = true;
            this.colFinanceInvoiceNumber.VisibleIndex = 2;
            this.colFinanceInvoiceNumber.Width = 136;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 11;
            this.colRemarks.Width = 107;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 245;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Width = 76;
            // 
            // colGrittingJobCount
            // 
            this.colGrittingJobCount.Caption = "Gritting Jobs";
            this.colGrittingJobCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colGrittingJobCount.FieldName = "GrittingJobCount";
            this.colGrittingJobCount.Name = "colGrittingJobCount";
            this.colGrittingJobCount.OptionsColumn.ReadOnly = true;
            this.colGrittingJobCount.Visible = true;
            this.colGrittingJobCount.VisibleIndex = 3;
            this.colGrittingJobCount.Width = 81;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colSnowClearanceJobCount
            // 
            this.colSnowClearanceJobCount.Caption = "Snow Clearance Jobs";
            this.colSnowClearanceJobCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colSnowClearanceJobCount.FieldName = "SnowClearanceJobCount";
            this.colSnowClearanceJobCount.Name = "colSnowClearanceJobCount";
            this.colSnowClearanceJobCount.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceJobCount.Visible = true;
            this.colSnowClearanceJobCount.VisibleIndex = 4;
            this.colSnowClearanceJobCount.Width = 123;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colSentToAccounts
            // 
            this.colSentToAccounts.Caption = "Sent To Accounts";
            this.colSentToAccounts.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colSentToAccounts.FieldName = "SentToAccounts";
            this.colSentToAccounts.Name = "colSentToAccounts";
            this.colSentToAccounts.OptionsColumn.AllowEdit = false;
            this.colSentToAccounts.OptionsColumn.AllowFocus = false;
            this.colSentToAccounts.OptionsColumn.ReadOnly = true;
            this.colSentToAccounts.Visible = true;
            this.colSentToAccounts.VisibleIndex = 6;
            this.colSentToAccounts.Width = 103;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colSentToClientMethodID
            // 
            this.colSentToClientMethodID.Caption = "Sent To Client Method ID";
            this.colSentToClientMethodID.FieldName = "SentToClientMethodID";
            this.colSentToClientMethodID.Name = "colSentToClientMethodID";
            this.colSentToClientMethodID.OptionsColumn.AllowEdit = false;
            this.colSentToClientMethodID.OptionsColumn.AllowFocus = false;
            this.colSentToClientMethodID.OptionsColumn.ReadOnly = true;
            this.colSentToClientMethodID.Width = 139;
            // 
            // colSentToClientMethod
            // 
            this.colSentToClientMethod.Caption = "Sent To Client Method";
            this.colSentToClientMethod.FieldName = "SentToClientMethod";
            this.colSentToClientMethod.Name = "colSentToClientMethod";
            this.colSentToClientMethod.OptionsColumn.AllowEdit = false;
            this.colSentToClientMethod.OptionsColumn.AllowFocus = false;
            this.colSentToClientMethod.OptionsColumn.ReadOnly = true;
            this.colSentToClientMethod.Visible = true;
            this.colSentToClientMethod.VisibleIndex = 7;
            this.colSentToClientMethod.Width = 159;
            // 
            // colDateTimeSent
            // 
            this.colDateTimeSent.Caption = "Date \\ Time Sent";
            this.colDateTimeSent.ColumnEdit = this.repositoryItemDateEdit1;
            this.colDateTimeSent.FieldName = "DateTimeSent";
            this.colDateTimeSent.Name = "colDateTimeSent";
            this.colDateTimeSent.OptionsColumn.AllowEdit = false;
            this.colDateTimeSent.OptionsColumn.AllowFocus = false;
            this.colDateTimeSent.OptionsColumn.ReadOnly = true;
            this.colDateTimeSent.Visible = true;
            this.colDateTimeSent.VisibleIndex = 8;
            this.colDateTimeSent.Width = 120;
            // 
            // colAudited
            // 
            this.colAudited.Caption = "Audited";
            this.colAudited.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colAudited.FieldName = "Audited";
            this.colAudited.Name = "colAudited";
            this.colAudited.OptionsColumn.AllowEdit = false;
            this.colAudited.OptionsColumn.AllowFocus = false;
            this.colAudited.OptionsColumn.ReadOnly = true;
            this.colAudited.Visible = true;
            this.colAudited.VisibleIndex = 9;
            this.colAudited.Width = 56;
            // 
            // colOnHoldReasonID
            // 
            this.colOnHoldReasonID.Caption = "On-Hold Reason ID";
            this.colOnHoldReasonID.FieldName = "OnHoldReasonID";
            this.colOnHoldReasonID.Name = "colOnHoldReasonID";
            this.colOnHoldReasonID.OptionsColumn.AllowEdit = false;
            this.colOnHoldReasonID.OptionsColumn.AllowFocus = false;
            this.colOnHoldReasonID.OptionsColumn.ReadOnly = true;
            this.colOnHoldReasonID.Width = 111;
            // 
            // colOnHoldReason
            // 
            this.colOnHoldReason.Caption = "On-Hold Reason";
            this.colOnHoldReason.FieldName = "OnHoldReason";
            this.colOnHoldReason.Name = "colOnHoldReason";
            this.colOnHoldReason.OptionsColumn.AllowEdit = false;
            this.colOnHoldReason.OptionsColumn.AllowFocus = false;
            this.colOnHoldReason.OptionsColumn.ReadOnly = true;
            this.colOnHoldReason.Visible = true;
            this.colOnHoldReason.VisibleIndex = 10;
            this.colOnHoldReason.Width = 136;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl3);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Gritting Jobs";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl4);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Snow Clearance Jobs";
            this.splitContainerControl2.Size = new System.Drawing.Size(491, 492);
            this.splitContainerControl2.SplitterPosition = 240;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp04259GCFinanceGritCalloutsForClientInvoiceLineBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Remove Selected Record(s) from Saved Invoice Line(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit25KgBags,
            this.repositoryItemTextEditCurrency3,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditPercent});
            this.gridControl3.Size = new System.Drawing.Size(466, 237);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp04259GCFinanceGritCalloutsForClientInvoiceLineBindingSource
            // 
            this.sp04259GCFinanceGritCalloutsForClientInvoiceLineBindingSource.DataMember = "sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_Line";
            this.sp04259GCFinanceGritCalloutsForClientInvoiceLineBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGrittingCallOutID,
            this.colSiteGrittingContractID,
            this.colSiteID,
            this.colSiteName,
            this.colClientID,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.colSiteXCoordinate,
            this.colSiteYCoordinate,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colSiteTelephone,
            this.colSiteEmail,
            this.colClientsSiteCode,
            this.gridColumn10,
            this.colSubContractorName,
            this.colSubContractorIsDirectLabour,
            this.colOriginalSubContractorID,
            this.colOriginalSubContarctorName,
            this.colReactive,
            this.colJobStatusID,
            this.colCalloutStatusDescription,
            this.colCalloutStatusOrder,
            this.colCallOutDateTime,
            this.colImportedWeatherForecastID,
            this.colTextSentTime,
            this.colSubContractorReceivedTime,
            this.colSubContractorRespondedTime,
            this.colSubContractorETA,
            this.colCompletedTime,
            this.colAuthorisedByStaffID,
            this.colAuthorisedByName,
            this.colVisitAborted,
            this.colAbortedReason,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishedLatitude,
            this.colFinishedLongitude,
            this.colClientPONumber,
            this.colSaltUsed,
            this.colSaltCost,
            this.colSaltSell,
            this.colSaltVatRate,
            this.colHoursWorked,
            this.colTeamHourlyRate,
            this.colTeamCharge,
            this.colLabourCost,
            this.colLabourVatRate,
            this.colOtherCost,
            this.colOtherSell,
            this.colTotalCost,
            this.colTotalSell,
            this.colClientInvoiceNumber,
            this.colSubContractorPaid,
            this.colGrittingInvoiceID,
            this.colRecordedByStaffID,
            this.colRecordedByName,
            this.colPaidByStaffID,
            this.colPaidByName,
            this.colDoNotPaySubContractor,
            this.colDoNotPaySubContractorReason,
            this.colGritSourceLocationID,
            this.colGritSourceLocationTypeID,
            this.colNoAccess,
            this.colSiteWeather,
            this.colSiteTemperature,
            this.colPdaID,
            this.colTeamPresent,
            this.colServiceFailure,
            this.colComments,
            this.colClientPrice,
            this.colGritJobTransferMethod,
            this.colSnowOnSite,
            this.colStartTime,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn19,
            this.colProfit,
            this.colMarkup,
            this.colClientPOID,
            this.colNonStandardCost,
            this.colNonStandardSell,
            this.colDoNotInvoiceClient,
            this.colDoNotInvoiceClientReason,
            this.colGritMobileTelephoneNumber,
            this.colSelfBillingInvoice,
            this.colClientInvoiceLineID1,
            this.colClientInvoiceLineDescription});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientInvoiceLineDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCallOutDateTime, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colGrittingCallOutID
            // 
            this.colGrittingCallOutID.Caption = "Callout ID";
            this.colGrittingCallOutID.FieldName = "GrittingCallOutID";
            this.colGrittingCallOutID.Name = "colGrittingCallOutID";
            this.colGrittingCallOutID.OptionsColumn.AllowEdit = false;
            this.colGrittingCallOutID.OptionsColumn.AllowFocus = false;
            this.colGrittingCallOutID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteGrittingContractID
            // 
            this.colSiteGrittingContractID.Caption = "Site Gritting Contract ID";
            this.colSiteGrittingContractID.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID.Name = "colSiteGrittingContractID";
            this.colSiteGrittingContractID.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID.Width = 136;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 173;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Client Name";
            this.gridColumn1.FieldName = "ClientName";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 3;
            this.gridColumn1.Width = 108;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Company ID";
            this.gridColumn2.FieldName = "CompanyID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 80;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Company Name";
            this.gridColumn3.FieldName = "CompanyName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 5;
            this.gridColumn3.Width = 107;
            // 
            // colSiteXCoordinate
            // 
            this.colSiteXCoordinate.Caption = "Site X Coord.";
            this.colSiteXCoordinate.FieldName = "SiteXCoordinate";
            this.colSiteXCoordinate.Name = "colSiteXCoordinate";
            this.colSiteXCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteXCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteXCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteXCoordinate.Width = 84;
            // 
            // colSiteYCoordinate
            // 
            this.colSiteYCoordinate.Caption = "Site Y Coord.";
            this.colSiteYCoordinate.FieldName = "SiteYCoordinate";
            this.colSiteYCoordinate.Name = "colSiteYCoordinate";
            this.colSiteYCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteYCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteYCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteYCoordinate.Width = 84;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Location X";
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 91;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Location Y";
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 91;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Site Telephone";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Width = 92;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Site Email";
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.AllowEdit = false;
            this.colSiteEmail.OptionsColumn.AllowFocus = false;
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Width = 102;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Team ID";
            this.gridColumn10.FieldName = "SubContractorID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Team Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 4;
            this.colSubContractorName.Width = 218;
            // 
            // colSubContractorIsDirectLabour
            // 
            this.colSubContractorIsDirectLabour.Caption = "Is Direct Labour";
            this.colSubContractorIsDirectLabour.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSubContractorIsDirectLabour.FieldName = "SubContractorIsDirectLabour";
            this.colSubContractorIsDirectLabour.Name = "colSubContractorIsDirectLabour";
            this.colSubContractorIsDirectLabour.OptionsColumn.AllowEdit = false;
            this.colSubContractorIsDirectLabour.OptionsColumn.AllowFocus = false;
            this.colSubContractorIsDirectLabour.OptionsColumn.ReadOnly = true;
            this.colSubContractorIsDirectLabour.Visible = true;
            this.colSubContractorIsDirectLabour.VisibleIndex = 14;
            this.colSubContractorIsDirectLabour.Width = 97;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colOriginalSubContractorID
            // 
            this.colOriginalSubContractorID.Caption = "Original Team ID";
            this.colOriginalSubContractorID.FieldName = "OriginalSubContractorID";
            this.colOriginalSubContractorID.Name = "colOriginalSubContractorID";
            this.colOriginalSubContractorID.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorID.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorID.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorID.Width = 100;
            // 
            // colOriginalSubContarctorName
            // 
            this.colOriginalSubContarctorName.Caption = "Original Team Name";
            this.colOriginalSubContarctorName.FieldName = "OriginalSubContarctorName";
            this.colOriginalSubContarctorName.Name = "colOriginalSubContarctorName";
            this.colOriginalSubContarctorName.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContarctorName.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContarctorName.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContarctorName.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 2;
            this.colReactive.Width = 62;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 88;
            // 
            // colCalloutStatusDescription
            // 
            this.colCalloutStatusDescription.Caption = "Status";
            this.colCalloutStatusDescription.FieldName = "CalloutStatusDescription";
            this.colCalloutStatusDescription.Name = "colCalloutStatusDescription";
            this.colCalloutStatusDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusDescription.Width = 182;
            // 
            // colCalloutStatusOrder
            // 
            this.colCalloutStatusOrder.Caption = "Status Order";
            this.colCalloutStatusOrder.FieldName = "CalloutStatusOrder";
            this.colCalloutStatusOrder.Name = "colCalloutStatusOrder";
            this.colCalloutStatusOrder.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusOrder.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusOrder.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusOrder.Width = 83;
            // 
            // colCallOutDateTime
            // 
            this.colCallOutDateTime.Caption = "Callout Date\\Time";
            this.colCallOutDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCallOutDateTime.FieldName = "CallOutDateTime";
            this.colCallOutDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCallOutDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCallOutDateTime.Name = "colCallOutDateTime";
            this.colCallOutDateTime.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCallOutDateTime.Visible = true;
            this.colCallOutDateTime.VisibleIndex = 1;
            this.colCallOutDateTime.Width = 117;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colImportedWeatherForecastID
            // 
            this.colImportedWeatherForecastID.Caption = "Imported Forecast ID";
            this.colImportedWeatherForecastID.FieldName = "ImportedWeatherForecastID";
            this.colImportedWeatherForecastID.Name = "colImportedWeatherForecastID";
            this.colImportedWeatherForecastID.OptionsColumn.AllowEdit = false;
            this.colImportedWeatherForecastID.OptionsColumn.AllowFocus = false;
            this.colImportedWeatherForecastID.OptionsColumn.ReadOnly = true;
            this.colImportedWeatherForecastID.Width = 124;
            // 
            // colTextSentTime
            // 
            this.colTextSentTime.Caption = "Text Sent Time";
            this.colTextSentTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colTextSentTime.FieldName = "TextSentTime";
            this.colTextSentTime.Name = "colTextSentTime";
            this.colTextSentTime.OptionsColumn.AllowEdit = false;
            this.colTextSentTime.OptionsColumn.AllowFocus = false;
            this.colTextSentTime.OptionsColumn.ReadOnly = true;
            this.colTextSentTime.Width = 93;
            // 
            // colSubContractorReceivedTime
            // 
            this.colSubContractorReceivedTime.Caption = "Team Received Time";
            this.colSubContractorReceivedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSubContractorReceivedTime.FieldName = "SubContractorReceivedTime";
            this.colSubContractorReceivedTime.Name = "colSubContractorReceivedTime";
            this.colSubContractorReceivedTime.OptionsColumn.AllowEdit = false;
            this.colSubContractorReceivedTime.OptionsColumn.AllowFocus = false;
            this.colSubContractorReceivedTime.OptionsColumn.ReadOnly = true;
            this.colSubContractorReceivedTime.Width = 119;
            // 
            // colSubContractorRespondedTime
            // 
            this.colSubContractorRespondedTime.Caption = "Team Responded Time";
            this.colSubContractorRespondedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSubContractorRespondedTime.FieldName = "SubContractorRespondedTime";
            this.colSubContractorRespondedTime.Name = "colSubContractorRespondedTime";
            this.colSubContractorRespondedTime.OptionsColumn.AllowEdit = false;
            this.colSubContractorRespondedTime.OptionsColumn.AllowFocus = false;
            this.colSubContractorRespondedTime.OptionsColumn.ReadOnly = true;
            this.colSubContractorRespondedTime.Width = 129;
            // 
            // colSubContractorETA
            // 
            this.colSubContractorETA.Caption = "Team ETA";
            this.colSubContractorETA.FieldName = "SubContractorETA";
            this.colSubContractorETA.Name = "colSubContractorETA";
            this.colSubContractorETA.OptionsColumn.AllowEdit = false;
            this.colSubContractorETA.OptionsColumn.AllowFocus = false;
            this.colSubContractorETA.OptionsColumn.ReadOnly = true;
            // 
            // colCompletedTime
            // 
            this.colCompletedTime.Caption = "Completed Time";
            this.colCompletedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCompletedTime.FieldName = "CompletedTime";
            this.colCompletedTime.Name = "colCompletedTime";
            this.colCompletedTime.OptionsColumn.AllowEdit = false;
            this.colCompletedTime.OptionsColumn.AllowFocus = false;
            this.colCompletedTime.OptionsColumn.ReadOnly = true;
            this.colCompletedTime.Visible = true;
            this.colCompletedTime.VisibleIndex = 6;
            this.colCompletedTime.Width = 97;
            // 
            // colAuthorisedByStaffID
            // 
            this.colAuthorisedByStaffID.Caption = "Authorised By ID";
            this.colAuthorisedByStaffID.FieldName = "AuthorisedByStaffID";
            this.colAuthorisedByStaffID.Name = "colAuthorisedByStaffID";
            this.colAuthorisedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByStaffID.Width = 102;
            // 
            // colAuthorisedByName
            // 
            this.colAuthorisedByName.Caption = "Authorised By";
            this.colAuthorisedByName.FieldName = "AuthorisedByName";
            this.colAuthorisedByName.Name = "colAuthorisedByName";
            this.colAuthorisedByName.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByName.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByName.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByName.Visible = true;
            this.colAuthorisedByName.VisibleIndex = 11;
            this.colAuthorisedByName.Width = 88;
            // 
            // colVisitAborted
            // 
            this.colVisitAborted.Caption = "Aborted";
            this.colVisitAborted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colVisitAborted.FieldName = "VisitAborted";
            this.colVisitAborted.Name = "colVisitAborted";
            this.colVisitAborted.OptionsColumn.AllowEdit = false;
            this.colVisitAborted.OptionsColumn.AllowFocus = false;
            this.colVisitAborted.OptionsColumn.ReadOnly = true;
            this.colVisitAborted.Visible = true;
            this.colVisitAborted.VisibleIndex = 12;
            // 
            // colAbortedReason
            // 
            this.colAbortedReason.Caption = "Aborted Reason";
            this.colAbortedReason.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colAbortedReason.FieldName = "AbortedReason";
            this.colAbortedReason.Name = "colAbortedReason";
            this.colAbortedReason.OptionsColumn.ReadOnly = true;
            this.colAbortedReason.Visible = true;
            this.colAbortedReason.VisibleIndex = 13;
            this.colAbortedReason.Width = 99;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Lat";
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Long";
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            // 
            // colFinishedLatitude
            // 
            this.colFinishedLatitude.Caption = "Finished Lat";
            this.colFinishedLatitude.FieldName = "FinishedLatitude";
            this.colFinishedLatitude.Name = "colFinishedLatitude";
            this.colFinishedLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishedLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishedLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishedLatitude.Width = 78;
            // 
            // colFinishedLongitude
            // 
            this.colFinishedLongitude.Caption = "Finished Long";
            this.colFinishedLongitude.FieldName = "FinishedLongitude";
            this.colFinishedLongitude.Name = "colFinishedLongitude";
            this.colFinishedLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishedLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishedLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishedLongitude.Width = 86;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "P.O. Number";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Width = 83;
            // 
            // colSaltUsed
            // 
            this.colSaltUsed.Caption = "Salt Used";
            this.colSaltUsed.ColumnEdit = this.repositoryItemTextEdit25KgBags;
            this.colSaltUsed.FieldName = "SaltUsed";
            this.colSaltUsed.Name = "colSaltUsed";
            this.colSaltUsed.OptionsColumn.AllowEdit = false;
            this.colSaltUsed.OptionsColumn.AllowFocus = false;
            this.colSaltUsed.OptionsColumn.ReadOnly = true;
            this.colSaltUsed.Visible = true;
            this.colSaltUsed.VisibleIndex = 20;
            // 
            // repositoryItemTextEdit25KgBags
            // 
            this.repositoryItemTextEdit25KgBags.AutoHeight = false;
            this.repositoryItemTextEdit25KgBags.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25KgBags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KgBags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KgBags.Name = "repositoryItemTextEdit25KgBags";
            // 
            // colSaltCost
            // 
            this.colSaltCost.Caption = "Salt Cost";
            this.colSaltCost.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colSaltCost.FieldName = "SaltCost";
            this.colSaltCost.Name = "colSaltCost";
            this.colSaltCost.OptionsColumn.AllowEdit = false;
            this.colSaltCost.OptionsColumn.AllowFocus = false;
            this.colSaltCost.OptionsColumn.ReadOnly = true;
            this.colSaltCost.Visible = true;
            this.colSaltCost.VisibleIndex = 21;
            // 
            // repositoryItemTextEditCurrency3
            // 
            this.repositoryItemTextEditCurrency3.AutoHeight = false;
            this.repositoryItemTextEditCurrency3.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency3.Name = "repositoryItemTextEditCurrency3";
            // 
            // colSaltSell
            // 
            this.colSaltSell.Caption = "Salt Sell";
            this.colSaltSell.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colSaltSell.FieldName = "SaltSell";
            this.colSaltSell.Name = "colSaltSell";
            this.colSaltSell.OptionsColumn.AllowEdit = false;
            this.colSaltSell.OptionsColumn.AllowFocus = false;
            this.colSaltSell.OptionsColumn.ReadOnly = true;
            this.colSaltSell.Visible = true;
            this.colSaltSell.VisibleIndex = 22;
            // 
            // colSaltVatRate
            // 
            this.colSaltVatRate.Caption = "Salt VAT Rate";
            this.colSaltVatRate.ColumnEdit = this.repositoryItemTextEditPercent;
            this.colSaltVatRate.FieldName = "SaltVatRate";
            this.colSaltVatRate.Name = "colSaltVatRate";
            this.colSaltVatRate.OptionsColumn.AllowEdit = false;
            this.colSaltVatRate.OptionsColumn.AllowFocus = false;
            this.colSaltVatRate.OptionsColumn.ReadOnly = true;
            this.colSaltVatRate.Visible = true;
            this.colSaltVatRate.VisibleIndex = 23;
            this.colSaltVatRate.Width = 87;
            // 
            // repositoryItemTextEditPercent
            // 
            this.repositoryItemTextEditPercent.AutoHeight = false;
            this.repositoryItemTextEditPercent.Mask.EditMask = "P0";
            this.repositoryItemTextEditPercent.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercent.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercent.Name = "repositoryItemTextEditPercent";
            // 
            // colHoursWorked
            // 
            this.colHoursWorked.Caption = "Hours Worked";
            this.colHoursWorked.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colHoursWorked.FieldName = "HoursWorked";
            this.colHoursWorked.Name = "colHoursWorked";
            this.colHoursWorked.OptionsColumn.AllowEdit = false;
            this.colHoursWorked.OptionsColumn.AllowFocus = false;
            this.colHoursWorked.OptionsColumn.ReadOnly = true;
            this.colHoursWorked.Visible = true;
            this.colHoursWorked.VisibleIndex = 15;
            this.colHoursWorked.Width = 89;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colTeamHourlyRate
            // 
            this.colTeamHourlyRate.Caption = "Hourly Rate";
            this.colTeamHourlyRate.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colTeamHourlyRate.FieldName = "TeamHourlyRate";
            this.colTeamHourlyRate.Name = "colTeamHourlyRate";
            this.colTeamHourlyRate.OptionsColumn.AllowEdit = false;
            this.colTeamHourlyRate.OptionsColumn.AllowFocus = false;
            this.colTeamHourlyRate.OptionsColumn.ReadOnly = true;
            this.colTeamHourlyRate.Visible = true;
            this.colTeamHourlyRate.VisibleIndex = 16;
            this.colTeamHourlyRate.Width = 78;
            // 
            // colTeamCharge
            // 
            this.colTeamCharge.Caption = "Team Charge";
            this.colTeamCharge.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colTeamCharge.FieldName = "TeamCharge";
            this.colTeamCharge.Name = "colTeamCharge";
            this.colTeamCharge.OptionsColumn.AllowEdit = false;
            this.colTeamCharge.OptionsColumn.AllowFocus = false;
            this.colTeamCharge.OptionsColumn.ReadOnly = true;
            this.colTeamCharge.Visible = true;
            this.colTeamCharge.VisibleIndex = 17;
            this.colTeamCharge.Width = 85;
            // 
            // colLabourCost
            // 
            this.colLabourCost.Caption = "Labour Cost";
            this.colLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colLabourCost.FieldName = "LabourCost";
            this.colLabourCost.Name = "colLabourCost";
            this.colLabourCost.OptionsColumn.AllowEdit = false;
            this.colLabourCost.OptionsColumn.AllowFocus = false;
            this.colLabourCost.OptionsColumn.ReadOnly = true;
            this.colLabourCost.Visible = true;
            this.colLabourCost.VisibleIndex = 18;
            this.colLabourCost.Width = 79;
            // 
            // colLabourVatRate
            // 
            this.colLabourVatRate.Caption = "Labour VAT Rate";
            this.colLabourVatRate.ColumnEdit = this.repositoryItemTextEditPercent;
            this.colLabourVatRate.FieldName = "LabourVatRate";
            this.colLabourVatRate.Name = "colLabourVatRate";
            this.colLabourVatRate.OptionsColumn.AllowEdit = false;
            this.colLabourVatRate.OptionsColumn.AllowFocus = false;
            this.colLabourVatRate.OptionsColumn.ReadOnly = true;
            this.colLabourVatRate.Visible = true;
            this.colLabourVatRate.VisibleIndex = 19;
            this.colLabourVatRate.Width = 102;
            // 
            // colOtherCost
            // 
            this.colOtherCost.Caption = "Other Cost";
            this.colOtherCost.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colOtherCost.FieldName = "OtherCost";
            this.colOtherCost.Name = "colOtherCost";
            this.colOtherCost.OptionsColumn.AllowEdit = false;
            this.colOtherCost.OptionsColumn.AllowFocus = false;
            this.colOtherCost.OptionsColumn.ReadOnly = true;
            this.colOtherCost.Visible = true;
            this.colOtherCost.VisibleIndex = 24;
            // 
            // colOtherSell
            // 
            this.colOtherSell.Caption = "Other Sell";
            this.colOtherSell.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colOtherSell.FieldName = "OtherSell";
            this.colOtherSell.Name = "colOtherSell";
            this.colOtherSell.OptionsColumn.AllowEdit = false;
            this.colOtherSell.OptionsColumn.AllowFocus = false;
            this.colOtherSell.OptionsColumn.ReadOnly = true;
            this.colOtherSell.Visible = true;
            this.colOtherSell.VisibleIndex = 25;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 26;
            // 
            // colTotalSell
            // 
            this.colTotalSell.Caption = "Total Sell";
            this.colTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colTotalSell.FieldName = "TotalSell";
            this.colTotalSell.Name = "colTotalSell";
            this.colTotalSell.OptionsColumn.AllowEdit = false;
            this.colTotalSell.OptionsColumn.AllowFocus = false;
            this.colTotalSell.OptionsColumn.ReadOnly = true;
            this.colTotalSell.Visible = true;
            this.colTotalSell.VisibleIndex = 28;
            // 
            // colClientInvoiceNumber
            // 
            this.colClientInvoiceNumber.Caption = "Invoice Number";
            this.colClientInvoiceNumber.FieldName = "ClientInvoiceNumber";
            this.colClientInvoiceNumber.Name = "colClientInvoiceNumber";
            this.colClientInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceNumber.Width = 96;
            // 
            // colSubContractorPaid
            // 
            this.colSubContractorPaid.Caption = "Team Paid";
            this.colSubContractorPaid.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSubContractorPaid.FieldName = "SubContractorPaid";
            this.colSubContractorPaid.Name = "colSubContractorPaid";
            this.colSubContractorPaid.OptionsColumn.AllowEdit = false;
            this.colSubContractorPaid.OptionsColumn.AllowFocus = false;
            this.colSubContractorPaid.OptionsColumn.ReadOnly = true;
            // 
            // colGrittingInvoiceID
            // 
            this.colGrittingInvoiceID.Caption = "Gritting Invoice ID";
            this.colGrittingInvoiceID.FieldName = "GrittingInvoiceID";
            this.colGrittingInvoiceID.Name = "colGrittingInvoiceID";
            this.colGrittingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colGrittingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colGrittingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colGrittingInvoiceID.Width = 108;
            // 
            // colRecordedByStaffID
            // 
            this.colRecordedByStaffID.Caption = "Recorded By Staff ID";
            this.colRecordedByStaffID.FieldName = "RecordedByStaffID";
            this.colRecordedByStaffID.Name = "colRecordedByStaffID";
            this.colRecordedByStaffID.OptionsColumn.AllowEdit = false;
            this.colRecordedByStaffID.OptionsColumn.AllowFocus = false;
            this.colRecordedByStaffID.OptionsColumn.ReadOnly = true;
            this.colRecordedByStaffID.Width = 123;
            // 
            // colRecordedByName
            // 
            this.colRecordedByName.Caption = "Recorded By";
            this.colRecordedByName.FieldName = "RecordedByName";
            this.colRecordedByName.Name = "colRecordedByName";
            this.colRecordedByName.OptionsColumn.AllowEdit = false;
            this.colRecordedByName.OptionsColumn.AllowFocus = false;
            this.colRecordedByName.OptionsColumn.ReadOnly = true;
            this.colRecordedByName.Width = 118;
            // 
            // colPaidByStaffID
            // 
            this.colPaidByStaffID.Caption = "Paid By Staff ID";
            this.colPaidByStaffID.FieldName = "PaidByStaffID";
            this.colPaidByStaffID.Name = "colPaidByStaffID";
            this.colPaidByStaffID.OptionsColumn.AllowEdit = false;
            this.colPaidByStaffID.OptionsColumn.AllowFocus = false;
            this.colPaidByStaffID.OptionsColumn.ReadOnly = true;
            this.colPaidByStaffID.Width = 97;
            // 
            // colPaidByName
            // 
            this.colPaidByName.Caption = "Paid By";
            this.colPaidByName.FieldName = "PaidByName";
            this.colPaidByName.Name = "colPaidByName";
            this.colPaidByName.OptionsColumn.AllowEdit = false;
            this.colPaidByName.OptionsColumn.AllowFocus = false;
            this.colPaidByName.OptionsColumn.ReadOnly = true;
            this.colPaidByName.Width = 93;
            // 
            // colDoNotPaySubContractor
            // 
            this.colDoNotPaySubContractor.Caption = "Don\'t Pay Team";
            this.colDoNotPaySubContractor.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPaySubContractor.FieldName = "DoNotPaySubContractor";
            this.colDoNotPaySubContractor.Name = "colDoNotPaySubContractor";
            this.colDoNotPaySubContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPaySubContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPaySubContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractor.Visible = true;
            this.colDoNotPaySubContractor.VisibleIndex = 7;
            this.colDoNotPaySubContractor.Width = 96;
            // 
            // colDoNotPaySubContractorReason
            // 
            this.colDoNotPaySubContractorReason.Caption = "Don\'t Pay Team Reason";
            this.colDoNotPaySubContractorReason.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colDoNotPaySubContractorReason.FieldName = "DoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.Name = "colDoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractorReason.Visible = true;
            this.colDoNotPaySubContractorReason.VisibleIndex = 8;
            this.colDoNotPaySubContractorReason.Width = 135;
            // 
            // colGritSourceLocationID
            // 
            this.colGritSourceLocationID.Caption = "Grit Source Location ID";
            this.colGritSourceLocationID.FieldName = "GritSourceLocationID";
            this.colGritSourceLocationID.Name = "colGritSourceLocationID";
            this.colGritSourceLocationID.OptionsColumn.AllowEdit = false;
            this.colGritSourceLocationID.OptionsColumn.AllowFocus = false;
            this.colGritSourceLocationID.OptionsColumn.ReadOnly = true;
            this.colGritSourceLocationID.Width = 131;
            // 
            // colGritSourceLocationTypeID
            // 
            this.colGritSourceLocationTypeID.Caption = "Grit Source Location Type ID";
            this.colGritSourceLocationTypeID.FieldName = "GritSourceLocationTypeID";
            this.colGritSourceLocationTypeID.Name = "colGritSourceLocationTypeID";
            this.colGritSourceLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colGritSourceLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colGritSourceLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colGritSourceLocationTypeID.Width = 158;
            // 
            // colNoAccess
            // 
            this.colNoAccess.Caption = "No Access";
            this.colNoAccess.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoAccess.FieldName = "NoAccess";
            this.colNoAccess.Name = "colNoAccess";
            this.colNoAccess.OptionsColumn.AllowEdit = false;
            this.colNoAccess.OptionsColumn.AllowFocus = false;
            this.colNoAccess.OptionsColumn.ReadOnly = true;
            this.colNoAccess.Visible = true;
            this.colNoAccess.VisibleIndex = 33;
            // 
            // colSiteWeather
            // 
            this.colSiteWeather.Caption = "Site Weather";
            this.colSiteWeather.FieldName = "SiteWeather";
            this.colSiteWeather.Name = "colSiteWeather";
            this.colSiteWeather.OptionsColumn.AllowEdit = false;
            this.colSiteWeather.OptionsColumn.AllowFocus = false;
            this.colSiteWeather.OptionsColumn.ReadOnly = true;
            this.colSiteWeather.Width = 84;
            // 
            // colSiteTemperature
            // 
            this.colSiteTemperature.Caption = "Site Temperature";
            this.colSiteTemperature.FieldName = "SiteTemperature";
            this.colSiteTemperature.Name = "colSiteTemperature";
            this.colSiteTemperature.OptionsColumn.AllowEdit = false;
            this.colSiteTemperature.OptionsColumn.AllowFocus = false;
            this.colSiteTemperature.OptionsColumn.ReadOnly = true;
            this.colSiteTemperature.Width = 104;
            // 
            // colPdaID
            // 
            this.colPdaID.Caption = "Device ID";
            this.colPdaID.FieldName = "PdaID";
            this.colPdaID.Name = "colPdaID";
            this.colPdaID.OptionsColumn.AllowEdit = false;
            this.colPdaID.OptionsColumn.AllowFocus = false;
            this.colPdaID.OptionsColumn.ReadOnly = true;
            // 
            // colTeamPresent
            // 
            this.colTeamPresent.Caption = "Team Present";
            this.colTeamPresent.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTeamPresent.FieldName = "TeamPresent";
            this.colTeamPresent.Name = "colTeamPresent";
            this.colTeamPresent.OptionsColumn.AllowEdit = false;
            this.colTeamPresent.OptionsColumn.AllowFocus = false;
            this.colTeamPresent.OptionsColumn.ReadOnly = true;
            this.colTeamPresent.Width = 87;
            // 
            // colServiceFailure
            // 
            this.colServiceFailure.Caption = "Service Failure";
            this.colServiceFailure.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colServiceFailure.FieldName = "ServiceFailure";
            this.colServiceFailure.Name = "colServiceFailure";
            this.colServiceFailure.OptionsColumn.AllowEdit = false;
            this.colServiceFailure.OptionsColumn.AllowFocus = false;
            this.colServiceFailure.OptionsColumn.ReadOnly = true;
            this.colServiceFailure.Visible = true;
            this.colServiceFailure.VisibleIndex = 34;
            this.colServiceFailure.Width = 91;
            // 
            // colComments
            // 
            this.colComments.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colComments.FieldName = "Comments";
            this.colComments.Name = "colComments";
            this.colComments.OptionsColumn.ReadOnly = true;
            this.colComments.Visible = true;
            this.colComments.VisibleIndex = 35;
            // 
            // colClientPrice
            // 
            this.colClientPrice.Caption = "Client Price";
            this.colClientPrice.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colClientPrice.FieldName = "ClientPrice";
            this.colClientPrice.Name = "colClientPrice";
            this.colClientPrice.OptionsColumn.AllowEdit = false;
            this.colClientPrice.OptionsColumn.AllowFocus = false;
            this.colClientPrice.OptionsColumn.ReadOnly = true;
            this.colClientPrice.Visible = true;
            this.colClientPrice.VisibleIndex = 27;
            // 
            // colGritJobTransferMethod
            // 
            this.colGritJobTransferMethod.Caption = "Transfer Method";
            this.colGritJobTransferMethod.FieldName = "GritJobTransferMethod";
            this.colGritJobTransferMethod.Name = "colGritJobTransferMethod";
            this.colGritJobTransferMethod.OptionsColumn.AllowEdit = false;
            this.colGritJobTransferMethod.OptionsColumn.AllowFocus = false;
            this.colGritJobTransferMethod.OptionsColumn.ReadOnly = true;
            this.colGritJobTransferMethod.Width = 101;
            // 
            // colSnowOnSite
            // 
            this.colSnowOnSite.Caption = "Snow On Site";
            this.colSnowOnSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSnowOnSite.FieldName = "SnowOnSite";
            this.colSnowOnSite.Name = "colSnowOnSite";
            this.colSnowOnSite.OptionsColumn.AllowEdit = false;
            this.colSnowOnSite.OptionsColumn.AllowFocus = false;
            this.colSnowOnSite.OptionsColumn.ReadOnly = true;
            this.colSnowOnSite.Width = 85;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "<b>Calculated 1</b>";
            this.gridColumn11.FieldName = "Calculated1";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.ShowUnboundExpressionMenu = true;
            this.gridColumn11.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 36;
            this.gridColumn11.Width = 90;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "<b>Calculated 2</b>";
            this.gridColumn12.FieldName = "Calculated2";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.ShowUnboundExpressionMenu = true;
            this.gridColumn12.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 37;
            this.gridColumn12.Width = 90;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "<b>Calculated 3</b>";
            this.gridColumn19.FieldName = "Calculated3";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.ShowUnboundExpressionMenu = true;
            this.gridColumn19.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 38;
            this.gridColumn19.Width = 90;
            // 
            // colProfit
            // 
            this.colProfit.Caption = "Profit";
            this.colProfit.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colProfit.FieldName = "Profit";
            this.colProfit.Name = "colProfit";
            this.colProfit.OptionsColumn.AllowEdit = false;
            this.colProfit.OptionsColumn.AllowFocus = false;
            this.colProfit.OptionsColumn.ReadOnly = true;
            this.colProfit.Visible = true;
            this.colProfit.VisibleIndex = 29;
            // 
            // colMarkup
            // 
            this.colMarkup.Caption = "Markup";
            this.colMarkup.ColumnEdit = this.repositoryItemTextEditPercent;
            this.colMarkup.FieldName = "Markup";
            this.colMarkup.Name = "colMarkup";
            this.colMarkup.OptionsColumn.AllowEdit = false;
            this.colMarkup.OptionsColumn.AllowFocus = false;
            this.colMarkup.OptionsColumn.ReadOnly = true;
            this.colMarkup.Visible = true;
            this.colMarkup.VisibleIndex = 30;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "P.O. Linked ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 90;
            // 
            // colNonStandardCost
            // 
            this.colNonStandardCost.Caption = "Non-Standard Cost";
            this.colNonStandardCost.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardCost.FieldName = "NonStandardCost";
            this.colNonStandardCost.Name = "colNonStandardCost";
            this.colNonStandardCost.OptionsColumn.AllowEdit = false;
            this.colNonStandardCost.OptionsColumn.AllowFocus = false;
            this.colNonStandardCost.OptionsColumn.ReadOnly = true;
            this.colNonStandardCost.Visible = true;
            this.colNonStandardCost.VisibleIndex = 31;
            this.colNonStandardCost.Width = 113;
            // 
            // colNonStandardSell
            // 
            this.colNonStandardSell.Caption = "Non-Standard Sell";
            this.colNonStandardSell.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardSell.FieldName = "NonStandardSell";
            this.colNonStandardSell.Name = "colNonStandardSell";
            this.colNonStandardSell.OptionsColumn.AllowEdit = false;
            this.colNonStandardSell.OptionsColumn.AllowFocus = false;
            this.colNonStandardSell.OptionsColumn.ReadOnly = true;
            this.colNonStandardSell.Visible = true;
            this.colNonStandardSell.VisibleIndex = 32;
            this.colNonStandardSell.Width = 107;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 9;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colDoNotInvoiceClientReason
            // 
            this.colDoNotInvoiceClientReason.Caption = "Do Not Invoice Site Reason";
            this.colDoNotInvoiceClientReason.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colDoNotInvoiceClientReason.FieldName = "DoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.Name = "colDoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClientReason.Visible = true;
            this.colDoNotInvoiceClientReason.VisibleIndex = 10;
            this.colDoNotInvoiceClientReason.Width = 150;
            // 
            // colGritMobileTelephoneNumber
            // 
            this.colGritMobileTelephoneNumber.Caption = "Grit Mobile Telephone";
            this.colGritMobileTelephoneNumber.FieldName = "GritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.Name = "colGritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.OptionsColumn.ReadOnly = true;
            this.colGritMobileTelephoneNumber.Width = 124;
            // 
            // colSelfBillingInvoice
            // 
            this.colSelfBillingInvoice.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelfBillingInvoice.FieldName = "SelfBillingInvoice";
            this.colSelfBillingInvoice.Name = "colSelfBillingInvoice";
            this.colSelfBillingInvoice.Width = 106;
            // 
            // colClientInvoiceLineID1
            // 
            this.colClientInvoiceLineID1.Caption = "Client Invoice Line ID";
            this.colClientInvoiceLineID1.FieldName = "ClientInvoiceLineID";
            this.colClientInvoiceLineID1.Name = "colClientInvoiceLineID1";
            this.colClientInvoiceLineID1.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceLineID1.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceLineID1.Width = 119;
            // 
            // colClientInvoiceLineDescription
            // 
            this.colClientInvoiceLineDescription.Caption = "Client Invoice Line";
            this.colClientInvoiceLineDescription.FieldName = "ClientInvoiceLineDescription";
            this.colClientInvoiceLineDescription.Name = "colClientInvoiceLineDescription";
            this.colClientInvoiceLineDescription.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceLineDescription.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceLineDescription.Width = 201;
            // 
            // gridControl4
            // 
            this.gridControl4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl4.BackgroundImage")));
            this.gridControl4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gridControl4.DataSource = this.sp04260GCFinanceSnowCalloutManagerForClientInvoiceLineBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Remove Selected Record(s) from Saved Invoice Line(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemTextEdit2DP2});
            this.gridControl4.Size = new System.Drawing.Size(466, 243);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp04260GCFinanceSnowCalloutManagerForClientInvoiceLineBindingSource
            // 
            this.sp04260GCFinanceSnowCalloutManagerForClientInvoiceLineBindingSource.DataMember = "sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_Line";
            this.sp04260GCFinanceSnowCalloutManagerForClientInvoiceLineBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSnowClearanceCallOutID,
            this.colSnowClearanceSiteContractID,
            this.colGCPONumberSuffix,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn20,
            this.colSubContractorID,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.colOriginalSubContractorName,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.colLabourSell,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.colSubContractorContactedByStaffID,
            this.colSubContractorContactedByStaffName,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.colAccessComments,
            this.colClientHowSoon,
            this.gridColumn57,
            this.colRemarks2,
            this.colNoAccessAbortedRate,
            this.colTeamInvoiceNumber,
            this.colTeamPOFileName,
            this.colTimesheetSubmitted,
            this.colClientInvoiceLineID2,
            this.colClientInvoiceLineDescription1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientInvoiceLineDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn28, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView4.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView4_CustomDrawCell);
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colSnowClearanceCallOutID
            // 
            this.colSnowClearanceCallOutID.Caption = "GC PO No";
            this.colSnowClearanceCallOutID.FieldName = "SnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.Name = "colSnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutID.Visible = true;
            this.colSnowClearanceCallOutID.VisibleIndex = 5;
            this.colSnowClearanceCallOutID.Width = 71;
            // 
            // colSnowClearanceSiteContractID
            // 
            this.colSnowClearanceSiteContractID.Caption = "Snow Clearance Contract ID";
            this.colSnowClearanceSiteContractID.FieldName = "SnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.Name = "colSnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSiteContractID.Width = 157;
            // 
            // colGCPONumberSuffix
            // 
            this.colGCPONumberSuffix.Caption = "GC PO Suffix";
            this.colGCPONumberSuffix.FieldName = "GCPONumberSuffix";
            this.colGCPONumberSuffix.Name = "colGCPONumberSuffix";
            this.colGCPONumberSuffix.OptionsColumn.AllowEdit = false;
            this.colGCPONumberSuffix.OptionsColumn.AllowFocus = false;
            this.colGCPONumberSuffix.OptionsColumn.ReadOnly = true;
            this.colGCPONumberSuffix.Visible = true;
            this.colGCPONumberSuffix.VisibleIndex = 6;
            this.colGCPONumberSuffix.Width = 84;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Site ID";
            this.gridColumn4.FieldName = "SiteID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Site Name";
            this.gridColumn5.FieldName = "SiteName";
            this.gridColumn5.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 157;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Client ID";
            this.gridColumn6.FieldName = "ClientID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Client Name";
            this.gridColumn7.FieldName = "ClientName";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 120;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Company ID";
            this.gridColumn8.FieldName = "CompanyID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 80;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Company Name";
            this.gridColumn9.FieldName = "CompanyName";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 16;
            this.gridColumn9.Width = 137;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Site X Coordinate";
            this.gridColumn13.FieldName = "SiteXCoordinate";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 104;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Site Y Coordinate";
            this.gridColumn14.FieldName = "SiteYCoordinate";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 104;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Site Location X";
            this.gridColumn15.FieldName = "SiteLocationX";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 91;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Site Location Y";
            this.gridColumn16.FieldName = "SiteLocationY";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 91;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Site Telephone ";
            this.gridColumn17.FieldName = "SiteTelephone";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 95;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Site Email";
            this.gridColumn18.FieldName = "SiteEmail";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 116;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Clients Site Code";
            this.gridColumn20.FieldName = "ClientsSiteCode";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 105;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Team Name";
            this.gridColumn21.FieldName = "SubContractorName";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 4;
            this.gridColumn21.Width = 122;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Team Mobile No";
            this.gridColumn22.FieldName = "GritMobileTelephoneNumber";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 96;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Original Team ID";
            this.gridColumn23.FieldName = "OriginalSubContractorID";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 100;
            // 
            // colOriginalSubContractorName
            // 
            this.colOriginalSubContractorName.Caption = "Original Team Name";
            this.colOriginalSubContractorName.FieldName = "OriginalSubContractorName";
            this.colOriginalSubContractorName.Name = "colOriginalSubContractorName";
            this.colOriginalSubContractorName.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorName.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorName.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorName.Width = 116;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Reactive";
            this.gridColumn24.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn24.FieldName = "Reactive";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 2;
            this.gridColumn24.Width = 63;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Status ID";
            this.gridColumn25.FieldName = "JobStatusID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Status Description";
            this.gridColumn26.FieldName = "CalloutStatusDescription";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 196;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Status Order";
            this.gridColumn27.FieldName = "CalloutStatusOrder";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Width = 83;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Callout Date\\Time";
            this.gridColumn28.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn28.FieldName = "CallOutDateTime";
            this.gridColumn28.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn28.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 1;
            this.gridColumn28.Width = 121;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Team ETA";
            this.gridColumn29.FieldName = "SubContractorETA";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 117;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Completed Date\\Time";
            this.gridColumn30.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn30.FieldName = "CompletedTime";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 14;
            this.gridColumn30.Width = 124;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Recorded By Staff ID";
            this.gridColumn31.FieldName = "RecordedByStaffID";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Width = 123;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Aborted";
            this.gridColumn32.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn32.FieldName = "VisitAborted";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 11;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Aborted Reason";
            this.gridColumn33.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn33.FieldName = "AbortedReason";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 12;
            this.gridColumn33.Width = 99;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Client PO Number";
            this.gridColumn34.FieldName = "ClientPONumber";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 9;
            this.gridColumn34.Width = 105;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Labour Cost";
            this.gridColumn35.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn35.FieldName = "LabourCost";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 18;
            this.gridColumn35.Width = 79;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Labout VAT Rate";
            this.gridColumn36.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.gridColumn36.FieldName = "LabourVatRate";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 19;
            this.gridColumn36.Width = 102;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colLabourSell
            // 
            this.colLabourSell.Caption = "Labour Sell";
            this.colLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colLabourSell.FieldName = "LabourSell";
            this.colLabourSell.Name = "colLabourSell";
            this.colLabourSell.OptionsColumn.AllowEdit = false;
            this.colLabourSell.OptionsColumn.AllowFocus = false;
            this.colLabourSell.OptionsColumn.ReadOnly = true;
            this.colLabourSell.Visible = true;
            this.colLabourSell.VisibleIndex = 20;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Other Cost";
            this.gridColumn37.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn37.FieldName = "OtherCost";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 21;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Other Sell";
            this.gridColumn38.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn38.FieldName = "OtherSell";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 22;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Total Cost";
            this.gridColumn39.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn39.FieldName = "TotalCost";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 23;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Total Sell ";
            this.gridColumn40.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn40.FieldName = "TotalSell";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 24;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Client Invoice Number";
            this.gridColumn41.FieldName = "ClientInvoiceNumber";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 29;
            this.gridColumn41.Width = 126;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Team Paid";
            this.gridColumn42.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn42.FieldName = "SubContractorPaid";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 30;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Recorded By Name";
            this.gridColumn43.FieldName = "RecordedByName";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Width = 112;
            // 
            // colSubContractorContactedByStaffID
            // 
            this.colSubContractorContactedByStaffID.Caption = "Team Contacted By Staff ID";
            this.colSubContractorContactedByStaffID.FieldName = "SubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.Name = "colSubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.OptionsColumn.AllowEdit = false;
            this.colSubContractorContactedByStaffID.OptionsColumn.AllowFocus = false;
            this.colSubContractorContactedByStaffID.OptionsColumn.ReadOnly = true;
            this.colSubContractorContactedByStaffID.Width = 156;
            // 
            // colSubContractorContactedByStaffName
            // 
            this.colSubContractorContactedByStaffName.Caption = "Team Contacted By Staff Name";
            this.colSubContractorContactedByStaffName.FieldName = "SubContractorContactedByStaffName";
            this.colSubContractorContactedByStaffName.Name = "colSubContractorContactedByStaffName";
            this.colSubContractorContactedByStaffName.OptionsColumn.AllowEdit = false;
            this.colSubContractorContactedByStaffName.OptionsColumn.AllowFocus = false;
            this.colSubContractorContactedByStaffName.OptionsColumn.ReadOnly = true;
            this.colSubContractorContactedByStaffName.Width = 172;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Paid By Staff ID";
            this.gridColumn44.FieldName = "PaidByStaffID";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Width = 97;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Paid By Staff Name";
            this.gridColumn45.FieldName = "PaidByName";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 31;
            this.gridColumn45.Width = 113;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Do Not Pay Team";
            this.gridColumn46.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn46.FieldName = "DoNotPaySubContractor";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 32;
            this.gridColumn46.Width = 104;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Do Not Pay Team Reason";
            this.gridColumn47.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn47.FieldName = "DoNotPaySubContractorReason";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 33;
            this.gridColumn47.Width = 143;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Do Not Invoice Client";
            this.gridColumn48.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn48.FieldName = "DoNotInvoiceClient";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 34;
            this.gridColumn48.Width = 122;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Do Not Invoice Client Reason";
            this.gridColumn49.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn49.FieldName = "DoNotInvoiceClientReason";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 35;
            this.gridColumn49.Width = 161;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "No Access";
            this.gridColumn50.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn50.FieldName = "NoAccess";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 10;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Start Date\\Time";
            this.gridColumn51.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn51.FieldName = "StartTime";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 13;
            this.gridColumn51.Width = 105;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Profit";
            this.gridColumn52.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn52.FieldName = "Profit";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 25;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "% Markup";
            this.gridColumn53.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.gridColumn53.FieldName = "Markup";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 26;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Client PO ID";
            this.gridColumn54.FieldName = "ClientPOID";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 79;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Non Standard Cost";
            this.gridColumn55.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn55.FieldName = "NonStandardCost";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 27;
            this.gridColumn55.Width = 112;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Non Standard Sell";
            this.gridColumn56.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn56.FieldName = "NonStandardSell";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 28;
            this.gridColumn56.Width = 106;
            // 
            // colAccessComments
            // 
            this.colAccessComments.Caption = "Access Comments";
            this.colAccessComments.FieldName = "AccessComments";
            this.colAccessComments.Name = "colAccessComments";
            this.colAccessComments.OptionsColumn.ReadOnly = true;
            this.colAccessComments.Width = 107;
            // 
            // colClientHowSoon
            // 
            this.colClientHowSoon.Caption = "How Soon";
            this.colClientHowSoon.FieldName = "ClientHowSoon";
            this.colClientHowSoon.Name = "colClientHowSoon";
            this.colClientHowSoon.OptionsColumn.AllowEdit = false;
            this.colClientHowSoon.OptionsColumn.AllowFocus = false;
            this.colClientHowSoon.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Hours Worked";
            this.gridColumn57.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.gridColumn57.FieldName = "HoursWorked";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 15;
            this.gridColumn57.Width = 89;
            // 
            // repositoryItemTextEdit2DP2
            // 
            this.repositoryItemTextEdit2DP2.AutoHeight = false;
            this.repositoryItemTextEdit2DP2.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP2.Name = "repositoryItemTextEdit2DP2";
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 37;
            // 
            // colNoAccessAbortedRate
            // 
            this.colNoAccessAbortedRate.Caption = "No Access \\ Aborted Rate";
            this.colNoAccessAbortedRate.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colNoAccessAbortedRate.FieldName = "NoAccessAbortedRate";
            this.colNoAccessAbortedRate.Name = "colNoAccessAbortedRate";
            this.colNoAccessAbortedRate.OptionsColumn.AllowEdit = false;
            this.colNoAccessAbortedRate.OptionsColumn.AllowFocus = false;
            this.colNoAccessAbortedRate.OptionsColumn.ReadOnly = true;
            this.colNoAccessAbortedRate.Visible = true;
            this.colNoAccessAbortedRate.VisibleIndex = 17;
            this.colNoAccessAbortedRate.Width = 145;
            // 
            // colTeamInvoiceNumber
            // 
            this.colTeamInvoiceNumber.Caption = "Team Invoice No.";
            this.colTeamInvoiceNumber.FieldName = "TeamInvoiceNumber";
            this.colTeamInvoiceNumber.Name = "colTeamInvoiceNumber";
            this.colTeamInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colTeamInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colTeamInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colTeamInvoiceNumber.Visible = true;
            this.colTeamInvoiceNumber.VisibleIndex = 8;
            this.colTeamInvoiceNumber.Width = 105;
            // 
            // colTeamPOFileName
            // 
            this.colTeamPOFileName.Caption = "Team P.O. File";
            this.colTeamPOFileName.FieldName = "TeamPOFileName";
            this.colTeamPOFileName.Name = "colTeamPOFileName";
            this.colTeamPOFileName.Visible = true;
            this.colTeamPOFileName.VisibleIndex = 36;
            this.colTeamPOFileName.Width = 91;
            // 
            // colTimesheetSubmitted
            // 
            this.colTimesheetSubmitted.Caption = "Timesheet Submitted";
            this.colTimesheetSubmitted.FieldName = "TimesheetSubmitted";
            this.colTimesheetSubmitted.Name = "colTimesheetSubmitted";
            this.colTimesheetSubmitted.OptionsColumn.AllowEdit = false;
            this.colTimesheetSubmitted.OptionsColumn.AllowFocus = false;
            this.colTimesheetSubmitted.OptionsColumn.ReadOnly = true;
            this.colTimesheetSubmitted.Visible = true;
            this.colTimesheetSubmitted.VisibleIndex = 7;
            this.colTimesheetSubmitted.Width = 121;
            // 
            // colClientInvoiceLineID2
            // 
            this.colClientInvoiceLineID2.Caption = "Client Invoice Line ID";
            this.colClientInvoiceLineID2.FieldName = "ClientInvoiceLineID";
            this.colClientInvoiceLineID2.Name = "colClientInvoiceLineID2";
            this.colClientInvoiceLineID2.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceLineID2.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceLineID2.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceLineID2.Width = 122;
            // 
            // colClientInvoiceLineDescription1
            // 
            this.colClientInvoiceLineDescription1.Caption = "Client Invoice Line";
            this.colClientInvoiceLineDescription1.FieldName = "ClientInvoiceLineDescription";
            this.colClientInvoiceLineDescription1.Name = "colClientInvoiceLineDescription1";
            this.colClientInvoiceLineDescription1.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceLineDescription1.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceLineDescription1.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceLineDescription1.Visible = true;
            this.colClientInvoiceLineDescription1.VisibleIndex = 31;
            this.colClientInvoiceLineDescription1.Width = 206;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.standaloneBarDockControl2);
            this.xtraTabPage2.Controls.Add(this.gridControl5);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1041, 516);
            this.xtraTabPage2.Text = "Team Billing";
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl2.Manager = this.barManager1;
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(1042, 42);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp04265GCFinanceTeamJobsForExportBindingSource;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 42);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEdit25KgBags2,
            this.repositoryItemTextEditCurrency4});
            this.gridControl5.Size = new System.Drawing.Size(1042, 481);
            this.gridControl5.TabIndex = 0;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04265GCFinanceTeamJobsForExportBindingSource
            // 
            this.sp04265GCFinanceTeamJobsForExportBindingSource.DataMember = "sp04265_GC_Finance_Team_Jobs_For_Export";
            this.sp04265GCFinanceTeamJobsForExportBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUniqueID,
            this.colCalloutID,
            this.colRecordType,
            this.colClientName1,
            this.colSiteName1,
            this.colClientsSiteCode1,
            this.colCallOutDateTime1,
            this.colJobType,
            this.colSiteWeather1,
            this.colSiteTemperature1,
            this.colSnowOnSite1,
            this.colVisitAborted1,
            this.colNoAccess1,
            this.colClientPONumber1,
            this.colSaltUsed1,
            this.colSaltSell1,
            this.colOtherSell1,
            this.colLabourSell1,
            this.colTotalSell1,
            this.colintContractorID,
            this.colTeamName,
            this.colInternalContractor,
            this.colSelfBillingInvoiceDate,
            this.colSelfBillingInvoiceNumber});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCallOutDateTime1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView5_CustomDrawCell);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colUniqueID
            // 
            this.colUniqueID.Caption = "Unique ID";
            this.colUniqueID.FieldName = "UniqueID";
            this.colUniqueID.Name = "colUniqueID";
            this.colUniqueID.OptionsColumn.AllowEdit = false;
            this.colUniqueID.OptionsColumn.AllowFocus = false;
            this.colUniqueID.OptionsColumn.ReadOnly = true;
            // 
            // colCalloutID
            // 
            this.colCalloutID.Caption = "Callout ID";
            this.colCalloutID.FieldName = "CalloutID";
            this.colCalloutID.Name = "colCalloutID";
            this.colCalloutID.OptionsColumn.AllowEdit = false;
            this.colCalloutID.OptionsColumn.AllowFocus = false;
            this.colCalloutID.OptionsColumn.ReadOnly = true;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Width = 113;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 1;
            this.colClientName1.Width = 160;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 2;
            this.colSiteName1.Width = 160;
            // 
            // colClientsSiteCode1
            // 
            this.colClientsSiteCode1.Caption = "Site Code";
            this.colClientsSiteCode1.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode1.Name = "colClientsSiteCode1";
            this.colClientsSiteCode1.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode1.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode1.OptionsColumn.ReadOnly = true;
            // 
            // colCallOutDateTime1
            // 
            this.colCallOutDateTime1.Caption = "Callout Date\\Time";
            this.colCallOutDateTime1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colCallOutDateTime1.FieldName = "CallOutDateTime";
            this.colCallOutDateTime1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCallOutDateTime1.Name = "colCallOutDateTime1";
            this.colCallOutDateTime1.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime1.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime1.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCallOutDateTime1.Visible = true;
            this.colCallOutDateTime1.VisibleIndex = 3;
            this.colCallOutDateTime1.Width = 124;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colJobType
            // 
            this.colJobType.Caption = "Job Type";
            this.colJobType.FieldName = "JobType";
            this.colJobType.Name = "colJobType";
            this.colJobType.OptionsColumn.AllowEdit = false;
            this.colJobType.OptionsColumn.AllowFocus = false;
            this.colJobType.OptionsColumn.ReadOnly = true;
            this.colJobType.Visible = true;
            this.colJobType.VisibleIndex = 6;
            // 
            // colSiteWeather1
            // 
            this.colSiteWeather1.Caption = "Site Weather";
            this.colSiteWeather1.FieldName = "SiteWeather";
            this.colSiteWeather1.Name = "colSiteWeather1";
            this.colSiteWeather1.OptionsColumn.AllowEdit = false;
            this.colSiteWeather1.OptionsColumn.AllowFocus = false;
            this.colSiteWeather1.OptionsColumn.ReadOnly = true;
            this.colSiteWeather1.Visible = true;
            this.colSiteWeather1.VisibleIndex = 17;
            this.colSiteWeather1.Width = 84;
            // 
            // colSiteTemperature1
            // 
            this.colSiteTemperature1.Caption = "Site Temperature";
            this.colSiteTemperature1.FieldName = "SiteTemperature";
            this.colSiteTemperature1.Name = "colSiteTemperature1";
            this.colSiteTemperature1.OptionsColumn.AllowEdit = false;
            this.colSiteTemperature1.OptionsColumn.AllowFocus = false;
            this.colSiteTemperature1.OptionsColumn.ReadOnly = true;
            this.colSiteTemperature1.Visible = true;
            this.colSiteTemperature1.VisibleIndex = 18;
            this.colSiteTemperature1.Width = 104;
            // 
            // colSnowOnSite1
            // 
            this.colSnowOnSite1.Caption = "Snow On Site";
            this.colSnowOnSite1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colSnowOnSite1.FieldName = "SnowOnSite";
            this.colSnowOnSite1.Name = "colSnowOnSite1";
            this.colSnowOnSite1.OptionsColumn.AllowEdit = false;
            this.colSnowOnSite1.OptionsColumn.AllowFocus = false;
            this.colSnowOnSite1.OptionsColumn.ReadOnly = true;
            this.colSnowOnSite1.Visible = true;
            this.colSnowOnSite1.VisibleIndex = 16;
            this.colSnowOnSite1.Width = 85;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colVisitAborted1
            // 
            this.colVisitAborted1.Caption = "Visit Aborted";
            this.colVisitAborted1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colVisitAborted1.FieldName = "VisitAborted";
            this.colVisitAborted1.Name = "colVisitAborted1";
            this.colVisitAborted1.OptionsColumn.AllowEdit = false;
            this.colVisitAborted1.OptionsColumn.AllowFocus = false;
            this.colVisitAborted1.OptionsColumn.ReadOnly = true;
            this.colVisitAborted1.Visible = true;
            this.colVisitAborted1.VisibleIndex = 14;
            this.colVisitAborted1.Width = 82;
            // 
            // colNoAccess1
            // 
            this.colNoAccess1.Caption = "No Access";
            this.colNoAccess1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colNoAccess1.FieldName = "NoAccess";
            this.colNoAccess1.Name = "colNoAccess1";
            this.colNoAccess1.OptionsColumn.AllowEdit = false;
            this.colNoAccess1.OptionsColumn.AllowFocus = false;
            this.colNoAccess1.OptionsColumn.ReadOnly = true;
            this.colNoAccess1.Visible = true;
            this.colNoAccess1.VisibleIndex = 15;
            // 
            // colClientPONumber1
            // 
            this.colClientPONumber1.Caption = "Client PO Number";
            this.colClientPONumber1.FieldName = "ClientPONumber";
            this.colClientPONumber1.Name = "colClientPONumber1";
            this.colClientPONumber1.OptionsColumn.AllowEdit = false;
            this.colClientPONumber1.OptionsColumn.AllowFocus = false;
            this.colClientPONumber1.OptionsColumn.ReadOnly = true;
            this.colClientPONumber1.Visible = true;
            this.colClientPONumber1.VisibleIndex = 13;
            this.colClientPONumber1.Width = 105;
            // 
            // colSaltUsed1
            // 
            this.colSaltUsed1.Caption = "Salt Used";
            this.colSaltUsed1.ColumnEdit = this.repositoryItemTextEdit25KgBags2;
            this.colSaltUsed1.FieldName = "SaltUsed";
            this.colSaltUsed1.Name = "colSaltUsed1";
            this.colSaltUsed1.OptionsColumn.AllowEdit = false;
            this.colSaltUsed1.OptionsColumn.AllowFocus = false;
            this.colSaltUsed1.OptionsColumn.ReadOnly = true;
            this.colSaltUsed1.Visible = true;
            this.colSaltUsed1.VisibleIndex = 7;
            // 
            // repositoryItemTextEdit25KgBags2
            // 
            this.repositoryItemTextEdit25KgBags2.AutoHeight = false;
            this.repositoryItemTextEdit25KgBags2.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25KgBags2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25KgBags2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25KgBags2.Name = "repositoryItemTextEdit25KgBags2";
            // 
            // colSaltSell1
            // 
            this.colSaltSell1.Caption = "Salt Cost";
            this.colSaltSell1.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colSaltSell1.FieldName = "SaltSell";
            this.colSaltSell1.Name = "colSaltSell1";
            this.colSaltSell1.OptionsColumn.AllowEdit = false;
            this.colSaltSell1.OptionsColumn.AllowFocus = false;
            this.colSaltSell1.OptionsColumn.ReadOnly = true;
            this.colSaltSell1.Visible = true;
            this.colSaltSell1.VisibleIndex = 8;
            // 
            // repositoryItemTextEditCurrency4
            // 
            this.repositoryItemTextEditCurrency4.AutoHeight = false;
            this.repositoryItemTextEditCurrency4.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency4.Name = "repositoryItemTextEditCurrency4";
            // 
            // colOtherSell1
            // 
            this.colOtherSell1.Caption = "Other Cost";
            this.colOtherSell1.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colOtherSell1.FieldName = "OtherSell";
            this.colOtherSell1.Name = "colOtherSell1";
            this.colOtherSell1.OptionsColumn.AllowEdit = false;
            this.colOtherSell1.OptionsColumn.AllowFocus = false;
            this.colOtherSell1.OptionsColumn.ReadOnly = true;
            this.colOtherSell1.Visible = true;
            this.colOtherSell1.VisibleIndex = 9;
            // 
            // colLabourSell1
            // 
            this.colLabourSell1.Caption = "Labour Cost";
            this.colLabourSell1.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colLabourSell1.FieldName = "LabourSell";
            this.colLabourSell1.Name = "colLabourSell1";
            this.colLabourSell1.OptionsColumn.AllowEdit = false;
            this.colLabourSell1.OptionsColumn.AllowFocus = false;
            this.colLabourSell1.OptionsColumn.ReadOnly = true;
            this.colLabourSell1.Visible = true;
            this.colLabourSell1.VisibleIndex = 10;
            // 
            // colTotalSell1
            // 
            this.colTotalSell1.Caption = "Total Cost";
            this.colTotalSell1.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.colTotalSell1.FieldName = "TotalSell";
            this.colTotalSell1.Name = "colTotalSell1";
            this.colTotalSell1.OptionsColumn.AllowEdit = false;
            this.colTotalSell1.OptionsColumn.AllowFocus = false;
            this.colTotalSell1.OptionsColumn.ReadOnly = true;
            this.colTotalSell1.Visible = true;
            this.colTotalSell1.VisibleIndex = 11;
            // 
            // colintContractorID
            // 
            this.colintContractorID.Caption = "Team ID";
            this.colintContractorID.FieldName = "intContractorID";
            this.colintContractorID.Name = "colintContractorID";
            this.colintContractorID.OptionsColumn.AllowEdit = false;
            this.colintContractorID.OptionsColumn.AllowFocus = false;
            this.colintContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName
            // 
            this.colTeamName.Caption = "Team Name";
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.Visible = true;
            this.colTeamName.VisibleIndex = 0;
            this.colTeamName.Width = 162;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Team";
            this.colInternalContractor.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Visible = true;
            this.colInternalContractor.VisibleIndex = 12;
            this.colInternalContractor.Width = 88;
            // 
            // colSelfBillingInvoiceDate
            // 
            this.colSelfBillingInvoiceDate.Caption = "Self Billing Invoice Date";
            this.colSelfBillingInvoiceDate.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colSelfBillingInvoiceDate.FieldName = "SelfBillingInvoiceDate";
            this.colSelfBillingInvoiceDate.Name = "colSelfBillingInvoiceDate";
            this.colSelfBillingInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceDate.Visible = true;
            this.colSelfBillingInvoiceDate.VisibleIndex = 5;
            this.colSelfBillingInvoiceDate.Width = 129;
            // 
            // colSelfBillingInvoiceNumber
            // 
            this.colSelfBillingInvoiceNumber.Caption = "Self Billing Invoice Number";
            this.colSelfBillingInvoiceNumber.FieldName = "SelfBillingInvoiceNumber";
            this.colSelfBillingInvoiceNumber.Name = "colSelfBillingInvoiceNumber";
            this.colSelfBillingInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceNumber.Visible = true;
            this.colSelfBillingInvoiceNumber.VisibleIndex = 4;
            this.colSelfBillingInvoiceNumber.Width = 143;
            // 
            // bar1
            // 
            this.bar1.BarName = "Parameters";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(487, 246);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClientInvoiceWizard),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClientSpreadsheets, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemParameters, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadData)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Parameters";
            // 
            // bbiClientInvoiceWizard
            // 
            this.bbiClientInvoiceWizard.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bbiClientInvoiceWizard.Caption = "Invoice Wizard";
            this.bbiClientInvoiceWizard.Id = 29;
            this.bbiClientInvoiceWizard.ImageOptions.Image = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.bbiClientInvoiceWizard.Name = "bbiClientInvoiceWizard";
            this.bbiClientInvoiceWizard.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Invoice Wizard - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiClientInvoiceWizard.SuperTip = superToolTip1;
            this.bbiClientInvoiceWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClientInvoiceWizard_ItemClick);
            // 
            // bbiClientSpreadsheets
            // 
            this.bbiClientSpreadsheets.Caption = "Client Spreadsheets";
            this.bbiClientSpreadsheets.Id = 30;
            this.bbiClientSpreadsheets.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClientSpreadsheets.ImageOptions.Image")));
            this.bbiClientSpreadsheets.Name = "bbiClientSpreadsheets";
            this.bbiClientSpreadsheets.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Client Spreadsheets - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to create client spreadsheets.\r\n\r\n<b><color=green>Tip:</color></b> Clien" +
    "t spreadsheets are only created for clients selected within the list.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiClientSpreadsheets.SuperTip = superToolTip2;
            this.bbiClientSpreadsheets.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClientSpreadsheets_ItemClick);
            // 
            // barEditItemParameters
            // 
            this.barEditItemParameters.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItemParameters.Caption = "Parameters";
            this.barEditItemParameters.Edit = this.repositoryItemPopupContainerEditParameters;
            this.barEditItemParameters.EditValue = "Parameters";
            this.barEditItemParameters.EditWidth = 80;
            this.barEditItemParameters.Id = 27;
            this.barEditItemParameters.Name = "barEditItemParameters";
            // 
            // repositoryItemPopupContainerEditParameters
            // 
            this.repositoryItemPopupContainerEditParameters.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEditParameters.Appearance.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEditParameters.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEditParameters.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEditParameters.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEditParameters.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEditParameters.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEditParameters.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEditParameters.AutoHeight = false;
            this.repositoryItemPopupContainerEditParameters.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditParameters.Name = "repositoryItemPopupContainerEditParameters";
            this.repositoryItemPopupContainerEditParameters.PopupControl = this.popupContainerControlParameters;
            this.repositoryItemPopupContainerEditParameters.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditParameters.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditParameters_QueryResultValue);
            this.repositoryItemPopupContainerEditParameters.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.repositoryItemPopupContainerEditParameters_QueryPopUp);
            // 
            // bbiReloadData
            // 
            this.bbiReloadData.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiReloadData.Caption = "Reload Data";
            this.bbiReloadData.Id = 28;
            this.bbiReloadData.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiReloadData.Name = "bbiReloadData";
            this.bbiReloadData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Reload Data - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to Reload Data. \r\n\r\nOnce any parameters have been changed, this button s" +
    "hould be clicked to reload the data using the updated parameters.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiReloadData.SuperTip = superToolTip5;
            this.bbiReloadData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadData_ItemClick);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp04237_GC_Company_Filter_ListTableAdapter
            // 
            this.sp04237_GC_Company_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04258_GC_Finance_Client_Invoice_LinesTableAdapter
            // 
            this.sp04258_GC_Finance_Client_Invoice_LinesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_LineTableAdapter
            // 
            this.sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_LineTableAdapter.ClearBeforeFill = true;
            // 
            // sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_LineTableAdapter
            // 
            this.sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_LineTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            this.sp00039GetFormPermissionsForUserBindingSource.Position = 0;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl3;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl4;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(491, 235);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFinanceTeamSpreadsheets),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPaidComplete, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemParameters2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadData2)});
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar2.Text = "Custom 3";
            // 
            // bbiFinanceTeamSpreadsheets
            // 
            this.bbiFinanceTeamSpreadsheets.Caption = "Create Spreadsheets";
            this.bbiFinanceTeamSpreadsheets.Id = 33;
            this.bbiFinanceTeamSpreadsheets.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFinanceTeamSpreadsheets.ImageOptions.Image")));
            this.bbiFinanceTeamSpreadsheets.Name = "bbiFinanceTeamSpreadsheets";
            this.bbiFinanceTeamSpreadsheets.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Create Spreadsheets - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to create Finance spreadsheets.\r\n\r\nThe Finance spreadsheets can be impor" +
    "ted directly into your Finance System.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiFinanceTeamSpreadsheets.SuperTip = superToolTip6;
            this.bbiFinanceTeamSpreadsheets.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFinanceTeamSpreadsheets_ItemClick);
            // 
            // bbiPaidComplete
            // 
            this.bbiPaidComplete.Caption = "Set as Paid";
            this.bbiPaidComplete.Id = 34;
            this.bbiPaidComplete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaidComplete.ImageOptions.Image")));
            this.bbiPaidComplete.Name = "bbiPaidComplete";
            this.bbiPaidComplete.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Set as Paid - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to Set the selected (Ticked) jobs as \"Paid - Complete\".\r\n\r\n<b><color=gre" +
    "en>Tip:</color></b> To change a Paid - Completed job you will need to edit the j" +
    "ob and manually change it\'s Job Status.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiPaidComplete.SuperTip = superToolTip7;
            this.bbiPaidComplete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPaidComplete_ItemClick);
            // 
            // barEditItemParameters2
            // 
            this.barEditItemParameters2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItemParameters2.Caption = "Parameters";
            this.barEditItemParameters2.Edit = this.repositoryItemPopupContainerEditCompanyFilter2;
            this.barEditItemParameters2.EditValue = "Parameters";
            this.barEditItemParameters2.EditWidth = 79;
            this.barEditItemParameters2.Id = 32;
            this.barEditItemParameters2.ItemAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.barEditItemParameters2.ItemAppearance.Normal.Options.UseBackColor = true;
            this.barEditItemParameters2.Name = "barEditItemParameters2";
            // 
            // repositoryItemPopupContainerEditCompanyFilter2
            // 
            this.repositoryItemPopupContainerEditCompanyFilter2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEditCompanyFilter2.Appearance.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEditCompanyFilter2.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEditCompanyFilter2.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEditCompanyFilter2.AppearanceDropDown.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEditCompanyFilter2.AppearanceDropDown.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEditCompanyFilter2.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEditCompanyFilter2.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEditCompanyFilter2.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEditCompanyFilter2.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEditCompanyFilter2.AutoHeight = false;
            this.repositoryItemPopupContainerEditCompanyFilter2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditCompanyFilter2.Name = "repositoryItemPopupContainerEditCompanyFilter2";
            this.repositoryItemPopupContainerEditCompanyFilter2.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditCompanyFilter2.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.repositoryItemPopupContainerEditCompanyFilter2_QueryPopUp);
            // 
            // bbiReloadData2
            // 
            this.bbiReloadData2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiReloadData2.Caption = "Reload Data";
            this.bbiReloadData2.Id = 31;
            this.bbiReloadData2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiReloadData2.Name = "bbiReloadData2";
            this.bbiReloadData2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Reload Data - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to Reload Data. \r\n\r\nOnce any parameters have been changed, this button s" +
    "hould be clicked to reload the data using the updated parameters.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiReloadData2.SuperTip = superToolTip8;
            this.bbiReloadData2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadData2_ItemClick);
            // 
            // sp04265_GC_Finance_Team_Jobs_For_ExportTableAdapter
            // 
            this.sp04265_GC_Finance_Team_Jobs_For_ExportTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Finance_Invoice_Jobs
            // 
            this.ClientSize = new System.Drawing.Size(1046, 542);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Finance_Invoice_Jobs";
            this.Text = "Finance - Job Invoicing Manager";
            this.Activated += new System.EventHandler(this.frm_GC_Finance_Invoice_Jobs_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Finance_Invoice_Jobs_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Finance_Invoice_Jobs_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlParameters)).EndInit();
            this.popupContainerControlParameters.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIncludeCompleted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04258GCFinanceClientInvoiceLinesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04259GCFinanceGritCalloutsForClientInvoiceLineBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04260GCFinanceSnowCalloutManagerForClientInvoiceLineBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04265GCFinanceTeamJobsForExportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25KgBags2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditCompanyFilter2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemParameters;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditParameters;
        private DevExpress.XtraBars.BarButtonItem bbiReloadData;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlParameters;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOrder;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton btnOK2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_AT dataSet_AT;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource;
        private DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter sp04237_GC_Company_Filter_ListTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiClientInvoiceWizard;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp04258GCFinanceClientInvoiceLinesBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceLineID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colLineValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingJobCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceJobCount;
        private DataSet_GC_CoreTableAdapters.sp04258_GC_Finance_Client_Invoice_LinesTableAdapter sp04258_GC_Finance_Client_Invoice_LinesTableAdapter;
        private DevExpress.XtraEditors.CheckEdit checkEditIncludeCompleted;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource sp04259GCFinanceGritCalloutsForClientInvoiceLineBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCallOutID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorIsDirectLabour;
        private DevExpress.XtraGrid.Columns.GridColumn colGritMobileTelephoneNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContarctorName;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colImportedWeatherForecastID;
        private DevExpress.XtraGrid.Columns.GridColumn colTextSentTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorReceivedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorRespondedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorETA;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitAborted;
        private DevExpress.XtraGrid.Columns.GridColumn colAbortedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishedLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishedLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltSell;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamHourlyRate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamCharge;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCost;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherSell;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByName;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractorReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClientReason;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSourceLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSourceLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteWeather;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTemperature;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamPresent;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceFailure;
        private DevExpress.XtraGrid.Columns.GridColumn colComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colGritJobTransferMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colProfit;
        private DevExpress.XtraGrid.Columns.GridColumn colMarkup;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardSell;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoice;
        private System.Windows.Forms.BindingSource sp04260GCFinanceSnowCalloutManagerForClientInvoiceLineBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutID;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCPONumberSuffix;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorContactedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientHowSoon;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccessAbortedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamPOFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTimesheetSubmitted;
        private DataSet_GC_CoreTableAdapters.sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_LineTableAdapter sp04259_GC_Finance_Grit_Callouts_For_Client_Invoice_LineTableAdapter;
        private DataSet_GC_CoreTableAdapters.sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_LineTableAdapter sp04260_GC_Finance_Snow_Callout_Manager_For_Client_Invoice_LineTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceLineID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceLineDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceLineID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceLineDescription1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KgBags;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercent;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraBars.BarButtonItem bbiClientSpreadsheets;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiReloadData2;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraBars.BarEditItem barEditItemParameters2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditCompanyFilter2;
        private DevExpress.XtraBars.BarButtonItem bbiFinanceTeamSpreadsheets;
        private System.Windows.Forms.BindingSource sp04265GCFinanceTeamJobsForExportBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colUniqueID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobType;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteWeather1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTemperature1;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowOnSite1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitAborted1;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccess1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUsed1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25KgBags2;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltSell1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency4;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherSell1;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourSell1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell1;
        private DevExpress.XtraGrid.Columns.GridColumn colintContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DataSet_GC_CoreTableAdapters.sp04265_GC_Finance_Team_Jobs_For_ExportTableAdapter sp04265_GC_Finance_Team_Jobs_For_ExportTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiPaidComplete;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSentToAccounts;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colSentToClientMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colSentToClientMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeSent;
        private DevExpress.XtraGrid.Columns.GridColumn colAudited;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReason;
    }
}

namespace WoodPlan5
{
    partial class frm_GC_Salt_Depot_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Salt_Depot_Edit));
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.TransferredOutGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sp04212GCSaltDepotEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.OrderedInGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TransferredInGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.StartingGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WarningLabel = new DevExpress.XtraEditors.LabelControl();
            this.EmailPasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.UrgentGritLevelSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WarningGritLevelSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CurrentGritLevelSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactNamePositionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WebSiteTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TextTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FaxTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MobileTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.GritDepotIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.CurrentCostConvertedTextEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForGritDepotID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMobile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFax = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForText = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWebSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactNamePosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEmailPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCurrentGritLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWarningGritLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUrgentGritLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartingGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransferredInGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOrderedInGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransferredOutGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCurrentCostConverted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWarningLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp04212_GC_Salt_Depot_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04212_GC_Salt_Depot_EditTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredOutGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04212GCSaltDepotEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderedInGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredInGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartingGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailPasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UrgentGritLevelSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WarningGritLevelSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentGritLevelSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNamePositionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSiteTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaxTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritDepotIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentCostConvertedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritDepotID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactNamePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentGritLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningGritLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUrgentGritLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartingGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredInGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderedInGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredOutGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentCostConverted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(731, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            this.barDockControlBottom.Size = new System.Drawing.Size(731, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(731, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(731, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Size = new System.Drawing.Size(731, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(731, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.TransferredOutGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.OrderedInGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TransferredInGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.StartingGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.WarningLabel);
            this.dataLayoutControl1.Controls.Add(this.EmailPasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.UrgentGritLevelSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.WarningGritLevelSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CurrentGritLevelSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactNamePositionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WebSiteTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TextTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FaxTextEdit);
            this.dataLayoutControl1.Controls.Add(this.MobileTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.GritDepotIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.CurrentCostConvertedTextEdit);
            this.dataLayoutControl1.DataSource = this.sp04212GCSaltDepotEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGritDepotID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1568, 292, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(731, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // TransferredOutGritAmountSpinEdit
            // 
            this.TransferredOutGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "TransferredOutGritAmount", true));
            this.TransferredOutGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TransferredOutGritAmountSpinEdit.Location = new System.Drawing.Point(170, 200);
            this.TransferredOutGritAmountSpinEdit.MenuManager = this.barManager1;
            this.TransferredOutGritAmountSpinEdit.Name = "TransferredOutGritAmountSpinEdit";
            this.TransferredOutGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TransferredOutGritAmountSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.TransferredOutGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TransferredOutGritAmountSpinEdit.Properties.ReadOnly = true;
            this.TransferredOutGritAmountSpinEdit.Size = new System.Drawing.Size(185, 20);
            this.TransferredOutGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.TransferredOutGritAmountSpinEdit.TabIndex = 39;
            // 
            // sp04212GCSaltDepotEditBindingSource
            // 
            this.sp04212GCSaltDepotEditBindingSource.DataMember = "sp04212_GC_Salt_Depot_Edit";
            this.sp04212GCSaltDepotEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // OrderedInGritAmountSpinEdit
            // 
            this.OrderedInGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "OrderedInGritAmount", true));
            this.OrderedInGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OrderedInGritAmountSpinEdit.Location = new System.Drawing.Point(170, 176);
            this.OrderedInGritAmountSpinEdit.MenuManager = this.barManager1;
            this.OrderedInGritAmountSpinEdit.Name = "OrderedInGritAmountSpinEdit";
            this.OrderedInGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OrderedInGritAmountSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.OrderedInGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OrderedInGritAmountSpinEdit.Properties.ReadOnly = true;
            this.OrderedInGritAmountSpinEdit.Size = new System.Drawing.Size(185, 20);
            this.OrderedInGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.OrderedInGritAmountSpinEdit.TabIndex = 38;
            // 
            // TransferredInGritAmountSpinEdit
            // 
            this.TransferredInGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "TransferredInGritAmount", true));
            this.TransferredInGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TransferredInGritAmountSpinEdit.Location = new System.Drawing.Point(170, 152);
            this.TransferredInGritAmountSpinEdit.MenuManager = this.barManager1;
            this.TransferredInGritAmountSpinEdit.Name = "TransferredInGritAmountSpinEdit";
            this.TransferredInGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TransferredInGritAmountSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.TransferredInGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TransferredInGritAmountSpinEdit.Properties.ReadOnly = true;
            this.TransferredInGritAmountSpinEdit.Size = new System.Drawing.Size(185, 20);
            this.TransferredInGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.TransferredInGritAmountSpinEdit.TabIndex = 37;
            // 
            // StartingGritAmountSpinEdit
            // 
            this.StartingGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "StartingGritAmount", true));
            this.StartingGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StartingGritAmountSpinEdit.Location = new System.Drawing.Point(170, 128);
            this.StartingGritAmountSpinEdit.MenuManager = this.barManager1;
            this.StartingGritAmountSpinEdit.Name = "StartingGritAmountSpinEdit";
            this.StartingGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StartingGritAmountSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.StartingGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartingGritAmountSpinEdit.Size = new System.Drawing.Size(185, 20);
            this.StartingGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.StartingGritAmountSpinEdit.TabIndex = 36;
            this.StartingGritAmountSpinEdit.EditValueChanged += new System.EventHandler(this.StartingGritAmountSpinEdit_EditValueChanged);
            this.StartingGritAmountSpinEdit.Validated += new System.EventHandler(this.StartingGritAmountSpinEdit_Validated);
            // 
            // WarningLabel
            // 
            this.WarningLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F);
            this.WarningLabel.Appearance.ForeColor = System.Drawing.Color.White;
            this.WarningLabel.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.WarningLabel.Appearance.ImageIndex = 3;
            this.WarningLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.WarningLabel.Location = new System.Drawing.Point(433, 10);
            this.WarningLabel.Name = "WarningLabel";
            this.WarningLabel.Size = new System.Drawing.Size(271, 20);
            this.WarningLabel.StyleController = this.dataLayoutControl1;
            this.WarningLabel.TabIndex = 35;
            this.WarningLabel.Text = "       Warning: Current Salt Level Below Warning Level";
            // 
            // EmailPasswordTextEdit
            // 
            this.EmailPasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "EmailPassword", true));
            this.EmailPasswordTextEdit.Location = new System.Drawing.Point(182, 508);
            this.EmailPasswordTextEdit.MenuManager = this.barManager1;
            this.EmailPasswordTextEdit.Name = "EmailPasswordTextEdit";
            this.EmailPasswordTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmailPasswordTextEdit, true);
            this.EmailPasswordTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmailPasswordTextEdit, optionsSpelling1);
            this.EmailPasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailPasswordTextEdit.TabIndex = 32;
            // 
            // UrgentGritLevelSpinEdit
            // 
            this.UrgentGritLevelSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "UrgentGritLevel", true));
            this.UrgentGritLevelSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UrgentGritLevelSpinEdit.Location = new System.Drawing.Point(170, 272);
            this.UrgentGritLevelSpinEdit.MenuManager = this.barManager1;
            this.UrgentGritLevelSpinEdit.Name = "UrgentGritLevelSpinEdit";
            this.UrgentGritLevelSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.UrgentGritLevelSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.UrgentGritLevelSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UrgentGritLevelSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.UrgentGritLevelSpinEdit.Size = new System.Drawing.Size(185, 20);
            this.UrgentGritLevelSpinEdit.StyleController = this.dataLayoutControl1;
            this.UrgentGritLevelSpinEdit.TabIndex = 31;
            this.UrgentGritLevelSpinEdit.EditValueChanged += new System.EventHandler(this.UrgentGritLevelSpinEdit_EditValueChanged);
            this.UrgentGritLevelSpinEdit.Validated += new System.EventHandler(this.UrgentGritLevelSpinEdit_Validated);
            // 
            // WarningGritLevelSpinEdit
            // 
            this.WarningGritLevelSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "WarningGritLevel", true));
            this.WarningGritLevelSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WarningGritLevelSpinEdit.Location = new System.Drawing.Point(170, 248);
            this.WarningGritLevelSpinEdit.MenuManager = this.barManager1;
            this.WarningGritLevelSpinEdit.Name = "WarningGritLevelSpinEdit";
            this.WarningGritLevelSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.WarningGritLevelSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.WarningGritLevelSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WarningGritLevelSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.WarningGritLevelSpinEdit.Size = new System.Drawing.Size(185, 20);
            this.WarningGritLevelSpinEdit.StyleController = this.dataLayoutControl1;
            this.WarningGritLevelSpinEdit.TabIndex = 29;
            this.WarningGritLevelSpinEdit.EditValueChanged += new System.EventHandler(this.WarningGritLevelSpinEdit_EditValueChanged);
            this.WarningGritLevelSpinEdit.Validated += new System.EventHandler(this.WarningGritLevelSpinEdit_Validated);
            // 
            // CurrentGritLevelSpinEdit
            // 
            this.CurrentGritLevelSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "CurrentGritLevel", true));
            this.CurrentGritLevelSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CurrentGritLevelSpinEdit.Location = new System.Drawing.Point(170, 224);
            this.CurrentGritLevelSpinEdit.MenuManager = this.barManager1;
            this.CurrentGritLevelSpinEdit.Name = "CurrentGritLevelSpinEdit";
            this.CurrentGritLevelSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CurrentGritLevelSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.CurrentGritLevelSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CurrentGritLevelSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.CurrentGritLevelSpinEdit.Properties.ReadOnly = true;
            this.CurrentGritLevelSpinEdit.Size = new System.Drawing.Size(185, 20);
            this.CurrentGritLevelSpinEdit.StyleController = this.dataLayoutControl1;
            this.CurrentGritLevelSpinEdit.TabIndex = 28;
            // 
            // LongitudeTextEdit
            // 
            this.LongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Longitude", true));
            this.LongitudeTextEdit.Location = new System.Drawing.Point(158, 672);
            this.LongitudeTextEdit.MenuManager = this.barManager1;
            this.LongitudeTextEdit.Name = "LongitudeTextEdit";
            this.LongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.LongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LongitudeTextEdit, true);
            this.LongitudeTextEdit.Size = new System.Drawing.Size(544, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LongitudeTextEdit, optionsSpelling2);
            this.LongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.LongitudeTextEdit.TabIndex = 27;
            // 
            // LatitudeTextEdit
            // 
            this.LatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Latitude", true));
            this.LatitudeTextEdit.Location = new System.Drawing.Point(158, 648);
            this.LatitudeTextEdit.MenuManager = this.barManager1;
            this.LatitudeTextEdit.Name = "LatitudeTextEdit";
            this.LatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.LatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LatitudeTextEdit, true);
            this.LatitudeTextEdit.Size = new System.Drawing.Size(544, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LatitudeTextEdit, optionsSpelling3);
            this.LatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.LatitudeTextEdit.TabIndex = 26;
            // 
            // ContactNamePositionTextEdit
            // 
            this.ContactNamePositionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "ContactNamePosition", true));
            this.ContactNamePositionTextEdit.Location = new System.Drawing.Point(182, 590);
            this.ContactNamePositionTextEdit.MenuManager = this.barManager1;
            this.ContactNamePositionTextEdit.Name = "ContactNamePositionTextEdit";
            this.ContactNamePositionTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactNamePositionTextEdit, true);
            this.ContactNamePositionTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactNamePositionTextEdit, optionsSpelling4);
            this.ContactNamePositionTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactNamePositionTextEdit.TabIndex = 25;
            // 
            // ContactNameTextEdit
            // 
            this.ContactNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "ContactName", true));
            this.ContactNameTextEdit.Location = new System.Drawing.Point(182, 566);
            this.ContactNameTextEdit.MenuManager = this.barManager1;
            this.ContactNameTextEdit.Name = "ContactNameTextEdit";
            this.ContactNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactNameTextEdit, true);
            this.ContactNameTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactNameTextEdit, optionsSpelling5);
            this.ContactNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactNameTextEdit.TabIndex = 24;
            // 
            // WebSiteTextEdit
            // 
            this.WebSiteTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "WebSite", true));
            this.WebSiteTextEdit.Location = new System.Drawing.Point(182, 532);
            this.WebSiteTextEdit.MenuManager = this.barManager1;
            this.WebSiteTextEdit.Name = "WebSiteTextEdit";
            this.WebSiteTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WebSiteTextEdit, true);
            this.WebSiteTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WebSiteTextEdit, optionsSpelling6);
            this.WebSiteTextEdit.StyleController = this.dataLayoutControl1;
            this.WebSiteTextEdit.TabIndex = 23;
            // 
            // EmailTextEdit
            // 
            this.EmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Email", true));
            this.EmailTextEdit.Location = new System.Drawing.Point(182, 484);
            this.EmailTextEdit.MenuManager = this.barManager1;
            this.EmailTextEdit.Name = "EmailTextEdit";
            this.EmailTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmailTextEdit, true);
            this.EmailTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmailTextEdit, optionsSpelling7);
            this.EmailTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailTextEdit.TabIndex = 22;
            // 
            // TextTextEdit
            // 
            this.TextTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Text", true));
            this.TextTextEdit.Location = new System.Drawing.Point(182, 460);
            this.TextTextEdit.MenuManager = this.barManager1;
            this.TextTextEdit.Name = "TextTextEdit";
            this.TextTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TextTextEdit, true);
            this.TextTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TextTextEdit, optionsSpelling8);
            this.TextTextEdit.StyleController = this.dataLayoutControl1;
            this.TextTextEdit.TabIndex = 21;
            // 
            // FaxTextEdit
            // 
            this.FaxTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Fax", true));
            this.FaxTextEdit.Location = new System.Drawing.Point(182, 436);
            this.FaxTextEdit.MenuManager = this.barManager1;
            this.FaxTextEdit.Name = "FaxTextEdit";
            this.FaxTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FaxTextEdit, true);
            this.FaxTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FaxTextEdit, optionsSpelling9);
            this.FaxTextEdit.StyleController = this.dataLayoutControl1;
            this.FaxTextEdit.TabIndex = 20;
            // 
            // MobileTextEdit
            // 
            this.MobileTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Mobile", true));
            this.MobileTextEdit.Location = new System.Drawing.Point(182, 412);
            this.MobileTextEdit.MenuManager = this.barManager1;
            this.MobileTextEdit.Name = "MobileTextEdit";
            this.MobileTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MobileTextEdit, true);
            this.MobileTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MobileTextEdit, optionsSpelling10);
            this.MobileTextEdit.StyleController = this.dataLayoutControl1;
            this.MobileTextEdit.TabIndex = 19;
            // 
            // TelephoneTextEdit
            // 
            this.TelephoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Telephone", true));
            this.TelephoneTextEdit.Location = new System.Drawing.Point(182, 388);
            this.TelephoneTextEdit.MenuManager = this.barManager1;
            this.TelephoneTextEdit.Name = "TelephoneTextEdit";
            this.TelephoneTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TelephoneTextEdit, true);
            this.TelephoneTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TelephoneTextEdit, optionsSpelling11);
            this.TelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.TelephoneTextEdit.TabIndex = 18;
            // 
            // PostcodeTextEdit
            // 
            this.PostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Postcode", true));
            this.PostcodeTextEdit.Location = new System.Drawing.Point(182, 508);
            this.PostcodeTextEdit.MenuManager = this.barManager1;
            this.PostcodeTextEdit.Name = "PostcodeTextEdit";
            this.PostcodeTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PostcodeTextEdit, true);
            this.PostcodeTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PostcodeTextEdit, optionsSpelling12);
            this.PostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.PostcodeTextEdit.TabIndex = 17;
            // 
            // AddressLine5TextEdit
            // 
            this.AddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "AddressLine5", true));
            this.AddressLine5TextEdit.Location = new System.Drawing.Point(182, 484);
            this.AddressLine5TextEdit.MenuManager = this.barManager1;
            this.AddressLine5TextEdit.Name = "AddressLine5TextEdit";
            this.AddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine5TextEdit, true);
            this.AddressLine5TextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine5TextEdit, optionsSpelling13);
            this.AddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine5TextEdit.TabIndex = 16;
            // 
            // AddressLine4TextEdit
            // 
            this.AddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "AddressLine4", true));
            this.AddressLine4TextEdit.Location = new System.Drawing.Point(182, 460);
            this.AddressLine4TextEdit.MenuManager = this.barManager1;
            this.AddressLine4TextEdit.Name = "AddressLine4TextEdit";
            this.AddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine4TextEdit, true);
            this.AddressLine4TextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine4TextEdit, optionsSpelling14);
            this.AddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine4TextEdit.TabIndex = 15;
            // 
            // AddressLine3TextEdit
            // 
            this.AddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "AddressLine3", true));
            this.AddressLine3TextEdit.Location = new System.Drawing.Point(182, 436);
            this.AddressLine3TextEdit.MenuManager = this.barManager1;
            this.AddressLine3TextEdit.Name = "AddressLine3TextEdit";
            this.AddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine3TextEdit, true);
            this.AddressLine3TextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine3TextEdit, optionsSpelling15);
            this.AddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine3TextEdit.TabIndex = 14;
            // 
            // AddressLine2TextEdit
            // 
            this.AddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "AddressLine2", true));
            this.AddressLine2TextEdit.Location = new System.Drawing.Point(182, 412);
            this.AddressLine2TextEdit.MenuManager = this.barManager1;
            this.AddressLine2TextEdit.Name = "AddressLine2TextEdit";
            this.AddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine2TextEdit, true);
            this.AddressLine2TextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine2TextEdit, optionsSpelling16);
            this.AddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine2TextEdit.TabIndex = 13;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04212GCSaltDepotEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(159, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(222, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 9;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // GritDepotIDTextEdit
            // 
            this.GritDepotIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "GritDepotID", true));
            this.GritDepotIDTextEdit.Location = new System.Drawing.Point(128, 36);
            this.GritDepotIDTextEdit.MenuManager = this.barManager1;
            this.GritDepotIDTextEdit.Name = "GritDepotIDTextEdit";
            this.GritDepotIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GritDepotIDTextEdit, true);
            this.GritDepotIDTextEdit.Size = new System.Drawing.Size(471, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GritDepotIDTextEdit, optionsSpelling17);
            this.GritDepotIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GritDepotIDTextEdit.TabIndex = 4;
            // 
            // AddressLine1TextEdit
            // 
            this.AddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "AddressLine1", true));
            this.AddressLine1TextEdit.Location = new System.Drawing.Point(182, 388);
            this.AddressLine1TextEdit.MenuManager = this.barManager1;
            this.AddressLine1TextEdit.Name = "AddressLine1TextEdit";
            this.AddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine1TextEdit, true);
            this.AddressLine1TextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine1TextEdit, optionsSpelling18);
            this.AddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine1TextEdit.TabIndex = 5;
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Name", true));
            this.NameTextEdit.Location = new System.Drawing.Point(158, 36);
            this.NameTextEdit.MenuManager = this.barManager1;
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NameTextEdit, true);
            this.NameTextEdit.Size = new System.Drawing.Size(544, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NameTextEdit, optionsSpelling19);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 6;
            this.NameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.NameTextEdit_Validating);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 388);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(642, 222);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling20);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 8;
            // 
            // CurrentCostConvertedTextEdit
            // 
            this.CurrentCostConvertedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04212GCSaltDepotEditBindingSource, "CurrentCostConverted", true));
            this.CurrentCostConvertedTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CurrentCostConvertedTextEdit.Location = new System.Drawing.Point(158, 60);
            this.CurrentCostConvertedTextEdit.MenuManager = this.barManager1;
            this.CurrentCostConvertedTextEdit.Name = "CurrentCostConvertedTextEdit";
            this.CurrentCostConvertedTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CurrentCostConvertedTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.CurrentCostConvertedTextEdit.Properties.Mask.EditMask = "c";
            this.CurrentCostConvertedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CurrentCostConvertedTextEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.CurrentCostConvertedTextEdit.Size = new System.Drawing.Size(544, 20);
            this.CurrentCostConvertedTextEdit.StyleController = this.dataLayoutControl1;
            this.CurrentCostConvertedTextEdit.TabIndex = 33;
            // 
            // ItemForGritDepotID
            // 
            this.ItemForGritDepotID.Control = this.GritDepotIDTextEdit;
            this.ItemForGritDepotID.CustomizationFormText = "Salt Depot ID:";
            this.ItemForGritDepotID.Location = new System.Drawing.Point(0, 24);
            this.ItemForGritDepotID.Name = "ItemForGritDepotID";
            this.ItemForGritDepotID.Size = new System.Drawing.Size(591, 24);
            this.ItemForGritDepotID.Text = "Salt Depot ID:";
            this.ItemForGritDepotID.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(714, 790);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.ItemForName,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem1,
            this.ItemForLatitude,
            this.emptySpaceItem5,
            this.ItemForLongitude,
            this.layoutControlGroup8,
            this.emptySpaceItem7,
            this.ItemForCurrentCostConverted,
            this.ItemForWarningLabel,
            this.emptySpaceItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(694, 770);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 306);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(694, 320);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(670, 274);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImage")));
            this.layoutControlGroup5.CustomizationFormText = "Contact Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTelephone,
            this.ItemForMobile,
            this.ItemForFax,
            this.ItemForText,
            this.ItemForEmail,
            this.ItemForWebSite,
            this.ItemForContactName,
            this.ItemForContactNamePosition,
            this.emptySpaceItem6,
            this.ItemForEmailPassword});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(646, 226);
            this.layoutControlGroup5.Text = "Contact Details";
            // 
            // ItemForTelephone
            // 
            this.ItemForTelephone.Control = this.TelephoneTextEdit;
            this.ItemForTelephone.CustomizationFormText = "Telephone:";
            this.ItemForTelephone.Location = new System.Drawing.Point(0, 0);
            this.ItemForTelephone.Name = "ItemForTelephone";
            this.ItemForTelephone.Size = new System.Drawing.Size(646, 24);
            this.ItemForTelephone.Text = "Telephone:";
            this.ItemForTelephone.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForMobile
            // 
            this.ItemForMobile.Control = this.MobileTextEdit;
            this.ItemForMobile.CustomizationFormText = "Mobile:";
            this.ItemForMobile.Location = new System.Drawing.Point(0, 24);
            this.ItemForMobile.Name = "ItemForMobile";
            this.ItemForMobile.Size = new System.Drawing.Size(646, 24);
            this.ItemForMobile.Text = "Mobile:";
            this.ItemForMobile.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForFax
            // 
            this.ItemForFax.Control = this.FaxTextEdit;
            this.ItemForFax.CustomizationFormText = "Fax:";
            this.ItemForFax.Location = new System.Drawing.Point(0, 48);
            this.ItemForFax.Name = "ItemForFax";
            this.ItemForFax.Size = new System.Drawing.Size(646, 24);
            this.ItemForFax.Text = "Fax:";
            this.ItemForFax.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForText
            // 
            this.ItemForText.Control = this.TextTextEdit;
            this.ItemForText.CustomizationFormText = "Text:";
            this.ItemForText.Location = new System.Drawing.Point(0, 72);
            this.ItemForText.Name = "ItemForText";
            this.ItemForText.Size = new System.Drawing.Size(646, 24);
            this.ItemForText.Text = "Text:";
            this.ItemForText.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailTextEdit;
            this.ItemForEmail.CustomizationFormText = "Email:";
            this.ItemForEmail.Location = new System.Drawing.Point(0, 96);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(646, 24);
            this.ItemForEmail.Text = "Email:";
            this.ItemForEmail.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForWebSite
            // 
            this.ItemForWebSite.Control = this.WebSiteTextEdit;
            this.ItemForWebSite.CustomizationFormText = "Website:";
            this.ItemForWebSite.Location = new System.Drawing.Point(0, 144);
            this.ItemForWebSite.Name = "ItemForWebSite";
            this.ItemForWebSite.Size = new System.Drawing.Size(646, 24);
            this.ItemForWebSite.Text = "Website:";
            this.ItemForWebSite.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForContactName
            // 
            this.ItemForContactName.Control = this.ContactNameTextEdit;
            this.ItemForContactName.CustomizationFormText = "Contact Name:";
            this.ItemForContactName.Location = new System.Drawing.Point(0, 178);
            this.ItemForContactName.Name = "ItemForContactName";
            this.ItemForContactName.Size = new System.Drawing.Size(646, 24);
            this.ItemForContactName.Text = "Contact Name:";
            this.ItemForContactName.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForContactNamePosition
            // 
            this.ItemForContactNamePosition.Control = this.ContactNamePositionTextEdit;
            this.ItemForContactNamePosition.CustomizationFormText = "Contact Name Position:";
            this.ItemForContactNamePosition.Location = new System.Drawing.Point(0, 202);
            this.ItemForContactNamePosition.Name = "ItemForContactNamePosition";
            this.ItemForContactNamePosition.Size = new System.Drawing.Size(646, 24);
            this.ItemForContactNamePosition.Text = "Contact Name Position:";
            this.ItemForContactNamePosition.TextSize = new System.Drawing.Size(143, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 168);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(646, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEmailPassword
            // 
            this.ItemForEmailPassword.Control = this.EmailPasswordTextEdit;
            this.ItemForEmailPassword.CustomizationFormText = "Email Password:";
            this.ItemForEmailPassword.Location = new System.Drawing.Point(0, 120);
            this.ItemForEmailPassword.Name = "ItemForEmailPassword";
            this.ItemForEmailPassword.Size = new System.Drawing.Size(646, 24);
            this.ItemForEmailPassword.Text = "Email Password:";
            this.ItemForEmailPassword.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CaptionImage = global::WoodPlan5.Properties.Resources.Home_16x16;
            this.layoutControlGroup6.CustomizationFormText = "Address";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAddressLine1,
            this.ItemForAddressLine2,
            this.ItemForAddressLine3,
            this.ItemForAddressLine4,
            this.ItemForAddressLine5,
            this.ItemForPostcode});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(646, 226);
            this.layoutControlGroup6.Text = "Address";
            // 
            // ItemForAddressLine1
            // 
            this.ItemForAddressLine1.Control = this.AddressLine1TextEdit;
            this.ItemForAddressLine1.CustomizationFormText = "Address Line 1:";
            this.ItemForAddressLine1.Location = new System.Drawing.Point(0, 0);
            this.ItemForAddressLine1.Name = "ItemForAddressLine1";
            this.ItemForAddressLine1.Size = new System.Drawing.Size(646, 24);
            this.ItemForAddressLine1.Text = "Address Line 1:";
            this.ItemForAddressLine1.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForAddressLine2
            // 
            this.ItemForAddressLine2.Control = this.AddressLine2TextEdit;
            this.ItemForAddressLine2.CustomizationFormText = "Address Line 2:";
            this.ItemForAddressLine2.Location = new System.Drawing.Point(0, 24);
            this.ItemForAddressLine2.Name = "ItemForAddressLine2";
            this.ItemForAddressLine2.Size = new System.Drawing.Size(646, 24);
            this.ItemForAddressLine2.Text = "Address Line 2:";
            this.ItemForAddressLine2.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForAddressLine3
            // 
            this.ItemForAddressLine3.Control = this.AddressLine3TextEdit;
            this.ItemForAddressLine3.CustomizationFormText = "Address Line 3:";
            this.ItemForAddressLine3.Location = new System.Drawing.Point(0, 48);
            this.ItemForAddressLine3.Name = "ItemForAddressLine3";
            this.ItemForAddressLine3.Size = new System.Drawing.Size(646, 24);
            this.ItemForAddressLine3.Text = "Address Line 3:";
            this.ItemForAddressLine3.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForAddressLine4
            // 
            this.ItemForAddressLine4.Control = this.AddressLine4TextEdit;
            this.ItemForAddressLine4.CustomizationFormText = "Address Line 4:";
            this.ItemForAddressLine4.Location = new System.Drawing.Point(0, 72);
            this.ItemForAddressLine4.Name = "ItemForAddressLine4";
            this.ItemForAddressLine4.Size = new System.Drawing.Size(646, 24);
            this.ItemForAddressLine4.Text = "Address Line 4:";
            this.ItemForAddressLine4.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForAddressLine5
            // 
            this.ItemForAddressLine5.Control = this.AddressLine5TextEdit;
            this.ItemForAddressLine5.CustomizationFormText = "Address Line 5:";
            this.ItemForAddressLine5.Location = new System.Drawing.Point(0, 96);
            this.ItemForAddressLine5.Name = "ItemForAddressLine5";
            this.ItemForAddressLine5.Size = new System.Drawing.Size(646, 24);
            this.ItemForAddressLine5.Text = "Address Line 5:";
            this.ItemForAddressLine5.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForPostcode
            // 
            this.ItemForPostcode.Control = this.PostcodeTextEdit;
            this.ItemForPostcode.CustomizationFormText = "Postcode:";
            this.ItemForPostcode.Location = new System.Drawing.Point(0, 120);
            this.ItemForPostcode.Name = "ItemForPostcode";
            this.ItemForPostcode.Size = new System.Drawing.Size(646, 106);
            this.ItemForPostcode.Text = "Postcode:";
            this.ItemForPostcode.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(646, 226);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(646, 226);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForName
            // 
            this.ItemForName.AllowHide = false;
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.CustomizationFormText = "Depot Name:";
            this.ItemForName.Location = new System.Drawing.Point(0, 24);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(694, 24);
            this.ItemForName.Text = "Depot Name:";
            this.ItemForName.TextSize = new System.Drawing.Size(143, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 684);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 86);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 86);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(694, 86);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(147, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(147, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(147, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(147, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(694, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLatitude
            // 
            this.ItemForLatitude.Control = this.LatitudeTextEdit;
            this.ItemForLatitude.CustomizationFormText = "Latitude:";
            this.ItemForLatitude.Location = new System.Drawing.Point(0, 636);
            this.ItemForLatitude.Name = "ItemForLatitude";
            this.ItemForLatitude.Size = new System.Drawing.Size(694, 24);
            this.ItemForLatitude.Text = "Latitude:";
            this.ItemForLatitude.TextSize = new System.Drawing.Size(143, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 626);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(694, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLongitude
            // 
            this.ItemForLongitude.Control = this.LongitudeTextEdit;
            this.ItemForLongitude.CustomizationFormText = "Longitude:";
            this.ItemForLongitude.Location = new System.Drawing.Point(0, 660);
            this.ItemForLongitude.Name = "ItemForLongitude";
            this.ItemForLongitude.Size = new System.Drawing.Size(694, 24);
            this.ItemForLongitude.Text = "Longitude:";
            this.ItemForLongitude.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Costs";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartingGritAmount,
            this.emptySpaceItem8,
            this.ItemForTransferredInGritAmount,
            this.ItemForOrderedInGritAmount,
            this.ItemForTransferredOutGritAmount,
            this.ItemForCurrentGritLevel,
            this.ItemForWarningGritLevel,
            this.ItemForUrgentGritLevel});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 82);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(694, 214);
            this.layoutControlGroup8.Text = "Salt Levels";
            // 
            // ItemForCurrentGritLevel
            // 
            this.ItemForCurrentGritLevel.Control = this.CurrentGritLevelSpinEdit;
            this.ItemForCurrentGritLevel.CustomizationFormText = "Supplier Cost:";
            this.ItemForCurrentGritLevel.Location = new System.Drawing.Point(0, 96);
            this.ItemForCurrentGritLevel.Name = "ItemForCurrentGritLevel";
            this.ItemForCurrentGritLevel.Size = new System.Drawing.Size(335, 24);
            this.ItemForCurrentGritLevel.Text = "Current Level:";
            this.ItemForCurrentGritLevel.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForWarningGritLevel
            // 
            this.ItemForWarningGritLevel.Control = this.WarningGritLevelSpinEdit;
            this.ItemForWarningGritLevel.CustomizationFormText = "Units:";
            this.ItemForWarningGritLevel.Location = new System.Drawing.Point(0, 120);
            this.ItemForWarningGritLevel.Name = "ItemForWarningGritLevel";
            this.ItemForWarningGritLevel.Size = new System.Drawing.Size(335, 24);
            this.ItemForWarningGritLevel.Text = "Warning Level:";
            this.ItemForWarningGritLevel.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForUrgentGritLevel
            // 
            this.ItemForUrgentGritLevel.Control = this.UrgentGritLevelSpinEdit;
            this.ItemForUrgentGritLevel.CustomizationFormText = "Converted Unit Cost:";
            this.ItemForUrgentGritLevel.Location = new System.Drawing.Point(0, 144);
            this.ItemForUrgentGritLevel.Name = "ItemForUrgentGritLevel";
            this.ItemForUrgentGritLevel.Size = new System.Drawing.Size(335, 24);
            this.ItemForUrgentGritLevel.Text = "Urgent Level:";
            this.ItemForUrgentGritLevel.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForStartingGritAmount
            // 
            this.ItemForStartingGritAmount.Control = this.StartingGritAmountSpinEdit;
            this.ItemForStartingGritAmount.CustomizationFormText = "Starting Salt Amount:";
            this.ItemForStartingGritAmount.Location = new System.Drawing.Point(0, 0);
            this.ItemForStartingGritAmount.Name = "ItemForStartingGritAmount";
            this.ItemForStartingGritAmount.Size = new System.Drawing.Size(335, 24);
            this.ItemForStartingGritAmount.Text = "Starting Salt Amount:";
            this.ItemForStartingGritAmount.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForTransferredInGritAmount
            // 
            this.ItemForTransferredInGritAmount.Control = this.TransferredInGritAmountSpinEdit;
            this.ItemForTransferredInGritAmount.CustomizationFormText = "Transferred In Salt Amount:";
            this.ItemForTransferredInGritAmount.Location = new System.Drawing.Point(0, 24);
            this.ItemForTransferredInGritAmount.Name = "ItemForTransferredInGritAmount";
            this.ItemForTransferredInGritAmount.Size = new System.Drawing.Size(335, 24);
            this.ItemForTransferredInGritAmount.Text = "Transferred In Salt Amount:";
            this.ItemForTransferredInGritAmount.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForOrderedInGritAmount
            // 
            this.ItemForOrderedInGritAmount.Control = this.OrderedInGritAmountSpinEdit;
            this.ItemForOrderedInGritAmount.CustomizationFormText = "Ordered In Salt Amount:";
            this.ItemForOrderedInGritAmount.Location = new System.Drawing.Point(0, 48);
            this.ItemForOrderedInGritAmount.Name = "ItemForOrderedInGritAmount";
            this.ItemForOrderedInGritAmount.Size = new System.Drawing.Size(335, 24);
            this.ItemForOrderedInGritAmount.Text = "Ordered In Salt Amount:";
            this.ItemForOrderedInGritAmount.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForTransferredOutGritAmount
            // 
            this.ItemForTransferredOutGritAmount.Control = this.TransferredOutGritAmountSpinEdit;
            this.ItemForTransferredOutGritAmount.CustomizationFormText = "Transferred Out Salt Amount:";
            this.ItemForTransferredOutGritAmount.Location = new System.Drawing.Point(0, 72);
            this.ItemForTransferredOutGritAmount.Name = "ItemForTransferredOutGritAmount";
            this.ItemForTransferredOutGritAmount.Size = new System.Drawing.Size(335, 24);
            this.ItemForTransferredOutGritAmount.Text = "Transferred Out Salt Amount:";
            this.ItemForTransferredOutGritAmount.TextSize = new System.Drawing.Size(143, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 296);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(694, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCurrentCostConverted
            // 
            this.ItemForCurrentCostConverted.Control = this.CurrentCostConvertedTextEdit;
            this.ItemForCurrentCostConverted.CustomizationFormText = "Current Cost:";
            this.ItemForCurrentCostConverted.Location = new System.Drawing.Point(0, 48);
            this.ItemForCurrentCostConverted.Name = "ItemForCurrentCostConverted";
            this.ItemForCurrentCostConverted.Size = new System.Drawing.Size(694, 24);
            this.ItemForCurrentCostConverted.Text = "Current Cost:";
            this.ItemForCurrentCostConverted.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForWarningLabel
            // 
            this.ItemForWarningLabel.Control = this.WarningLabel;
            this.ItemForWarningLabel.CustomizationFormText = "Salt Warning Label:";
            this.ItemForWarningLabel.Location = new System.Drawing.Point(423, 0);
            this.ItemForWarningLabel.MaxSize = new System.Drawing.Size(271, 20);
            this.ItemForWarningLabel.MinSize = new System.Drawing.Size(271, 17);
            this.ItemForWarningLabel.Name = "ItemForWarningLabel";
            this.ItemForWarningLabel.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.ItemForWarningLabel.Size = new System.Drawing.Size(271, 24);
            this.ItemForWarningLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWarningLabel.Text = "Salt Warning Label:";
            this.ItemForWarningLabel.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForWarningLabel.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(373, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(50, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp04212_GC_Salt_Depot_EditTableAdapter
            // 
            this.sp04212_GC_Salt_Depot_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(335, 0);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(335, 168);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_GC_Salt_Depot_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(731, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Salt_Depot_Edit";
            this.Text = "Edit Salt Depot";
            this.Activated += new System.EventHandler(this.frm_GC_Salt_Depot_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Salt_Depot_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Salt_Depot_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransferredOutGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04212GCSaltDepotEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderedInGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredInGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartingGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailPasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UrgentGritLevelSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WarningGritLevelSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentGritLevelSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNamePositionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSiteTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaxTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritDepotIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentCostConvertedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritDepotID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactNamePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentGritLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningGritLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUrgentGritLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartingGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredInGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderedInGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredOutGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentCostConverted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit GritDepotIDTextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine1TextEdit;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritDepotID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DevExpress.XtraEditors.TextEdit AddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine2TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit PostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit TelephoneTextEdit;
        private DevExpress.XtraEditors.TextEdit MobileTextEdit;
        private DevExpress.XtraEditors.TextEdit TextTextEdit;
        private DevExpress.XtraEditors.TextEdit FaxTextEdit;
        private DevExpress.XtraEditors.TextEdit WebSiteTextEdit;
        private DevExpress.XtraEditors.TextEdit EmailTextEdit;
        private DevExpress.XtraEditors.TextEdit ContactNameTextEdit;
        private DevExpress.XtraEditors.TextEdit LongitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit LatitudeTextEdit;
        private DevExpress.XtraEditors.TextEdit ContactNamePositionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLatitude;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLongitude;
        private DevExpress.XtraEditors.SpinEdit CurrentGritLevelSpinEdit;
        private DevExpress.XtraEditors.SpinEdit WarningGritLevelSpinEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTelephone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMobile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFax;
        private DevExpress.XtraLayout.LayoutControlItem ItemForText;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWebSite;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactNamePosition;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostcode;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.SpinEdit UrgentGritLevelSpinEdit;
        private DevExpress.XtraEditors.TextEdit EmailPasswordTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailPassword;
        private System.Windows.Forms.BindingSource sp04212GCSaltDepotEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04212_GC_Salt_Depot_EditTableAdapter sp04212_GC_Salt_Depot_EditTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCurrentGritLevel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarningGritLevel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUrgentGritLevel;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.SpinEdit CurrentCostConvertedTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCurrentCostConverted;
        private DevExpress.XtraEditors.LabelControl WarningLabel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarningLabel;
        private DevExpress.XtraEditors.SpinEdit TransferredOutGritAmountSpinEdit;
        private DevExpress.XtraEditors.SpinEdit OrderedInGritAmountSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TransferredInGritAmountSpinEdit;
        private DevExpress.XtraEditors.SpinEdit StartingGritAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartingGritAmount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferredInGritAmount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOrderedInGritAmount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferredOutGritAmount;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;  // Required for uploading file to optimiser //
using System.Collections.Specialized;  // Required for uploading file to optimiser //
using System.IO;  // Required for uploading file to optimiser //
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Security.Cryptography;  // Required for MD5 encryption of password //
using System.Net.Http;  // Required for File Upload to Web Service //

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_Gritting_Callouts_Send_To_Optimizer : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public string _CalloutIDs = "";
        public bool _boolLeakData = false;

        #endregion

        public frm_GC_Gritting_Callouts_Send_To_Optimizer()
        {
            InitializeComponent();
        }

        private void frm_GC_Gritting_Callouts_Send_To_Optimizer_Load(object sender, EventArgs e)
        {
            this.FormID = 400151;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            LoadData();

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (splashScreenManager != null)
            {
                splashScreenManager.CloseWaitForm();
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {

        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            try
            {

                sp04355_GC_Export_Gritting_Callouts_For_Route_OptimisationTableAdapter.Connection.ConnectionString = strConnectionString;
                sp04355_GC_Export_Gritting_Callouts_For_Route_OptimisationTableAdapter.Fill(dataSet_GC_Core.sp04355_GC_Export_Gritting_Callouts_For_Route_Optimisation, _CalloutIDs, (_boolLeakData ? 1 : 0));
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while preparing the data for sending to the route optimizer... Error [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Callout Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Callouts available");
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        #endregion


        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to send Callouts for Route Optimization.\n\nAre you sure you wish to proceed?", "Send Data to Route Optimizer", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            #region GetConfigurationSettings

            string strGrittingRouteOptimisationURL = "";
            string strGrittingRouteOptimisationUsername = "";
            string strGrittingRouteOptimisationPassword = "";
            string strGrittingRouteOptimisationJobFileSavePath = "";
            string strGrittingRouteOptimisationIniFileSavePath = "";
            string strGrittingRouteOptimisationOptimisedRouteSavePath = "";
            string strGrittingRouteOptimisationProject = "";
            string strGrittingRouteOptimisationProfile = "";
            try
            {
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp04356_GC_Route_optimization_Get_Settings", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
                DataSet dsSettings = new DataSet("NewDataSet");
                sdaSettings.Fill(dsSettings, "Table");
                conn.Close();
                conn.Dispose();
                if (dsSettings.Tables[0].Rows.Count != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Route Optimization Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Route Optimization Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                DataRow dr = dsSettings.Tables[0].Rows[0];
                strGrittingRouteOptimisationURL = dr["GrittingRouteOptimisationURL"].ToString();
                strGrittingRouteOptimisationUsername = dr["GrittingRouteOptimisationUsername"].ToString();
                strGrittingRouteOptimisationPassword = dr["GrittingRouteOptimisationPassword"].ToString();
                strGrittingRouteOptimisationJobFileSavePath = dr["GrittingRouteOptimisationJobFileSavePath"].ToString();
                strGrittingRouteOptimisationIniFileSavePath = dr["GrittingRouteOptimisationIniFileSavePath"].ToString();
                strGrittingRouteOptimisationOptimisedRouteSavePath = dr["GrittingRouteOptimisationOptimisedRouteSavePath"].ToString();
                strGrittingRouteOptimisationProject = dr["GrittingRouteOptimisationProject"].ToString();
                strGrittingRouteOptimisationProfile = dr["GrittingRouteOptimisationProfile"].ToString();
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Route Optimization Settings' (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Route Optimization Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            #endregion

            bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
            if (!boolNoInternet) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send jobs for route optimization.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Encrypt password with MD5 as web service requires this //
            string strHashedPassword = "";
            try
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    strHashedPassword = GetMd5Hash(md5Hash, strGrittingRouteOptimisationPassword);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("An error occurred while attempting to encrypt the password for the Route Optimizer web servicer - [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support!", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // Get a token from the web site //
            string strMessage = "";
            string strToken = "";
            string strServer = "";
            try
            {
                // Create a 'WebRequest' object with the specified url //
                string strURL = strGrittingRouteOptimisationURL + "/api/getToken?username=" + strGrittingRouteOptimisationUsername + "&password=" + strHashedPassword + "&project=" + strGrittingRouteOptimisationProject + "&profile=" + strGrittingRouteOptimisationProfile;
                System.Net.WebRequest myWebRequest = System.Net.WebRequest.Create(strURL);


                System.Net.WebResponse myWebResponse = myWebRequest.GetResponse();  // Send the 'WebRequest' and wait for response //
                Stream ReceiveStream = myWebResponse.GetResponseStream();  // Obtain a 'Stream' object associated with the response object //

                XmlDocument xDoc = new XmlDocument(); 
                xDoc.Load(ReceiveStream);

                foreach (XmlNode node in xDoc.DocumentElement.ChildNodes)  // cycle through each child node // 
                {
                    switch (node.Name.ToLower())
                        {
                        case "message":
                            {
                                strMessage = node.InnerText;
                            }
                            break;
                        case "token":
                            {
                                strToken = node.InnerText;
                            }
                            break;
                        case "server":
                            {
                                strServer = node.InnerText;
                            }
                            break;
                        default:
                            break;
                    }
                }

                myWebResponse.Close();  // Release the resources of response object //
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("An error occurred while attempting to get a token from the Route Optimizer web servicer - [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support!", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (string.IsNullOrWhiteSpace(strToken))
            {
                XtraMessageBox.Show("Unable to get token from the Route Optimizer web servicer - Server Message: " + strMessage + ".\n\nPlease try again. If the problem persists contact Technical Support!", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (string.IsNullOrWhiteSpace(strServer))
            {
                XtraMessageBox.Show("URL not returned from the Route Optimizer web servicer - Server Message: " + strMessage + ".\n\nPlease try again. If the problem persists contact Technical Support!", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // Create Ini File //
            string strReturn = CreateConfigFile(strGrittingRouteOptimisationIniFileSavePath);
            if (strReturn != "Success")
            {
                XtraMessageBox.Show("An error occurred while attempting to create the ini file for the Route Optimizer - [" + strReturn + "].\n\nPlease try again. If the problem persists contact Technical Support!", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            string FileName1 = "";
            FileName1 = Path.Combine(strGrittingRouteOptimisationJobFileSavePath, "Config.roxml");

            string FileName2 = "";
            try
            {
                FileName2 = Path.Combine(strGrittingRouteOptimisationJobFileSavePath, "JobsToOptimize.csv");
                gridControl1.ExportToCsv(FileName2);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("An error occurred while attempting to create the export file for the Route Optimizer - [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support!", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // Check URL from web service is valid - MB: 28/02/2017 //
            Uri uriResult;
            bool result = Uri.TryCreate(strServer, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
            if (!result)
            {
                if (!strServer.StartsWith("http")) strServer = "http://" + strServer; // handles http and https on the check //
            }
            strGrittingRouteOptimisationURL = strServer; // Substitute our store URL for the one returned back by the Get Token call //

            try
            {
                // Send Files to API //
                string strURL = strGrittingRouteOptimisationURL + "/api/importData?token=" + strToken;

                MemoryStream memStream1 = new MemoryStream();
                using (FileStream fileStream = File.OpenRead(FileName1))
                {
                    memStream1.SetLength(fileStream.Length);
                    fileStream.Read(memStream1.GetBuffer(), 0, (int)fileStream.Length);
                }   
        
                MemoryStream memStream2 = new MemoryStream();
                using (FileStream fileStream = File.OpenRead(FileName2))
                {
                    memStream2.SetLength(fileStream.Length);
                    fileStream.Read(memStream2.GetBuffer(), 0, (int)fileStream.Length);
                }
                Stream streamReturn = Upload(strURL, memStream1, memStream2);  // Send Files to API Now //


                // Get Response from API //
                strURL = strGrittingRouteOptimisationURL + "/api/importData/getResult?token=" + strToken;
                System.Net.WebRequest myWebRequest = System.Net.WebRequest.Create(strURL);

                System.Net.WebResponse myWebResponse = myWebRequest.GetResponse();  // Send the 'WebRequest' and wait for response //
                Stream ReceiveStream = myWebResponse.GetResponseStream();  // Obtain a 'Stream' object associated with the response object //

                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(ReceiveStream);

                // Close the Token //
                strURL = strGrittingRouteOptimisationURL + "/api/closeToken?token=" + strToken;
                myWebRequest = System.Net.WebRequest.Create(strURL);
                myWebRequest.GetResponse();  // Send the 'WebRequest' and wait for response //


                DevExpress.XtraEditors.XtraMessageBox.Show("Data Sent to Route Optimizer Successfully.\n\nA copy of the send file is stored in:\n" + FileName1 + "\n" + FileName2, "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex) 
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to send data to Route Optimizer - Error: " + ex.Message + "\n\nPlease try again - if the problem persists please contact Technical Support.", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }

/*
            // Send the Ini file to Route Optimizer Software //
            //string URL = strGrittingRouteOptimisationURL; //uri  to logixdirect server api

            string paramName = "file1";   //name of form file field - this can be anything
            string contentType = "text/xml"; //content type of file
            NameValueCollection type = new NameValueCollection(); //name value pair method, only need username here
            type.Add("username", strGrittingRouteOptimisationUsername);
            
            Boolean bProceed = false;
            if (HttpUploadFileCall(URL, FileName1, paramName, contentType, type))
            {
                bProceed = true;
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to send data to Route Optimizer.\n\nPlease try again - if the problem persists please contact Technical Support.", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                bProceed = false;
            }
            if (!bProceed) return;

            // Send the Data file to Route Optimizer Software //
            paramName = "file2";   //name of form file field - this can be anything
            contentType = "text/csv"; //content type of file
            if (HttpUploadFileCall(URL, FileName2, paramName, contentType, type))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Data Sent to Route Optimizer Successfully.\n\nA copy of the send file is stored in:\n" + FileName1 + "\n" + FileName2, "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to send data to Route Optimizer.\n\nPlease try again - if the problem persists please contact Technical Support.", "Send Data to Route Optimizer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
 */
        }

        public Boolean HttpUploadFileCall(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            //Debugger.Log(1, "output", string.Format("Uploading {0} to {1}", file, url));

            //create boundary ref
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            //start web request, add parameters
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

            Stream rs = wr.GetRequestStream();

            //start form data construction
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";

            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);

            //start file for upload construction
            //this area needs to be looped for multiple files---------------------------------------------------------------------

            FileInfo fileInfo = new FileInfo(file);

            string justFile = fileInfo.Name.ToString();

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, justFile, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            //END this area needs to be looped for multiple files---------------------------------------------------------------------

            WebResponse wresp = null;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                System.Diagnostics.Debug.Write(string.Format("File uploaded, server response is: {0}", reader2.ReadToEnd()));

                return true;
            }
            catch (Exception ex)
            {
                //Debugger.Log(1, "debug", "Error uploading file" + ex.ToString());
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                XtraMessageBox.Show("An Error Occurred while uploading the file: " + file + " to the Route Optimiser.. [" + ex.Message + "].\n\nTry again - if the problem persists please contact Technical Support.", "Upload Job Files for Optimisation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                wr = null;
            }
        }


        private string CreateConfigFile(string strFilename)
        {
            if (strFilename == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Upload to the Route optimiser - No Config File Location has been specified in the System Settings table.", "Upload Job Files for Optimisation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            strFilename = Path.Combine(strFilename, "Config.roxml");
            XmlTextWriter textWriter;

            try
            {
                textWriter = new XmlTextWriter(strFilename, null);
                textWriter.WriteStartDocument();  // Open the document //
                textWriter.WriteComment("Generated by Ground Control Information Centre");  // Write comments //
                textWriter.WriteStartElement("rocommand"); textWriter.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                {
                    textWriter.WriteStartElement("import");  // Write second element - Not closed so subsequent items are indented inside it //
                    {
                        textWriter.WriteStartElement("import_file", ""); textWriter.WriteString("JobsToOptimize.csv"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("errors_file_name_format", ""); textWriter.WriteString("WLIERROR_&amp;lt;date&amp;gt;_&amp;lt;time&amp;gt;.csv"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("file_type", ""); textWriter.WriteString("7"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("import_mode", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("schedule_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("schedule_mode", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("clear_errors", ""); textWriter.WriteString("false"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("clear_confirmed", ""); textWriter.WriteString("true"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("automatic_zoning", ""); textWriter.WriteString("false"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("overwrite_zones", ""); textWriter.WriteString("true"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("text_separator", ""); textWriter.WriteString(","); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("text_delimiter", ""); textWriter.WriteString("\""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("min_confidence", ""); textWriter.WriteString("8"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("purge_data", ""); textWriter.WriteString("true"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("MaxTripsPerRoute", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("TrailerSwapping", ""); textWriter.WriteString("No"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("ImportJourneys", ""); textWriter.WriteString("false"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("CollectionAtEnd", ""); textWriter.WriteString("No"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("FixedFleet", ""); textWriter.WriteString("true"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("PlanningDay", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("SchedulingPeriod", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("MaxNightsAway", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("RunType", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("RoutePrefix", ""); textWriter.WriteString("Run-"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // import //
                }
                textWriter.WriteEndElement();  // rocommand //
                textWriter.WriteEndDocument();  // Ends the document //
                textWriter.Close();  // Close writer //
            }
            catch (Exception ex)
            {
                return "Error - A problem occurred while creating the ini file [" + ex.Message + "].";
            }
            return "Success";
        }

        private string CreateConfigFile_BACKUP_OF_OLD_VERSION(string strFilename)
        {
            if (strFilename == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Upl to the Route optimiser - No Config File Location has been specified in the System Settings table.", "Upload Job Files for Optimisation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            strFilename = Path.Combine(strFilename, "Config.lxxml");
            XmlTextWriter textWriter;

            try
            {
                textWriter = new XmlTextWriter(strFilename, null);
                textWriter.WriteStartDocument();  // Open the document //
                textWriter.WriteComment("Generated by Ground Control Information Centre");  // Write comments //
                textWriter.WriteStartElement("lxcommand"); textWriter.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                {

                    textWriter.WriteStartElement("version", ""); textWriter.WriteString("1.2"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("command");  // Write second element - Not closed so subsequent items are indented inside it //
                    {
                        textWriter.WriteStartElement("action", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("clear_task_file", ""); textWriter.WriteString("true"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // command //
                    textWriter.WriteStartElement("profile");  // Write second element - Not closed so subsequent items are indented inside it //
                    {
                        textWriter.WriteStartElement("profile_name", ""); textWriter.WriteString("Gritting_07-10-2013"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("project_name", ""); textWriter.WriteString("Route_07-10-2013"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // profile //
                    textWriter.WriteStartElement("import");  // Write second element - Not closed so subsequent items are indented inside it //
                    {
                        textWriter.WriteStartElement("import_file", ""); textWriter.WriteString("JobsToOptimize.csv"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("errors_file_name_format", ""); textWriter.WriteString("WLIERROR_&lt;date&gt;_&lt;time&gt;.CSV"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("file_type", ""); textWriter.WriteString("7"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("text_separator", ""); textWriter.WriteString(","); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("text_delimiter", ""); textWriter.WriteString("'"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("import_mode", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("min_confidence", ""); textWriter.WriteString("8"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("schedule_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("schedule_mode", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("clear_errors", ""); textWriter.WriteString("false"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("clear_confirmed", ""); textWriter.WriteString("true"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("automatic_zoning", ""); textWriter.WriteString("false"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("overwrite_zones", ""); textWriter.WriteString("true"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // import //
                    textWriter.WriteStartElement("logixie");  // Write second element - Not closed so subsequent items are indented inside it //
                    {
                        textWriter.WriteStartElement("auto_schedule");  // Write second element - Not closed so subsequent items are indented inside it //
                        {
                            textWriter.WriteStartElement("start_day"); textWriter.WriteString("1"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("planning_days"); textWriter.WriteString("1"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("max_nights_away"); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("max_trips"); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("collections_after_deliveries"); textWriter.WriteString("false"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("fixed_fleet"); textWriter.WriteString("true"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("route_prefix"); textWriter.WriteString("Run-"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("run_type"); textWriter.WriteString("1"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("delete_existing_routes"); textWriter.WriteString("true"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // auto_schedule //
                        textWriter.WriteStartElement("depots");
                        {

                            // Put in Depots from Stored Proc //
                            try
                            {
                                SqlConnection conn = new SqlConnection(strConnectionString);
                                SqlCommand cmd = null;
                                cmd = new SqlCommand("sp04357_GC_Route_Optimisation_Get_Gritting_Teams", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@CalloutIDs", _CalloutIDs));
                                SqlDataAdapter sdaDepots = new SqlDataAdapter(cmd);
                                DataSet dsDepots = new DataSet("NewDataSet");
                                sdaDepots.Fill(dsDepots, "Table");
                                conn.Close();
                                conn.Dispose();
                                if (dsDepots.Tables[0].Rows.Count <= 0)
                                {
                                    return "Error - No Depots returned for Ini File";
                                }

                                conn = new SqlConnection(strConnectionString);
                                SqlDataAdapter sdaPdaIDs = new SqlDataAdapter();
                                DataSet dsPdaIDs = new DataSet("NewDataSet");
                                cmd = null;
                                cmd = new SqlCommand("sp04361_GC_Route_Optimisation_Get_PdaIDs", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@CalloutIDs", _CalloutIDs));
                                dsPdaIDs.Clear();  // Remove old values first //
                                sdaPdaIDs = new SqlDataAdapter(cmd);
                                sdaPdaIDs.Fill(dsPdaIDs, "Table");

                                foreach (DataRow dr in dsDepots.Tables[0].Rows)
                                {
                                    textWriter.WriteStartElement("depot");
                                    {
                                        textWriter.WriteStartElement("name"); textWriter.WriteString("A" + dr["ContractorID"].ToString()); textWriter.WriteEndElement();
                                        textWriter.WriteStartElement("drivers"); textWriter.WriteString(dr["Drivers"].ToString()); textWriter.WriteEndElement();

                                        DataRow[] drFilteredPDAs = dsPdaIDs.Tables[0].Select("SubContractorID = " + Convert.ToInt32(dr["ContractorID"]));
                                        if (drFilteredPDAs.Length > 0)
                                        {
                                            foreach (DataRow dr2 in drFilteredPDAs)
                                            {
                                                textWriter.WriteStartElement("vehicle");
                                                {
                                                    textWriter.WriteStartElement("name"); textWriter.WriteString("PDA" + dr2["AllocatedPdaID"].ToString()); textWriter.WriteEndElement();
                                                    textWriter.WriteStartElement("quantity"); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                                }
                                                textWriter.WriteEndElement();
                                            }
                                        }
                                    }  // depot //
                                    textWriter.WriteEndElement();
                                }
                            }
                            catch (Exception ex)
                            {
                                textWriter.WriteEndDocument();  // Ends the document //
                                textWriter.Close();  // Close writer //
                                return "Error - Unable to get Depots List for Ini File.";
                            }

                        }
                        textWriter.WriteEndElement();  // Depots //
                        textWriter.WriteStartElement("routes"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();    // logixie //
                    textWriter.WriteStartElement("user_out");
                    {
                        textWriter.WriteStartElement("user_csv_mode"); textWriter.WriteString("4"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("user_csv_ini"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("user_csv_name"); textWriter.WriteString("userout.csv"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("wrap_days"); textWriter.WriteString("false"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("separator"); textWriter.WriteString(","); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("date_format"); textWriter.WriteString("dd/mm/yyyy"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("time_format"); textWriter.WriteString("hh:mm:ss"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("user_csv_section");
                        {
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("ORDER"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("1"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("CUST_NAME"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("2"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("ADDRESS_1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("3"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("ADDRESS_2"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("4"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("ADDRESS_3"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("5"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("ADDRESS_4"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("6"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("ZIPCODE"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("7"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("ROUTENAME"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("8"); textWriter.WriteEndElement();
                            }  // user_csv_field //
                            textWriter.WriteEndElement();
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("SEQUENCE"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("9"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("CALLTYPE"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("10"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("TASK_TYPE"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("11"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("UNIT_1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("12"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("UNIT_2"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("13"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("CUST_INFO1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("14"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("OPEN1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("15"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("CLOSE1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("16"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("PROD_X"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("17"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("ARR_DAY"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("18"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //
                            textWriter.WriteStartElement("user_csv_field");
                            {
                                textWriter.WriteStartElement("name"); textWriter.WriteString("ARR_TIME"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("col"); textWriter.WriteString("19"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // user_csv_field //

                        }
                        textWriter.WriteEndElement();  // user_csv_section //
                    }
                    textWriter.WriteEndElement();  // user_out //
                }
                textWriter.WriteEndElement();  // lxcommand //
                textWriter.WriteEndDocument();  // Ends the document //
                textWriter.Close();  // Close writer //
            }
            catch (Exception ex)
            {
                return "Error - A problem occurred while creating the ini file [" + ex.Message + "].";
            }
            return "Success";
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash //
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes and create a string //
            StringBuilder sBuilder = new StringBuilder();  

            // Loop through each byte of the hashed data and format each one as a hexadecimal string //
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();  // Return the hexadecimal string //
        }

        private Stream Upload(string actionUrl, Stream paramFileStream1, Stream paramFileStream2)
        {
            // Upload the files to the API //
            HttpContent fileStreamContent1 = new StreamContent(paramFileStream1);
            HttpContent fileStreamContent2 = new StreamContent(paramFileStream2);
            using (var client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                formData.Add(fileStreamContent1, "file1", "Config.roxml");
                formData.Add(fileStreamContent2, "file2", "JobsToOptimize.csv");
                var response = client.PostAsync(actionUrl, formData).Result;
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                return response.Content.ReadAsStreamAsync().Result;
            }
        }
        //private Stream Upload(string actionUrl, string paramString, Stream paramFileStream, byte[] paramFileBytes)
        //{
        //    HttpContent stringContent = new StringContent(paramString);
        //    HttpContent fileStreamContent = new StreamContent(paramFileStream);
        //    HttpContent bytesContent = new ByteArrayContent(paramFileBytes);
        //    using (var client = new HttpClient())
        //    using (var formData = new MultipartFormDataContent())
        //    {
        //        formData.Add(stringContent, "param1", "param1");
        //        formData.Add(fileStreamContent, "file1", "file1");
        //        formData.Add(bytesContent, "file2", "file2");
        //        var response = client.PostAsync(actionUrl, formData).Result;
        //        if (!response.IsSuccessStatusCode)
        //        {
        //            return null;
        //        }
        //        return response.Content.ReadAsStreamAsync().Result;
        //    }
        //}



    }
}

namespace WoodPlan5
{
    partial class frm_GC_Callout_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Callout_Manager));
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip23 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem23 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip24 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem24 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem23 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip25 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem25 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem24 = new DevExpress.Utils.ToolTipItem();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemHyperLinkEditLinkedFile = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04074GCGrittingCalloutManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGrittingCallOutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGrittingContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorIsDirectLabour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContarctorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImportedWeatherForecastID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextSentTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorReceivedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorRespondedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorETA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitAborted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbortedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishedLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishedLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaltVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoursWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamHourlyRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamCharge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractorReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSourceLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritSourceLocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteWeather = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTemperature = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamPresent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceFailure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritJobTransferMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarkup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClientReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritMobileTelephoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSignatureFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPPEWorn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccessAbortedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMembersPresent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowInfo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEveningRateModifier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedOnPDA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSheetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceTeamPaymentExported = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrioritySite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowOnSiteOver5cm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDACode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowCleared = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearedSatisfactory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAttendanceOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletionEmailSent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsFloatingSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoubleGrit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKMLFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKMLImageFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletionSheetFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlCompanies = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCompanyFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04237GCCompanyFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.popupContainerControlLinkedGrittingCalloutsFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnGritCalloutFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04001GCJobCallOutStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp04075GCGrittingCalloutLinkedExtraCostsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colExtraCostID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingCallOutID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedFromDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCostTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGrittingContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colChargedPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberOfUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowOnSite1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingTimeToDeduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp04337GCServiceAreasLinkedToGrittingCalloutBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGrittingServicedSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingCalloutID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteServiceAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWasServiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCreatedFromDefault1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPictureID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingCallOutID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPicturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDateTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteGrittingContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamAddressLine11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl15 = new DevExpress.XtraGrid.GridControl();
            this.sp05089CRMContactsLinkedToRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCRMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDueDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContactMadeDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.popupContainerEditCompanies = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnFormatData = new DevExpress.XtraEditors.SimpleButton();
            this.VisitIDsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.checkEditColourCode = new DevExpress.XtraEditors.CheckEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroupFilterType = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupFilterCriteria = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupSpecificVisitIDs = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForVisitIDs = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemforFormatDataBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter();
            this.sp04074_GC_Gritting_Callout_ManagerTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04074_GC_Gritting_Callout_ManagerTableAdapter();
            this.sp04075_GC_Gritting_Callout_Linked_Extra_CostsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04075_GC_Gritting_Callout_Linked_Extra_CostsTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiAddJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOptimization = new DevExpress.XtraBars.BarButtonItem();
            this.pmOptimisation = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bsiRouteOptimiseSites = new DevExpress.XtraBars.BarSubItem();
            this.bbiGetDataToRouteOptimize = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendDataToRouteOptimizer = new DevExpress.XtraBars.BarButtonItem();
            this.bsiRouteOptimiseLeaks = new DevExpress.XtraBars.BarSubItem();
            this.bbiGetDataToRouteOptimizeLeaks = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendDataToRouteOptimizerLeaks = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGetDataFromRouteOptimizer = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSend = new DevExpress.XtraBars.BarSubItem();
            this.bbiSetReadyToSend1 = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectSetReadyToSend = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetReadyToSend2 = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSendJobs = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectJobsReadyToSend = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendNoProactiveMessages = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReassignJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewSMSErrors = new DevExpress.XtraBars.BarButtonItem();
            this.bsiManuallyComplete = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectJobsToComplete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiManuallyCompleteJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bsiCompletionSheet = new DevExpress.XtraBars.BarSubItem();
            this.bsiKMLMapImage = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectJobsForKMLCreation = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateKMLAndMapImages = new DevExpress.XtraBars.BarButtonItem();
            this.bsiCompletionSheetCreate = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectCompletionSheetJobs = new DevExpress.XtraBars.BarButtonItem();
            this.beiShowMapsOnCompletionSheets = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditShowMapsOnCompletionSheets = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiPreviewCompletionSheets = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCompletionSheet = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAuthorise = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPay = new DevExpress.XtraBars.BarButtonItem();
            this.beiLoadingProgress = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemMarqueeProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.bbiLoadingCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bsiViewOnMap = new DevExpress.XtraBars.BarSubItem();
            this.bbiViewSiteOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewJobOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterVisitsSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount = new DevExpress.XtraBars.BarStaticItem();
            this.bbiManuallyComplete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendJobs2 = new DevExpress.XtraBars.BarButtonItem();
            this.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp04237_GC_Company_Filter_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter();
            this.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter();
            this.sp04337_GC_Service_Areas_Linked_To_Gritting_CalloutTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04337_GC_Service_Areas_Linked_To_Gritting_CalloutTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending15 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04074GCGrittingCalloutManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).BeginInit();
            this.popupContainerControlCompanies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).BeginInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04075GCGrittingCalloutLinkedExtraCostsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours2DP)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04337GCServiceAreasLinkedToGrittingCalloutBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05089CRMContactsLinkedToRecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCompanies.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditColourCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilterType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFilterCriteria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSpecificVisitIDs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitIDs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforFormatDataBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmOptimisation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowMapsOnCompletionSheets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1560, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(1560, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1560, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSendJobs2,
            this.bbiReassignJobs,
            this.bbiViewSiteOnMap,
            this.bbiAuthorise,
            this.bbiPay,
            this.bsiSendJobs,
            this.bbiSelectJobsReadyToSend,
            this.bbiSendJobs,
            this.bsiViewOnMap,
            this.bbiViewJobOnMap,
            this.bbiViewSMSErrors,
            this.bbiSelectSetReadyToSend,
            this.bbiSetReadyToSend2,
            this.bbiAddJobs,
            this.bbiManuallyComplete,
            this.bsiManuallyComplete,
            this.bbiSelectJobsToComplete,
            this.bbiManuallyCompleteJobs,
            this.bbiSendNoProactiveMessages,
            this.bbiOptimization,
            this.bbiGetDataToRouteOptimize,
            this.bbiSendDataToRouteOptimizer,
            this.bbiGetDataFromRouteOptimizer,
            this.bsiRouteOptimiseSites,
            this.bsiRouteOptimiseLeaks,
            this.bbiGetDataToRouteOptimizeLeaks,
            this.bbiSendDataToRouteOptimizerLeaks,
            this.bsiSend,
            this.bbiCompletionSheet,
            this.bbiSelectCompletionSheetJobs,
            this.bbiSetReadyToSend1,
            this.bbiRefresh,
            this.bsiSelectedCount,
            this.bciFilterVisitsSelected,
            this.bsiKMLMapImage,
            this.bsiCompletionSheetCreate,
            this.bbiSelectJobsForKMLCreation,
            this.bbiCreateKMLAndMapImages,
            this.beiShowMapsOnCompletionSheets,
            this.bbiPreviewCompletionSheets,
            this.bbiLoadingCancel,
            this.beiLoadingProgress,
            this.bsiCompletionSheet});
            this.barManager1.MaxItemId = 81;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit5,
            this.repositoryItemCheckEditShowMapsOnCompletionSheets,
            this.repositoryItemTextEdit6,
            this.repositoryItemMarqueeProgressBar1});
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // repositoryItemTextEditTime
            // 
            this.repositoryItemTextEditTime.AutoHeight = false;
            this.repositoryItemTextEditTime.Mask.EditMask = "t";
            this.repositoryItemTextEditTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditTime.Name = "repositoryItemTextEditTime";
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "f";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ReadOnly = true;
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // repositoryItemHyperLinkEditLinkedFile
            // 
            this.repositoryItemHyperLinkEditLinkedFile.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedFile.Name = "repositoryItemHyperLinkEditLinkedFile";
            this.repositoryItemHyperLinkEditLinkedFile.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedFile.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedFile_OpenLink);
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp04074GCGrittingCalloutManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Reload Selected Records Only", "reload_selected")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditTime,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemPictureEdit1,
            this.repositoryItemHyperLinkEditLinkedFile});
            this.gridControl1.Size = new System.Drawing.Size(1533, 340);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04074GCGrittingCalloutManagerBindingSource
            // 
            this.sp04074GCGrittingCalloutManagerBindingSource.DataMember = "sp04074_GC_Gritting_Callout_Manager";
            this.sp04074GCGrittingCalloutManagerBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Scheduler_16x16.png");
            this.imageCollection1.InsertGalleryImage("convert_16x16.png", "images/actions/convert_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/convert_16x16.png"), 7);
            this.imageCollection1.Images.SetKeyName(7, "convert_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGrittingCallOutID,
            this.colSiteGrittingContractID,
            this.colSiteID,
            this.colSiteName,
            this.colClientID,
            this.colClientName,
            this.colCompanyID,
            this.colCompanyName,
            this.colSiteXCoordinate,
            this.colSiteYCoordinate,
            this.colSiteTelephone,
            this.colSiteEmail,
            this.colClientsSiteCode,
            this.colSubContractorID,
            this.colSubContractorName,
            this.colSubContractorIsDirectLabour,
            this.colOriginalSubContractorID,
            this.colOriginalSubContarctorName,
            this.colReactive,
            this.colJobStatusID,
            this.colCalloutStatusDescription,
            this.colCalloutStatusOrder,
            this.colCallOutDateTime,
            this.colImportedWeatherForecastID,
            this.colTextSentTime,
            this.colSubContractorReceivedTime,
            this.colSubContractorRespondedTime,
            this.colSubContractorETA,
            this.colCompletedTime,
            this.colAuthorisedByStaffID,
            this.colAuthorisedByName,
            this.colVisitAborted,
            this.colAbortedReason,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishedLatitude,
            this.colFinishedLongitude,
            this.colClientPONumber,
            this.colSaltUsed,
            this.colSaltCost,
            this.colSaltSell,
            this.colSaltVatRate,
            this.colHoursWorked,
            this.colTeamHourlyRate,
            this.colTeamCharge,
            this.colLabourCost,
            this.colLabourVatRate,
            this.colOtherCost,
            this.colOtherSell,
            this.colTotalCost,
            this.colTotalSell,
            this.colClientInvoiceNumber,
            this.colSubContractorPaid,
            this.colGrittingInvoiceID,
            this.colRecordedByStaffID,
            this.colRecordedByName,
            this.colPaidByStaffID,
            this.colPaidByName,
            this.colDoNotPaySubContractor,
            this.colDoNotPaySubContractorReason,
            this.colGritSourceLocationID,
            this.colGritSourceLocationTypeID,
            this.colNoAccess,
            this.colSiteWeather,
            this.colSiteTemperature,
            this.colPdaID,
            this.colTeamPresent,
            this.colServiceFailure,
            this.colComments,
            this.colClientPrice,
            this.colGritJobTransferMethod,
            this.colSnowOnSite,
            this.colStartTime,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.colProfit,
            this.colMarkup,
            this.colClientPOID,
            this.colNonStandardCost,
            this.colNonStandardSell,
            this.colDoNotInvoiceClient,
            this.colDoNotInvoiceClientReason,
            this.colGritMobileTelephoneNumber,
            this.colClientSignatureFileName,
            this.colPPEWorn,
            this.colNoAccessAbortedRate,
            this.gridColumn4,
            this.colTeamMembersPresent,
            this.colSnowInfo,
            this.colEveningRateModifier,
            this.colCompletedOnPDA,
            this.colJobSheetNumber,
            this.colFinanceInvoiceNumber,
            this.colFinanceTeamPaymentExported,
            this.colCalloutInvoiced,
            this.colSiteManagerName,
            this.colPrioritySite,
            this.colSnowOnSiteOver5cm,
            this.colInternalRemarks,
            this.colPDACode,
            this.colSnowCleared,
            this.colSnowClearedSatisfactory,
            this.colAttendanceOrder,
            this.colCompletionEmailSent,
            this.colIsFloatingSite,
            this.colSiteAddress1,
            this.colSitePostcode,
            this.colSiteLat,
            this.colSiteLong,
            this.colSiteCode,
            this.colDoubleGrit,
            this.colSelected,
            this.colKMLFile,
            this.colKMLImageFile,
            this.colCompletionSheetFile});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsClipboard.AllowCopy = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsClipboard.AllowExcelFormat = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsClipboard.AllowRtfFormat = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsClipboard.ClipboardMode = DevExpress.Export.ClipboardMode.Formatted;
            this.gridView1.OptionsClipboard.CopyColumnHeaders = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCallOutDateTime, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colGrittingCallOutID
            // 
            this.colGrittingCallOutID.Caption = "Callout ID";
            this.colGrittingCallOutID.FieldName = "GrittingCallOutID";
            this.colGrittingCallOutID.Name = "colGrittingCallOutID";
            this.colGrittingCallOutID.OptionsColumn.AllowEdit = false;
            this.colGrittingCallOutID.OptionsColumn.AllowFocus = false;
            this.colGrittingCallOutID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteGrittingContractID
            // 
            this.colSiteGrittingContractID.Caption = "Site Gritting Contract ID";
            this.colSiteGrittingContractID.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID.Name = "colSiteGrittingContractID";
            this.colSiteGrittingContractID.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID.Width = 136;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 8;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 5;
            this.colSiteName.Width = 173;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 4;
            this.colClientName.Width = 108;
            // 
            // colCompanyID
            // 
            this.colCompanyID.Caption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 80;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 10;
            this.colCompanyName.Width = 107;
            // 
            // colSiteXCoordinate
            // 
            this.colSiteXCoordinate.Caption = "Site X Coord.";
            this.colSiteXCoordinate.FieldName = "SiteXCoordinate";
            this.colSiteXCoordinate.Name = "colSiteXCoordinate";
            this.colSiteXCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteXCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteXCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteXCoordinate.Width = 84;
            // 
            // colSiteYCoordinate
            // 
            this.colSiteYCoordinate.Caption = "Site Y Coord.";
            this.colSiteYCoordinate.FieldName = "SiteYCoordinate";
            this.colSiteYCoordinate.Name = "colSiteYCoordinate";
            this.colSiteYCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteYCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteYCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteYCoordinate.Width = 84;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Site Telephone";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Visible = true;
            this.colSiteTelephone.VisibleIndex = 29;
            this.colSiteTelephone.Width = 92;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Site Email";
            this.colSiteEmail.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            this.colSiteEmail.Visible = true;
            this.colSiteEmail.VisibleIndex = 30;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Visible = true;
            this.colClientsSiteCode.VisibleIndex = 22;
            this.colClientsSiteCode.Width = 102;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Team Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 11;
            this.colSubContractorName.Width = 131;
            // 
            // colSubContractorIsDirectLabour
            // 
            this.colSubContractorIsDirectLabour.Caption = "Is Direct Labour";
            this.colSubContractorIsDirectLabour.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSubContractorIsDirectLabour.FieldName = "SubContractorIsDirectLabour";
            this.colSubContractorIsDirectLabour.Name = "colSubContractorIsDirectLabour";
            this.colSubContractorIsDirectLabour.OptionsColumn.AllowEdit = false;
            this.colSubContractorIsDirectLabour.OptionsColumn.AllowFocus = false;
            this.colSubContractorIsDirectLabour.OptionsColumn.ReadOnly = true;
            this.colSubContractorIsDirectLabour.Visible = true;
            this.colSubContractorIsDirectLabour.VisibleIndex = 35;
            this.colSubContractorIsDirectLabour.Width = 97;
            // 
            // colOriginalSubContractorID
            // 
            this.colOriginalSubContractorID.Caption = "Original Team ID";
            this.colOriginalSubContractorID.FieldName = "OriginalSubContractorID";
            this.colOriginalSubContractorID.Name = "colOriginalSubContractorID";
            this.colOriginalSubContractorID.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorID.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorID.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorID.Width = 100;
            // 
            // colOriginalSubContarctorName
            // 
            this.colOriginalSubContarctorName.Caption = "Original Team Name";
            this.colOriginalSubContarctorName.FieldName = "OriginalSubContarctorName";
            this.colOriginalSubContarctorName.Name = "colOriginalSubContarctorName";
            this.colOriginalSubContarctorName.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContarctorName.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContarctorName.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContarctorName.Visible = true;
            this.colOriginalSubContarctorName.VisibleIndex = 74;
            this.colOriginalSubContarctorName.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 3;
            this.colReactive.Width = 62;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 88;
            // 
            // colCalloutStatusDescription
            // 
            this.colCalloutStatusDescription.Caption = "Status";
            this.colCalloutStatusDescription.FieldName = "CalloutStatusDescription";
            this.colCalloutStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCalloutStatusDescription.Name = "colCalloutStatusDescription";
            this.colCalloutStatusDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusDescription.Visible = true;
            this.colCalloutStatusDescription.VisibleIndex = 1;
            this.colCalloutStatusDescription.Width = 250;
            // 
            // colCalloutStatusOrder
            // 
            this.colCalloutStatusOrder.Caption = "Status Order";
            this.colCalloutStatusOrder.FieldName = "CalloutStatusOrder";
            this.colCalloutStatusOrder.Name = "colCalloutStatusOrder";
            this.colCalloutStatusOrder.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusOrder.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusOrder.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusOrder.Width = 83;
            // 
            // colCallOutDateTime
            // 
            this.colCallOutDateTime.Caption = "Callout Date\\Time";
            this.colCallOutDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCallOutDateTime.FieldName = "CallOutDateTime";
            this.colCallOutDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCallOutDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCallOutDateTime.Name = "colCallOutDateTime";
            this.colCallOutDateTime.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCallOutDateTime.Visible = true;
            this.colCallOutDateTime.VisibleIndex = 2;
            this.colCallOutDateTime.Width = 117;
            // 
            // colImportedWeatherForecastID
            // 
            this.colImportedWeatherForecastID.Caption = "Imported Forecast ID";
            this.colImportedWeatherForecastID.FieldName = "ImportedWeatherForecastID";
            this.colImportedWeatherForecastID.Name = "colImportedWeatherForecastID";
            this.colImportedWeatherForecastID.OptionsColumn.AllowEdit = false;
            this.colImportedWeatherForecastID.OptionsColumn.AllowFocus = false;
            this.colImportedWeatherForecastID.OptionsColumn.ReadOnly = true;
            this.colImportedWeatherForecastID.Width = 124;
            // 
            // colTextSentTime
            // 
            this.colTextSentTime.Caption = "Job Sent Time";
            this.colTextSentTime.ColumnEdit = this.repositoryItemTextEditTime;
            this.colTextSentTime.FieldName = "TextSentTime";
            this.colTextSentTime.Name = "colTextSentTime";
            this.colTextSentTime.OptionsColumn.AllowEdit = false;
            this.colTextSentTime.OptionsColumn.AllowFocus = false;
            this.colTextSentTime.OptionsColumn.ReadOnly = true;
            this.colTextSentTime.Visible = true;
            this.colTextSentTime.VisibleIndex = 23;
            this.colTextSentTime.Width = 93;
            // 
            // colSubContractorReceivedTime
            // 
            this.colSubContractorReceivedTime.Caption = "Team Received Time";
            this.colSubContractorReceivedTime.ColumnEdit = this.repositoryItemTextEditTime;
            this.colSubContractorReceivedTime.FieldName = "SubContractorReceivedTime";
            this.colSubContractorReceivedTime.Name = "colSubContractorReceivedTime";
            this.colSubContractorReceivedTime.OptionsColumn.AllowEdit = false;
            this.colSubContractorReceivedTime.OptionsColumn.AllowFocus = false;
            this.colSubContractorReceivedTime.OptionsColumn.ReadOnly = true;
            this.colSubContractorReceivedTime.Visible = true;
            this.colSubContractorReceivedTime.VisibleIndex = 24;
            this.colSubContractorReceivedTime.Width = 119;
            // 
            // colSubContractorRespondedTime
            // 
            this.colSubContractorRespondedTime.Caption = "Team Responded Time";
            this.colSubContractorRespondedTime.ColumnEdit = this.repositoryItemTextEditTime;
            this.colSubContractorRespondedTime.FieldName = "SubContractorRespondedTime";
            this.colSubContractorRespondedTime.Name = "colSubContractorRespondedTime";
            this.colSubContractorRespondedTime.OptionsColumn.AllowEdit = false;
            this.colSubContractorRespondedTime.OptionsColumn.AllowFocus = false;
            this.colSubContractorRespondedTime.OptionsColumn.ReadOnly = true;
            this.colSubContractorRespondedTime.Visible = true;
            this.colSubContractorRespondedTime.VisibleIndex = 25;
            this.colSubContractorRespondedTime.Width = 129;
            // 
            // colSubContractorETA
            // 
            this.colSubContractorETA.Caption = "Team ETA";
            this.colSubContractorETA.FieldName = "SubContractorETA";
            this.colSubContractorETA.Name = "colSubContractorETA";
            this.colSubContractorETA.OptionsColumn.AllowEdit = false;
            this.colSubContractorETA.OptionsColumn.AllowFocus = false;
            this.colSubContractorETA.OptionsColumn.ReadOnly = true;
            this.colSubContractorETA.Visible = true;
            this.colSubContractorETA.VisibleIndex = 20;
            // 
            // colCompletedTime
            // 
            this.colCompletedTime.Caption = "Completed Time";
            this.colCompletedTime.ColumnEdit = this.repositoryItemTextEditTime;
            this.colCompletedTime.FieldName = "CompletedTime";
            this.colCompletedTime.Name = "colCompletedTime";
            this.colCompletedTime.OptionsColumn.AllowEdit = false;
            this.colCompletedTime.OptionsColumn.AllowFocus = false;
            this.colCompletedTime.OptionsColumn.ReadOnly = true;
            this.colCompletedTime.Visible = true;
            this.colCompletedTime.VisibleIndex = 27;
            this.colCompletedTime.Width = 97;
            // 
            // colAuthorisedByStaffID
            // 
            this.colAuthorisedByStaffID.Caption = "Authorised By ID";
            this.colAuthorisedByStaffID.FieldName = "AuthorisedByStaffID";
            this.colAuthorisedByStaffID.Name = "colAuthorisedByStaffID";
            this.colAuthorisedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByStaffID.Width = 102;
            // 
            // colAuthorisedByName
            // 
            this.colAuthorisedByName.Caption = "Authorised By";
            this.colAuthorisedByName.FieldName = "AuthorisedByName";
            this.colAuthorisedByName.Name = "colAuthorisedByName";
            this.colAuthorisedByName.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByName.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByName.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByName.Visible = true;
            this.colAuthorisedByName.VisibleIndex = 31;
            this.colAuthorisedByName.Width = 88;
            // 
            // colVisitAborted
            // 
            this.colVisitAborted.Caption = "Aborted";
            this.colVisitAborted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colVisitAborted.FieldName = "VisitAborted";
            this.colVisitAborted.Name = "colVisitAborted";
            this.colVisitAborted.OptionsColumn.AllowEdit = false;
            this.colVisitAborted.OptionsColumn.AllowFocus = false;
            this.colVisitAborted.OptionsColumn.ReadOnly = true;
            this.colVisitAborted.Visible = true;
            this.colVisitAborted.VisibleIndex = 32;
            // 
            // colAbortedReason
            // 
            this.colAbortedReason.Caption = "Aborted Reason";
            this.colAbortedReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colAbortedReason.FieldName = "AbortedReason";
            this.colAbortedReason.Name = "colAbortedReason";
            this.colAbortedReason.OptionsColumn.ReadOnly = true;
            this.colAbortedReason.Visible = true;
            this.colAbortedReason.VisibleIndex = 33;
            this.colAbortedReason.Width = 99;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Lat";
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Long";
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            // 
            // colFinishedLatitude
            // 
            this.colFinishedLatitude.Caption = "Finished Lat";
            this.colFinishedLatitude.FieldName = "FinishedLatitude";
            this.colFinishedLatitude.Name = "colFinishedLatitude";
            this.colFinishedLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishedLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishedLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishedLatitude.Width = 78;
            // 
            // colFinishedLongitude
            // 
            this.colFinishedLongitude.Caption = "Finished Long";
            this.colFinishedLongitude.FieldName = "FinishedLongitude";
            this.colFinishedLongitude.Name = "colFinishedLongitude";
            this.colFinishedLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishedLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishedLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishedLongitude.Width = 86;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "P.O. Number";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 34;
            this.colClientPONumber.Width = 83;
            // 
            // colSaltUsed
            // 
            this.colSaltUsed.Caption = "Salt Used";
            this.colSaltUsed.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colSaltUsed.FieldName = "SaltUsed";
            this.colSaltUsed.Name = "colSaltUsed";
            this.colSaltUsed.OptionsColumn.AllowEdit = false;
            this.colSaltUsed.OptionsColumn.AllowFocus = false;
            this.colSaltUsed.OptionsColumn.ReadOnly = true;
            this.colSaltUsed.Visible = true;
            this.colSaltUsed.VisibleIndex = 42;
            // 
            // colSaltCost
            // 
            this.colSaltCost.Caption = "Salt Cost";
            this.colSaltCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSaltCost.FieldName = "SaltCost";
            this.colSaltCost.Name = "colSaltCost";
            this.colSaltCost.OptionsColumn.AllowEdit = false;
            this.colSaltCost.OptionsColumn.AllowFocus = false;
            this.colSaltCost.OptionsColumn.ReadOnly = true;
            this.colSaltCost.Visible = true;
            this.colSaltCost.VisibleIndex = 43;
            // 
            // colSaltSell
            // 
            this.colSaltSell.Caption = "Salt Sell";
            this.colSaltSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSaltSell.FieldName = "SaltSell";
            this.colSaltSell.Name = "colSaltSell";
            this.colSaltSell.OptionsColumn.AllowEdit = false;
            this.colSaltSell.OptionsColumn.AllowFocus = false;
            this.colSaltSell.OptionsColumn.ReadOnly = true;
            this.colSaltSell.Visible = true;
            this.colSaltSell.VisibleIndex = 44;
            // 
            // colSaltVatRate
            // 
            this.colSaltVatRate.Caption = "Salt VAT Rate";
            this.colSaltVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSaltVatRate.FieldName = "SaltVatRate";
            this.colSaltVatRate.Name = "colSaltVatRate";
            this.colSaltVatRate.OptionsColumn.AllowEdit = false;
            this.colSaltVatRate.OptionsColumn.AllowFocus = false;
            this.colSaltVatRate.OptionsColumn.ReadOnly = true;
            this.colSaltVatRate.Visible = true;
            this.colSaltVatRate.VisibleIndex = 45;
            this.colSaltVatRate.Width = 87;
            // 
            // colHoursWorked
            // 
            this.colHoursWorked.Caption = "Hours Worked";
            this.colHoursWorked.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colHoursWorked.FieldName = "HoursWorked";
            this.colHoursWorked.Name = "colHoursWorked";
            this.colHoursWorked.OptionsColumn.AllowEdit = false;
            this.colHoursWorked.OptionsColumn.AllowFocus = false;
            this.colHoursWorked.OptionsColumn.ReadOnly = true;
            this.colHoursWorked.Visible = true;
            this.colHoursWorked.VisibleIndex = 36;
            this.colHoursWorked.Width = 89;
            // 
            // colTeamHourlyRate
            // 
            this.colTeamHourlyRate.Caption = "Hourly Rate";
            this.colTeamHourlyRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTeamHourlyRate.FieldName = "TeamHourlyRate";
            this.colTeamHourlyRate.Name = "colTeamHourlyRate";
            this.colTeamHourlyRate.OptionsColumn.AllowEdit = false;
            this.colTeamHourlyRate.OptionsColumn.AllowFocus = false;
            this.colTeamHourlyRate.OptionsColumn.ReadOnly = true;
            this.colTeamHourlyRate.Visible = true;
            this.colTeamHourlyRate.VisibleIndex = 37;
            this.colTeamHourlyRate.Width = 78;
            // 
            // colTeamCharge
            // 
            this.colTeamCharge.Caption = "Team Charge";
            this.colTeamCharge.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTeamCharge.FieldName = "TeamCharge";
            this.colTeamCharge.Name = "colTeamCharge";
            this.colTeamCharge.OptionsColumn.AllowEdit = false;
            this.colTeamCharge.OptionsColumn.AllowFocus = false;
            this.colTeamCharge.OptionsColumn.ReadOnly = true;
            this.colTeamCharge.Visible = true;
            this.colTeamCharge.VisibleIndex = 39;
            this.colTeamCharge.Width = 85;
            // 
            // colLabourCost
            // 
            this.colLabourCost.Caption = "Labour Cost";
            this.colLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colLabourCost.FieldName = "LabourCost";
            this.colLabourCost.Name = "colLabourCost";
            this.colLabourCost.OptionsColumn.AllowEdit = false;
            this.colLabourCost.OptionsColumn.AllowFocus = false;
            this.colLabourCost.OptionsColumn.ReadOnly = true;
            this.colLabourCost.Visible = true;
            this.colLabourCost.VisibleIndex = 40;
            this.colLabourCost.Width = 79;
            // 
            // colLabourVatRate
            // 
            this.colLabourVatRate.Caption = "Labour VAT Rate";
            this.colLabourVatRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colLabourVatRate.FieldName = "LabourVatRate";
            this.colLabourVatRate.Name = "colLabourVatRate";
            this.colLabourVatRate.OptionsColumn.AllowEdit = false;
            this.colLabourVatRate.OptionsColumn.AllowFocus = false;
            this.colLabourVatRate.OptionsColumn.ReadOnly = true;
            this.colLabourVatRate.Visible = true;
            this.colLabourVatRate.VisibleIndex = 41;
            this.colLabourVatRate.Width = 102;
            // 
            // colOtherCost
            // 
            this.colOtherCost.Caption = "Other Cost";
            this.colOtherCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherCost.FieldName = "OtherCost";
            this.colOtherCost.Name = "colOtherCost";
            this.colOtherCost.OptionsColumn.AllowEdit = false;
            this.colOtherCost.OptionsColumn.AllowFocus = false;
            this.colOtherCost.OptionsColumn.ReadOnly = true;
            this.colOtherCost.Visible = true;
            this.colOtherCost.VisibleIndex = 46;
            // 
            // colOtherSell
            // 
            this.colOtherSell.Caption = "Other Sell";
            this.colOtherSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherSell.FieldName = "OtherSell";
            this.colOtherSell.Name = "colOtherSell";
            this.colOtherSell.OptionsColumn.AllowEdit = false;
            this.colOtherSell.OptionsColumn.AllowFocus = false;
            this.colOtherSell.OptionsColumn.ReadOnly = true;
            this.colOtherSell.Visible = true;
            this.colOtherSell.VisibleIndex = 47;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 48;
            // 
            // colTotalSell
            // 
            this.colTotalSell.Caption = "Total Sell";
            this.colTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalSell.FieldName = "TotalSell";
            this.colTotalSell.Name = "colTotalSell";
            this.colTotalSell.OptionsColumn.AllowEdit = false;
            this.colTotalSell.OptionsColumn.AllowFocus = false;
            this.colTotalSell.OptionsColumn.ReadOnly = true;
            this.colTotalSell.Visible = true;
            this.colTotalSell.VisibleIndex = 50;
            // 
            // colClientInvoiceNumber
            // 
            this.colClientInvoiceNumber.Caption = "Invoice Number";
            this.colClientInvoiceNumber.FieldName = "ClientInvoiceNumber";
            this.colClientInvoiceNumber.Name = "colClientInvoiceNumber";
            this.colClientInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceNumber.Visible = true;
            this.colClientInvoiceNumber.VisibleIndex = 55;
            this.colClientInvoiceNumber.Width = 96;
            // 
            // colSubContractorPaid
            // 
            this.colSubContractorPaid.Caption = "Team Paid";
            this.colSubContractorPaid.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSubContractorPaid.FieldName = "SubContractorPaid";
            this.colSubContractorPaid.Name = "colSubContractorPaid";
            this.colSubContractorPaid.OptionsColumn.AllowEdit = false;
            this.colSubContractorPaid.OptionsColumn.AllowFocus = false;
            this.colSubContractorPaid.OptionsColumn.ReadOnly = true;
            this.colSubContractorPaid.Visible = true;
            this.colSubContractorPaid.VisibleIndex = 57;
            // 
            // colGrittingInvoiceID
            // 
            this.colGrittingInvoiceID.Caption = "Gritting Invoice ID";
            this.colGrittingInvoiceID.FieldName = "GrittingInvoiceID";
            this.colGrittingInvoiceID.Name = "colGrittingInvoiceID";
            this.colGrittingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colGrittingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colGrittingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colGrittingInvoiceID.Width = 108;
            // 
            // colRecordedByStaffID
            // 
            this.colRecordedByStaffID.Caption = "Recorded By Staff ID";
            this.colRecordedByStaffID.FieldName = "RecordedByStaffID";
            this.colRecordedByStaffID.Name = "colRecordedByStaffID";
            this.colRecordedByStaffID.OptionsColumn.AllowEdit = false;
            this.colRecordedByStaffID.OptionsColumn.AllowFocus = false;
            this.colRecordedByStaffID.OptionsColumn.ReadOnly = true;
            this.colRecordedByStaffID.Width = 123;
            // 
            // colRecordedByName
            // 
            this.colRecordedByName.Caption = "Recorded By";
            this.colRecordedByName.FieldName = "RecordedByName";
            this.colRecordedByName.Name = "colRecordedByName";
            this.colRecordedByName.OptionsColumn.AllowEdit = false;
            this.colRecordedByName.OptionsColumn.AllowFocus = false;
            this.colRecordedByName.OptionsColumn.ReadOnly = true;
            this.colRecordedByName.Visible = true;
            this.colRecordedByName.VisibleIndex = 58;
            this.colRecordedByName.Width = 118;
            // 
            // colPaidByStaffID
            // 
            this.colPaidByStaffID.Caption = "Paid By Staff ID";
            this.colPaidByStaffID.FieldName = "PaidByStaffID";
            this.colPaidByStaffID.Name = "colPaidByStaffID";
            this.colPaidByStaffID.OptionsColumn.AllowEdit = false;
            this.colPaidByStaffID.OptionsColumn.AllowFocus = false;
            this.colPaidByStaffID.OptionsColumn.ReadOnly = true;
            this.colPaidByStaffID.Width = 97;
            // 
            // colPaidByName
            // 
            this.colPaidByName.Caption = "Paid By";
            this.colPaidByName.FieldName = "PaidByName";
            this.colPaidByName.Name = "colPaidByName";
            this.colPaidByName.OptionsColumn.AllowEdit = false;
            this.colPaidByName.OptionsColumn.AllowFocus = false;
            this.colPaidByName.OptionsColumn.ReadOnly = true;
            this.colPaidByName.Visible = true;
            this.colPaidByName.VisibleIndex = 59;
            this.colPaidByName.Width = 93;
            // 
            // colDoNotPaySubContractor
            // 
            this.colDoNotPaySubContractor.Caption = "Don\'t Pay Team";
            this.colDoNotPaySubContractor.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPaySubContractor.FieldName = "DoNotPaySubContractor";
            this.colDoNotPaySubContractor.Name = "colDoNotPaySubContractor";
            this.colDoNotPaySubContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPaySubContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPaySubContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractor.Visible = true;
            this.colDoNotPaySubContractor.VisibleIndex = 60;
            this.colDoNotPaySubContractor.Width = 96;
            // 
            // colDoNotPaySubContractorReason
            // 
            this.colDoNotPaySubContractorReason.Caption = "Don\'t Pay Team Reason";
            this.colDoNotPaySubContractorReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDoNotPaySubContractorReason.FieldName = "DoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.Name = "colDoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.OptionsColumn.AllowEdit = false;
            this.colDoNotPaySubContractorReason.OptionsColumn.AllowFocus = false;
            this.colDoNotPaySubContractorReason.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractorReason.Visible = true;
            this.colDoNotPaySubContractorReason.VisibleIndex = 61;
            this.colDoNotPaySubContractorReason.Width = 135;
            // 
            // colGritSourceLocationID
            // 
            this.colGritSourceLocationID.Caption = "Grit Source Location ID";
            this.colGritSourceLocationID.FieldName = "GritSourceLocationID";
            this.colGritSourceLocationID.Name = "colGritSourceLocationID";
            this.colGritSourceLocationID.OptionsColumn.AllowEdit = false;
            this.colGritSourceLocationID.OptionsColumn.AllowFocus = false;
            this.colGritSourceLocationID.OptionsColumn.ReadOnly = true;
            this.colGritSourceLocationID.Width = 131;
            // 
            // colGritSourceLocationTypeID
            // 
            this.colGritSourceLocationTypeID.Caption = "Grit Source Location Type ID";
            this.colGritSourceLocationTypeID.FieldName = "GritSourceLocationTypeID";
            this.colGritSourceLocationTypeID.Name = "colGritSourceLocationTypeID";
            this.colGritSourceLocationTypeID.OptionsColumn.AllowEdit = false;
            this.colGritSourceLocationTypeID.OptionsColumn.AllowFocus = false;
            this.colGritSourceLocationTypeID.OptionsColumn.ReadOnly = true;
            this.colGritSourceLocationTypeID.Width = 158;
            // 
            // colNoAccess
            // 
            this.colNoAccess.Caption = "No Access";
            this.colNoAccess.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoAccess.FieldName = "NoAccess";
            this.colNoAccess.Name = "colNoAccess";
            this.colNoAccess.OptionsColumn.AllowEdit = false;
            this.colNoAccess.OptionsColumn.AllowFocus = false;
            this.colNoAccess.OptionsColumn.ReadOnly = true;
            this.colNoAccess.Visible = true;
            this.colNoAccess.VisibleIndex = 62;
            // 
            // colSiteWeather
            // 
            this.colSiteWeather.Caption = "Site Weather";
            this.colSiteWeather.FieldName = "SiteWeather";
            this.colSiteWeather.Name = "colSiteWeather";
            this.colSiteWeather.OptionsColumn.AllowEdit = false;
            this.colSiteWeather.OptionsColumn.AllowFocus = false;
            this.colSiteWeather.OptionsColumn.ReadOnly = true;
            this.colSiteWeather.Visible = true;
            this.colSiteWeather.VisibleIndex = 63;
            this.colSiteWeather.Width = 84;
            // 
            // colSiteTemperature
            // 
            this.colSiteTemperature.Caption = "Site Temperature";
            this.colSiteTemperature.FieldName = "SiteTemperature";
            this.colSiteTemperature.Name = "colSiteTemperature";
            this.colSiteTemperature.OptionsColumn.AllowEdit = false;
            this.colSiteTemperature.OptionsColumn.AllowFocus = false;
            this.colSiteTemperature.OptionsColumn.ReadOnly = true;
            this.colSiteTemperature.Visible = true;
            this.colSiteTemperature.VisibleIndex = 64;
            this.colSiteTemperature.Width = 104;
            // 
            // colPdaID
            // 
            this.colPdaID.Caption = "Device ID";
            this.colPdaID.FieldName = "PdaID";
            this.colPdaID.Name = "colPdaID";
            this.colPdaID.OptionsColumn.AllowEdit = false;
            this.colPdaID.OptionsColumn.AllowFocus = false;
            this.colPdaID.OptionsColumn.ReadOnly = true;
            this.colPdaID.Visible = true;
            this.colPdaID.VisibleIndex = 71;
            // 
            // colTeamPresent
            // 
            this.colTeamPresent.Caption = "Team Present";
            this.colTeamPresent.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTeamPresent.FieldName = "TeamPresent";
            this.colTeamPresent.Name = "colTeamPresent";
            this.colTeamPresent.OptionsColumn.AllowEdit = false;
            this.colTeamPresent.OptionsColumn.AllowFocus = false;
            this.colTeamPresent.OptionsColumn.ReadOnly = true;
            this.colTeamPresent.Visible = true;
            this.colTeamPresent.VisibleIndex = 19;
            this.colTeamPresent.Width = 87;
            // 
            // colServiceFailure
            // 
            this.colServiceFailure.Caption = "Service Failure";
            this.colServiceFailure.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colServiceFailure.FieldName = "ServiceFailure";
            this.colServiceFailure.Name = "colServiceFailure";
            this.colServiceFailure.OptionsColumn.AllowEdit = false;
            this.colServiceFailure.OptionsColumn.AllowFocus = false;
            this.colServiceFailure.OptionsColumn.ReadOnly = true;
            this.colServiceFailure.Visible = true;
            this.colServiceFailure.VisibleIndex = 73;
            this.colServiceFailure.Width = 91;
            // 
            // colComments
            // 
            this.colComments.Caption = "Comments";
            this.colComments.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colComments.FieldName = "Comments";
            this.colComments.Name = "colComments";
            this.colComments.OptionsColumn.ReadOnly = true;
            this.colComments.Visible = true;
            this.colComments.VisibleIndex = 76;
            // 
            // colClientPrice
            // 
            this.colClientPrice.Caption = "Client Price";
            this.colClientPrice.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colClientPrice.FieldName = "ClientPrice";
            this.colClientPrice.Name = "colClientPrice";
            this.colClientPrice.OptionsColumn.AllowEdit = false;
            this.colClientPrice.OptionsColumn.AllowFocus = false;
            this.colClientPrice.OptionsColumn.ReadOnly = true;
            this.colClientPrice.Visible = true;
            this.colClientPrice.VisibleIndex = 49;
            // 
            // colGritJobTransferMethod
            // 
            this.colGritJobTransferMethod.Caption = "Transfer Method";
            this.colGritJobTransferMethod.FieldName = "GritJobTransferMethod";
            this.colGritJobTransferMethod.Name = "colGritJobTransferMethod";
            this.colGritJobTransferMethod.OptionsColumn.AllowEdit = false;
            this.colGritJobTransferMethod.OptionsColumn.AllowFocus = false;
            this.colGritJobTransferMethod.OptionsColumn.ReadOnly = true;
            this.colGritJobTransferMethod.Visible = true;
            this.colGritJobTransferMethod.VisibleIndex = 21;
            this.colGritJobTransferMethod.Width = 101;
            // 
            // colSnowOnSite
            // 
            this.colSnowOnSite.Caption = "Snow On Site";
            this.colSnowOnSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSnowOnSite.FieldName = "SnowOnSite";
            this.colSnowOnSite.Name = "colSnowOnSite";
            this.colSnowOnSite.OptionsColumn.AllowEdit = false;
            this.colSnowOnSite.OptionsColumn.AllowFocus = false;
            this.colSnowOnSite.OptionsColumn.ReadOnly = true;
            this.colSnowOnSite.Visible = true;
            this.colSnowOnSite.VisibleIndex = 65;
            this.colSnowOnSite.Width = 85;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEditTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 26;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "<b>Calculated 1</b>";
            this.gridColumn1.FieldName = "Calculated1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.ShowUnboundExpressionMenu = true;
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 85;
            this.gridColumn1.Width = 90;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "<b>Calculated 2</b>";
            this.gridColumn2.FieldName = "Calculated2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.ShowUnboundExpressionMenu = true;
            this.gridColumn2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 86;
            this.gridColumn2.Width = 90;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "<b>Calculated 3</b>";
            this.gridColumn3.FieldName = "Calculated3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.ShowUnboundExpressionMenu = true;
            this.gridColumn3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 87;
            this.gridColumn3.Width = 90;
            // 
            // colProfit
            // 
            this.colProfit.Caption = "Profit";
            this.colProfit.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colProfit.FieldName = "Profit";
            this.colProfit.Name = "colProfit";
            this.colProfit.OptionsColumn.AllowEdit = false;
            this.colProfit.OptionsColumn.AllowFocus = false;
            this.colProfit.OptionsColumn.ReadOnly = true;
            this.colProfit.Visible = true;
            this.colProfit.VisibleIndex = 51;
            // 
            // colMarkup
            // 
            this.colMarkup.Caption = "Markup";
            this.colMarkup.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colMarkup.FieldName = "Markup";
            this.colMarkup.Name = "colMarkup";
            this.colMarkup.OptionsColumn.AllowEdit = false;
            this.colMarkup.OptionsColumn.AllowFocus = false;
            this.colMarkup.OptionsColumn.ReadOnly = true;
            this.colMarkup.Visible = true;
            this.colMarkup.VisibleIndex = 52;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "P.O. Linked ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 90;
            // 
            // colNonStandardCost
            // 
            this.colNonStandardCost.Caption = "Non-Standard Cost";
            this.colNonStandardCost.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardCost.FieldName = "NonStandardCost";
            this.colNonStandardCost.Name = "colNonStandardCost";
            this.colNonStandardCost.OptionsColumn.AllowEdit = false;
            this.colNonStandardCost.OptionsColumn.AllowFocus = false;
            this.colNonStandardCost.OptionsColumn.ReadOnly = true;
            this.colNonStandardCost.Visible = true;
            this.colNonStandardCost.VisibleIndex = 77;
            this.colNonStandardCost.Width = 113;
            // 
            // colNonStandardSell
            // 
            this.colNonStandardSell.Caption = "Non-Standard Sell";
            this.colNonStandardSell.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNonStandardSell.FieldName = "NonStandardSell";
            this.colNonStandardSell.Name = "colNonStandardSell";
            this.colNonStandardSell.OptionsColumn.AllowEdit = false;
            this.colNonStandardSell.OptionsColumn.AllowFocus = false;
            this.colNonStandardSell.OptionsColumn.ReadOnly = true;
            this.colNonStandardSell.Visible = true;
            this.colNonStandardSell.VisibleIndex = 78;
            this.colNonStandardSell.Width = 107;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 53;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colDoNotInvoiceClientReason
            // 
            this.colDoNotInvoiceClientReason.Caption = "Do Not Invoice Client Reason";
            this.colDoNotInvoiceClientReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDoNotInvoiceClientReason.FieldName = "DoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.Name = "colDoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClientReason.Width = 161;
            // 
            // colGritMobileTelephoneNumber
            // 
            this.colGritMobileTelephoneNumber.Caption = "Grit Mobile Telephone";
            this.colGritMobileTelephoneNumber.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colGritMobileTelephoneNumber.FieldName = "GritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.Name = "colGritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.OptionsColumn.ReadOnly = true;
            this.colGritMobileTelephoneNumber.Visible = true;
            this.colGritMobileTelephoneNumber.VisibleIndex = 70;
            this.colGritMobileTelephoneNumber.Width = 124;
            // 
            // colClientSignatureFileName
            // 
            this.colClientSignatureFileName.Caption = "Client Signature";
            this.colClientSignatureFileName.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colClientSignatureFileName.FieldName = "ClientSignatureFileName";
            this.colClientSignatureFileName.Name = "colClientSignatureFileName";
            this.colClientSignatureFileName.OptionsColumn.ReadOnly = true;
            this.colClientSignatureFileName.Visible = true;
            this.colClientSignatureFileName.VisibleIndex = 79;
            this.colClientSignatureFileName.Width = 97;
            // 
            // colPPEWorn
            // 
            this.colPPEWorn.Caption = "PPE Worn";
            this.colPPEWorn.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPPEWorn.FieldName = "PPEWorn";
            this.colPPEWorn.Name = "colPPEWorn";
            this.colPPEWorn.OptionsColumn.AllowEdit = false;
            this.colPPEWorn.OptionsColumn.AllowFocus = false;
            this.colPPEWorn.OptionsColumn.ReadOnly = true;
            // 
            // colNoAccessAbortedRate
            // 
            this.colNoAccessAbortedRate.Caption = "No Access\\Aborted Rate";
            this.colNoAccessAbortedRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colNoAccessAbortedRate.FieldName = "NoAccessAbortedRate";
            this.colNoAccessAbortedRate.Name = "colNoAccessAbortedRate";
            this.colNoAccessAbortedRate.OptionsColumn.AllowEdit = false;
            this.colNoAccessAbortedRate.OptionsColumn.AllowFocus = false;
            this.colNoAccessAbortedRate.OptionsColumn.ReadOnly = true;
            this.colNoAccessAbortedRate.Visible = true;
            this.colNoAccessAbortedRate.VisibleIndex = 38;
            this.colNoAccessAbortedRate.Width = 139;
            // 
            // gridColumn4
            // 
            this.gridColumn4.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumn4.CustomizationCaption = "Alert";
            this.gridColumn4.FieldName = "Alert";
            this.gridColumn4.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.AllowSize = false;
            this.gridColumn4.OptionsColumn.FixedWidth = true;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsColumn.ShowCaption = false;
            this.gridColumn4.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn4.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 20;
            // 
            // colTeamMembersPresent
            // 
            this.colTeamMembersPresent.Caption = "Team Members Present";
            this.colTeamMembersPresent.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colTeamMembersPresent.FieldName = "TeamMembersPresent";
            this.colTeamMembersPresent.Name = "colTeamMembersPresent";
            this.colTeamMembersPresent.OptionsColumn.ReadOnly = true;
            this.colTeamMembersPresent.Visible = true;
            this.colTeamMembersPresent.VisibleIndex = 75;
            this.colTeamMembersPresent.Width = 133;
            // 
            // colSnowInfo
            // 
            this.colSnowInfo.Caption = "Snow Info";
            this.colSnowInfo.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSnowInfo.FieldName = "SnowInfo";
            this.colSnowInfo.Name = "colSnowInfo";
            this.colSnowInfo.OptionsColumn.ReadOnly = true;
            this.colSnowInfo.Visible = true;
            this.colSnowInfo.VisibleIndex = 67;
            // 
            // colEveningRateModifier
            // 
            this.colEveningRateModifier.Caption = "Evening Rate Modifier";
            this.colEveningRateModifier.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colEveningRateModifier.FieldName = "EveningRateModifier";
            this.colEveningRateModifier.Name = "colEveningRateModifier";
            this.colEveningRateModifier.OptionsColumn.AllowEdit = false;
            this.colEveningRateModifier.OptionsColumn.AllowFocus = false;
            this.colEveningRateModifier.OptionsColumn.ReadOnly = true;
            this.colEveningRateModifier.Width = 126;
            // 
            // colCompletedOnPDA
            // 
            this.colCompletedOnPDA.Caption = "Completed on Device";
            this.colCompletedOnPDA.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCompletedOnPDA.FieldName = "CompletedOnPDA";
            this.colCompletedOnPDA.Name = "colCompletedOnPDA";
            this.colCompletedOnPDA.OptionsColumn.AllowEdit = false;
            this.colCompletedOnPDA.OptionsColumn.AllowFocus = false;
            this.colCompletedOnPDA.OptionsColumn.ReadOnly = true;
            this.colCompletedOnPDA.Visible = true;
            this.colCompletedOnPDA.VisibleIndex = 80;
            this.colCompletedOnPDA.Width = 120;
            // 
            // colJobSheetNumber
            // 
            this.colJobSheetNumber.Caption = "Job Sheet Number";
            this.colJobSheetNumber.FieldName = "JobSheetNumber";
            this.colJobSheetNumber.Name = "colJobSheetNumber";
            this.colJobSheetNumber.OptionsColumn.AllowEdit = false;
            this.colJobSheetNumber.OptionsColumn.AllowFocus = false;
            this.colJobSheetNumber.OptionsColumn.ReadOnly = true;
            this.colJobSheetNumber.Visible = true;
            this.colJobSheetNumber.VisibleIndex = 81;
            this.colJobSheetNumber.Width = 109;
            // 
            // colFinanceInvoiceNumber
            // 
            this.colFinanceInvoiceNumber.Caption = "Finance Invoice No";
            this.colFinanceInvoiceNumber.FieldName = "FinanceInvoiceNumber";
            this.colFinanceInvoiceNumber.Name = "colFinanceInvoiceNumber";
            this.colFinanceInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colFinanceInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colFinanceInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colFinanceInvoiceNumber.Visible = true;
            this.colFinanceInvoiceNumber.VisibleIndex = 56;
            this.colFinanceInvoiceNumber.Width = 112;
            // 
            // colFinanceTeamPaymentExported
            // 
            this.colFinanceTeamPaymentExported.Caption = "Team Payment Exported To Finance";
            this.colFinanceTeamPaymentExported.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colFinanceTeamPaymentExported.FieldName = "FinanceTeamPaymentExported";
            this.colFinanceTeamPaymentExported.Name = "colFinanceTeamPaymentExported";
            this.colFinanceTeamPaymentExported.OptionsColumn.AllowEdit = false;
            this.colFinanceTeamPaymentExported.OptionsColumn.AllowFocus = false;
            this.colFinanceTeamPaymentExported.OptionsColumn.ReadOnly = true;
            this.colFinanceTeamPaymentExported.Width = 191;
            // 
            // colCalloutInvoiced
            // 
            this.colCalloutInvoiced.Caption = "Invoiced";
            this.colCalloutInvoiced.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCalloutInvoiced.FieldName = "CalloutInvoiced";
            this.colCalloutInvoiced.Name = "colCalloutInvoiced";
            this.colCalloutInvoiced.OptionsColumn.AllowEdit = false;
            this.colCalloutInvoiced.OptionsColumn.AllowFocus = false;
            this.colCalloutInvoiced.OptionsColumn.ReadOnly = true;
            this.colCalloutInvoiced.Visible = true;
            this.colCalloutInvoiced.VisibleIndex = 54;
            this.colCalloutInvoiced.Width = 62;
            // 
            // colSiteManagerName
            // 
            this.colSiteManagerName.Caption = "Site Manager Name";
            this.colSiteManagerName.FieldName = "SiteManagerName";
            this.colSiteManagerName.Name = "colSiteManagerName";
            this.colSiteManagerName.OptionsColumn.AllowEdit = false;
            this.colSiteManagerName.OptionsColumn.AllowFocus = false;
            this.colSiteManagerName.OptionsColumn.ReadOnly = true;
            this.colSiteManagerName.Visible = true;
            this.colSiteManagerName.VisibleIndex = 72;
            this.colSiteManagerName.Width = 114;
            // 
            // colPrioritySite
            // 
            this.colPrioritySite.Caption = "Priority Site";
            this.colPrioritySite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPrioritySite.FieldName = "PrioritySite";
            this.colPrioritySite.Name = "colPrioritySite";
            this.colPrioritySite.OptionsColumn.AllowEdit = false;
            this.colPrioritySite.OptionsColumn.AllowFocus = false;
            this.colPrioritySite.OptionsColumn.ReadOnly = true;
            this.colPrioritySite.Visible = true;
            this.colPrioritySite.VisibleIndex = 6;
            this.colPrioritySite.Width = 74;
            // 
            // colSnowOnSiteOver5cm
            // 
            this.colSnowOnSiteOver5cm.Caption = "Snow Over 5 cm";
            this.colSnowOnSiteOver5cm.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSnowOnSiteOver5cm.FieldName = "SnowOnSiteOver5cm";
            this.colSnowOnSiteOver5cm.Name = "colSnowOnSiteOver5cm";
            this.colSnowOnSiteOver5cm.OptionsColumn.AllowEdit = false;
            this.colSnowOnSiteOver5cm.OptionsColumn.AllowFocus = false;
            this.colSnowOnSiteOver5cm.OptionsColumn.ReadOnly = true;
            this.colSnowOnSiteOver5cm.Visible = true;
            this.colSnowOnSiteOver5cm.VisibleIndex = 66;
            this.colSnowOnSiteOver5cm.Width = 99;
            // 
            // colInternalRemarks
            // 
            this.colInternalRemarks.Caption = "Internal Remarks";
            this.colInternalRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colInternalRemarks.FieldName = "InternalRemarks";
            this.colInternalRemarks.Name = "colInternalRemarks";
            this.colInternalRemarks.OptionsColumn.AllowEdit = false;
            this.colInternalRemarks.OptionsColumn.AllowFocus = false;
            this.colInternalRemarks.OptionsColumn.ReadOnly = true;
            this.colInternalRemarks.Width = 103;
            // 
            // colPDACode
            // 
            this.colPDACode.Caption = "Device Code";
            this.colPDACode.FieldName = "PDACode";
            this.colPDACode.Name = "colPDACode";
            this.colPDACode.OptionsColumn.AllowEdit = false;
            this.colPDACode.OptionsColumn.AllowFocus = false;
            this.colPDACode.OptionsColumn.ReadOnly = true;
            this.colPDACode.Visible = true;
            this.colPDACode.VisibleIndex = 18;
            // 
            // colSnowCleared
            // 
            this.colSnowCleared.Caption = "Snow Cleared";
            this.colSnowCleared.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSnowCleared.FieldName = "SnowCleared";
            this.colSnowCleared.Name = "colSnowCleared";
            this.colSnowCleared.OptionsColumn.AllowEdit = false;
            this.colSnowCleared.OptionsColumn.AllowFocus = false;
            this.colSnowCleared.OptionsColumn.ReadOnly = true;
            this.colSnowCleared.Visible = true;
            this.colSnowCleared.VisibleIndex = 68;
            this.colSnowCleared.Width = 87;
            // 
            // colSnowClearedSatisfactory
            // 
            this.colSnowClearedSatisfactory.Caption = "Snow Cleared Ok";
            this.colSnowClearedSatisfactory.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSnowClearedSatisfactory.FieldName = "SnowClearedSatisfactory";
            this.colSnowClearedSatisfactory.Name = "colSnowClearedSatisfactory";
            this.colSnowClearedSatisfactory.OptionsColumn.AllowEdit = false;
            this.colSnowClearedSatisfactory.OptionsColumn.AllowFocus = false;
            this.colSnowClearedSatisfactory.OptionsColumn.ReadOnly = true;
            this.colSnowClearedSatisfactory.Visible = true;
            this.colSnowClearedSatisfactory.VisibleIndex = 69;
            this.colSnowClearedSatisfactory.Width = 103;
            // 
            // colAttendanceOrder
            // 
            this.colAttendanceOrder.Caption = "Route Order";
            this.colAttendanceOrder.FieldName = "AttendanceOrder";
            this.colAttendanceOrder.Name = "colAttendanceOrder";
            this.colAttendanceOrder.OptionsColumn.AllowEdit = false;
            this.colAttendanceOrder.OptionsColumn.AllowFocus = false;
            this.colAttendanceOrder.OptionsColumn.ReadOnly = true;
            this.colAttendanceOrder.Visible = true;
            this.colAttendanceOrder.VisibleIndex = 17;
            this.colAttendanceOrder.Width = 81;
            // 
            // colCompletionEmailSent
            // 
            this.colCompletionEmailSent.Caption = "Completion Email Sent";
            this.colCompletionEmailSent.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCompletionEmailSent.FieldName = "CompletionEmailSent";
            this.colCompletionEmailSent.Name = "colCompletionEmailSent";
            this.colCompletionEmailSent.OptionsColumn.AllowEdit = false;
            this.colCompletionEmailSent.OptionsColumn.AllowFocus = false;
            this.colCompletionEmailSent.OptionsColumn.ReadOnly = true;
            this.colCompletionEmailSent.Visible = true;
            this.colCompletionEmailSent.VisibleIndex = 28;
            this.colCompletionEmailSent.Width = 126;
            // 
            // colIsFloatingSite
            // 
            this.colIsFloatingSite.Caption = "Is Floating Site";
            this.colIsFloatingSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsFloatingSite.FieldName = "IsFloatingSite";
            this.colIsFloatingSite.Name = "colIsFloatingSite";
            this.colIsFloatingSite.OptionsColumn.AllowEdit = false;
            this.colIsFloatingSite.OptionsColumn.AllowFocus = false;
            this.colIsFloatingSite.OptionsColumn.ReadOnly = true;
            this.colIsFloatingSite.OptionsFilter.AllowFilter = false;
            this.colIsFloatingSite.Visible = true;
            this.colIsFloatingSite.VisibleIndex = 7;
            this.colIsFloatingSite.Width = 92;
            // 
            // colSiteAddress1
            // 
            this.colSiteAddress1.Caption = "Site Address";
            this.colSiteAddress1.FieldName = "SiteAddress1";
            this.colSiteAddress1.Name = "colSiteAddress1";
            this.colSiteAddress1.OptionsColumn.AllowEdit = false;
            this.colSiteAddress1.OptionsColumn.AllowFocus = false;
            this.colSiteAddress1.OptionsColumn.ReadOnly = true;
            this.colSiteAddress1.Visible = true;
            this.colSiteAddress1.VisibleIndex = 12;
            this.colSiteAddress1.Width = 81;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Visible = true;
            this.colSitePostcode.VisibleIndex = 13;
            this.colSitePostcode.Width = 86;
            // 
            // colSiteLat
            // 
            this.colSiteLat.Caption = "Site Latitude";
            this.colSiteLat.FieldName = "SiteLat";
            this.colSiteLat.Name = "colSiteLat";
            this.colSiteLat.OptionsColumn.AllowEdit = false;
            this.colSiteLat.OptionsColumn.AllowFocus = false;
            this.colSiteLat.OptionsColumn.ReadOnly = true;
            this.colSiteLat.Visible = true;
            this.colSiteLat.VisibleIndex = 14;
            this.colSiteLat.Width = 81;
            // 
            // colSiteLong
            // 
            this.colSiteLong.Caption = "Site Longitude";
            this.colSiteLong.FieldName = "SiteLong";
            this.colSiteLong.Name = "colSiteLong";
            this.colSiteLong.Visible = true;
            this.colSiteLong.VisibleIndex = 15;
            this.colSiteLong.Width = 89;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 9;
            // 
            // colDoubleGrit
            // 
            this.colDoubleGrit.Caption = "Double Grit";
            this.colDoubleGrit.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoubleGrit.FieldName = "DoubleGrit";
            this.colDoubleGrit.Name = "colDoubleGrit";
            this.colDoubleGrit.OptionsColumn.AllowEdit = false;
            this.colDoubleGrit.OptionsColumn.AllowFocus = false;
            this.colDoubleGrit.OptionsColumn.ReadOnly = true;
            this.colDoubleGrit.Visible = true;
            this.colDoubleGrit.VisibleIndex = 16;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Width = 60;
            // 
            // colKMLFile
            // 
            this.colKMLFile.Caption = "KML File";
            this.colKMLFile.ColumnEdit = this.repositoryItemHyperLinkEditLinkedFile;
            this.colKMLFile.FieldName = "KMLFile";
            this.colKMLFile.Name = "colKMLFile";
            this.colKMLFile.OptionsColumn.ReadOnly = true;
            this.colKMLFile.Visible = true;
            this.colKMLFile.VisibleIndex = 82;
            this.colKMLFile.Width = 84;
            // 
            // colKMLImageFile
            // 
            this.colKMLImageFile.Caption = "KML Image File";
            this.colKMLImageFile.ColumnEdit = this.repositoryItemHyperLinkEditLinkedFile;
            this.colKMLImageFile.FieldName = "KMLImageFile";
            this.colKMLImageFile.Name = "colKMLImageFile";
            this.colKMLImageFile.OptionsColumn.ReadOnly = true;
            this.colKMLImageFile.Visible = true;
            this.colKMLImageFile.VisibleIndex = 83;
            this.colKMLImageFile.Width = 90;
            // 
            // colCompletionSheetFile
            // 
            this.colCompletionSheetFile.Caption = "Completion Sheet";
            this.colCompletionSheetFile.ColumnEdit = this.repositoryItemHyperLinkEditLinkedFile;
            this.colCompletionSheetFile.FieldName = "CompletionSheetFile";
            this.colCompletionSheetFile.Name = "colCompletionSheetFile";
            this.colCompletionSheetFile.OptionsColumn.ReadOnly = true;
            this.colCompletionSheetFile.Visible = true;
            this.colCompletionSheetFile.VisibleIndex = 84;
            this.colCompletionSheetFile.Width = 103;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlCompanies);
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlLinkedGrittingCalloutsFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Text = "GRITTING Callouts";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1537, 537);
            this.splitContainerControl1.SplitterPosition = 144;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // popupContainerControlCompanies
            // 
            this.popupContainerControlCompanies.Controls.Add(this.btnCompanyFilterOK);
            this.popupContainerControlCompanies.Controls.Add(this.gridControl5);
            this.popupContainerControlCompanies.Location = new System.Drawing.Point(395, 130);
            this.popupContainerControlCompanies.Name = "popupContainerControlCompanies";
            this.popupContainerControlCompanies.Size = new System.Drawing.Size(189, 163);
            this.popupContainerControlCompanies.TabIndex = 15;
            // 
            // btnCompanyFilterOK
            // 
            this.btnCompanyFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCompanyFilterOK.Location = new System.Drawing.Point(3, 138);
            this.btnCompanyFilterOK.Name = "btnCompanyFilterOK";
            this.btnCompanyFilterOK.Size = new System.Drawing.Size(77, 22);
            this.btnCompanyFilterOK.TabIndex = 18;
            this.btnCompanyFilterOK.Text = "OK";
            this.btnCompanyFilterOK.Click += new System.EventHandler(this.btnCompanyFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp04237GCCompanyFilterListBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(183, 133);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04237GCCompanyFilterListBindingSource
            // 
            this.sp04237GCCompanyFilterListBindingSource.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.colCompanyCode,
            this.colCompanyOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Company ID";
            this.gridColumn5.FieldName = "CompanyID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 80;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Company Name";
            this.gridColumn6.FieldName = "CompanyName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 152;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyOrder
            // 
            this.colCompanyOrder.Caption = "Order";
            this.colCompanyOrder.FieldName = "CompanyOrder";
            this.colCompanyOrder.Name = "colCompanyOrder";
            this.colCompanyOrder.OptionsColumn.AllowEdit = false;
            this.colCompanyOrder.OptionsColumn.AllowFocus = false;
            this.colCompanyOrder.OptionsColumn.ReadOnly = true;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1533, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // popupContainerControlLinkedGrittingCalloutsFilter
            // 
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.btnGritCalloutFilterOK);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.gridControl3);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Location = new System.Drawing.Point(70, 62);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Name = "popupContainerControlLinkedGrittingCalloutsFilter";
            this.popupContainerControlLinkedGrittingCalloutsFilter.Size = new System.Drawing.Size(289, 220);
            this.popupContainerControlLinkedGrittingCalloutsFilter.TabIndex = 5;
            // 
            // btnGritCalloutFilterOK
            // 
            this.btnGritCalloutFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGritCalloutFilterOK.Location = new System.Drawing.Point(3, 194);
            this.btnGritCalloutFilterOK.Name = "btnGritCalloutFilterOK";
            this.btnGritCalloutFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnGritCalloutFilterOK.TabIndex = 12;
            this.btnGritCalloutFilterOK.Text = "OK";
            this.btnGritCalloutFilterOK.Click += new System.EventHandler(this.btnGritCalloutFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp04001GCJobCallOutStatusesBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(283, 189);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp04001GCJobCallOutStatusesBindingSource
            // 
            this.sp04001GCJobCallOutStatusesBindingSource.DataMember = "sp04001_GC_Job_CallOut_Statuses";
            this.sp04001GCJobCallOutStatusesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colValue,
            this.colOrder});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Callout Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 263;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(-1, 43);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1533, 340);
            this.gridSplitContainer1.TabIndex = 17;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(1537, 144);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage4,
            this.xtraTabPage1,
            this.xtraTabPage3});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1532, 115);
            this.xtraTabPage2.Text = "Linked Extra Costs";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1532, 115);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp04075GCGrittingCalloutLinkedExtraCostsBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit2DP2,
            this.repositoryItemTextEditHours2DP});
            this.gridControl2.Size = new System.Drawing.Size(1532, 115);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp04075GCGrittingCalloutLinkedExtraCostsBindingSource
            // 
            this.sp04075GCGrittingCalloutLinkedExtraCostsBindingSource.DataMember = "sp04075_GC_Gritting_Callout_Linked_Extra_Costs";
            this.sp04075GCGrittingCalloutLinkedExtraCostsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colExtraCostID,
            this.colGrittingCallOutID1,
            this.colCostTypeID,
            this.colDescription,
            this.colCost,
            this.colSell,
            this.colVatRate,
            this.colRemarks,
            this.colCreatedFromDefault,
            this.colCostTypeDescription,
            this.colSiteGrittingContractID1,
            this.colSiteName1,
            this.colClientName1,
            this.colSubContractorID1,
            this.colTeamName,
            this.colTeamAddressLine1,
            this.colCalloutDescription,
            this.colCallOutDateTime1,
            this.colChargedPerHour,
            this.colNumberOfUnits,
            this.colTotalCost1,
            this.colTotalSell1,
            this.colSnowOnSite1,
            this.colHours,
            this.colGrittingTimeToDeduct});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCalloutDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colExtraCostID
            // 
            this.colExtraCostID.Caption = "Extra Cost ID";
            this.colExtraCostID.FieldName = "ExtraCostID";
            this.colExtraCostID.Name = "colExtraCostID";
            this.colExtraCostID.OptionsColumn.AllowEdit = false;
            this.colExtraCostID.OptionsColumn.AllowFocus = false;
            this.colExtraCostID.OptionsColumn.ReadOnly = true;
            this.colExtraCostID.Width = 86;
            // 
            // colGrittingCallOutID1
            // 
            this.colGrittingCallOutID1.Caption = "Gritting Callout ID";
            this.colGrittingCallOutID1.FieldName = "GrittingCallOutID";
            this.colGrittingCallOutID1.Name = "colGrittingCallOutID1";
            this.colGrittingCallOutID1.OptionsColumn.AllowEdit = false;
            this.colGrittingCallOutID1.OptionsColumn.AllowFocus = false;
            this.colGrittingCallOutID1.OptionsColumn.ReadOnly = true;
            this.colGrittingCallOutID1.Width = 106;
            // 
            // colCostTypeID
            // 
            this.colCostTypeID.Caption = "Cost Type ID";
            this.colCostTypeID.FieldName = "CostTypeID";
            this.colCostTypeID.Name = "colCostTypeID";
            this.colCostTypeID.OptionsColumn.AllowEdit = false;
            this.colCostTypeID.OptionsColumn.AllowFocus = false;
            this.colCostTypeID.OptionsColumn.ReadOnly = true;
            this.colCostTypeID.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 207;
            // 
            // colCost
            // 
            this.colCost.Caption = "Unit Cost";
            this.colCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colCost.FieldName = "Cost";
            this.colCost.Name = "colCost";
            this.colCost.OptionsColumn.AllowEdit = false;
            this.colCost.OptionsColumn.AllowFocus = false;
            this.colCost.OptionsColumn.ReadOnly = true;
            this.colCost.Visible = true;
            this.colCost.VisibleIndex = 6;
            this.colCost.Width = 65;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colSell
            // 
            this.colSell.Caption = "Unit Sell";
            this.colSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colSell.FieldName = "Sell";
            this.colSell.Name = "colSell";
            this.colSell.OptionsColumn.AllowEdit = false;
            this.colSell.OptionsColumn.AllowFocus = false;
            this.colSell.OptionsColumn.ReadOnly = true;
            this.colSell.Visible = true;
            this.colSell.VisibleIndex = 7;
            this.colSell.Width = 59;
            // 
            // colVatRate
            // 
            this.colVatRate.Caption = "VAT Rate";
            this.colVatRate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colVatRate.FieldName = "VatRate";
            this.colVatRate.Name = "colVatRate";
            this.colVatRate.OptionsColumn.AllowEdit = false;
            this.colVatRate.OptionsColumn.AllowFocus = false;
            this.colVatRate.OptionsColumn.ReadOnly = true;
            this.colVatRate.Visible = true;
            this.colVatRate.VisibleIndex = 2;
            this.colVatRate.Width = 69;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "P";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colCreatedFromDefault
            // 
            this.colCreatedFromDefault.Caption = "System Generated";
            this.colCreatedFromDefault.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colCreatedFromDefault.FieldName = "CreatedFromDefault";
            this.colCreatedFromDefault.Name = "colCreatedFromDefault";
            this.colCreatedFromDefault.OptionsColumn.AllowEdit = false;
            this.colCreatedFromDefault.OptionsColumn.AllowFocus = false;
            this.colCreatedFromDefault.OptionsColumn.ReadOnly = true;
            this.colCreatedFromDefault.Visible = true;
            this.colCreatedFromDefault.VisibleIndex = 12;
            this.colCreatedFromDefault.Width = 110;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colCostTypeDescription
            // 
            this.colCostTypeDescription.Caption = "Cost Type";
            this.colCostTypeDescription.FieldName = "CostTypeDescription";
            this.colCostTypeDescription.Name = "colCostTypeDescription";
            this.colCostTypeDescription.OptionsColumn.AllowEdit = false;
            this.colCostTypeDescription.OptionsColumn.AllowFocus = false;
            this.colCostTypeDescription.OptionsColumn.ReadOnly = true;
            this.colCostTypeDescription.Visible = true;
            this.colCostTypeDescription.VisibleIndex = 1;
            this.colCostTypeDescription.Width = 169;
            // 
            // colSiteGrittingContractID1
            // 
            this.colSiteGrittingContractID1.Caption = "Site Gritting Contract";
            this.colSiteGrittingContractID1.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID1.Name = "colSiteGrittingContractID1";
            this.colSiteGrittingContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID1.Width = 122;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 78;
            // 
            // colSubContractorID1
            // 
            this.colSubContractorID1.Caption = "Team ID";
            this.colSubContractorID1.FieldName = "SubContractorID";
            this.colSubContractorID1.Name = "colSubContractorID1";
            this.colSubContractorID1.OptionsColumn.AllowEdit = false;
            this.colSubContractorID1.OptionsColumn.AllowFocus = false;
            this.colSubContractorID1.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName
            // 
            this.colTeamName.Caption = "Team Name";
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.Visible = true;
            this.colTeamName.VisibleIndex = 14;
            this.colTeamName.Width = 179;
            // 
            // colTeamAddressLine1
            // 
            this.colTeamAddressLine1.Caption = "Team Address Line 1";
            this.colTeamAddressLine1.FieldName = "TeamAddressLine1";
            this.colTeamAddressLine1.Name = "colTeamAddressLine1";
            this.colTeamAddressLine1.OptionsColumn.AllowEdit = false;
            this.colTeamAddressLine1.OptionsColumn.AllowFocus = false;
            this.colTeamAddressLine1.OptionsColumn.ReadOnly = true;
            this.colTeamAddressLine1.Width = 120;
            // 
            // colCalloutDescription
            // 
            this.colCalloutDescription.Caption = "Callout Description";
            this.colCalloutDescription.FieldName = "CalloutDescription";
            this.colCalloutDescription.Name = "colCalloutDescription";
            this.colCalloutDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutDescription.Width = 291;
            // 
            // colCallOutDateTime1
            // 
            this.colCallOutDateTime1.Caption = "Callout Date\\Time";
            this.colCallOutDateTime1.ColumnEdit = this.repositoryItemTextEdit2;
            this.colCallOutDateTime1.FieldName = "CallOutDateTime";
            this.colCallOutDateTime1.Name = "colCallOutDateTime1";
            this.colCallOutDateTime1.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime1.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime1.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime1.Visible = true;
            this.colCallOutDateTime1.VisibleIndex = 15;
            this.colCallOutDateTime1.Width = 106;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colChargedPerHour
            // 
            this.colChargedPerHour.Caption = "Charged Per Hour";
            this.colChargedPerHour.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colChargedPerHour.FieldName = "ChargedPerHour";
            this.colChargedPerHour.Name = "colChargedPerHour";
            this.colChargedPerHour.Visible = true;
            this.colChargedPerHour.VisibleIndex = 3;
            this.colChargedPerHour.Width = 107;
            // 
            // colNumberOfUnits
            // 
            this.colNumberOfUnits.Caption = "Number of Units";
            this.colNumberOfUnits.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colNumberOfUnits.FieldName = "NumberOfUnits";
            this.colNumberOfUnits.Name = "colNumberOfUnits";
            this.colNumberOfUnits.Visible = true;
            this.colNumberOfUnits.VisibleIndex = 8;
            this.colNumberOfUnits.Width = 98;
            // 
            // repositoryItemTextEdit2DP2
            // 
            this.repositoryItemTextEdit2DP2.AutoHeight = false;
            this.repositoryItemTextEdit2DP2.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP2.Name = "repositoryItemTextEdit2DP2";
            // 
            // colTotalCost1
            // 
            this.colTotalCost1.Caption = "Total Cost";
            this.colTotalCost1.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colTotalCost1.FieldName = "TotalCost";
            this.colTotalCost1.Name = "colTotalCost1";
            this.colTotalCost1.Visible = true;
            this.colTotalCost1.VisibleIndex = 9;
            // 
            // colTotalSell1
            // 
            this.colTotalSell1.Caption = "Total Sell";
            this.colTotalSell1.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colTotalSell1.FieldName = "TotalSell";
            this.colTotalSell1.Name = "colTotalSell1";
            this.colTotalSell1.Visible = true;
            this.colTotalSell1.VisibleIndex = 10;
            // 
            // colSnowOnSite1
            // 
            this.colSnowOnSite1.Caption = "Snow On Site";
            this.colSnowOnSite1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSnowOnSite1.FieldName = "SnowOnSite";
            this.colSnowOnSite1.Name = "colSnowOnSite1";
            this.colSnowOnSite1.Visible = true;
            this.colSnowOnSite1.VisibleIndex = 13;
            this.colSnowOnSite1.Width = 85;
            // 
            // colHours
            // 
            this.colHours.Caption = "Hours";
            this.colHours.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colHours.FieldName = "Hours";
            this.colHours.Name = "colHours";
            this.colHours.OptionsColumn.AllowEdit = false;
            this.colHours.OptionsColumn.AllowFocus = false;
            this.colHours.OptionsColumn.ReadOnly = true;
            this.colHours.Visible = true;
            this.colHours.VisibleIndex = 4;
            this.colHours.Width = 49;
            // 
            // colGrittingTimeToDeduct
            // 
            this.colGrittingTimeToDeduct.Caption = "Gritting Time to Deduct";
            this.colGrittingTimeToDeduct.ColumnEdit = this.repositoryItemTextEditHours2DP;
            this.colGrittingTimeToDeduct.FieldName = "GrittingTimeToDeduct";
            this.colGrittingTimeToDeduct.Name = "colGrittingTimeToDeduct";
            this.colGrittingTimeToDeduct.OptionsColumn.AllowEdit = false;
            this.colGrittingTimeToDeduct.OptionsColumn.AllowFocus = false;
            this.colGrittingTimeToDeduct.OptionsColumn.ReadOnly = true;
            this.colGrittingTimeToDeduct.Visible = true;
            this.colGrittingTimeToDeduct.VisibleIndex = 5;
            this.colGrittingTimeToDeduct.Width = 131;
            // 
            // repositoryItemTextEditHours2DP
            // 
            this.repositoryItemTextEditHours2DP.AutoHeight = false;
            this.repositoryItemTextEditHours2DP.Mask.EditMask = "##0.00 Hours";
            this.repositoryItemTextEditHours2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours2DP.Name = "repositoryItemTextEditHours2DP";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridControl6);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1532, 115);
            this.xtraTabPage4.Text = "Linked Serviced Areas";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp04337GCServiceAreasLinkedToGrittingCalloutBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3});
            this.gridControl6.Size = new System.Drawing.Size(1532, 115);
            this.gridControl6.TabIndex = 1;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp04337GCServiceAreasLinkedToGrittingCalloutBindingSource
            // 
            this.sp04337GCServiceAreasLinkedToGrittingCalloutBindingSource.DataMember = "sp04337_GC_Service_Areas_Linked_To_Gritting_Callout";
            this.sp04337GCServiceAreasLinkedToGrittingCalloutBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGrittingServicedSiteID,
            this.colGrittingCalloutID3,
            this.colSiteServiceAreaID,
            this.colDescriptionID,
            this.colOrderID,
            this.colWasServiced,
            this.colCreatedFromDefault1,
            this.colCalloutDescription2,
            this.colAreaDescription,
            this.colGroupID});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 1;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsLayout.StoreFormatRules = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCalloutDescription2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrderID, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAreaDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colGrittingServicedSiteID
            // 
            this.colGrittingServicedSiteID.Caption = "Gritting Serviced Area";
            this.colGrittingServicedSiteID.FieldName = "GrittingServicedSiteID";
            this.colGrittingServicedSiteID.Name = "colGrittingServicedSiteID";
            this.colGrittingServicedSiteID.OptionsColumn.AllowEdit = false;
            this.colGrittingServicedSiteID.OptionsColumn.AllowFocus = false;
            this.colGrittingServicedSiteID.OptionsColumn.ReadOnly = true;
            this.colGrittingServicedSiteID.Width = 126;
            // 
            // colGrittingCalloutID3
            // 
            this.colGrittingCalloutID3.Caption = "Gritting Callout ID";
            this.colGrittingCalloutID3.FieldName = "GrittingCalloutID";
            this.colGrittingCalloutID3.Name = "colGrittingCalloutID3";
            this.colGrittingCalloutID3.OptionsColumn.AllowEdit = false;
            this.colGrittingCalloutID3.OptionsColumn.AllowFocus = false;
            this.colGrittingCalloutID3.OptionsColumn.ReadOnly = true;
            this.colGrittingCalloutID3.Width = 106;
            // 
            // colSiteServiceAreaID
            // 
            this.colSiteServiceAreaID.Caption = "Site Service Area";
            this.colSiteServiceAreaID.FieldName = "SiteServiceAreaID";
            this.colSiteServiceAreaID.Name = "colSiteServiceAreaID";
            this.colSiteServiceAreaID.OptionsColumn.AllowEdit = false;
            this.colSiteServiceAreaID.OptionsColumn.AllowFocus = false;
            this.colSiteServiceAreaID.OptionsColumn.ReadOnly = true;
            this.colSiteServiceAreaID.Width = 103;
            // 
            // colDescriptionID
            // 
            this.colDescriptionID.Caption = "Description ID";
            this.colDescriptionID.FieldName = "DescriptionID";
            this.colDescriptionID.Name = "colDescriptionID";
            this.colDescriptionID.OptionsColumn.AllowEdit = false;
            this.colDescriptionID.OptionsColumn.AllowFocus = false;
            this.colDescriptionID.OptionsColumn.ReadOnly = true;
            this.colDescriptionID.Width = 88;
            // 
            // colOrderID
            // 
            this.colOrderID.Caption = "Order";
            this.colOrderID.FieldName = "OrderID";
            this.colOrderID.Name = "colOrderID";
            this.colOrderID.OptionsColumn.AllowEdit = false;
            this.colOrderID.OptionsColumn.AllowFocus = false;
            this.colOrderID.OptionsColumn.ReadOnly = true;
            this.colOrderID.Visible = true;
            this.colOrderID.VisibleIndex = 4;
            this.colOrderID.Width = 64;
            // 
            // colWasServiced
            // 
            this.colWasServiced.Caption = "Was Serviced";
            this.colWasServiced.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colWasServiced.FieldName = "WasServiced";
            this.colWasServiced.Name = "colWasServiced";
            this.colWasServiced.OptionsColumn.AllowEdit = false;
            this.colWasServiced.OptionsColumn.AllowFocus = false;
            this.colWasServiced.OptionsColumn.ReadOnly = true;
            this.colWasServiced.Visible = true;
            this.colWasServiced.VisibleIndex = 1;
            this.colWasServiced.Width = 86;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colCreatedFromDefault1
            // 
            this.colCreatedFromDefault1.Caption = "Created From Default";
            this.colCreatedFromDefault1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCreatedFromDefault1.FieldName = "CreatedFromDefault";
            this.colCreatedFromDefault1.Name = "colCreatedFromDefault1";
            this.colCreatedFromDefault1.OptionsColumn.AllowEdit = false;
            this.colCreatedFromDefault1.OptionsColumn.AllowFocus = false;
            this.colCreatedFromDefault1.OptionsColumn.ReadOnly = true;
            this.colCreatedFromDefault1.Visible = true;
            this.colCreatedFromDefault1.VisibleIndex = 2;
            this.colCreatedFromDefault1.Width = 125;
            // 
            // colCalloutDescription2
            // 
            this.colCalloutDescription2.Caption = "Callout Description";
            this.colCalloutDescription2.FieldName = "CalloutDescription";
            this.colCalloutDescription2.Name = "colCalloutDescription2";
            this.colCalloutDescription2.OptionsColumn.AllowEdit = false;
            this.colCalloutDescription2.OptionsColumn.AllowFocus = false;
            this.colCalloutDescription2.OptionsColumn.ReadOnly = true;
            this.colCalloutDescription2.Visible = true;
            this.colCalloutDescription2.VisibleIndex = 7;
            this.colCalloutDescription2.Width = 392;
            // 
            // colAreaDescription
            // 
            this.colAreaDescription.Caption = "Service Area";
            this.colAreaDescription.FieldName = "AreaDescription";
            this.colAreaDescription.Name = "colAreaDescription";
            this.colAreaDescription.OptionsColumn.AllowEdit = false;
            this.colAreaDescription.OptionsColumn.AllowFocus = false;
            this.colAreaDescription.OptionsColumn.ReadOnly = true;
            this.colAreaDescription.Visible = true;
            this.colAreaDescription.VisibleIndex = 0;
            this.colAreaDescription.Width = 334;
            // 
            // colGroupID
            // 
            this.colGroupID.Caption = "Group ID";
            this.colGroupID.FieldName = "GroupID";
            this.colGroupID.Name = "colGroupID";
            this.colGroupID.OptionsColumn.AllowEdit = false;
            this.colGroupID.OptionsColumn.AllowFocus = false;
            this.colGroupID.OptionsColumn.ReadOnly = true;
            this.colGroupID.Visible = true;
            this.colGroupID.VisibleIndex = 3;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1532, 115);
            this.xtraTabPage1.Text = "Linked Pictures";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl4;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1532, 115);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(1532, 115);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp04092GCGrittingCalloutPicturesForCalloutBindingSource
            // 
            this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource.DataMember = "sp04092_GC_Gritting_Callout_Pictures_For_Callout";
            this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPictureID,
            this.colGrittingCallOutID2,
            this.colPicturePath,
            this.colDateTimeTaken,
            this.colRemarks1,
            this.colSiteGrittingContractID2,
            this.colSiteName2,
            this.colClientName2,
            this.colSubContractorID2,
            this.colTeamName1,
            this.colTeamAddressLine11,
            this.colCalloutDescription1,
            this.colCallOutDateTime2});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCalloutDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTimeTaken, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView4.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView4_CustomRowCellEdit);
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView4_ShowingEditor);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colPictureID
            // 
            this.colPictureID.Caption = "Picture ID";
            this.colPictureID.FieldName = "PictureID";
            this.colPictureID.Name = "colPictureID";
            this.colPictureID.OptionsColumn.AllowEdit = false;
            this.colPictureID.OptionsColumn.AllowFocus = false;
            this.colPictureID.OptionsColumn.ReadOnly = true;
            // 
            // colGrittingCallOutID2
            // 
            this.colGrittingCallOutID2.Caption = "Callout ID";
            this.colGrittingCallOutID2.FieldName = "GrittingCallOutID";
            this.colGrittingCallOutID2.Name = "colGrittingCallOutID2";
            this.colGrittingCallOutID2.OptionsColumn.AllowEdit = false;
            this.colGrittingCallOutID2.OptionsColumn.AllowFocus = false;
            this.colGrittingCallOutID2.OptionsColumn.ReadOnly = true;
            // 
            // colPicturePath
            // 
            this.colPicturePath.Caption = "Picture";
            this.colPicturePath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colPicturePath.FieldName = "PicturePath";
            this.colPicturePath.Name = "colPicturePath";
            this.colPicturePath.OptionsColumn.ReadOnly = true;
            this.colPicturePath.Visible = true;
            this.colPicturePath.VisibleIndex = 1;
            this.colPicturePath.Width = 469;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDateTimeTaken
            // 
            this.colDateTimeTaken.Caption = "Date\\Time Taken";
            this.colDateTimeTaken.ColumnEdit = this.repositoryItemTextEdit4;
            this.colDateTimeTaken.FieldName = "DateTimeTaken";
            this.colDateTimeTaken.Name = "colDateTimeTaken";
            this.colDateTimeTaken.OptionsColumn.AllowEdit = false;
            this.colDateTimeTaken.OptionsColumn.AllowFocus = false;
            this.colDateTimeTaken.OptionsColumn.ReadOnly = true;
            this.colDateTimeTaken.Visible = true;
            this.colDateTimeTaken.VisibleIndex = 0;
            this.colDateTimeTaken.Width = 119;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 2;
            this.colRemarks1.Width = 161;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colSiteGrittingContractID2
            // 
            this.colSiteGrittingContractID2.Caption = "Site Gritting Contract ID";
            this.colSiteGrittingContractID2.FieldName = "SiteGrittingContractID";
            this.colSiteGrittingContractID2.Name = "colSiteGrittingContractID2";
            this.colSiteGrittingContractID2.OptionsColumn.AllowEdit = false;
            this.colSiteGrittingContractID2.OptionsColumn.AllowFocus = false;
            this.colSiteGrittingContractID2.OptionsColumn.ReadOnly = true;
            this.colSiteGrittingContractID2.Width = 136;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 78;
            // 
            // colSubContractorID2
            // 
            this.colSubContractorID2.Caption = "Team ID";
            this.colSubContractorID2.FieldName = "SubContractorID";
            this.colSubContractorID2.Name = "colSubContractorID2";
            this.colSubContractorID2.OptionsColumn.AllowEdit = false;
            this.colSubContractorID2.OptionsColumn.AllowFocus = false;
            this.colSubContractorID2.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName1
            // 
            this.colTeamName1.Caption = "Team Name";
            this.colTeamName1.FieldName = "TeamName";
            this.colTeamName1.Name = "colTeamName1";
            this.colTeamName1.OptionsColumn.AllowEdit = false;
            this.colTeamName1.OptionsColumn.AllowFocus = false;
            this.colTeamName1.OptionsColumn.ReadOnly = true;
            this.colTeamName1.Width = 77;
            // 
            // colTeamAddressLine11
            // 
            this.colTeamAddressLine11.Caption = "Team Address Line 1";
            this.colTeamAddressLine11.FieldName = "TeamAddressLine1";
            this.colTeamAddressLine11.Name = "colTeamAddressLine11";
            this.colTeamAddressLine11.OptionsColumn.AllowEdit = false;
            this.colTeamAddressLine11.OptionsColumn.AllowFocus = false;
            this.colTeamAddressLine11.OptionsColumn.ReadOnly = true;
            this.colTeamAddressLine11.Width = 120;
            // 
            // colCalloutDescription1
            // 
            this.colCalloutDescription1.Caption = "Callout Description";
            this.colCalloutDescription1.FieldName = "CalloutDescription";
            this.colCalloutDescription1.Name = "colCalloutDescription1";
            this.colCalloutDescription1.OptionsColumn.AllowEdit = false;
            this.colCalloutDescription1.OptionsColumn.AllowFocus = false;
            this.colCalloutDescription1.OptionsColumn.ReadOnly = true;
            // 
            // colCallOutDateTime2
            // 
            this.colCallOutDateTime2.Caption = "Callout Time";
            this.colCallOutDateTime2.ColumnEdit = this.repositoryItemTextEdit3;
            this.colCallOutDateTime2.FieldName = "CallOutDateTime";
            this.colCallOutDateTime2.Name = "colCallOutDateTime2";
            this.colCallOutDateTime2.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime2.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime2.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime2.Width = 79;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridSplitContainer4);
            this.xtraTabPage3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage3.ImageOptions.Image")));
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1532, 115);
            this.xtraTabPage3.Text = "CRM";
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControl15;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControl15);
            this.gridSplitContainer4.Size = new System.Drawing.Size(1532, 115);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControl15
            // 
            this.gridControl15.DataSource = this.sp05089CRMContactsLinkedToRecordBindingSource;
            this.gridControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl15.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl15.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl15.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl15.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl15_EmbeddedNavigator_ButtonClick);
            this.gridControl15.Location = new System.Drawing.Point(0, 0);
            this.gridControl15.MainView = this.gridView15;
            this.gridControl15.MenuManager = this.barManager1;
            this.gridControl15.Name = "gridControl15";
            this.gridControl15.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit12,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextDateTime});
            this.gridControl15.Size = new System.Drawing.Size(1532, 115);
            this.gridControl15.TabIndex = 6;
            this.gridControl15.UseEmbeddedNavigator = true;
            this.gridControl15.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView15});
            // 
            // sp05089CRMContactsLinkedToRecordBindingSource
            // 
            this.sp05089CRMContactsLinkedToRecordBindingSource.DataMember = "sp05089_CRM_Contacts_Linked_To_Record";
            this.sp05089CRMContactsLinkedToRecordBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCRMID,
            this.gridColumn7,
            this.colClientContactID,
            this.colLinkedToRecordTypeID1,
            this.colLinkedToRecordID1,
            this.colContactDueDateTime,
            this.colContactMadeDateTime,
            this.colContactMethodID,
            this.colContactTypeID,
            this.gridColumn8,
            this.gridColumn9,
            this.colStatusID,
            this.colCreatedByStaffID,
            this.colContactedByStaffID,
            this.colContactDirectionID,
            this.colLinkedRecordDescription1,
            this.colCreatedByStaffName,
            this.colContactedByStaffName,
            this.colContactMethod,
            this.colContactType,
            this.colStatusDescription,
            this.colContactDirectionDescription,
            this.gridColumn10,
            this.colLinkedRecordTypeDescription,
            this.colDateCreated1,
            this.colClientContact});
            this.gridView15.GridControl = this.gridControl15;
            this.gridView15.GroupCount = 1;
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView15.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView15.OptionsLayout.StoreAppearance = true;
            this.gridView15.OptionsLayout.StoreFormatRules = true;
            this.gridView15.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView15.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView15.OptionsSelection.MultiSelect = true;
            this.gridView15.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView15.OptionsView.ColumnAutoWidth = false;
            this.gridView15.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            this.gridView15.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateCreated1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView15.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView15.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView15.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView15.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView15.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView15.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView15_MouseUp);
            this.gridView15.DoubleClick += new System.EventHandler(this.gridView15_DoubleClick);
            this.gridView15.GotFocus += new System.EventHandler(this.gridView15_GotFocus);
            // 
            // colCRMID
            // 
            this.colCRMID.Caption = "CRM ID";
            this.colCRMID.FieldName = "CRMID";
            this.colCRMID.Name = "colCRMID";
            this.colCRMID.OptionsColumn.AllowEdit = false;
            this.colCRMID.OptionsColumn.AllowFocus = false;
            this.colCRMID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Client ID";
            this.gridColumn7.FieldName = "ClientID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // colClientContactID
            // 
            this.colClientContactID.Caption = "Client Contact ID";
            this.colClientContactID.FieldName = "ClientContactID";
            this.colClientContactID.Name = "colClientContactID";
            this.colClientContactID.OptionsColumn.AllowEdit = false;
            this.colClientContactID.OptionsColumn.AllowFocus = false;
            this.colClientContactID.OptionsColumn.ReadOnly = true;
            this.colClientContactID.Width = 100;
            // 
            // colLinkedToRecordTypeID1
            // 
            this.colLinkedToRecordTypeID1.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID1.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID1.Name = "colLinkedToRecordTypeID1";
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID1.Width = 141;
            // 
            // colLinkedToRecordID1
            // 
            this.colLinkedToRecordID1.Caption = "Linked To Record ID";
            this.colLinkedToRecordID1.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID1.Name = "colLinkedToRecordID1";
            this.colLinkedToRecordID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID1.Width = 114;
            // 
            // colContactDueDateTime
            // 
            this.colContactDueDateTime.Caption = "Contact Due";
            this.colContactDueDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactDueDateTime.FieldName = "ContactDueDateTime";
            this.colContactDueDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactDueDateTime.Name = "colContactDueDateTime";
            this.colContactDueDateTime.OptionsColumn.AllowEdit = false;
            this.colContactDueDateTime.OptionsColumn.AllowFocus = false;
            this.colContactDueDateTime.OptionsColumn.ReadOnly = true;
            this.colContactDueDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactDueDateTime.Visible = true;
            this.colContactDueDateTime.VisibleIndex = 3;
            this.colContactDueDateTime.Width = 108;
            // 
            // repositoryItemTextDateTime
            // 
            this.repositoryItemTextDateTime.AutoHeight = false;
            this.repositoryItemTextDateTime.Mask.EditMask = "g";
            this.repositoryItemTextDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextDateTime.Name = "repositoryItemTextDateTime";
            // 
            // colContactMadeDateTime
            // 
            this.colContactMadeDateTime.Caption = "Contact Made";
            this.colContactMadeDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactMadeDateTime.FieldName = "ContactMadeDateTime";
            this.colContactMadeDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactMadeDateTime.Name = "colContactMadeDateTime";
            this.colContactMadeDateTime.OptionsColumn.AllowEdit = false;
            this.colContactMadeDateTime.OptionsColumn.AllowFocus = false;
            this.colContactMadeDateTime.OptionsColumn.ReadOnly = true;
            this.colContactMadeDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactMadeDateTime.Visible = true;
            this.colContactMadeDateTime.VisibleIndex = 4;
            this.colContactMadeDateTime.Width = 102;
            // 
            // colContactMethodID
            // 
            this.colContactMethodID.Caption = "Contact Method ID";
            this.colContactMethodID.FieldName = "ContactMethodID";
            this.colContactMethodID.Name = "colContactMethodID";
            this.colContactMethodID.OptionsColumn.AllowEdit = false;
            this.colContactMethodID.OptionsColumn.AllowFocus = false;
            this.colContactMethodID.OptionsColumn.ReadOnly = true;
            this.colContactMethodID.Width = 109;
            // 
            // colContactTypeID
            // 
            this.colContactTypeID.Caption = "Contact Type ID";
            this.colContactTypeID.FieldName = "ContactTypeID";
            this.colContactTypeID.Name = "colContactTypeID";
            this.colContactTypeID.OptionsColumn.AllowEdit = false;
            this.colContactTypeID.OptionsColumn.AllowFocus = false;
            this.colContactTypeID.OptionsColumn.ReadOnly = true;
            this.colContactTypeID.Width = 97;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Description";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            this.gridColumn8.Width = 260;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Remarks";
            this.gridColumn9.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.gridColumn9.FieldName = "Remarks";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 113;
            // 
            // colContactedByStaffID
            // 
            this.colContactedByStaffID.Caption = "Contacted By";
            this.colContactedByStaffID.FieldName = "ContactedByStaffID";
            this.colContactedByStaffID.Name = "colContactedByStaffID";
            this.colContactedByStaffID.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffID.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffID.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffID.Width = 83;
            // 
            // colContactDirectionID
            // 
            this.colContactDirectionID.Caption = "Contact Direction ID";
            this.colContactDirectionID.FieldName = "ContactDirectionID";
            this.colContactDirectionID.Name = "colContactDirectionID";
            this.colContactDirectionID.OptionsColumn.AllowEdit = false;
            this.colContactDirectionID.OptionsColumn.AllowFocus = false;
            this.colContactDirectionID.OptionsColumn.ReadOnly = true;
            this.colContactDirectionID.Width = 101;
            // 
            // colLinkedRecordDescription1
            // 
            this.colLinkedRecordDescription1.Caption = "Linked Record";
            this.colLinkedRecordDescription1.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription1.Name = "colLinkedRecordDescription1";
            this.colLinkedRecordDescription1.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription1.Width = 191;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 11;
            this.colCreatedByStaffName.Width = 121;
            // 
            // colContactedByStaffName
            // 
            this.colContactedByStaffName.Caption = "GC Contact Person";
            this.colContactedByStaffName.FieldName = "ContactedByStaffName";
            this.colContactedByStaffName.Name = "colContactedByStaffName";
            this.colContactedByStaffName.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffName.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffName.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffName.Visible = true;
            this.colContactedByStaffName.VisibleIndex = 5;
            this.colContactedByStaffName.Width = 133;
            // 
            // colContactMethod
            // 
            this.colContactMethod.Caption = "Contact Method";
            this.colContactMethod.FieldName = "ContactMethod";
            this.colContactMethod.Name = "colContactMethod";
            this.colContactMethod.OptionsColumn.AllowEdit = false;
            this.colContactMethod.OptionsColumn.AllowFocus = false;
            this.colContactMethod.OptionsColumn.ReadOnly = true;
            this.colContactMethod.Visible = true;
            this.colContactMethod.VisibleIndex = 8;
            this.colContactMethod.Width = 104;
            // 
            // colContactType
            // 
            this.colContactType.Caption = "Contact Type";
            this.colContactType.FieldName = "ContactType";
            this.colContactType.Name = "colContactType";
            this.colContactType.OptionsColumn.AllowEdit = false;
            this.colContactType.OptionsColumn.AllowFocus = false;
            this.colContactType.OptionsColumn.ReadOnly = true;
            this.colContactType.Visible = true;
            this.colContactType.VisibleIndex = 9;
            this.colContactType.Width = 102;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 1;
            // 
            // colContactDirectionDescription
            // 
            this.colContactDirectionDescription.Caption = "Contact Direction";
            this.colContactDirectionDescription.FieldName = "ContactDirectionDescription";
            this.colContactDirectionDescription.Name = "colContactDirectionDescription";
            this.colContactDirectionDescription.OptionsColumn.AllowEdit = false;
            this.colContactDirectionDescription.OptionsColumn.AllowFocus = false;
            this.colContactDirectionDescription.OptionsColumn.ReadOnly = true;
            this.colContactDirectionDescription.Visible = true;
            this.colContactDirectionDescription.VisibleIndex = 10;
            this.colContactDirectionDescription.Width = 101;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Client";
            this.gridColumn10.FieldName = "ClientName";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 204;
            // 
            // colLinkedRecordTypeDescription
            // 
            this.colLinkedRecordTypeDescription.Caption = "Linked Record Type";
            this.colLinkedRecordTypeDescription.FieldName = "LinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.Name = "colLinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordTypeDescription.Width = 112;
            // 
            // colDateCreated1
            // 
            this.colDateCreated1.Caption = "Date Created";
            this.colDateCreated1.ColumnEdit = this.repositoryItemTextDateTime;
            this.colDateCreated1.FieldName = "DateCreated";
            this.colDateCreated1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateCreated1.Name = "colDateCreated1";
            this.colDateCreated1.OptionsColumn.AllowEdit = false;
            this.colDateCreated1.OptionsColumn.AllowFocus = false;
            this.colDateCreated1.OptionsColumn.ReadOnly = true;
            this.colDateCreated1.Visible = true;
            this.colDateCreated1.VisibleIndex = 0;
            this.colDateCreated1.Width = 120;
            // 
            // colClientContact
            // 
            this.colClientContact.Caption = "Client Contact Person";
            this.colClientContact.FieldName = "ClientContact";
            this.colClientContact.Name = "colClientContact";
            this.colClientContact.OptionsColumn.AllowEdit = false;
            this.colClientContact.OptionsColumn.AllowFocus = false;
            this.colClientContact.OptionsColumn.ReadOnly = true;
            this.colClientContact.Visible = true;
            this.colClientContact.VisibleIndex = 6;
            this.colClientContact.Width = 122;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            // 
            // popupContainerEditCompanies
            // 
            this.popupContainerEditCompanies.EditValue = "No Company Filter";
            this.popupContainerEditCompanies.Location = new System.Drawing.Point(98, 117);
            this.popupContainerEditCompanies.MenuManager = this.barManager1;
            this.popupContainerEditCompanies.Name = "popupContainerEditCompanies";
            this.popupContainerEditCompanies.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditCompanies.Properties.PopupControl = this.popupContainerControlCompanies;
            this.popupContainerEditCompanies.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditCompanies.Size = new System.Drawing.Size(216, 20);
            this.popupContainerEditCompanies.StyleController = this.layoutControl2;
            this.popupContainerEditCompanies.TabIndex = 15;
            this.popupContainerEditCompanies.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditCompanies_QueryResultValue);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btnFormatData);
            this.layoutControl2.Controls.Add(this.VisitIDsMemoEdit);
            this.layoutControl2.Controls.Add(this.checkEditColourCode);
            this.layoutControl2.Controls.Add(this.btnLoad);
            this.layoutControl2.Controls.Add(this.popupContainerEditCompanies);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Controls.Add(this.popupContainerEdit2);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(982, 310, 468, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(337, 505);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnFormatData
            // 
            this.btnFormatData.Location = new System.Drawing.Point(245, 433);
            this.btnFormatData.Name = "btnFormatData";
            this.btnFormatData.Size = new System.Drawing.Size(69, 22);
            this.btnFormatData.StyleController = this.layoutControl2;
            toolTipTitleItem21.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem21.Text = "Format Data - Information";
            toolTipItem20.LeftIndent = 6;
            toolTipItem20.Text = "Click me to convert carriage returns into commas and clean up any spaces within t" +
    "he Specific Callout IDs box.";
            superToolTip21.Items.Add(toolTipTitleItem21);
            superToolTip21.Items.Add(toolTipItem20);
            this.btnFormatData.SuperTip = superToolTip21;
            this.btnFormatData.TabIndex = 17;
            this.btnFormatData.Text = "Format Data";
            this.btnFormatData.Click += new System.EventHandler(this.btnFormatData_Click);
            // 
            // VisitIDsMemoEdit
            // 
            this.VisitIDsMemoEdit.Location = new System.Drawing.Point(24, 45);
            this.VisitIDsMemoEdit.MenuManager = this.barManager1;
            this.VisitIDsMemoEdit.Name = "VisitIDsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDsMemoEdit, true);
            this.VisitIDsMemoEdit.Size = new System.Drawing.Size(290, 384);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDsMemoEdit, optionsSpelling1);
            this.VisitIDsMemoEdit.StyleController = this.layoutControl2;
            this.VisitIDsMemoEdit.TabIndex = 16;
            // 
            // checkEditColourCode
            // 
            this.checkEditColourCode.Location = new System.Drawing.Point(98, 141);
            this.checkEditColourCode.MenuManager = this.barManager1;
            this.checkEditColourCode.Name = "checkEditColourCode";
            this.checkEditColourCode.Properties.Caption = "[Tick if Yes]";
            this.checkEditColourCode.Size = new System.Drawing.Size(77, 19);
            this.checkEditColourCode.StyleController = this.layoutControl2;
            this.checkEditColourCode.TabIndex = 15;
            this.checkEditColourCode.CheckedChanged += new System.EventHandler(this.checkEditColourCode_CheckedChanged);
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnLoad.Location = new System.Drawing.Point(249, 471);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(77, 22);
            this.btnLoad.StyleController = this.layoutControl2;
            toolTipTitleItem22.Text = "Load Data - Information";
            toolTipItem21.LeftIndent = 6;
            toolTipItem21.Text = "Click me to Load Callout Data.\r\n\r\nOnly data matching the specified Filter Criteri" +
    "a (From and To Dates and Callout Status) will be loaded.";
            superToolTip22.Items.Add(toolTipTitleItem22);
            superToolTip22.Items.Add(toolTipItem21);
            this.btnLoad.SuperTip = superToolTip22;
            this.btnLoad.TabIndex = 12;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(98, 45);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip23.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem23.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem23.Appearance.Options.UseImage = true;
            toolTipTitleItem23.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem23.Text = "Clear Date - Information";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip23.Items.Add(toolTipTitleItem23);
            superToolTip23.Items.Add(toolTipItem22);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip23, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(216, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            this.dateEditFromDate.EditValueChanged += new System.EventHandler(this.dateEditFromDate_EditValueChanged);
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(98, 69);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip24.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem24.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem24.Appearance.Options.UseImage = true;
            toolTipTitleItem24.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem24.Text = "Clear Date - Information";
            toolTipItem23.LeftIndent = 6;
            toolTipItem23.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Re" +
    "cords will be included.";
            superToolTip24.Items.Add(toolTipTitleItem24);
            superToolTip24.Items.Add(toolTipItem23);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip24, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(216, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 11;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            this.dateEditToDate.EditValueChanged += new System.EventHandler(this.dateEditToDate_EditValueChanged);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Callout Status Filter";
            this.popupContainerEdit2.Location = new System.Drawing.Point(98, 93);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControlLinkedGrittingCalloutsFilter;
            this.popupContainerEdit2.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit2.Size = new System.Drawing.Size(216, 20);
            this.popupContainerEdit2.StyleController = this.layoutControl2;
            this.popupContainerEdit2.TabIndex = 13;
            this.popupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroupFilterType,
            this.layoutControlItem3,
            this.emptySpaceItem3});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(338, 505);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // tabbedControlGroupFilterType
            // 
            this.tabbedControlGroupFilterType.AllowHide = false;
            this.tabbedControlGroupFilterType.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroupFilterType.Name = "tabbedControlGroupFilterType";
            this.tabbedControlGroupFilterType.SelectedTabPage = this.layoutControlGroupFilterCriteria;
            this.tabbedControlGroupFilterType.SelectedTabPageIndex = 0;
            this.tabbedControlGroupFilterType.Size = new System.Drawing.Size(318, 459);
            this.tabbedControlGroupFilterType.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupFilterCriteria,
            this.layoutControlGroupSpecificVisitIDs});
            // 
            // layoutControlGroupFilterCriteria
            // 
            this.layoutControlGroupFilterCriteria.AllowHide = false;
            this.layoutControlGroupFilterCriteria.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroupFilterCriteria.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupFilterCriteria.Name = "layoutControlGroupFilterCriteria";
            this.layoutControlGroupFilterCriteria.Size = new System.Drawing.Size(294, 414);
            this.layoutControlGroupFilterCriteria.Text = "Filter Criteria";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDate;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem1.Text = "From Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDate;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem2.Text = "To Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.popupContainerEdit2;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem4.Text = "Callout Status:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.popupContainerEditCompanies;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem6.Text = "Company:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.checkEditColourCode;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(155, 23);
            this.layoutControlItem5.Text = "Colour code:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(71, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 119);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(294, 295);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(155, 96);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(139, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupSpecificVisitIDs
            // 
            this.layoutControlGroupSpecificVisitIDs.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForVisitIDs,
            this.ItemforFormatDataBtn,
            this.emptySpaceItem4});
            this.layoutControlGroupSpecificVisitIDs.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupSpecificVisitIDs.Name = "layoutControlGroupSpecificVisitIDs";
            this.layoutControlGroupSpecificVisitIDs.Size = new System.Drawing.Size(294, 414);
            this.layoutControlGroupSpecificVisitIDs.Text = "Specific Callout IDs";
            // 
            // ItemForVisitIDs
            // 
            this.ItemForVisitIDs.Control = this.VisitIDsMemoEdit;
            this.ItemForVisitIDs.Location = new System.Drawing.Point(0, 0);
            this.ItemForVisitIDs.Name = "ItemForVisitIDs";
            this.ItemForVisitIDs.Size = new System.Drawing.Size(294, 388);
            this.ItemForVisitIDs.Text = "Callout IDs:";
            this.ItemForVisitIDs.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForVisitIDs.TextVisible = false;
            // 
            // ItemforFormatDataBtn
            // 
            this.ItemforFormatDataBtn.Control = this.btnFormatData;
            this.ItemforFormatDataBtn.Location = new System.Drawing.Point(221, 388);
            this.ItemforFormatDataBtn.MaxSize = new System.Drawing.Size(73, 26);
            this.ItemforFormatDataBtn.MinSize = new System.Drawing.Size(73, 26);
            this.ItemforFormatDataBtn.Name = "ItemforFormatDataBtn";
            this.ItemforFormatDataBtn.Size = new System.Drawing.Size(73, 26);
            this.ItemforFormatDataBtn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemforFormatDataBtn.TextSize = new System.Drawing.Size(0, 0);
            this.ItemforFormatDataBtn.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 388);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(221, 26);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnLoad;
            this.layoutControlItem3.Location = new System.Drawing.Point(237, 459);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(81, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 459);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(237, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(237, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(237, 26);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp04001_GC_Job_CallOut_StatusesTableAdapter
            // 
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04074_GC_Gritting_Callout_ManagerTableAdapter
            // 
            this.sp04074_GC_Gritting_Callout_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp04075_GC_Gritting_Callout_Linked_Extra_CostsTableAdapter
            // 
            this.sp04075_GC_Gritting_Callout_Linked_Extra_CostsTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "GrittingCalloutToolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(519, 261);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddJobs, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiOptimization, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSend, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReassignJobs, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewSMSErrors, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiManuallyComplete, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiCompletionSheet, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, this.bbiAuthorise, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, this.bbiPay, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiLoadingProgress, "", true, true, true, 87),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLoadingCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiViewOnMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterVisitsSelected, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // bbiAddJobs
            // 
            this.bbiAddJobs.Caption = "Wizard";
            this.bbiAddJobs.Id = 42;
            this.bbiAddJobs.ImageOptions.Image = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.bbiAddJobs.Name = "bbiAddJobs";
            this.bbiAddJobs.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiAddJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddJobs_ItemClick);
            // 
            // bbiOptimization
            // 
            this.bbiOptimization.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiOptimization.Caption = "Optimization";
            this.bbiOptimization.DropDownControl = this.pmOptimisation;
            this.bbiOptimization.Id = 49;
            this.bbiOptimization.ImageOptions.ImageIndex = 0;
            this.bbiOptimization.Name = "bbiOptimization";
            this.bbiOptimization.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Text = "Optimization - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to send the <b>selected</b> data to the Route Optimizer.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiOptimization.SuperTip = superToolTip4;
            this.bbiOptimization.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOptimization_ItemClick);
            // 
            // pmOptimisation
            // 
            this.pmOptimisation.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiRouteOptimiseSites),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiRouteOptimiseLeaks, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGetDataFromRouteOptimizer, true)});
            this.pmOptimisation.Manager = this.barManager1;
            this.pmOptimisation.Name = "pmOptimisation";
            // 
            // bsiRouteOptimiseSites
            // 
            this.bsiRouteOptimiseSites.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiRouteOptimiseSites.Caption = "<b>Site</b> Gritting Route Optimise";
            this.bsiRouteOptimiseSites.Id = 53;
            this.bsiRouteOptimiseSites.ImageOptions.ImageIndex = 0;
            this.bsiRouteOptimiseSites.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGetDataToRouteOptimize),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendDataToRouteOptimizer)});
            this.bsiRouteOptimiseSites.Name = "bsiRouteOptimiseSites";
            // 
            // bbiGetDataToRouteOptimize
            // 
            this.bbiGetDataToRouteOptimize.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiGetDataToRouteOptimize.Caption = "Select <b>Site</b> Gritting Data to Route Optimise";
            this.bbiGetDataToRouteOptimize.Id = 50;
            this.bbiGetDataToRouteOptimize.ImageOptions.ImageIndex = 2;
            this.bbiGetDataToRouteOptimize.Name = "bbiGetDataToRouteOptimize";
            toolTipTitleItem1.Text = "Select Data To Route Optimize - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to select data with an appropriate status for sending to the Route Optim" +
    "izer.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiGetDataToRouteOptimize.SuperTip = superToolTip1;
            this.bbiGetDataToRouteOptimize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGetDataToRouteOptimize_ItemClick);
            // 
            // bbiSendDataToRouteOptimizer
            // 
            this.bbiSendDataToRouteOptimizer.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendDataToRouteOptimizer.Caption = "Send <b>Site</b> Gritting Data to Route Optimiser...";
            this.bbiSendDataToRouteOptimizer.Id = 51;
            this.bbiSendDataToRouteOptimizer.ImageOptions.ImageIndex = 0;
            this.bbiSendDataToRouteOptimizer.Name = "bbiSendDataToRouteOptimizer";
            toolTipTitleItem2.Text = "Send Data to Route Optimizer - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to send the selected callout records to the Route Optimizer. \r\nRoutes ar" +
    "e created for the selected callouts and optionally, the best located teams can b" +
    "e assigned to the callouts.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiSendDataToRouteOptimizer.SuperTip = superToolTip2;
            this.bbiSendDataToRouteOptimizer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendDataToRouteOptimizer_ItemClick);
            // 
            // bsiRouteOptimiseLeaks
            // 
            this.bsiRouteOptimiseLeaks.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiRouteOptimiseLeaks.Caption = "<b>Leak</b> Gritting Route Optimise";
            this.bsiRouteOptimiseLeaks.Id = 54;
            this.bsiRouteOptimiseLeaks.ImageOptions.ImageIndex = 0;
            this.bsiRouteOptimiseLeaks.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGetDataToRouteOptimizeLeaks),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendDataToRouteOptimizerLeaks)});
            this.bsiRouteOptimiseLeaks.Name = "bsiRouteOptimiseLeaks";
            // 
            // bbiGetDataToRouteOptimizeLeaks
            // 
            this.bbiGetDataToRouteOptimizeLeaks.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiGetDataToRouteOptimizeLeaks.Caption = "Select <b>Leak</b> Gritting Data to Route Optimise";
            this.bbiGetDataToRouteOptimizeLeaks.Id = 55;
            this.bbiGetDataToRouteOptimizeLeaks.ImageOptions.ImageIndex = 2;
            this.bbiGetDataToRouteOptimizeLeaks.Name = "bbiGetDataToRouteOptimizeLeaks";
            this.bbiGetDataToRouteOptimizeLeaks.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGetDataToRouteOptimizeLeaks_ItemClick);
            // 
            // bbiSendDataToRouteOptimizerLeaks
            // 
            this.bbiSendDataToRouteOptimizerLeaks.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendDataToRouteOptimizerLeaks.Caption = "Send <b>Leak</b> Gritting Data to Route Optimiser...";
            this.bbiSendDataToRouteOptimizerLeaks.Id = 56;
            this.bbiSendDataToRouteOptimizerLeaks.ImageOptions.ImageIndex = 0;
            this.bbiSendDataToRouteOptimizerLeaks.Name = "bbiSendDataToRouteOptimizerLeaks";
            this.bbiSendDataToRouteOptimizerLeaks.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendDataToRouteOptimizerLeaks_ItemClick);
            // 
            // bbiGetDataFromRouteOptimizer
            // 
            this.bbiGetDataFromRouteOptimizer.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiGetDataFromRouteOptimizer.Caption = "<b>Get Data</b> from Route Optimizer...";
            this.bbiGetDataFromRouteOptimizer.Id = 52;
            this.bbiGetDataFromRouteOptimizer.ImageOptions.ImageIndex = 1;
            this.bbiGetDataFromRouteOptimizer.Name = "bbiGetDataFromRouteOptimizer";
            toolTipTitleItem3.Text = "Get Data from Route Optimizer - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to collect the results from the Route Optimizer and update the callout r" +
    "ecords with their route.\r\n";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiGetDataFromRouteOptimizer.SuperTip = superToolTip3;
            this.bbiGetDataFromRouteOptimizer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGetDataFromRouteOptimizer_ItemClick);
            // 
            // bsiSend
            // 
            this.bsiSend.Caption = "Send";
            this.bsiSend.Id = 57;
            this.bsiSend.ImageOptions.ImageIndex = 5;
            this.bsiSend.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetReadyToSend1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSendJobs, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendNoProactiveMessages, true)});
            this.bsiSend.Name = "bsiSend";
            this.bsiSend.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSetReadyToSend1
            // 
            this.bbiSetReadyToSend1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSetReadyToSend1.Caption = "Set <b>Ready To Send</b>";
            this.bbiSetReadyToSend1.Enabled = false;
            this.bbiSetReadyToSend1.Id = 61;
            this.bbiSetReadyToSend1.ImageOptions.ImageIndex = 6;
            this.bbiSetReadyToSend1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectSetReadyToSend),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetReadyToSend2, true)});
            this.bbiSetReadyToSend1.Name = "bbiSetReadyToSend1";
            this.bbiSetReadyToSend1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiSetReadyToSend1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiSelectSetReadyToSend
            // 
            this.bbiSelectSetReadyToSend.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectSetReadyToSend.Caption = "<b>Select</b> Jobs for Setting \'Ready To Send\'";
            this.bbiSelectSetReadyToSend.Id = 40;
            this.bbiSelectSetReadyToSend.ImageOptions.ImageIndex = 2;
            this.bbiSelectSetReadyToSend.Name = "bbiSelectSetReadyToSend";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Text = "Select Jobs to Set \'Ready To Send\' - Information";
            toolTipItem5.LeftIndent = 6;
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiSelectSetReadyToSend.SuperTip = superToolTip5;
            this.bbiSelectSetReadyToSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectSetReadyToSend_ItemClick);
            // 
            // bbiSetReadyToSend2
            // 
            this.bbiSetReadyToSend2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSetReadyToSend2.Caption = "<b>Set</b> Jobs as \'Ready To Send\'";
            this.bbiSetReadyToSend2.Id = 41;
            this.bbiSetReadyToSend2.ImageOptions.ImageIndex = 6;
            this.bbiSetReadyToSend2.Name = "bbiSetReadyToSend2";
            toolTipTitleItem6.Text = "Set Jobs as \'Ready To Send\' - Information";
            superToolTip6.Items.Add(toolTipTitleItem6);
            this.bbiSetReadyToSend2.SuperTip = superToolTip6;
            this.bbiSetReadyToSend2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetReadyToSend2_ItemClick);
            // 
            // bsiSendJobs
            // 
            this.bsiSendJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSendJobs.Caption = "<b>Send</b> Jobs";
            this.bsiSendJobs.Id = 33;
            this.bsiSendJobs.ImageOptions.ImageIndex = 5;
            this.bsiSendJobs.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectJobsReadyToSend),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendJobs, true)});
            this.bsiSendJobs.Name = "bsiSendJobs";
            this.bsiSendJobs.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Text = "Send Jobs - Information";
            toolTipItem8.LeftIndent = 6;
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem8);
            this.bsiSendJobs.SuperTip = superToolTip9;
            // 
            // bbiSelectJobsReadyToSend
            // 
            this.bbiSelectJobsReadyToSend.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectJobsReadyToSend.Caption = "<b>Select</b> \'Ready to Send\' Jobs";
            this.bbiSelectJobsReadyToSend.Id = 34;
            this.bbiSelectJobsReadyToSend.ImageOptions.ImageIndex = 2;
            this.bbiSelectJobsReadyToSend.Name = "bbiSelectJobsReadyToSend";
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Text = "Select \'Ready To Send\' Jobs - Information";
            toolTipItem6.LeftIndent = 6;
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem6);
            this.bbiSelectJobsReadyToSend.SuperTip = superToolTip7;
            this.bbiSelectJobsReadyToSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectJobsReadyToSend_ItemClick);
            // 
            // bbiSendJobs
            // 
            this.bbiSendJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendJobs.Caption = "<B>Send</b> Jobs";
            this.bbiSendJobs.Id = 35;
            this.bbiSendJobs.ImageOptions.ImageIndex = 5;
            this.bbiSendJobs.Name = "bbiSendJobs";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Text = "Send Jobs - Information";
            toolTipItem7.LeftIndent = 6;
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem7);
            this.bbiSendJobs.SuperTip = superToolTip8;
            this.bbiSendJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendJobs_ItemClick);
            // 
            // bbiSendNoProactiveMessages
            // 
            this.bbiSendNoProactiveMessages.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendNoProactiveMessages.Caption = "Send No Proactive <b>Messages</b>";
            this.bbiSendNoProactiveMessages.Id = 48;
            this.bbiSendNoProactiveMessages.ImageOptions.ImageIndex = 7;
            this.bbiSendNoProactiveMessages.Name = "bbiSendNoProactiveMessages";
            this.bbiSendNoProactiveMessages.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Send No Proactive Messages - Information";
            toolTipItem9.LeftIndent = 6;
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem9);
            this.bbiSendNoProactiveMessages.SuperTip = superToolTip10;
            this.bbiSendNoProactiveMessages.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendNoProactiveMessages_ItemClick);
            // 
            // bbiReassignJobs
            // 
            this.bbiReassignJobs.Caption = "Reassign";
            this.bbiReassignJobs.Id = 29;
            this.bbiReassignJobs.ImageOptions.ImageIndex = 9;
            this.bbiReassignJobs.Name = "bbiReassignJobs";
            this.bbiReassignJobs.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.Text = "Reassign Jobs - Information";
            toolTipItem10.LeftIndent = 6;
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem10);
            this.bbiReassignJobs.SuperTip = superToolTip11;
            this.bbiReassignJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReassignJobs_ItemClick);
            // 
            // bbiViewSMSErrors
            // 
            this.bbiViewSMSErrors.Caption = "SMS Errors";
            this.bbiViewSMSErrors.Id = 38;
            this.bbiViewSMSErrors.ImageOptions.ImageIndex = 11;
            this.bbiViewSMSErrors.Name = "bbiViewSMSErrors";
            this.bbiViewSMSErrors.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip12.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem12.Text = "View SMS Errors - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to open the View SMS Errors screen to view invalid text messages returne" +
    "d from jobs.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem11);
            this.bbiViewSMSErrors.SuperTip = superToolTip12;
            this.bbiViewSMSErrors.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewSMSErrors_ItemClick);
            // 
            // bsiManuallyComplete
            // 
            this.bsiManuallyComplete.Caption = "Complete";
            this.bsiManuallyComplete.Id = 44;
            this.bsiManuallyComplete.ImageOptions.ImageIndex = 4;
            this.bsiManuallyComplete.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectJobsToComplete, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiManuallyCompleteJobs)});
            this.bsiManuallyComplete.Name = "bsiManuallyComplete";
            this.bsiManuallyComplete.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectJobsToComplete
            // 
            this.bbiSelectJobsToComplete.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectJobsToComplete.Caption = "<b>Select</b> Incomplete Jobs";
            this.bbiSelectJobsToComplete.Id = 46;
            this.bbiSelectJobsToComplete.ImageOptions.ImageIndex = 2;
            this.bbiSelectJobsToComplete.Name = "bbiSelectJobsToComplete";
            superToolTip13.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem13.Text = "Select Incomplete Jobs - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to select all jobs which are not yet complete.\r\n\r\n<b><color=red>Importan" +
    "t Note:</color></b> Only jobs which have not yet been completed may be selected." +
    "";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem12);
            this.bbiSelectJobsToComplete.SuperTip = superToolTip13;
            this.bbiSelectJobsToComplete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectJobsToComplete_ItemClick);
            // 
            // bbiManuallyCompleteJobs
            // 
            this.bbiManuallyCompleteJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiManuallyCompleteJobs.Caption = "<b>Manually Complete</b> Selected Jobs";
            this.bbiManuallyCompleteJobs.Id = 47;
            this.bbiManuallyCompleteJobs.ImageOptions.ImageIndex = 4;
            this.bbiManuallyCompleteJobs.Name = "bbiManuallyCompleteJobs";
            superToolTip14.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem14.Text = "Manually Complete Jobs - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to manually complete the selected jobs.\r\n\r\n<b>Note:</b> Only jobs with a" +
    " selected Site and Gritting Team can be manually completed.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem13);
            this.bbiManuallyCompleteJobs.SuperTip = superToolTip14;
            this.bbiManuallyCompleteJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiManuallyCompleteJobs_ItemClick);
            // 
            // bsiCompletionSheet
            // 
            this.bsiCompletionSheet.Caption = "Completion Sheet";
            this.bsiCompletionSheet.Id = 80;
            this.bsiCompletionSheet.ImageOptions.ImageIndex = 3;
            this.bsiCompletionSheet.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiKMLMapImage),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiCompletionSheetCreate)});
            this.bsiCompletionSheet.Name = "bsiCompletionSheet";
            this.bsiCompletionSheet.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bsiKMLMapImage
            // 
            this.bsiKMLMapImage.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiKMLMapImage.Caption = "Create <b>KML \\ Map Images</b>";
            this.bsiKMLMapImage.Id = 67;
            this.bsiKMLMapImage.ImageOptions.ImageIndex = 8;
            this.bsiKMLMapImage.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectJobsForKMLCreation),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateKMLAndMapImages, true)});
            this.bsiKMLMapImage.Name = "bsiKMLMapImage";
            this.bsiKMLMapImage.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectJobsForKMLCreation
            // 
            this.bbiSelectJobsForKMLCreation.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectJobsForKMLCreation.Caption = "<b>Select</b> Jobs for KML\\ Map Image Creation";
            this.bbiSelectJobsForKMLCreation.Id = 69;
            this.bbiSelectJobsForKMLCreation.ImageOptions.ImageIndex = 2;
            this.bbiSelectJobsForKMLCreation.Name = "bbiSelectJobsForKMLCreation";
            this.bbiSelectJobsForKMLCreation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiSelectJobsForKMLCreation.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectJobsForKMLCreation_ItemClick);
            // 
            // bbiCreateKMLAndMapImages
            // 
            this.bbiCreateKMLAndMapImages.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiCreateKMLAndMapImages.Caption = "<b>Create</b> KML \\ Map Images";
            this.bbiCreateKMLAndMapImages.Id = 70;
            this.bbiCreateKMLAndMapImages.ImageOptions.ImageIndex = 8;
            this.bbiCreateKMLAndMapImages.Name = "bbiCreateKMLAndMapImages";
            this.bbiCreateKMLAndMapImages.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreateKMLAndMapImages_ItemClick);
            // 
            // bsiCompletionSheetCreate
            // 
            this.bsiCompletionSheetCreate.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiCompletionSheetCreate.Caption = "Create <b>Completion Sheets</b>";
            this.bsiCompletionSheetCreate.Id = 68;
            this.bsiCompletionSheetCreate.ImageOptions.ImageIndex = 3;
            this.bsiCompletionSheetCreate.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectCompletionSheetJobs),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiShowMapsOnCompletionSheets, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPreviewCompletionSheets),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCompletionSheet)});
            this.bsiCompletionSheetCreate.Name = "bsiCompletionSheetCreate";
            this.bsiCompletionSheetCreate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectCompletionSheetJobs
            // 
            this.bbiSelectCompletionSheetJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectCompletionSheetJobs.Caption = "<b>Select</b> Jobs Due Completion Sheets";
            this.bbiSelectCompletionSheetJobs.Id = 60;
            this.bbiSelectCompletionSheetJobs.ImageOptions.ImageIndex = 2;
            this.bbiSelectCompletionSheetJobs.Name = "bbiSelectCompletionSheetJobs";
            this.bbiSelectCompletionSheetJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectCompletionSheetJobs_ItemClick);
            // 
            // beiShowMapsOnCompletionSheets
            // 
            this.beiShowMapsOnCompletionSheets.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.beiShowMapsOnCompletionSheets.Caption = "Show Maps on Completion Sheets";
            this.beiShowMapsOnCompletionSheets.Edit = this.repositoryItemCheckEditShowMapsOnCompletionSheets;
            this.beiShowMapsOnCompletionSheets.EditValue = true;
            this.beiShowMapsOnCompletionSheets.Id = 73;
            this.beiShowMapsOnCompletionSheets.ItemAppearance.Disabled.BackColor = System.Drawing.Color.Transparent;
            this.beiShowMapsOnCompletionSheets.ItemAppearance.Disabled.Options.UseBackColor = true;
            this.beiShowMapsOnCompletionSheets.ItemAppearance.Hovered.BackColor = System.Drawing.Color.Transparent;
            this.beiShowMapsOnCompletionSheets.ItemAppearance.Hovered.Options.UseBackColor = true;
            this.beiShowMapsOnCompletionSheets.ItemAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.beiShowMapsOnCompletionSheets.ItemAppearance.Normal.Options.UseBackColor = true;
            this.beiShowMapsOnCompletionSheets.ItemAppearance.Pressed.BackColor = System.Drawing.Color.Transparent;
            this.beiShowMapsOnCompletionSheets.ItemAppearance.Pressed.Options.UseBackColor = true;
            this.beiShowMapsOnCompletionSheets.ItemInMenuAppearance.Disabled.BackColor = System.Drawing.Color.Transparent;
            this.beiShowMapsOnCompletionSheets.ItemInMenuAppearance.Disabled.Options.UseBackColor = true;
            this.beiShowMapsOnCompletionSheets.ItemInMenuAppearance.Hovered.BackColor = System.Drawing.Color.Transparent;
            this.beiShowMapsOnCompletionSheets.ItemInMenuAppearance.Hovered.Options.UseBackColor = true;
            this.beiShowMapsOnCompletionSheets.ItemInMenuAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.beiShowMapsOnCompletionSheets.ItemInMenuAppearance.Normal.Options.UseBackColor = true;
            this.beiShowMapsOnCompletionSheets.ItemInMenuAppearance.Pressed.BackColor = System.Drawing.Color.Transparent;
            this.beiShowMapsOnCompletionSheets.ItemInMenuAppearance.Pressed.Options.UseBackColor = true;
            this.beiShowMapsOnCompletionSheets.Name = "beiShowMapsOnCompletionSheets";
            // 
            // repositoryItemCheckEditShowMapsOnCompletionSheets
            // 
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.AutoWidth = true;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.Caption = "";
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEditShowMapsOnCompletionSheets.Name = "repositoryItemCheckEditShowMapsOnCompletionSheets";
            // 
            // bbiPreviewCompletionSheets
            // 
            this.bbiPreviewCompletionSheets.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiPreviewCompletionSheets.Caption = "<b>Preview</b> Completion Sheets";
            this.bbiPreviewCompletionSheets.Id = 76;
            this.bbiPreviewCompletionSheets.ImageOptions.ImageIndex = 14;
            this.bbiPreviewCompletionSheets.Name = "bbiPreviewCompletionSheets";
            this.bbiPreviewCompletionSheets.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiPreviewCompletionSheets.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPreviewCompletionSheets_ItemClick);
            // 
            // bbiCompletionSheet
            // 
            this.bbiCompletionSheet.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiCompletionSheet.Caption = "<b>Create</b> Completion Sheets";
            this.bbiCompletionSheet.Id = 58;
            this.bbiCompletionSheet.ImageOptions.ImageIndex = 3;
            this.bbiCompletionSheet.Name = "bbiCompletionSheet";
            this.bbiCompletionSheet.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem15.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Appearance.Options.UseImage = true;
            toolTipTitleItem15.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Text = "Completion Sheet - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to create completion sheets for the selected Gritting Callout(s).";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem14);
            this.bbiCompletionSheet.SuperTip = superToolTip15;
            this.bbiCompletionSheet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCompletionSheet_ItemClick);
            // 
            // bbiAuthorise
            // 
            this.bbiAuthorise.Caption = "Authorise";
            this.bbiAuthorise.Enabled = false;
            this.bbiAuthorise.Id = 31;
            this.bbiAuthorise.ImageOptions.ImageIndex = 10;
            this.bbiAuthorise.Name = "bbiAuthorise";
            this.bbiAuthorise.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip16.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem16.Text = "Authorise Payment - Information";
            toolTipItem15.LeftIndent = 6;
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem15);
            this.bbiAuthorise.SuperTip = superToolTip16;
            this.bbiAuthorise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAuthorise_ItemClick);
            // 
            // bbiPay
            // 
            this.bbiPay.Caption = "Pay";
            this.bbiPay.Enabled = false;
            this.bbiPay.Id = 32;
            this.bbiPay.ImageOptions.Image = global::WoodPlan5.Properties.Resources.team_invoice_32x32;
            this.bbiPay.Name = "bbiPay";
            this.bbiPay.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip17.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem17.Text = "Pay - Information";
            toolTipItem16.LeftIndent = 6;
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem16);
            this.bbiPay.SuperTip = superToolTip17;
            this.bbiPay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPay_ItemClick);
            // 
            // beiLoadingProgress
            // 
            this.beiLoadingProgress.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.beiLoadingProgress.Caption = "Loading Data Progress";
            this.beiLoadingProgress.Edit = this.repositoryItemMarqueeProgressBar1;
            this.beiLoadingProgress.EditValue = "Loading Data...";
            this.beiLoadingProgress.Id = 79;
            this.beiLoadingProgress.Name = "beiLoadingProgress";
            // 
            // repositoryItemMarqueeProgressBar1
            // 
            this.repositoryItemMarqueeProgressBar1.MarqueeWidth = 20;
            this.repositoryItemMarqueeProgressBar1.Name = "repositoryItemMarqueeProgressBar1";
            this.repositoryItemMarqueeProgressBar1.ShowTitle = true;
            // 
            // bbiLoadingCancel
            // 
            this.bbiLoadingCancel.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiLoadingCancel.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiLoadingCancel.Caption = "Cancel Load";
            this.bbiLoadingCancel.Id = 77;
            this.bbiLoadingCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLoadingCancel.ImageOptions.Image")));
            this.bbiLoadingCancel.Name = "bbiLoadingCancel";
            toolTipTitleItem18.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem18.Appearance.Options.UseImage = true;
            toolTipTitleItem18.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem18.Text = "Cancel Load - Information";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Click me to cancel the current data loading operation.";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem17);
            this.bbiLoadingCancel.SuperTip = superToolTip18;
            this.bbiLoadingCancel.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiLoadingCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadingCancel_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 62;
            this.bbiRefresh.ImageOptions.ImageIndex = 12;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bsiViewOnMap
            // 
            this.bsiViewOnMap.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiViewOnMap.Caption = "Mapping";
            this.bsiViewOnMap.Id = 36;
            this.bsiViewOnMap.ImageOptions.ImageIndex = 8;
            this.bsiViewOnMap.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewSiteOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewJobOnMap, true)});
            this.bsiViewOnMap.Name = "bsiViewOnMap";
            this.bsiViewOnMap.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiViewSiteOnMap
            // 
            this.bbiViewSiteOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewSiteOnMap.Caption = "View <b>Site</b> On Map";
            this.bbiViewSiteOnMap.Id = 30;
            this.bbiViewSiteOnMap.ImageOptions.ImageIndex = 8;
            this.bbiViewSiteOnMap.Name = "bbiViewSiteOnMap";
            superToolTip19.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem19.Text = "View Site On Map - Information";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Click me to View the Site on the Map.\r\n\r\n<b>Note:</b> An internet connection is r" +
    "equired.";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem18);
            this.bbiViewSiteOnMap.SuperTip = superToolTip19;
            this.bbiViewSiteOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewSiteOnMap_ItemClick);
            // 
            // bbiViewJobOnMap
            // 
            this.bbiViewJobOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewJobOnMap.Caption = "View <b>Callout</b> Start and Finish Position On Map";
            this.bbiViewJobOnMap.Id = 37;
            this.bbiViewJobOnMap.ImageOptions.ImageIndex = 8;
            this.bbiViewJobOnMap.Name = "bbiViewJobOnMap";
            superToolTip20.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem20.Text = "View Callout Start and Finish Position on Map - Information";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Click me to the View Job Start and End position on the Map.\r\n\r\n<b>Note:</b> An in" +
    "ternet connection is required.";
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem19);
            this.bbiViewJobOnMap.SuperTip = superToolTip20;
            this.bbiViewJobOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewJobOnMap_ItemClick);
            // 
            // bciFilterVisitsSelected
            // 
            this.bciFilterVisitsSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterVisitsSelected.Caption = "Filter Selected <b>Callouts</b>";
            this.bciFilterVisitsSelected.Id = 66;
            this.bciFilterVisitsSelected.ImageOptions.ImageIndex = 13;
            this.bciFilterVisitsSelected.Name = "bciFilterVisitsSelected";
            this.bciFilterVisitsSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterVisitsSelected_CheckedChanged);
            // 
            // bsiSelectedCount
            // 
            this.bsiSelectedCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount.Caption = "0 Callouts Selected";
            this.bsiSelectedCount.Id = 63;
            this.bsiSelectedCount.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount.Name = "bsiSelectedCount";
            this.bsiSelectedCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiManuallyComplete
            // 
            this.bbiManuallyComplete.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bbiManuallyComplete.Caption = "Complete";
            this.bbiManuallyComplete.Id = 43;
            this.bbiManuallyComplete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiManuallyComplete.ImageOptions.Image")));
            this.bbiManuallyComplete.Name = "bbiManuallyComplete";
            this.bbiManuallyComplete.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip25.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem25.Text = "Manually Complete Job - Information";
            toolTipItem24.LeftIndent = 6;
            superToolTip25.Items.Add(toolTipTitleItem25);
            superToolTip25.Items.Add(toolTipItem24);
            this.bbiManuallyComplete.SuperTip = superToolTip25;
            // 
            // bbiSendJobs2
            // 
            this.bbiSendJobs2.Caption = "Send Jobs";
            this.bbiSendJobs2.Id = 27;
            this.bbiSendJobs2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSendJobs2.ImageOptions.Image")));
            this.bbiSendJobs2.Name = "bbiSendJobs2";
            this.bbiSendJobs2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter
            // 
            this.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter.ClearBeforeFill = true;
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp04237_GC_Company_Filter_ListTableAdapter
            // 
            this.sp04237_GC_Company_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp05089_CRM_Contacts_Linked_To_RecordTableAdapter
            // 
            this.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // sp04337_GC_Service_Areas_Linked_To_Gritting_CalloutTableAdapter
            // 
            this.sp04337_GC_Service_Areas_Linked_To_Gritting_CalloutTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(0, "route_optimize_send.png");
            this.imageCollection2.Images.SetKeyName(1, "route_optimize_get.png");
            this.imageCollection2.InsertGalleryImage("contentarrangeinrows_32x32.png", "images/alignment/contentarrangeinrows_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/alignment/contentarrangeinrows_32x32.png"), 2);
            this.imageCollection2.Images.SetKeyName(2, "contentarrangeinrows_32x32.png");
            this.imageCollection2.Images.SetKeyName(3, "completion_sheet_32.png");
            this.imageCollection2.Images.SetKeyName(4, "Visit_Manually_Complete_32x32.png");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_send_32x32, "team_send_32x32", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection2.Images.SetKeyName(5, "team_send_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_send2_32x32, "team_send2_32x32", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection2.Images.SetKeyName(6, "team_send2_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_send_disabled_32x32, "team_send_disabled_32x32", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection2.Images.SetKeyName(7, "team_send_disabled_32x32");
            this.imageCollection2.InsertGalleryImage("geopointmap_32x32.png", "images/maps/geopointmap_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopointmap_32x32.png"), 8);
            this.imageCollection2.Images.SetKeyName(8, "geopointmap_32x32.png");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.team_reassign_32, "team_reassign_32", typeof(global::WoodPlan5.Properties.Resources), 9);
            this.imageCollection2.Images.SetKeyName(9, "team_reassign_32");
            this.imageCollection2.InsertGalleryImage("apply_32x32.png", "images/actions/apply_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_32x32.png"), 10);
            this.imageCollection2.Images.SetKeyName(10, "apply_32x32.png");
            this.imageCollection2.InsertGalleryImage("knowledgebasearticle_32x32.png", "images/support/knowledgebasearticle_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/knowledgebasearticle_32x32.png"), 11);
            this.imageCollection2.Images.SetKeyName(11, "knowledgebasearticle_32x32.png");
            this.imageCollection2.InsertGalleryImage("refresh_32x32.png", "images/actions/refresh_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_32x32.png"), 12);
            this.imageCollection2.Images.SetKeyName(12, "refresh_32x32.png");
            this.imageCollection2.InsertGalleryImage("masterfilter_32x32.png", "images/filter/masterfilter_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter/masterfilter_32x32.png"), 13);
            this.imageCollection2.Images.SetKeyName(13, "masterfilter_32x32.png");
            this.imageCollection2.InsertGalleryImage("preview_32x32.png", "images/print/preview_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_32x32.png"), 14);
            this.imageCollection2.Images.SetKeyName(14, "preview_32x32.png");
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending15
            // 
            this.xtraGridBlending15.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending15.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending15.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending15.GridControl = this.gridControl15;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 537);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.ID = new System.Guid("3306840f-a5fb-463d-9ebd-352620ccc0fc");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(344, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(344, 537);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(337, 505);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // frm_GC_Callout_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1560, 537);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Callout_Manager";
            this.Text = "GRITTING Callout Manager";
            this.Activated += new System.EventHandler(this.frm_GC_Callout_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Callout_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Callout_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04074GCGrittingCalloutManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).EndInit();
            this.popupContainerControlCompanies.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).EndInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04075GCGrittingCalloutLinkedExtraCostsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours2DP)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04337GCServiceAreasLinkedToGrittingCalloutBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04092GCGrittingCalloutPicturesForCalloutBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05089CRMContactsLinkedToRecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCompanies.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditColourCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilterType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFilterCriteria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSpecificVisitIDs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitIDs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforFormatDataBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmOptimisation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowMapsOnCompletionSheets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLinkedGrittingCalloutsFilter;
        private DevExpress.XtraEditors.SimpleButton btnGritCalloutFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private System.Windows.Forms.BindingSource sp04001GCJobCallOutStatusesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter sp04001_GC_Job_CallOut_StatusesTableAdapter;
        private System.Windows.Forms.BindingSource sp04074GCGrittingCalloutManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCallOutID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorIsDirectLabour;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContarctorName;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colImportedWeatherForecastID;
        private DevExpress.XtraGrid.Columns.GridColumn colTextSentTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorReceivedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorRespondedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorETA;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitAborted;
        private DevExpress.XtraGrid.Columns.GridColumn colAbortedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishedLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishedLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltSell;
        private DevExpress.XtraGrid.Columns.GridColumn colSaltVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamHourlyRate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamCharge;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCost;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherSell;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByName;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractorReason;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSourceLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colGritSourceLocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteWeather;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTemperature;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamPresent;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceFailure;
        private DevExpress.XtraGrid.Columns.GridColumn colComments;
        private DataSet_GC_CoreTableAdapters.sp04074_GC_Gritting_Callout_ManagerTableAdapter sp04074_GC_Gritting_Callout_ManagerTableAdapter;
        private System.Windows.Forms.BindingSource sp04075GCGrittingCalloutLinkedExtraCostsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraCostID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCallOutID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraGrid.Columns.GridColumn colSell;
        private DevExpress.XtraGrid.Columns.GridColumn colVatRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromDefault;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime1;
        private DataSet_GC_CoreTableAdapters.sp04075_GC_Gritting_Callout_Linked_Extra_CostsTableAdapter sp04075_GC_Gritting_Callout_Linked_Extra_CostsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPrice;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiSendJobs2;
        private DevExpress.XtraBars.BarButtonItem bbiReassignJobs;
        private DevExpress.XtraBars.BarButtonItem bbiViewSiteOnMap;
        private DevExpress.XtraGrid.Columns.GridColumn colGritJobTransferMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraBars.BarButtonItem bbiAuthorise;
        private DevExpress.XtraBars.BarButtonItem bbiPay;
        private DevExpress.XtraBars.BarSubItem bsiSendJobs;
        private DevExpress.XtraBars.BarButtonItem bbiSelectJobsReadyToSend;
        private DevExpress.XtraBars.BarButtonItem bbiSendJobs;
        private DevExpress.XtraBars.BarSubItem bsiViewOnMap;
        private DevExpress.XtraBars.BarButtonItem bbiViewJobOnMap;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private System.Windows.Forms.BindingSource sp04092GCGrittingCalloutPicturesForCalloutBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCallOutID2;
        private DevExpress.XtraGrid.Columns.GridColumn colPicturePath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeTaken;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGrittingContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamAddressLine11;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DataSet_GC_CoreTableAdapters.sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter sp04092_GC_Gritting_Callout_Pictures_For_CalloutTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colProfit;
        private DevExpress.XtraGrid.Columns.GridColumn colMarkup;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardSell;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClientReason;
        private DevExpress.XtraGrid.Columns.GridColumn colGritMobileTelephoneNumber;
        private DevExpress.XtraBars.BarButtonItem bbiViewSMSErrors;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarButtonItem bbiSelectSetReadyToSend;
        private DevExpress.XtraBars.BarButtonItem bbiSetReadyToSend2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSignatureFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colPPEWorn;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccessAbortedRate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMembersPresent;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowInfo;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colEveningRateModifier;
        public DevExpress.XtraBars.BarButtonItem bbiAddJobs;
        private DevExpress.XtraGrid.Columns.GridColumn colChargedPerHour;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOfUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP2;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell1;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowOnSite1;
        private DevExpress.XtraGrid.Columns.GridColumn colHours;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingTimeToDeduct;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours2DP;
        private DevExpress.XtraBars.BarButtonItem bbiManuallyComplete;
        private DevExpress.XtraBars.BarSubItem bsiManuallyComplete;
        private DevExpress.XtraBars.BarButtonItem bbiSelectJobsToComplete;
        private DevExpress.XtraBars.BarButtonItem bbiManuallyCompleteJobs;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCompanies;
        private DevExpress.XtraEditors.SimpleButton btnCompanyFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOrder;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditCompanies;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource;
        private DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter sp04237_GC_Company_Filter_ListTableAdapter;
        private WoodPlan5.DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedOnPDA;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSheetNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceInvoiceNumber;
        private DevExpress.XtraBars.BarButtonItem bbiSendNoProactiveMessages;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceTeamPaymentExported;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl15;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraGrid.Columns.GridColumn colCRMID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDueDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMadeDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colContactType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private System.Windows.Forms.BindingSource sp05089CRMContactsLinkedToRecordBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private WoodPlanDataSetTableAdapters.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter sp05089_CRM_Contacts_Linked_To_RecordTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContact;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private System.Windows.Forms.BindingSource sp04337GCServiceAreasLinkedToGrittingCalloutBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingServicedSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingCalloutID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteServiceAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptionID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colWasServiced;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromDefault1;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaDescription;
        private DataSet_GC_CoreTableAdapters.sp04337_GC_Service_Areas_Linked_To_Gritting_CalloutTableAdapter sp04337_GC_Service_Areas_Linked_To_Gritting_CalloutTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrioritySite;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowOnSiteOver5cm;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colPDACode;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowCleared;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearedSatisfactory;
        private DevExpress.XtraGrid.Columns.GridColumn colAttendanceOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionEmailSent;
        private DevExpress.XtraGrid.Columns.GridColumn colIsFloatingSite;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress1;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLat;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLong;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupID;
        private DevExpress.XtraBars.BarButtonItem bbiOptimization;
        private DevExpress.XtraBars.BarButtonItem bbiGetDataToRouteOptimize;
        private DevExpress.XtraBars.BarButtonItem bbiSendDataToRouteOptimizer;
        private DevExpress.XtraBars.BarButtonItem bbiGetDataFromRouteOptimizer;
        private DevExpress.XtraBars.PopupMenu pmOptimisation;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraBars.BarSubItem bsiRouteOptimiseSites;
        private DevExpress.XtraBars.BarSubItem bsiRouteOptimiseLeaks;
        private DevExpress.XtraBars.BarButtonItem bbiGetDataToRouteOptimizeLeaks;
        private DevExpress.XtraBars.BarButtonItem bbiSendDataToRouteOptimizerLeaks;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending15;
        private DevExpress.XtraGrid.Columns.GridColumn colDoubleGrit;
        private DevExpress.XtraBars.BarSubItem bsiSend;
        private DevExpress.XtraBars.BarButtonItem bbiCompletionSheet;
        private DevExpress.XtraBars.BarButtonItem bbiSelectCompletionSheetJobs;
        private DevExpress.XtraBars.BarSubItem bbiSetReadyToSend1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount;
        private DevExpress.XtraBars.BarCheckItem bciFilterVisitsSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colKMLFile;
        private DevExpress.XtraGrid.Columns.GridColumn colKMLImageFile;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionSheetFile;
        private DevExpress.XtraBars.BarSubItem bsiKMLMapImage;
        private DevExpress.XtraBars.BarButtonItem bbiSelectJobsForKMLCreation;
        private DevExpress.XtraBars.BarButtonItem bbiCreateKMLAndMapImages;
        private DevExpress.XtraBars.BarSubItem bsiCompletionSheetCreate;
        private DevExpress.XtraBars.BarEditItem beiShowMapsOnCompletionSheets;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowMapsOnCompletionSheets;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraBars.BarButtonItem bbiPreviewCompletionSheets;
        private DevExpress.XtraBars.BarButtonItem bbiLoadingCancel;
        private DevExpress.XtraBars.BarEditItem beiLoadingProgress;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar repositoryItemMarqueeProgressBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupFilterType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupFilterCriteria;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CheckEdit checkEditColourCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSpecificVisitIDs;
        private DevExpress.XtraEditors.MemoEdit VisitIDsMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitIDs;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedFile;
        private DevExpress.XtraBars.BarSubItem bsiCompletionSheet;
        private DevExpress.XtraEditors.SimpleButton btnFormatData;
        private DevExpress.XtraLayout.LayoutControlItem ItemforFormatDataBtn;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_GC_Snow_Authorise_Callouts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Snow_Authorise_Callouts));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiTimesheetSubmitted = new DevExpress.XtraBars.BarButtonItem();
            this.pmTimesheet = new DevExpress.XtraBars.PopupMenu(this.components);
            this.TimesheetSubmitted2 = new DevExpress.XtraBars.BarButtonItem();
            this.TimesheetSubmittedClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTeamInvoiceNo1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmTeamInvoiceNo = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiTeamInvcoiceNo2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTeamInvoiceNoClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAuthorise1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmAuthorise = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiAuthorise2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUnAuthorise = new DevExpress.XtraBars.BarButtonItem();
            this.bbiQuery1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmQuery = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiQuery2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiQueryClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDontPay1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmDoNotPay = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiDontPay2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDoPay = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem4 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CheckEditShowDontPay = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditShowQueries = new DevExpress.XtraEditors.CheckEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.bbiLoadData = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemShowQueries = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditShowQueries = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barEditItemShowDontPay = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditShowDontPay = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiInvoiceSelectRecords = new DevExpress.XtraBars.BarButtonItem();
            this.bbiInvoice2 = new DevExpress.XtraBars.BarButtonItem();
            this.barLinkContainerItem2 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barLinkContainerItem3 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp04197GCSnowCalloutManagerForAuthorisationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Snow_DataEntry = new WoodPlan5.DataSet_GC_Snow_DataEntry();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSnowClearanceCallOutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCPONumberSuffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritMobileTelephoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCallOutDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSubContractorETA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitAborted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbortedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLabourVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditVat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorContactedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPaySubContractorReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClientReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarkup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNonStandardSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientHowSoon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoursWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoAccessAbortedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamPOFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colTimesheetSubmitted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Alert = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barEditItem3 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp04197_GC_Snow_Callout_Manager_For_AuthorisationTableAdapter = new WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters.sp04197_GC_Snow_Callout_Manager_For_AuthorisationTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmTimesheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmTeamInvoiceNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmAuthorise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmQuery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDoNotPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditShowDontPay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditShowQueries.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowQueries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowDontPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04197GCSnowCalloutManagerForAuthorisationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1109, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 515);
            this.barDockControlBottom.Size = new System.Drawing.Size(1109, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 473);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1109, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 473);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAuthorise2,
            this.bbiUnAuthorise,
            this.barSubItem2,
            this.bbiQuery2,
            this.barButtonItem4,
            this.bbiDontPay2,
            this.bbiDoPay,
            this.bbiQueryClear,
            this.bbiAuthorise1,
            this.bbiQuery1,
            this.bbiDontPay1,
            this.bbiInvoice2,
            this.bbiInvoiceSelectRecords,
            this.bbiLoadData,
            this.barLinkContainerItem2,
            this.barLinkContainerItem3,
            this.barCheckItem1,
            this.barEditItemShowQueries,
            this.barEditItemShowDontPay,
            this.barEditItem1,
            this.barEditItem2,
            this.barEditItem3,
            this.barEditItem4,
            this.bbiTeamInvoiceNo1,
            this.bbiTeamInvcoiceNo2,
            this.bbiTeamInvoiceNoClear,
            this.TimesheetSubmittedClear,
            this.bbiTimesheetSubmitted,
            this.TimesheetSubmitted2});
            this.barManager1.MaxItemId = 65;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEditShowQueries,
            this.repositoryItemCheckEditShowDontPay,
            this.repositoryItemDateEdit1,
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemDateEdit2,
            this.repositoryItemDateEdit3,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemPopupContainerEdit3});
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "team_disabled_16.png");
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bar1
            // 
            this.bar1.BarName = "Gritting Callout Toolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(501, 185);
            this.bar1.OptionsBar.AllowCollapse = true;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // bar2
            // 
            this.bar2.BarName = "Authorisation Toolbar";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(651, 176);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTimesheetSubmitted, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTeamInvoiceNo1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAuthorise1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiQuery1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDontPay1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem4, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLoadData)});
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Authorisation Toolbar";
            // 
            // bbiTimesheetSubmitted
            // 
            this.bbiTimesheetSubmitted.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiTimesheetSubmitted.Caption = "Timesheet Submitted";
            this.bbiTimesheetSubmitted.DropDownControl = this.pmTimesheet;
            this.bbiTimesheetSubmitted.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiTimesheetSubmitted.Glyph")));
            this.bbiTimesheetSubmitted.Id = 63;
            this.bbiTimesheetSubmitted.Name = "bbiTimesheetSubmitted";
            this.bbiTimesheetSubmitted.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Timesheet Submitted - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to set selected callout(s) as Team Timesheet Submitted.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiTimesheetSubmitted.SuperTip = superToolTip3;
            this.bbiTimesheetSubmitted.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTimesheetSubmitted_ItemClick);
            // 
            // pmTimesheet
            // 
            this.pmTimesheet.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.TimesheetSubmitted2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.TimesheetSubmittedClear, true)});
            this.pmTimesheet.Manager = this.barManager1;
            this.pmTimesheet.Name = "pmTimesheet";
            // 
            // TimesheetSubmitted2
            // 
            this.TimesheetSubmitted2.Caption = "Team Timesheet Submitted";
            this.TimesheetSubmitted2.Glyph = ((System.Drawing.Image)(resources.GetObject("TimesheetSubmitted2.Glyph")));
            this.TimesheetSubmitted2.Id = 64;
            this.TimesheetSubmitted2.Name = "TimesheetSubmitted2";
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Team Timesheet Submitted - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to set selected callout(s) as Team Timesheet Submitted.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.TimesheetSubmitted2.SuperTip = superToolTip1;
            this.TimesheetSubmitted2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.TimesheetSubmitted2_ItemClick);
            // 
            // TimesheetSubmittedClear
            // 
            this.TimesheetSubmittedClear.Caption = "Clear Team Timesheet Submitted";
            this.TimesheetSubmittedClear.Glyph = ((System.Drawing.Image)(resources.GetObject("TimesheetSubmittedClear.Glyph")));
            this.TimesheetSubmittedClear.Id = 62;
            this.TimesheetSubmittedClear.Name = "TimesheetSubmittedClear";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Clear Team Timesheet Submitted - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to set selected callout(s) as <b>No</b> Team Timesheet Submitted.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.TimesheetSubmittedClear.SuperTip = superToolTip2;
            this.TimesheetSubmittedClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.TimesheetSubmittedClear_ItemClick);
            // 
            // bbiTeamInvoiceNo1
            // 
            this.bbiTeamInvoiceNo1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiTeamInvoiceNo1.Caption = "Team Invoice No";
            this.bbiTeamInvoiceNo1.DropDownControl = this.pmTeamInvoiceNo;
            this.bbiTeamInvoiceNo1.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiTeamInvoiceNo1.Glyph")));
            this.bbiTeamInvoiceNo1.Id = 59;
            this.bbiTeamInvoiceNo1.Name = "bbiTeamInvoiceNo1";
            this.bbiTeamInvoiceNo1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem6.Image")));
            toolTipTitleItem6.Text = "Team Invoice No - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to input a Team Invoice Number and\\or set Team Timesheet Submitted for t" +
    "he selected callout(s).";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiTeamInvoiceNo1.SuperTip = superToolTip6;
            this.bbiTeamInvoiceNo1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTeamInvoiceNo1_ItemClick);
            // 
            // pmTeamInvoiceNo
            // 
            this.pmTeamInvoiceNo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTeamInvcoiceNo2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTeamInvoiceNoClear, true)});
            this.pmTeamInvoiceNo.Manager = this.barManager1;
            this.pmTeamInvoiceNo.Name = "pmTeamInvoiceNo";
            // 
            // bbiTeamInvcoiceNo2
            // 
            this.bbiTeamInvcoiceNo2.Caption = "Set Team Invoice No.";
            this.bbiTeamInvcoiceNo2.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiTeamInvcoiceNo2.Glyph")));
            this.bbiTeamInvcoiceNo2.Id = 60;
            this.bbiTeamInvcoiceNo2.Name = "bbiTeamInvcoiceNo2";
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Set Team Invoice Number - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to input a Team Invoice Number for the selected callout(s).";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiTeamInvcoiceNo2.SuperTip = superToolTip4;
            this.bbiTeamInvcoiceNo2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTeamInvcoiceNo2_ItemClick);
            // 
            // bbiTeamInvoiceNoClear
            // 
            this.bbiTeamInvoiceNoClear.Caption = "Clear Team Invoice No.";
            this.bbiTeamInvoiceNoClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiTeamInvoiceNoClear.Glyph")));
            this.bbiTeamInvoiceNoClear.Id = 61;
            this.bbiTeamInvoiceNoClear.Name = "bbiTeamInvoiceNoClear";
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem5.Image")));
            toolTipTitleItem5.Text = "Clear Team Invoice Number - information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to clear the Team Invoice Number for the selected callout(s).";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiTeamInvoiceNoClear.SuperTip = superToolTip5;
            this.bbiTeamInvoiceNoClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTeamInvoiceNoClear_ItemClick);
            // 
            // bbiAuthorise1
            // 
            this.bbiAuthorise1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiAuthorise1.Caption = "Authorise";
            this.bbiAuthorise1.DropDownControl = this.pmAuthorise;
            this.bbiAuthorise1.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAuthorise1.Glyph")));
            this.bbiAuthorise1.Id = 37;
            this.bbiAuthorise1.Name = "bbiAuthorise1";
            this.bbiAuthorise1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem9.Image")));
            toolTipTitleItem9.Text = "Authorise Callout - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to mark the Callout record as Authorised for Payment.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiAuthorise1.SuperTip = superToolTip9;
            this.bbiAuthorise1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAuthorise1_ItemClick);
            // 
            // pmAuthorise
            // 
            this.pmAuthorise.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAuthorise2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUnAuthorise)});
            this.pmAuthorise.Manager = this.barManager1;
            this.pmAuthorise.MenuCaption = "Authorise Menu";
            this.pmAuthorise.Name = "pmAuthorise";
            // 
            // bbiAuthorise2
            // 
            this.bbiAuthorise2.Caption = "Authorise Callout";
            this.bbiAuthorise2.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAuthorise2.Glyph")));
            this.bbiAuthorise2.Id = 27;
            this.bbiAuthorise2.Name = "bbiAuthorise2";
            this.bbiAuthorise2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem7.Image")));
            toolTipTitleItem7.Text = "Authorise Callout - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to mark the Callout record as Authorised for Payment.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiAuthorise2.SuperTip = superToolTip7;
            this.bbiAuthorise2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAuthorise2_ItemClick);
            // 
            // bbiUnAuthorise
            // 
            this.bbiUnAuthorise.Caption = "Un-authorise Callout";
            this.bbiUnAuthorise.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiUnAuthorise.Glyph")));
            this.bbiUnAuthorise.Id = 29;
            this.bbiUnAuthorise.Name = "bbiUnAuthorise";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem8.Image")));
            toolTipTitleItem8.Text = "Un-authorise Callout - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to <b>clear</b> the Authorise Payment Status of the the Callout record.\r" +
    "\nThe Status will then be changed back to Completed.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiUnAuthorise.SuperTip = superToolTip8;
            this.bbiUnAuthorise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUnAuthorise_ItemClick);
            // 
            // bbiQuery1
            // 
            this.bbiQuery1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiQuery1.Caption = "Query";
            this.bbiQuery1.DropDownControl = this.pmQuery;
            this.bbiQuery1.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiQuery1.Glyph")));
            this.bbiQuery1.Id = 38;
            this.bbiQuery1.Name = "bbiQuery1";
            this.bbiQuery1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiQuery1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiQuery1_ItemClick);
            // 
            // pmQuery
            // 
            this.pmQuery.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiQuery2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiQueryClear)});
            this.pmQuery.Manager = this.barManager1;
            this.pmQuery.MenuCaption = "Query Menu";
            this.pmQuery.Name = "pmQuery";
            // 
            // bbiQuery2
            // 
            this.bbiQuery2.Caption = "Callout - Query";
            this.bbiQuery2.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiQuery2.Glyph")));
            this.bbiQuery2.Id = 31;
            this.bbiQuery2.Name = "bbiQuery2";
            this.bbiQuery2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiQuery2_ItemClick);
            // 
            // bbiQueryClear
            // 
            this.bbiQueryClear.Caption = "Callout - Clear Query";
            this.bbiQueryClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiQueryClear.Glyph")));
            this.bbiQueryClear.Id = 36;
            this.bbiQueryClear.Name = "bbiQueryClear";
            this.bbiQueryClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiQueryClear_ItemClick);
            // 
            // bbiDontPay1
            // 
            this.bbiDontPay1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiDontPay1.Caption = "Don\'t Pay";
            this.bbiDontPay1.DropDownControl = this.pmDoNotPay;
            this.bbiDontPay1.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDontPay1.Glyph")));
            this.bbiDontPay1.Id = 39;
            this.bbiDontPay1.Name = "bbiDontPay1";
            this.bbiDontPay1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiDontPay1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDontPay1_ItemClick);
            // 
            // pmDoNotPay
            // 
            this.pmDoNotPay.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDontPay2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDoPay)});
            this.pmDoNotPay.Manager = this.barManager1;
            this.pmDoNotPay.MenuCaption = "Don\'t Pay Menu";
            this.pmDoNotPay.Name = "pmDoNotPay";
            // 
            // bbiDontPay2
            // 
            this.bbiDontPay2.Caption = "Callout - Don\'t Pay";
            this.bbiDontPay2.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDontPay2.Glyph")));
            this.bbiDontPay2.Id = 34;
            this.bbiDontPay2.Name = "bbiDontPay2";
            this.bbiDontPay2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDontPay2_ItemClick);
            // 
            // bbiDoPay
            // 
            this.bbiDoPay.Caption = "Callout - Clear Don\'t Pay";
            this.bbiDoPay.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDoPay.Glyph")));
            this.bbiDoPay.Id = 35;
            this.bbiDoPay.Name = "bbiDoPay";
            this.bbiDoPay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDoPay_ItemClick);
            // 
            // barEditItem4
            // 
            this.barEditItem4.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItem4.Caption = "Parameters";
            this.barEditItem4.Edit = this.repositoryItemPopupContainerEdit3;
            this.barEditItem4.EditWidth = 20;
            this.barEditItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("barEditItem4.Glyph")));
            this.barEditItem4.Id = 58;
            this.barEditItem4.Name = "barEditItem4";
            this.barEditItem4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit3
            // 
            this.repositoryItemPopupContainerEdit3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEdit3.Appearance.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEdit3.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEdit3.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEdit3.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEdit3.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEdit3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemPopupContainerEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit3.Name = "repositoryItemPopupContainerEdit3";
            this.repositoryItemPopupContainerEdit3.PopupControl = this.popupContainerControl1;
            this.repositoryItemPopupContainerEdit3.PopupSizeable = false;
            this.repositoryItemPopupContainerEdit3.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControl1.Controls.Add(this.layoutControl1);
            this.popupContainerControl1.Location = new System.Drawing.Point(31, 151);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(286, 162);
            this.popupContainerControl1.TabIndex = 7;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(214, 132);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.Size = new System.Drawing.Size(64, 22);
            this.btnDateRangeOK.TabIndex = 14;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.CheckEditShowDontPay);
            this.layoutControl1.Controls.Add(this.CheckEditShowQueries);
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Location = new System.Drawing.Point(3, 3);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1013, 326, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(282, 127);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // CheckEditShowDontPay
            // 
            this.CheckEditShowDontPay.EditValue = 1;
            this.CheckEditShowDontPay.Location = new System.Drawing.Point(97, 106);
            this.CheckEditShowDontPay.MenuManager = this.barManager1;
            this.CheckEditShowDontPay.Name = "CheckEditShowDontPay";
            this.CheckEditShowDontPay.Properties.Caption = "(Tick if Yes)";
            this.CheckEditShowDontPay.Properties.ValueChecked = 1;
            this.CheckEditShowDontPay.Properties.ValueUnchecked = 0;
            this.CheckEditShowDontPay.Size = new System.Drawing.Size(153, 19);
            this.CheckEditShowDontPay.StyleController = this.layoutControl1;
            this.CheckEditShowDontPay.TabIndex = 14;
            // 
            // CheckEditShowQueries
            // 
            this.CheckEditShowQueries.EditValue = 1;
            this.CheckEditShowQueries.Location = new System.Drawing.Point(97, 83);
            this.CheckEditShowQueries.MenuManager = this.barManager1;
            this.CheckEditShowQueries.Name = "CheckEditShowQueries";
            this.CheckEditShowQueries.Properties.Caption = "(Tick if Yes)";
            this.CheckEditShowQueries.Properties.ValueChecked = 1;
            this.CheckEditShowQueries.Properties.ValueUnchecked = 0;
            this.CheckEditShowQueries.Size = new System.Drawing.Size(153, 19);
            this.CheckEditShowQueries.StyleController = this.layoutControl1;
            this.CheckEditShowQueries.TabIndex = 13;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(97, 59);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Clear Date - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ca" +
    "llouts will be loaded.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip10, true)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(153, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 12;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(97, 35);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "Clear Date - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ca" +
    "llouts will be loaded.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip11, true)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(153, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 11;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(265, 150);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 130);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(255, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Grid Parameters";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(255, 130);
            this.layoutControlGroup2.Text = "Grid Parameters";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateEditFromDate;
            this.layoutControlItem1.CustomizationFormText = "From Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(239, 24);
            this.layoutControlItem1.Text = "From Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEditToDate;
            this.layoutControlItem2.CustomizationFormText = "To Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(239, 24);
            this.layoutControlItem2.Text = "To Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.CheckEditShowQueries;
            this.layoutControlItem3.CustomizationFormText = "Show Queries:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(239, 23);
            this.layoutControlItem3.Text = "Show Queries:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.CheckEditShowDontPay;
            this.layoutControlItem4.CustomizationFormText = "Show Don\'t Pay:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 71);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(239, 23);
            this.layoutControlItem4.Text = "Show Don\'t Pay:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(79, 13);
            // 
            // bbiLoadData
            // 
            this.bbiLoadData.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiLoadData.Caption = "Reload Data";
            this.bbiLoadData.Glyph = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiLoadData.Id = 43;
            this.bbiLoadData.Name = "bbiLoadData";
            this.bbiLoadData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem12.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem12.Image")));
            toolTipTitleItem12.Text = "Reload Data - information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to reload the list of Snow Clearance Callouts.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.bbiLoadData.SuperTip = superToolTip12;
            this.bbiLoadData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadData_ItemClick);
            // 
            // barEditItemShowQueries
            // 
            this.barEditItemShowQueries.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItemShowQueries.Edit = this.repositoryItemCheckEditShowQueries;
            this.barEditItemShowQueries.EditWidth = 90;
            this.barEditItemShowQueries.Id = 48;
            this.barEditItemShowQueries.Name = "barEditItemShowQueries";
            // 
            // repositoryItemCheckEditShowQueries
            // 
            this.repositoryItemCheckEditShowQueries.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowQueries.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowQueries.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowQueries.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowQueries.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowQueries.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowQueries.AutoHeight = false;
            this.repositoryItemCheckEditShowQueries.Caption = "Show Queries:";
            this.repositoryItemCheckEditShowQueries.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowQueries.Name = "repositoryItemCheckEditShowQueries";
            this.repositoryItemCheckEditShowQueries.ValueChecked = 1;
            this.repositoryItemCheckEditShowQueries.ValueUnchecked = 0;
            // 
            // barEditItemShowDontPay
            // 
            this.barEditItemShowDontPay.Edit = this.repositoryItemCheckEditShowDontPay;
            this.barEditItemShowDontPay.EditWidth = 98;
            this.barEditItemShowDontPay.Id = 49;
            this.barEditItemShowDontPay.Name = "barEditItemShowDontPay";
            // 
            // repositoryItemCheckEditShowDontPay
            // 
            this.repositoryItemCheckEditShowDontPay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowDontPay.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowDontPay.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowDontPay.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowDontPay.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowDontPay.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowDontPay.AutoHeight = false;
            this.repositoryItemCheckEditShowDontPay.Caption = "Show Don\'t Pay:";
            this.repositoryItemCheckEditShowDontPay.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowDontPay.Name = "repositoryItemCheckEditShowDontPay";
            this.repositoryItemCheckEditShowDontPay.ValueChecked = 1;
            this.repositoryItemCheckEditShowDontPay.ValueUnchecked = 0;
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Query";
            this.barSubItem2.Id = 30;
            this.barSubItem2.Name = "barSubItem2";
            this.barSubItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Callout - Clear Query";
            this.barButtonItem4.Id = 32;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // bbiInvoiceSelectRecords
            // 
            this.bbiInvoiceSelectRecords.Caption = "Select Teams Requiring Invoice";
            this.bbiInvoiceSelectRecords.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiInvoiceSelectRecords.Glyph")));
            this.bbiInvoiceSelectRecords.Id = 42;
            this.bbiInvoiceSelectRecords.Name = "bbiInvoiceSelectRecords";
            // 
            // bbiInvoice2
            // 
            this.bbiInvoice2.Caption = "Invoice";
            this.bbiInvoice2.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiInvoice2.Glyph")));
            this.bbiInvoice2.Id = 41;
            this.bbiInvoice2.Name = "bbiInvoice2";
            superToolTip13.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem13.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem13.Image")));
            toolTipTitleItem13.Text = "Invoice - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to generate and send Self-Billing Invoices to <b>selected</b> Gritting T" +
    "eams.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.bbiInvoice2.SuperTip = superToolTip13;
            // 
            // barLinkContainerItem2
            // 
            this.barLinkContainerItem2.Caption = "barLinkContainerItem2";
            this.barLinkContainerItem2.Id = 44;
            this.barLinkContainerItem2.Name = "barLinkContainerItem2";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // barLinkContainerItem3
            // 
            this.barLinkContainerItem3.Caption = "barLinkContainerItem3";
            this.barLinkContainerItem3.Id = 46;
            this.barLinkContainerItem3.Name = "barLinkContainerItem3";
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 47;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // gridControl1
            // 
            this.gridControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl1.BackgroundImage")));
            this.gridControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gridControl1.DataSource = this.sp04197GCSnowCalloutManagerForAuthorisationBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 42);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditVat,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemPictureEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1109, 473);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp04197GCSnowCalloutManagerForAuthorisationBindingSource
            // 
            this.sp04197GCSnowCalloutManagerForAuthorisationBindingSource.DataMember = "sp04197_GC_Snow_Callout_Manager_For_Authorisation";
            this.sp04197GCSnowCalloutManagerForAuthorisationBindingSource.DataSource = this.dataSet_GC_Snow_DataEntry;
            // 
            // dataSet_GC_Snow_DataEntry
            // 
            this.dataSet_GC_Snow_DataEntry.DataSetName = "DataSet_GC_Snow_DataEntry";
            this.dataSet_GC_Snow_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSnowClearanceCallOutID,
            this.colSnowClearanceSiteContractID,
            this.colGCPONumberSuffix,
            this.colSiteID,
            this.colSiteName,
            this.colClientID,
            this.colClientName,
            this.colCompanyID,
            this.colCompanyName,
            this.colSiteXCoordinate,
            this.colSiteYCoordinate,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colSiteTelephone,
            this.colSiteEmail,
            this.colClientsSiteCode,
            this.colSubContractorID,
            this.colSubContractorName,
            this.colGritMobileTelephoneNumber,
            this.colOriginalSubContractorID,
            this.colOriginalSubContractorName,
            this.colReactive,
            this.colJobStatusID,
            this.colCalloutStatusDescription,
            this.colCalloutStatusOrder,
            this.colCallOutDateTime,
            this.colSubContractorETA,
            this.colCompletedTime,
            this.colRecordedByStaffID,
            this.colVisitAborted,
            this.colAbortedReason,
            this.colClientPONumber,
            this.colLabourCost,
            this.colLabourVatRate,
            this.colLabourSell,
            this.colOtherCost,
            this.colOtherSell,
            this.colTotalCost,
            this.colTotalSell,
            this.colClientInvoiceNumber,
            this.colSubContractorPaid,
            this.colRecordedByName,
            this.colSubContractorContactedByStaffID,
            this.colSubContractorContactedByStaffName,
            this.colPaidByStaffID,
            this.colPaidByName,
            this.colDoNotPaySubContractor,
            this.colDoNotPaySubContractorReason,
            this.colDoNotInvoiceClient,
            this.colDoNotInvoiceClientReason,
            this.colNoAccess,
            this.colStartTime,
            this.colProfit,
            this.colMarkup,
            this.colClientPOID,
            this.colNonStandardCost,
            this.colNonStandardSell,
            this.colAccessComments,
            this.colClientHowSoon,
            this.colHoursWorked,
            this.colRemarks2,
            this.colNoAccessAbortedRate,
            this.colTeamInvoiceNumber,
            this.colTeamPOFileName,
            this.colTimesheetSubmitted,
            this.Alert});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCallOutDateTime, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSnowClearanceCallOutID
            // 
            this.colSnowClearanceCallOutID.Caption = "GC PO No";
            this.colSnowClearanceCallOutID.FieldName = "SnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.Name = "colSnowClearanceCallOutID";
            this.colSnowClearanceCallOutID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceCallOutID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceCallOutID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceCallOutID.Visible = true;
            this.colSnowClearanceCallOutID.VisibleIndex = 3;
            this.colSnowClearanceCallOutID.Width = 71;
            // 
            // colSnowClearanceSiteContractID
            // 
            this.colSnowClearanceSiteContractID.Caption = "Snow Clearance Contract ID";
            this.colSnowClearanceSiteContractID.FieldName = "SnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.Name = "colSnowClearanceSiteContractID";
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSiteContractID.Width = 157;
            // 
            // colGCPONumberSuffix
            // 
            this.colGCPONumberSuffix.Caption = "GC PO Suffix";
            this.colGCPONumberSuffix.FieldName = "GCPONumberSuffix";
            this.colGCPONumberSuffix.Name = "colGCPONumberSuffix";
            this.colGCPONumberSuffix.OptionsColumn.AllowEdit = false;
            this.colGCPONumberSuffix.OptionsColumn.AllowFocus = false;
            this.colGCPONumberSuffix.OptionsColumn.ReadOnly = true;
            this.colGCPONumberSuffix.Visible = true;
            this.colGCPONumberSuffix.VisibleIndex = 4;
            this.colGCPONumberSuffix.Width = 84;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 8;
            this.colSiteName.Width = 157;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 7;
            this.colClientName.Width = 120;
            // 
            // colCompanyID
            // 
            this.colCompanyID.Caption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 80;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 18;
            this.colCompanyName.Width = 137;
            // 
            // colSiteXCoordinate
            // 
            this.colSiteXCoordinate.Caption = "Site X Coordinate";
            this.colSiteXCoordinate.FieldName = "SiteXCoordinate";
            this.colSiteXCoordinate.Name = "colSiteXCoordinate";
            this.colSiteXCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteXCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteXCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteXCoordinate.Width = 104;
            // 
            // colSiteYCoordinate
            // 
            this.colSiteYCoordinate.Caption = "Site Y Coordinate";
            this.colSiteYCoordinate.FieldName = "SiteYCoordinate";
            this.colSiteYCoordinate.Name = "colSiteYCoordinate";
            this.colSiteYCoordinate.OptionsColumn.AllowEdit = false;
            this.colSiteYCoordinate.OptionsColumn.AllowFocus = false;
            this.colSiteYCoordinate.OptionsColumn.ReadOnly = true;
            this.colSiteYCoordinate.Width = 104;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Location X";
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 91;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Location Y";
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 91;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Site Telephone ";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Width = 95;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Site Email";
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.AllowEdit = false;
            this.colSiteEmail.OptionsColumn.AllowFocus = false;
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            this.colSiteEmail.Width = 116;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Width = 105;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Team Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 6;
            this.colSubContractorName.Width = 122;
            // 
            // colGritMobileTelephoneNumber
            // 
            this.colGritMobileTelephoneNumber.Caption = "Team Mobile No";
            this.colGritMobileTelephoneNumber.FieldName = "GritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.Name = "colGritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.OptionsColumn.AllowEdit = false;
            this.colGritMobileTelephoneNumber.OptionsColumn.AllowFocus = false;
            this.colGritMobileTelephoneNumber.OptionsColumn.ReadOnly = true;
            this.colGritMobileTelephoneNumber.Width = 96;
            // 
            // colOriginalSubContractorID
            // 
            this.colOriginalSubContractorID.Caption = "Original Team ID";
            this.colOriginalSubContractorID.FieldName = "OriginalSubContractorID";
            this.colOriginalSubContractorID.Name = "colOriginalSubContractorID";
            this.colOriginalSubContractorID.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorID.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorID.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorID.Width = 100;
            // 
            // colOriginalSubContractorName
            // 
            this.colOriginalSubContractorName.Caption = "Original Team Name";
            this.colOriginalSubContractorName.FieldName = "OriginalSubContractorName";
            this.colOriginalSubContractorName.Name = "colOriginalSubContractorName";
            this.colOriginalSubContractorName.OptionsColumn.AllowEdit = false;
            this.colOriginalSubContractorName.OptionsColumn.AllowFocus = false;
            this.colOriginalSubContractorName.OptionsColumn.ReadOnly = true;
            this.colOriginalSubContractorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOriginalSubContractorName.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 5;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colCalloutStatusDescription
            // 
            this.colCalloutStatusDescription.Caption = "Status Description";
            this.colCalloutStatusDescription.FieldName = "CalloutStatusDescription";
            this.colCalloutStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCalloutStatusDescription.Name = "colCalloutStatusDescription";
            this.colCalloutStatusDescription.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusDescription.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusDescription.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCalloutStatusDescription.Visible = true;
            this.colCalloutStatusDescription.VisibleIndex = 1;
            this.colCalloutStatusDescription.Width = 196;
            // 
            // colCalloutStatusOrder
            // 
            this.colCalloutStatusOrder.Caption = "Status Order";
            this.colCalloutStatusOrder.FieldName = "CalloutStatusOrder";
            this.colCalloutStatusOrder.Name = "colCalloutStatusOrder";
            this.colCalloutStatusOrder.OptionsColumn.AllowEdit = false;
            this.colCalloutStatusOrder.OptionsColumn.AllowFocus = false;
            this.colCalloutStatusOrder.OptionsColumn.ReadOnly = true;
            this.colCalloutStatusOrder.Width = 83;
            // 
            // colCallOutDateTime
            // 
            this.colCallOutDateTime.Caption = "Callout Date\\Time";
            this.colCallOutDateTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCallOutDateTime.FieldName = "CallOutDateTime";
            this.colCallOutDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCallOutDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCallOutDateTime.Name = "colCallOutDateTime";
            this.colCallOutDateTime.OptionsColumn.AllowEdit = false;
            this.colCallOutDateTime.OptionsColumn.AllowFocus = false;
            this.colCallOutDateTime.OptionsColumn.ReadOnly = true;
            this.colCallOutDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCallOutDateTime.Visible = true;
            this.colCallOutDateTime.VisibleIndex = 2;
            this.colCallOutDateTime.Width = 121;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colSubContractorETA
            // 
            this.colSubContractorETA.Caption = "Team ETA";
            this.colSubContractorETA.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSubContractorETA.FieldName = "SubContractorETA";
            this.colSubContractorETA.Name = "colSubContractorETA";
            this.colSubContractorETA.OptionsColumn.AllowEdit = false;
            this.colSubContractorETA.OptionsColumn.AllowFocus = false;
            this.colSubContractorETA.OptionsColumn.ReadOnly = true;
            this.colSubContractorETA.Width = 117;
            // 
            // colCompletedTime
            // 
            this.colCompletedTime.Caption = "Completed Date\\Time";
            this.colCompletedTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCompletedTime.FieldName = "CompletedTime";
            this.colCompletedTime.Name = "colCompletedTime";
            this.colCompletedTime.OptionsColumn.AllowEdit = false;
            this.colCompletedTime.OptionsColumn.AllowFocus = false;
            this.colCompletedTime.OptionsColumn.ReadOnly = true;
            this.colCompletedTime.Visible = true;
            this.colCompletedTime.VisibleIndex = 16;
            this.colCompletedTime.Width = 124;
            // 
            // colRecordedByStaffID
            // 
            this.colRecordedByStaffID.Caption = "Recorded By Staff ID";
            this.colRecordedByStaffID.FieldName = "RecordedByStaffID";
            this.colRecordedByStaffID.Name = "colRecordedByStaffID";
            this.colRecordedByStaffID.OptionsColumn.AllowEdit = false;
            this.colRecordedByStaffID.OptionsColumn.AllowFocus = false;
            this.colRecordedByStaffID.OptionsColumn.ReadOnly = true;
            this.colRecordedByStaffID.Width = 123;
            // 
            // colVisitAborted
            // 
            this.colVisitAborted.Caption = "Aborted";
            this.colVisitAborted.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colVisitAborted.FieldName = "VisitAborted";
            this.colVisitAborted.Name = "colVisitAborted";
            this.colVisitAborted.OptionsColumn.AllowEdit = false;
            this.colVisitAborted.OptionsColumn.AllowFocus = false;
            this.colVisitAborted.OptionsColumn.ReadOnly = true;
            this.colVisitAborted.Visible = true;
            this.colVisitAborted.VisibleIndex = 13;
            // 
            // colAbortedReason
            // 
            this.colAbortedReason.Caption = "Aborted Reason";
            this.colAbortedReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colAbortedReason.FieldName = "AbortedReason";
            this.colAbortedReason.Name = "colAbortedReason";
            this.colAbortedReason.OptionsColumn.ReadOnly = true;
            this.colAbortedReason.Visible = true;
            this.colAbortedReason.VisibleIndex = 14;
            this.colAbortedReason.Width = 99;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO Number";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 11;
            this.colClientPONumber.Width = 105;
            // 
            // colLabourCost
            // 
            this.colLabourCost.Caption = "Labour Cost";
            this.colLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colLabourCost.FieldName = "LabourCost";
            this.colLabourCost.Name = "colLabourCost";
            this.colLabourCost.OptionsColumn.AllowEdit = false;
            this.colLabourCost.OptionsColumn.AllowFocus = false;
            this.colLabourCost.OptionsColumn.ReadOnly = true;
            this.colLabourCost.Visible = true;
            this.colLabourCost.VisibleIndex = 20;
            this.colLabourCost.Width = 79;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colLabourVatRate
            // 
            this.colLabourVatRate.Caption = "Labour VAT Rate";
            this.colLabourVatRate.ColumnEdit = this.repositoryItemTextEditVat;
            this.colLabourVatRate.FieldName = "LabourVatRate";
            this.colLabourVatRate.Name = "colLabourVatRate";
            this.colLabourVatRate.OptionsColumn.AllowEdit = false;
            this.colLabourVatRate.OptionsColumn.AllowFocus = false;
            this.colLabourVatRate.OptionsColumn.ReadOnly = true;
            this.colLabourVatRate.Visible = true;
            this.colLabourVatRate.VisibleIndex = 21;
            this.colLabourVatRate.Width = 102;
            // 
            // repositoryItemTextEditVat
            // 
            this.repositoryItemTextEditVat.AutoHeight = false;
            this.repositoryItemTextEditVat.Mask.EditMask = "P2";
            this.repositoryItemTextEditVat.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditVat.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditVat.Name = "repositoryItemTextEditVat";
            // 
            // colLabourSell
            // 
            this.colLabourSell.Caption = "Labour Sell";
            this.colLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colLabourSell.FieldName = "LabourSell";
            this.colLabourSell.Name = "colLabourSell";
            this.colLabourSell.OptionsColumn.AllowEdit = false;
            this.colLabourSell.OptionsColumn.AllowFocus = false;
            this.colLabourSell.OptionsColumn.ReadOnly = true;
            this.colLabourSell.Visible = true;
            this.colLabourSell.VisibleIndex = 22;
            // 
            // colOtherCost
            // 
            this.colOtherCost.Caption = "Other Cost";
            this.colOtherCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherCost.FieldName = "OtherCost";
            this.colOtherCost.Name = "colOtherCost";
            this.colOtherCost.OptionsColumn.AllowEdit = false;
            this.colOtherCost.OptionsColumn.AllowFocus = false;
            this.colOtherCost.OptionsColumn.ReadOnly = true;
            this.colOtherCost.Visible = true;
            this.colOtherCost.VisibleIndex = 23;
            // 
            // colOtherSell
            // 
            this.colOtherSell.Caption = "Other Sell";
            this.colOtherSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colOtherSell.FieldName = "OtherSell";
            this.colOtherSell.Name = "colOtherSell";
            this.colOtherSell.OptionsColumn.AllowEdit = false;
            this.colOtherSell.OptionsColumn.AllowFocus = false;
            this.colOtherSell.OptionsColumn.ReadOnly = true;
            this.colOtherSell.Visible = true;
            this.colOtherSell.VisibleIndex = 24;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 25;
            // 
            // colTotalSell
            // 
            this.colTotalSell.Caption = "Total Sell ";
            this.colTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalSell.FieldName = "TotalSell";
            this.colTotalSell.Name = "colTotalSell";
            this.colTotalSell.OptionsColumn.AllowEdit = false;
            this.colTotalSell.OptionsColumn.AllowFocus = false;
            this.colTotalSell.OptionsColumn.ReadOnly = true;
            this.colTotalSell.Visible = true;
            this.colTotalSell.VisibleIndex = 26;
            // 
            // colClientInvoiceNumber
            // 
            this.colClientInvoiceNumber.Caption = "Client Invoice Number";
            this.colClientInvoiceNumber.FieldName = "ClientInvoiceNumber";
            this.colClientInvoiceNumber.Name = "colClientInvoiceNumber";
            this.colClientInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientInvoiceNumber.Visible = true;
            this.colClientInvoiceNumber.VisibleIndex = 31;
            this.colClientInvoiceNumber.Width = 126;
            // 
            // colSubContractorPaid
            // 
            this.colSubContractorPaid.Caption = "Team Paid";
            this.colSubContractorPaid.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSubContractorPaid.FieldName = "SubContractorPaid";
            this.colSubContractorPaid.Name = "colSubContractorPaid";
            this.colSubContractorPaid.OptionsColumn.AllowEdit = false;
            this.colSubContractorPaid.OptionsColumn.AllowFocus = false;
            this.colSubContractorPaid.OptionsColumn.ReadOnly = true;
            this.colSubContractorPaid.Visible = true;
            this.colSubContractorPaid.VisibleIndex = 32;
            // 
            // colRecordedByName
            // 
            this.colRecordedByName.Caption = "Recorded By Name";
            this.colRecordedByName.FieldName = "RecordedByName";
            this.colRecordedByName.Name = "colRecordedByName";
            this.colRecordedByName.OptionsColumn.AllowEdit = false;
            this.colRecordedByName.OptionsColumn.AllowFocus = false;
            this.colRecordedByName.OptionsColumn.ReadOnly = true;
            this.colRecordedByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRecordedByName.Width = 112;
            // 
            // colSubContractorContactedByStaffID
            // 
            this.colSubContractorContactedByStaffID.Caption = "Team Contacted By Staff ID";
            this.colSubContractorContactedByStaffID.FieldName = "SubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.Name = "colSubContractorContactedByStaffID";
            this.colSubContractorContactedByStaffID.OptionsColumn.AllowEdit = false;
            this.colSubContractorContactedByStaffID.OptionsColumn.AllowFocus = false;
            this.colSubContractorContactedByStaffID.OptionsColumn.ReadOnly = true;
            this.colSubContractorContactedByStaffID.Width = 156;
            // 
            // colSubContractorContactedByStaffName
            // 
            this.colSubContractorContactedByStaffName.Caption = "Team Contacted By Staff Name";
            this.colSubContractorContactedByStaffName.FieldName = "SubContractorContactedByStaffName";
            this.colSubContractorContactedByStaffName.Name = "colSubContractorContactedByStaffName";
            this.colSubContractorContactedByStaffName.OptionsColumn.AllowEdit = false;
            this.colSubContractorContactedByStaffName.OptionsColumn.AllowFocus = false;
            this.colSubContractorContactedByStaffName.OptionsColumn.ReadOnly = true;
            this.colSubContractorContactedByStaffName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubContractorContactedByStaffName.Width = 172;
            // 
            // colPaidByStaffID
            // 
            this.colPaidByStaffID.Caption = "Paid By Staff ID";
            this.colPaidByStaffID.FieldName = "PaidByStaffID";
            this.colPaidByStaffID.Name = "colPaidByStaffID";
            this.colPaidByStaffID.OptionsColumn.AllowEdit = false;
            this.colPaidByStaffID.OptionsColumn.AllowFocus = false;
            this.colPaidByStaffID.OptionsColumn.ReadOnly = true;
            this.colPaidByStaffID.Width = 97;
            // 
            // colPaidByName
            // 
            this.colPaidByName.Caption = "Paid By Staff Name";
            this.colPaidByName.FieldName = "PaidByName";
            this.colPaidByName.Name = "colPaidByName";
            this.colPaidByName.OptionsColumn.AllowEdit = false;
            this.colPaidByName.OptionsColumn.AllowFocus = false;
            this.colPaidByName.OptionsColumn.ReadOnly = true;
            this.colPaidByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPaidByName.Visible = true;
            this.colPaidByName.VisibleIndex = 33;
            this.colPaidByName.Width = 113;
            // 
            // colDoNotPaySubContractor
            // 
            this.colDoNotPaySubContractor.Caption = "Do Not Pay Team";
            this.colDoNotPaySubContractor.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotPaySubContractor.FieldName = "DoNotPaySubContractor";
            this.colDoNotPaySubContractor.Name = "colDoNotPaySubContractor";
            this.colDoNotPaySubContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPaySubContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPaySubContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractor.Visible = true;
            this.colDoNotPaySubContractor.VisibleIndex = 34;
            this.colDoNotPaySubContractor.Width = 104;
            // 
            // colDoNotPaySubContractorReason
            // 
            this.colDoNotPaySubContractorReason.Caption = "Do Not Pay Team Reason";
            this.colDoNotPaySubContractorReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDoNotPaySubContractorReason.FieldName = "DoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.Name = "colDoNotPaySubContractorReason";
            this.colDoNotPaySubContractorReason.OptionsColumn.ReadOnly = true;
            this.colDoNotPaySubContractorReason.Visible = true;
            this.colDoNotPaySubContractorReason.VisibleIndex = 35;
            this.colDoNotPaySubContractorReason.Width = 143;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 36;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colDoNotInvoiceClientReason
            // 
            this.colDoNotInvoiceClientReason.Caption = "Do Not Invoice Client Reason";
            this.colDoNotInvoiceClientReason.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDoNotInvoiceClientReason.FieldName = "DoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.Name = "colDoNotInvoiceClientReason";
            this.colDoNotInvoiceClientReason.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClientReason.Visible = true;
            this.colDoNotInvoiceClientReason.VisibleIndex = 37;
            this.colDoNotInvoiceClientReason.Width = 161;
            // 
            // colNoAccess
            // 
            this.colNoAccess.Caption = "No Access";
            this.colNoAccess.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNoAccess.FieldName = "NoAccess";
            this.colNoAccess.Name = "colNoAccess";
            this.colNoAccess.OptionsColumn.AllowEdit = false;
            this.colNoAccess.OptionsColumn.AllowFocus = false;
            this.colNoAccess.OptionsColumn.ReadOnly = true;
            this.colNoAccess.Visible = true;
            this.colNoAccess.VisibleIndex = 12;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Date\\Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 15;
            this.colStartTime.Width = 105;
            // 
            // colProfit
            // 
            this.colProfit.Caption = "Profit";
            this.colProfit.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colProfit.FieldName = "Profit";
            this.colProfit.Name = "colProfit";
            this.colProfit.OptionsColumn.AllowEdit = false;
            this.colProfit.OptionsColumn.AllowFocus = false;
            this.colProfit.OptionsColumn.ReadOnly = true;
            this.colProfit.Visible = true;
            this.colProfit.VisibleIndex = 27;
            // 
            // colMarkup
            // 
            this.colMarkup.Caption = "% Markup";
            this.colMarkup.ColumnEdit = this.repositoryItemTextEditVat;
            this.colMarkup.FieldName = "Markup";
            this.colMarkup.Name = "colMarkup";
            this.colMarkup.OptionsColumn.AllowEdit = false;
            this.colMarkup.OptionsColumn.AllowFocus = false;
            this.colMarkup.OptionsColumn.ReadOnly = true;
            this.colMarkup.Visible = true;
            this.colMarkup.VisibleIndex = 28;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 79;
            // 
            // colNonStandardCost
            // 
            this.colNonStandardCost.Caption = "Non Standard Cost";
            this.colNonStandardCost.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNonStandardCost.FieldName = "NonStandardCost";
            this.colNonStandardCost.Name = "colNonStandardCost";
            this.colNonStandardCost.OptionsColumn.AllowEdit = false;
            this.colNonStandardCost.OptionsColumn.AllowFocus = false;
            this.colNonStandardCost.OptionsColumn.ReadOnly = true;
            this.colNonStandardCost.Visible = true;
            this.colNonStandardCost.VisibleIndex = 29;
            this.colNonStandardCost.Width = 112;
            // 
            // colNonStandardSell
            // 
            this.colNonStandardSell.Caption = "Non Standard Sell";
            this.colNonStandardSell.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNonStandardSell.FieldName = "NonStandardSell";
            this.colNonStandardSell.Name = "colNonStandardSell";
            this.colNonStandardSell.OptionsColumn.AllowEdit = false;
            this.colNonStandardSell.OptionsColumn.AllowFocus = false;
            this.colNonStandardSell.OptionsColumn.ReadOnly = true;
            this.colNonStandardSell.Visible = true;
            this.colNonStandardSell.VisibleIndex = 30;
            this.colNonStandardSell.Width = 106;
            // 
            // colAccessComments
            // 
            this.colAccessComments.Caption = "Access Comments";
            this.colAccessComments.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colAccessComments.FieldName = "AccessComments";
            this.colAccessComments.Name = "colAccessComments";
            this.colAccessComments.OptionsColumn.ReadOnly = true;
            this.colAccessComments.Width = 107;
            // 
            // colClientHowSoon
            // 
            this.colClientHowSoon.Caption = "How Soon";
            this.colClientHowSoon.FieldName = "ClientHowSoon";
            this.colClientHowSoon.Name = "colClientHowSoon";
            this.colClientHowSoon.OptionsColumn.AllowEdit = false;
            this.colClientHowSoon.OptionsColumn.AllowFocus = false;
            this.colClientHowSoon.OptionsColumn.ReadOnly = true;
            // 
            // colHoursWorked
            // 
            this.colHoursWorked.Caption = "Hours Worked";
            this.colHoursWorked.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colHoursWorked.FieldName = "HoursWorked";
            this.colHoursWorked.Name = "colHoursWorked";
            this.colHoursWorked.OptionsColumn.AllowEdit = false;
            this.colHoursWorked.OptionsColumn.AllowFocus = false;
            this.colHoursWorked.OptionsColumn.ReadOnly = true;
            this.colHoursWorked.Visible = true;
            this.colHoursWorked.VisibleIndex = 17;
            this.colHoursWorked.Width = 89;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 39;
            // 
            // colNoAccessAbortedRate
            // 
            this.colNoAccessAbortedRate.Caption = "No Access \\ Aborted Rate";
            this.colNoAccessAbortedRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colNoAccessAbortedRate.FieldName = "NoAccessAbortedRate";
            this.colNoAccessAbortedRate.Name = "colNoAccessAbortedRate";
            this.colNoAccessAbortedRate.OptionsColumn.AllowEdit = false;
            this.colNoAccessAbortedRate.OptionsColumn.AllowFocus = false;
            this.colNoAccessAbortedRate.OptionsColumn.ReadOnly = true;
            this.colNoAccessAbortedRate.Visible = true;
            this.colNoAccessAbortedRate.VisibleIndex = 19;
            this.colNoAccessAbortedRate.Width = 145;
            // 
            // colTeamInvoiceNumber
            // 
            this.colTeamInvoiceNumber.Caption = "Team Invoice No.";
            this.colTeamInvoiceNumber.FieldName = "TeamInvoiceNumber";
            this.colTeamInvoiceNumber.Name = "colTeamInvoiceNumber";
            this.colTeamInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colTeamInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colTeamInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colTeamInvoiceNumber.Visible = true;
            this.colTeamInvoiceNumber.VisibleIndex = 10;
            this.colTeamInvoiceNumber.Width = 105;
            // 
            // colTeamPOFileName
            // 
            this.colTeamPOFileName.Caption = "Team P.O. File";
            this.colTeamPOFileName.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colTeamPOFileName.FieldName = "TeamPOFileName";
            this.colTeamPOFileName.Name = "colTeamPOFileName";
            this.colTeamPOFileName.Visible = true;
            this.colTeamPOFileName.VisibleIndex = 38;
            this.colTeamPOFileName.Width = 91;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colTimesheetSubmitted
            // 
            this.colTimesheetSubmitted.Caption = "Timesheet Submitted";
            this.colTimesheetSubmitted.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTimesheetSubmitted.FieldName = "TimesheetSubmitted";
            this.colTimesheetSubmitted.Name = "colTimesheetSubmitted";
            this.colTimesheetSubmitted.OptionsColumn.AllowEdit = false;
            this.colTimesheetSubmitted.OptionsColumn.AllowFocus = false;
            this.colTimesheetSubmitted.OptionsColumn.ReadOnly = true;
            this.colTimesheetSubmitted.Visible = true;
            this.colTimesheetSubmitted.VisibleIndex = 9;
            this.colTimesheetSubmitted.Width = 121;
            // 
            // Alert
            // 
            this.Alert.ColumnEdit = this.repositoryItemPictureEdit1;
            this.Alert.CustomizationCaption = "Alert";
            this.Alert.FieldName = "Alert";
            this.Alert.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.Alert.Name = "Alert";
            this.Alert.OptionsColumn.AllowEdit = false;
            this.Alert.OptionsColumn.AllowFocus = false;
            this.Alert.OptionsColumn.AllowSize = false;
            this.Alert.OptionsColumn.FixedWidth = true;
            this.Alert.OptionsColumn.ReadOnly = true;
            this.Alert.OptionsColumn.ShowInExpressionEditor = false;
            this.Alert.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Alert.Visible = true;
            this.Alert.VisibleIndex = 0;
            this.Alert.Width = 20;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "From Date:";
            this.barEditItem1.Edit = this.repositoryItemDateEdit2;
            this.barEditItem1.Id = 55;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "To Date:";
            this.barEditItem2.Edit = this.repositoryItemDateEdit3;
            this.barEditItem2.Id = 56;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // barEditItem3
            // 
            this.barEditItem3.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItem3.Caption = "Date Range";
            this.barEditItem3.Edit = this.repositoryItemPopupContainerEdit2;
            this.barEditItem3.EditWidth = 20;
            this.barEditItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("barEditItem3.Glyph")));
            this.barEditItem3.Id = 57;
            this.barEditItem3.ItemAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.barEditItem3.ItemAppearance.Normal.Options.UseBackColor = true;
            this.barEditItem3.Name = "barEditItem3";
            this.barEditItem3.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemPopupContainerEdit2.Appearance.Options.UseBackColor = true;
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            this.repositoryItemPopupContainerEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // sp04197_GC_Snow_Callout_Manager_For_AuthorisationTableAdapter
            // 
            this.sp04197_GC_Snow_Callout_Manager_For_AuthorisationTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_GC_Snow_Authorise_Callouts
            // 
            this.ClientSize = new System.Drawing.Size(1109, 515);
            this.Controls.Add(this.popupContainerControl1);
            this.Controls.Add(this.gridControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Snow_Authorise_Callouts";
            this.Text = "Authorise SNOW CLEARANCE Callouts";
            this.Activated += new System.EventHandler(this.frm_GC_Snow_Authorise_Callouts_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Snow_Authorise_Callouts_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Snow_Authorise_Callouts_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            this.Controls.SetChildIndex(this.popupContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmTimesheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmTeamInvoiceNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmAuthorise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmQuery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDoNotPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditShowDontPay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditShowQueries.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowQueries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowDontPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04197GCSnowCalloutManagerForAuthorisationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAuthorise2;
        private DevExpress.XtraBars.BarButtonItem bbiUnAuthorise;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem bbiQuery2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem bbiDontPay2;
        private DevExpress.XtraBars.BarButtonItem bbiDoPay;
        private DevExpress.XtraBars.BarButtonItem bbiQueryClear;
        private DevExpress.XtraBars.BarButtonItem bbiAuthorise1;
        private DevExpress.XtraBars.PopupMenu pmAuthorise;
        private DevExpress.XtraBars.BarButtonItem bbiQuery1;
        private DevExpress.XtraBars.PopupMenu pmQuery;
        private DevExpress.XtraBars.BarButtonItem bbiDontPay1;
        private DevExpress.XtraBars.PopupMenu pmDoNotPay;
        private DevExpress.XtraBars.BarButtonItem bbiInvoiceSelectRecords;
        private DevExpress.XtraBars.BarButtonItem bbiInvoice2;
        private DevExpress.XtraBars.BarButtonItem bbiLoadData;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem2;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem3;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.BarEditItem barEditItemShowQueries;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowQueries;
        private DevExpress.XtraBars.BarEditItem barEditItemShowDontPay;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowDontPay;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraBars.BarEditItem barEditItem4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraBars.BarEditItem barEditItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceCallOutID;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCPONumberSuffix;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colGritMobileTelephoneNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutStatusOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCallOutDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorETA;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedTime;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitAborted;
        private DevExpress.XtraGrid.Columns.GridColumn colAbortedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCost;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherSell;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorContactedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidByName;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPaySubContractorReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClientReason;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colProfit;
        private DevExpress.XtraGrid.Columns.GridColumn colMarkup;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNonStandardSell;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientHowSoon;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colNoAccessAbortedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamPOFileName;
        private System.Windows.Forms.BindingSource sp04197GCSnowCalloutManagerForAuthorisationBindingSource;
        private DataSet_GC_Snow_DataEntry dataSet_GC_Snow_DataEntry;
        private DataSet_GC_Snow_DataEntryTableAdapters.sp04197_GC_Snow_Callout_Manager_For_AuthorisationTableAdapter sp04197_GC_Snow_Callout_Manager_For_AuthorisationTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditVat;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraBars.BarButtonItem bbiTeamInvoiceNo1;
        private DevExpress.XtraBars.PopupMenu pmTeamInvoiceNo;
        private DevExpress.XtraBars.BarButtonItem bbiTeamInvcoiceNo2;
        private DevExpress.XtraBars.BarButtonItem bbiTeamInvoiceNoClear;
        private DevExpress.XtraGrid.Columns.GridColumn colTimesheetSubmitted;
        private DevExpress.XtraBars.BarButtonItem bbiTimesheetSubmitted;
        private DevExpress.XtraBars.BarButtonItem TimesheetSubmittedClear;
        private DevExpress.XtraBars.PopupMenu pmTimesheet;
        private DevExpress.XtraBars.BarButtonItem TimesheetSubmitted2;
        private DevExpress.XtraEditors.CheckEdit CheckEditShowDontPay;
        private DevExpress.XtraEditors.CheckEdit CheckEditShowQueries;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn Alert;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
    }
}

namespace WoodPlan5
{
    partial class frm_GC_Team_Gritting_Information_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Team_Gritting_Information_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.colValue2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.AutoAllocateGritJobsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.sp04033GCTeamGrittingInformationEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.gridLookUpEditMissingJobSheetDeliveryMethodID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04276GCMissingJobSheetNumbersDeliveryMethodsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescriptor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CalloutUsedGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.OrderedInGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TransferredOutGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TransferredInGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.StartingGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GritJobTransferMethodCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.HourlyReactiveRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.HourlyProactiveRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.IsDirectLabourCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WarningLabel = new DevExpress.XtraEditors.LabelControl();
            this.SelfBillingFrequencySpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StockSuppliedByDepotIDGridLookupEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04036GCStockDepotsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebSite1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarningGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUrgentGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UrgentGritLevelSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WarningGritLevelSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CurrentGritLevelSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.SubContractorGritInformationIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SelfBillingInvoiceCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.HoldsStockCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SubContractorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04026ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalCOntractorDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GrittingActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GritMobileTelephoneNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForSubContractorGritInformationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForHoldsStock = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartingGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStockSuppliedByDepotID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTransferredInGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransferredOutGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOrderedInGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCalloutUsedGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWarningGritLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUrgentGritLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSelfBillingInvoice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSelfBillingFrequency = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSelfBillingFrequencyDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWarningLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForHourlyProactiveRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForGritMobileTelephoneNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritJobTransferMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMissingJobSheetDeliveryMethodID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAutoAllocateGritJobs = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForHourlyReactiveRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsDirectLabour = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp04026_Contractor_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04026_Contractor_List_With_BlankTableAdapter();
            this.sp04033_GC_Team_Gritting_Information_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04033_GC_Team_Gritting_Information_EditTableAdapter();
            this.sp04036_GC_Stock_Depots_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04036_GC_Stock_Depots_With_BlankTableAdapter();
            this.sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter();
            this.sp04276_GC_Missing_Job_Sheet_Numbers_Delivery_Methods_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04276_GC_Missing_Job_Sheet_Numbers_Delivery_Methods_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AutoAllocateGritJobsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04033GCTeamGrittingInformationEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMissingJobSheetDeliveryMethodID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04276GCMissingJobSheetNumbersDeliveryMethodsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalloutUsedGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderedInGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredOutGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredInGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartingGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritJobTransferMethodCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourlyReactiveRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourlyProactiveRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsDirectLabourCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingFrequencySpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StockSuppliedByDepotIDGridLookupEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04036GCStockDepotsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UrgentGritLevelSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WarningGritLevelSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentGritLevelSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorGritInformationIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoldsStockCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04026ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritMobileTelephoneNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorGritInformationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoldsStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartingGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStockSuppliedByDepotID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredInGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredOutGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderedInGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalloutUsedGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningGritLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUrgentGritLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingFrequencyDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHourlyProactiveRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritMobileTelephoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritJobTransferMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMissingJobSheetDeliveryMethodID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAutoAllocateGritJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHourlyReactiveRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsDirectLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(718, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 507);
            this.barDockControlBottom.Size = new System.Drawing.Size(718, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(718, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 481);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colValue2
            // 
            this.colValue2.Caption = "Value";
            this.colValue2.FieldName = "Value";
            this.colValue2.Name = "colValue2";
            this.colValue2.OptionsColumn.AllowEdit = false;
            this.colValue2.OptionsColumn.AllowFocus = false;
            this.colValue2.OptionsColumn.ReadOnly = true;
            // 
            // colValue1
            // 
            this.colValue1.Caption = "Descriptor ID";
            this.colValue1.FieldName = "Value";
            this.colValue1.Name = "colValue1";
            this.colValue1.OptionsColumn.AllowEdit = false;
            this.colValue1.OptionsColumn.AllowFocus = false;
            this.colValue1.OptionsColumn.ReadOnly = true;
            this.colValue1.Width = 96;
            // 
            // colValue
            // 
            this.colValue.Caption = "Depot ID";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(718, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 507);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(718, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 481);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(718, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 481);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.AutoAllocateGritJobsCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.gridLookUpEditMissingJobSheetDeliveryMethodID);
            this.dataLayoutControl1.Controls.Add(this.CalloutUsedGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.OrderedInGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TransferredOutGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TransferredInGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.StartingGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GritJobTransferMethodCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.HourlyReactiveRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.HourlyProactiveRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.IsDirectLabourCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WarningLabel);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingFrequencySpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingFrequencyDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.StockSuppliedByDepotIDGridLookupEdit);
            this.dataLayoutControl1.Controls.Add(this.UrgentGritLevelSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.WarningGritLevelSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CurrentGritLevelSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SubContractorGritInformationIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingInvoiceCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.HoldsStockCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GritMobileTelephoneNumberTextEdit);
            this.dataLayoutControl1.DataSource = this.sp04033GCTeamGrittingInformationEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSubContractorGritInformationID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1411, 366, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(718, 481);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // AutoAllocateGritJobsCheckEdit
            // 
            this.AutoAllocateGritJobsCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "AutoAllocateGritJobs", true));
            this.AutoAllocateGritJobsCheckEdit.EditValue = 0;
            this.AutoAllocateGritJobsCheckEdit.Location = new System.Drawing.Point(183, 210);
            this.AutoAllocateGritJobsCheckEdit.MenuManager = this.barManager1;
            this.AutoAllocateGritJobsCheckEdit.Name = "AutoAllocateGritJobsCheckEdit";
            this.AutoAllocateGritJobsCheckEdit.Properties.Caption = "(Tick if Yes  -  Default Device needs to be set on Site Contract Prefrred Teams s" +
    "creen)";
            this.AutoAllocateGritJobsCheckEdit.Properties.ValueChecked = 1;
            this.AutoAllocateGritJobsCheckEdit.Properties.ValueUnchecked = 0;
            this.AutoAllocateGritJobsCheckEdit.Size = new System.Drawing.Size(433, 19);
            this.AutoAllocateGritJobsCheckEdit.StyleController = this.dataLayoutControl1;
            this.AutoAllocateGritJobsCheckEdit.TabIndex = 33;
            // 
            // sp04033GCTeamGrittingInformationEditBindingSource
            // 
            this.sp04033GCTeamGrittingInformationEditBindingSource.DataMember = "sp04033_GC_Team_Gritting_Information_Edit";
            this.sp04033GCTeamGrittingInformationEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEditMissingJobSheetDeliveryMethodID
            // 
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "MissingJobSheetDeliveryMethodID", true));
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.Location = new System.Drawing.Point(183, 233);
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.MenuManager = this.barManager1;
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.Name = "gridLookUpEditMissingJobSheetDeliveryMethodID";
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.Properties.DataSource = this.sp04276GCMissingJobSheetNumbersDeliveryMethodsWithBlankBindingSource;
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.Properties.DisplayMember = "Descriptor";
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.Properties.NullText = "";
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.Properties.PopupView = this.gridView3;
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.Properties.ValueMember = "Value";
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.Size = new System.Drawing.Size(506, 20);
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.StyleController = this.dataLayoutControl1;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Missing Job Sheet Number Method - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I store the method used to send Missing Job Sheet Numbers to Teams.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.SuperTip = superToolTip4;
            this.gridLookUpEditMissingJobSheetDeliveryMethodID.TabIndex = 32;
            // 
            // sp04276GCMissingJobSheetNumbersDeliveryMethodsWithBlankBindingSource
            // 
            this.sp04276GCMissingJobSheetNumbersDeliveryMethodsWithBlankBindingSource.DataMember = "sp04276_GC_Missing_Job_Sheet_Numbers_Delivery_Methods_With_Blank";
            this.sp04276GCMissingJobSheetNumbersDeliveryMethodsWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescriptor1,
            this.colOrder1,
            this.colValue2});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colValue2;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescriptor1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescriptor1
            // 
            this.colDescriptor1.Caption = "Description";
            this.colDescriptor1.FieldName = "Descriptor";
            this.colDescriptor1.Name = "colDescriptor1";
            this.colDescriptor1.OptionsColumn.AllowEdit = false;
            this.colDescriptor1.OptionsColumn.AllowFocus = false;
            this.colDescriptor1.OptionsColumn.ReadOnly = true;
            this.colDescriptor1.Visible = true;
            this.colDescriptor1.VisibleIndex = 0;
            this.colDescriptor1.Width = 232;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.AllowFocus = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            // 
            // CalloutUsedGritAmountSpinEdit
            // 
            this.CalloutUsedGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "CalloutUsedGritAmount", true));
            this.CalloutUsedGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CalloutUsedGritAmountSpinEdit.Location = new System.Drawing.Point(219, 526);
            this.CalloutUsedGritAmountSpinEdit.MenuManager = this.barManager1;
            this.CalloutUsedGritAmountSpinEdit.Name = "CalloutUsedGritAmountSpinEdit";
            this.CalloutUsedGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CalloutUsedGritAmountSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.CalloutUsedGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CalloutUsedGritAmountSpinEdit.Properties.ReadOnly = true;
            this.CalloutUsedGritAmountSpinEdit.Size = new System.Drawing.Size(174, 20);
            this.CalloutUsedGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.CalloutUsedGritAmountSpinEdit.TabIndex = 31;
            // 
            // OrderedInGritAmountSpinEdit
            // 
            this.OrderedInGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "OrderedInGritAmount", true));
            this.OrderedInGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OrderedInGritAmountSpinEdit.Location = new System.Drawing.Point(219, 502);
            this.OrderedInGritAmountSpinEdit.MenuManager = this.barManager1;
            this.OrderedInGritAmountSpinEdit.Name = "OrderedInGritAmountSpinEdit";
            this.OrderedInGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OrderedInGritAmountSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.OrderedInGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OrderedInGritAmountSpinEdit.Properties.ReadOnly = true;
            this.OrderedInGritAmountSpinEdit.Size = new System.Drawing.Size(174, 20);
            this.OrderedInGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.OrderedInGritAmountSpinEdit.TabIndex = 30;
            // 
            // TransferredOutGritAmountSpinEdit
            // 
            this.TransferredOutGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "TransferredOutGritAmount", true));
            this.TransferredOutGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TransferredOutGritAmountSpinEdit.Location = new System.Drawing.Point(219, 478);
            this.TransferredOutGritAmountSpinEdit.MenuManager = this.barManager1;
            this.TransferredOutGritAmountSpinEdit.Name = "TransferredOutGritAmountSpinEdit";
            this.TransferredOutGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TransferredOutGritAmountSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.TransferredOutGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TransferredOutGritAmountSpinEdit.Properties.ReadOnly = true;
            this.TransferredOutGritAmountSpinEdit.Size = new System.Drawing.Size(174, 20);
            this.TransferredOutGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.TransferredOutGritAmountSpinEdit.TabIndex = 29;
            // 
            // TransferredInGritAmountSpinEdit
            // 
            this.TransferredInGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "TransferredInGritAmount", true));
            this.TransferredInGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TransferredInGritAmountSpinEdit.Location = new System.Drawing.Point(219, 454);
            this.TransferredInGritAmountSpinEdit.MenuManager = this.barManager1;
            this.TransferredInGritAmountSpinEdit.Name = "TransferredInGritAmountSpinEdit";
            this.TransferredInGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TransferredInGritAmountSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.TransferredInGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TransferredInGritAmountSpinEdit.Properties.ReadOnly = true;
            this.TransferredInGritAmountSpinEdit.Size = new System.Drawing.Size(174, 20);
            this.TransferredInGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.TransferredInGritAmountSpinEdit.TabIndex = 28;
            // 
            // StartingGritAmountSpinEdit
            // 
            this.StartingGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "StartingGritAmount", true));
            this.StartingGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StartingGritAmountSpinEdit.Location = new System.Drawing.Point(219, 430);
            this.StartingGritAmountSpinEdit.MenuManager = this.barManager1;
            this.StartingGritAmountSpinEdit.Name = "StartingGritAmountSpinEdit";
            this.StartingGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StartingGritAmountSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.StartingGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartingGritAmountSpinEdit.Size = new System.Drawing.Size(174, 20);
            this.StartingGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.StartingGritAmountSpinEdit.TabIndex = 27;
            this.StartingGritAmountSpinEdit.EditValueChanged += new System.EventHandler(this.StartingGritAmountSpinEdit_EditValueChanged);
            this.StartingGritAmountSpinEdit.Validated += new System.EventHandler(this.StartingGritAmountSpinEdit_Validated);
            // 
            // GritJobTransferMethodCheckEdit
            // 
            this.GritJobTransferMethodCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "GritJobTransferMethod", true));
            this.GritJobTransferMethodCheckEdit.Location = new System.Drawing.Point(183, 187);
            this.GritJobTransferMethodCheckEdit.MenuManager = this.barManager1;
            this.GritJobTransferMethodCheckEdit.Name = "GritJobTransferMethodCheckEdit";
            this.GritJobTransferMethodCheckEdit.Properties.Caption = "(Tick if Yes, Leave Unticked for Text Message)";
            this.GritJobTransferMethodCheckEdit.Properties.ValueChecked = 1;
            this.GritJobTransferMethodCheckEdit.Properties.ValueUnchecked = 0;
            this.GritJobTransferMethodCheckEdit.Size = new System.Drawing.Size(243, 19);
            this.GritJobTransferMethodCheckEdit.StyleController = this.dataLayoutControl1;
            this.GritJobTransferMethodCheckEdit.TabIndex = 26;
            // 
            // HourlyReactiveRateSpinEdit
            // 
            this.HourlyReactiveRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "HourlyReactiveRate", true));
            this.HourlyReactiveRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.HourlyReactiveRateSpinEdit.Location = new System.Drawing.Point(183, 129);
            this.HourlyReactiveRateSpinEdit.MenuManager = this.barManager1;
            this.HourlyReactiveRateSpinEdit.Name = "HourlyReactiveRateSpinEdit";
            this.HourlyReactiveRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.HourlyReactiveRateSpinEdit.Properties.Mask.EditMask = "c";
            this.HourlyReactiveRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HourlyReactiveRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.HourlyReactiveRateSpinEdit.Size = new System.Drawing.Size(165, 20);
            this.HourlyReactiveRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.HourlyReactiveRateSpinEdit.TabIndex = 23;
            // 
            // HourlyProactiveRateSpinEdit
            // 
            this.HourlyProactiveRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "HourlyProactiveRate", true));
            this.HourlyProactiveRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.HourlyProactiveRateSpinEdit.Location = new System.Drawing.Point(183, 105);
            this.HourlyProactiveRateSpinEdit.MenuManager = this.barManager1;
            this.HourlyProactiveRateSpinEdit.Name = "HourlyProactiveRateSpinEdit";
            this.HourlyProactiveRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.HourlyProactiveRateSpinEdit.Properties.Mask.EditMask = "c";
            this.HourlyProactiveRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HourlyProactiveRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.HourlyProactiveRateSpinEdit.Size = new System.Drawing.Size(165, 20);
            this.HourlyProactiveRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.HourlyProactiveRateSpinEdit.TabIndex = 22;
            // 
            // IsDirectLabourCheckEdit
            // 
            this.IsDirectLabourCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "IsDirectLabour", true));
            this.IsDirectLabourCheckEdit.Location = new System.Drawing.Point(183, 82);
            this.IsDirectLabourCheckEdit.MenuManager = this.barManager1;
            this.IsDirectLabourCheckEdit.Name = "IsDirectLabourCheckEdit";
            this.IsDirectLabourCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsDirectLabourCheckEdit.Properties.ValueChecked = 1;
            this.IsDirectLabourCheckEdit.Properties.ValueUnchecked = 0;
            this.IsDirectLabourCheckEdit.Size = new System.Drawing.Size(165, 19);
            this.IsDirectLabourCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsDirectLabourCheckEdit.TabIndex = 21;
            this.IsDirectLabourCheckEdit.EditValueChanged += new System.EventHandler(this.IsDirectLabourCheckEdit_EditValueChanged);
            // 
            // WarningLabel
            // 
            this.WarningLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F);
            this.WarningLabel.Appearance.ForeColor = System.Drawing.Color.White;
            this.WarningLabel.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.WarningLabel.Appearance.ImageIndex = 3;
            this.WarningLabel.Appearance.Options.UseFont = true;
            this.WarningLabel.Appearance.Options.UseForeColor = true;
            this.WarningLabel.Appearance.Options.UseImageAlign = true;
            this.WarningLabel.Appearance.Options.UseImageIndex = true;
            this.WarningLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.WarningLabel.Location = new System.Drawing.Point(429, 11);
            this.WarningLabel.Name = "WarningLabel";
            this.WarningLabel.Size = new System.Drawing.Size(261, 21);
            this.WarningLabel.StyleController = this.dataLayoutControl1;
            this.WarningLabel.TabIndex = 13;
            this.WarningLabel.Text = "       Warning: Current Salt Level Below Warning Level";
            // 
            // SelfBillingFrequencySpinEdit
            // 
            this.SelfBillingFrequencySpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "SelfBillingFrequency", true));
            this.SelfBillingFrequencySpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SelfBillingFrequencySpinEdit.Location = new System.Drawing.Point(219, 725);
            this.SelfBillingFrequencySpinEdit.MenuManager = this.barManager1;
            this.SelfBillingFrequencySpinEdit.Name = "SelfBillingFrequencySpinEdit";
            this.SelfBillingFrequencySpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SelfBillingFrequencySpinEdit.Properties.IsFloatValue = false;
            this.SelfBillingFrequencySpinEdit.Properties.Mask.EditMask = "N00";
            this.SelfBillingFrequencySpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.SelfBillingFrequencySpinEdit.Size = new System.Drawing.Size(173, 20);
            this.SelfBillingFrequencySpinEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingFrequencySpinEdit.TabIndex = 20;
            // 
            // SelfBillingFrequencyDescriptorIDGridLookUpEdit
            // 
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "SelfBillingFrequencyDescriptorID", true));
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(219, 701);
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Name = "SelfBillingFrequencyDescriptorIDGridLookUpEdit";
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource;
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Descriptor";
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties.ValueMember = "Value";
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(173, 20);
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.TabIndex = 19;
            // 
            // sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource
            // 
            this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource.DataMember = "sp04035_GC_Self_Billing_Frequency_Descriptors_With_Blank";
            this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colValue1,
            this.colDescriptor,
            this.colOrder});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colValue1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescriptor
            // 
            this.colDescriptor.Caption = "Frequency Descriptor";
            this.colDescriptor.FieldName = "Descriptor";
            this.colDescriptor.Name = "colDescriptor";
            this.colDescriptor.OptionsColumn.AllowEdit = false;
            this.colDescriptor.OptionsColumn.AllowFocus = false;
            this.colDescriptor.OptionsColumn.ReadOnly = true;
            this.colDescriptor.Visible = true;
            this.colDescriptor.VisibleIndex = 0;
            this.colDescriptor.Width = 169;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // StockSuppliedByDepotIDGridLookupEdit
            // 
            this.StockSuppliedByDepotIDGridLookupEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "StockSuppliedByDepotID", true));
            this.StockSuppliedByDepotIDGridLookupEdit.Location = new System.Drawing.Point(219, 396);
            this.StockSuppliedByDepotIDGridLookupEdit.MenuManager = this.barManager1;
            this.StockSuppliedByDepotIDGridLookupEdit.Name = "StockSuppliedByDepotIDGridLookupEdit";
            this.StockSuppliedByDepotIDGridLookupEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StockSuppliedByDepotIDGridLookupEdit.Properties.DataSource = this.sp04036GCStockDepotsWithBlankBindingSource;
            this.StockSuppliedByDepotIDGridLookupEdit.Properties.DisplayMember = "Description";
            this.StockSuppliedByDepotIDGridLookupEdit.Properties.NullText = "";
            this.StockSuppliedByDepotIDGridLookupEdit.Properties.PopupView = this.gridView1;
            this.StockSuppliedByDepotIDGridLookupEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.StockSuppliedByDepotIDGridLookupEdit.Properties.ValueMember = "Value";
            this.StockSuppliedByDepotIDGridLookupEdit.Size = new System.Drawing.Size(434, 20);
            this.StockSuppliedByDepotIDGridLookupEdit.StyleController = this.dataLayoutControl1;
            this.StockSuppliedByDepotIDGridLookupEdit.TabIndex = 18;
            // 
            // sp04036GCStockDepotsWithBlankBindingSource
            // 
            this.sp04036GCStockDepotsWithBlankBindingSource.DataMember = "sp04036_GC_Stock_Depots_With_Blank";
            this.sp04036GCStockDepotsWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colValue,
            this.colDescription,
            this.colAddressLine11,
            this.colPostcode1,
            this.colTelephone,
            this.colMobile1,
            this.colEmail,
            this.colWebSite1,
            this.colContactName,
            this.colLatitude,
            this.colLongitude,
            this.colRemarks1,
            this.colCurrentGritLevel,
            this.colWarningGritLevel,
            this.colUrgentGritLevel});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colValue;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Stock Depot Name";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 210;
            // 
            // colAddressLine11
            // 
            this.colAddressLine11.Caption = "Address Line 1";
            this.colAddressLine11.FieldName = "AddressLine1";
            this.colAddressLine11.Name = "colAddressLine11";
            this.colAddressLine11.OptionsColumn.AllowEdit = false;
            this.colAddressLine11.OptionsColumn.AllowFocus = false;
            this.colAddressLine11.OptionsColumn.ReadOnly = true;
            this.colAddressLine11.Visible = true;
            this.colAddressLine11.VisibleIndex = 4;
            this.colAddressLine11.Width = 103;
            // 
            // colPostcode1
            // 
            this.colPostcode1.Caption = "Postcode";
            this.colPostcode1.FieldName = "Postcode";
            this.colPostcode1.Name = "colPostcode1";
            this.colPostcode1.OptionsColumn.AllowEdit = false;
            this.colPostcode1.OptionsColumn.AllowFocus = false;
            this.colPostcode1.OptionsColumn.ReadOnly = true;
            this.colPostcode1.Visible = true;
            this.colPostcode1.VisibleIndex = 5;
            this.colPostcode1.Width = 64;
            // 
            // colTelephone
            // 
            this.colTelephone.Caption = "Telephone";
            this.colTelephone.FieldName = "Telephone";
            this.colTelephone.Name = "colTelephone";
            this.colTelephone.OptionsColumn.AllowEdit = false;
            this.colTelephone.OptionsColumn.AllowFocus = false;
            this.colTelephone.OptionsColumn.ReadOnly = true;
            // 
            // colMobile1
            // 
            this.colMobile1.Caption = "Mobile";
            this.colMobile1.FieldName = "Mobile";
            this.colMobile1.Name = "colMobile1";
            this.colMobile1.OptionsColumn.AllowEdit = false;
            this.colMobile1.OptionsColumn.AllowFocus = false;
            this.colMobile1.OptionsColumn.ReadOnly = true;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowEdit = false;
            this.colEmail.OptionsColumn.AllowFocus = false;
            this.colEmail.OptionsColumn.ReadOnly = true;
            // 
            // colWebSite1
            // 
            this.colWebSite1.Caption = "Website";
            this.colWebSite1.FieldName = "WebSite";
            this.colWebSite1.Name = "colWebSite1";
            this.colWebSite1.OptionsColumn.AllowEdit = false;
            this.colWebSite1.OptionsColumn.AllowFocus = false;
            this.colWebSite1.OptionsColumn.ReadOnly = true;
            // 
            // colContactName
            // 
            this.colContactName.Caption = "Contact Name";
            this.colContactName.FieldName = "ContactName";
            this.colContactName.Name = "colContactName";
            this.colContactName.OptionsColumn.AllowEdit = false;
            this.colContactName.OptionsColumn.AllowFocus = false;
            this.colContactName.OptionsColumn.ReadOnly = true;
            this.colContactName.Width = 89;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 6;
            // 
            // colCurrentGritLevel
            // 
            this.colCurrentGritLevel.Caption = "Current Grit Level";
            this.colCurrentGritLevel.FieldName = "CurrentGritLevel";
            this.colCurrentGritLevel.Name = "colCurrentGritLevel";
            this.colCurrentGritLevel.OptionsColumn.AllowEdit = false;
            this.colCurrentGritLevel.OptionsColumn.AllowFocus = false;
            this.colCurrentGritLevel.OptionsColumn.ReadOnly = true;
            this.colCurrentGritLevel.Visible = true;
            this.colCurrentGritLevel.VisibleIndex = 1;
            this.colCurrentGritLevel.Width = 106;
            // 
            // colWarningGritLevel
            // 
            this.colWarningGritLevel.Caption = "Warning Grit Level";
            this.colWarningGritLevel.FieldName = "WarningGritLevel";
            this.colWarningGritLevel.Name = "colWarningGritLevel";
            this.colWarningGritLevel.OptionsColumn.AllowEdit = false;
            this.colWarningGritLevel.OptionsColumn.AllowFocus = false;
            this.colWarningGritLevel.OptionsColumn.ReadOnly = true;
            this.colWarningGritLevel.Visible = true;
            this.colWarningGritLevel.VisibleIndex = 2;
            this.colWarningGritLevel.Width = 109;
            // 
            // colUrgentGritLevel
            // 
            this.colUrgentGritLevel.Caption = "Urgent Grit Level";
            this.colUrgentGritLevel.FieldName = "UrgentGritLevel";
            this.colUrgentGritLevel.Name = "colUrgentGritLevel";
            this.colUrgentGritLevel.OptionsColumn.AllowEdit = false;
            this.colUrgentGritLevel.OptionsColumn.AllowFocus = false;
            this.colUrgentGritLevel.OptionsColumn.ReadOnly = true;
            this.colUrgentGritLevel.Visible = true;
            this.colUrgentGritLevel.VisibleIndex = 3;
            this.colUrgentGritLevel.Width = 102;
            // 
            // UrgentGritLevelSpinEdit
            // 
            this.UrgentGritLevelSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "UrgentGritLevel", true));
            this.UrgentGritLevelSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UrgentGritLevelSpinEdit.Location = new System.Drawing.Point(219, 598);
            this.UrgentGritLevelSpinEdit.MenuManager = this.barManager1;
            this.UrgentGritLevelSpinEdit.Name = "UrgentGritLevelSpinEdit";
            this.UrgentGritLevelSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.UrgentGritLevelSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.UrgentGritLevelSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UrgentGritLevelSpinEdit.Size = new System.Drawing.Size(174, 20);
            this.UrgentGritLevelSpinEdit.StyleController = this.dataLayoutControl1;
            this.UrgentGritLevelSpinEdit.TabIndex = 17;
            this.UrgentGritLevelSpinEdit.DragOver += new System.Windows.Forms.DragEventHandler(this.UrgentGritLevelSpinEdit_DragOver);
            this.UrgentGritLevelSpinEdit.Validated += new System.EventHandler(this.UrgentGritLevelSpinEdit_Validated);
            // 
            // WarningGritLevelSpinEdit
            // 
            this.WarningGritLevelSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "WarningGritLevel", true));
            this.WarningGritLevelSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WarningGritLevelSpinEdit.Location = new System.Drawing.Point(219, 574);
            this.WarningGritLevelSpinEdit.MenuManager = this.barManager1;
            this.WarningGritLevelSpinEdit.Name = "WarningGritLevelSpinEdit";
            this.WarningGritLevelSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.WarningGritLevelSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.WarningGritLevelSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WarningGritLevelSpinEdit.Size = new System.Drawing.Size(174, 20);
            this.WarningGritLevelSpinEdit.StyleController = this.dataLayoutControl1;
            this.WarningGritLevelSpinEdit.TabIndex = 16;
            this.WarningGritLevelSpinEdit.EditValueChanged += new System.EventHandler(this.WarningGritLevelSpinEdit_EditValueChanged);
            this.WarningGritLevelSpinEdit.Validated += new System.EventHandler(this.WarningGritLevelSpinEdit_Validated);
            // 
            // CurrentGritLevelSpinEdit
            // 
            this.CurrentGritLevelSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "CurrentGritLevel", true));
            this.CurrentGritLevelSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CurrentGritLevelSpinEdit.Location = new System.Drawing.Point(219, 550);
            this.CurrentGritLevelSpinEdit.MenuManager = this.barManager1;
            this.CurrentGritLevelSpinEdit.Name = "CurrentGritLevelSpinEdit";
            this.CurrentGritLevelSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CurrentGritLevelSpinEdit.Properties.Mask.EditMask = "######0.00 25 Kg Bags";
            this.CurrentGritLevelSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CurrentGritLevelSpinEdit.Properties.ReadOnly = true;
            this.CurrentGritLevelSpinEdit.Size = new System.Drawing.Size(174, 20);
            this.CurrentGritLevelSpinEdit.StyleController = this.dataLayoutControl1;
            this.CurrentGritLevelSpinEdit.TabIndex = 15;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04033GCTeamGrittingInformationEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(184, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 14;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // SubContractorGritInformationIDTextEdit
            // 
            this.SubContractorGritInformationIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "SubContractorGritInformationID", true));
            this.SubContractorGritInformationIDTextEdit.Location = new System.Drawing.Point(96, 341);
            this.SubContractorGritInformationIDTextEdit.MenuManager = this.barManager1;
            this.SubContractorGritInformationIDTextEdit.Name = "SubContractorGritInformationIDTextEdit";
            this.SubContractorGritInformationIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubContractorGritInformationIDTextEdit, true);
            this.SubContractorGritInformationIDTextEdit.Size = new System.Drawing.Size(216, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubContractorGritInformationIDTextEdit, optionsSpelling1);
            this.SubContractorGritInformationIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorGritInformationIDTextEdit.TabIndex = 4;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 339);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(629, 418);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling2);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 11;
            // 
            // SelfBillingInvoiceCheckEdit
            // 
            this.SelfBillingInvoiceCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "SellBillingInvoice", true));
            this.SelfBillingInvoiceCheckEdit.Location = new System.Drawing.Point(219, 678);
            this.SelfBillingInvoiceCheckEdit.MenuManager = this.barManager1;
            this.SelfBillingInvoiceCheckEdit.Name = "SelfBillingInvoiceCheckEdit";
            this.SelfBillingInvoiceCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SelfBillingInvoiceCheckEdit.Properties.ValueChecked = 1;
            this.SelfBillingInvoiceCheckEdit.Properties.ValueUnchecked = 0;
            this.SelfBillingInvoiceCheckEdit.Size = new System.Drawing.Size(98, 19);
            this.SelfBillingInvoiceCheckEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingInvoiceCheckEdit.TabIndex = 12;
            this.SelfBillingInvoiceCheckEdit.EditValueChanged += new System.EventHandler(this.SelfBillingInvoiceCheckEdit_EditValueChanged);
            // 
            // HoldsStockCheckEdit
            // 
            this.HoldsStockCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "HoldsStock", true));
            this.HoldsStockCheckEdit.Location = new System.Drawing.Point(219, 373);
            this.HoldsStockCheckEdit.MenuManager = this.barManager1;
            this.HoldsStockCheckEdit.Name = "HoldsStockCheckEdit";
            this.HoldsStockCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.HoldsStockCheckEdit.Properties.ValueChecked = 1;
            this.HoldsStockCheckEdit.Properties.ValueUnchecked = 0;
            this.HoldsStockCheckEdit.Size = new System.Drawing.Size(96, 19);
            this.HoldsStockCheckEdit.StyleController = this.dataLayoutControl1;
            this.HoldsStockCheckEdit.TabIndex = 13;
            this.HoldsStockCheckEdit.EditValueChanged += new System.EventHandler(this.HoldsStockCheckEdit_EditValueChanged);
            // 
            // SubContractorIDGridLookUpEdit
            // 
            this.SubContractorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "SubContractorID", true));
            this.SubContractorIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SubContractorIDGridLookUpEdit.Location = new System.Drawing.Point(183, 35);
            this.SubContractorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SubContractorIDGridLookUpEdit.Name = "SubContractorIDGridLookUpEdit";
            this.SubContractorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubContractorIDGridLookUpEdit.Properties.DataSource = this.sp04026ContractorListWithBlankBindingSource;
            this.SubContractorIDGridLookUpEdit.Properties.DisplayMember = "ContractorName";
            this.SubContractorIDGridLookUpEdit.Properties.NullText = "";
            this.SubContractorIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.SubContractorIDGridLookUpEdit.Properties.ReadOnly = true;
            this.SubContractorIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1});
            this.SubContractorIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SubContractorIDGridLookUpEdit.Properties.ValueMember = "ContractorID";
            this.SubContractorIDGridLookUpEdit.Size = new System.Drawing.Size(506, 20);
            this.SubContractorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorIDGridLookUpEdit.TabIndex = 5;
            this.SubContractorIDGridLookUpEdit.TabStop = false;
            this.SubContractorIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SubContractorIDGridLookUpEdit_Validating);
            // 
            // sp04026ContractorListWithBlankBindingSource
            // 
            this.sp04026ContractorListWithBlankBindingSource.DataMember = "sp04026_Contractor_List_With_Blank";
            this.sp04026ContractorListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colContractorCode,
            this.colContractorID,
            this.colContractorName,
            this.colDisabled,
            this.colEmailPassword,
            this.colInternalContractor,
            this.colInternalCOntractorDescription,
            this.colMobile,
            this.colPostcode,
            this.colRemarks,
            this.colTelephone1,
            this.colTelephone2,
            this.colTypeDescription,
            this.colTypeID,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colVatReg,
            this.colWebsite});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colContractorID;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInternalCOntractorDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 273;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor ID";
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 118;
            // 
            // colInternalCOntractorDescription
            // 
            this.colInternalCOntractorDescription.Caption = "Status";
            this.colInternalCOntractorDescription.FieldName = "InternalCOntractorDescription";
            this.colInternalCOntractorDescription.Name = "colInternalCOntractorDescription";
            this.colInternalCOntractorDescription.OptionsColumn.AllowEdit = false;
            this.colInternalCOntractorDescription.OptionsColumn.AllowFocus = false;
            this.colInternalCOntractorDescription.OptionsColumn.ReadOnly = true;
            this.colInternalCOntractorDescription.Width = 52;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Width = 51;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Width = 62;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 1;
            this.colTypeDescription.Width = 111;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.Width = 49;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Reg";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 62;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // GrittingActiveCheckEdit
            // 
            this.GrittingActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "GrittingActive", true));
            this.GrittingActiveCheckEdit.EditValue = null;
            this.GrittingActiveCheckEdit.Location = new System.Drawing.Point(183, 59);
            this.GrittingActiveCheckEdit.MenuManager = this.barManager1;
            this.GrittingActiveCheckEdit.Name = "GrittingActiveCheckEdit";
            this.GrittingActiveCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.GrittingActiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GrittingActiveCheckEdit.Properties.ValueChecked = 1;
            this.GrittingActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.GrittingActiveCheckEdit.Size = new System.Drawing.Size(165, 19);
            this.GrittingActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.GrittingActiveCheckEdit.TabIndex = 6;
            // 
            // GritMobileTelephoneNumberTextEdit
            // 
            this.GritMobileTelephoneNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04033GCTeamGrittingInformationEditBindingSource, "GritMobileTelephoneNumber", true));
            this.GritMobileTelephoneNumberTextEdit.Location = new System.Drawing.Point(183, 163);
            this.GritMobileTelephoneNumberTextEdit.MenuManager = this.barManager1;
            this.GritMobileTelephoneNumberTextEdit.Name = "GritMobileTelephoneNumberTextEdit";
            this.GritMobileTelephoneNumberTextEdit.Properties.Mask.EditMask = "(\\d{7,},)*(\\d{7,})?";
            this.GritMobileTelephoneNumberTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.GritMobileTelephoneNumberTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.GritMobileTelephoneNumberTextEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GritMobileTelephoneNumberTextEdit, true);
            this.GritMobileTelephoneNumberTextEdit.Size = new System.Drawing.Size(506, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GritMobileTelephoneNumberTextEdit, optionsSpelling3);
            this.GritMobileTelephoneNumberTextEdit.StyleController = this.dataLayoutControl1;
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem5.Text = "Gritting Mobile telephone Number: - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = resources.GetString("toolTipItem5.Text");
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.GritMobileTelephoneNumberTextEdit.SuperTip = superToolTip5;
            this.GritMobileTelephoneNumberTextEdit.TabIndex = 24;
            // 
            // ItemForSubContractorGritInformationID
            // 
            this.ItemForSubContractorGritInformationID.Control = this.SubContractorGritInformationIDTextEdit;
            this.ItemForSubContractorGritInformationID.CustomizationFormText = "Contact ID:";
            this.ItemForSubContractorGritInformationID.Location = new System.Drawing.Point(0, 329);
            this.ItemForSubContractorGritInformationID.Name = "ItemForSubContractorGritInformationID";
            this.ItemForSubContractorGritInformationID.Size = new System.Drawing.Size(304, 132);
            this.ItemForSubContractorGritInformationID.Text = "Contact ID:";
            this.ItemForSubContractorGritInformationID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(701, 813);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSubContractorID,
            this.emptySpaceItem1,
            this.layoutControlGroup3,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem5,
            this.ItemForWarningLabel,
            this.emptySpaceItem4,
            this.ItemForHourlyProactiveRate,
            this.emptySpaceItem7,
            this.ItemForGritMobileTelephoneNumber,
            this.ItemForGritJobTransferMethod,
            this.ItemForMissingJobSheetDeliveryMethodID,
            this.ItemForAutoAllocateGritJobs,
            this.emptySpaceItem13,
            this.ItemForHourlyReactiveRate,
            this.ItemForIsDirectLabour,
            this.ItemForGrittingActive,
            this.emptySpaceItem14,
            this.emptySpaceItem15});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(681, 783);
            // 
            // ItemForSubContractorID
            // 
            this.ItemForSubContractorID.Control = this.SubContractorIDGridLookUpEdit;
            this.ItemForSubContractorID.CustomizationFormText = "Team:";
            this.ItemForSubContractorID.Location = new System.Drawing.Point(0, 23);
            this.ItemForSubContractorID.Name = "ItemForSubContractorID";
            this.ItemForSubContractorID.Size = new System.Drawing.Size(681, 24);
            this.ItemForSubContractorID.Text = "Team:";
            this.ItemForSubContractorID.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 245);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(681, 12);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 257);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(681, 516);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(657, 470);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup5});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Stock \\ Billing";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup4,
            this.emptySpaceItem6});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(633, 422);
            this.layoutControlGroup7.Text = "Stock \\ Billing";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Stock";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForHoldsStock,
            this.ItemForStartingGritAmount,
            this.ItemForStockSuppliedByDepotID,
            this.emptySpaceItem8,
            this.emptySpaceItem9,
            this.ItemForTransferredInGritAmount,
            this.ItemForTransferredOutGritAmount,
            this.ItemForOrderedInGritAmount,
            this.ItemForCalloutUsedGritAmount,
            this.layoutControlItem2,
            this.ItemForWarningGritLevel,
            this.ItemForUrgentGritLevel,
            this.emptySpaceItem11});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(633, 295);
            this.layoutControlGroup6.Text = "Stock";
            // 
            // ItemForHoldsStock
            // 
            this.ItemForHoldsStock.Control = this.HoldsStockCheckEdit;
            this.ItemForHoldsStock.CustomizationFormText = "Holds Stock:";
            this.ItemForHoldsStock.Location = new System.Drawing.Point(0, 0);
            this.ItemForHoldsStock.Name = "ItemForHoldsStock";
            this.ItemForHoldsStock.Size = new System.Drawing.Size(271, 23);
            this.ItemForHoldsStock.Text = "Holds Stock:";
            this.ItemForHoldsStock.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForStartingGritAmount
            // 
            this.ItemForStartingGritAmount.Control = this.StartingGritAmountSpinEdit;
            this.ItemForStartingGritAmount.CustomizationFormText = "Starting Salt Amount:";
            this.ItemForStartingGritAmount.Location = new System.Drawing.Point(0, 57);
            this.ItemForStartingGritAmount.MaxSize = new System.Drawing.Size(0, 24);
            this.ItemForStartingGritAmount.MinSize = new System.Drawing.Size(200, 24);
            this.ItemForStartingGritAmount.Name = "ItemForStartingGritAmount";
            this.ItemForStartingGritAmount.Size = new System.Drawing.Size(349, 24);
            this.ItemForStartingGritAmount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStartingGritAmount.Text = "Starting Salt Amount:";
            this.ItemForStartingGritAmount.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForStockSuppliedByDepotID
            // 
            this.ItemForStockSuppliedByDepotID.Control = this.StockSuppliedByDepotIDGridLookupEdit;
            this.ItemForStockSuppliedByDepotID.CustomizationFormText = "Stock Supplier Depot:";
            this.ItemForStockSuppliedByDepotID.Location = new System.Drawing.Point(0, 23);
            this.ItemForStockSuppliedByDepotID.Name = "ItemForStockSuppliedByDepotID";
            this.ItemForStockSuppliedByDepotID.Size = new System.Drawing.Size(609, 24);
            this.ItemForStockSuppliedByDepotID.Text = "Stock Supplier Depot:";
            this.ItemForStockSuppliedByDepotID.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(609, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(349, 57);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(260, 192);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTransferredInGritAmount
            // 
            this.ItemForTransferredInGritAmount.Control = this.TransferredInGritAmountSpinEdit;
            this.ItemForTransferredInGritAmount.CustomizationFormText = "Transferred In Salt Amount:";
            this.ItemForTransferredInGritAmount.Location = new System.Drawing.Point(0, 81);
            this.ItemForTransferredInGritAmount.Name = "ItemForTransferredInGritAmount";
            this.ItemForTransferredInGritAmount.Size = new System.Drawing.Size(349, 24);
            this.ItemForTransferredInGritAmount.Text = "Transferred In Salt Amount:";
            this.ItemForTransferredInGritAmount.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForTransferredOutGritAmount
            // 
            this.ItemForTransferredOutGritAmount.Control = this.TransferredOutGritAmountSpinEdit;
            this.ItemForTransferredOutGritAmount.CustomizationFormText = "Transferred Out Salt Amount:";
            this.ItemForTransferredOutGritAmount.Location = new System.Drawing.Point(0, 105);
            this.ItemForTransferredOutGritAmount.Name = "ItemForTransferredOutGritAmount";
            this.ItemForTransferredOutGritAmount.Size = new System.Drawing.Size(349, 24);
            this.ItemForTransferredOutGritAmount.Text = "Transferred Out Salt Amount:";
            this.ItemForTransferredOutGritAmount.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForOrderedInGritAmount
            // 
            this.ItemForOrderedInGritAmount.Control = this.OrderedInGritAmountSpinEdit;
            this.ItemForOrderedInGritAmount.CustomizationFormText = "Ordered In Salt Amount:";
            this.ItemForOrderedInGritAmount.Location = new System.Drawing.Point(0, 129);
            this.ItemForOrderedInGritAmount.Name = "ItemForOrderedInGritAmount";
            this.ItemForOrderedInGritAmount.Size = new System.Drawing.Size(349, 24);
            this.ItemForOrderedInGritAmount.Text = "Ordered In Salt Amount:";
            this.ItemForOrderedInGritAmount.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForCalloutUsedGritAmount
            // 
            this.ItemForCalloutUsedGritAmount.Control = this.CalloutUsedGritAmountSpinEdit;
            this.ItemForCalloutUsedGritAmount.CustomizationFormText = "Callout Used Salt Amount:";
            this.ItemForCalloutUsedGritAmount.Location = new System.Drawing.Point(0, 153);
            this.ItemForCalloutUsedGritAmount.Name = "ItemForCalloutUsedGritAmount";
            this.ItemForCalloutUsedGritAmount.Size = new System.Drawing.Size(349, 24);
            this.ItemForCalloutUsedGritAmount.Text = "Callout Used Salt Amount:";
            this.ItemForCalloutUsedGritAmount.TextSize = new System.Drawing.Size(168, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHtmlStringInCaption = true;
            this.layoutControlItem2.Control = this.CurrentGritLevelSpinEdit;
            this.layoutControlItem2.CustomizationFormText = "Current Salt Level:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 177);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(349, 24);
            this.layoutControlItem2.Text = "<b>Current Salt Level:</b>";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForWarningGritLevel
            // 
            this.ItemForWarningGritLevel.Control = this.WarningGritLevelSpinEdit;
            this.ItemForWarningGritLevel.CustomizationFormText = "Warning Salt Level:";
            this.ItemForWarningGritLevel.Location = new System.Drawing.Point(0, 201);
            this.ItemForWarningGritLevel.Name = "ItemForWarningGritLevel";
            this.ItemForWarningGritLevel.Size = new System.Drawing.Size(349, 24);
            this.ItemForWarningGritLevel.Text = "Warning Salt Level:";
            this.ItemForWarningGritLevel.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForUrgentGritLevel
            // 
            this.ItemForUrgentGritLevel.Control = this.UrgentGritLevelSpinEdit;
            this.ItemForUrgentGritLevel.CustomizationFormText = "Urgent Salt Level:";
            this.ItemForUrgentGritLevel.Location = new System.Drawing.Point(0, 225);
            this.ItemForUrgentGritLevel.Name = "ItemForUrgentGritLevel";
            this.ItemForUrgentGritLevel.Size = new System.Drawing.Size(349, 24);
            this.ItemForUrgentGritLevel.Text = "Urgent Salt Level:";
            this.ItemForUrgentGritLevel.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(271, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(338, 23);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Self Billing";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSelfBillingInvoice,
            this.ItemForSelfBillingFrequency,
            this.emptySpaceItem10,
            this.ItemForSelfBillingFrequencyDescriptorID,
            this.emptySpaceItem12});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 305);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(633, 117);
            this.layoutControlGroup4.Text = "Self Billing";
            // 
            // ItemForSelfBillingInvoice
            // 
            this.ItemForSelfBillingInvoice.Control = this.SelfBillingInvoiceCheckEdit;
            this.ItemForSelfBillingInvoice.CustomizationFormText = "Disabled:";
            this.ItemForSelfBillingInvoice.Location = new System.Drawing.Point(0, 0);
            this.ItemForSelfBillingInvoice.Name = "ItemForSelfBillingInvoice";
            this.ItemForSelfBillingInvoice.Size = new System.Drawing.Size(273, 23);
            this.ItemForSelfBillingInvoice.Text = "Self Billing Invoice:";
            this.ItemForSelfBillingInvoice.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForSelfBillingFrequency
            // 
            this.ItemForSelfBillingFrequency.Control = this.SelfBillingFrequencySpinEdit;
            this.ItemForSelfBillingFrequency.CustomizationFormText = "Billing Frequency:";
            this.ItemForSelfBillingFrequency.Location = new System.Drawing.Point(0, 47);
            this.ItemForSelfBillingFrequency.Name = "ItemForSelfBillingFrequency";
            this.ItemForSelfBillingFrequency.Size = new System.Drawing.Size(348, 24);
            this.ItemForSelfBillingFrequency.Text = "Billing Frequency:";
            this.ItemForSelfBillingFrequency.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(348, 23);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(261, 48);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSelfBillingFrequencyDescriptorID
            // 
            this.ItemForSelfBillingFrequencyDescriptorID.Control = this.SelfBillingFrequencyDescriptorIDGridLookUpEdit;
            this.ItemForSelfBillingFrequencyDescriptorID.CustomizationFormText = "Billing Frequency Descriptor:";
            this.ItemForSelfBillingFrequencyDescriptorID.Location = new System.Drawing.Point(0, 23);
            this.ItemForSelfBillingFrequencyDescriptorID.Name = "ItemForSelfBillingFrequencyDescriptorID";
            this.ItemForSelfBillingFrequencyDescriptorID.Size = new System.Drawing.Size(348, 24);
            this.ItemForSelfBillingFrequencyDescriptorID.Text = "Billing Frequency Descriptor:";
            this.ItemForSelfBillingFrequencyDescriptorID.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(273, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(336, 23);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 295);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(633, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup5.CustomizationFormText = "Remarks";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(633, 422);
            this.layoutControlGroup5.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.RemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(633, 422);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(172, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(172, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(172, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(172, 23);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 773);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(681, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWarningLabel
            // 
            this.ItemForWarningLabel.Control = this.WarningLabel;
            this.ItemForWarningLabel.CustomizationFormText = "Grit Warning Label:";
            this.ItemForWarningLabel.Location = new System.Drawing.Point(418, 0);
            this.ItemForWarningLabel.MaxSize = new System.Drawing.Size(263, 0);
            this.ItemForWarningLabel.MinSize = new System.Drawing.Size(263, 17);
            this.ItemForWarningLabel.Name = "ItemForWarningLabel";
            this.ItemForWarningLabel.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.ItemForWarningLabel.Size = new System.Drawing.Size(263, 23);
            this.ItemForWarningLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWarningLabel.Text = "Grit Warning Label:";
            this.ItemForWarningLabel.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForWarningLabel.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(372, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(46, 23);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForHourlyProactiveRate
            // 
            this.ItemForHourlyProactiveRate.Control = this.HourlyProactiveRateSpinEdit;
            this.ItemForHourlyProactiveRate.CustomizationFormText = "Proactive Hourly Rate:";
            this.ItemForHourlyProactiveRate.Location = new System.Drawing.Point(0, 93);
            this.ItemForHourlyProactiveRate.Name = "ItemForHourlyProactiveRate";
            this.ItemForHourlyProactiveRate.Size = new System.Drawing.Size(340, 24);
            this.ItemForHourlyProactiveRate.Text = "Proactive Hourly Rate:";
            this.ItemForHourlyProactiveRate.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 141);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(681, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForGritMobileTelephoneNumber
            // 
            this.ItemForGritMobileTelephoneNumber.Control = this.GritMobileTelephoneNumberTextEdit;
            this.ItemForGritMobileTelephoneNumber.CustomizationFormText = "Gritting Mobile Telephone:";
            this.ItemForGritMobileTelephoneNumber.Location = new System.Drawing.Point(0, 151);
            this.ItemForGritMobileTelephoneNumber.Name = "ItemForGritMobileTelephoneNumber";
            this.ItemForGritMobileTelephoneNumber.Size = new System.Drawing.Size(681, 24);
            this.ItemForGritMobileTelephoneNumber.Text = "Gritting Mobile Telephone:";
            this.ItemForGritMobileTelephoneNumber.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForGritJobTransferMethod
            // 
            this.ItemForGritJobTransferMethod.Control = this.GritJobTransferMethodCheckEdit;
            this.ItemForGritJobTransferMethod.CustomizationFormText = "Device Used:";
            this.ItemForGritJobTransferMethod.Location = new System.Drawing.Point(0, 175);
            this.ItemForGritJobTransferMethod.Name = "ItemForGritJobTransferMethod";
            this.ItemForGritJobTransferMethod.Size = new System.Drawing.Size(418, 23);
            this.ItemForGritJobTransferMethod.Text = "Device Used:";
            this.ItemForGritJobTransferMethod.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForMissingJobSheetDeliveryMethodID
            // 
            this.ItemForMissingJobSheetDeliveryMethodID.Control = this.gridLookUpEditMissingJobSheetDeliveryMethodID;
            this.ItemForMissingJobSheetDeliveryMethodID.CustomizationFormText = "Missing Job Sheet Number Method:";
            this.ItemForMissingJobSheetDeliveryMethodID.Location = new System.Drawing.Point(0, 221);
            this.ItemForMissingJobSheetDeliveryMethodID.Name = "ItemForMissingJobSheetDeliveryMethodID";
            this.ItemForMissingJobSheetDeliveryMethodID.Size = new System.Drawing.Size(681, 24);
            this.ItemForMissingJobSheetDeliveryMethodID.Text = "Missing Job Sheet Number Method:";
            this.ItemForMissingJobSheetDeliveryMethodID.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForAutoAllocateGritJobs
            // 
            this.ItemForAutoAllocateGritJobs.Control = this.AutoAllocateGritJobsCheckEdit;
            this.ItemForAutoAllocateGritJobs.CustomizationFormText = "Auto Allocate Gritting Jobs:";
            this.ItemForAutoAllocateGritJobs.Location = new System.Drawing.Point(0, 198);
            this.ItemForAutoAllocateGritJobs.Name = "ItemForAutoAllocateGritJobs";
            this.ItemForAutoAllocateGritJobs.Size = new System.Drawing.Size(608, 23);
            this.ItemForAutoAllocateGritJobs.Text = "Auto Allocate Gritting Jobs:";
            this.ItemForAutoAllocateGritJobs.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(340, 47);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(341, 94);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForHourlyReactiveRate
            // 
            this.ItemForHourlyReactiveRate.Control = this.HourlyReactiveRateSpinEdit;
            this.ItemForHourlyReactiveRate.CustomizationFormText = "Reactive Hourly Rate:";
            this.ItemForHourlyReactiveRate.Location = new System.Drawing.Point(0, 117);
            this.ItemForHourlyReactiveRate.Name = "ItemForHourlyReactiveRate";
            this.ItemForHourlyReactiveRate.Size = new System.Drawing.Size(340, 24);
            this.ItemForHourlyReactiveRate.Text = "Reactive Hourly Rate:";
            this.ItemForHourlyReactiveRate.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForIsDirectLabour
            // 
            this.ItemForIsDirectLabour.Control = this.IsDirectLabourCheckEdit;
            this.ItemForIsDirectLabour.CustomizationFormText = "Is Direct Labour:";
            this.ItemForIsDirectLabour.Location = new System.Drawing.Point(0, 70);
            this.ItemForIsDirectLabour.Name = "ItemForIsDirectLabour";
            this.ItemForIsDirectLabour.Size = new System.Drawing.Size(340, 23);
            this.ItemForIsDirectLabour.Text = "Is Direct Labour:";
            this.ItemForIsDirectLabour.TextSize = new System.Drawing.Size(168, 13);
            // 
            // ItemForGrittingActive
            // 
            this.ItemForGrittingActive.Control = this.GrittingActiveCheckEdit;
            this.ItemForGrittingActive.CustomizationFormText = "Gritting Active:";
            this.ItemForGrittingActive.Location = new System.Drawing.Point(0, 47);
            this.ItemForGrittingActive.Name = "ItemForGrittingActive";
            this.ItemForGrittingActive.Size = new System.Drawing.Size(340, 23);
            this.ItemForGrittingActive.Text = "Gritting Active:";
            this.ItemForGrittingActive.TextSize = new System.Drawing.Size(168, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(418, 175);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(263, 23);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(608, 198);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(73, 23);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 783);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(681, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp04026_Contractor_List_With_BlankTableAdapter
            // 
            this.sp04026_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04033_GC_Team_Gritting_Information_EditTableAdapter
            // 
            this.sp04033_GC_Team_Gritting_Information_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04036_GC_Stock_Depots_With_BlankTableAdapter
            // 
            this.sp04036_GC_Stock_Depots_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter
            // 
            this.sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04276_GC_Missing_Job_Sheet_Numbers_Delivery_Methods_With_BlankTableAdapter
            // 
            this.sp04276_GC_Missing_Job_Sheet_Numbers_Delivery_Methods_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_GC_Team_Gritting_Information_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(718, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Team_Gritting_Information_Edit";
            this.Text = "Edit Team Gritting Information";
            this.Activated += new System.EventHandler(this.frm_GC_Team_Gritting_Information_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Team_Gritting_Information_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Team_Gritting_Information_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AutoAllocateGritJobsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04033GCTeamGrittingInformationEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMissingJobSheetDeliveryMethodID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04276GCMissingJobSheetNumbersDeliveryMethodsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalloutUsedGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderedInGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredOutGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransferredInGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartingGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritJobTransferMethodCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourlyReactiveRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourlyProactiveRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsDirectLabourCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingFrequencySpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingFrequencyDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StockSuppliedByDepotIDGridLookupEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04036GCStockDepotsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UrgentGritLevelSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WarningGritLevelSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentGritLevelSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorGritInformationIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoldsStockCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04026ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritMobileTelephoneNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorGritInformationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoldsStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartingGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStockSuppliedByDepotID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredInGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransferredOutGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderedInGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalloutUsedGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningGritLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUrgentGritLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingFrequencyDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHourlyProactiveRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritMobileTelephoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritJobTransferMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMissingJobSheetDeliveryMethodID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAutoAllocateGritJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHourlyReactiveRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsDirectLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit SubContractorGritInformationIDTextEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.CheckEdit SelfBillingInvoiceCheckEdit;
        private DevExpress.XtraEditors.CheckEdit HoldsStockCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorGritInformationID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingActive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingInvoice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHoldsStock;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.GridLookUpEdit SubContractorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalCOntractorDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private System.Windows.Forms.BindingSource sp04026ContractorListWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04026_Contractor_List_With_BlankTableAdapter sp04026_Contractor_List_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp04033GCTeamGrittingInformationEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04033_GC_Team_Gritting_Information_EditTableAdapter sp04033_GC_Team_Gritting_Information_EditTableAdapter;
        private DevExpress.XtraEditors.CheckEdit GrittingActiveCheckEdit;
        private DevExpress.XtraEditors.SpinEdit CurrentGritLevelSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SpinEdit UrgentGritLevelSpinEdit;
        private DevExpress.XtraEditors.SpinEdit WarningGritLevelSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarningGritLevel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUrgentGritLevel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraEditors.GridLookUpEdit StockSuppliedByDepotIDGridLookupEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStockSuppliedByDepotID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private System.Windows.Forms.BindingSource sp04036GCStockDepotsWithBlankBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine11;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colWebSite1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactName;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colUrgentGritLevel;
        private DataSet_GC_DataEntryTableAdapters.sp04036_GC_Stock_Depots_With_BlankTableAdapter sp04036_GC_Stock_Depots_With_BlankTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.GridLookUpEdit SelfBillingFrequencyDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingFrequencyDescriptorID;
        private System.Windows.Forms.BindingSource sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.SpinEdit SelfBillingFrequencySpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingFrequency;
        private DevExpress.XtraEditors.CheckEdit IsDirectLabourCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsDirectLabour;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.LabelControl WarningLabel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarningLabel;
        private DevExpress.XtraEditors.SpinEdit HourlyReactiveRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit HourlyProactiveRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHourlyProactiveRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHourlyReactiveRate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritMobileTelephoneNumber;
        private DevExpress.XtraEditors.CheckEdit GritJobTransferMethodCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritJobTransferMethod;
        private DevExpress.XtraEditors.TextEdit GritMobileTelephoneNumberTextEdit;
        private DevExpress.XtraEditors.SpinEdit TransferredInGritAmountSpinEdit;
        private DevExpress.XtraEditors.SpinEdit StartingGritAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartingGritAmount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferredInGritAmount;
        private DevExpress.XtraEditors.SpinEdit TransferredOutGritAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransferredOutGritAmount;
        private DevExpress.XtraEditors.SpinEdit OrderedInGritAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOrderedInGritAmount;
        private DevExpress.XtraEditors.SpinEdit CalloutUsedGritAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCalloutUsedGritAmount;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMissingJobSheetDeliveryMethodID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMissingJobSheetDeliveryMethodID;
        private System.Windows.Forms.BindingSource sp04276GCMissingJobSheetNumbersDeliveryMethodsWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04276_GC_Missing_Job_Sheet_Numbers_Delivery_Methods_With_BlankTableAdapter sp04276_GC_Missing_Job_Sheet_Numbers_Delivery_Methods_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptor1;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colValue2;
        private DevExpress.XtraEditors.CheckEdit AutoAllocateGritJobsCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAutoAllocateGritJobs;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

namespace WoodPlan5
{
    partial class frm_GC_Site_Gritting_contract_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_GC_Site_Gritting_contract_Edit));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition13 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition14 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition15 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition16 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition17 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition18 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SeasonPeriodIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04038GCSiteGrittingContractEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.sp04052GCGrittingActivationCodesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.DontInvoiceClientReasonMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.InvoiceClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.Annual_ContractCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SiteClosedTime2TimeSpanEdit = new DevExpress.XtraEditors.TimeSpanEdit();
            this.SiteOpenTime2TimeSpanEdit = new DevExpress.XtraEditors.TimeSpanEdit();
            this.SiteClosedTime1TimeSpanEdit = new DevExpress.XtraEditors.TimeSpanEdit();
            this.SiteOpenTime1TimeSpanEdit = new DevExpress.XtraEditors.TimeSpanEdit();
            this.AccessRestrictionsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AnnualCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GrittingCompletionEmailLinkedPicturesCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GrittingTimeOnSiteSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GrittingCompletionEmailCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsFloatingSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.BandingEndGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04343GCGrittingForecastBandingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BandingStartGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RedOverridesBandingCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DefaultNoAccessRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PrioritySiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SiteGrittingNotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ClientEveningRateModifierSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ClientSaltVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TeamReactiveRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TeamProactiveRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DefaultGritAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MinimumTemperatureSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GritOnSundayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GritOnSaturdayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GritOnFridayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GritOnThursdayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GritOnWednesdayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GritOnTuesdayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GritOnMondayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientSaltPriceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ClientChargedForSaltCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientReactivePriceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ClientProactivePriceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ProactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AreaSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.StartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ForecastingTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04051GCGrittingForecastTypeWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ContractManagerIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00226StaffListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SiteIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04058GCSitesWithBlankDDLBJustGrittingSitesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.SiteGrittingContractIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.GrittingActivationCodeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colForecastingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForecastingTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingActivationCodeValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MinimumPictureCountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForSiteGrittingContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForArea = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForGritOnMonday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritOnTuesday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritOnWednesday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritOnThursday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritOnFriday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritOnSaturday = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritOnSunday = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDefaultGritAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForProactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAccessRestrictions = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSiteOpenTime1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteClosedTime1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteOpenTime2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteClosedTime2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem28 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingTimeOnSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForInvoiceClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDontInvoiceClientReason = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamProactiveRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamReactiveRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultNoAccessRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientProactivePrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientReactivePrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientEveningRateModifier = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem29 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemClientChargedForSalt = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientSaltPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientSaltVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAnnualCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSeasonPeriodID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteGrittingNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForContractManagerID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForForecastingTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingActivationCodeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMinimumTemperature = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBandingStart = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBandingEnd = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRedOverridesBanding = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsFloatingSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingCompletionEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingCompletionEmailLinkedPictures = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem26 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAnnual_Contract = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMinimumPictureCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPrioritySite = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04038_GC_Site_Gritting_Contract_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04038_GC_Site_Gritting_Contract_EditTableAdapter();
            this.sp00226_Staff_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp04051_GC_Gritting_Forecast_Type_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04051_GC_Gritting_Forecast_Type_With_BlankTableAdapter();
            this.sp04052_GC_Gritting_Activation_Codes_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04052_GC_Gritting_Activation_Codes_With_BlankTableAdapter();
            this.sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_SitesTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_SitesTableAdapter();
            this.sp04343_GC_Gritting_Forecast_BandingsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04343_GC_Gritting_Forecast_BandingsTableAdapter();
            this.sp04373WMSeasonPeriodsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp04373_WM_Season_Periods_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04373_WM_Season_Periods_With_BlankTableAdapter();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SeasonPeriodIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04038GCSiteGrittingContractEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04052GCGrittingActivationCodesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DontInvoiceClientReasonMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Annual_ContractCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteClosedTime2TimeSpanEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteOpenTime2TimeSpanEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteClosedTime1TimeSpanEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteOpenTime1TimeSpanEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessRestrictionsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnnualCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingCompletionEmailLinkedPicturesCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingTimeOnSiteSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingCompletionEmailCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFloatingSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandingEndGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04343GCGrittingForecastBandingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandingStartGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedOverridesBandingCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultNoAccessRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrioritySiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteGrittingNotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientEveningRateModifierSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientSaltVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamReactiveRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamProactiveRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultGritAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumTemperatureSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnSundayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnSaturdayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnFridayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnThursdayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnWednesdayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnTuesdayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnMondayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientSaltPriceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientChargedForSaltCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientReactivePriceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientProactivePriceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ForecastingTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04051GCGrittingForecastTypeWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractManagerIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04058GCSitesWithBlankDDLBJustGrittingSitesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteGrittingContractIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingActivationCodeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumPictureCountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteGrittingContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnMonday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnTuesday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnWednesday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnThursday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnFriday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnSaturday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnSunday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultGritAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessRestrictions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteOpenTime1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteClosedTime1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteOpenTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteClosedTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingTimeOnSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDontInvoiceClientReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamProactiveRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamReactiveRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultNoAccessRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientProactivePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientReactivePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientEveningRateModifier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemClientChargedForSalt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientSaltPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientSaltVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAnnualCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSeasonPeriodID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteGrittingNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractManagerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForForecastingTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingActivationCodeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumTemperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBandingStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBandingEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRedOverridesBanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsFloatingSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingCompletionEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingCompletionEmailLinkedPictures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAnnual_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumPictureCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrioritySite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04373WMSeasonPeriodsWithBlankBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(719, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 655);
            this.barDockControlBottom.Size = new System.Drawing.Size(719, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 629);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(719, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 629);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "ID";
            this.gridColumn2.FieldName = "ID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colValue
            // 
            this.colValue.Caption = "Forecast Type ID";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            this.colValue.Width = 116;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Staff ID";
            this.gridColumn57.FieldName = "StaffID";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Width = 59;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colValue1
            // 
            this.colValue1.Caption = "Weather Activation Point ID";
            this.colValue1.FieldName = "Value";
            this.colValue1.Name = "colValue1";
            this.colValue1.OptionsColumn.AllowEdit = false;
            this.colValue1.OptionsColumn.AllowFocus = false;
            this.colValue1.OptionsColumn.ReadOnly = true;
            this.colValue1.Width = 153;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Save Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiFormSave.SuperTip = superToolTip7;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Cancel Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiFormCancel.SuperTip = superToolTip8;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Form Mode - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.barStaticItemFormMode.SuperTip = superToolTip9;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(719, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 655);
            this.barDockControl2.Size = new System.Drawing.Size(719, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 629);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(719, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 629);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SeasonPeriodIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DontInvoiceClientReasonMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.InvoiceClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.Annual_ContractCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteClosedTime2TimeSpanEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteOpenTime2TimeSpanEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteClosedTime1TimeSpanEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteOpenTime1TimeSpanEdit);
            this.dataLayoutControl1.Controls.Add(this.AccessRestrictionsCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.AnnualCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingCompletionEmailLinkedPicturesCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingTimeOnSiteSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingCompletionEmailCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsFloatingSiteCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.BandingEndGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.BandingStartGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.RedOverridesBandingCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultNoAccessRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PrioritySiteCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteGrittingNotesMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientEveningRateModifierSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientSaltVatRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamReactiveRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamProactiveRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultGritAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MinimumTemperatureSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GritOnSundayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GritOnSaturdayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GritOnFridayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GritOnThursdayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GritOnWednesdayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GritOnTuesdayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GritOnMondayCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientSaltPriceSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientChargedForSaltCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientReactivePriceSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientProactivePriceSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ProactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.AreaSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.StartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ForecastingTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractManagerIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SiteGrittingContractIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingActivationCodeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.MinimumPictureCountSpinEdit);
            this.dataLayoutControl1.DataSource = this.sp04038GCSiteGrittingContractEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteGrittingContractID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1201, 228, 565, 578);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(719, 629);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SeasonPeriodIDGridLookUpEdit
            // 
            this.SeasonPeriodIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "SeasonPeriodID", true));
            this.SeasonPeriodIDGridLookUpEdit.Location = new System.Drawing.Point(178, 449);
            this.SeasonPeriodIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SeasonPeriodIDGridLookUpEdit.Name = "SeasonPeriodIDGridLookUpEdit";
            this.SeasonPeriodIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SeasonPeriodIDGridLookUpEdit.Properties.DataSource = this.sp04373WMSeasonPeriodsWithBlankBindingSource;
            this.SeasonPeriodIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SeasonPeriodIDGridLookUpEdit.Properties.MaxLength = 50;
            this.SeasonPeriodIDGridLookUpEdit.Properties.NullText = "";
            this.SeasonPeriodIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SeasonPeriodIDGridLookUpEdit.Properties.View = this.gridView5;
            this.SeasonPeriodIDGridLookUpEdit.Size = new System.Drawing.Size(171, 20);
            this.SeasonPeriodIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SeasonPeriodIDGridLookUpEdit.TabIndex = 27;
            this.SeasonPeriodIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SeasonPeriodIDGridLookUpEdit_Validating);
            // 
            // sp04038GCSiteGrittingContractEditBindingSource
            // 
            this.sp04038GCSiteGrittingContractEditBindingSource.DataMember = "sp04038_GC_Site_Gritting_Contract_Edit";
            this.sp04038GCSiteGrittingContractEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp04052GCGrittingActivationCodesWithBlankBindingSource
            // 
            this.sp04052GCGrittingActivationCodesWithBlankBindingSource.DataMember = "sp04052_GC_Gritting_Activation_Codes_With_Blank";
            this.sp04052GCGrittingActivationCodesWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription2,
            this.colRecordOrder});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.colID1;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView5.FormatRules.Add(gridFormatRule3);
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsCustomization.AllowFilter = false;
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // DontInvoiceClientReasonMemoEdit
            // 
            this.DontInvoiceClientReasonMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "DontInvoiceClientReason", true));
            this.DontInvoiceClientReasonMemoEdit.Location = new System.Drawing.Point(190, 741);
            this.DontInvoiceClientReasonMemoEdit.MenuManager = this.barManager1;
            this.DontInvoiceClientReasonMemoEdit.Name = "DontInvoiceClientReasonMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.DontInvoiceClientReasonMemoEdit, true);
            this.DontInvoiceClientReasonMemoEdit.Size = new System.Drawing.Size(464, 68);
            this.scSpellChecker.SetSpellCheckerOptions(this.DontInvoiceClientReasonMemoEdit, optionsSpelling3);
            this.DontInvoiceClientReasonMemoEdit.StyleController = this.dataLayoutControl1;
            this.DontInvoiceClientReasonMemoEdit.TabIndex = 66;
            // 
            // InvoiceClientCheckEdit
            // 
            this.InvoiceClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "InvoiceClient", true));
            this.InvoiceClientCheckEdit.Location = new System.Drawing.Point(190, 718);
            this.InvoiceClientCheckEdit.MenuManager = this.barManager1;
            this.InvoiceClientCheckEdit.Name = "InvoiceClientCheckEdit";
            this.InvoiceClientCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.InvoiceClientCheckEdit.Properties.ValueChecked = 1;
            this.InvoiceClientCheckEdit.Properties.ValueUnchecked = 0;
            this.InvoiceClientCheckEdit.Size = new System.Drawing.Size(159, 19);
            this.InvoiceClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.InvoiceClientCheckEdit.TabIndex = 25;
            // 
            // Annual_ContractCheckEdit
            // 
            this.Annual_ContractCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "Annual_Contract", true));
            this.Annual_ContractCheckEdit.Location = new System.Drawing.Point(154, 86);
            this.Annual_ContractCheckEdit.MenuManager = this.barManager1;
            this.Annual_ContractCheckEdit.Name = "Annual_ContractCheckEdit";
            this.Annual_ContractCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.Annual_ContractCheckEdit.Properties.ValueChecked = 1;
            this.Annual_ContractCheckEdit.Properties.ValueUnchecked = 0;
            this.Annual_ContractCheckEdit.Size = new System.Drawing.Size(183, 19);
            this.Annual_ContractCheckEdit.StyleController = this.dataLayoutControl1;
            this.Annual_ContractCheckEdit.TabIndex = 58;
            // 
            // SiteClosedTime2TimeSpanEdit
            // 
            this.SiteClosedTime2TimeSpanEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "SiteClosedTime2", true));
            this.SiteClosedTime2TimeSpanEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.SiteClosedTime2TimeSpanEdit.Location = new System.Drawing.Point(495, 569);
            this.SiteClosedTime2TimeSpanEdit.MenuManager = this.barManager1;
            this.SiteClosedTime2TimeSpanEdit.Name = "SiteClosedTime2TimeSpanEdit";
            this.SiteClosedTime2TimeSpanEdit.Properties.AllowEditDays = false;
            this.SiteClosedTime2TimeSpanEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SiteClosedTime2TimeSpanEdit.Properties.Mask.EditMask = "HH:mm:ss";
            this.SiteClosedTime2TimeSpanEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SiteClosedTime2TimeSpanEdit.Size = new System.Drawing.Size(171, 20);
            this.SiteClosedTime2TimeSpanEdit.StyleController = this.dataLayoutControl1;
            this.SiteClosedTime2TimeSpanEdit.TabIndex = 65;
            // 
            // SiteOpenTime2TimeSpanEdit
            // 
            this.SiteOpenTime2TimeSpanEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "SiteOpenTime2", true));
            this.SiteOpenTime2TimeSpanEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.SiteOpenTime2TimeSpanEdit.Location = new System.Drawing.Point(178, 569);
            this.SiteOpenTime2TimeSpanEdit.MenuManager = this.barManager1;
            this.SiteOpenTime2TimeSpanEdit.Name = "SiteOpenTime2TimeSpanEdit";
            this.SiteOpenTime2TimeSpanEdit.Properties.AllowEditDays = false;
            this.SiteOpenTime2TimeSpanEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SiteOpenTime2TimeSpanEdit.Properties.Mask.EditMask = "HH:mm:ss";
            this.SiteOpenTime2TimeSpanEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SiteOpenTime2TimeSpanEdit.Size = new System.Drawing.Size(171, 20);
            this.SiteOpenTime2TimeSpanEdit.StyleController = this.dataLayoutControl1;
            this.SiteOpenTime2TimeSpanEdit.TabIndex = 64;
            // 
            // SiteClosedTime1TimeSpanEdit
            // 
            this.SiteClosedTime1TimeSpanEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "SiteClosedTime1", true));
            this.SiteClosedTime1TimeSpanEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.SiteClosedTime1TimeSpanEdit.Location = new System.Drawing.Point(495, 545);
            this.SiteClosedTime1TimeSpanEdit.MenuManager = this.barManager1;
            this.SiteClosedTime1TimeSpanEdit.Name = "SiteClosedTime1TimeSpanEdit";
            this.SiteClosedTime1TimeSpanEdit.Properties.AllowEditDays = false;
            this.SiteClosedTime1TimeSpanEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SiteClosedTime1TimeSpanEdit.Properties.Mask.EditMask = "HH:mm:ss";
            this.SiteClosedTime1TimeSpanEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SiteClosedTime1TimeSpanEdit.Size = new System.Drawing.Size(171, 20);
            this.SiteClosedTime1TimeSpanEdit.StyleController = this.dataLayoutControl1;
            this.SiteClosedTime1TimeSpanEdit.TabIndex = 63;
            // 
            // SiteOpenTime1TimeSpanEdit
            // 
            this.SiteOpenTime1TimeSpanEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "SiteOpenTime1", true));
            this.SiteOpenTime1TimeSpanEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.SiteOpenTime1TimeSpanEdit.Location = new System.Drawing.Point(178, 545);
            this.SiteOpenTime1TimeSpanEdit.MenuManager = this.barManager1;
            this.SiteOpenTime1TimeSpanEdit.Name = "SiteOpenTime1TimeSpanEdit";
            this.SiteOpenTime1TimeSpanEdit.Properties.AllowEditDays = false;
            this.SiteOpenTime1TimeSpanEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SiteOpenTime1TimeSpanEdit.Properties.Mask.EditMask = "HH:mm:ss";
            this.SiteOpenTime1TimeSpanEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SiteOpenTime1TimeSpanEdit.Size = new System.Drawing.Size(171, 20);
            this.SiteOpenTime1TimeSpanEdit.StyleController = this.dataLayoutControl1;
            this.SiteOpenTime1TimeSpanEdit.TabIndex = 62;
            // 
            // AccessRestrictionsCheckEdit
            // 
            this.AccessRestrictionsCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "AccessRestrictions", true));
            this.AccessRestrictionsCheckEdit.EditValue = 0;
            this.AccessRestrictionsCheckEdit.Location = new System.Drawing.Point(178, 593);
            this.AccessRestrictionsCheckEdit.MenuManager = this.barManager1;
            this.AccessRestrictionsCheckEdit.Name = "AccessRestrictionsCheckEdit";
            this.AccessRestrictionsCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.AccessRestrictionsCheckEdit.Properties.ValueChecked = 1;
            this.AccessRestrictionsCheckEdit.Properties.ValueUnchecked = 0;
            this.AccessRestrictionsCheckEdit.Size = new System.Drawing.Size(170, 19);
            this.AccessRestrictionsCheckEdit.StyleController = this.dataLayoutControl1;
            this.AccessRestrictionsCheckEdit.TabIndex = 46;
            // 
            // AnnualCostSpinEdit
            // 
            this.AnnualCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "AnnualCost", true));
            this.AnnualCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AnnualCostSpinEdit.Location = new System.Drawing.Point(190, 956);
            this.AnnualCostSpinEdit.MenuManager = this.barManager1;
            this.AnnualCostSpinEdit.Name = "AnnualCostSpinEdit";
            this.AnnualCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.AnnualCostSpinEdit.Properties.Mask.EditMask = "c";
            this.AnnualCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AnnualCostSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.AnnualCostSpinEdit.Properties.MinValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            -2147352576});
            this.AnnualCostSpinEdit.Size = new System.Drawing.Size(159, 20);
            this.AnnualCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.AnnualCostSpinEdit.TabIndex = 31;
            // 
            // GrittingCompletionEmailLinkedPicturesCheckEdit
            // 
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GrittingCompletionEmailLinkedPictures", true));
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.Location = new System.Drawing.Point(154, 320);
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.MenuManager = this.barManager1;
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.Name = "GrittingCompletionEmailLinkedPicturesCheckEdit";
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.Properties.ValueChecked = 1;
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.Properties.ValueUnchecked = 0;
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.Size = new System.Drawing.Size(79, 19);
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.StyleController = this.dataLayoutControl1;
            this.GrittingCompletionEmailLinkedPicturesCheckEdit.TabIndex = 57;
            // 
            // GrittingTimeOnSiteSpinEdit
            // 
            this.GrittingTimeOnSiteSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GrittingTimeOnSite", true));
            this.GrittingTimeOnSiteSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.GrittingTimeOnSiteSpinEdit.Location = new System.Drawing.Point(178, 521);
            this.GrittingTimeOnSiteSpinEdit.MenuManager = this.barManager1;
            this.GrittingTimeOnSiteSpinEdit.Name = "GrittingTimeOnSiteSpinEdit";
            this.GrittingTimeOnSiteSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GrittingTimeOnSiteSpinEdit.Properties.Mask.EditMask = "#####0.00 Minutes";
            this.GrittingTimeOnSiteSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.GrittingTimeOnSiteSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            131072});
            this.GrittingTimeOnSiteSpinEdit.Size = new System.Drawing.Size(171, 20);
            this.GrittingTimeOnSiteSpinEdit.StyleController = this.dataLayoutControl1;
            this.GrittingTimeOnSiteSpinEdit.TabIndex = 52;
            // 
            // GrittingCompletionEmailCheckEdit
            // 
            this.GrittingCompletionEmailCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GrittingCompletionEmail", true));
            this.GrittingCompletionEmailCheckEdit.Location = new System.Drawing.Point(154, 297);
            this.GrittingCompletionEmailCheckEdit.MenuManager = this.barManager1;
            this.GrittingCompletionEmailCheckEdit.Name = "GrittingCompletionEmailCheckEdit";
            this.GrittingCompletionEmailCheckEdit.Properties.Caption = "(Tick if Yes to send Email on Gritting Callout Completion)";
            this.GrittingCompletionEmailCheckEdit.Properties.ValueChecked = 1;
            this.GrittingCompletionEmailCheckEdit.Properties.ValueUnchecked = 0;
            this.GrittingCompletionEmailCheckEdit.Size = new System.Drawing.Size(287, 19);
            this.GrittingCompletionEmailCheckEdit.StyleController = this.dataLayoutControl1;
            this.GrittingCompletionEmailCheckEdit.TabIndex = 51;
            // 
            // IsFloatingSiteCheckEdit
            // 
            this.IsFloatingSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "IsFloatingSite", true));
            this.IsFloatingSiteCheckEdit.Location = new System.Drawing.Point(154, 109);
            this.IsFloatingSiteCheckEdit.MenuManager = this.barManager1;
            this.IsFloatingSiteCheckEdit.Name = "IsFloatingSiteCheckEdit";
            this.IsFloatingSiteCheckEdit.Properties.Caption = "(Tick if Yes [Thames Water Sites])";
            this.IsFloatingSiteCheckEdit.Properties.ValueChecked = 1;
            this.IsFloatingSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.IsFloatingSiteCheckEdit.Size = new System.Drawing.Size(183, 19);
            this.IsFloatingSiteCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsFloatingSiteCheckEdit.TabIndex = 50;
            this.IsFloatingSiteCheckEdit.EditValueChanged += new System.EventHandler(this.IsFloatingSiteCheckEdit_EditValueChanged);
            this.IsFloatingSiteCheckEdit.Validated += new System.EventHandler(this.IsFloatingSiteCheckEdit_Validated);
            // 
            // BandingEndGridLookUpEdit
            // 
            this.BandingEndGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "BandingEnd", true));
            this.BandingEndGridLookUpEdit.Location = new System.Drawing.Point(514, 204);
            this.BandingEndGridLookUpEdit.MenuManager = this.barManager1;
            this.BandingEndGridLookUpEdit.Name = "BandingEndGridLookUpEdit";
            this.BandingEndGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BandingEndGridLookUpEdit.Properties.DataSource = this.sp04343GCGrittingForecastBandingsBindingSource;
            this.BandingEndGridLookUpEdit.Properties.DisplayMember = "Value";
            this.BandingEndGridLookUpEdit.Properties.NullText = "";
            this.BandingEndGridLookUpEdit.Properties.ValueMember = "ID";
            this.BandingEndGridLookUpEdit.Properties.View = this.gridLookUpEdit2View;
            this.BandingEndGridLookUpEdit.Size = new System.Drawing.Size(176, 20);
            this.BandingEndGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.BandingEndGridLookUpEdit.TabIndex = 49;
            // 
            // sp04343GCGrittingForecastBandingsBindingSource
            // 
            this.sp04343GCGrittingForecastBandingsBindingSource.DataMember = "sp04343_GC_Gritting_Forecast_Bandings";
            this.sp04343GCGrittingForecastBandingsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit2View
            // 
            this.gridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition13.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition13.Appearance.Options.UseForeColor = true;
            styleFormatCondition13.ApplyToRow = true;
            styleFormatCondition13.Column = this.gridColumn2;
            styleFormatCondition13.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition13.Value1 = 0;
            this.gridLookUpEdit2View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition13});
            this.gridLookUpEdit2View.Name = "gridLookUpEdit2View";
            this.gridLookUpEdit2View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "ItemOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Band End Time";
            this.gridColumn4.FieldName = "Value";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 220;
            // 
            // BandingStartGridLookUpEdit
            // 
            this.BandingStartGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "BandingStart", true));
            this.BandingStartGridLookUpEdit.Location = new System.Drawing.Point(154, 204);
            this.BandingStartGridLookUpEdit.MenuManager = this.barManager1;
            this.BandingStartGridLookUpEdit.Name = "BandingStartGridLookUpEdit";
            this.BandingStartGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BandingStartGridLookUpEdit.Properties.DataSource = this.sp04343GCGrittingForecastBandingsBindingSource;
            this.BandingStartGridLookUpEdit.Properties.DisplayMember = "Value";
            this.BandingStartGridLookUpEdit.Properties.NullText = "";
            this.BandingStartGridLookUpEdit.Properties.ValueMember = "ID";
            this.BandingStartGridLookUpEdit.Properties.View = this.gridView4;
            this.BandingStartGridLookUpEdit.Size = new System.Drawing.Size(214, 20);
            this.BandingStartGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.BandingStartGridLookUpEdit.TabIndex = 48;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colItemOrder,
            this.gridColumn1});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition14.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition14.Appearance.Options.UseForeColor = true;
            styleFormatCondition14.ApplyToRow = true;
            styleFormatCondition14.Column = this.colID;
            styleFormatCondition14.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition14.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition14});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemOrder
            // 
            this.colItemOrder.Caption = "Order";
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.AllowEdit = false;
            this.colItemOrder.OptionsColumn.AllowFocus = false;
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Band Start Time";
            this.gridColumn1.FieldName = "Value";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 220;
            // 
            // RedOverridesBandingCheckEdit
            // 
            this.RedOverridesBandingCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "RedOverridesBanding", true));
            this.RedOverridesBandingCheckEdit.EditValue = 0;
            this.RedOverridesBandingCheckEdit.Location = new System.Drawing.Point(154, 228);
            this.RedOverridesBandingCheckEdit.MenuManager = this.barManager1;
            this.RedOverridesBandingCheckEdit.Name = "RedOverridesBandingCheckEdit";
            this.RedOverridesBandingCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.RedOverridesBandingCheckEdit.Properties.ValueChecked = 1;
            this.RedOverridesBandingCheckEdit.Properties.ValueUnchecked = 0;
            this.RedOverridesBandingCheckEdit.Size = new System.Drawing.Size(166, 19);
            this.RedOverridesBandingCheckEdit.StyleController = this.dataLayoutControl1;
            this.RedOverridesBandingCheckEdit.TabIndex = 47;
            // 
            // DefaultNoAccessRateSpinEdit
            // 
            this.DefaultNoAccessRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "DefaultNoAccessRate", true));
            this.DefaultNoAccessRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DefaultNoAccessRateSpinEdit.Location = new System.Drawing.Point(190, 837);
            this.DefaultNoAccessRateSpinEdit.MenuManager = this.barManager1;
            this.DefaultNoAccessRateSpinEdit.Name = "DefaultNoAccessRateSpinEdit";
            this.DefaultNoAccessRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DefaultNoAccessRateSpinEdit.Properties.Mask.EditMask = "c";
            this.DefaultNoAccessRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DefaultNoAccessRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.DefaultNoAccessRateSpinEdit.Size = new System.Drawing.Size(159, 20);
            this.DefaultNoAccessRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.DefaultNoAccessRateSpinEdit.TabIndex = 46;
            // 
            // PrioritySiteCheckEdit
            // 
            this.PrioritySiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "PrioritySite", true));
            this.PrioritySiteCheckEdit.EditValue = 0;
            this.PrioritySiteCheckEdit.Location = new System.Drawing.Point(154, 274);
            this.PrioritySiteCheckEdit.MenuManager = this.barManager1;
            this.PrioritySiteCheckEdit.Name = "PrioritySiteCheckEdit";
            this.PrioritySiteCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.PrioritySiteCheckEdit.Properties.ValueChecked = 1;
            this.PrioritySiteCheckEdit.Properties.ValueUnchecked = 0;
            this.PrioritySiteCheckEdit.Size = new System.Drawing.Size(166, 19);
            this.PrioritySiteCheckEdit.StyleController = this.dataLayoutControl1;
            this.PrioritySiteCheckEdit.TabIndex = 45;
            // 
            // SiteGrittingNotesMemoEdit
            // 
            this.SiteGrittingNotesMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "SiteGrittingNotes", true));
            this.SiteGrittingNotesMemoEdit.Location = new System.Drawing.Point(36, 449);
            this.SiteGrittingNotesMemoEdit.MenuManager = this.barManager1;
            this.SiteGrittingNotesMemoEdit.Name = "SiteGrittingNotesMemoEdit";
            this.SiteGrittingNotesMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteGrittingNotesMemoEdit, true);
            this.SiteGrittingNotesMemoEdit.Size = new System.Drawing.Size(630, 756);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteGrittingNotesMemoEdit, optionsSpelling4);
            this.SiteGrittingNotesMemoEdit.StyleController = this.dataLayoutControl1;
            this.SiteGrittingNotesMemoEdit.TabIndex = 44;
            // 
            // ClientEveningRateModifierSpinEdit
            // 
            this.ClientEveningRateModifierSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "ClientEveningRateModifier", true));
            this.ClientEveningRateModifierSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientEveningRateModifierSpinEdit.Location = new System.Drawing.Point(190, 885);
            this.ClientEveningRateModifierSpinEdit.MenuManager = this.barManager1;
            this.ClientEveningRateModifierSpinEdit.Name = "ClientEveningRateModifierSpinEdit";
            this.ClientEveningRateModifierSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ClientEveningRateModifierSpinEdit.Properties.Mask.EditMask = "f2";
            this.ClientEveningRateModifierSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ClientEveningRateModifierSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            131072});
            this.ClientEveningRateModifierSpinEdit.Size = new System.Drawing.Size(159, 20);
            this.ClientEveningRateModifierSpinEdit.StyleController = this.dataLayoutControl1;
            this.ClientEveningRateModifierSpinEdit.TabIndex = 43;
            // 
            // ClientSaltVatRateSpinEdit
            // 
            this.ClientSaltVatRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "ClientSaltVatRate", true));
            this.ClientSaltVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientSaltVatRateSpinEdit.Location = new System.Drawing.Point(495, 932);
            this.ClientSaltVatRateSpinEdit.MenuManager = this.barManager1;
            this.ClientSaltVatRateSpinEdit.Name = "ClientSaltVatRateSpinEdit";
            this.ClientSaltVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ClientSaltVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.ClientSaltVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ClientSaltVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ClientSaltVatRateSpinEdit.Size = new System.Drawing.Size(159, 20);
            this.ClientSaltVatRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.ClientSaltVatRateSpinEdit.TabIndex = 42;
            // 
            // TeamReactiveRateSpinEdit
            // 
            this.TeamReactiveRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "TeamReactiveRate", true));
            this.TeamReactiveRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TeamReactiveRateSpinEdit.Location = new System.Drawing.Point(495, 813);
            this.TeamReactiveRateSpinEdit.MenuManager = this.barManager1;
            this.TeamReactiveRateSpinEdit.Name = "TeamReactiveRateSpinEdit";
            this.TeamReactiveRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TeamReactiveRateSpinEdit.Properties.Mask.EditMask = "c";
            this.TeamReactiveRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TeamReactiveRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TeamReactiveRateSpinEdit.Size = new System.Drawing.Size(159, 20);
            this.TeamReactiveRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.TeamReactiveRateSpinEdit.TabIndex = 41;
            // 
            // TeamProactiveRateSpinEdit
            // 
            this.TeamProactiveRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "TeamProactiveRate", true));
            this.TeamProactiveRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TeamProactiveRateSpinEdit.Location = new System.Drawing.Point(190, 813);
            this.TeamProactiveRateSpinEdit.MenuManager = this.barManager1;
            this.TeamProactiveRateSpinEdit.Name = "TeamProactiveRateSpinEdit";
            this.TeamProactiveRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TeamProactiveRateSpinEdit.Properties.Mask.EditMask = "c";
            this.TeamProactiveRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TeamProactiveRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.TeamProactiveRateSpinEdit.Size = new System.Drawing.Size(159, 20);
            this.TeamProactiveRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.TeamProactiveRateSpinEdit.TabIndex = 40;
            // 
            // DefaultGritAmountSpinEdit
            // 
            this.DefaultGritAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "DefaultGritAmount", true));
            this.DefaultGritAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DefaultGritAmountSpinEdit.Location = new System.Drawing.Point(496, 626);
            this.DefaultGritAmountSpinEdit.MenuManager = this.barManager1;
            this.DefaultGritAmountSpinEdit.Name = "DefaultGritAmountSpinEdit";
            this.DefaultGritAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DefaultGritAmountSpinEdit.Properties.Mask.EditMask = "########0.00 25 kg Bags";
            this.DefaultGritAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DefaultGritAmountSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.DefaultGritAmountSpinEdit.Size = new System.Drawing.Size(170, 20);
            this.DefaultGritAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.DefaultGritAmountSpinEdit.TabIndex = 39;
            // 
            // MinimumTemperatureSpinEdit
            // 
            this.MinimumTemperatureSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "MinimumTemperature", true));
            this.MinimumTemperatureSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MinimumTemperatureSpinEdit.Location = new System.Drawing.Point(154, 180);
            this.MinimumTemperatureSpinEdit.MenuManager = this.barManager1;
            this.MinimumTemperatureSpinEdit.Name = "MinimumTemperatureSpinEdit";
            this.MinimumTemperatureSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MinimumTemperatureSpinEdit.Properties.Mask.EditMask = "########0.00 Degrees";
            this.MinimumTemperatureSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MinimumTemperatureSpinEdit.Size = new System.Drawing.Size(214, 20);
            this.MinimumTemperatureSpinEdit.StyleController = this.dataLayoutControl1;
            this.MinimumTemperatureSpinEdit.TabIndex = 38;
            // 
            // GritOnSundayCheckEdit
            // 
            this.GritOnSundayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GritOnSunday", true));
            this.GritOnSundayCheckEdit.Location = new System.Drawing.Point(190, 1174);
            this.GritOnSundayCheckEdit.MenuManager = this.barManager1;
            this.GritOnSundayCheckEdit.Name = "GritOnSundayCheckEdit";
            this.GritOnSundayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GritOnSundayCheckEdit.Properties.ValueChecked = 1;
            this.GritOnSundayCheckEdit.Properties.ValueUnchecked = 0;
            this.GritOnSundayCheckEdit.Size = new System.Drawing.Size(158, 19);
            this.GritOnSundayCheckEdit.StyleController = this.dataLayoutControl1;
            this.GritOnSundayCheckEdit.TabIndex = 37;
            // 
            // GritOnSaturdayCheckEdit
            // 
            this.GritOnSaturdayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GritOnSaturday", true));
            this.GritOnSaturdayCheckEdit.Location = new System.Drawing.Point(190, 1151);
            this.GritOnSaturdayCheckEdit.MenuManager = this.barManager1;
            this.GritOnSaturdayCheckEdit.Name = "GritOnSaturdayCheckEdit";
            this.GritOnSaturdayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GritOnSaturdayCheckEdit.Properties.ValueChecked = 1;
            this.GritOnSaturdayCheckEdit.Properties.ValueUnchecked = 0;
            this.GritOnSaturdayCheckEdit.Size = new System.Drawing.Size(158, 19);
            this.GritOnSaturdayCheckEdit.StyleController = this.dataLayoutControl1;
            this.GritOnSaturdayCheckEdit.TabIndex = 36;
            // 
            // GritOnFridayCheckEdit
            // 
            this.GritOnFridayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GritOnFriday", true));
            this.GritOnFridayCheckEdit.Location = new System.Drawing.Point(190, 1128);
            this.GritOnFridayCheckEdit.MenuManager = this.barManager1;
            this.GritOnFridayCheckEdit.Name = "GritOnFridayCheckEdit";
            this.GritOnFridayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GritOnFridayCheckEdit.Properties.ValueChecked = 1;
            this.GritOnFridayCheckEdit.Properties.ValueUnchecked = 0;
            this.GritOnFridayCheckEdit.Size = new System.Drawing.Size(158, 19);
            this.GritOnFridayCheckEdit.StyleController = this.dataLayoutControl1;
            this.GritOnFridayCheckEdit.TabIndex = 35;
            // 
            // GritOnThursdayCheckEdit
            // 
            this.GritOnThursdayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GritOnThursday", true));
            this.GritOnThursdayCheckEdit.Location = new System.Drawing.Point(190, 1105);
            this.GritOnThursdayCheckEdit.MenuManager = this.barManager1;
            this.GritOnThursdayCheckEdit.Name = "GritOnThursdayCheckEdit";
            this.GritOnThursdayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GritOnThursdayCheckEdit.Properties.ValueChecked = 1;
            this.GritOnThursdayCheckEdit.Properties.ValueUnchecked = 0;
            this.GritOnThursdayCheckEdit.Size = new System.Drawing.Size(158, 19);
            this.GritOnThursdayCheckEdit.StyleController = this.dataLayoutControl1;
            this.GritOnThursdayCheckEdit.TabIndex = 34;
            // 
            // GritOnWednesdayCheckEdit
            // 
            this.GritOnWednesdayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GritOnWednesday", true));
            this.GritOnWednesdayCheckEdit.Location = new System.Drawing.Point(190, 1082);
            this.GritOnWednesdayCheckEdit.MenuManager = this.barManager1;
            this.GritOnWednesdayCheckEdit.Name = "GritOnWednesdayCheckEdit";
            this.GritOnWednesdayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GritOnWednesdayCheckEdit.Properties.ValueChecked = 1;
            this.GritOnWednesdayCheckEdit.Properties.ValueUnchecked = 0;
            this.GritOnWednesdayCheckEdit.Size = new System.Drawing.Size(158, 19);
            this.GritOnWednesdayCheckEdit.StyleController = this.dataLayoutControl1;
            this.GritOnWednesdayCheckEdit.TabIndex = 33;
            // 
            // GritOnTuesdayCheckEdit
            // 
            this.GritOnTuesdayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GritOnTuesday", true));
            this.GritOnTuesdayCheckEdit.Location = new System.Drawing.Point(190, 1059);
            this.GritOnTuesdayCheckEdit.MenuManager = this.barManager1;
            this.GritOnTuesdayCheckEdit.Name = "GritOnTuesdayCheckEdit";
            this.GritOnTuesdayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GritOnTuesdayCheckEdit.Properties.ValueChecked = 1;
            this.GritOnTuesdayCheckEdit.Properties.ValueUnchecked = 0;
            this.GritOnTuesdayCheckEdit.Size = new System.Drawing.Size(158, 19);
            this.GritOnTuesdayCheckEdit.StyleController = this.dataLayoutControl1;
            this.GritOnTuesdayCheckEdit.TabIndex = 32;
            // 
            // GritOnMondayCheckEdit
            // 
            this.GritOnMondayCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GritOnMonday", true));
            this.GritOnMondayCheckEdit.Location = new System.Drawing.Point(190, 1036);
            this.GritOnMondayCheckEdit.MenuManager = this.barManager1;
            this.GritOnMondayCheckEdit.Name = "GritOnMondayCheckEdit";
            this.GritOnMondayCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GritOnMondayCheckEdit.Properties.ValueChecked = 1;
            this.GritOnMondayCheckEdit.Properties.ValueUnchecked = 0;
            this.GritOnMondayCheckEdit.Size = new System.Drawing.Size(158, 19);
            this.GritOnMondayCheckEdit.StyleController = this.dataLayoutControl1;
            this.GritOnMondayCheckEdit.TabIndex = 31;
            // 
            // ClientSaltPriceSpinEdit
            // 
            this.ClientSaltPriceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "ClientSaltPrice", true));
            this.ClientSaltPriceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientSaltPriceSpinEdit.Location = new System.Drawing.Point(190, 932);
            this.ClientSaltPriceSpinEdit.MenuManager = this.barManager1;
            this.ClientSaltPriceSpinEdit.Name = "ClientSaltPriceSpinEdit";
            this.ClientSaltPriceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ClientSaltPriceSpinEdit.Properties.Mask.EditMask = "c";
            this.ClientSaltPriceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ClientSaltPriceSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ClientSaltPriceSpinEdit.Size = new System.Drawing.Size(159, 20);
            this.ClientSaltPriceSpinEdit.StyleController = this.dataLayoutControl1;
            this.ClientSaltPriceSpinEdit.TabIndex = 30;
            // 
            // ClientChargedForSaltCheckEdit
            // 
            this.ClientChargedForSaltCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "ClientChargedForSalt", true));
            this.ClientChargedForSaltCheckEdit.Location = new System.Drawing.Point(190, 909);
            this.ClientChargedForSaltCheckEdit.MenuManager = this.barManager1;
            this.ClientChargedForSaltCheckEdit.Name = "ClientChargedForSaltCheckEdit";
            this.ClientChargedForSaltCheckEdit.Properties.Caption = "(tick if Yes)";
            this.ClientChargedForSaltCheckEdit.Properties.ValueChecked = 1;
            this.ClientChargedForSaltCheckEdit.Properties.ValueUnchecked = 0;
            this.ClientChargedForSaltCheckEdit.Size = new System.Drawing.Size(159, 19);
            this.ClientChargedForSaltCheckEdit.StyleController = this.dataLayoutControl1;
            this.ClientChargedForSaltCheckEdit.TabIndex = 29;
            this.ClientChargedForSaltCheckEdit.Validated += new System.EventHandler(this.ClientChargedForSaltCheckEdit_Validated);
            // 
            // ClientReactivePriceSpinEdit
            // 
            this.ClientReactivePriceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "ClientReactivePrice", true));
            this.ClientReactivePriceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientReactivePriceSpinEdit.Location = new System.Drawing.Point(495, 861);
            this.ClientReactivePriceSpinEdit.MenuManager = this.barManager1;
            this.ClientReactivePriceSpinEdit.Name = "ClientReactivePriceSpinEdit";
            this.ClientReactivePriceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ClientReactivePriceSpinEdit.Properties.Mask.EditMask = "c";
            this.ClientReactivePriceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ClientReactivePriceSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ClientReactivePriceSpinEdit.Size = new System.Drawing.Size(159, 20);
            this.ClientReactivePriceSpinEdit.StyleController = this.dataLayoutControl1;
            this.ClientReactivePriceSpinEdit.TabIndex = 28;
            // 
            // ClientProactivePriceSpinEdit
            // 
            this.ClientProactivePriceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "ClientProactivePrice", true));
            this.ClientProactivePriceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientProactivePriceSpinEdit.Location = new System.Drawing.Point(190, 861);
            this.ClientProactivePriceSpinEdit.MenuManager = this.barManager1;
            this.ClientProactivePriceSpinEdit.Name = "ClientProactivePriceSpinEdit";
            this.ClientProactivePriceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ClientProactivePriceSpinEdit.Properties.Mask.EditMask = "c";
            this.ClientProactivePriceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ClientProactivePriceSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ClientProactivePriceSpinEdit.Size = new System.Drawing.Size(159, 20);
            this.ClientProactivePriceSpinEdit.StyleController = this.dataLayoutControl1;
            this.ClientProactivePriceSpinEdit.TabIndex = 27;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "Reactive", true));
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(496, 650);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "(tick if Yes)";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(170, 19);
            this.ReactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCheckEdit.TabIndex = 25;
            this.ReactiveCheckEdit.Validated += new System.EventHandler(this.ReactiveCheckEdit_Validated);
            // 
            // ProactiveCheckEdit
            // 
            this.ProactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "Proactive", true));
            this.ProactiveCheckEdit.Location = new System.Drawing.Point(178, 650);
            this.ProactiveCheckEdit.MenuManager = this.barManager1;
            this.ProactiveCheckEdit.Name = "ProactiveCheckEdit";
            this.ProactiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ProactiveCheckEdit.Properties.ValueChecked = 1;
            this.ProactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ProactiveCheckEdit.Size = new System.Drawing.Size(172, 19);
            this.ProactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ProactiveCheckEdit.TabIndex = 24;
            this.ProactiveCheckEdit.Validated += new System.EventHandler(this.ProactiveCheckEdit_Validated);
            // 
            // AreaSpinEdit
            // 
            this.AreaSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "Area", true));
            this.AreaSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AreaSpinEdit.Location = new System.Drawing.Point(178, 626);
            this.AreaSpinEdit.MenuManager = this.barManager1;
            this.AreaSpinEdit.Name = "AreaSpinEdit";
            this.AreaSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.AreaSpinEdit.Properties.Mask.EditMask = "########0.00 M�";
            this.AreaSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AreaSpinEdit.Properties.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            131072});
            this.AreaSpinEdit.Size = new System.Drawing.Size(172, 20);
            this.AreaSpinEdit.StyleController = this.dataLayoutControl1;
            this.AreaSpinEdit.TabIndex = 23;
            // 
            // EndDateDateEdit
            // 
            this.EndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "EndDate", true));
            this.EndDateDateEdit.EditValue = null;
            this.EndDateDateEdit.Location = new System.Drawing.Point(178, 497);
            this.EndDateDateEdit.MenuManager = this.barManager1;
            this.EndDateDateEdit.Name = "EndDateDateEdit";
            this.EndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EndDateDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.EndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Size = new System.Drawing.Size(171, 20);
            this.EndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EndDateDateEdit.TabIndex = 22;
            // 
            // StartDateDateEdit
            // 
            this.StartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "StartDate", true));
            this.StartDateDateEdit.EditValue = null;
            this.StartDateDateEdit.Location = new System.Drawing.Point(178, 473);
            this.StartDateDateEdit.MenuManager = this.barManager1;
            this.StartDateDateEdit.Name = "StartDateDateEdit";
            this.StartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StartDateDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.StartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Size = new System.Drawing.Size(171, 20);
            this.StartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.StartDateDateEdit.TabIndex = 21;
            // 
            // ForecastingTypeIDGridLookUpEdit
            // 
            this.ForecastingTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "ForecastingTypeID", true));
            this.ForecastingTypeIDGridLookUpEdit.EditValue = "";
            this.ForecastingTypeIDGridLookUpEdit.Location = new System.Drawing.Point(154, 132);
            this.ForecastingTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ForecastingTypeIDGridLookUpEdit.Name = "ForecastingTypeIDGridLookUpEdit";
            this.ForecastingTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ForecastingTypeIDGridLookUpEdit.Properties.DataSource = this.sp04051GCGrittingForecastTypeWithBlankBindingSource;
            this.ForecastingTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ForecastingTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ForecastingTypeIDGridLookUpEdit.Properties.ValueMember = "Value";
            this.ForecastingTypeIDGridLookUpEdit.Properties.View = this.gridView2;
            this.ForecastingTypeIDGridLookUpEdit.Size = new System.Drawing.Size(536, 20);
            this.ForecastingTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ForecastingTypeIDGridLookUpEdit.TabIndex = 20;
            this.ForecastingTypeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ForecastingTypeIDGridLookUpEdit_EditValueChanged);
            this.ForecastingTypeIDGridLookUpEdit.Validated += new System.EventHandler(this.ForecastingTypeIDGridLookUpEdit_Validated);
            // 
            // sp04051GCGrittingForecastTypeWithBlankBindingSource
            // 
            this.sp04051GCGrittingForecastTypeWithBlankBindingSource.DataMember = "sp04051_GC_Gritting_Forecast_Type_With_Blank";
            this.sp04051GCGrittingForecastTypeWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colValue,
            this.colDescription});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition15.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition15.Appearance.Options.UseForeColor = true;
            styleFormatCondition15.ApplyToRow = true;
            styleFormatCondition15.Column = this.colValue;
            styleFormatCondition15.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition15.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition15});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Forecast Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 254;
            // 
            // ContractManagerIDGridLookUpEdit
            // 
            this.ContractManagerIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "ContractManagerID", true));
            this.ContractManagerIDGridLookUpEdit.Location = new System.Drawing.Point(154, 62);
            this.ContractManagerIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContractManagerIDGridLookUpEdit.Name = "ContractManagerIDGridLookUpEdit";
            this.ContractManagerIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractManagerIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.ContractManagerIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.ContractManagerIDGridLookUpEdit.Properties.NullText = "";
            this.ContractManagerIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.ContractManagerIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.ContractManagerIDGridLookUpEdit.Properties.View = this.gridView1;
            this.ContractManagerIDGridLookUpEdit.Size = new System.Drawing.Size(536, 20);
            this.ContractManagerIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContractManagerIDGridLookUpEdit.TabIndex = 19;
            // 
            // sp00226StaffListWithBlankBindingSource
            // 
            this.sp00226StaffListWithBlankBindingSource.DataMember = "sp00226_Staff_List_With_Blank";
            this.sp00226StaffListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition16.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition16.Appearance.Options.UseForeColor = true;
            styleFormatCondition16.ApplyToRow = true;
            styleFormatCondition16.Column = this.gridColumn57;
            styleFormatCondition16.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition16.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition16});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn53, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Active";
            this.gridColumn52.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn52.FieldName = "Active";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 1;
            this.gridColumn52.Width = 51;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Surname: Forename";
            this.gridColumn53.FieldName = "DisplayName";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 0;
            this.gridColumn53.Width = 225;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Email";
            this.gridColumn54.FieldName = "Email";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 192;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Forename";
            this.gridColumn55.FieldName = "Forename";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Width = 178;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Network ID";
            this.gridColumn56.FieldName = "NetworkID";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Width = 180;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Staff Name";
            this.gridColumn58.FieldName = "StaffName";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Width = 231;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Surname";
            this.gridColumn59.FieldName = "Surname";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Width = 191;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "User Type";
            this.gridColumn60.FieldName = "UserType";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Width = 223;
            // 
            // ActiveCheckEdit
            // 
            this.ActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "Active", true));
            this.ActiveCheckEdit.EditValue = 0;
            this.ActiveCheckEdit.Location = new System.Drawing.Point(154, 251);
            this.ActiveCheckEdit.MenuManager = this.barManager1;
            this.ActiveCheckEdit.Name = "ActiveCheckEdit";
            this.ActiveCheckEdit.Properties.Caption = "(Tick if Yes, Only one per Site)";
            this.ActiveCheckEdit.Properties.ValueChecked = 1;
            this.ActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveCheckEdit.Size = new System.Drawing.Size(166, 19);
            this.ActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveCheckEdit.TabIndex = 18;
            // 
            // SiteIDGridLookUpEdit
            // 
            this.SiteIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "SiteID", true));
            this.SiteIDGridLookUpEdit.Location = new System.Drawing.Point(154, 36);
            this.SiteIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SiteIDGridLookUpEdit.Name = "SiteIDGridLookUpEdit";
            this.SiteIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Edit Underlying Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Reload Underlying Data", "reload", null, true)});
            this.SiteIDGridLookUpEdit.Properties.DataSource = this.sp04058GCSitesWithBlankDDLBJustGrittingSitesBindingSource;
            this.SiteIDGridLookUpEdit.Properties.DisplayMember = "SiteName";
            this.SiteIDGridLookUpEdit.Properties.NullText = "";
            this.SiteIDGridLookUpEdit.Properties.ValueMember = "SiteID";
            this.SiteIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.SiteIDGridLookUpEdit.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteIDGridLookUpEdit_Properties_ButtonClick);
            this.SiteIDGridLookUpEdit.Size = new System.Drawing.Size(536, 22);
            this.SiteIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDGridLookUpEdit.TabIndex = 16;
            this.SiteIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteIDGridLookUpEdit_Validating);
            // 
            // sp04058GCSitesWithBlankDDLBJustGrittingSitesBindingSource
            // 
            this.sp04058GCSitesWithBlankDDLBJustGrittingSitesBindingSource.DataMember = "sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_Sites";
            this.sp04058GCSitesWithBlankDDLBJustGrittingSitesBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientCode,
            this.colClientID,
            this.colClientName,
            this.colClientSiteName,
            this.colContactPerson,
            this.colSiteAddressLine1,
            this.colSiteCode,
            this.colSiteID,
            this.colSiteName,
            this.colSiteTypeDescription,
            this.colSiteTypeID,
            this.colXCoordinate,
            this.colYCoordinate});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition17.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition17.Appearance.Options.UseForeColor = true;
            styleFormatCondition17.ApplyToRow = true;
            styleFormatCondition17.Column = this.colSiteID;
            styleFormatCondition17.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition17.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition17});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 181;
            // 
            // colClientSiteName
            // 
            this.colClientSiteName.Caption = "Client \\ Site Name";
            this.colClientSiteName.FieldName = "ClientSiteName";
            this.colClientSiteName.Name = "colClientSiteName";
            this.colClientSiteName.OptionsColumn.AllowEdit = false;
            this.colClientSiteName.OptionsColumn.AllowFocus = false;
            this.colClientSiteName.OptionsColumn.ReadOnly = true;
            this.colClientSiteName.Width = 242;
            // 
            // colContactPerson
            // 
            this.colContactPerson.Caption = "Contact Person";
            this.colContactPerson.FieldName = "ContactPerson";
            this.colContactPerson.Name = "colContactPerson";
            this.colContactPerson.OptionsColumn.AllowEdit = false;
            this.colContactPerson.OptionsColumn.AllowFocus = false;
            this.colContactPerson.OptionsColumn.ReadOnly = true;
            this.colContactPerson.Visible = true;
            this.colContactPerson.VisibleIndex = 3;
            this.colContactPerson.Width = 168;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 4;
            this.colSiteAddressLine1.Width = 141;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 1;
            this.colSiteCode.Width = 96;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 251;
            // 
            // colSiteTypeDescription
            // 
            this.colSiteTypeDescription.Caption = "Site Type";
            this.colSiteTypeDescription.FieldName = "SiteTypeDescription";
            this.colSiteTypeDescription.Name = "colSiteTypeDescription";
            this.colSiteTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSiteTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSiteTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSiteTypeDescription.Visible = true;
            this.colSiteTypeDescription.VisibleIndex = 2;
            this.colSiteTypeDescription.Width = 99;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04038GCSiteGrittingContractEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(155, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // SiteGrittingContractIDSpinEdit
            // 
            this.SiteGrittingContractIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "SiteGrittingContractID", true));
            this.SiteGrittingContractIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SiteGrittingContractIDSpinEdit.Location = new System.Drawing.Point(135, 112);
            this.SiteGrittingContractIDSpinEdit.MenuManager = this.barManager1;
            this.SiteGrittingContractIDSpinEdit.Name = "SiteGrittingContractIDSpinEdit";
            this.SiteGrittingContractIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SiteGrittingContractIDSpinEdit.Properties.ReadOnly = true;
            this.SiteGrittingContractIDSpinEdit.Size = new System.Drawing.Size(481, 20);
            this.SiteGrittingContractIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.SiteGrittingContractIDSpinEdit.TabIndex = 4;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 449);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(630, 756);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling1);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // GrittingActivationCodeIDGridLookUpEdit
            // 
            this.GrittingActivationCodeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "GrittingActivationCodeID", true));
            this.GrittingActivationCodeIDGridLookUpEdit.Location = new System.Drawing.Point(154, 156);
            this.GrittingActivationCodeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.GrittingActivationCodeIDGridLookUpEdit.Name = "GrittingActivationCodeIDGridLookUpEdit";
            this.GrittingActivationCodeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GrittingActivationCodeIDGridLookUpEdit.Properties.DataSource = this.sp04052GCGrittingActivationCodesWithBlankBindingSource;
            this.GrittingActivationCodeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.GrittingActivationCodeIDGridLookUpEdit.Properties.MaxLength = 50;
            this.GrittingActivationCodeIDGridLookUpEdit.Properties.NullText = "";
            this.GrittingActivationCodeIDGridLookUpEdit.Properties.ValueMember = "Value";
            this.GrittingActivationCodeIDGridLookUpEdit.Properties.View = this.gridView3;
            this.GrittingActivationCodeIDGridLookUpEdit.Size = new System.Drawing.Size(536, 20);
            this.GrittingActivationCodeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.GrittingActivationCodeIDGridLookUpEdit.TabIndex = 26;
            this.GrittingActivationCodeIDGridLookUpEdit.Enter += new System.EventHandler(this.GrittingActivationCodeIDGridLookUpEdit_Enter);
            this.GrittingActivationCodeIDGridLookUpEdit.Leave += new System.EventHandler(this.GrittingActivationCodeIDGridLookUpEdit_Leave);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colValue1,
            this.colForecastingTypeID,
            this.colForecastingTypeDescription,
            this.colDescription1,
            this.colGrittingActivationCodeValue});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition18.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition18.Appearance.Options.UseForeColor = true;
            styleFormatCondition18.ApplyToRow = true;
            styleFormatCondition18.Column = this.colValue1;
            styleFormatCondition18.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition18.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition18});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsFilter.AllowMRUFilterList = false;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colForecastingTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colForecastingTypeID
            // 
            this.colForecastingTypeID.Caption = "Forecasting Type ID";
            this.colForecastingTypeID.FieldName = "ForecastingTypeID";
            this.colForecastingTypeID.Name = "colForecastingTypeID";
            this.colForecastingTypeID.OptionsColumn.AllowEdit = false;
            this.colForecastingTypeID.OptionsColumn.AllowFocus = false;
            this.colForecastingTypeID.OptionsColumn.ReadOnly = true;
            this.colForecastingTypeID.Width = 118;
            // 
            // colForecastingTypeDescription
            // 
            this.colForecastingTypeDescription.Caption = "Forecasting Type";
            this.colForecastingTypeDescription.FieldName = "ForecastingTypeDescription";
            this.colForecastingTypeDescription.Name = "colForecastingTypeDescription";
            this.colForecastingTypeDescription.OptionsColumn.AllowEdit = false;
            this.colForecastingTypeDescription.OptionsColumn.AllowFocus = false;
            this.colForecastingTypeDescription.OptionsColumn.ReadOnly = true;
            this.colForecastingTypeDescription.Visible = true;
            this.colForecastingTypeDescription.VisibleIndex = 1;
            this.colForecastingTypeDescription.Width = 247;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Weather Activation Point";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 219;
            // 
            // colGrittingActivationCodeValue
            // 
            this.colGrittingActivationCodeValue.Caption = "Gritting Activation Value";
            this.colGrittingActivationCodeValue.FieldName = "GrittingActivationCodeValue";
            this.colGrittingActivationCodeValue.Name = "colGrittingActivationCodeValue";
            this.colGrittingActivationCodeValue.OptionsColumn.AllowEdit = false;
            this.colGrittingActivationCodeValue.OptionsColumn.AllowFocus = false;
            this.colGrittingActivationCodeValue.OptionsColumn.ReadOnly = true;
            this.colGrittingActivationCodeValue.Width = 126;
            // 
            // MinimumPictureCountSpinEdit
            // 
            this.MinimumPictureCountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04038GCSiteGrittingContractEditBindingSource, "MinimumPictureCount", true));
            this.MinimumPictureCountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MinimumPictureCountSpinEdit.Location = new System.Drawing.Point(154, 343);
            this.MinimumPictureCountSpinEdit.MenuManager = this.barManager1;
            this.MinimumPictureCountSpinEdit.Name = "MinimumPictureCountSpinEdit";
            this.MinimumPictureCountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MinimumPictureCountSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.MinimumPictureCountSpinEdit.Properties.IsFloatValue = false;
            this.MinimumPictureCountSpinEdit.Properties.Mask.EditMask = "N00";
            this.MinimumPictureCountSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.MinimumPictureCountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MinimumPictureCountSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.MinimumPictureCountSpinEdit.Size = new System.Drawing.Size(79, 20);
            this.MinimumPictureCountSpinEdit.StyleController = this.dataLayoutControl1;
            this.MinimumPictureCountSpinEdit.TabIndex = 67;
            // 
            // ItemForSiteGrittingContractID
            // 
            this.ItemForSiteGrittingContractID.Control = this.SiteGrittingContractIDSpinEdit;
            this.ItemForSiteGrittingContractID.CustomizationFormText = "Site Gritting Contract ID:";
            this.ItemForSiteGrittingContractID.Location = new System.Drawing.Point(0, 100);
            this.ItemForSiteGrittingContractID.Name = "ItemForSiteGrittingContractID";
            this.ItemForSiteGrittingContractID.Size = new System.Drawing.Size(608, 24);
            this.ItemForSiteGrittingContractID.Text = "Site Gritting Contract ID:";
            this.ItemForSiteGrittingContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(702, 1251);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroup4,
            this.emptySpaceItem2,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.ItemForSiteID,
            this.emptySpaceItem3,
            this.ItemForContractManagerID,
            this.ItemForForecastingTypeID,
            this.ItemForGrittingActivationCodeID,
            this.ItemForMinimumTemperature,
            this.ItemForBandingStart,
            this.ItemForBandingEnd,
            this.ItemForRedOverridesBanding,
            this.ItemForIsFloatingSite,
            this.ItemForGrittingCompletionEmail,
            this.ItemForGrittingCompletionEmailLinkedPictures,
            this.emptySpaceItem21,
            this.emptySpaceItem22,
            this.emptySpaceItem23,
            this.emptySpaceItem26,
            this.emptySpaceItem27,
            this.ItemForAnnual_Contract,
            this.ItemForMinimumPictureCount,
            this.ItemForActive,
            this.ItemForPrioritySite});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(682, 1231);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 1221);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(682, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 367);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(682, 854);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(658, 808);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup7,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartDate,
            this.ItemForArea,
            this.layoutControlGroup6,
            this.ItemForDefaultGritAmount,
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.ItemForProactive,
            this.ItemForReactive,
            this.emptySpaceItem10,
            this.ItemForAccessRestrictions,
            this.emptySpaceItem13,
            this.ItemForSiteOpenTime1,
            this.ItemForSiteClosedTime1,
            this.ItemForSiteOpenTime2,
            this.ItemForSiteClosedTime2,
            this.emptySpaceItem28,
            this.ItemForEndDate,
            this.ItemForGrittingTimeOnSite,
            this.layoutControlGroup8,
            this.ItemForSeasonPeriodID});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(634, 760);
            this.layoutControlGroup5.Text = "Details";
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.StartDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForStartDate.MaxSize = new System.Drawing.Size(0, 24);
            this.ItemForStartDate.MinSize = new System.Drawing.Size(182, 24);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(317, 24);
            this.ItemForStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStartDate.Text = "Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForArea
            // 
            this.ItemForArea.Control = this.AreaSpinEdit;
            this.ItemForArea.CustomizationFormText = "Area:";
            this.ItemForArea.Location = new System.Drawing.Point(0, 177);
            this.ItemForArea.Name = "ItemForArea";
            this.ItemForArea.Size = new System.Drawing.Size(318, 24);
            this.ItemForArea.Text = "Area:";
            this.ItemForArea.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Gritting Pattern";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGritOnMonday,
            this.ItemForGritOnTuesday,
            this.ItemForGritOnWednesday,
            this.ItemForGritOnThursday,
            this.ItemForGritOnFriday,
            this.ItemForGritOnSaturday,
            this.ItemForGritOnSunday,
            this.emptySpaceItem14,
            this.emptySpaceItem15,
            this.emptySpaceItem16,
            this.emptySpaceItem17,
            this.emptySpaceItem18,
            this.emptySpaceItem19,
            this.emptySpaceItem20});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 553);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(634, 207);
            this.layoutControlGroup6.Text = "Gritting Pattern";
            // 
            // ItemForGritOnMonday
            // 
            this.ItemForGritOnMonday.Control = this.GritOnMondayCheckEdit;
            this.ItemForGritOnMonday.CustomizationFormText = "Monday Grit:";
            this.ItemForGritOnMonday.Location = new System.Drawing.Point(0, 0);
            this.ItemForGritOnMonday.Name = "ItemForGritOnMonday";
            this.ItemForGritOnMonday.Size = new System.Drawing.Size(304, 23);
            this.ItemForGritOnMonday.Text = "Monday Grit:";
            this.ItemForGritOnMonday.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGritOnTuesday
            // 
            this.ItemForGritOnTuesday.Control = this.GritOnTuesdayCheckEdit;
            this.ItemForGritOnTuesday.CustomizationFormText = "Tuesday Grit:";
            this.ItemForGritOnTuesday.Location = new System.Drawing.Point(0, 23);
            this.ItemForGritOnTuesday.Name = "ItemForGritOnTuesday";
            this.ItemForGritOnTuesday.Size = new System.Drawing.Size(304, 23);
            this.ItemForGritOnTuesday.Text = "Tuesday Grit:";
            this.ItemForGritOnTuesday.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGritOnWednesday
            // 
            this.ItemForGritOnWednesday.Control = this.GritOnWednesdayCheckEdit;
            this.ItemForGritOnWednesday.CustomizationFormText = "Wednesday Grit:";
            this.ItemForGritOnWednesday.Location = new System.Drawing.Point(0, 46);
            this.ItemForGritOnWednesday.Name = "ItemForGritOnWednesday";
            this.ItemForGritOnWednesday.Size = new System.Drawing.Size(304, 23);
            this.ItemForGritOnWednesday.Text = "Wednesday Grit:";
            this.ItemForGritOnWednesday.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGritOnThursday
            // 
            this.ItemForGritOnThursday.Control = this.GritOnThursdayCheckEdit;
            this.ItemForGritOnThursday.CustomizationFormText = "Thursday Grit:";
            this.ItemForGritOnThursday.Location = new System.Drawing.Point(0, 69);
            this.ItemForGritOnThursday.Name = "ItemForGritOnThursday";
            this.ItemForGritOnThursday.Size = new System.Drawing.Size(304, 23);
            this.ItemForGritOnThursday.Text = "Thursday Grit:";
            this.ItemForGritOnThursday.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGritOnFriday
            // 
            this.ItemForGritOnFriday.Control = this.GritOnFridayCheckEdit;
            this.ItemForGritOnFriday.CustomizationFormText = "Friday Grit:";
            this.ItemForGritOnFriday.Location = new System.Drawing.Point(0, 92);
            this.ItemForGritOnFriday.Name = "ItemForGritOnFriday";
            this.ItemForGritOnFriday.Size = new System.Drawing.Size(304, 23);
            this.ItemForGritOnFriday.Text = "Friday Grit:";
            this.ItemForGritOnFriday.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGritOnSaturday
            // 
            this.ItemForGritOnSaturday.Control = this.GritOnSaturdayCheckEdit;
            this.ItemForGritOnSaturday.CustomizationFormText = "Saturday Grit:";
            this.ItemForGritOnSaturday.Location = new System.Drawing.Point(0, 115);
            this.ItemForGritOnSaturday.Name = "ItemForGritOnSaturday";
            this.ItemForGritOnSaturday.Size = new System.Drawing.Size(304, 23);
            this.ItemForGritOnSaturday.Text = "Saturday Grit:";
            this.ItemForGritOnSaturday.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGritOnSunday
            // 
            this.ItemForGritOnSunday.Control = this.GritOnSundayCheckEdit;
            this.ItemForGritOnSunday.CustomizationFormText = "Sunday Grit:";
            this.ItemForGritOnSunday.Location = new System.Drawing.Point(0, 138);
            this.ItemForGritOnSunday.Name = "ItemForGritOnSunday";
            this.ItemForGritOnSunday.Size = new System.Drawing.Size(304, 23);
            this.ItemForGritOnSunday.Text = "Sunday Grit:";
            this.ItemForGritOnSunday.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(304, 0);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(306, 23);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(304, 23);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(306, 23);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(304, 46);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(306, 23);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(304, 69);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(306, 23);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(304, 92);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(306, 23);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(304, 115);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(306, 23);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(304, 138);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(306, 23);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDefaultGritAmount
            // 
            this.ItemForDefaultGritAmount.Control = this.DefaultGritAmountSpinEdit;
            this.ItemForDefaultGritAmount.CustomizationFormText = "Default Grit Amount:";
            this.ItemForDefaultGritAmount.Location = new System.Drawing.Point(318, 177);
            this.ItemForDefaultGritAmount.Name = "ItemForDefaultGritAmount";
            this.ItemForDefaultGritAmount.Size = new System.Drawing.Size(316, 24);
            this.ItemForDefaultGritAmount.Text = "Default Grit Amount:";
            this.ItemForDefaultGritAmount.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 225);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(634, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 543);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(634, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForProactive
            // 
            this.ItemForProactive.Control = this.ProactiveCheckEdit;
            this.ItemForProactive.CustomizationFormText = "Proactive:";
            this.ItemForProactive.Location = new System.Drawing.Point(0, 201);
            this.ItemForProactive.MaxSize = new System.Drawing.Size(0, 24);
            this.ItemForProactive.MinSize = new System.Drawing.Size(209, 24);
            this.ItemForProactive.Name = "ItemForProactive";
            this.ItemForProactive.Size = new System.Drawing.Size(318, 24);
            this.ItemForProactive.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForProactive.Text = "Proactive:";
            this.ItemForProactive.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(318, 201);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(316, 24);
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(634, 10);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAccessRestrictions
            // 
            this.ItemForAccessRestrictions.Control = this.AccessRestrictionsCheckEdit;
            this.ItemForAccessRestrictions.Location = new System.Drawing.Point(0, 144);
            this.ItemForAccessRestrictions.Name = "ItemForAccessRestrictions";
            this.ItemForAccessRestrictions.Size = new System.Drawing.Size(316, 23);
            this.ItemForAccessRestrictions.Text = "Access Restrictions:";
            this.ItemForAccessRestrictions.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(316, 144);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(318, 23);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSiteOpenTime1
            // 
            this.ItemForSiteOpenTime1.Control = this.SiteOpenTime1TimeSpanEdit;
            this.ItemForSiteOpenTime1.Location = new System.Drawing.Point(0, 96);
            this.ItemForSiteOpenTime1.Name = "ItemForSiteOpenTime1";
            this.ItemForSiteOpenTime1.Size = new System.Drawing.Size(317, 24);
            this.ItemForSiteOpenTime1.Text = "Site Open Time 1:";
            this.ItemForSiteOpenTime1.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForSiteClosedTime1
            // 
            this.ItemForSiteClosedTime1.Control = this.SiteClosedTime1TimeSpanEdit;
            this.ItemForSiteClosedTime1.Location = new System.Drawing.Point(317, 96);
            this.ItemForSiteClosedTime1.Name = "ItemForSiteClosedTime1";
            this.ItemForSiteClosedTime1.Size = new System.Drawing.Size(317, 24);
            this.ItemForSiteClosedTime1.Text = "Site Closed Time 1:";
            this.ItemForSiteClosedTime1.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForSiteOpenTime2
            // 
            this.ItemForSiteOpenTime2.Control = this.SiteOpenTime2TimeSpanEdit;
            this.ItemForSiteOpenTime2.Location = new System.Drawing.Point(0, 120);
            this.ItemForSiteOpenTime2.Name = "ItemForSiteOpenTime2";
            this.ItemForSiteOpenTime2.Size = new System.Drawing.Size(317, 24);
            this.ItemForSiteOpenTime2.Text = "Site Open Time 2:";
            this.ItemForSiteOpenTime2.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForSiteClosedTime2
            // 
            this.ItemForSiteClosedTime2.Control = this.SiteClosedTime2TimeSpanEdit;
            this.ItemForSiteClosedTime2.Location = new System.Drawing.Point(317, 120);
            this.ItemForSiteClosedTime2.Name = "ItemForSiteClosedTime2";
            this.ItemForSiteClosedTime2.Size = new System.Drawing.Size(317, 24);
            this.ItemForSiteClosedTime2.Text = "Site Closed Time 2:";
            this.ItemForSiteClosedTime2.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem28
            // 
            this.emptySpaceItem28.AllowHotTrack = false;
            this.emptySpaceItem28.Location = new System.Drawing.Point(317, 0);
            this.emptySpaceItem28.Name = "emptySpaceItem28";
            this.emptySpaceItem28.Size = new System.Drawing.Size(317, 96);
            this.emptySpaceItem28.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.EndDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(317, 24);
            this.ItemForEndDate.Text = "End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGrittingTimeOnSite
            // 
            this.ItemForGrittingTimeOnSite.Control = this.GrittingTimeOnSiteSpinEdit;
            this.ItemForGrittingTimeOnSite.CustomizationFormText = "Gritting Time On Site:";
            this.ItemForGrittingTimeOnSite.Location = new System.Drawing.Point(0, 72);
            this.ItemForGrittingTimeOnSite.Name = "ItemForGrittingTimeOnSite";
            this.ItemForGrittingTimeOnSite.Size = new System.Drawing.Size(317, 24);
            this.ItemForGrittingTimeOnSite.Text = "Gritting Time On Site:";
            this.ItemForGrittingTimeOnSite.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForInvoiceClient,
            this.ItemForDontInvoiceClientReason,
            this.ItemForTeamProactiveRate,
            this.ItemForTeamReactiveRate,
            this.ItemForDefaultNoAccessRate,
            this.emptySpaceItem9,
            this.ItemForClientProactivePrice,
            this.ItemForClientReactivePrice,
            this.ItemForClientEveningRateModifier,
            this.emptySpaceItem29,
            this.ItemClientChargedForSalt,
            this.ItemForClientSaltPrice,
            this.ItemForClientSaltVatRate,
            this.ItemForAnnualCost,
            this.emptySpaceItem12,
            this.emptySpaceItem11});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 235);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(634, 308);
            this.layoutControlGroup8.Text = "Financial";
            // 
            // ItemForInvoiceClient
            // 
            this.ItemForInvoiceClient.Control = this.InvoiceClientCheckEdit;
            this.ItemForInvoiceClient.Location = new System.Drawing.Point(0, 0);
            this.ItemForInvoiceClient.Name = "ItemForInvoiceClient";
            this.ItemForInvoiceClient.Size = new System.Drawing.Size(305, 23);
            this.ItemForInvoiceClient.Text = "Invoice Client:";
            this.ItemForInvoiceClient.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForDontInvoiceClientReason
            // 
            this.ItemForDontInvoiceClientReason.Control = this.DontInvoiceClientReasonMemoEdit;
            this.ItemForDontInvoiceClientReason.Location = new System.Drawing.Point(0, 23);
            this.ItemForDontInvoiceClientReason.MaxSize = new System.Drawing.Size(0, 72);
            this.ItemForDontInvoiceClientReason.MinSize = new System.Drawing.Size(156, 72);
            this.ItemForDontInvoiceClientReason.Name = "ItemForDontInvoiceClientReason";
            this.ItemForDontInvoiceClientReason.Size = new System.Drawing.Size(610, 72);
            this.ItemForDontInvoiceClientReason.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDontInvoiceClientReason.Text = "Don\'t Invoice Site Reason:";
            this.ItemForDontInvoiceClientReason.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForTeamProactiveRate
            // 
            this.ItemForTeamProactiveRate.Control = this.TeamProactiveRateSpinEdit;
            this.ItemForTeamProactiveRate.CustomizationFormText = "Team Proactive Rate:";
            this.ItemForTeamProactiveRate.Location = new System.Drawing.Point(0, 95);
            this.ItemForTeamProactiveRate.Name = "ItemForTeamProactiveRate";
            this.ItemForTeamProactiveRate.Size = new System.Drawing.Size(305, 24);
            this.ItemForTeamProactiveRate.Text = "Team Proactive Rate:";
            this.ItemForTeamProactiveRate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForTeamReactiveRate
            // 
            this.ItemForTeamReactiveRate.Control = this.TeamReactiveRateSpinEdit;
            this.ItemForTeamReactiveRate.CustomizationFormText = "Team Reactive Rate:";
            this.ItemForTeamReactiveRate.Location = new System.Drawing.Point(305, 95);
            this.ItemForTeamReactiveRate.Name = "ItemForTeamReactiveRate";
            this.ItemForTeamReactiveRate.Size = new System.Drawing.Size(305, 24);
            this.ItemForTeamReactiveRate.Text = "Team Reactive Rate:";
            this.ItemForTeamReactiveRate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForDefaultNoAccessRate
            // 
            this.ItemForDefaultNoAccessRate.Control = this.DefaultNoAccessRateSpinEdit;
            this.ItemForDefaultNoAccessRate.CustomizationFormText = "Team No Access Rate:";
            this.ItemForDefaultNoAccessRate.Location = new System.Drawing.Point(0, 119);
            this.ItemForDefaultNoAccessRate.Name = "ItemForDefaultNoAccessRate";
            this.ItemForDefaultNoAccessRate.Size = new System.Drawing.Size(305, 24);
            this.ItemForDefaultNoAccessRate.Text = "Team No Access Rate:";
            this.ItemForDefaultNoAccessRate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(305, 119);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(305, 24);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientProactivePrice
            // 
            this.ItemForClientProactivePrice.Control = this.ClientProactivePriceSpinEdit;
            this.ItemForClientProactivePrice.CustomizationFormText = "Client Proactive Price:";
            this.ItemForClientProactivePrice.Location = new System.Drawing.Point(0, 143);
            this.ItemForClientProactivePrice.Name = "ItemForClientProactivePrice";
            this.ItemForClientProactivePrice.Size = new System.Drawing.Size(305, 24);
            this.ItemForClientProactivePrice.Text = "Client Proactive Price:";
            this.ItemForClientProactivePrice.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForClientReactivePrice
            // 
            this.ItemForClientReactivePrice.Control = this.ClientReactivePriceSpinEdit;
            this.ItemForClientReactivePrice.CustomizationFormText = "Client Reactive Price:";
            this.ItemForClientReactivePrice.Location = new System.Drawing.Point(305, 143);
            this.ItemForClientReactivePrice.Name = "ItemForClientReactivePrice";
            this.ItemForClientReactivePrice.Size = new System.Drawing.Size(305, 24);
            this.ItemForClientReactivePrice.Text = "Client Reactive Price:";
            this.ItemForClientReactivePrice.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForClientEveningRateModifier
            // 
            this.ItemForClientEveningRateModifier.Control = this.ClientEveningRateModifierSpinEdit;
            this.ItemForClientEveningRateModifier.CustomizationFormText = "Client Evening Modifier:";
            this.ItemForClientEveningRateModifier.Location = new System.Drawing.Point(0, 167);
            this.ItemForClientEveningRateModifier.Name = "ItemForClientEveningRateModifier";
            this.ItemForClientEveningRateModifier.Size = new System.Drawing.Size(305, 24);
            this.ItemForClientEveningRateModifier.Text = "Client Evening Rate Modifier:";
            this.ItemForClientEveningRateModifier.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem29
            // 
            this.emptySpaceItem29.AllowHotTrack = false;
            this.emptySpaceItem29.Location = new System.Drawing.Point(305, 0);
            this.emptySpaceItem29.Name = "emptySpaceItem29";
            this.emptySpaceItem29.Size = new System.Drawing.Size(305, 23);
            this.emptySpaceItem29.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemClientChargedForSalt
            // 
            this.ItemClientChargedForSalt.Control = this.ClientChargedForSaltCheckEdit;
            this.ItemClientChargedForSalt.CustomizationFormText = "Client Charged For Salt:";
            this.ItemClientChargedForSalt.Location = new System.Drawing.Point(0, 191);
            this.ItemClientChargedForSalt.Name = "ItemClientChargedForSalt";
            this.ItemClientChargedForSalt.Size = new System.Drawing.Size(305, 23);
            this.ItemClientChargedForSalt.Text = "Client Charged For Salt:";
            this.ItemClientChargedForSalt.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForClientSaltPrice
            // 
            this.ItemForClientSaltPrice.Control = this.ClientSaltPriceSpinEdit;
            this.ItemForClientSaltPrice.CustomizationFormText = "Client Salt Price:";
            this.ItemForClientSaltPrice.Location = new System.Drawing.Point(0, 214);
            this.ItemForClientSaltPrice.Name = "ItemForClientSaltPrice";
            this.ItemForClientSaltPrice.Size = new System.Drawing.Size(305, 24);
            this.ItemForClientSaltPrice.Text = "Client Salt Price:";
            this.ItemForClientSaltPrice.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForClientSaltVatRate
            // 
            this.ItemForClientSaltVatRate.Control = this.ClientSaltVatRateSpinEdit;
            this.ItemForClientSaltVatRate.CustomizationFormText = "Client Salt VAT Rate:";
            this.ItemForClientSaltVatRate.Location = new System.Drawing.Point(305, 214);
            this.ItemForClientSaltVatRate.Name = "ItemForClientSaltVatRate";
            this.ItemForClientSaltVatRate.Size = new System.Drawing.Size(305, 24);
            this.ItemForClientSaltVatRate.Text = "Client Salt VAT Rate:";
            this.ItemForClientSaltVatRate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForAnnualCost
            // 
            this.ItemForAnnualCost.Control = this.AnnualCostSpinEdit;
            this.ItemForAnnualCost.Location = new System.Drawing.Point(0, 238);
            this.ItemForAnnualCost.Name = "ItemForAnnualCost";
            this.ItemForAnnualCost.Size = new System.Drawing.Size(305, 24);
            this.ItemForAnnualCost.Text = "Annual Cost:";
            this.ItemForAnnualCost.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(305, 238);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(305, 24);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(305, 167);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(305, 47);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSeasonPeriodID
            // 
            this.ItemForSeasonPeriodID.Control = this.SeasonPeriodIDGridLookUpEdit;
            this.ItemForSeasonPeriodID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSeasonPeriodID.Name = "ItemForSeasonPeriodID";
            this.ItemForSeasonPeriodID.Size = new System.Drawing.Size(317, 24);
            this.ItemForSeasonPeriodID.Text = "Season Period:";
            this.ItemForSeasonPeriodID.TextSize = new System.Drawing.Size(139, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Site Gritting Notes";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteGrittingNotes});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(634, 760);
            this.layoutControlGroup7.Text = "Site Gritting Notes";
            // 
            // ItemForSiteGrittingNotes
            // 
            this.ItemForSiteGrittingNotes.Control = this.SiteGrittingNotesMemoEdit;
            this.ItemForSiteGrittingNotes.CustomizationFormText = "Site Gritting Notes:";
            this.ItemForSiteGrittingNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForSiteGrittingNotes.Name = "ItemForSiteGrittingNotes";
            this.ItemForSiteGrittingNotes.Size = new System.Drawing.Size(634, 760);
            this.ItemForSiteGrittingNotes.Text = "Site Gritting Notes:";
            this.ItemForSiteGrittingNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForSiteGrittingNotes.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(634, 760);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(634, 760);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 355);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(682, 12);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(343, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(339, 24);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(143, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDGridLookUpEdit;
            this.ItemForSiteID.CustomizationFormText = "Site:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(682, 26);
            this.ItemForSiteID.Text = "Site:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(143, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(143, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(143, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForContractManagerID
            // 
            this.ItemForContractManagerID.Control = this.ContractManagerIDGridLookUpEdit;
            this.ItemForContractManagerID.CustomizationFormText = "Contract Manager:";
            this.ItemForContractManagerID.Location = new System.Drawing.Point(0, 50);
            this.ItemForContractManagerID.Name = "ItemForContractManagerID";
            this.ItemForContractManagerID.Size = new System.Drawing.Size(682, 24);
            this.ItemForContractManagerID.Text = "Contract Manager:";
            this.ItemForContractManagerID.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForForecastingTypeID
            // 
            this.ItemForForecastingTypeID.Control = this.ForecastingTypeIDGridLookUpEdit;
            this.ItemForForecastingTypeID.CustomizationFormText = "Forecasting Type:";
            this.ItemForForecastingTypeID.Location = new System.Drawing.Point(0, 120);
            this.ItemForForecastingTypeID.Name = "ItemForForecastingTypeID";
            this.ItemForForecastingTypeID.Size = new System.Drawing.Size(682, 24);
            this.ItemForForecastingTypeID.Text = "Forecasting Type:";
            this.ItemForForecastingTypeID.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGrittingActivationCodeID
            // 
            this.ItemForGrittingActivationCodeID.Control = this.GrittingActivationCodeIDGridLookUpEdit;
            this.ItemForGrittingActivationCodeID.CustomizationFormText = "Weather Activation Point:";
            this.ItemForGrittingActivationCodeID.Location = new System.Drawing.Point(0, 144);
            this.ItemForGrittingActivationCodeID.Name = "ItemForGrittingActivationCodeID";
            this.ItemForGrittingActivationCodeID.Size = new System.Drawing.Size(682, 24);
            this.ItemForGrittingActivationCodeID.Text = "Weather Activation Point:";
            this.ItemForGrittingActivationCodeID.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForMinimumTemperature
            // 
            this.ItemForMinimumTemperature.Control = this.MinimumTemperatureSpinEdit;
            this.ItemForMinimumTemperature.CustomizationFormText = "Minimum Temperature:";
            this.ItemForMinimumTemperature.Location = new System.Drawing.Point(0, 168);
            this.ItemForMinimumTemperature.Name = "ItemForMinimumTemperature";
            this.ItemForMinimumTemperature.Size = new System.Drawing.Size(360, 24);
            this.ItemForMinimumTemperature.Text = "Minimum Temperature:";
            this.ItemForMinimumTemperature.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForBandingStart
            // 
            this.ItemForBandingStart.Control = this.BandingStartGridLookUpEdit;
            this.ItemForBandingStart.CustomizationFormText = "Band Start:";
            this.ItemForBandingStart.Location = new System.Drawing.Point(0, 192);
            this.ItemForBandingStart.Name = "ItemForBandingStart";
            this.ItemForBandingStart.Size = new System.Drawing.Size(360, 24);
            this.ItemForBandingStart.Text = "Band Start:";
            this.ItemForBandingStart.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForBandingEnd
            // 
            this.ItemForBandingEnd.Control = this.BandingEndGridLookUpEdit;
            this.ItemForBandingEnd.CustomizationFormText = "Band End:";
            this.ItemForBandingEnd.Location = new System.Drawing.Point(360, 192);
            this.ItemForBandingEnd.Name = "ItemForBandingEnd";
            this.ItemForBandingEnd.Size = new System.Drawing.Size(322, 24);
            this.ItemForBandingEnd.Text = "Band End:";
            this.ItemForBandingEnd.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForRedOverridesBanding
            // 
            this.ItemForRedOverridesBanding.Control = this.RedOverridesBandingCheckEdit;
            this.ItemForRedOverridesBanding.CustomizationFormText = "Red Overrides Bandings:";
            this.ItemForRedOverridesBanding.Location = new System.Drawing.Point(0, 216);
            this.ItemForRedOverridesBanding.Name = "ItemForRedOverridesBanding";
            this.ItemForRedOverridesBanding.Size = new System.Drawing.Size(312, 23);
            this.ItemForRedOverridesBanding.Text = "Red Overrides Bandings:";
            this.ItemForRedOverridesBanding.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForIsFloatingSite
            // 
            this.ItemForIsFloatingSite.Control = this.IsFloatingSiteCheckEdit;
            this.ItemForIsFloatingSite.CustomizationFormText = "Is Floating Site:";
            this.ItemForIsFloatingSite.Location = new System.Drawing.Point(0, 97);
            this.ItemForIsFloatingSite.Name = "ItemForIsFloatingSite";
            this.ItemForIsFloatingSite.Size = new System.Drawing.Size(329, 23);
            this.ItemForIsFloatingSite.Text = "Is Floating Site:";
            this.ItemForIsFloatingSite.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGrittingCompletionEmail
            // 
            this.ItemForGrittingCompletionEmail.Control = this.GrittingCompletionEmailCheckEdit;
            this.ItemForGrittingCompletionEmail.CustomizationFormText = "Gritting Completion Email:";
            this.ItemForGrittingCompletionEmail.Location = new System.Drawing.Point(0, 285);
            this.ItemForGrittingCompletionEmail.Name = "ItemForGrittingCompletionEmail";
            this.ItemForGrittingCompletionEmail.Size = new System.Drawing.Size(433, 23);
            this.ItemForGrittingCompletionEmail.Text = "Gritting Completion Email:";
            this.ItemForGrittingCompletionEmail.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForGrittingCompletionEmailLinkedPictures
            // 
            this.ItemForGrittingCompletionEmailLinkedPictures.Control = this.GrittingCompletionEmailLinkedPicturesCheckEdit;
            this.ItemForGrittingCompletionEmailLinkedPictures.CustomizationFormText = "Email Linked Pictures:";
            this.ItemForGrittingCompletionEmailLinkedPictures.Location = new System.Drawing.Point(0, 308);
            this.ItemForGrittingCompletionEmailLinkedPictures.MaxSize = new System.Drawing.Size(225, 23);
            this.ItemForGrittingCompletionEmailLinkedPictures.MinSize = new System.Drawing.Size(225, 23);
            this.ItemForGrittingCompletionEmailLinkedPictures.Name = "ItemForGrittingCompletionEmailLinkedPictures";
            this.ItemForGrittingCompletionEmailLinkedPictures.Size = new System.Drawing.Size(225, 23);
            this.ItemForGrittingCompletionEmailLinkedPictures.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGrittingCompletionEmailLinkedPictures.Text = "Email Linked Pictures:";
            this.ItemForGrittingCompletionEmailLinkedPictures.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.Location = new System.Drawing.Point(360, 168);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(322, 24);
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.Location = new System.Drawing.Point(329, 74);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(353, 46);
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.Location = new System.Drawing.Point(312, 216);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(370, 69);
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem26
            // 
            this.emptySpaceItem26.AllowHotTrack = false;
            this.emptySpaceItem26.Location = new System.Drawing.Point(225, 308);
            this.emptySpaceItem26.Name = "emptySpaceItem26";
            this.emptySpaceItem26.Size = new System.Drawing.Size(457, 47);
            this.emptySpaceItem26.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.Location = new System.Drawing.Point(433, 285);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(249, 23);
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAnnual_Contract
            // 
            this.ItemForAnnual_Contract.Control = this.Annual_ContractCheckEdit;
            this.ItemForAnnual_Contract.Location = new System.Drawing.Point(0, 74);
            this.ItemForAnnual_Contract.Name = "ItemForAnnual_Contract";
            this.ItemForAnnual_Contract.Size = new System.Drawing.Size(329, 23);
            this.ItemForAnnual_Contract.Text = "Annual Contract:";
            this.ItemForAnnual_Contract.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForMinimumPictureCount
            // 
            this.ItemForMinimumPictureCount.Control = this.MinimumPictureCountSpinEdit;
            this.ItemForMinimumPictureCount.Location = new System.Drawing.Point(0, 331);
            this.ItemForMinimumPictureCount.Name = "ItemForMinimumPictureCount";
            this.ItemForMinimumPictureCount.Size = new System.Drawing.Size(225, 24);
            this.ItemForMinimumPictureCount.Text = "Min Pictures from App:";
            this.ItemForMinimumPictureCount.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForActive
            // 
            this.ItemForActive.Control = this.ActiveCheckEdit;
            this.ItemForActive.CustomizationFormText = "Active Contract:";
            this.ItemForActive.Location = new System.Drawing.Point(0, 239);
            this.ItemForActive.Name = "ItemForActive";
            this.ItemForActive.Size = new System.Drawing.Size(312, 23);
            this.ItemForActive.Text = "Active Contract:";
            this.ItemForActive.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForPrioritySite
            // 
            this.ItemForPrioritySite.Control = this.PrioritySiteCheckEdit;
            this.ItemForPrioritySite.CustomizationFormText = "Priority Site:";
            this.ItemForPrioritySite.Location = new System.Drawing.Point(0, 262);
            this.ItemForPrioritySite.Name = "ItemForPrioritySite";
            this.ItemForPrioritySite.Size = new System.Drawing.Size(312, 23);
            this.ItemForPrioritySite.Text = "Priority Site:";
            this.ItemForPrioritySite.TextSize = new System.Drawing.Size(139, 13);
            // 
            // sp04038_GC_Site_Gritting_Contract_EditTableAdapter
            // 
            this.sp04038_GC_Site_Gritting_Contract_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp00226_Staff_List_With_BlankTableAdapter
            // 
            this.sp00226_Staff_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ReactiveCheckEdit;
            this.layoutControlItem2.CustomizationFormText = "Reactive:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem2.Name = "ItemForReactive";
            this.layoutControlItem2.Size = new System.Drawing.Size(280, 24);
            this.layoutControlItem2.Text = "Reactive:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(124, 13);
            // 
            // sp04051_GC_Gritting_Forecast_Type_With_BlankTableAdapter
            // 
            this.sp04051_GC_Gritting_Forecast_Type_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04052_GC_Gritting_Activation_Codes_With_BlankTableAdapter
            // 
            this.sp04052_GC_Gritting_Activation_Codes_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_SitesTableAdapter
            // 
            this.sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_SitesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04343_GC_Gritting_Forecast_BandingsTableAdapter
            // 
            this.sp04343_GC_Gritting_Forecast_BandingsTableAdapter.ClearBeforeFill = true;
            // 
            // sp04373WMSeasonPeriodsWithBlankBindingSource
            // 
            this.sp04373WMSeasonPeriodsWithBlankBindingSource.DataMember = "sp04373_WM_Season_Periods_With_Blank";
            this.sp04373WMSeasonPeriodsWithBlankBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // sp04373_WM_Season_Periods_With_BlankTableAdapter
            // 
            this.sp04373_WM_Season_Periods_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // colID1
            // 
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Season Period";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 171;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Width = 87;
            // 
            // frm_GC_Site_Gritting_contract_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(719, 685);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_GC_Site_Gritting_contract_Edit";
            this.Text = "Edit Site Gritting Contract";
            this.Activated += new System.EventHandler(this.frm_GC_Site_Gritting_contract_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_GC_Site_Gritting_contract_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_GC_Site_Gritting_contract_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SeasonPeriodIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04038GCSiteGrittingContractEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04052GCGrittingActivationCodesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DontInvoiceClientReasonMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Annual_ContractCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteClosedTime2TimeSpanEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteOpenTime2TimeSpanEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteClosedTime1TimeSpanEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteOpenTime1TimeSpanEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessRestrictionsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnnualCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingCompletionEmailLinkedPicturesCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingTimeOnSiteSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingCompletionEmailCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFloatingSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandingEndGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04343GCGrittingForecastBandingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BandingStartGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedOverridesBandingCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultNoAccessRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrioritySiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteGrittingNotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientEveningRateModifierSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientSaltVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamReactiveRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamProactiveRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultGritAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumTemperatureSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnSundayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnSaturdayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnFridayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnThursdayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnWednesdayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnTuesdayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GritOnMondayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientSaltPriceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientChargedForSaltCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientReactivePriceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientProactivePriceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ForecastingTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04051GCGrittingForecastTypeWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractManagerIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04058GCSitesWithBlankDDLBJustGrittingSitesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteGrittingContractIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingActivationCodeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimumPictureCountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteGrittingContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnMonday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnTuesday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnWednesday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnThursday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnFriday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnSaturday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritOnSunday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultGritAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessRestrictions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteOpenTime1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteClosedTime1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteOpenTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteClosedTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingTimeOnSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDontInvoiceClientReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamProactiveRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamReactiveRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultNoAccessRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientProactivePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientReactivePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientEveningRateModifier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemClientChargedForSalt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientSaltPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientSaltVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAnnualCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSeasonPeriodID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteGrittingNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractManagerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForForecastingTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingActivationCodeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumTemperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBandingStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBandingEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRedOverridesBanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsFloatingSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingCompletionEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingCompletionEmailLinkedPictures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAnnual_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimumPictureCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrioritySite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04373WMSeasonPeriodsWithBlankBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SpinEdit SiteGrittingContractIDSpinEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteGrittingContractID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.GridLookUpEdit SiteIDGridLookUpEdit;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private DevExpress.XtraEditors.CheckEdit ActiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActive;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private System.Windows.Forms.BindingSource sp04038GCSiteGrittingContractEditBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04038_GC_Site_Gritting_Contract_EditTableAdapter sp04038_GC_Site_Gritting_Contract_EditTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit ContractManagerIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractManagerID;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp00226StaffListWithBlankBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter sp00226_Staff_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraEditors.GridLookUpEdit ForecastingTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForForecastingTypeID;
        private DevExpress.XtraEditors.DateEdit EndDateDateEdit;
        private DevExpress.XtraEditors.DateEdit StartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.SpinEdit AreaSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForArea;
        private DevExpress.XtraEditors.CheckEdit ProactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProactive;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingActivationCodeID;
        private DevExpress.XtraEditors.SpinEdit ClientReactivePriceSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ClientProactivePriceSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientProactivePrice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientReactivePrice;
        private DevExpress.XtraEditors.CheckEdit ClientChargedForSaltCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemClientChargedForSalt;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SpinEdit ClientSaltPriceSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientSaltPrice;
        private DevExpress.XtraEditors.CheckEdit GritOnSundayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit GritOnSaturdayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit GritOnFridayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit GritOnThursdayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit GritOnWednesdayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit GritOnTuesdayCheckEdit;
        private DevExpress.XtraEditors.CheckEdit GritOnMondayCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritOnMonday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritOnTuesday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritOnWednesday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritOnThursday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritOnFriday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritOnSaturday;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritOnSunday;
        private System.Windows.Forms.BindingSource sp04051GCGrittingForecastTypeWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04051_GC_Gritting_Forecast_Type_With_BlankTableAdapter sp04051_GC_Gritting_Forecast_Type_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraEditors.GridLookUpEdit GrittingActivationCodeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp04052GCGrittingActivationCodesWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04052_GC_Gritting_Activation_Codes_With_BlankTableAdapter sp04052_GC_Gritting_Activation_Codes_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colForecastingTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingActivationCodeValue;
        private DevExpress.XtraEditors.SpinEdit MinimumTemperatureSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimumTemperature;
        private DevExpress.XtraEditors.SpinEdit DefaultGritAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultGritAmount;
        private System.Windows.Forms.BindingSource sp04058GCSitesWithBlankDDLBJustGrittingSitesBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_SitesTableAdapter sp04058_GC_Sites_With_Blank_DDLB_Just_Gritting_SitesTableAdapter;
        private DevExpress.XtraEditors.SpinEdit TeamReactiveRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TeamProactiveRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamReactiveRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamProactiveRate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.SpinEdit ClientSaltVatRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientSaltVatRate;
        private DevExpress.XtraEditors.SpinEdit ClientEveningRateModifierSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientEveningRateModifier;
        private DevExpress.XtraEditors.MemoEdit SiteGrittingNotesMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteGrittingNotes;
        private DevExpress.XtraEditors.CheckEdit PrioritySiteCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrioritySite;
        private DevExpress.XtraEditors.SpinEdit DefaultNoAccessRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultNoAccessRate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraEditors.CheckEdit RedOverridesBandingCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRedOverridesBanding;
        private DevExpress.XtraEditors.GridLookUpEdit BandingEndGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit2View;
        private DevExpress.XtraEditors.GridLookUpEdit BandingStartGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBandingStart;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBandingEnd;
        private DataSet_GC_Core dataSet_GC_Core;
        private System.Windows.Forms.BindingSource sp04343GCGrittingForecastBandingsBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04343_GC_Gritting_Forecast_BandingsTableAdapter sp04343_GC_Gritting_Forecast_BandingsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.CheckEdit GrittingCompletionEmailCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsFloatingSiteCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsFloatingSite;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingCompletionEmail;
        private DevExpress.XtraEditors.SpinEdit GrittingTimeOnSiteSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingTimeOnSite;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraEditors.CheckEdit GrittingCompletionEmailLinkedPicturesCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingCompletionEmailLinkedPictures;
        private DevExpress.XtraEditors.SpinEdit AnnualCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAnnualCost;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.CheckEdit AccessRestrictionsCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccessRestrictions;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem26;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraEditors.TimeSpanEdit SiteClosedTime2TimeSpanEdit;
        private DevExpress.XtraEditors.TimeSpanEdit SiteOpenTime2TimeSpanEdit;
        private DevExpress.XtraEditors.TimeSpanEdit SiteClosedTime1TimeSpanEdit;
        private DevExpress.XtraEditors.TimeSpanEdit SiteOpenTime1TimeSpanEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteOpenTime1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteClosedTime1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteOpenTime2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteClosedTime2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem28;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit Annual_ContractCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAnnual_Contract;
        private DevExpress.XtraEditors.MemoEdit DontInvoiceClientReasonMemoEdit;
        private DevExpress.XtraEditors.CheckEdit InvoiceClientCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvoiceClient;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDontInvoiceClientReason;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem29;
        private DevExpress.XtraEditors.SpinEdit MinimumPictureCountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimumPictureCount;
        private DevExpress.XtraEditors.GridLookUpEdit SeasonPeriodIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSeasonPeriodID;
        private System.Windows.Forms.BindingSource sp04373WMSeasonPeriodsWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DataSet_GC_CoreTableAdapters.sp04373_WM_Season_Periods_With_BlankTableAdapter sp04373_WM_Season_Periods_With_BlankTableAdapter;
    }
}

using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;  // Mouse down Args for GridView Mouse Down //

namespace WoodPlan5
{
    public partial class frmSystemPeriods : frmBase
    {
        #region Instance Variables

        Settings set = Settings.Default;
        public int intViewedPeriodID = 0;
        public DateTime dtViewedStartDate;
        public DateTime dtViewedEndDate;
        public int intLivePeriodID = 0;
        public DateTime dtLiveStartDate;
        public DateTime dtLiveEndDate;
        GridHitInfo downHitInfo;
        public RefreshGridState RefreshGridViewState;  // Used by Grid View State Facilities //
        private int intTempViewedPeriod;
        private int intTempLivePeriod;

        #endregion

        public frmSystemPeriods()
        {
            InitializeComponent();
        }

        private void frmSystemPeriods_Load(object sender, EventArgs e)
        {
            intTempLivePeriod = GlobalSettings.LivePeriodID;
            intTempViewedPeriod = GlobalSettings.ViewedPeriodID;

            Set_Grid_Highlighter_Transparent(this.Controls);

            GridView view = (GridView)gridControl1.MainView;
            RefreshGridViewState = new RefreshGridState(view, "intPeriodID");
            sp00097_date_periods_listTableAdapter.Connection.ConnectionString = GlobalSettings.ConnectionString; ;
            LoadData(view, false);
       
        }

        private void LoadData(GridView view, Boolean boolLoadGridState)
        {
            view.BeginUpdate();
            sp00097_date_periods_listTableAdapter.Fill(woodPlanDataSet.sp00097_date_periods_list, GlobalSettings.LivePeriodID, GlobalSettings.ViewedPeriodID, 1);
            if (boolLoadGridState) RefreshGridViewState.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            AddRow();
        }

        private void AddRow()
        {
            GridView view = gridView1;
            view.BeginUpdate();

            view.AddNewRow();
            view.UpdateCurrentRow();
            DataRow dr = view.GetDataRow(view.FocusedRowHandle);
            dr.SetColumnError("strDesc", String.Format("{0} requires a value.", view.Columns["strDesc"].Caption));
            dr.SetColumnError("dtFromDate", String.Format("{0} requires a value.", view.Columns["dtFromDate"].Caption));
            dr.SetColumnError("dtToDate", String.Format("{0} requires a value.", view.Columns["dtToDate"].Caption));
            view.EndUpdate();
            view.RefreshData();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Date Period to be edited before proceeding then try again.", "Edit Date Period", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strRecordIDs = String.Format("{0},", Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "intPeriodID")));

            RefreshGridViewState.SaveViewInfo();  // Store Grid View State //
            /*
            frmUserScreenSettingsEditLayout fChildForm = new frmUserScreenSettingsEditLayout();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = strRecordIDs;
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = "frmUserScreenSettingsLoad";
            fChildForm.intRecordCount = 1;
            fChildForm.boolAllowEdit = true;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.frmToProcess = this.frmToProcess;
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                LoadData(view, false);  // Changes made on child screen so reload data //
            }*/
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            DeleteRow();
        }

        private void DeleteRow()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the period to be marked for deletion before proceeding then try again.", "Delete Date Period", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "intPeriodID")) == intTempLivePeriod)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Date Period selected is marked as the System Live Period!\n\nYou cannot delete the Live Period. If you need to delete this period, first set a different period as the live period by right clicking the mouse on ta different period and select Set as Live Period then try again.", "Delete Date Period", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "intPeriodID")) == intTempLivePeriod)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Date Period selected is marked as your currently Viewed Period!\n\nYou cannot delete the Viewed Period. If you need to delete this period, first set a different period as the viewed period by right clicking the mouse on a different period and select Set as Viewed Period then try again.", "Delete Date Period", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = String.Format("You are about to mark Date Period [{0}] for deletion!\n\nAre you sure you wish to proceed?\n\nIf you proceed this date period will no longer be available once you click the OK button on the form to commit the changes.", view.GetRowCellValue(view.FocusedRowHandle, "strDesc"));
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete Screen Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                string strRecordIDs = String.Format("{0},", view.GetRowCellValue(view.FocusedRowHandle, "intPeriodID"));

                WoodPlanDataSetTableAdapters.QueriesTableAdapter RemoveRecord = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                RemoveRecord.ChangeConnectionString(GlobalSettings.ConnectionString);
                gridControl1.BeginUpdate();
                //RemoveRecord.sp00096_delete_user_screen_layout(strRecordIDs);  // Delete record from back end //
                // Row not physically removed at this point - only done on clicking Save //
                view.DeleteRow(view.FocusedRowHandle);  // Delete record from grid //
                gridControl1.EndUpdate();
                if (this.GlobalSettings.ShowConfirmations == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Date Period marked for deletion.\n\nNote: The date has only been marked for deletion. When you click the OK button on the form it will be physically deleted.", "Delete Date Period", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        #region gridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No System Periods Available");

        }

        private void gridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            GridView gvView = (GridView)sender;
            Bitmap bmpTest = new Bitmap(70, 91);

            if (e.Column.FieldName == "SystemPeriod" && e.IsGetData)
            {
                if (Convert.ToInt32(gvView.GetRowCellValue(e.ListSourceRowIndex, "intPeriodID")) == intTempLivePeriod)
                {
                    bmpTest = new Bitmap(imageList3.Images["system_live"], 16, 16);
                }
                else
                {
                    bmpTest = new Bitmap(16, 16);
                }
            }
            else if (e.Column.FieldName == "ViewedPeriod" && e.IsGetData)
            {
                if (Convert.ToInt32(gvView.GetRowCellValue(e.ListSourceRowIndex, "intPeriodID")) == intTempViewedPeriod)
                {
                    bmpTest = new Bitmap(imageList3.Images["viewed"], 16, 16);
                }
                else
                {
                    bmpTest = new Bitmap(16, 16);
                }
            }
            e.Value = bmpTest;
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            /*  // Following code creates a generic Edit form on the fly //
            GridView gView = (GridView)sender;
            GridHitInfo hInfo = gView.CalcHitInfo(gView.GridControl.PointToClient(Control.MousePosition));
            if (hInfo.InRowCell) 
            {
                new EditForm(Control.MousePosition, gView.Columns, gView.DataSource, BindingContext).ShowDialog(this);
                gView.UpdateCurrentRow();
            }
           */
        }


        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);

            GridView view = sender as GridView;
            view.GridControl.Focus();
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));

            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiBlockAdd.Enabled = false;
                bbiBlockEdit.Enabled = false;
                bbiSave.Enabled = false;
                bsiDataset.Enabled = false;

                int[] intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length <= 0)
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                    bbiDelete.Enabled = false;
                    bbiSetViewedPeriod.Enabled = false;
                    bbiSetLivePeriod.Enabled = false;
                    if (GlobalSettings.PersonType == 0)  // Super User //
                    {
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    else
                    {
                        bsiAdd.Enabled = false;
                        bbiSingleAdd.Enabled = false;
                    }
                }
                else
                {
                    if (GlobalSettings.PersonType == 0)  // Super User //
                    {
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        bbiDelete.Enabled = true;
                        bbiSetViewedPeriod.Enabled = true;
                        if (Convert.ToInt32(view.GetRowCellValue(hitInfo.RowHandle, "intPeriodID")) != intTempLivePeriod)
                        {
                            bbiSetLivePeriod.Enabled = true;
                        }
                        else
                        {
                            bbiSetLivePeriod.Enabled = false;
                        }
                    }
                    else
                    {
                        bsiAdd.Enabled = false;
                        bbiSingleAdd.Enabled = false;
                        bsiEdit.Enabled = false;
                        bbiSingleEdit.Enabled = false;
                        bbiDelete.Enabled = false;
                        bbiSetViewedPeriod.Enabled = true;
                        bbiSetLivePeriod.Enabled = false;
                    }
                    if (Convert.ToInt32(view.GetRowCellValue(hitInfo.RowHandle, "intPeriodID")) != intTempViewedPeriod)
                    {
                        bbiSetViewedPeriod.Enabled = true;
                    }
                    else
                    {
                        bbiSetViewedPeriod.Enabled = false;
                    }
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            ValidateRow(view, e);
        }

        private static void ValidateRow(GridView view, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            DateTime? dtToDate;
            DateTime? dtFromDate;
            string strDescription;
            DataRow row = view.GetDataRow(view.FocusedRowHandle);
            Boolean boolValidFromDate = true;
            Boolean boolValidToDate = true;
            if (view.FocusedColumn.Name == "colstrDesc")
            {
                if (e.Value == DBNull.Value) strDescription = null;
                else strDescription = Convert.ToString(e.Value).Trim();
            }
            else
            {
                if (view.GetFocusedRowCellValue("strDesc") == DBNull.Value) strDescription = null;
                else strDescription = Convert.ToString(view.GetFocusedRowCellValue("strDesc")).Trim();
            }
            if (strDescription == "" || strDescription == null)
            {
                string strColumnHeader = view.Columns["strDesc"].Caption;
                row.SetColumnError("strDesc", String.Format("{0} requires a value.", strColumnHeader));
            }
            else
            {
                row.SetColumnError("strDesc", "");  // Clear Error Text //
            }
            

            if (view.FocusedColumn.Name == "coldtFromDate")
            {
                if (e.Value == DBNull.Value) dtFromDate = null;
                else dtFromDate = Convert.ToDateTime(e.Value);
            }
            else
            {
                if (view.GetFocusedRowCellValue("dtFromDate") == DBNull.Value) dtFromDate = null;
                else dtFromDate = Convert.ToDateTime(view.GetFocusedRowCellValue("dtFromDate"));
            }
            if (dtFromDate == null)
            {
                string strColumnHeader = view.Columns["dtFromDate"].Caption;
                row.SetColumnError("dtFromDate", String.Format("{0} requires a value.", strColumnHeader));
                boolValidFromDate = false;
            }
            if (view.FocusedColumn.Name == "coldtToDate")
            {
                if (e.Value == DBNull.Value) dtToDate = null;
                else dtToDate = Convert.ToDateTime(e.Value);
            }
            else
            {
                if (view.GetFocusedRowCellValue("dtToDate") == DBNull.Value) dtToDate = null;
                else dtToDate = Convert.ToDateTime(view.GetFocusedRowCellValue("dtToDate"));
            }
            if (dtToDate == null)
            {
                string strColumnHeader = view.Columns["dtToDate"].Caption;
                row.SetColumnError("dtToDate", String.Format("{0} requires a value.", strColumnHeader));
                boolValidToDate = false;
            }

            if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
            {
                string strColumnHeader = view.Columns["dtFromDate"].Caption;
                row.SetColumnError("dtFromDate", String.Format("{0} should be less than To Date.", strColumnHeader));
                boolValidFromDate = false;
                strColumnHeader = view.Columns["dtToDate"].Caption;
                row.SetColumnError("dtToDate", String.Format("{0} should be greater than From Date.", strColumnHeader));
                boolValidToDate = false;
            }
            if (boolValidFromDate) row.SetColumnError("dtFromDate", "");  // Clear Error Text //
            if (boolValidToDate) row.SetColumnError("dtToDate", "");  // Clear Error Text //
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    e.Handled = true;
                    AddRow();
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.CancelEdit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Edit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.EndEdit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.NextPage:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.PrevPage:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    e.Handled = true;
                    DeleteRow();
                    break;
                default:
                    break;
            }
        }


        #endregion

      
        private void btnOK_Click(object sender, EventArgs e)
        {
            // Save changes first //
            
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to save all changes made to the System Periods.\n\nAre you sure you wish to proceed?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                GridView view = (GridView)gridControl1.MainView;
                if (!(view.PostEditor() && view.UpdateCurrentRow())) return; //Save the latest changes to the bound DataTable//

                int? intPeriodID = null;
                DateTime? dtFromDate = null;
                DateTime? dtToDate = null;
                string strDesc = null;

                int? intLiveID = null;
                int? intViewedID = null;
                
                Boolean boolMissingInfo = false;
                string strErrorMessage = "";
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (view.GetRowCellValue(i, "strDesc") == DBNull.Value || view.GetRowCellValue(i, "strDesc").ToString().Trim() == "") 
                    {
                        boolMissingInfo = true;
                        strErrorMessage += String.Format("Missing Description on record {0}\n", Convert.ToString(i + 1));
                    }
                    if (view.GetRowCellValue(i, "dtFromDate") == DBNull.Value || view.GetRowCellValue(i, "dtFromDate").ToString().Trim() == "") 
                    {
                        boolMissingInfo = true;
                        strErrorMessage += String.Format("Missing From Date on record {0}\n", Convert.ToString(i + 1));
                    }
                    else
                    {
                        dtFromDate = Convert.ToDateTime(view.GetRowCellValue(i, "dtFromDate"));
                    }
                    if (view.GetRowCellValue(i, "dtToDate") == DBNull.Value || view.GetRowCellValue(i, "dtToDate").ToString().Trim() == "") 
                    {
                        boolMissingInfo = true;
                        strErrorMessage += String.Format("Missing To Date on record {0}\n", Convert.ToString(i + 1));
                    }
                    else
                    {
                        dtToDate = Convert.ToDateTime(view.GetRowCellValue(i, "dtToDate"));
                    }
                    if (dtToDate < dtFromDate) 
                    {
                        boolMissingInfo = true;
                        strErrorMessage += String.Format("From Date should be less than To Date on record {0}\n", Convert.ToString(i + 1));
                    }
                    if (intLiveID == null && Convert.ToInt32(view.GetRowCellValue(i, "intPeriodID")) == intTempLivePeriod)
                        {

                        intLiveID = Convert.ToInt32(view.GetRowCellValue(i, "intPeriodID"));
                        if (dtFromDate != null) dtLiveStartDate = Convert.ToDateTime(dtFromDate);
                        if (dtToDate != null) dtLiveEndDate = Convert.ToDateTime(dtToDate);
                        intLivePeriodID = Convert.ToInt32(intLiveID);
                    }
                    if (intViewedID == null && Convert.ToInt32(view.GetRowCellValue(i, "intPeriodID")) == intTempViewedPeriod)
                    {
                        intViewedID = Convert.ToInt32(view.GetRowCellValue(i, "intPeriodID"));
                        if (dtFromDate != null) dtViewedStartDate = Convert.ToDateTime(dtFromDate);
                        if (dtToDate != null) dtViewedEndDate = Convert.ToDateTime(dtToDate);
                        intViewedPeriodID = Convert.ToInt32(intViewedID);
                    }
                }
                if (intLiveID == null)
                {
                    boolMissingInfo = true;
                    strErrorMessage += "No Period marked as Live\n";
                }
                if (intViewedID == null)
                {
                    boolMissingInfo = true;
                    strErrorMessage += "No Period marked as Viewed\n";
                }
                if (boolMissingInfo)
                {
                    strErrorMessage = String.Format("Unable to Save Changes - Correct the following errors first...\n\n{0}", strErrorMessage);
                    DevExpress.XtraEditors.XtraMessageBox.Show(strErrorMessage, "Save Changes - Errors Found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // Error check passed so proceed with save...//
                WoodPlanDataSetTableAdapters.QueriesTableAdapter UpdateData = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                UpdateData.ChangeConnectionString(GlobalSettings.ConnectionString);
                for (int i = 0; i < woodPlanDataSet.sp00097_date_periods_list.Rows.Count; i++)
                {
                    switch (woodPlanDataSet.sp00097_date_periods_list.Rows[i].RowState)
                    {
                        case DataRowState.Added:
                            if (woodPlanDataSet.sp00097_date_periods_list.Rows[i]["dtFromDate"] == DBNull.Value) dtFromDate = null;
                            else dtFromDate = Convert.ToDateTime(woodPlanDataSet.sp00097_date_periods_list.Rows[i]["dtFromDate"]);

                            if (woodPlanDataSet.sp00097_date_periods_list.Rows[i]["dtToDate"] == DBNull.Value) dtToDate = null;
                            else dtToDate = Convert.ToDateTime(woodPlanDataSet.sp00097_date_periods_list.Rows[i]["dtToDate"]);

                            if (woodPlanDataSet.sp00097_date_periods_list.Rows[i]["strDesc"] == DBNull.Value) strDesc = null;
                            else strDesc = Convert.ToString(woodPlanDataSet.sp00097_date_periods_list.Rows[i]["strDesc"]);

                            intPeriodID = Convert.ToInt32(UpdateData.sp00121_date_periods_update("ADD", 0, dtFromDate, dtToDate, strDesc));
                            woodPlanDataSet.sp00097_date_periods_list.Rows[i]["intPeriodID"] = intPeriodID;  // Update row with new auto increment id //
                            break;
                        case DataRowState.Modified:
                            if (woodPlanDataSet.sp00097_date_periods_list.Rows[i]["intPeriodID"] == DBNull.Value) intPeriodID = null;
                            else intPeriodID = Convert.ToInt32(woodPlanDataSet.sp00097_date_periods_list.Rows[i]["intPeriodID"]);

                            if (woodPlanDataSet.sp00097_date_periods_list.Rows[i]["dtFromDate"] == DBNull.Value) dtFromDate = null;
                            else dtFromDate = Convert.ToDateTime(woodPlanDataSet.sp00097_date_periods_list.Rows[i]["dtFromDate"]);

                            if (woodPlanDataSet.sp00097_date_periods_list.Rows[i]["dtToDate"] == DBNull.Value) dtToDate = null;
                            else dtToDate = Convert.ToDateTime(woodPlanDataSet.sp00097_date_periods_list.Rows[i]["dtToDate"]);

                            if (woodPlanDataSet.sp00097_date_periods_list.Rows[i]["strDesc"] == DBNull.Value) strDesc = null;
                            else strDesc = Convert.ToString(woodPlanDataSet.sp00097_date_periods_list.Rows[i]["strDesc"]);

                            UpdateData.sp00121_date_periods_update("EDIT", intPeriodID, dtFromDate, dtToDate, strDesc);
                            break;
                        case DataRowState.Deleted:
                            if (woodPlanDataSet.sp00097_date_periods_list.Rows[i].HasVersion(DataRowVersion.Original))
                            {
                                if (woodPlanDataSet.sp00097_date_periods_list.Rows[i]["intPeriodID", DataRowVersion.Original] == DBNull.Value) intPeriodID = 0;
                                else intPeriodID = Convert.ToInt32(woodPlanDataSet.sp00097_date_periods_list.Rows[i]["intPeriodID", DataRowVersion.Original]);
                                if (intPeriodID > 0) UpdateData.sp00121_date_periods_update("DELETE", intPeriodID, dtFromDate, dtToDate, strDesc);
                            }
                            break;
                    }
                    woodPlanDataSet.sp00097_date_periods_list.Rows[i].AcceptChanges();
                }
                	
                // Update current system settings...//
                if (GlobalSettings.LivePeriodID != intLiveID)
                {
                    WoodPlanDataSetTableAdapters.QueriesTableAdapter UpdateSettings = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                    UpdateSettings.ChangeConnectionString(GlobalSettings.ConnectionString);
                    UpdateSettings.sp00122_system_setting_update(intLiveID.ToString(), "LivePeriod", 0);
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void bbiSetViewedPeriod_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "intPeriodID")) != intTempViewedPeriod)
            {
                intTempViewedPeriod = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "intPeriodID"));
            }
        }

        private void bbiSetLivePeriod_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "intPeriodID")) != intTempLivePeriod)
            {
                intTempLivePeriod = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "intPeriodID"));
            }
        }

     }
}


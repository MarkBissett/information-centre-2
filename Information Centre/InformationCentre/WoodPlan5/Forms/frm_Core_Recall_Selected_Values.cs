using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Recall_Selected_Values : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public int intPassedInScreenID = 0;
        public Hashtable htPassedInObjects;

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_Core_Recall_Selected_Values()
        {
            InitializeComponent();
        }

        private void frm_Core_Recall_Selected_Values_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 20012;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            this.sp01378_AT_User_Screen_Settings_Select_ValuesTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01378_AT_User_Screen_Settings_Select_ValuesTableAdapter.Fill(dataSet_AT_DataEntry.sp01378_AT_User_Screen_Settings_Select_Values, this.GlobalSettings.UserID, intPassedInScreenID);

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.BeginSort();
            view.BeginDataUpdate();
            int intFoundRow = 0;
            IDictionaryEnumerator enumerator = htPassedInObjects.GetEnumerator();
            while (enumerator.MoveNext())
            {
                intFoundRow = view.LocateByValue(0, view.Columns["ItemName"], enumerator.Key.ToString());
                if (intFoundRow != GridControl.InvalidRowHandle) view.SetRowCellValue(intFoundRow, "UserFriendlyName", enumerator.Value.ToString());                
            }
            // Set Selected Status //
            for (int i = 0; i < view.DataRowCount; i++)
            {
                view.SetRowCellValue(i, "CheckMarkSelection", (view.GetRowCellValue(i, "UserSelected").ToString() == "1" ? true : false));
            }

            view.EndSort();
            view.EndDataUpdate();
            view.EndUpdate();
            htPassedInObjects.Clear();  // Empty HashTable to free up memory //

            this.UnlockThisWindow();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Values Stored");
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
 /*               bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));*/
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (selection1.SelectedCount <= 0)
            {
                XtraMessageBox.Show("Select one or more values to copy from by ticking them before proceeding.", "Select Values", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            GridView view = (GridView)gridControl1.MainView;
       
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    htPassedInObjects.Add(view.GetRowCellValue(i, "ItemName").ToString(), view.GetRowCellValue(i, "ItemValue").ToString());
                    view.SetRowCellValue(i, "UserSelected", 1);
                }
                else
                {
                    view.SetRowCellValue(i, "UserSelected", 0);
                }
            }
            // Store Row Selected Status for each item //
            this.sp01378ATUserScreenSettingsSelectValuesBindingSource.EndEdit();
            try
            {
                this.sp01378_AT_User_Screen_Settings_Select_ValuesTableAdapter.Update(dataSet_AT_DataEntry);  // Update query defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("An error occurred while saving the record selection status [" + ex.Message + "].", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }




    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Accident_Reported_By_Select : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public int intOriginalTypeID = 0;
        public int intOriginalID = 0;
        public int intOriginalParentID = 0;
        public string strOriginalName = "";
        public int intSelectedID = 0;
        public int intSelectedParentID = 0;
        public string strSelectedName = "";
        public int intSelectedTypeID = 0;
        public string strSelectedType = "";
        public bool _ShowOtherPage = true;

        #endregion

        public frm_Core_Accident_Reported_By_Select()
        {
            InitializeComponent();
        }

        private void frm_Core_Accident_Reported_By_Select_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500209;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp09110_HR_Staff_SelectTableAdapter.Fill(dataSet_HR_Core.sp09110_HR_Staff_Select, "", "", 0, 0);
            sp09111_HR_Contractor_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04023_GC_SubContractor_Team_MembersTableAdapter.Connection.ConnectionString = strConnectionString;

            GridView view = (GridView)gridControl1.MainView;
            
            LoadData();
            gridControl1.ForceInitialize();
            gridControl2.ForceInitialize();
            gridControl3.ForceInitialize();
            gridControl4.ForceInitialize();
            
            xtraTabPageOther.PageVisible = _ShowOtherPage;
            xtraTabPageOther.PageEnabled = _ShowOtherPage;

            switch (intOriginalTypeID)
            {
                case 1:
                    {
                        if (intOriginalID != 0)  // Record selected so try to find and highlight //
                        {
                            int intFoundRow = view.LocateByValue(0, view.Columns["inStaffID"], intOriginalID);
                            if (intFoundRow != GridControl.InvalidRowHandle)
                            {
                                view.FocusedRowHandle = intFoundRow;
                                view.MakeRowVisible(intFoundRow, false);
                            }
                        }
                        xtraTabControl1.SelectedTabPage = xtraTabPageStaff;
                    }
                    break;
                case 2:
                    {
                        if (intOriginalID != 0)  // Record selected so try to find and highlight //
                        {
                            view = (GridView)gridControl2.MainView;
                            int intFoundRow = view.LocateByValue(0, view.Columns["intContractorID"], intOriginalID);
                            if (intFoundRow != GridControl.InvalidRowHandle)
                            {
                                view.FocusedRowHandle = intFoundRow;
                                view.MakeRowVisible(intFoundRow, false);
                            }
                        }
                        xtraTabControl1.SelectedTabPage = xtraTabPageTeam;
                    }
                    break;
                case 3:
                    {
                        if (intOriginalParentID != 0)  // Record selected so try to find and highlight //
                        {
                            view = (GridView)gridControl3.MainView;
                            int intFoundRow = view.LocateByValue(0, view.Columns["intContractorID"], intOriginalParentID);
                            if (intFoundRow != GridControl.InvalidRowHandle)
                            {
                                view.FocusedRowHandle = intFoundRow;
                                view.MakeRowVisible(intFoundRow, false);
                            }
                            if (intOriginalID != 0)  // Record selected so try to find and highlight //
                            {
                                view = (GridView)gridControl4.MainView;
                                intFoundRow = view.LocateByValue(0, view.Columns["TeamMemberID"], intOriginalID);
                                if (intFoundRow != GridControl.InvalidRowHandle)
                                {
                                    view.FocusedRowHandle = intFoundRow;
                                    view.MakeRowVisible(intFoundRow, false);
                                }
                            }
                        }
                        xtraTabControl1.SelectedTabPage = xtraTabPageTeamMember;
                    }
                    break;
                case 4:
                    {
                        textEditOtherPersonName.EditValue = strOriginalName;
                        xtraTabControl1.SelectedTabPage = xtraTabPageOther;
                    }
                    break;
                default:
                    break;
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp09110_HR_Staff_SelectTableAdapter.Fill(dataSet_HR_Core.sp09110_HR_Staff_Select, "", "", 0, 0);
            view.EndUpdate();
           
            view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            sp09111_HR_Contractor_SelectTableAdapter.Fill(dataSet_HR_Core.sp09111_HR_Contractor_Select);
            view.EndUpdate();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Staff Available";
                    break;
                case "gridView2":
                    message = "No Teams Available";
                    break;
                case "gridView3":
                    message = "No Teams Available";
                    break;
                case "gridView4":
                    message = "No Team Members Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView3":
                    LoadLinkedData1();
                    GridView viewChild = (GridView)gridControl4.MainView;
                    viewChild.ExpandAllGroups();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        bool internalRowFocusing;


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            var intSelectedID = 0;

            if (intCount == 1)
            {
                intSelectedID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], view.Columns["intContractorID"]));
            }

            //Populate Linked sub types //
            gridControl4.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_GC_Core.sp04023_GC_SubContractor_Team_Members.Clear();
            }
            else
            {
                try
                {
                    sp04023_GC_SubContractor_Team_MembersTableAdapter.Fill(dataSet_GC_Core.sp04023_GC_SubContractor_Team_Members, intSelectedID.ToString() + ",");
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Team Members.\n\nTry selecting Team again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl4.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrWhiteSpace(strSelectedName))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Person", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            switch (xtraTabControl1.SelectedTabPage.Text)
            {
                case "Staff":
                    {
                        intSelectedTypeID = 1;
                        GridView view = (GridView)gridControl1.MainView;
                        if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                        {
                            intSelectedID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "intStaffID"));
                            strSelectedName = view.GetRowCellValue(view.FocusedRowHandle, "SurnameForename").ToString();
                        }
                    }
                    break;
                case "Team":
                    {
                        intSelectedTypeID = 2;
                        GridView view = (GridView)gridControl2.MainView;
                        if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                        {
                            intSelectedID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "intContractorID"));
                            strSelectedName = view.GetRowCellValue(view.FocusedRowHandle, "Name").ToString();
                        }
                    }
                    break;
                case "Team Member":
                    {
                        intSelectedTypeID = 3;
                        GridView view = (GridView)gridControl4.MainView;
                        if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                        {
                            intSelectedParentID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SubContractorID"));
                            intSelectedID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TeamMemberID"));
                            strSelectedName = view.GetRowCellValue(view.FocusedRowHandle, "Name").ToString();
                        }
                    }
                    break;
                case "Other":
                    {
                        intSelectedTypeID = 4;
                        strSelectedName = textEditOtherPersonName.EditValue.ToString();
                    }
                    break;
                default:
                    break;
            }
            strSelectedType = xtraTabControl1.SelectedTabPage.Text;
        }





    }
}


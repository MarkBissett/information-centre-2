namespace WoodPlan5
{
    partial class frm_Core_Web_Service_Queue_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Web_Service_Queue_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.btnViewData = new DevExpress.XtraEditors.SimpleButton();
            this.RecordTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp01027CoreWebServiceQueueQueueItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.SystemDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SystemIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateAddedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ErrorCountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.QueueIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.DataTextEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSystemID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForData = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForErrorCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForQueueID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSystemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRecordTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateAdded = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBtnViewData = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp01027_Core_Web_Service_Queue_Queue_ItemTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01027_Core_Web_Service_Queue_Queue_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01027CoreWebServiceQueueQueueItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SystemDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SystemIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorCountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QueueIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSystemID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForErrorCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQueueID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSystemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBtnViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(881, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 478);
            this.barDockControlBottom.Size = new System.Drawing.Size(881, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(881, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(881, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 478);
            this.barDockControl2.Size = new System.Drawing.Size(881, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(881, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 452);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.btnViewData);
            this.dataLayoutControl1.Controls.Add(this.RecordTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SystemDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SystemIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateAddedTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ErrorCountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.QueueIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.DataTextEdit);
            this.dataLayoutControl1.DataSource = this.sp01027CoreWebServiceQueueQueueItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStatusID,
            this.ItemForTypeID,
            this.ItemForSystemID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1145, 316, 598, 551);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(881, 452);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // btnViewData
            // 
            this.btnViewData.Image = ((System.Drawing.Image)(resources.GetObject("btnViewData.Image")));
            this.btnViewData.Location = new System.Drawing.Point(747, 179);
            this.btnViewData.Name = "btnViewData";
            this.btnViewData.Size = new System.Drawing.Size(122, 22);
            this.btnViewData.StyleController = this.dataLayoutControl1;
            this.btnViewData.TabIndex = 56;
            this.btnViewData.Text = "View Data in Viewer";
            this.btnViewData.Click += new System.EventHandler(this.btnViewData_Click);
            // 
            // RecordTypeDescriptionTextEdit
            // 
            this.RecordTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "RecordTypeDescription", true));
            this.RecordTypeDescriptionTextEdit.Location = new System.Drawing.Point(80, 83);
            this.RecordTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.RecordTypeDescriptionTextEdit.Name = "RecordTypeDescriptionTextEdit";
            this.RecordTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RecordTypeDescriptionTextEdit, true);
            this.RecordTypeDescriptionTextEdit.Size = new System.Drawing.Size(180, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RecordTypeDescriptionTextEdit, optionsSpelling1);
            this.RecordTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.RecordTypeDescriptionTextEdit.TabIndex = 55;
            // 
            // sp01027CoreWebServiceQueueQueueItemBindingSource
            // 
            this.sp01027CoreWebServiceQueueQueueItemBindingSource.DataMember = "sp01027_Core_Web_Service_Queue_Queue_Item";
            this.sp01027CoreWebServiceQueueQueueItemBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SystemDescriptionTextEdit
            // 
            this.SystemDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "SystemDescription", true));
            this.SystemDescriptionTextEdit.Location = new System.Drawing.Point(80, 59);
            this.SystemDescriptionTextEdit.MenuManager = this.barManager1;
            this.SystemDescriptionTextEdit.Name = "SystemDescriptionTextEdit";
            this.SystemDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SystemDescriptionTextEdit, true);
            this.SystemDescriptionTextEdit.Size = new System.Drawing.Size(180, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SystemDescriptionTextEdit, optionsSpelling2);
            this.SystemDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.SystemDescriptionTextEdit.TabIndex = 54;
            // 
            // StatusDescriptionTextEdit
            // 
            this.StatusDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "StatusDescription", true));
            this.StatusDescriptionTextEdit.Location = new System.Drawing.Point(80, 131);
            this.StatusDescriptionTextEdit.MenuManager = this.barManager1;
            this.StatusDescriptionTextEdit.Name = "StatusDescriptionTextEdit";
            this.StatusDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StatusDescriptionTextEdit, true);
            this.StatusDescriptionTextEdit.Size = new System.Drawing.Size(180, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StatusDescriptionTextEdit, optionsSpelling3);
            this.StatusDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.StatusDescriptionTextEdit.TabIndex = 53;
            // 
            // SystemIDTextEdit
            // 
            this.SystemIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "StatusID", true));
            this.SystemIDTextEdit.Location = new System.Drawing.Point(76, 107);
            this.SystemIDTextEdit.MenuManager = this.barManager1;
            this.SystemIDTextEdit.Name = "SystemIDTextEdit";
            this.SystemIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SystemIDTextEdit, true);
            this.SystemIDTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SystemIDTextEdit, optionsSpelling4);
            this.SystemIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SystemIDTextEdit.TabIndex = 52;
            // 
            // StatusIDTextEdit
            // 
            this.StatusIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "StatusID", true));
            this.StatusIDTextEdit.Location = new System.Drawing.Point(76, 107);
            this.StatusIDTextEdit.MenuManager = this.barManager1;
            this.StatusIDTextEdit.Name = "StatusIDTextEdit";
            this.StatusIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StatusIDTextEdit, true);
            this.StatusIDTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StatusIDTextEdit, optionsSpelling5);
            this.StatusIDTextEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDTextEdit.TabIndex = 51;
            // 
            // TypeIDTextEdit
            // 
            this.TypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "TypeID", true));
            this.TypeIDTextEdit.Location = new System.Drawing.Point(76, 59);
            this.TypeIDTextEdit.MenuManager = this.barManager1;
            this.TypeIDTextEdit.Name = "TypeIDTextEdit";
            this.TypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TypeIDTextEdit, true);
            this.TypeIDTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TypeIDTextEdit, optionsSpelling6);
            this.TypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TypeIDTextEdit.TabIndex = 50;
            // 
            // DateAddedTextEdit
            // 
            this.DateAddedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "DateAdded", true));
            this.DateAddedTextEdit.Location = new System.Drawing.Point(80, 107);
            this.DateAddedTextEdit.MenuManager = this.barManager1;
            this.DateAddedTextEdit.Name = "DateAddedTextEdit";
            this.DateAddedTextEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss.fff";
            this.DateAddedTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.DateAddedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateAddedTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DateAddedTextEdit, true);
            this.DateAddedTextEdit.Size = new System.Drawing.Size(180, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DateAddedTextEdit, optionsSpelling7);
            this.DateAddedTextEdit.StyleController = this.dataLayoutControl1;
            this.DateAddedTextEdit.TabIndex = 49;
            // 
            // ErrorCountSpinEdit
            // 
            this.ErrorCountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "ErrorCount", true));
            this.ErrorCountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ErrorCountSpinEdit.Location = new System.Drawing.Point(80, 155);
            this.ErrorCountSpinEdit.MenuManager = this.barManager1;
            this.ErrorCountSpinEdit.Name = "ErrorCountSpinEdit";
            this.ErrorCountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ErrorCountSpinEdit.Properties.Mask.EditMask = "n0";
            this.ErrorCountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ErrorCountSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.ErrorCountSpinEdit.Size = new System.Drawing.Size(180, 20);
            this.ErrorCountSpinEdit.StyleController = this.dataLayoutControl1;
            this.ErrorCountSpinEdit.TabIndex = 48;
            // 
            // QueueIDTextEdit
            // 
            this.QueueIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "QueueID", true));
            this.QueueIDTextEdit.Location = new System.Drawing.Point(80, 35);
            this.QueueIDTextEdit.MenuManager = this.barManager1;
            this.QueueIDTextEdit.Name = "QueueIDTextEdit";
            this.QueueIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QueueIDTextEdit, true);
            this.QueueIDTextEdit.Size = new System.Drawing.Size(180, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QueueIDTextEdit, optionsSpelling8);
            this.QueueIDTextEdit.StyleController = this.dataLayoutControl1;
            this.QueueIDTextEdit.TabIndex = 25;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp01027CoreWebServiceQueueQueueItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(79, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(181, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // DataTextEdit
            // 
            this.DataTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01027CoreWebServiceQueueQueueItemBindingSource, "Data", true));
            this.DataTextEdit.Location = new System.Drawing.Point(80, 205);
            this.DataTextEdit.MenuManager = this.barManager1;
            this.DataTextEdit.Name = "DataTextEdit";
            this.DataTextEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DataTextEdit, true);
            this.DataTextEdit.Size = new System.Drawing.Size(789, 235);
            this.scSpellChecker.SetSpellCheckerOptions(this.DataTextEdit, optionsSpelling9);
            this.DataTextEdit.StyleController = this.dataLayoutControl1;
            this.DataTextEdit.TabIndex = 47;
            this.DataTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DataTextEdit_Validating);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDTextEdit;
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 95);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(226, 24);
            this.ItemForStatusID.Text = "Status ID:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForTypeID
            // 
            this.ItemForTypeID.Control = this.TypeIDTextEdit;
            this.ItemForTypeID.Location = new System.Drawing.Point(0, 47);
            this.ItemForTypeID.Name = "ItemForTypeID";
            this.ItemForTypeID.Size = new System.Drawing.Size(226, 24);
            this.ItemForTypeID.Text = "Type ID:";
            this.ItemForTypeID.TextSize = new System.Drawing.Size(61, 13);
            // 
            // ItemForSystemID
            // 
            this.ItemForSystemID.Control = this.SystemIDTextEdit;
            this.ItemForSystemID.Location = new System.Drawing.Point(0, 95);
            this.ItemForSystemID.Name = "ItemForSystemID";
            this.ItemForSystemID.Size = new System.Drawing.Size(226, 24);
            this.ItemForSystemID.Text = "Module ID:";
            this.ItemForSystemID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(881, 452);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem6,
            this.ItemForData,
            this.ItemForErrorCount,
            this.emptySpaceItem3,
            this.ItemForQueueID,
            this.ItemForStatusDescription,
            this.ItemForSystemDescription,
            this.ItemForRecordTypeDescription,
            this.ItemForDateAdded,
            this.ItemForBtnViewData});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(861, 432);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(67, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(67, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(67, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(252, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(609, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(67, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(185, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(185, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(185, 23);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(735, 26);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForData
            // 
            this.ItemForData.Control = this.DataTextEdit;
            this.ItemForData.CustomizationFormText = "Data";
            this.ItemForData.Location = new System.Drawing.Point(0, 193);
            this.ItemForData.Name = "ItemForData";
            this.ItemForData.Size = new System.Drawing.Size(861, 239);
            this.ItemForData.Text = "Data";
            this.ItemForData.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForErrorCount
            // 
            this.ItemForErrorCount.Control = this.ErrorCountSpinEdit;
            this.ItemForErrorCount.Location = new System.Drawing.Point(0, 143);
            this.ItemForErrorCount.Name = "ItemForErrorCount";
            this.ItemForErrorCount.Size = new System.Drawing.Size(252, 24);
            this.ItemForErrorCount.Text = "Error Count:";
            this.ItemForErrorCount.TextSize = new System.Drawing.Size(65, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(252, 23);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(609, 144);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForQueueID
            // 
            this.ItemForQueueID.Control = this.QueueIDTextEdit;
            this.ItemForQueueID.CustomizationFormText = "Queue ID:";
            this.ItemForQueueID.Location = new System.Drawing.Point(0, 23);
            this.ItemForQueueID.MaxSize = new System.Drawing.Size(252, 24);
            this.ItemForQueueID.MinSize = new System.Drawing.Size(252, 24);
            this.ItemForQueueID.Name = "ItemForQueueID";
            this.ItemForQueueID.Size = new System.Drawing.Size(252, 24);
            this.ItemForQueueID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForQueueID.Text = "Queue ID:";
            this.ItemForQueueID.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForStatusDescription
            // 
            this.ItemForStatusDescription.Control = this.StatusDescriptionTextEdit;
            this.ItemForStatusDescription.Location = new System.Drawing.Point(0, 119);
            this.ItemForStatusDescription.Name = "ItemForStatusDescription";
            this.ItemForStatusDescription.Size = new System.Drawing.Size(252, 24);
            this.ItemForStatusDescription.Text = "Status:";
            this.ItemForStatusDescription.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSystemDescription
            // 
            this.ItemForSystemDescription.Control = this.SystemDescriptionTextEdit;
            this.ItemForSystemDescription.Location = new System.Drawing.Point(0, 47);
            this.ItemForSystemDescription.Name = "ItemForSystemDescription";
            this.ItemForSystemDescription.Size = new System.Drawing.Size(252, 24);
            this.ItemForSystemDescription.Text = "Module:";
            this.ItemForSystemDescription.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForRecordTypeDescription
            // 
            this.ItemForRecordTypeDescription.Control = this.RecordTypeDescriptionTextEdit;
            this.ItemForRecordTypeDescription.Location = new System.Drawing.Point(0, 71);
            this.ItemForRecordTypeDescription.Name = "ItemForRecordTypeDescription";
            this.ItemForRecordTypeDescription.Size = new System.Drawing.Size(252, 24);
            this.ItemForRecordTypeDescription.Text = "Record Type:";
            this.ItemForRecordTypeDescription.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForDateAdded
            // 
            this.ItemForDateAdded.Control = this.DateAddedTextEdit;
            this.ItemForDateAdded.Location = new System.Drawing.Point(0, 95);
            this.ItemForDateAdded.Name = "ItemForDateAdded";
            this.ItemForDateAdded.Size = new System.Drawing.Size(252, 24);
            this.ItemForDateAdded.Text = "Date Added:";
            this.ItemForDateAdded.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForBtnViewData
            // 
            this.ItemForBtnViewData.Control = this.btnViewData;
            this.ItemForBtnViewData.Location = new System.Drawing.Point(735, 167);
            this.ItemForBtnViewData.MaxSize = new System.Drawing.Size(126, 26);
            this.ItemForBtnViewData.MinSize = new System.Drawing.Size(126, 26);
            this.ItemForBtnViewData.Name = "ItemForBtnViewData";
            this.ItemForBtnViewData.Size = new System.Drawing.Size(126, 26);
            this.ItemForBtnViewData.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForBtnViewData.Text = "View Data Button:";
            this.ItemForBtnViewData.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForBtnViewData.TextVisible = false;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp01027_Core_Web_Service_Queue_Queue_ItemTableAdapter
            // 
            this.sp01027_Core_Web_Service_Queue_Queue_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Web_Service_Queue_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(881, 508);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Web_Service_Queue_Edit";
            this.Text = "Web Service Queue Edit";
            this.Activated += new System.EventHandler(this.frm_Core_Web_Service_Queue_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Web_Service_Queue_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Web_Service_Queue_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RecordTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01027CoreWebServiceQueueQueueItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SystemDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SystemIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorCountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QueueIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSystemID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForErrorCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQueueID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSystemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBtnViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.TextEdit QueueIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQueueID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.MemoEdit DataTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForData;
        private System.Windows.Forms.BindingSource sp01027CoreWebServiceQueueQueueItemBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DataSet_Common_FunctionalityTableAdapters.sp01027_Core_Web_Service_Queue_Queue_ItemTableAdapter sp01027_Core_Web_Service_Queue_Queue_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit DateAddedTextEdit;
        private DevExpress.XtraEditors.SpinEdit ErrorCountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForErrorCount;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateAdded;
        private DevExpress.XtraEditors.TextEdit StatusIDTextEdit;
        private DevExpress.XtraEditors.TextEdit TypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTypeID;
        private DevExpress.XtraEditors.TextEdit SystemDescriptionTextEdit;
        private DevExpress.XtraEditors.TextEdit StatusDescriptionTextEdit;
        private DevExpress.XtraEditors.TextEdit SystemIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSystemID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSystemDescription;
        private DevExpress.XtraEditors.TextEdit RecordTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRecordTypeDescription;
        private DevExpress.XtraEditors.SimpleButton btnViewData;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBtnViewData;
    }
}

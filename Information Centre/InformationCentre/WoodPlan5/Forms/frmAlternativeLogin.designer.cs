namespace WoodPlan5
{
    partial class frmAlternativeLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AvailableLoginsLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.sp00071getavailableloginsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sp00071_get_available_loginsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00071_get_available_loginsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AvailableLoginsLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00071getavailableloginsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // AvailableLoginsLookUpEdit
            // 
            this.AvailableLoginsLookUpEdit.Location = new System.Drawing.Point(52, 9);
            this.AvailableLoginsLookUpEdit.MenuManager = this.barManager1;
            this.AvailableLoginsLookUpEdit.Name = "AvailableLoginsLookUpEdit";
            this.AvailableLoginsLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AvailableLoginsLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StaffID", "StaffID", 54, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StaffName", "Staff Name", 57, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NetworkID", "NetworkID", 57, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)});
            this.AvailableLoginsLookUpEdit.Properties.DataSource = this.sp00071getavailableloginsBindingSource;
            this.AvailableLoginsLookUpEdit.Properties.DisplayMember = "StaffName";
            this.AvailableLoginsLookUpEdit.Properties.NullText = "";
            this.AvailableLoginsLookUpEdit.Properties.ValueMember = "StaffID";
            this.AvailableLoginsLookUpEdit.Size = new System.Drawing.Size(327, 20);
            this.AvailableLoginsLookUpEdit.TabIndex = 4;
            this.AvailableLoginsLookUpEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AvailableLoginsLookUpEdit_KeyPress);
            // 
            // sp00071getavailableloginsBindingSource
            // 
            this.sp00071getavailableloginsBindingSource.DataMember = "sp00071_get_available_logins";
            this.sp00071getavailableloginsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(34, 14);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Logins:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(304, 37);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "Login";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // sp00071_get_available_loginsTableAdapter
            // 
            this.sp00071_get_available_loginsTableAdapter.ClearBeforeFill = true;
            // 
            // frmAlternativeLogin
            // 
            this.AcceptButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(388, 67);
            this.ControlBox = false;
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.AvailableLoginsLookUpEdit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAlternativeLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Available Logins";
            this.Load += new System.EventHandler(this.frmAlternativeLogin_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.AvailableLoginsLookUpEdit, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AvailableLoginsLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00071getavailableloginsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit AvailableLoginsLookUpEdit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private System.Windows.Forms.BindingSource sp00071getavailableloginsBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00071_get_available_loginsTableAdapter sp00071_get_available_loginsTableAdapter;
    }
}

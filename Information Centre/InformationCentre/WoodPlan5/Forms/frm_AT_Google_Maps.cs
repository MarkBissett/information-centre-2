using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WoodPlan5
{
    public partial class frm_AT_Google_Maps : WoodPlan5.frmBase_Modal
    {
        public frm_AT_Google_Maps(string lat, string lon)
        {
            InitializeComponent();
            
            if (lat == string.Empty || lon == string.Empty)
            {
                this.Dispose();
            }
            try
            {
                
                StringBuilder queryAddress = new StringBuilder();
                queryAddress.Append("http://maps.google.com/maps?q=");

                if (lat != string.Empty)
                {
                    queryAddress.Append(lat + "%2C");
                }

                if (lon != string.Empty)
                {
                    queryAddress.Append(lon);
                }
                queryAddress.Append("&output=svembed");
                webBrowser1.Navigate(queryAddress.ToString());
                //webBrowser1.Navigate("C:\\Mark_Development\\OnlineMapping_Test\\Bing\\HTMLPage.htm");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to Retrieve Map - " + ex.Message.ToString(), "Show in Google Maps");
                this.Close();
            }
        }

        void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            HtmlDocument doc = webBrowser1.Document;
            HtmlElement logo = doc.GetElementById("stview");
            HtmlElement line = doc.CreateElement("hr");
            logo.InsertAdjacentElement(HtmlElementInsertionOrientation.BeforeBegin, line);
        }

        private void frm_AT_Google_Maps_Load(object sender, EventArgs e)
        {

        }
    }
}


namespace WoodPlan5
{
    partial class frmGroupManagerEditGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGroupManagerEditGroup));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.gridLookUpEdit1 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00078permissiongroupeditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Groups = new WoodPlan5.DataSet_Groups();
            this.sp00079permissiongrouptypelistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGroupTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lcEditCount = new DevExpress.XtraEditors.LabelControl();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.GroupIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GroupDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.GroupRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.CreatedByIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForGroupID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForGroupDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGroupRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBlockEditWarningIcon = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBlockEditWarningLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00078_permission_group_editTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00078_permission_group_editTableAdapter();
            this.sp00079_permission_group_type_listTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00079_permission_group_type_listTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00078permissiongroupeditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Groups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00079permissiongrouptypelistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGroupID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGroupDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGroupRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBlockEditWarningIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBlockEditWarningLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(614, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 421);
            this.barDockControlBottom.Size = new System.Drawing.Size(614, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 421);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(614, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 421);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.labelControl1);
            this.dataLayoutControl1.Controls.Add(this.pictureEdit1);
            this.dataLayoutControl1.Controls.Add(this.gridLookUpEdit1);
            this.dataLayoutControl1.Controls.Add(this.lcEditCount);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.GroupIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GroupDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.GroupRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp00078permissiongroupeditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGroupID,
            this.ItemForCreatedByID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.MenuManager = this.barManager1;
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1088, 235, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(614, 421);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Location = new System.Drawing.Point(45, 57);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(557, 27);
            this.labelControl1.StyleController = this.dataLayoutControl1;
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "<b>Warning</b>: You are block editing - any change made to any field will be copi" +
    "ed to all selected records!";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.attention_16;
            this.pictureEdit1.Location = new System.Drawing.Point(12, 57);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.AllowFocused = false;
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ReadOnly = true;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(29, 27);
            this.pictureEdit1.StyleController = this.dataLayoutControl1;
            this.pictureEdit1.TabIndex = 12;
            // 
            // gridLookUpEdit1
            // 
            this.gridLookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00078permissiongroupeditBindingSource, "GroupTypeID", true));
            this.gridLookUpEdit1.EditValue = "";
            this.gridLookUpEdit1.Location = new System.Drawing.Point(104, 88);
            this.gridLookUpEdit1.MenuManager = this.barManager1;
            this.gridLookUpEdit1.Name = "gridLookUpEdit1";
            this.gridLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEdit1.Properties.DataSource = this.sp00079permissiongrouptypelistBindingSource;
            this.gridLookUpEdit1.Properties.DisplayMember = "GroupTypeDescription";
            this.gridLookUpEdit1.Properties.NullText = "";
            this.gridLookUpEdit1.Properties.ValueMember = "GroupTypeID";
            this.gridLookUpEdit1.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEdit1.Size = new System.Drawing.Size(498, 20);
            this.gridLookUpEdit1.StyleController = this.dataLayoutControl1;
            this.gridLookUpEdit1.TabIndex = 11;
            // 
            // sp00078permissiongroupeditBindingSource
            // 
            this.sp00078permissiongroupeditBindingSource.DataMember = "sp00078_permission_group_edit";
            this.sp00078permissiongroupeditBindingSource.DataSource = this.dataSet_Groups;
            // 
            // dataSet_Groups
            // 
            this.dataSet_Groups.DataSetName = "DataSet_Groups";
            this.dataSet_Groups.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00079permissiongrouptypelistBindingSource
            // 
            this.sp00079permissiongrouptypelistBindingSource.DataMember = "sp00079_permission_group_type_list";
            this.sp00079permissiongrouptypelistBindingSource.DataSource = this.dataSet_Groups;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGroupTypeID,
            this.colGroupTypeDescription});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            // 
            // colGroupTypeID
            // 
            this.colGroupTypeID.Caption = "Group Type ID";
            this.colGroupTypeID.FieldName = "GroupTypeID";
            this.colGroupTypeID.Name = "colGroupTypeID";
            this.colGroupTypeID.OptionsColumn.AllowEdit = false;
            this.colGroupTypeID.OptionsColumn.AllowFocus = false;
            this.colGroupTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colGroupTypeDescription
            // 
            this.colGroupTypeDescription.Caption = "Group Type Description";
            this.colGroupTypeDescription.FieldName = "GroupTypeDescription";
            this.colGroupTypeDescription.Name = "colGroupTypeDescription";
            this.colGroupTypeDescription.OptionsColumn.AllowEdit = false;
            this.colGroupTypeDescription.OptionsColumn.AllowFocus = false;
            this.colGroupTypeDescription.OptionsColumn.ReadOnly = true;
            this.colGroupTypeDescription.Visible = true;
            this.colGroupTypeDescription.VisibleIndex = 0;
            // 
            // lcEditCount
            // 
            this.lcEditCount.AllowHtmlString = true;
            this.lcEditCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lcEditCount.Location = new System.Drawing.Point(12, 40);
            this.lcEditCount.Name = "lcEditCount";
            this.lcEditCount.Size = new System.Drawing.Size(590, 13);
            this.lcEditCount.StyleController = this.dataLayoutControl1;
            this.lcEditCount.TabIndex = 10;
            this.lcEditCount.Text = "Edit x Groups";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Hint = "Cancel changes and close screen";
            this.dataNavigator1.Buttons.CancelEdit.ImageIndex = 1;
            this.dataNavigator1.Buttons.EndEdit.Hint = "Save changes and close screen";
            this.dataNavigator1.Buttons.EndEdit.ImageIndex = 0;
            this.dataNavigator1.Buttons.ImageList = this.imageCollection1;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00078permissiongroupeditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.ShowToolTips = true;
            this.dataNavigator1.Size = new System.Drawing.Size(226, 24);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 9;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.SaveAndClose_16x16, "SaveAndClose_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "SaveAndClose_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.close_16x16, "close_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "close_16x16");
            // 
            // GroupIDSpinEdit
            // 
            this.GroupIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00078permissiongroupeditBindingSource, "GroupID", true));
            this.GroupIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.GroupIDSpinEdit.Location = new System.Drawing.Point(0, 0);
            this.GroupIDSpinEdit.Name = "GroupIDSpinEdit";
            this.GroupIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.GroupIDSpinEdit.Size = new System.Drawing.Size(0, 20);
            this.GroupIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.GroupIDSpinEdit.TabIndex = 4;
            // 
            // GroupDescriptionTextEdit
            // 
            this.GroupDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00078permissiongroupeditBindingSource, "GroupDescription", true));
            this.GroupDescriptionTextEdit.Location = new System.Drawing.Point(104, 119);
            this.GroupDescriptionTextEdit.MenuManager = this.barManager1;
            this.GroupDescriptionTextEdit.Name = "GroupDescriptionTextEdit";
            this.GroupDescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GroupDescriptionTextEdit, true);
            this.GroupDescriptionTextEdit.Size = new System.Drawing.Size(498, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GroupDescriptionTextEdit, optionsSpelling1);
            this.GroupDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.GroupDescriptionTextEdit.TabIndex = 6;
            // 
            // GroupRemarksMemoEdit
            // 
            this.GroupRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00078permissiongroupeditBindingSource, "GroupRemarks", true));
            this.GroupRemarksMemoEdit.Location = new System.Drawing.Point(104, 150);
            this.GroupRemarksMemoEdit.MenuManager = this.barManager1;
            this.GroupRemarksMemoEdit.Name = "GroupRemarksMemoEdit";
            this.GroupRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GroupRemarksMemoEdit, true);
            this.GroupRemarksMemoEdit.Size = new System.Drawing.Size(498, 168);
            this.scSpellChecker.SetSpellCheckerOptions(this.GroupRemarksMemoEdit, optionsSpelling2);
            this.GroupRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.GroupRemarksMemoEdit.TabIndex = 7;
            // 
            // CreatedByIDTextEdit
            // 
            this.CreatedByIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00078permissiongroupeditBindingSource, "CreatedByID", true));
            this.CreatedByIDTextEdit.Location = new System.Drawing.Point(0, 0);
            this.CreatedByIDTextEdit.Name = "CreatedByIDTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByIDTextEdit, true);
            this.CreatedByIDTextEdit.Size = new System.Drawing.Size(0, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByIDTextEdit, optionsSpelling3);
            this.CreatedByIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByIDTextEdit.TabIndex = 8;
            // 
            // ItemForGroupID
            // 
            this.ItemForGroupID.Control = this.GroupIDSpinEdit;
            this.ItemForGroupID.CustomizationFormText = "GroupID";
            this.ItemForGroupID.Location = new System.Drawing.Point(0, 0);
            this.ItemForGroupID.Name = "ItemForGroupID";
            this.ItemForGroupID.Size = new System.Drawing.Size(0, 0);
            this.ItemForGroupID.Text = "GroupID";
            this.ItemForGroupID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByID
            // 
            this.ItemForCreatedByID.Control = this.CreatedByIDTextEdit;
            this.ItemForCreatedByID.CustomizationFormText = "CreatedByID";
            this.ItemForCreatedByID.Location = new System.Drawing.Point(0, 0);
            this.ItemForCreatedByID.Name = "ItemForCreatedByID";
            this.ItemForCreatedByID.Size = new System.Drawing.Size(0, 0);
            this.ItemForCreatedByID.Text = "CreatedByID";
            this.ItemForCreatedByID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGroupDescription,
            this.ItemForGroupRemarks,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlBlockEditWarningIcon,
            this.layoutControlBlockEditWarningLabel,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup1.Size = new System.Drawing.Size(614, 421);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForGroupDescription
            // 
            this.ItemForGroupDescription.AllowHtmlStringInCaption = true;
            this.ItemForGroupDescription.Control = this.GroupDescriptionTextEdit;
            this.ItemForGroupDescription.CustomizationFormText = "Group Description:";
            this.ItemForGroupDescription.Location = new System.Drawing.Point(0, 107);
            this.ItemForGroupDescription.MaxSize = new System.Drawing.Size(0, 31);
            this.ItemForGroupDescription.MinSize = new System.Drawing.Size(155, 31);
            this.ItemForGroupDescription.Name = "ItemForGroupDescription";
            this.ItemForGroupDescription.Size = new System.Drawing.Size(594, 31);
            this.ItemForGroupDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGroupDescription.Text = "Group Description:";
            this.ItemForGroupDescription.TextSize = new System.Drawing.Size(89, 13);
            // 
            // ItemForGroupRemarks
            // 
            this.ItemForGroupRemarks.AllowHtmlStringInCaption = true;
            this.ItemForGroupRemarks.Control = this.GroupRemarksMemoEdit;
            this.ItemForGroupRemarks.CustomizationFormText = "Group Remarks:";
            this.ItemForGroupRemarks.Location = new System.Drawing.Point(0, 138);
            this.ItemForGroupRemarks.MaxSize = new System.Drawing.Size(0, 172);
            this.ItemForGroupRemarks.MinSize = new System.Drawing.Size(115, 172);
            this.ItemForGroupRemarks.Name = "ItemForGroupRemarks";
            this.ItemForGroupRemarks.Size = new System.Drawing.Size(594, 172);
            this.ItemForGroupRemarks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGroupRemarks.Text = "Group Remarks:";
            this.ItemForGroupRemarks.TextSize = new System.Drawing.Size(89, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lcEditCount;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(594, 17);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AllowHtmlStringInCaption = true;
            this.layoutControlItem3.Control = this.gridLookUpEdit1;
            this.layoutControlItem3.CustomizationFormText = "Group Type:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 76);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 31);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(155, 31);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(594, 31);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Group Type:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(89, 13);
            // 
            // layoutControlBlockEditWarningIcon
            // 
            this.layoutControlBlockEditWarningIcon.Control = this.pictureEdit1;
            this.layoutControlBlockEditWarningIcon.CustomizationFormText = "Block Edit Warning Icon";
            this.layoutControlBlockEditWarningIcon.Location = new System.Drawing.Point(0, 45);
            this.layoutControlBlockEditWarningIcon.MaxSize = new System.Drawing.Size(33, 31);
            this.layoutControlBlockEditWarningIcon.MinSize = new System.Drawing.Size(33, 31);
            this.layoutControlBlockEditWarningIcon.Name = "layoutControlBlockEditWarningIcon";
            this.layoutControlBlockEditWarningIcon.Size = new System.Drawing.Size(33, 31);
            this.layoutControlBlockEditWarningIcon.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBlockEditWarningIcon.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBlockEditWarningIcon.TextVisible = false;
            // 
            // layoutControlBlockEditWarningLabel
            // 
            this.layoutControlBlockEditWarningLabel.Control = this.labelControl1;
            this.layoutControlBlockEditWarningLabel.CustomizationFormText = "Block Edit Warning Label";
            this.layoutControlBlockEditWarningLabel.Location = new System.Drawing.Point(33, 45);
            this.layoutControlBlockEditWarningLabel.MaxSize = new System.Drawing.Size(0, 31);
            this.layoutControlBlockEditWarningLabel.MinSize = new System.Drawing.Size(74, 31);
            this.layoutControlBlockEditWarningLabel.Name = "layoutControlBlockEditWarningLabel";
            this.layoutControlBlockEditWarningLabel.Size = new System.Drawing.Size(561, 31);
            this.layoutControlBlockEditWarningLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBlockEditWarningLabel.Text = "Warning: You are block editing - any chnage made to any field will copied to all " +
    "selected records!";
            this.layoutControlBlockEditWarningLabel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBlockEditWarningLabel.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 310);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(594, 91);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(230, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(364, 28);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(230, 28);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // sp00078_permission_group_editTableAdapter
            // 
            this.sp00078_permission_group_editTableAdapter.ClearBeforeFill = true;
            // 
            // sp00079_permission_group_type_listTableAdapter
            // 
            this.sp00079_permission_group_type_listTableAdapter.ClearBeforeFill = true;
            // 
            // frmGroupManagerEditGroup
            // 
            this.ClientSize = new System.Drawing.Size(614, 421);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frmGroupManagerEditGroup";
            this.Text = "Permission Groups - Add/Edit";
            this.Activated += new System.EventHandler(this.frmGroupManagerEditGroup_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmGroupManagerEditGroup_FormClosing);
            this.Load += new System.EventHandler(this.frmGroupManagerEditGroup_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00078permissiongroupeditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Groups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00079permissiongrouptypelistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGroupID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGroupDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGroupRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBlockEditWarningIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBlockEditWarningLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00078permissiongroupeditBindingSource;
        private DataSet_Groups dataSet_Groups;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private WoodPlan5.DataSet_GroupsTableAdapters.sp00078_permission_group_editTableAdapter sp00078_permission_group_editTableAdapter;
        private DevExpress.XtraEditors.SpinEdit GroupIDSpinEdit;
        private DevExpress.XtraEditors.TextEdit GroupDescriptionTextEdit;
        private DevExpress.XtraEditors.MemoEdit GroupRemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit CreatedByIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGroupID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGroupDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGroupRemarks;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl lcEditCount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource sp00079permissiongrouptypelistBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupTypeDescription;
        private WoodPlan5.DataSet_GroupsTableAdapters.sp00079_permission_group_type_listTableAdapter sp00079_permission_group_type_listTableAdapter;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBlockEditWarningIcon;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBlockEditWarningLabel;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;

    }
}

﻿using System;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraMap;

namespace WoodPlan5
{
    public partial class frm_Core_Mapping_Create_Image : BaseObjects.frmBase 
    {
        string strKMLFileToLoad = "";
        string strSavedImageFile = ""; MapControl map;
        Timer timer = new Timer() { Interval = 50 };
        bool tilesLoaded;

        public frm_Core_Mapping_Create_Image(string KMLFileToLoad, string SavedFileName) 
        {
            InitializeComponent();
            strKMLFileToLoad = KMLFileToLoad;
            strSavedImageFile = SavedFileName;
            labelMapFileName.Text += " " + SavedFileName;
            map = new MapControl() { EnableAnimation = false, Size = new System.Drawing.Size(500, 450) };
            map.BeginInit();

            #region #BingMap
            ImageTilesLayer imageLayer = new ImageTilesLayer();
            imageLayer.DataProvider = new BingMapDataProvider();
            imageLayer.DataLoaded += imageLayer_DataLoaded;
            imageLayer.RequestDataLoading += imageLayer_RequestDataLoading;
            map.Layers.Add(imageLayer);
            #endregion

            timer.Tick += timer_Tick;

            #region #KmlFileDataAdapter
            VectorItemsLayer kmlLayer = new VectorItemsLayer();
            kmlLayer.DataLoaded += KmlLayer_DataLoaded;
            Uri baseUri = new Uri(System.Reflection.Assembly.GetEntryAssembly().Location);
            kmlLayer.Data = new KmlFileDataAdapter() { FileUri = new Uri(baseUri, strKMLFileToLoad) };
            map.Layers.Add(kmlLayer);
            #endregion #KmlFileDataAdapter

            map.EndInit();
        }

        void imageLayer_RequestDataLoading(object sender, EventArgs e){
            tilesLoaded = false;
            map.ZoomLevel = Math.Min(map.ZoomLevel, 19.4);  // Bing Maps tiles are not available at a certain zoom level (ZoomLevel = 20) and the DataLoaded event is never fired so prevent it. //
        }
        void imageLayer_DataLoaded(object sender, DataLoadedEventArgs e){
            tilesLoaded = true;
        }
        void timer_Tick(object sender, EventArgs e){
            if (tilesLoaded) {
                timer.Stop();
                try
                {
                    map.ExportToImage(strSavedImageFile, ImageFormat.Jpeg);
                }
                catch (Exception) { }
                //Process.Start(strSavedImageFile);
                this.Close();
            }
        }
        void KmlLayer_DataLoaded(object sender, DataLoadedEventArgs e) {
            map.ZoomToFit(((MapItemsLoadedEventArgs)e).Items, 0.2);
            timer.Start();
        }
    }
}

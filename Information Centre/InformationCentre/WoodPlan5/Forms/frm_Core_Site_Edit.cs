using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.IO;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_Core_Site_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public int intLinkedToRecordID2 = 0;
        public int intMaxOrder = 0;
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";   // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        public frm_OM_Tender_Edit frm_Tender_Edit_Screen_To_Update = null;  // Only used if called from the Edit_Tender screen so we can update the correct instance //
        public bool AllowClientChange = true;

        #endregion

        public frm_Core_Site_Edit()
        {
            InitializeComponent();
        }

        private void frm_Core_Site_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 30021;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GroundControlAppSiteDrawingsPath").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default App Site Drawing Files Path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default App Site Image Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            sp03009_EP_Client_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03009_EP_Client_List_With_BlankTableAdapter.Fill(dataSet_EP_DataEntry.sp03009_EP_Client_List_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00227_Picklist_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00227_Picklist_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00227_Picklist_List_With_Blank, 159);  // 159 = SiteIDTextEdit //
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp03063_EP_Mapping_Workspaces_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03063_EP_Mapping_Workspaces_With_BlankTableAdapter.Fill(dataSet_EP_DataEntry.sp03063_EP_Mapping_Workspaces_With_Blank);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp03067_EP_Site_Edit_Linked_InspectionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView2, "InspectionID");
            sp03044_EP_Asset_Manager_List_SimpleTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView3, "AssetID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04002_GC_Select_Company_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04002_GC_Select_Company_ListTableAdapter.Fill(dataSet_GC_Core.sp04002_GC_Select_Company_List);

            // Populate Main Dataset //
            sp03007_EP_Site_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp03007_EP_Site_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SiteCode"] = "";
                        drNewRow["SiteName"] = "";
                        drNewRow["ClientID"] = intLinkedToRecordID;
                        drNewRow["CompanyID"] = intLinkedToRecordID2;
                        drNewRow["SiteOrder"] = intMaxOrder + 1;
                        drNewRow["GeoFenceDistance"] = 2000;  // 2 Km //
                        drNewRow["SiteImageFile"] = "";
                        this.dataSet_EP_DataEntry.sp03007_EP_Site_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp03007_EP_Site_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SiteCode"] = "";
                        drNewRow["SiteName"] = "";
                        this.dataSet_EP_DataEntry.sp03007_EP_Site_Edit.Rows.Add(drNewRow);
                        this.dataSet_EP_DataEntry.sp03007_EP_Site_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp03007_EP_Site_EditTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03007_EP_Site_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_EP_DataEntry.sp03007_EP_Site_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        if (AllowClientChange)  // Set to False if called by the OM Edit Tender screen (when adding a new site) //
                        {
                            ClientIDGridLookUpEdit.Properties.ReadOnly = false;
                            ClientIDGridLookUpEdit.Focus();
                        }
                        else
                        {
                            ClientIDGridLookUpEdit.Properties.ReadOnly = true;
                            CompanyIDGridLookUpEdit.Focus();
                        }
                        SiteCodeTextEdit.Properties.ReadOnly = false;
                        SiteNameTextEdit.Properties.ReadOnly = false;
                        //HubIDTextEdit.Properties.ReadOnly = false;
                        XCoordinateSpinEdit.Properties.ReadOnly = false;
                        YCoordinateSpinEdit.Properties.ReadOnly = false;
                        LocationXSpinEdit.Properties.ReadOnly = false;
                        LocationYSpinEdit.Properties.ReadOnly = false;
                        gridControl1.Enabled = false;
                        gridControl2.Enabled = false;

                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        SiteCodeTextEdit.Focus();

                        SiteCodeTextEdit.Properties.ReadOnly = true;
                        SiteNameTextEdit.Properties.ReadOnly = true;
                        //HubIDTextEdit.Properties.ReadOnly = true;
                        XCoordinateSpinEdit.Properties.ReadOnly = true;
                        YCoordinateSpinEdit.Properties.ReadOnly = true;
                        LocationXSpinEdit.Properties.ReadOnly = true;
                        LocationYSpinEdit.Properties.ReadOnly = true;
                        gridControl1.Enabled = false;
                        gridControl2.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        SiteCodeTextEdit.Focus();

                        SiteCodeTextEdit.Properties.ReadOnly = false;
                        SiteNameTextEdit.Properties.ReadOnly = false;
                        //HubIDTextEdit.Properties.ReadOnly = false;
                        XCoordinateSpinEdit.Properties.ReadOnly = false;
                        YCoordinateSpinEdit.Properties.ReadOnly = false;
                        LocationXSpinEdit.Properties.ReadOnly = false;
                        LocationYSpinEdit.Properties.ReadOnly = false;
                        gridControl1.Enabled = true;
                        gridControl2.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        SiteCodeTextEdit.Focus();

                        SiteCodeTextEdit.Properties.ReadOnly = true;
                        SiteNameTextEdit.Properties.ReadOnly = true;
                        //HubIDTextEdit.Properties.ReadOnly = true;
                        XCoordinateSpinEdit.Properties.ReadOnly = true;
                        YCoordinateSpinEdit.Properties.ReadOnly = true;
                        LocationXSpinEdit.Properties.ReadOnly = true;
                        LocationYSpinEdit.Properties.ReadOnly = true;
                        gridControl1.Enabled = false;
                        gridControl2.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_EP_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            GridView view = null;
            int[] intRowHandles;

            int intRecordID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp03007EPSiteEditBindingSource.Current;
            if (currentRow != null) intRecordID = (currentRow["SiteID"] == null ? 0 : Convert.ToInt32(currentRow["SiteID"]));

            if (i_int_FocusedGrid == 1)  // Site Inspections //
            {
                view = (GridView)gridControl1.MainView;
            }
            else if (i_int_FocusedGrid == 2)  // Assets //
            {
                view = (GridView)gridControl2.MainView;
            }
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd && intRecordID > 0)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                if (intRowHandles.Length >= 1)
                {
                    //bbiBlockAddAction.Enabled = true;
                }
            }
            bbiBlockAdd.Enabled = false;
            if (iBool_AllowEdit && intRowHandles.Length >= 1)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                if (intRowHandles.Length >= 2)
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = true;
                }
            }
            if (iBool_AllowDelete && intRowHandles.Length >= 1)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
            }


            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd && intRecordID > 0)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd && intRecordID > 0)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
        }

        public void External_Call_For_Immediate_Refresh_Linked_Records(string strNewIDs1, string strNewIDs2)
        {
            // Triggered by main MDI Form's Site Inspection Control Panel //
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            LoadLinkedRecords();
        }


        private void frm_Core_Site_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                LoadLinkedRecords();
            }
            SetMenuStatus();
        }

        private void frm_Core_Site_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp03007EPSiteEditBindingSource.EndEdit();
            try
            {
                this.sp03007_EP_Site_EditTableAdapter.Update(dataSet_EP_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRowView = (DataRowView)sp03007EPSiteEditBindingSource.Current;
                var currentRow = (DataSet_EP_DataEntry.sp03007_EP_Site_EditRow)currentRowView.Row;
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    strNewIDs = currentRow.SiteID + ";";
                    currentRow.strMode = "edit";  // Switch mode to Edit so than any subsequent changes update this record //
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //

                    // See if there is a specific Edit Tender screen open we need to send this record's values to (only happens if the Edit Tender screen opened this screen) //
                    if (frm_Tender_Edit_Screen_To_Update != null)
                    {
                        try
                        {
                            string strSiteCode = (currentRow.IsNull("SiteCode") ? "" : currentRow.SiteCode);
                            string strSitePostcode = (currentRow.IsNull("SitePostcode") ? "" : currentRow.SitePostcode);
                            double dblLocationX = (currentRow.IsNull("LocationX") ? (double)0.00 : currentRow.LocationX);
                            double dblLocationY = (currentRow.IsNull("LocationY") ? (double)0.00 : currentRow.LocationY);

                            string strAddress = "";
                            string[] arrayAddress = new string[] { 
                                                                (currentRow.IsNull("SiteAddressLine1") ? "" : currentRow.SiteAddressLine1),
                                                                (currentRow.IsNull("SiteAddressLine2") ? "" : currentRow.SiteAddressLine2),
                                                                (currentRow.IsNull("SiteAddressLine3") ? "" : currentRow.SiteAddressLine3),
                                                                (currentRow.IsNull("SiteAddressLine4") ? "" : currentRow.SiteAddressLine4),
                                                                (currentRow.IsNull("SiteAddressLine5") ? "" : currentRow.SiteAddressLine5),
                                                                (currentRow.IsNull("SitePostcode") ? "" : currentRow.SitePostcode)
                                                            };
                            strAddress = String.Join(", ", arrayAddress.Where(s => !string.IsNullOrEmpty(s)));  // Merge the fields into one and eliminate blanks (using LINQ) //

                            frm_Tender_Edit_Screen_To_Update.SetTenderSiteFromAddScreen(currentRow.SiteID, currentRow.SiteName, strSiteCode, strAddress, strSitePostcode, dblLocationX, dblLocationY);
                        }
                        catch (Exception) { }
                    }
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_Core_Site_Manager")
                    {
                        var fParentForm = (frm_Core_Site_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "", "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_EP_DataEntry.sp03007_EP_Site_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_EP_DataEntry.sp03007_EP_Site_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strRecordID = "";
            DataRowView currentRow = (DataRowView)sp03007EPSiteEditBindingSource.Current;
            if (currentRow != null)
            {
                strRecordID = (currentRow["SiteID"] == null ? "" : currentRow["SiteID"].ToString() + ",");
            }

            // Linked Inspections //
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            sp03067_EP_Site_Edit_Linked_InspectionsTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03067_EP_Site_Edit_Linked_Inspections, strRecordID);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["InspectionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            // Linked Assets //
            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
            gridControl2.BeginUpdate();

            sp03044_EP_Asset_Manager_List_SimpleTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03044_EP_Asset_Manager_List_Simple, strRecordID);
            this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl2.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl2.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["AssetID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            if (this.strFormMode != "blockedit")
            {
                LoadLinkedRecords();
                GridView view = (GridView)gridControl1.MainView;
                view.ExpandAllGroups();
                view = (GridView)gridControl2.MainView;
                view.ExpandAllGroups();
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void ClientIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(ClientIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ClientIDGridLookUpEdit, "");
            }
        }

        private void SiteNameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(SiteNameTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SiteNameTextEdit, "");
            }
        }

        private void SiteTypeIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 159, "Site Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00227_Picklist_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00227_Picklist_List_With_Blank, 159);  // 159 = SiteIDTextEdit Types //
                }
            }
        }

        private void ClientIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 3001, "Clients");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp03009_EP_Client_List_With_BlankTableAdapter.Fill(dataSet_EP_DataEntry.sp03009_EP_Client_List_With_Blank);
                }
            }
        }

        private void CompanyIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 4003, "Companies");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp04002_GC_Select_Company_ListTableAdapter.Fill(dataSet_GC_Core.sp04002_GC_Select_Company_List);
                }
            }
        }

        private void SiteEmailMemoEdit_Validating(object sender, CancelEventArgs e)
        {
            //    MemoEdit me = (MemoEdit)sender;
            //    if (!string.IsNullOrEmpty(me.EditValue.ToString()))
            //    {
            //        // Check email addresses are valid //
            //        Boolean boolInvalid = false;
            //        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
            //        string strEmail = me.Text;
            //        Array arrayItems = strEmail.Split(',');  // Single quotes because char expected for delimeter //
            //        if (arrayItems.Length > 0)  // multiple email addresses present so check each //
            //        {
            //            foreach (string strElement in arrayItems)
            //            {
            //                System.Text.RegularExpressions.Match match = regex.Match(strElement);
            //                if (!match.Success)
            //                {
            //                    boolInvalid = true;
            //                    break;
            //                }
            //            }
            //        }
            //        else  // Only 1 email address present //
            //        {
            //            System.Text.RegularExpressions.Match match = regex.Match(strEmail);
            //            if (!match.Success) 
            //            {
            //                boolInvalid = true;
            //            }
            //        }
            //        if (boolInvalid)
            //        {
            //            dxErrorProvider1.SetError(SiteEmailMemoEdit, "Email Address entered is not in a valid format - multiple emails address should be separated with a comma. No spaces should be present.");
            //            e.Cancel = true;  // Show stop icon as field is invalid //
            //            return;
            //        }
            //        else
            //        {
            //            dxErrorProvider1.SetError(SiteEmailMemoEdit, "");
            //        }
            //    }
            //    else
            //    {
            //        dxErrorProvider1.SetError(SiteEmailMemoEdit, "");
            //    }
            //}

            MemoEdit me = (MemoEdit)sender;
            if (!string.IsNullOrEmpty(me.EditValue.ToString()))
            {
                // Check email addresses are valid //
                int intInvalidCount = 0;
                string strInvalidEmailAddresses = "";
                string strEmail = me.Text;
                Array arrayItems = strEmail.Split(',');  // Single quotes because char expected for delimeter //
                if (arrayItems.Length > 0)  // multiple email addresses present so check each //
                {
                    foreach (string strElement in arrayItems)
                    {
                        if (!BaseObjects.ExtensionFunctions.IsValidEmail(strElement))
                        {
                            intInvalidCount++;
                            strInvalidEmailAddresses += "--> " + strElement + "\n";
                        }
                    }
                }
                else  // Only 1 email address present //
                {
                    if (!BaseObjects.ExtensionFunctions.IsValidEmail(strEmail))
                    {
                        intInvalidCount++;
                        strInvalidEmailAddresses += "--> " + strEmail + "\n";
                    }
                }
                if (intInvalidCount > 0)
                {
                    string strMessage = "The Following Email Addresses are not in a valid format:\n\n" + strInvalidEmailAddresses + "\nMultiple emails addresses should be separated with a comma. No spaces should be present.";
                    dxErrorProvider1.SetError(SiteEmailMemoEdit, strMessage);
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
                else
                {
                    dxErrorProvider1.SetError(SiteEmailMemoEdit, "");
                }
            }
            else
            {
                dxErrorProvider1.SetError(SiteEmailMemoEdit, "");
            }

        }

        private void SiteImageFileButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            DataRowView currentRow = null;
            currentRow = (DataRowView)sp03007EPSiteEditBindingSource.Current;
            if (currentRow == null) return;
            switch (e.Button.Tag.ToString())
            {
                case "select file":
                    using (OpenFileDialog dlg = new OpenFileDialog())
                    {
                        dlg.DefaultExt = "jpg";
                        dlg.Filter = "JPEG Files (*.jpg)|*.jpg|PNG Files(*.png)|*.png|GIF Files(*.gif)|*.gif";
                        if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                        dlg.Multiselect = false;
                        dlg.ShowDialog();
                        if (dlg.FileNames.Length > 0)
                        {
                            string strTempFileName = "";
                            string strTempResult = "";
                            //string strExtension = "";
                            foreach (string filename in dlg.FileNames)
                            {
                                if (strDefaultPath != "")
                                {
                                    if (!filename.ToLower().StartsWith(strDefaultPath.ToLower()))
                                    {
                                        DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Site Drawings Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        break;
                                    }
                                }
                                long length = new System.IO.FileInfo(filename).Length;
                                if (length > 2097152)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files which have a file size less than or equal to 2 Mb.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    break;
                                }

                                strTempFileName = filename.Substring(strDefaultPath.Length);
                                if (strTempResult == "")
                                {
                                    if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                }
                                else
                                {
                                    if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                }
                            }
                            //SiteImageFileButtonEdit.Text = strTempResult;
                            currentRow["SiteImageFile"] = strTempResult;
                        }
                        this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter so linked file can be viewed if required //
                    }
                    break;
                case "view file":
                    string strFile = currentRow["SiteImageFile"].ToString();
                    if (string.IsNullOrEmpty(strFile))
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Link the File to be Viewed before proceeding.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Note: Can't use Path.Combine as the filenames starting with a backslash can't use Path.Combine successfully //
                    string strFilePath = strDefaultPath.TrimEnd(Path.DirectorySeparatorChar);
                    strFile = strFile.TrimStart(Path.DirectorySeparatorChar);
                    strFilePath += "\\" + strFile;
                    try
                    {
                        System.Diagnostics.Process.Start(strFilePath);
                    }
                    catch
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFilePath + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView2":
                    message = "No Inspections Available";
                    break;
                case "gridView3":
                    message = "No Assets Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
            SetMenuStatus();
        }

        #endregion


        #region Grid - Inspections

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /*bbiShowMap.Enabled = false;
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;*/
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Grid - Assets

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /*bbiShowMap.Enabled = false;
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;*/
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            int intParentID = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Site_Inspection_Edit fChildForm = new frm_EP_Site_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_Core_Site_Edit";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;

                        DataRowView currentRow = (DataRowView)sp03007EPSiteEditBindingSource.Current;
                        if (currentRow != null)
                        {
                            intParentID = (currentRow["SiteID"] == null ? 0 : Convert.ToInt32(currentRow["SiteID"]));
                        }
                        fChildForm.intLinkedToRecordID = intParentID;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:     // Assets //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Asset_Edit fChildForm = new frm_EP_Asset_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_Core_Site_Edit";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;

                        DataRowView currentRow = (DataRowView)sp03007EPSiteEditBindingSource.Current;
                        if (currentRow != null)
                        {
                            intParentID = (currentRow["SiteID"] == null ? 0 : Convert.ToInt32(currentRow["SiteID"]));
                        }
                        fChildForm.intLinkedToRecordID = intParentID;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Site_Inspection_Edit fChildForm = new frm_EP_Site_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = "frm_Core_Site_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:     // Assets //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AssetID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Asset_Edit fChildForm = new frm_EP_Asset_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = "frm_Core_Site_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Site_Inspection_Edit fChildForm = new frm_EP_Site_Inspection_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_Core_Site_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:     // Assets //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AssetID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_EP_Action_Edit fChildForm = new frm_EP_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = "frm_Core_Site_Edit";
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Inspections //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Inspections to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Site Inspection" : Convert.ToString(intRowHandles.Length) + " Site Inspections") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Site Inspection" : "these Site Inspections") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp03053_EP_Site_Inspection_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }

                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Site_Inspection_Refresh(this.ParentForm, this.Name, "", "");

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:  // Assets //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Assets to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Asset" : Convert.ToString(intRowHandles.Length) + " Assets") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Asset" : "these Assets") + " will no longer be available for selection and any linked Actions will also be deleted!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AssetID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp03029_EP_Asset_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords();
                        // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Asset_Refresh(this.ParentForm, this.Name, "", "");

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                default:
                    break;
            }
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            GridView view = null;
            string strSelectedIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteID")) + ',';
                        }
                        try
                        {
                            Mapping_Functions MapFunctions = new Mapping_Functions();
                            MapFunctions.Show_Map_From_Assets_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "site");
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        break;
                    }
                case 2:     // Assets //
                    {
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AssetID")) + ',';
                        }
                        try
                        {
                            Mapping_Functions MapFunctions = new Mapping_Functions();
                            MapFunctions.Show_Map_From_Assets_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "asset");
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    break;
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            DataRowView currentRow = (DataRowView)sp03007EPSiteEditBindingSource.Current;
            if (currentRow == null) return;
            if (String.IsNullOrEmpty(currentRow["SiteID"].ToString()) || currentRow["SiteID"].ToString() == "0")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Save the current record before attempting to display it on the map.\n\nTip: Once you have successfully opened the map for the site for the first time, scroll the map to a required centre point for the site then right click on the map and select Set Site Centre Point then choose the Locality. On subsequent opening of the map it will then auto scroll to this point.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = currentRow["SiteID"].ToString() + ',';
            try
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Show_Map_From_Assets_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "site");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void SitePostcodeTextEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "lookup")  // Lookup Latitude \ Longitude Button //
            {
                DataRowView currentRow = (DataRowView)sp03007EPSiteEditBindingSource.Current;
                if (currentRow != null)
                {
                    string strSitePostcode = "";
                    if (!string.IsNullOrEmpty(currentRow["SitePostcode"].ToString())) strSitePostcode = currentRow["SitePostcode"].ToString();
                    frm_GC_Get_Lat_Long_From_Postcode fChildForm = new frm_GC_Get_Lat_Long_From_Postcode();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strPassedInPostcode = strSitePostcode;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                    {
                        currentRow["XCoordinate"] = fChildForm.intSelectedEasting;
                        currentRow["YCoordinate"] = fChildForm.intSelectedNorthing;
                        currentRow["LocationX"] = fChildForm.dblSelectedLatitude;
                        currentRow["LocationY"] = fChildForm.dblSelectedLongitude;
                        this.sp03007EPSiteEditBindingSource.EndEdit();
                    }
                }
            }

        }

        private void btnViewOnMap_Click(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp03007EPSiteEditBindingSource.Current;
            if (currentRow == null) return;
            double dStartLat = (double)0.00;
            double dStartLong = (double)0.00;
            List<Mapping_Objects> mapObjectsList = new List<Mapping_Objects>();
            dStartLat = (!string.IsNullOrEmpty(currentRow["LocationX"].ToString()) ? Convert.ToDouble(currentRow["LocationX"]) : (double)0.00);
            dStartLong = (!string.IsNullOrEmpty(currentRow["LocationY"].ToString()) ? Convert.ToDouble(currentRow["LocationY"]) : (double)0.00);
            if (!(dStartLat == (double)0.0 && dStartLong == (double)0.0))
            {
                Mapping_Objects mapObject = new Mapping_Objects();
                mapObject.doubleLat = dStartLat;
                mapObject.doubleLong = dStartLong;
                mapObject.stringToolTip = "Site: " + (string.IsNullOrEmpty(currentRow["SiteName"].ToString()) ? "" : currentRow["SiteName"].ToString());
                mapObject.stringID = "Site|" + (string.IsNullOrEmpty(currentRow["SiteID"].ToString()) ? "0" : currentRow["SiteID"].ToString());
                mapObjectsList.Add(mapObject);
            }

            if (mapObjectsList.Count == 0)  // No objects added due to missing coordinates //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter the Latitude and Longitude for the site before proceeding.\n\nTip: If you are unsure of the Latitude and Longitude, look them up via the Lookup button.", "View Site on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                Mapping_Functions mapFunctions = new Mapping_Functions();
                mapFunctions.View_Object_In_Internet_Mapping(this.GlobalSettings, this.strConnectionString, this, mapObjectsList);
            }

        }

        private void btnViewKML_Click(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp03007EPSiteEditBindingSource.Current;
            if (currentRow == null) return;
            string strKML = currentRow["SiteKMLData"].ToString();
            if (string.IsNullOrWhiteSpace(strKML))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No KML Data Present - unable to proceed.", "View Site KML on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strFileName = string.Empty;

            try
            {
                // Get the full name of the newly created Temporary file. //
                // Note that the GetTempFileName() method actually creates a 0-byte file and returns the name of the created file. //
                strFileName = Path.GetTempFileName();

                // Create a FileInfo object to set the file's attributes //
                FileInfo fileInfo = new FileInfo(strFileName);

                // Set the Attribute property of this file to Temporary. //
                // Although this is not completely necessary, the .NET Framework is able to optimize the use of Temporary files by keeping them cached in memory. //
                fileInfo.Attributes = FileAttributes.Temporary;
                System.IO.File.WriteAllText(strFileName, strKML);
            }
            catch (Exception ex)
            {
               Console.WriteLine("Unable to create Temp file for KML or set its attributes: " + ex.Message +"\n\nUnable to proceed.", "View Site KML on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_Core_KML_Viewer();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInKMLFileName = strFileName;
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }





    }
}


namespace WoodPlan5
{
    partial class frm_Core_Package_Configuration_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Package_Configuration_Manager));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.packageConfigurationGridControl = new DevExpress.XtraGrid.GridControl();
            this.spPC922701PackageConfigurationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_PC_Core = new WoodPlan5.DataSet_PC_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.packageConfigurationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobPackageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobPackageDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailSubjectLine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateLastRun = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colErrorEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportRecipients = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActiveReportRecipients = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecipientsWithSuccessNotification = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecipientsWithFailureNotification = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHasReportOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.packageConfigurationChildTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.emailListTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.recipientListGridControl = new DevExpress.XtraGrid.GridControl();
            this.spPC922703PackageEmailListItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.recipientListGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colServiceEmailListID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobPackageID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobPackageDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecipientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecipientEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotifyFailure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotifySuccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastSendDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSendReport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsReportOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager();
            this.tableAdapterManager1 = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp_PC_922701_Package_Configuration_ItemTableAdapter = new WoodPlan5.DataSet_PC_CoreTableAdapters.sp_PC_922701_Package_Configuration_ItemTableAdapter();
            this.sp_PC_922703_Package_Email_List_ItemTableAdapter = new WoodPlan5.DataSet_PC_CoreTableAdapters.sp_PC_922703_Package_Email_List_ItemTableAdapter();
            this.spPC922709ReturnServiceEmailListIDBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_PC_922709_Return_ServiceEmailListIDTableAdapter = new WoodPlan5.DataSet_PC_CoreTableAdapters.sp_PC_922709_Return_ServiceEmailListIDTableAdapter();
            this.spPC922710ReturnJobPackageIDBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_PC_922710_Return_JobPackageIDTableAdapter = new WoodPlan5.DataSet_PC_CoreTableAdapters.sp_PC_922710_Return_JobPackageIDTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.packageConfigurationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922701PackageConfigurationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_PC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageConfigurationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageConfigurationChildTabControl)).BeginInit();
            this.packageConfigurationChildTabControl.SuspendLayout();
            this.emailListTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recipientListGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922703PackageEmailListItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recipientListGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922709ReturnServiceEmailListIDBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922710ReturnJobPackageIDBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1810, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 740);
            this.barDockControlBottom.Size = new System.Drawing.Size(1810, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 740);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1810, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 740);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.packageConfigurationChildTabControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1810, 740);
            this.splitContainerControl1.SplitterPosition = 183;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.packageConfigurationGridControl;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.packageConfigurationGridControl);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1810, 551);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // packageConfigurationGridControl
            // 
            this.packageConfigurationGridControl.DataSource = this.spPC922701PackageConfigurationItemBindingSource;
            this.packageConfigurationGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.packageConfigurationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.packageConfigurationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.packageConfigurationGridControl.Location = new System.Drawing.Point(0, 0);
            this.packageConfigurationGridControl.MainView = this.packageConfigurationGridView;
            this.packageConfigurationGridControl.MenuManager = this.barManager1;
            this.packageConfigurationGridControl.Name = "packageConfigurationGridControl";
            this.packageConfigurationGridControl.Size = new System.Drawing.Size(1810, 551);
            this.packageConfigurationGridControl.TabIndex = 1;
            this.packageConfigurationGridControl.UseEmbeddedNavigator = true;
            this.packageConfigurationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.packageConfigurationGridView});
            // 
            // spPC922701PackageConfigurationItemBindingSource
            // 
            this.spPC922701PackageConfigurationItemBindingSource.DataMember = "sp_PC_922701_Package_Configuration_Item";
            this.spPC922701PackageConfigurationItemBindingSource.DataSource = this.dataSet_PC_Core;
            // 
            // dataSet_PC_Core
            // 
            this.dataSet_PC_Core.DataSetName = "DataSet_PC_Core";
            this.dataSet_PC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("country_16x16.png", "images/miscellaneous/country_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/country_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "country_16x16.png");
            this.imageCollection1.InsertGalleryImage("insert_16x16.png", "images/actions/insert_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/insert_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "insert_16x16.png");
            // 
            // packageConfigurationGridView
            // 
            this.packageConfigurationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobPackageID,
            this.colJobPackageDescription,
            this.colEmailSubjectLine,
            this.colDateCreated,
            this.colDateLastRun,
            this.colIsActive,
            this.colErrorEmail,
            this.colReportRecipients,
            this.colActiveReportRecipients,
            this.colRecipientsWithSuccessNotification,
            this.colRecipientsWithFailureNotification,
            this.colHasReportOwner,
            this.colMode,
            this.colRecordID});
            this.packageConfigurationGridView.GridControl = this.packageConfigurationGridControl;
            this.packageConfigurationGridView.Name = "packageConfigurationGridView";
            this.packageConfigurationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.packageConfigurationGridView.OptionsFind.AlwaysVisible = true;
            this.packageConfigurationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.packageConfigurationGridView.OptionsLayout.StoreAppearance = true;
            this.packageConfigurationGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.packageConfigurationGridView.OptionsSelection.MultiSelect = true;
            this.packageConfigurationGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.packageConfigurationGridView.OptionsView.ColumnAutoWidth = false;
            this.packageConfigurationGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.packageConfigurationGridView.OptionsView.ShowGroupPanel = false;
            this.packageConfigurationGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.packageConfigurationView_PopupMenuShowing);
            this.packageConfigurationGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.packageConfigurationView_SelectionChanged);
            this.packageConfigurationGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.packageConfigurationGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.packageConfigurationGridView.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.packageConfigurationGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.packageConfigurationGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.packageConfigurationGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colJobPackageID
            // 
            this.colJobPackageID.FieldName = "JobPackageID";
            this.colJobPackageID.Name = "colJobPackageID";
            this.colJobPackageID.OptionsColumn.AllowEdit = false;
            this.colJobPackageID.OptionsColumn.AllowFocus = false;
            this.colJobPackageID.OptionsColumn.ReadOnly = true;
            this.colJobPackageID.Width = 155;
            // 
            // colJobPackageDescription
            // 
            this.colJobPackageDescription.FieldName = "JobPackageDescription";
            this.colJobPackageDescription.Name = "colJobPackageDescription";
            this.colJobPackageDescription.OptionsColumn.AllowEdit = false;
            this.colJobPackageDescription.OptionsColumn.AllowFocus = false;
            this.colJobPackageDescription.OptionsColumn.ReadOnly = true;
            this.colJobPackageDescription.Visible = true;
            this.colJobPackageDescription.VisibleIndex = 0;
            this.colJobPackageDescription.Width = 350;
            // 
            // colEmailSubjectLine
            // 
            this.colEmailSubjectLine.FieldName = "EmailSubjectLine";
            this.colEmailSubjectLine.Name = "colEmailSubjectLine";
            this.colEmailSubjectLine.OptionsColumn.AllowEdit = false;
            this.colEmailSubjectLine.OptionsColumn.AllowFocus = false;
            this.colEmailSubjectLine.OptionsColumn.ReadOnly = true;
            this.colEmailSubjectLine.Visible = true;
            this.colEmailSubjectLine.VisibleIndex = 1;
            this.colEmailSubjectLine.Width = 333;
            // 
            // colDateCreated
            // 
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 2;
            this.colDateCreated.Width = 80;
            // 
            // colDateLastRun
            // 
            this.colDateLastRun.FieldName = "DateLastRun";
            this.colDateLastRun.Name = "colDateLastRun";
            this.colDateLastRun.OptionsColumn.AllowEdit = false;
            this.colDateLastRun.OptionsColumn.AllowFocus = false;
            this.colDateLastRun.OptionsColumn.ReadOnly = true;
            this.colDateLastRun.Visible = true;
            this.colDateLastRun.VisibleIndex = 3;
            this.colDateLastRun.Width = 84;
            // 
            // colIsActive
            // 
            this.colIsActive.FieldName = "IsActive";
            this.colIsActive.Name = "colIsActive";
            this.colIsActive.OptionsColumn.AllowEdit = false;
            this.colIsActive.OptionsColumn.AllowFocus = false;
            this.colIsActive.OptionsColumn.ReadOnly = true;
            this.colIsActive.Visible = true;
            this.colIsActive.VisibleIndex = 4;
            // 
            // colErrorEmail
            // 
            this.colErrorEmail.FieldName = "ErrorEmail";
            this.colErrorEmail.Name = "colErrorEmail";
            this.colErrorEmail.OptionsColumn.AllowEdit = false;
            this.colErrorEmail.OptionsColumn.AllowFocus = false;
            this.colErrorEmail.OptionsColumn.ReadOnly = true;
            this.colErrorEmail.Visible = true;
            this.colErrorEmail.VisibleIndex = 5;
            this.colErrorEmail.Width = 203;
            // 
            // colReportRecipients
            // 
            this.colReportRecipients.FieldName = "ReportRecipients";
            this.colReportRecipients.Name = "colReportRecipients";
            this.colReportRecipients.OptionsColumn.AllowEdit = false;
            this.colReportRecipients.OptionsColumn.AllowFocus = false;
            this.colReportRecipients.OptionsColumn.ReadOnly = true;
            this.colReportRecipients.Visible = true;
            this.colReportRecipients.VisibleIndex = 6;
            this.colReportRecipients.Width = 98;
            // 
            // colActiveReportRecipients
            // 
            this.colActiveReportRecipients.FieldName = "ActiveReportRecipients";
            this.colActiveReportRecipients.Name = "colActiveReportRecipients";
            this.colActiveReportRecipients.OptionsColumn.AllowEdit = false;
            this.colActiveReportRecipients.OptionsColumn.AllowFocus = false;
            this.colActiveReportRecipients.OptionsColumn.ReadOnly = true;
            this.colActiveReportRecipients.Visible = true;
            this.colActiveReportRecipients.VisibleIndex = 7;
            this.colActiveReportRecipients.Width = 129;
            // 
            // colRecipientsWithSuccessNotification
            // 
            this.colRecipientsWithSuccessNotification.FieldName = "RecipientsWithSuccessNotification";
            this.colRecipientsWithSuccessNotification.Name = "colRecipientsWithSuccessNotification";
            this.colRecipientsWithSuccessNotification.OptionsColumn.AllowEdit = false;
            this.colRecipientsWithSuccessNotification.OptionsColumn.AllowFocus = false;
            this.colRecipientsWithSuccessNotification.OptionsColumn.ReadOnly = true;
            this.colRecipientsWithSuccessNotification.Visible = true;
            this.colRecipientsWithSuccessNotification.VisibleIndex = 8;
            this.colRecipientsWithSuccessNotification.Width = 198;
            // 
            // colRecipientsWithFailureNotification
            // 
            this.colRecipientsWithFailureNotification.FieldName = "RecipientsWithFailureNotification";
            this.colRecipientsWithFailureNotification.Name = "colRecipientsWithFailureNotification";
            this.colRecipientsWithFailureNotification.OptionsColumn.AllowEdit = false;
            this.colRecipientsWithFailureNotification.OptionsColumn.AllowFocus = false;
            this.colRecipientsWithFailureNotification.OptionsColumn.ReadOnly = true;
            this.colRecipientsWithFailureNotification.Visible = true;
            this.colRecipientsWithFailureNotification.VisibleIndex = 9;
            this.colRecipientsWithFailureNotification.Width = 183;
            // 
            // colHasReportOwner
            // 
            this.colHasReportOwner.FieldName = "HasReportOwner";
            this.colHasReportOwner.Name = "colHasReportOwner";
            this.colHasReportOwner.OptionsColumn.AllowEdit = false;
            this.colHasReportOwner.OptionsColumn.AllowFocus = false;
            this.colHasReportOwner.OptionsColumn.ReadOnly = true;
            this.colHasReportOwner.Visible = true;
            this.colHasReportOwner.VisibleIndex = 10;
            this.colHasReportOwner.Width = 113;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // packageConfigurationChildTabControl
            // 
            this.packageConfigurationChildTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.packageConfigurationChildTabControl.Location = new System.Drawing.Point(0, 0);
            this.packageConfigurationChildTabControl.Name = "packageConfigurationChildTabControl";
            this.packageConfigurationChildTabControl.SelectedTabPage = this.emailListTabPage;
            this.packageConfigurationChildTabControl.Size = new System.Drawing.Size(1810, 183);
            this.packageConfigurationChildTabControl.TabIndex = 0;
            this.packageConfigurationChildTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.emailListTabPage});
            // 
            // emailListTabPage
            // 
            this.emailListTabPage.Controls.Add(this.gridSplitContainer2);
            this.emailListTabPage.Name = "emailListTabPage";
            this.emailListTabPage.Size = new System.Drawing.Size(1805, 157);
            this.emailListTabPage.Text = "Package Recipient Details";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.recipientListGridControl;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.recipientListGridControl);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1805, 157);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // recipientListGridControl
            // 
            this.recipientListGridControl.DataSource = this.spPC922703PackageEmailListItemBindingSource;
            this.recipientListGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.recipientListGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.recipientListGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.recipientListGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.recipientListGridControl.Location = new System.Drawing.Point(0, 0);
            this.recipientListGridControl.MainView = this.recipientListGridView;
            this.recipientListGridControl.MenuManager = this.barManager1;
            this.recipientListGridControl.Name = "recipientListGridControl";
            this.recipientListGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemMemoExEdit3});
            this.recipientListGridControl.Size = new System.Drawing.Size(1805, 157);
            this.recipientListGridControl.TabIndex = 0;
            this.recipientListGridControl.UseEmbeddedNavigator = true;
            this.recipientListGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.recipientListGridView});
            // 
            // spPC922703PackageEmailListItemBindingSource
            // 
            this.spPC922703PackageEmailListItemBindingSource.DataMember = "sp_PC_922703_Package_Email_List_Item";
            this.spPC922703PackageEmailListItemBindingSource.DataSource = this.dataSet_PC_Core;
            // 
            // recipientListGridView
            // 
            this.recipientListGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colServiceEmailListID,
            this.colJobPackageID1,
            this.colJobPackageDescription1,
            this.colActive,
            this.colUserTypeID,
            this.colUserType,
            this.colUserID,
            this.colRecipientName,
            this.colRecipientEmail,
            this.colNotifyFailure,
            this.colNotifySuccess,
            this.colLastSendDate,
            this.colReportLevel,
            this.colSendReport,
            this.colIsReportOwner,
            this.colMode1,
            this.colRecordID1});
            this.recipientListGridView.GridControl = this.recipientListGridControl;
            this.recipientListGridView.Name = "recipientListGridView";
            this.recipientListGridView.OptionsCustomization.AllowFilter = false;
            this.recipientListGridView.OptionsCustomization.AllowGroup = false;
            this.recipientListGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.recipientListGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.recipientListGridView.OptionsLayout.StoreAppearance = true;
            this.recipientListGridView.OptionsSelection.MultiSelect = true;
            this.recipientListGridView.OptionsView.ColumnAutoWidth = false;
            this.recipientListGridView.OptionsView.ShowGroupPanel = false;
            this.recipientListGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.recipientListGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.recipientListGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.recipientListGridView.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.recipientListGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.recipientListGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.recipientListGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colServiceEmailListID
            // 
            this.colServiceEmailListID.FieldName = "ServiceEmailListID";
            this.colServiceEmailListID.Name = "colServiceEmailListID";
            this.colServiceEmailListID.OptionsColumn.AllowEdit = false;
            this.colServiceEmailListID.OptionsColumn.AllowFocus = false;
            this.colServiceEmailListID.OptionsColumn.ReadOnly = true;
            // 
            // colJobPackageID1
            // 
            this.colJobPackageID1.FieldName = "JobPackageID";
            this.colJobPackageID1.Name = "colJobPackageID1";
            this.colJobPackageID1.OptionsColumn.AllowEdit = false;
            this.colJobPackageID1.OptionsColumn.AllowFocus = false;
            this.colJobPackageID1.OptionsColumn.ReadOnly = true;
            this.colJobPackageID1.Width = 133;
            // 
            // colJobPackageDescription1
            // 
            this.colJobPackageDescription1.FieldName = "JobPackageDescription";
            this.colJobPackageDescription1.Name = "colJobPackageDescription1";
            this.colJobPackageDescription1.OptionsColumn.AllowEdit = false;
            this.colJobPackageDescription1.OptionsColumn.AllowFocus = false;
            this.colJobPackageDescription1.OptionsColumn.ReadOnly = true;
            this.colJobPackageDescription1.Visible = true;
            this.colJobPackageDescription1.VisibleIndex = 0;
            this.colJobPackageDescription1.Width = 326;
            // 
            // colActive
            // 
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            // 
            // colUserTypeID
            // 
            this.colUserTypeID.FieldName = "UserTypeID";
            this.colUserTypeID.Name = "colUserTypeID";
            this.colUserTypeID.OptionsColumn.AllowEdit = false;
            this.colUserTypeID.OptionsColumn.AllowFocus = false;
            this.colUserTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colUserType
            // 
            this.colUserType.FieldName = "UserType";
            this.colUserType.Name = "colUserType";
            this.colUserType.OptionsColumn.AllowEdit = false;
            this.colUserType.OptionsColumn.AllowFocus = false;
            this.colUserType.OptionsColumn.ReadOnly = true;
            this.colUserType.Visible = true;
            this.colUserType.VisibleIndex = 2;
            this.colUserType.Width = 125;
            // 
            // colUserID
            // 
            this.colUserID.FieldName = "UserID";
            this.colUserID.Name = "colUserID";
            this.colUserID.OptionsColumn.AllowEdit = false;
            this.colUserID.OptionsColumn.AllowFocus = false;
            this.colUserID.OptionsColumn.ReadOnly = true;
            // 
            // colRecipientName
            // 
            this.colRecipientName.FieldName = "RecipientName";
            this.colRecipientName.Name = "colRecipientName";
            this.colRecipientName.OptionsColumn.AllowEdit = false;
            this.colRecipientName.OptionsColumn.AllowFocus = false;
            this.colRecipientName.OptionsColumn.ReadOnly = true;
            this.colRecipientName.Visible = true;
            this.colRecipientName.VisibleIndex = 3;
            this.colRecipientName.Width = 250;
            // 
            // colRecipientEmail
            // 
            this.colRecipientEmail.FieldName = "RecipientEmail";
            this.colRecipientEmail.Name = "colRecipientEmail";
            this.colRecipientEmail.OptionsColumn.AllowEdit = false;
            this.colRecipientEmail.OptionsColumn.AllowFocus = false;
            this.colRecipientEmail.OptionsColumn.ReadOnly = true;
            this.colRecipientEmail.Visible = true;
            this.colRecipientEmail.VisibleIndex = 4;
            this.colRecipientEmail.Width = 306;
            // 
            // colNotifyFailure
            // 
            this.colNotifyFailure.FieldName = "NotifyFailure";
            this.colNotifyFailure.Name = "colNotifyFailure";
            this.colNotifyFailure.OptionsColumn.AllowEdit = false;
            this.colNotifyFailure.OptionsColumn.AllowFocus = false;
            this.colNotifyFailure.OptionsColumn.ReadOnly = true;
            this.colNotifyFailure.Visible = true;
            this.colNotifyFailure.VisibleIndex = 5;
            // 
            // colNotifySuccess
            // 
            this.colNotifySuccess.FieldName = "NotifySuccess";
            this.colNotifySuccess.Name = "colNotifySuccess";
            this.colNotifySuccess.OptionsColumn.AllowEdit = false;
            this.colNotifySuccess.OptionsColumn.AllowFocus = false;
            this.colNotifySuccess.OptionsColumn.ReadOnly = true;
            this.colNotifySuccess.Visible = true;
            this.colNotifySuccess.VisibleIndex = 6;
            this.colNotifySuccess.Width = 88;
            // 
            // colLastSendDate
            // 
            this.colLastSendDate.FieldName = "LastSendDate";
            this.colLastSendDate.Name = "colLastSendDate";
            this.colLastSendDate.OptionsColumn.AllowEdit = false;
            this.colLastSendDate.OptionsColumn.AllowFocus = false;
            this.colLastSendDate.OptionsColumn.ReadOnly = true;
            this.colLastSendDate.Visible = true;
            this.colLastSendDate.VisibleIndex = 7;
            this.colLastSendDate.Width = 92;
            // 
            // colReportLevel
            // 
            this.colReportLevel.FieldName = "ReportLevel";
            this.colReportLevel.Name = "colReportLevel";
            this.colReportLevel.OptionsColumn.AllowEdit = false;
            this.colReportLevel.OptionsColumn.AllowFocus = false;
            this.colReportLevel.OptionsColumn.ReadOnly = true;
            this.colReportLevel.Visible = true;
            this.colReportLevel.VisibleIndex = 8;
            // 
            // colSendReport
            // 
            this.colSendReport.FieldName = "SendReport";
            this.colSendReport.Name = "colSendReport";
            this.colSendReport.OptionsColumn.AllowEdit = false;
            this.colSendReport.OptionsColumn.AllowFocus = false;
            this.colSendReport.OptionsColumn.ReadOnly = true;
            this.colSendReport.Visible = true;
            this.colSendReport.VisibleIndex = 9;
            // 
            // colIsReportOwner
            // 
            this.colIsReportOwner.FieldName = "IsReportOwner";
            this.colIsReportOwner.Name = "colIsReportOwner";
            this.colIsReportOwner.OptionsColumn.AllowEdit = false;
            this.colIsReportOwner.OptionsColumn.AllowFocus = false;
            this.colIsReportOwner.OptionsColumn.ReadOnly = true;
            this.colIsReportOwner.Visible = true;
            this.colIsReportOwner.VisibleIndex = 10;
            this.colIsReportOwner.Width = 100;
            // 
            // colMode1
            // 
            this.colMode1.FieldName = "Mode";
            this.colMode1.Name = "colMode1";
            this.colMode1.OptionsColumn.AllowEdit = false;
            this.colMode1.OptionsColumn.AllowFocus = false;
            this.colMode1.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID1
            // 
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.AllowEdit = false;
            this.colRecordID1.OptionsColumn.AllowFocus = false;
            this.colRecordID1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ReadOnly = true;
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ReadOnly = true;
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.Connection = null;
            this.tableAdapterManager1.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_PC_922701_Package_Configuration_ItemTableAdapter
            // 
            this.sp_PC_922701_Package_Configuration_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_PC_922703_Package_Email_List_ItemTableAdapter
            // 
            this.sp_PC_922703_Package_Email_List_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spPC922709ReturnServiceEmailListIDBindingSource
            // 
            this.spPC922709ReturnServiceEmailListIDBindingSource.DataMember = "sp_PC_922709_Return_ServiceEmailListID";
            this.spPC922709ReturnServiceEmailListIDBindingSource.DataSource = this.dataSet_PC_Core;
            // 
            // sp_PC_922709_Return_ServiceEmailListIDTableAdapter
            // 
            this.sp_PC_922709_Return_ServiceEmailListIDTableAdapter.ClearBeforeFill = true;
            // 
            // spPC922710ReturnJobPackageIDBindingSource
            // 
            this.spPC922710ReturnJobPackageIDBindingSource.DataMember = "sp_PC_922710_Return_JobPackageID";
            this.spPC922710ReturnJobPackageIDBindingSource.DataSource = this.dataSet_PC_Core;
            // 
            // sp_PC_922710_Return_JobPackageIDTableAdapter
            // 
            this.sp_PC_922710_Return_JobPackageIDTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Package_Configuration_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1810, 740);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Package_Configuration_Manager";
            this.Text = "Package Configuration Manager";
            this.Activated += new System.EventHandler(this.frm_Core_Package_Configuration_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_Core_Package_Configuration_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.packageConfigurationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922701PackageConfigurationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_PC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageConfigurationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageConfigurationChildTabControl)).EndInit();
            this.packageConfigurationChildTabControl.ResumeLayout(false);
            this.emailListTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recipientListGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922703PackageEmailListItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recipientListGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922709ReturnServiceEmailListIDBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922710ReturnJobPackageIDBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl packageConfigurationChildTabControl;
        private DevExpress.XtraTab.XtraTabPage emailListTabPage;
        private DevExpress.XtraGrid.GridControl recipientListGridControl;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_AS_CoreTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraGrid.Views.Grid.GridView recipientListGridView;
        private DevExpress.XtraGrid.GridControl packageConfigurationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView packageConfigurationGridView;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_AT dataSet_AT;
        private DataSet_PC_Core dataSet_PC_Core;
        private System.Windows.Forms.BindingSource spPC922701PackageConfigurationItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colJobPackageID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobPackageDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailSubjectLine;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colDateLastRun;
        private DevExpress.XtraGrid.Columns.GridColumn colIsActive;
        private DevExpress.XtraGrid.Columns.GridColumn colErrorEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colReportRecipients;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveReportRecipients;
        private DevExpress.XtraGrid.Columns.GridColumn colRecipientsWithSuccessNotification;
        private DevExpress.XtraGrid.Columns.GridColumn colRecipientsWithFailureNotification;
        private DevExpress.XtraGrid.Columns.GridColumn colHasReportOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DataSet_PC_CoreTableAdapters.sp_PC_922701_Package_Configuration_ItemTableAdapter sp_PC_922701_Package_Configuration_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spPC922703PackageEmailListItemBindingSource;
        private DataSet_PC_CoreTableAdapters.sp_PC_922703_Package_Email_List_ItemTableAdapter sp_PC_922703_Package_Email_List_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spPC922709ReturnServiceEmailListIDBindingSource;
        private DataSet_PC_CoreTableAdapters.sp_PC_922709_Return_ServiceEmailListIDTableAdapter sp_PC_922709_Return_ServiceEmailListIDTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceEmailListID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobPackageID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobPackageDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colUserTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType;
        private DevExpress.XtraGrid.Columns.GridColumn colUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecipientName;
        private DevExpress.XtraGrid.Columns.GridColumn colRecipientEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colNotifyFailure;
        private DevExpress.XtraGrid.Columns.GridColumn colNotifySuccess;
        private DevExpress.XtraGrid.Columns.GridColumn colLastSendDate;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSendReport;
        private DevExpress.XtraGrid.Columns.GridColumn colIsReportOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private System.Windows.Forms.BindingSource spPC922710ReturnJobPackageIDBindingSource;
        private DataSet_PC_CoreTableAdapters.sp_PC_922710_Return_JobPackageIDTableAdapter sp_PC_922710_Return_JobPackageIDTableAdapter;
    }
}

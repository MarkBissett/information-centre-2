namespace WoodPlan5
{
    /// <summary>
    /// 
    /// </summary>
    partial class frmSystemSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSystemSettings));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.colReportLayoutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.vGridControl1 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.sp00045GetSystemSettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sp00127systemsettingsshowothertreedetailslistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sp00128systemsettingsdataentryofflistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sp00129systemsettingsliveperiodlistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp01239ATWorkOrdersPrintScaleListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colScaleDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemColorEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageListLineStyles = new System.Windows.Forms.ImageList(this.components);
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp01277ATDefaultWorkOrderLayoutListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp01300ATWorkOrderMapPageSizesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit3View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPageHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPageSizeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPageSizeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPageSizeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPageWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00147systemsettingsscreenpositionlistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit4View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScreenPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sp00148systemsettingsmapgenerationmethodslistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemColorEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemColorEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.repositoryItemSpinEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlTreeRefStructure = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnSetTreeRefStruc = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEditTreeRefRemovePreceedingChars = new DevExpress.XtraEditors.TextEdit();
            this.spinEditTreeRefRemoveNumberOfChars = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditTreeRef1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTreeRef3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTreeRef2 = new DevExpress.XtraEditors.CheckEdit();
            this.repositoryItemGridLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00149systemsettingsmappingprojectionslistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit5View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colProjectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjectionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjectionOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProjectionText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sp00150systemsettingsdatatransfermodelistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00023SystemSettingsRateCriteriaListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit6View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemGridLookUpEditDefaultCompanyHeader = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit7View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrSlogan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditDefaultContractor = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00190ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.ManagerLoadDataOnOpenRepositoryItemComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemButtonEditGetFileName = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemSpinEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemColorEditSpreadsheetRowBackColour = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemTextEditNumericOnly = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEditCalloutPicturesExternal = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemSpinEditDataSyncMachineID = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemSpinEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemGridLookUpEditTextDirections = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00236ReportWatermarkTextDirectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemFontEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            this.repositoryItemSpinEditWatermarkTextSize = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemTrackBarWatermarkTransparency = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.repositoryItemGridLookUpEditWatermarkImageAlign = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00237ReportWatermarkImageAlignmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditViewMode = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00238ReportWatermarkImageViewModeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowDataEntryOff = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLivePeriod = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLiveDBName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSystemDataTransferMode = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesLinkedDocumentsPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGroundControlContractorCertificatesPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow5 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowStaffImagePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowStaffImageThumbPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDefaultPhoto = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryWatermark = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowWatermarkShowBehind = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkTextPageRange = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryTextWatermark = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowWatermarkText = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkTextDirection = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkTextFont = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkTextFontSize = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkTextFontBold = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkTextFontItalic = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkTextColour = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkTextTransparency = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryImageWatermark = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowWatermarkImage = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkImageAlign = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkImageTiling = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkImageViewMode = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWatermarkImageTransparency = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow2 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesPictureFilesFolder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow23 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesJobRateCriteriaID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesJobDiscountRateCriteriaID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow6 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesWorkOrderLayoutLocation = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderDefaultLayoutID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesDefaultContractor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesDefaultCompanyHeader = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow8 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesWorkOrderMapGenerationMethod = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderMapTicked = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderPrintShowDetailsOfOtherTrees = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderMapTreeHighlightColour = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderMapLocation = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderDefaultPaperSize = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderDefaultLegendPosition = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderDefaultNorthArrowPosition = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderDefaultScaleBarPosition = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderOSCopyright = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderOSCopyrightDefaultPosition = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesWorkOrderOSCopyrightWidth = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow16 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesWorkOrderDefaultMapScale = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow4 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesMapTreeFilesFolder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesMapBackgroundFolder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesMapBackgroundFolderTablet = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesDefaultMappingProjection = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow3 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesReportLayoutLocation = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow21 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.categoryRow22 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMobileInterfaceExportFolder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMobileInterfaceImportFolder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow7 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.categoryRow9 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesExportToGBM_Background_Mapping_Folder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesExportToGBM_Geoset_Folder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesExportToGBM_Profile_Folder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesExportToGBM_GBM_Mobile_XML_Folder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesExportToGBM_Tab_Folder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesExportToGBM_Mid_Mif_Folder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesExportToGBM_Pda_Reference_Data_Folder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow10 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesImportFromGBM_Default_Tab_Folder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow13 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesVersion4WoodPlan_Incident_Link_Exe = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTrees_TreePicker_Polygon_Area = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTrees_TreePicker_Default_Hedge_Width = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMappingShowAmenityTreeObjects = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMappingShowEstatePlanObjects = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow20 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTrees_TreePicker_Tree_Ref_Format_Used = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow19 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTrees_TreePicker_Thematic_Band_Width = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow17 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTrees_TreePicker_GPS_CrossHair_Colour = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow12 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow18 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow14 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesTreeManagerLoadTreesOnOpen = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow15 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTrees_Sequence_From_Locality_Length = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTrees_Sequence_From_Locality_Seperator = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow24 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesCavatCalculation = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesCavatCalculationMethod = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAmenityTreesCavatDefaultUnitValueFactor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow25 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesInspectionManagerLoadInspectionsOnOpen = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow26 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowAmenityTreesActionManagerLoadActionsOnOpen = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryUtilities = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowDataSynchronisationMachineID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSavedReferenceFiles = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUtilitiesSavedReportLayouts = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSavedMaps = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSavedPictures = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSavedSignatures = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryPermissionDocuments = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowSavedPermissionDocuments = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowPermissionDocumentEmailFromAddressClient = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPermissionDocumentEmailFromAddressCustomer = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryEmalCCAddress = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowPermissionDocumentEmailCCAddressClient = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPermissionDocumentEmailCCAddressCustomer = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryEmailLayoutHTMLFile = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowPermissionDocumentEmailHTMLLayoutFileClient = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPermissionDocumentEmailHTMLLayoutFileCustomer = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryEmailSubjectLine = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowPermissionDocumentEmailSubjectLineClient = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPermissionDocumentEmailSubjectLineCustomer = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryWorkOrderDocuments = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowSavedWorkOrderDocuments = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWorkOrderDocumentEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWorkOrderDocumentEmailCCAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWorkOrderDocumentEmailHTMLLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowWorkOrderDocumentEmailSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowInformationCentre = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.categoryRow28 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowEstatePlanInspectionManagerLoadInspectionsOnOpen = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow29 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowEstatePlanAssetManagerLoadAssetsOnOpen = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow30 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowEstatePlanActionManagerLoadActionsOnOpen = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow31 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGroundControlSiteDrawingsPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowWinterMaintenance = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowSMTPMailServerAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSMTPMailServerUsername = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSMTPMailServerPassword = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSMTPMailServerPort = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTextAnywhereUsername = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTextAnywherePassword = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingDefaultVatRate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSaltStockControlPeriodID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingCalloutPictureFilesFolder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingCalloutPictureFilesFolderInternal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingSavedCalloutSignatureFolder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingReportLayouts = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowForecastImport = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGrittingForecastType1FilePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingForecastType2FilePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingForecastTypeFloatingFilePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingFloatingJobsFilePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryFloatingSiteForecastEmail = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGrittingFloatingSiteEmailHtmlLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingFloatingSiteEmailSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingFloatingSiteEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingFloatingSiteEmailCCToEmailAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowGrittingEmails = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGrittingEmailsFromName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingConfirmationCCToEmailAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingTeamConfirmationCCToEmailAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowClientAuthorisationEmail = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGrittingConfirmationSentPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingConfirmationReceivedPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingAuthorisationSentHtmlLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingClientEmailAuthorisationSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingAuthorisationGeneratedFileType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingAuthorisationOddRowColour = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingAuthorisationEvenRowColour = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowGrittingConfirmations = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGrittingConfirmationSentHtmlLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingClientEmailDailyReportSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowTeamEmailReport = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGrittingTeamReportHtmlLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTeamEmailReportSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingTeamReportEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRouteOptimization = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGrittingRouteOptimisationURL = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingRouteOptimisationUsername = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingRouteOptimisationPassword = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingRouteOptimisationIniFileSavePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingRouteOptimisationJobFileSavePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingRouteOptimisationOptimisedRouteSavePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryGrittingCompletedEmails = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGrittingCompletionEmailHtmlLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingCompletionEmailFromEmailAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingCompletionEmailCCToEmailAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingCompletionEmailSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowSelfBillingInvoices = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowGrittingSelfBillingInvoicePDFPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingSelfBillingInvoiceEmailHTMLLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingSelfBillingInvoiceEmailSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingSelfBillingInvoiceEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowGrittingSelfBillingInvoiceEmailCCFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowSnowClearanceTeamPO = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowSnowClearanceTeamPOPDFPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSnowClearanceTeamPOEmailHTMLLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSnowClearanceTeamPOEmailSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSnowClearanceTeamPOEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSnowClearanceTeamPOEmailCCAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowFinance = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowFinanceClientInvoiceSSDefaultSavePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFinanceTeamCostsSSDefaultSavePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFinanceTeamCostSSGrittingAnalysisCode = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFinanceTeamCostSSSnowClearanceAnalysisCode = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFinanceTeamCostSSGrittingCostCentre = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFinanceTeamCostSSSnowClearanceCostCentre = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowClientJobBreakdownReport = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowClientJobBreakdownReportPDFPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowClientJobBreakdownReportHTMLLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowClientJobBreakdownReportEmailSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowClientJobBreakdownReportEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowClientJobBreakdownReportEmailCCAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowMissingJobSheetNumbers = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMissingJobNumberSheetSavePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMissingJobNumberSheetSS_HTMLLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMissingJobNumberSheetPDF_HTMLLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMissingJobNumberSheetSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMissingJobNumberSheetEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMissingJobNumberSheetEmailCCAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMissingJobNumberSheetLoadPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryTenderRegister = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowTenderReportLayouts = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowTenderStaekholderEmail = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowTenderStakeholderEmailPDFPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTenderStakeholderEmailHTMLLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTenderStakeholderEmailSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTenderStakeholderEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTenderStakeholderEmailCCAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRowExtraWorksQuotationContractManagerEmail = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowExtraWorkQuotationEmailPDFPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowExtraWorkQuotationEmailHTMLLayoutFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowExtraWorkQuotationEmailSubjectLine = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowExtraWorkQuotationEmailFromAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowExtraWorkQuotationEmailCCAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categorySummerMainteance = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.categoryWorkPermits = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowArchivaWorkPermitFile = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryHR = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowHR_QualificationCertificatePath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowHR_LinkedDocumentPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowHR_PayrollReportExportSavedFolder = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtpSettingsPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.sp00045_GetSystemSettingsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00045_GetSystemSettingsTableAdapter();
            this.sp00127_system_settings_show_other_tree_details_listTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00127_system_settings_show_other_tree_details_listTableAdapter();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sp00128_system_settings_data_entry_off_listTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00128_system_settings_data_entry_off_listTableAdapter();
            this.sp00129_system_settings_live_period_listTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00129_system_settings_live_period_listTableAdapter();
            this.sp01239_AT_WorkOrders_Print_Scale_ListTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01239_AT_WorkOrders_Print_Scale_ListTableAdapter();
            this.categoryRow11 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.sp01277_AT_Default_WorkOrder_Layout_ListTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp01277_AT_Default_WorkOrder_Layout_ListTableAdapter();
            this.sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter();
            this.sp00147_system_settings_screen_position_listTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00147_system_settings_screen_position_listTableAdapter();
            this.sp00148_system_settings_map_generation_methods_listTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00148_system_settings_map_generation_methods_listTableAdapter();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp00149_system_settings_mapping_projections_listTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00149_system_settings_mapping_projections_listTableAdapter();
            this.sp00150_system_settings_data_transfer_mode_listTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00150_system_settings_data_transfer_mode_listTableAdapter();
            this.sp00023_System_Settings_Rate_Criteria_ListTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00023_System_Settings_Rate_Criteria_ListTableAdapter();
            this.sp00190_Contractor_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter();
            this.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter();
            this.sp00236_Report_Watermark_Text_DirectionsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00236_Report_Watermark_Text_DirectionsTableAdapter();
            this.sp00237_Report_Watermark_Image_AlignmentTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00237_Report_Watermark_Image_AlignmentTableAdapter();
            this.sp00238_Report_Watermark_Image_ViewModeTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00238_Report_Watermark_Image_ViewModeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00045GetSystemSettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00127systemsettingsshowothertreedetailslistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00128systemsettingsdataentryofflistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00129systemsettingsliveperiodlistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01239ATWorkOrdersPrintScaleListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01277ATDefaultWorkOrderLayoutListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01300ATWorkOrderMapPageSizesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00147systemsettingsscreenpositionlistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00148systemsettingsmapgenerationmethodslistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTreeRefStructure)).BeginInit();
            this.popupContainerControlTreeRefStructure.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTreeRefRemovePreceedingChars.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTreeRefRemoveNumberOfChars.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00149systemsettingsmappingprojectionslistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00150systemsettingsdatatransfermodelistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00023SystemSettingsRateCriteriaListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit6View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDefaultCompanyHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00232CompanyHeaderDropDownListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit7View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDefaultContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerLoadDataOnOpenRepositoryItemComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditGetFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEditSpreadsheetRowBackColour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCalloutPicturesExternal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditDataSyncMachineID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditTextDirections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00236ReportWatermarkTextDirectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditWatermarkTextSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBarWatermarkTransparency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditWatermarkImageAlign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00237ReportWatermarkImageAlignmentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditViewMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00238ReportWatermarkImageViewModeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtpSettingsPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(729, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 546);
            this.barDockControlBottom.Size = new System.Drawing.Size(729, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 546);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(729, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 546);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colReportLayoutID
            // 
            this.colReportLayoutID.Caption = "Layout ID";
            this.colReportLayoutID.FieldName = "ReportLayoutID";
            this.colReportLayoutID.Name = "colReportLayoutID";
            // 
            // colintHeaderID
            // 
            this.colintHeaderID.Caption = "Header ID";
            this.colintHeaderID.FieldName = "intHeaderID";
            this.colintHeaderID.Name = "colintHeaderID";
            this.colintHeaderID.OptionsColumn.AllowEdit = false;
            this.colintHeaderID.OptionsColumn.AllowFocus = false;
            this.colintHeaderID.OptionsColumn.ReadOnly = true;
            this.colintHeaderID.Width = 70;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Contractor ID";
            this.gridColumn7.FieldName = "ContractorID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 87;
            // 
            // vGridControl1
            // 
            this.vGridControl1.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.vGridControl1.DataSource = this.sp00045GetSystemSettingsBindingSource;
            this.vGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridControl1.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.vGridControl1.Location = new System.Drawing.Point(0, 0);
            this.vGridControl1.MenuManager = this.barManager1;
            this.vGridControl1.Name = "vGridControl1";
            this.vGridControl1.OptionsView.AutoScaleBands = true;
            this.vGridControl1.OptionsView.FixRowHeaderPanelWidth = true;
            this.vGridControl1.RecordWidth = 97;
            this.vGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemButtonEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemLookUpEdit3,
            this.repositoryItemLookUpEdit4,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemTextEdit3,
            this.repositoryItemColorEdit1,
            this.repositoryItemColorEdit2,
            this.repositoryItemSpinEdit1,
            this.repositoryItemComboBox2,
            this.repositoryItemImageComboBox1,
            this.repositoryItemSpinEdit2,
            this.repositoryItemTextEdit4,
            this.repositoryItemGridLookUpEdit2,
            this.repositoryItemGridLookUpEdit3,
            this.repositoryItemGridLookUpEdit4,
            this.repositoryItemTextEdit5,
            this.repositoryItemLookUpEdit5,
            this.repositoryItemColorEdit3,
            this.repositoryItemCheckEdit2,
            this.repositoryItemSpinEdit3,
            this.repositoryItemColorEdit4,
            this.repositoryItemTrackBar1,
            this.repositoryItemSpinEdit4,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemGridLookUpEdit5,
            this.repositoryItemLookUpEdit6,
            this.repositoryItemGridLookUpEdit6,
            this.repositoryItemSpinEdit5,
            this.repositoryItemGridLookUpEditDefaultCompanyHeader,
            this.repositoryItemGridLookUpEditDefaultContractor,
            this.repositoryItemTextEdit6,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox3,
            this.ManagerLoadDataOnOpenRepositoryItemComboBox,
            this.repositoryItemButtonEditGetFileName,
            this.repositoryItemSpinEdit6,
            this.repositoryItemColorEditSpreadsheetRowBackColour,
            this.repositoryItemComboBox4,
            this.repositoryItemTextEditNumericOnly,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditCalloutPicturesExternal,
            this.repositoryItemSpinEditDataSyncMachineID,
            this.repositoryItemSpinEdit7,
            this.repositoryItemGridLookUpEditTextDirections,
            this.repositoryItemFontEdit1,
            this.repositoryItemSpinEditWatermarkTextSize,
            this.repositoryItemTrackBarWatermarkTransparency,
            this.repositoryItemGridLookUpEditWatermarkImageAlign,
            this.repositoryItemGridLookUpEditViewMode});
            this.vGridControl1.RowHeaderWidth = 103;
            this.vGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow1,
            this.categoryRow2,
            this.categoryUtilities,
            this.categoryRowInformationCentre,
            this.categoryHR});
            this.vGridControl1.Size = new System.Drawing.Size(720, 485);
            this.vGridControl1.TabIndex = 0;
            // 
            // sp00045GetSystemSettingsBindingSource
            // 
            this.sp00045GetSystemSettingsBindingSource.DataMember = "sp00045_GetSystemSettings";
            this.sp00045GetSystemSettingsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanManDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Value", 49, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Display", "Value", 44, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Order", "Order", 38, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEdit1.DataSource = this.sp00127systemsettingsshowothertreedetailslistBindingSource;
            this.repositoryItemLookUpEdit1.DisplayMember = "Display";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "";
            this.repositoryItemLookUpEdit1.ShowLines = false;
            this.repositoryItemLookUpEdit1.ValueMember = "Value";
            // 
            // sp00127systemsettingsshowothertreedetailslistBindingSource
            // 
            this.sp00127systemsettingsshowothertreedetailslistBindingSource.DataMember = "sp00127_system_settings_show_other_tree_details_list";
            this.sp00127systemsettingsshowothertreedetailslistBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.EnableTransparency = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Select Folder - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to open the Windows Folder Browser screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose Folder", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 100;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.MaxLength = 100;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Value", 49, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Display", "Value", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Order", "Order", 38, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEdit3.DataSource = this.sp00128systemsettingsdataentryofflistBindingSource;
            this.repositoryItemLookUpEdit3.DisplayMember = "Display";
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            this.repositoryItemLookUpEdit3.NullText = "";
            this.repositoryItemLookUpEdit3.ShowLines = false;
            this.repositoryItemLookUpEdit3.ValueMember = "Value";
            // 
            // sp00128systemsettingsdataentryofflistBindingSource
            // 
            this.sp00128systemsettingsdataentryofflistBindingSource.DataMember = "sp00128_system_settings_data_entry_off_list";
            this.sp00128systemsettingsdataentryofflistBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemLookUpEdit4
            // 
            this.repositoryItemLookUpEdit4.AutoHeight = false;
            this.repositoryItemLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit4.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PeriodDescription", "Period Description", 109, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ConcatenatedDates", "Dates Range", 109, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("dtFromDate", "dt From Date", 73, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("dtToDate", "dt To Date", 61, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("strPeriodID", "str Period ID", 70, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEdit4.DataSource = this.sp00129systemsettingsliveperiodlistBindingSource;
            this.repositoryItemLookUpEdit4.DisplayMember = "PeriodDescription";
            this.repositoryItemLookUpEdit4.Name = "repositoryItemLookUpEdit4";
            this.repositoryItemLookUpEdit4.NullText = "";
            this.repositoryItemLookUpEdit4.ShowLines = false;
            this.repositoryItemLookUpEdit4.ValueMember = "strPeriodID";
            // 
            // sp00129systemsettingsliveperiodlistBindingSource
            // 
            this.sp00129systemsettingsliveperiodlistBindingSource.DataMember = "sp00129_system_settings_live_period_list";
            this.sp00129systemsettingsliveperiodlistBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DataSource = this.sp01239ATWorkOrdersPrintScaleListBindingSource;
            this.repositoryItemGridLookUpEdit1.DisplayMember = "ScaleDescription";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEdit1.ValueMember = "Scale";
            // 
            // sp01239ATWorkOrdersPrintScaleListBindingSource
            // 
            this.sp01239ATWorkOrdersPrintScaleListBindingSource.DataMember = "sp01239_AT_WorkOrders_Print_Scale_List";
            this.sp01239ATWorkOrdersPrintScaleListBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colScaleDescription,
            this.colScale});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsBehavior.Editable = false;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colScale, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colScaleDescription
            // 
            this.colScaleDescription.Caption = "Scale";
            this.colScaleDescription.FieldName = "ScaleDescription";
            this.colScaleDescription.Name = "colScaleDescription";
            this.colScaleDescription.OptionsColumn.AllowEdit = false;
            this.colScaleDescription.OptionsColumn.ReadOnly = true;
            this.colScaleDescription.Visible = true;
            this.colScaleDescription.VisibleIndex = 0;
            // 
            // colScale
            // 
            this.colScale.Caption = "Scale Value";
            this.colScale.FieldName = "Scale";
            this.colScale.Name = "colScale";
            this.colScale.OptionsColumn.AllowEdit = false;
            this.colScale.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.MaxLength = 1000;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            this.repositoryItemTextEdit3.Validating += new System.ComponentModel.CancelEventHandler(this.repositoryItemTextEdit3_Validating);
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            this.repositoryItemColorEdit1.StoreColorAsInteger = true;
            // 
            // repositoryItemColorEdit2
            // 
            this.repositoryItemColorEdit2.AutoHeight = false;
            this.repositoryItemColorEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit2.Name = "repositoryItemColorEdit2";
            this.repositoryItemColorEdit2.StoreColorAsInteger = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            "M�",
            "Hectares",
            "Acres"});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Solid Line", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dashed", 6, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dotted", 10, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dot Dashed", 41, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Arrowed", 54, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Double", 63, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triple", 64, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Thick Dashed", 73, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Thick Squares", 93, 8)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.imageListLineStyles;
            // 
            // imageListLineStyles
            // 
            this.imageListLineStyles.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLineStyles.ImageStream")));
            this.imageListLineStyles.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLineStyles.Images.SetKeyName(0, "line_02.png");
            this.imageListLineStyles.Images.SetKeyName(1, "line_06.png");
            this.imageListLineStyles.Images.SetKeyName(2, "line_10.png");
            this.imageListLineStyles.Images.SetKeyName(3, "line_41.png");
            this.imageListLineStyles.Images.SetKeyName(4, "line_54.png");
            this.imageListLineStyles.Images.SetKeyName(5, "line_63.png");
            this.imageListLineStyles.Images.SetKeyName(6, "line_64.png");
            this.imageListLineStyles.Images.SetKeyName(7, "line_73.png");
            this.imageListLineStyles.Images.SetKeyName(8, "line_93.png");
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.IsFloatValue = false;
            this.repositoryItemSpinEdit2.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.MaxLength = 1;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DataSource = this.sp01277ATDefaultWorkOrderLayoutListBindingSource;
            this.repositoryItemGridLookUpEdit2.DisplayMember = "ReportLayoutName";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.NullText = "";
            this.repositoryItemGridLookUpEdit2.PopupView = this.repositoryItemGridLookUpEdit2View;
            this.repositoryItemGridLookUpEdit2.ValueMember = "ReportLayoutID";
            // 
            // sp01277ATDefaultWorkOrderLayoutListBindingSource
            // 
            this.sp01277ATDefaultWorkOrderLayoutListBindingSource.DataMember = "sp01277_AT_Default_WorkOrder_Layout_List";
            this.sp01277ATDefaultWorkOrderLayoutListBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCreatedBy,
            this.colDummyGroup,
            this.colModuleID,
            this.colReportLayoutID,
            this.colReportLayoutName,
            this.colReportTypeName});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colReportLayoutID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.repositoryItemGridLookUpEdit2View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsBehavior.Editable = false;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDummyGroup, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReportLayoutName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colDummyGroup
            // 
            this.colDummyGroup.Caption = "Dummy Group";
            this.colDummyGroup.FieldName = "DummyGroup";
            this.colDummyGroup.Name = "colDummyGroup";
            this.colDummyGroup.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.Visible = true;
            this.colReportLayoutName.VisibleIndex = 0;
            this.colReportLayoutName.Width = 408;
            // 
            // colReportTypeName
            // 
            this.colReportTypeName.Caption = "Layout Type";
            this.colReportTypeName.FieldName = "ReportTypeName";
            this.colReportTypeName.Name = "colReportTypeName";
            this.colReportTypeName.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // repositoryItemGridLookUpEdit3
            // 
            this.repositoryItemGridLookUpEdit3.AutoHeight = false;
            this.repositoryItemGridLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit3.DataSource = this.sp01300ATWorkOrderMapPageSizesBindingSource;
            this.repositoryItemGridLookUpEdit3.DisplayMember = "PageSizeDescription";
            this.repositoryItemGridLookUpEdit3.Name = "repositoryItemGridLookUpEdit3";
            this.repositoryItemGridLookUpEdit3.NullText = "";
            this.repositoryItemGridLookUpEdit3.PopupView = this.repositoryItemGridLookUpEdit3View;
            this.repositoryItemGridLookUpEdit3.ValueMember = "PageSizeID";
            // 
            // sp01300ATWorkOrderMapPageSizesBindingSource
            // 
            this.sp01300ATWorkOrderMapPageSizesBindingSource.DataMember = "sp01300_AT_WorkOrder_Map_Page_Sizes";
            this.sp01300ATWorkOrderMapPageSizesBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemGridLookUpEdit3View
            // 
            this.repositoryItemGridLookUpEdit3View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPageHeight,
            this.colPageSizeDescription,
            this.colPageSizeID,
            this.colPageSizeOrder,
            this.colPageWidth});
            this.repositoryItemGridLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit3View.Name = "repositoryItemGridLookUpEdit3View";
            this.repositoryItemGridLookUpEdit3View.OptionsBehavior.Editable = false;
            this.repositoryItemGridLookUpEdit3View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit3View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit3View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit3View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit3View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit3View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPageSizeOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colPageHeight
            // 
            this.colPageHeight.Caption = "Height";
            this.colPageHeight.FieldName = "PageHeight";
            this.colPageHeight.Name = "colPageHeight";
            this.colPageHeight.Visible = true;
            this.colPageHeight.VisibleIndex = 2;
            this.colPageHeight.Width = 52;
            // 
            // colPageSizeDescription
            // 
            this.colPageSizeDescription.Caption = "Size Description";
            this.colPageSizeDescription.FieldName = "PageSizeDescription";
            this.colPageSizeDescription.Name = "colPageSizeDescription";
            this.colPageSizeDescription.Visible = true;
            this.colPageSizeDescription.VisibleIndex = 0;
            this.colPageSizeDescription.Width = 326;
            // 
            // colPageSizeID
            // 
            this.colPageSizeID.Caption = "Size ID";
            this.colPageSizeID.FieldName = "PageSizeID";
            this.colPageSizeID.Name = "colPageSizeID";
            this.colPageSizeID.Width = 44;
            // 
            // colPageSizeOrder
            // 
            this.colPageSizeOrder.Caption = "Order";
            this.colPageSizeOrder.FieldName = "PageSizeOrder";
            this.colPageSizeOrder.Name = "colPageSizeOrder";
            this.colPageSizeOrder.Width = 62;
            // 
            // colPageWidth
            // 
            this.colPageWidth.Caption = "Width";
            this.colPageWidth.FieldName = "PageWidth";
            this.colPageWidth.Name = "colPageWidth";
            this.colPageWidth.Visible = true;
            this.colPageWidth.VisibleIndex = 1;
            this.colPageWidth.Width = 49;
            // 
            // repositoryItemGridLookUpEdit4
            // 
            this.repositoryItemGridLookUpEdit4.AutoHeight = false;
            this.repositoryItemGridLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit4.DataSource = this.sp00147systemsettingsscreenpositionlistBindingSource;
            this.repositoryItemGridLookUpEdit4.DisplayMember = "ScreenPosition";
            this.repositoryItemGridLookUpEdit4.Name = "repositoryItemGridLookUpEdit4";
            this.repositoryItemGridLookUpEdit4.NullText = "";
            this.repositoryItemGridLookUpEdit4.PopupView = this.repositoryItemGridLookUpEdit4View;
            this.repositoryItemGridLookUpEdit4.ValueMember = "ScreenPosition";
            // 
            // sp00147systemsettingsscreenpositionlistBindingSource
            // 
            this.sp00147systemsettingsscreenpositionlistBindingSource.DataMember = "sp00147_system_settings_screen_position_list";
            this.sp00147systemsettingsscreenpositionlistBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemGridLookUpEdit4View
            // 
            this.repositoryItemGridLookUpEdit4View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemOrder,
            this.colScreenPosition});
            this.repositoryItemGridLookUpEdit4View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit4View.Name = "repositoryItemGridLookUpEdit4View";
            this.repositoryItemGridLookUpEdit4View.OptionsBehavior.Editable = false;
            this.repositoryItemGridLookUpEdit4View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit4View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit4View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit4View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit4View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit4View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit4View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemOrder
            // 
            this.colItemOrder.Caption = "Order";
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            // 
            // colScreenPosition
            // 
            this.colScreenPosition.Caption = "Screen Position";
            this.colScreenPosition.FieldName = "ScreenPosition";
            this.colScreenPosition.Name = "colScreenPosition";
            this.colScreenPosition.OptionsColumn.ReadOnly = true;
            this.colScreenPosition.Visible = true;
            this.colScreenPosition.VisibleIndex = 0;
            this.colScreenPosition.Width = 194;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // repositoryItemLookUpEdit5
            // 
            this.repositoryItemLookUpEdit5.AutoHeight = false;
            this.repositoryItemLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit5.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Value", 49, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Display", "Map Generation Method", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Order", "Order", 38, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEdit5.DataSource = this.sp00148systemsettingsmapgenerationmethodslistBindingSource;
            this.repositoryItemLookUpEdit5.DisplayMember = "Display";
            this.repositoryItemLookUpEdit5.Name = "repositoryItemLookUpEdit5";
            this.repositoryItemLookUpEdit5.NullText = "";
            this.repositoryItemLookUpEdit5.ValueMember = "Value";
            // 
            // sp00148systemsettingsmapgenerationmethodslistBindingSource
            // 
            this.sp00148systemsettingsmapgenerationmethodslistBindingSource.DataMember = "sp00148_system_settings_map_generation_methods_list";
            this.sp00148systemsettingsmapgenerationmethodslistBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemColorEdit3
            // 
            this.repositoryItemColorEdit3.AutoHeight = false;
            this.repositoryItemColorEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit3.Name = "repositoryItemColorEdit3";
            this.repositoryItemColorEdit3.StoreColorAsInteger = true;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "(Tick if Yes)";
            this.repositoryItemCheckEdit2.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit3.Mask.EditMask = "f2";
            this.repositoryItemSpinEdit3.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit3.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // repositoryItemColorEdit4
            // 
            this.repositoryItemColorEdit4.AutoHeight = false;
            this.repositoryItemColorEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit4.Name = "repositoryItemColorEdit4";
            this.repositoryItemColorEdit4.StoreColorAsInteger = true;
            // 
            // repositoryItemTrackBar1
            // 
            this.repositoryItemTrackBar1.Maximum = 100;
            this.repositoryItemTrackBar1.Name = "repositoryItemTrackBar1";
            this.repositoryItemTrackBar1.ShowValueToolTip = true;
            this.repositoryItemTrackBar1.TickFrequency = 5;
            // 
            // repositoryItemSpinEdit4
            // 
            this.repositoryItemSpinEdit4.AutoHeight = false;
            this.repositoryItemSpinEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit4.Mask.EditMask = "f2";
            this.repositoryItemSpinEdit4.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            131072});
            this.repositoryItemSpinEdit4.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit4.Name = "repositoryItemSpinEdit4";
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            this.repositoryItemPopupContainerEdit2.PopupControl = this.popupContainerControlTreeRefStructure;
            this.repositoryItemPopupContainerEdit2.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit2_QueryResultValue);
            // 
            // popupContainerControlTreeRefStructure
            // 
            this.popupContainerControlTreeRefStructure.Controls.Add(this.btnSetTreeRefStruc);
            this.popupContainerControlTreeRefStructure.Controls.Add(this.groupControl1);
            this.popupContainerControlTreeRefStructure.Location = new System.Drawing.Point(186, 33);
            this.popupContainerControlTreeRefStructure.Name = "popupContainerControlTreeRefStructure";
            this.popupContainerControlTreeRefStructure.Size = new System.Drawing.Size(332, 126);
            this.popupContainerControlTreeRefStructure.TabIndex = 26;
            // 
            // btnSetTreeRefStruc
            // 
            this.btnSetTreeRefStruc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSetTreeRefStruc.Location = new System.Drawing.Point(3, 100);
            this.btnSetTreeRefStruc.Name = "btnSetTreeRefStruc";
            this.btnSetTreeRefStruc.Size = new System.Drawing.Size(75, 23);
            this.btnSetTreeRefStruc.TabIndex = 5;
            this.btnSetTreeRefStruc.Text = "OK";
            this.btnSetTreeRefStruc.Click += new System.EventHandler(this.btnSetTreeRefStruc_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.textEditTreeRefRemovePreceedingChars);
            this.groupControl1.Controls.Add(this.spinEditTreeRefRemoveNumberOfChars);
            this.groupControl1.Controls.Add(this.checkEditTreeRef1);
            this.groupControl1.Controls.Add(this.checkEditTreeRef3);
            this.groupControl1.Controls.Add(this.checkEditTreeRef2);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(326, 95);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Tree Reference Structure when Displayed on Labels";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(157, 49);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(61, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Character(s)";
            // 
            // textEditTreeRefRemovePreceedingChars
            // 
            this.textEditTreeRefRemovePreceedingChars.EditValue = "|";
            this.textEditTreeRefRemovePreceedingChars.Location = new System.Drawing.Point(241, 68);
            this.textEditTreeRefRemovePreceedingChars.MenuManager = this.barManager1;
            this.textEditTreeRefRemovePreceedingChars.Name = "textEditTreeRefRemovePreceedingChars";
            this.textEditTreeRefRemovePreceedingChars.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTreeRefRemovePreceedingChars, true);
            this.textEditTreeRefRemovePreceedingChars.Size = new System.Drawing.Size(78, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTreeRefRemovePreceedingChars, optionsSpelling1);
            this.textEditTreeRefRemovePreceedingChars.TabIndex = 5;
            // 
            // spinEditTreeRefRemoveNumberOfChars
            // 
            this.spinEditTreeRefRemoveNumberOfChars.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditTreeRefRemoveNumberOfChars.Location = new System.Drawing.Point(97, 45);
            this.spinEditTreeRefRemoveNumberOfChars.MenuManager = this.barManager1;
            this.spinEditTreeRefRemoveNumberOfChars.Name = "spinEditTreeRefRemoveNumberOfChars";
            this.spinEditTreeRefRemoveNumberOfChars.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditTreeRefRemoveNumberOfChars.Properties.IsFloatValue = false;
            this.spinEditTreeRefRemoveNumberOfChars.Properties.Mask.EditMask = "N00";
            this.spinEditTreeRefRemoveNumberOfChars.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.spinEditTreeRefRemoveNumberOfChars.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditTreeRefRemoveNumberOfChars.Size = new System.Drawing.Size(53, 20);
            this.spinEditTreeRefRemoveNumberOfChars.TabIndex = 4;
            // 
            // checkEditTreeRef1
            // 
            this.checkEditTreeRef1.EditValue = true;
            this.checkEditTreeRef1.Location = new System.Drawing.Point(6, 25);
            this.checkEditTreeRef1.MenuManager = this.barManager1;
            this.checkEditTreeRef1.Name = "checkEditTreeRef1";
            this.checkEditTreeRef1.Properties.Caption = "Default  [unchanged]";
            this.checkEditTreeRef1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTreeRef1.Properties.RadioGroupIndex = 1;
            this.checkEditTreeRef1.Size = new System.Drawing.Size(121, 19);
            this.checkEditTreeRef1.TabIndex = 1;
            this.checkEditTreeRef1.CheckedChanged += new System.EventHandler(this.checkEditTreeRef1_CheckedChanged);
            // 
            // checkEditTreeRef3
            // 
            this.checkEditTreeRef3.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.False;
            this.checkEditTreeRef3.Location = new System.Drawing.Point(6, 69);
            this.checkEditTreeRef3.MenuManager = this.barManager1;
            this.checkEditTreeRef3.Name = "checkEditTreeRef3";
            this.checkEditTreeRef3.Properties.Caption = "Remove All Characters Up To and Including:";
            this.checkEditTreeRef3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTreeRef3.Properties.RadioGroupIndex = 1;
            this.checkEditTreeRef3.Size = new System.Drawing.Size(233, 19);
            this.checkEditTreeRef3.TabIndex = 3;
            this.checkEditTreeRef3.TabStop = false;
            this.checkEditTreeRef3.CheckedChanged += new System.EventHandler(this.checkEditTreeRef3_CheckedChanged);
            // 
            // checkEditTreeRef2
            // 
            this.checkEditTreeRef2.Location = new System.Drawing.Point(6, 47);
            this.checkEditTreeRef2.MenuManager = this.barManager1;
            this.checkEditTreeRef2.Name = "checkEditTreeRef2";
            this.checkEditTreeRef2.Properties.Caption = "Remove First:";
            this.checkEditTreeRef2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTreeRef2.Properties.RadioGroupIndex = 1;
            this.checkEditTreeRef2.Size = new System.Drawing.Size(89, 19);
            this.checkEditTreeRef2.TabIndex = 2;
            this.checkEditTreeRef2.TabStop = false;
            this.checkEditTreeRef2.CheckedChanged += new System.EventHandler(this.checkEditTreeRef2_CheckedChanged);
            // 
            // repositoryItemGridLookUpEdit5
            // 
            this.repositoryItemGridLookUpEdit5.AutoHeight = false;
            this.repositoryItemGridLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit5.DataSource = this.sp00149systemsettingsmappingprojectionslistBindingSource;
            this.repositoryItemGridLookUpEdit5.DisplayMember = "ProjectionName";
            this.repositoryItemGridLookUpEdit5.Name = "repositoryItemGridLookUpEdit5";
            this.repositoryItemGridLookUpEdit5.NullText = "";
            this.repositoryItemGridLookUpEdit5.NullValuePrompt = "Select Projection";
            this.repositoryItemGridLookUpEdit5.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemGridLookUpEdit5.PopupView = this.repositoryItemGridLookUpEdit5View;
            this.repositoryItemGridLookUpEdit5.ValueMember = "ProjectionText";
            // 
            // sp00149systemsettingsmappingprojectionslistBindingSource
            // 
            this.sp00149systemsettingsmappingprojectionslistBindingSource.DataMember = "sp00149_system_settings_mapping_projections_list";
            this.sp00149systemsettingsmappingprojectionslistBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemGridLookUpEdit5View
            // 
            this.repositoryItemGridLookUpEdit5View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colProjectionID,
            this.colProjectionName,
            this.colProjectionOrder,
            this.colProjectionText});
            this.repositoryItemGridLookUpEdit5View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit5View.Name = "repositoryItemGridLookUpEdit5View";
            this.repositoryItemGridLookUpEdit5View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit5View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit5View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit5View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit5View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit5View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit5View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit5View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit5View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit5View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit5View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colProjectionOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colProjectionID
            // 
            this.colProjectionID.Caption = "Projection ID";
            this.colProjectionID.FieldName = "ProjectionID";
            this.colProjectionID.Name = "colProjectionID";
            this.colProjectionID.Width = 83;
            // 
            // colProjectionName
            // 
            this.colProjectionName.Caption = "Projection Name";
            this.colProjectionName.FieldName = "ProjectionName";
            this.colProjectionName.Name = "colProjectionName";
            this.colProjectionName.Visible = true;
            this.colProjectionName.VisibleIndex = 0;
            this.colProjectionName.Width = 336;
            // 
            // colProjectionOrder
            // 
            this.colProjectionOrder.Caption = "Projection Order";
            this.colProjectionOrder.FieldName = "ProjectionOrder";
            this.colProjectionOrder.Name = "colProjectionOrder";
            this.colProjectionOrder.Width = 124;
            // 
            // colProjectionText
            // 
            this.colProjectionText.Caption = "Projection Value";
            this.colProjectionText.FieldName = "ProjectionText";
            this.colProjectionText.Name = "colProjectionText";
            this.colProjectionText.Width = 482;
            // 
            // repositoryItemLookUpEdit6
            // 
            this.repositoryItemLookUpEdit6.AutoHeight = false;
            this.repositoryItemLookUpEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit6.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Value", 49, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Display", "Display", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Order", "Order", 38, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default)});
            this.repositoryItemLookUpEdit6.DataSource = this.sp00150systemsettingsdatatransfermodelistBindingSource;
            this.repositoryItemLookUpEdit6.DisplayMember = "Display";
            this.repositoryItemLookUpEdit6.Name = "repositoryItemLookUpEdit6";
            this.repositoryItemLookUpEdit6.ValueMember = "Value";
            // 
            // sp00150systemsettingsdatatransfermodelistBindingSource
            // 
            this.sp00150systemsettingsdatatransfermodelistBindingSource.DataMember = "sp00150_system_settings_data_transfer_mode_list";
            this.sp00150systemsettingsdatatransfermodelistBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemGridLookUpEdit6
            // 
            this.repositoryItemGridLookUpEdit6.AutoHeight = false;
            this.repositoryItemGridLookUpEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit6.DataSource = this.sp00023SystemSettingsRateCriteriaListBindingSource;
            this.repositoryItemGridLookUpEdit6.DisplayMember = "ItemDescription";
            this.repositoryItemGridLookUpEdit6.Name = "repositoryItemGridLookUpEdit6";
            this.repositoryItemGridLookUpEdit6.NullText = "";
            this.repositoryItemGridLookUpEdit6.PopupView = this.repositoryItemGridLookUpEdit6View;
            this.repositoryItemGridLookUpEdit6.ValueMember = "ItemValue";
            // 
            // sp00023SystemSettingsRateCriteriaListBindingSource
            // 
            this.sp00023SystemSettingsRateCriteriaListBindingSource.DataMember = "sp00023_System_Settings_Rate_Criteria_List";
            this.sp00023SystemSettingsRateCriteriaListBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemGridLookUpEdit6View
            // 
            this.repositoryItemGridLookUpEdit6View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemDescription,
            this.colItemOrder1,
            this.colItemValue});
            this.repositoryItemGridLookUpEdit6View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit6View.Name = "repositoryItemGridLookUpEdit6View";
            this.repositoryItemGridLookUpEdit6View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit6View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit6View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit6View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit6View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit6View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit6View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit6View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit6View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit6View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit6View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit6View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Criteria";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 218;
            // 
            // colItemOrder1
            // 
            this.colItemOrder1.Caption = "Order";
            this.colItemOrder1.FieldName = "ItemOrder";
            this.colItemOrder1.Name = "colItemOrder1";
            this.colItemOrder1.OptionsColumn.AllowEdit = false;
            this.colItemOrder1.OptionsColumn.AllowFocus = false;
            this.colItemOrder1.OptionsColumn.ReadOnly = true;
            // 
            // colItemValue
            // 
            this.colItemValue.Caption = "Criteria ID";
            this.colItemValue.FieldName = "ItemValue";
            this.colItemValue.Name = "colItemValue";
            this.colItemValue.OptionsColumn.AllowEdit = false;
            this.colItemValue.OptionsColumn.AllowFocus = false;
            this.colItemValue.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemSpinEdit5
            // 
            this.repositoryItemSpinEdit5.AutoHeight = false;
            this.repositoryItemSpinEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit5.Mask.EditMask = "######0.00 metres";
            this.repositoryItemSpinEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit5.Name = "repositoryItemSpinEdit5";
            // 
            // repositoryItemGridLookUpEditDefaultCompanyHeader
            // 
            this.repositoryItemGridLookUpEditDefaultCompanyHeader.AutoHeight = false;
            this.repositoryItemGridLookUpEditDefaultCompanyHeader.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditDefaultCompanyHeader.DataSource = this.sp00232CompanyHeaderDropDownListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditDefaultCompanyHeader.DisplayMember = "strDescription";
            this.repositoryItemGridLookUpEditDefaultCompanyHeader.Name = "repositoryItemGridLookUpEditDefaultCompanyHeader";
            this.repositoryItemGridLookUpEditDefaultCompanyHeader.NullText = "";
            this.repositoryItemGridLookUpEditDefaultCompanyHeader.PopupView = this.repositoryItemGridLookUpEdit7View;
            this.repositoryItemGridLookUpEditDefaultCompanyHeader.ValueMember = "intHeaderID";
            // 
            // sp00232CompanyHeaderDropDownListWithBlankBindingSource
            // 
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource.DataMember = "sp00232_Company_Header_Drop_Down_List_With_Blank";
            this.sp00232CompanyHeaderDropDownListWithBlankBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // repositoryItemGridLookUpEdit7View
            // 
            this.repositoryItemGridLookUpEdit7View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colintHeaderID,
            this.colstrAddressLine1,
            this.colstrCompanyName,
            this.colstrDescription,
            this.colstrTelephone1,
            this.colstrSlogan});
            this.repositoryItemGridLookUpEdit7View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colintHeaderID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.repositoryItemGridLookUpEdit7View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.repositoryItemGridLookUpEdit7View.Name = "repositoryItemGridLookUpEdit7View";
            this.repositoryItemGridLookUpEdit7View.OptionsBehavior.AutoExpandAllGroups = true;
            this.repositoryItemGridLookUpEdit7View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit7View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit7View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit7View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit7View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit7View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit7View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit7View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit7View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit7View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit7View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit7View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrCompanyName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colstrAddressLine1
            // 
            this.colstrAddressLine1.Caption = "Address Line 1";
            this.colstrAddressLine1.FieldName = "strAddressLine1";
            this.colstrAddressLine1.Name = "colstrAddressLine1";
            this.colstrAddressLine1.OptionsColumn.AllowEdit = false;
            this.colstrAddressLine1.OptionsColumn.AllowFocus = false;
            this.colstrAddressLine1.OptionsColumn.ReadOnly = true;
            this.colstrAddressLine1.Visible = true;
            this.colstrAddressLine1.VisibleIndex = 2;
            this.colstrAddressLine1.Width = 133;
            // 
            // colstrCompanyName
            // 
            this.colstrCompanyName.Caption = "Company Name";
            this.colstrCompanyName.FieldName = "strCompanyName";
            this.colstrCompanyName.Name = "colstrCompanyName";
            this.colstrCompanyName.OptionsColumn.AllowEdit = false;
            this.colstrCompanyName.OptionsColumn.AllowFocus = false;
            this.colstrCompanyName.OptionsColumn.ReadOnly = true;
            this.colstrCompanyName.Visible = true;
            this.colstrCompanyName.VisibleIndex = 1;
            this.colstrCompanyName.Width = 211;
            // 
            // colstrDescription
            // 
            this.colstrDescription.Caption = "Header Description";
            this.colstrDescription.FieldName = "strDescription";
            this.colstrDescription.Name = "colstrDescription";
            this.colstrDescription.OptionsColumn.AllowEdit = false;
            this.colstrDescription.OptionsColumn.AllowFocus = false;
            this.colstrDescription.OptionsColumn.ReadOnly = true;
            this.colstrDescription.Visible = true;
            this.colstrDescription.VisibleIndex = 0;
            this.colstrDescription.Width = 255;
            // 
            // colstrTelephone1
            // 
            this.colstrTelephone1.Caption = "Telephone 1";
            this.colstrTelephone1.FieldName = "strTelephone1";
            this.colstrTelephone1.Name = "colstrTelephone1";
            this.colstrTelephone1.OptionsColumn.AllowEdit = false;
            this.colstrTelephone1.OptionsColumn.AllowFocus = false;
            this.colstrTelephone1.OptionsColumn.ReadOnly = true;
            this.colstrTelephone1.Visible = true;
            this.colstrTelephone1.VisibleIndex = 3;
            this.colstrTelephone1.Width = 108;
            // 
            // colstrSlogan
            // 
            this.colstrSlogan.Caption = "Slogan";
            this.colstrSlogan.FieldName = "strSlogan";
            this.colstrSlogan.Name = "colstrSlogan";
            this.colstrSlogan.OptionsColumn.AllowEdit = false;
            this.colstrSlogan.OptionsColumn.AllowFocus = false;
            this.colstrSlogan.OptionsColumn.ReadOnly = true;
            this.colstrSlogan.Width = 53;
            // 
            // repositoryItemGridLookUpEditDefaultContractor
            // 
            this.repositoryItemGridLookUpEditDefaultContractor.AutoHeight = false;
            this.repositoryItemGridLookUpEditDefaultContractor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditDefaultContractor.DataSource = this.sp00190ContractorListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditDefaultContractor.DisplayMember = "ContractorName";
            this.repositoryItemGridLookUpEditDefaultContractor.Name = "repositoryItemGridLookUpEditDefaultContractor";
            this.repositoryItemGridLookUpEditDefaultContractor.NullText = "";
            this.repositoryItemGridLookUpEditDefaultContractor.PopupView = this.gridView1;
            this.repositoryItemGridLookUpEditDefaultContractor.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.repositoryItemGridLookUpEditDefaultContractor.ValueMember = "ContractorID";
            // 
            // sp00190ContractorListWithBlankBindingSource
            // 
            this.sp00190ContractorListWithBlankBindingSource.DataMember = "sp00190_Contractor_List_With_Blank";
            this.sp00190ContractorListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn7;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Address Line 1";
            this.gridColumn1.FieldName = "AddressLine1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 144;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Address Line 2";
            this.gridColumn2.FieldName = "AddressLine2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 91;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Address Line 3";
            this.gridColumn3.FieldName = "AddressLine3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 91;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Address Line 4";
            this.gridColumn4.FieldName = "AddressLine4";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 91;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Address Line 5";
            this.gridColumn5.FieldName = "AddressLine5";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 91;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Contractor Code";
            this.gridColumn6.FieldName = "ContractorCode";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 101;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Contractor Name";
            this.gridColumn8.FieldName = "ContractorName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 273;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Disabled";
            this.gridColumn9.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn9.FieldName = "Disabled";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            this.gridColumn9.Width = 61;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Email Password";
            this.gridColumn10.FieldName = "EmailPassword";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 94;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Internal Contractor ID";
            this.gridColumn11.FieldName = "InternalContractor";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 118;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Status";
            this.gridColumn12.FieldName = "InternalCOntractorDescription";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 52;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Mobile";
            this.gridColumn13.FieldName = "Mobile";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 51;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Postcode";
            this.gridColumn14.FieldName = "Postcode";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 65;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Remarks";
            this.gridColumn15.FieldName = "Remarks";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 62;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Telephone 1";
            this.gridColumn16.FieldName = "Telephone1";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 80;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Telephone 2";
            this.gridColumn17.FieldName = "Telephone2";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 80;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Type";
            this.gridColumn18.FieldName = "TypeDescription";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            this.gridColumn18.Width = 111;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Type ID";
            this.gridColumn19.FieldName = "TypeID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 49;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "User Defined 1";
            this.gridColumn20.FieldName = "UserDefined1";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 92;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "User Defined 2";
            this.gridColumn21.FieldName = "UserDefined2";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 92;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "User Defined 3";
            this.gridColumn22.FieldName = "UserDefined3";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 92;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "VAT Reg";
            this.gridColumn23.FieldName = "VatReg";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 62;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Website";
            this.gridColumn24.FieldName = "Website";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 60;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Mask.EditMask = "f2";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "On",
            "Off"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Items.AddRange(new object[] {
            "Full",
            "Quick"});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // ManagerLoadDataOnOpenRepositoryItemComboBox
            // 
            this.ManagerLoadDataOnOpenRepositoryItemComboBox.AutoHeight = false;
            this.ManagerLoadDataOnOpenRepositoryItemComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ManagerLoadDataOnOpenRepositoryItemComboBox.Items.AddRange(new object[] {
            "On",
            "Off"});
            this.ManagerLoadDataOnOpenRepositoryItemComboBox.Name = "ManagerLoadDataOnOpenRepositoryItemComboBox";
            // 
            // repositoryItemButtonEditGetFileName
            // 
            this.repositoryItemButtonEditGetFileName.AutoHeight = false;
            editorButtonImageOptions2.EnableTransparency = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Choose File - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to open the Windows File Browser screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.repositoryItemButtonEditGetFileName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose File", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditGetFileName.Name = "repositoryItemButtonEditGetFileName";
            this.repositoryItemButtonEditGetFileName.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditGetFileName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditGetFileName_ButtonClick);
            // 
            // repositoryItemSpinEdit6
            // 
            this.repositoryItemSpinEdit6.AutoHeight = false;
            this.repositoryItemSpinEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit6.Mask.EditMask = "P";
            this.repositoryItemSpinEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit6.Name = "repositoryItemSpinEdit6";
            // 
            // repositoryItemColorEditSpreadsheetRowBackColour
            // 
            this.repositoryItemColorEditSpreadsheetRowBackColour.AutoHeight = false;
            this.repositoryItemColorEditSpreadsheetRowBackColour.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEditSpreadsheetRowBackColour.Name = "repositoryItemColorEditSpreadsheetRowBackColour";
            this.repositoryItemColorEditSpreadsheetRowBackColour.StoreColorAsInteger = true;
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.Items.AddRange(new object[] {
            "Excel",
            "CSV"});
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // repositoryItemTextEditNumericOnly
            // 
            this.repositoryItemTextEditNumericOnly.AutoHeight = false;
            this.repositoryItemTextEditNumericOnly.Mask.EditMask = "#########0";
            this.repositoryItemTextEditNumericOnly.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericOnly.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericOnly.Name = "repositoryItemTextEditNumericOnly";
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemTextEditCalloutPicturesExternal
            // 
            this.repositoryItemTextEditCalloutPicturesExternal.AutoHeight = false;
            this.repositoryItemTextEditCalloutPicturesExternal.MaxLength = 1000;
            this.repositoryItemTextEditCalloutPicturesExternal.Name = "repositoryItemTextEditCalloutPicturesExternal";
            // 
            // repositoryItemSpinEditDataSyncMachineID
            // 
            this.repositoryItemSpinEditDataSyncMachineID.AutoHeight = false;
            this.repositoryItemSpinEditDataSyncMachineID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditDataSyncMachineID.IsFloatValue = false;
            this.repositoryItemSpinEditDataSyncMachineID.Mask.EditMask = "N00";
            this.repositoryItemSpinEditDataSyncMachineID.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditDataSyncMachineID.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.repositoryItemSpinEditDataSyncMachineID.Name = "repositoryItemSpinEditDataSyncMachineID";
            // 
            // repositoryItemSpinEdit7
            // 
            this.repositoryItemSpinEdit7.AutoHeight = false;
            this.repositoryItemSpinEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit7.IsFloatValue = false;
            this.repositoryItemSpinEdit7.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit7.MaxValue = new decimal(new int[] {
            2500,
            0,
            0,
            0});
            this.repositoryItemSpinEdit7.Name = "repositoryItemSpinEdit7";
            // 
            // repositoryItemGridLookUpEditTextDirections
            // 
            this.repositoryItemGridLookUpEditTextDirections.AutoHeight = false;
            this.repositoryItemGridLookUpEditTextDirections.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditTextDirections.DataSource = this.sp00236ReportWatermarkTextDirectionsBindingSource;
            this.repositoryItemGridLookUpEditTextDirections.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditTextDirections.Name = "repositoryItemGridLookUpEditTextDirections";
            this.repositoryItemGridLookUpEditTextDirections.NullText = "";
            this.repositoryItemGridLookUpEditTextDirections.PopupView = this.gridView2;
            this.repositoryItemGridLookUpEditTextDirections.ValueMember = "Description";
            // 
            // sp00236ReportWatermarkTextDirectionsBindingSource
            // 
            this.sp00236ReportWatermarkTextDirectionsBindingSource.DataMember = "sp00236_Report_Watermark_Text_Directions";
            this.sp00236ReportWatermarkTextDirectionsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn25,
            this.gridColumn26});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn26, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Text Directions";
            this.gridColumn25.FieldName = "Description";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 0;
            this.gridColumn25.Width = 168;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Order";
            this.gridColumn26.FieldName = "Order";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemFontEdit1
            // 
            this.repositoryItemFontEdit1.AutoHeight = false;
            this.repositoryItemFontEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemFontEdit1.Name = "repositoryItemFontEdit1";
            // 
            // repositoryItemSpinEditWatermarkTextSize
            // 
            this.repositoryItemSpinEditWatermarkTextSize.AutoHeight = false;
            this.repositoryItemSpinEditWatermarkTextSize.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditWatermarkTextSize.IsFloatValue = false;
            this.repositoryItemSpinEditWatermarkTextSize.Mask.EditMask = "N00";
            this.repositoryItemSpinEditWatermarkTextSize.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditWatermarkTextSize.MaxValue = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.repositoryItemSpinEditWatermarkTextSize.Name = "repositoryItemSpinEditWatermarkTextSize";
            // 
            // repositoryItemTrackBarWatermarkTransparency
            // 
            this.repositoryItemTrackBarWatermarkTransparency.LabelAppearance.Options.UseTextOptions = true;
            this.repositoryItemTrackBarWatermarkTransparency.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTrackBarWatermarkTransparency.Maximum = 255;
            this.repositoryItemTrackBarWatermarkTransparency.Name = "repositoryItemTrackBarWatermarkTransparency";
            this.repositoryItemTrackBarWatermarkTransparency.ShowValueToolTip = true;
            // 
            // repositoryItemGridLookUpEditWatermarkImageAlign
            // 
            this.repositoryItemGridLookUpEditWatermarkImageAlign.AutoHeight = false;
            this.repositoryItemGridLookUpEditWatermarkImageAlign.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditWatermarkImageAlign.DataSource = this.sp00237ReportWatermarkImageAlignmentBindingSource;
            this.repositoryItemGridLookUpEditWatermarkImageAlign.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditWatermarkImageAlign.Name = "repositoryItemGridLookUpEditWatermarkImageAlign";
            this.repositoryItemGridLookUpEditWatermarkImageAlign.NullText = "";
            this.repositoryItemGridLookUpEditWatermarkImageAlign.PopupView = this.gridView3;
            this.repositoryItemGridLookUpEditWatermarkImageAlign.ValueMember = "Description";
            // 
            // sp00237ReportWatermarkImageAlignmentBindingSource
            // 
            this.sp00237ReportWatermarkImageAlignmentBindingSource.DataMember = "sp00237_Report_Watermark_Image_Alignment";
            this.sp00237ReportWatermarkImageAlignmentBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn27,
            this.gridColumn28});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn28, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Image Alignments";
            this.gridColumn27.FieldName = "Description";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 0;
            this.gridColumn27.Width = 168;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Order";
            this.gridColumn28.FieldName = "Order";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemGridLookUpEditViewMode
            // 
            this.repositoryItemGridLookUpEditViewMode.AutoHeight = false;
            this.repositoryItemGridLookUpEditViewMode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditViewMode.DataSource = this.sp00238ReportWatermarkImageViewModeBindingSource;
            this.repositoryItemGridLookUpEditViewMode.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditViewMode.Name = "repositoryItemGridLookUpEditViewMode";
            this.repositoryItemGridLookUpEditViewMode.NullText = "";
            this.repositoryItemGridLookUpEditViewMode.PopupView = this.gridView4;
            this.repositoryItemGridLookUpEditViewMode.ValueMember = "Description";
            // 
            // sp00238ReportWatermarkImageViewModeBindingSource
            // 
            this.sp00238ReportWatermarkImageViewModeBindingSource.DataMember = "sp00238_Report_Watermark_Image_ViewMode";
            this.sp00238ReportWatermarkImageViewModeBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn29,
            this.gridColumn30});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn30, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Image View Modes";
            this.gridColumn29.FieldName = "Description";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 0;
            this.gridColumn29.Width = 168;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Order";
            this.gridColumn30.FieldName = "Order";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            // 
            // categoryRow1
            // 
            this.categoryRow1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowDataEntryOff,
            this.rowLivePeriod,
            this.rowLiveDBName,
            this.rowSystemDataTransferMode,
            this.rowAmenityTreesLinkedDocumentsPath,
            this.rowGroundControlContractorCertificatesPath,
            this.categoryRow5,
            this.categoryWatermark});
            this.categoryRow1.Height = 17;
            this.categoryRow1.Name = "categoryRow1";
            this.categoryRow1.Properties.Caption = "System Settings";
            // 
            // rowDataEntryOff
            // 
            this.rowDataEntryOff.Name = "rowDataEntryOff";
            this.rowDataEntryOff.Properties.Caption = "Disable Data Entry";
            this.rowDataEntryOff.Properties.FieldName = "DataEntryOff";
            this.rowDataEntryOff.Properties.RowEdit = this.repositoryItemLookUpEdit3;
            // 
            // rowLivePeriod
            // 
            this.rowLivePeriod.Name = "rowLivePeriod";
            this.rowLivePeriod.Properties.Caption = "Live System Period";
            this.rowLivePeriod.Properties.FieldName = "LivePeriod";
            this.rowLivePeriod.Properties.RowEdit = this.repositoryItemLookUpEdit4;
            // 
            // rowLiveDBName
            // 
            this.rowLiveDBName.Name = "rowLiveDBName";
            this.rowLiveDBName.Properties.Caption = "Live Database Name";
            this.rowLiveDBName.Properties.FieldName = "LiveDBName";
            this.rowLiveDBName.Properties.RowEdit = this.repositoryItemTextEdit2;
            // 
            // rowSystemDataTransferMode
            // 
            this.rowSystemDataTransferMode.Name = "rowSystemDataTransferMode";
            this.rowSystemDataTransferMode.Properties.Caption = "Data Transfer Mode";
            this.rowSystemDataTransferMode.Properties.FieldName = "SystemDataTransferMode";
            this.rowSystemDataTransferMode.Properties.RowEdit = this.repositoryItemLookUpEdit6;
            // 
            // rowAmenityTreesLinkedDocumentsPath
            // 
            this.rowAmenityTreesLinkedDocumentsPath.Name = "rowAmenityTreesLinkedDocumentsPath";
            this.rowAmenityTreesLinkedDocumentsPath.Properties.Caption = "Linked Documents Folder";
            this.rowAmenityTreesLinkedDocumentsPath.Properties.FieldName = "AmenityTreesLinkedDocumentsPath";
            this.rowAmenityTreesLinkedDocumentsPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowGroundControlContractorCertificatesPath
            // 
            this.rowGroundControlContractorCertificatesPath.Name = "rowGroundControlContractorCertificatesPath";
            this.rowGroundControlContractorCertificatesPath.Properties.Caption = "Contractor Certificates Folder";
            this.rowGroundControlContractorCertificatesPath.Properties.FieldName = "GroundControlContractorCertificatesPath";
            this.rowGroundControlContractorCertificatesPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryRow5
            // 
            this.categoryRow5.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowStaffImagePath,
            this.rowStaffImageThumbPath,
            this.rowDefaultPhoto});
            this.categoryRow5.Name = "categoryRow5";
            this.categoryRow5.Properties.Caption = "Staff Images";
            // 
            // rowStaffImagePath
            // 
            this.rowStaffImagePath.Name = "rowStaffImagePath";
            this.rowStaffImagePath.Properties.Caption = "Image Path";
            this.rowStaffImagePath.Properties.FieldName = "StaffImagePath";
            this.rowStaffImagePath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowStaffImageThumbPath
            // 
            this.rowStaffImageThumbPath.Name = "rowStaffImageThumbPath";
            this.rowStaffImageThumbPath.Properties.Caption = "Image Thumb Path";
            this.rowStaffImageThumbPath.Properties.FieldName = "StaffImageThumbPath";
            this.rowStaffImageThumbPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowDefaultPhoto
            // 
            this.rowDefaultPhoto.Name = "rowDefaultPhoto";
            this.rowDefaultPhoto.Properties.Caption = "Default Image";
            this.rowDefaultPhoto.Properties.FieldName = "DefaultPhoto";
            this.rowDefaultPhoto.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // categoryWatermark
            // 
            this.categoryWatermark.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowWatermarkShowBehind,
            this.rowWatermarkTextPageRange,
            this.categoryTextWatermark,
            this.categoryImageWatermark});
            this.categoryWatermark.Name = "categoryWatermark";
            this.categoryWatermark.Properties.Caption = "Report Watermark";
            // 
            // rowWatermarkShowBehind
            // 
            this.rowWatermarkShowBehind.Name = "rowWatermarkShowBehind";
            this.rowWatermarkShowBehind.Properties.Caption = "Show Behind";
            this.rowWatermarkShowBehind.Properties.FieldName = "WatermarkShowBehind";
            this.rowWatermarkShowBehind.Properties.RowEdit = this.repositoryItemCheckEdit2;
            // 
            // rowWatermarkTextPageRange
            // 
            this.rowWatermarkTextPageRange.Name = "rowWatermarkTextPageRange";
            this.rowWatermarkTextPageRange.Properties.Caption = "Page Range";
            this.rowWatermarkTextPageRange.Properties.FieldName = "WatermarkTextPageRange";
            // 
            // categoryTextWatermark
            // 
            this.categoryTextWatermark.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowWatermarkText,
            this.rowWatermarkTextDirection,
            this.rowWatermarkTextFont,
            this.rowWatermarkTextFontSize,
            this.rowWatermarkTextFontBold,
            this.rowWatermarkTextFontItalic,
            this.rowWatermarkTextColour,
            this.rowWatermarkTextTransparency});
            this.categoryTextWatermark.Name = "categoryTextWatermark";
            this.categoryTextWatermark.Properties.Caption = "Text Watermark";
            // 
            // rowWatermarkText
            // 
            this.rowWatermarkText.Name = "rowWatermarkText";
            this.rowWatermarkText.Properties.Caption = "Text";
            this.rowWatermarkText.Properties.FieldName = "WatermarkText";
            // 
            // rowWatermarkTextDirection
            // 
            this.rowWatermarkTextDirection.Name = "rowWatermarkTextDirection";
            this.rowWatermarkTextDirection.Properties.Caption = "Direction";
            this.rowWatermarkTextDirection.Properties.FieldName = "WatermarkTextDirection";
            this.rowWatermarkTextDirection.Properties.RowEdit = this.repositoryItemGridLookUpEditTextDirections;
            // 
            // rowWatermarkTextFont
            // 
            this.rowWatermarkTextFont.Name = "rowWatermarkTextFont";
            this.rowWatermarkTextFont.Properties.Caption = "Font";
            this.rowWatermarkTextFont.Properties.FieldName = "WatermarkTextFont";
            this.rowWatermarkTextFont.Properties.RowEdit = this.repositoryItemFontEdit1;
            // 
            // rowWatermarkTextFontSize
            // 
            this.rowWatermarkTextFontSize.Name = "rowWatermarkTextFontSize";
            this.rowWatermarkTextFontSize.Properties.Caption = "Font Size";
            this.rowWatermarkTextFontSize.Properties.FieldName = "WatermarkTextFontSize";
            this.rowWatermarkTextFontSize.Properties.RowEdit = this.repositoryItemSpinEditWatermarkTextSize;
            // 
            // rowWatermarkTextFontBold
            // 
            this.rowWatermarkTextFontBold.Name = "rowWatermarkTextFontBold";
            this.rowWatermarkTextFontBold.Properties.Caption = "Font Bold";
            this.rowWatermarkTextFontBold.Properties.FieldName = "WatermarkTextFontBold";
            this.rowWatermarkTextFontBold.Properties.RowEdit = this.repositoryItemCheckEdit2;
            // 
            // rowWatermarkTextFontItalic
            // 
            this.rowWatermarkTextFontItalic.Name = "rowWatermarkTextFontItalic";
            this.rowWatermarkTextFontItalic.Properties.Caption = "Font Italic";
            this.rowWatermarkTextFontItalic.Properties.FieldName = "WatermarkTextFontItalic";
            this.rowWatermarkTextFontItalic.Properties.RowEdit = this.repositoryItemCheckEdit2;
            // 
            // rowWatermarkTextColour
            // 
            this.rowWatermarkTextColour.Name = "rowWatermarkTextColour";
            this.rowWatermarkTextColour.Properties.Caption = "Colour";
            this.rowWatermarkTextColour.Properties.FieldName = "WatermarkTextColour";
            this.rowWatermarkTextColour.Properties.RowEdit = this.repositoryItemColorEdit2;
            // 
            // rowWatermarkTextTransparency
            // 
            this.rowWatermarkTextTransparency.Name = "rowWatermarkTextTransparency";
            this.rowWatermarkTextTransparency.Properties.Caption = "Transparency";
            this.rowWatermarkTextTransparency.Properties.FieldName = "WatermarkTextTransparency";
            this.rowWatermarkTextTransparency.Properties.RowEdit = this.repositoryItemTrackBarWatermarkTransparency;
            // 
            // categoryImageWatermark
            // 
            this.categoryImageWatermark.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowWatermarkImage,
            this.rowWatermarkImageAlign,
            this.rowWatermarkImageTiling,
            this.rowWatermarkImageViewMode,
            this.rowWatermarkImageTransparency});
            this.categoryImageWatermark.Name = "categoryImageWatermark";
            this.categoryImageWatermark.Properties.Caption = "Image Watermark";
            // 
            // rowWatermarkImage
            // 
            this.rowWatermarkImage.Name = "rowWatermarkImage";
            this.rowWatermarkImage.Properties.Caption = "Image";
            this.rowWatermarkImage.Properties.FieldName = "WatermarkImage";
            this.rowWatermarkImage.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowWatermarkImageAlign
            // 
            this.rowWatermarkImageAlign.Name = "rowWatermarkImageAlign";
            this.rowWatermarkImageAlign.Properties.Caption = "Alignment";
            this.rowWatermarkImageAlign.Properties.FieldName = "WatermarkImageAlign";
            this.rowWatermarkImageAlign.Properties.RowEdit = this.repositoryItemGridLookUpEditWatermarkImageAlign;
            // 
            // rowWatermarkImageTiling
            // 
            this.rowWatermarkImageTiling.Name = "rowWatermarkImageTiling";
            this.rowWatermarkImageTiling.Properties.Caption = "Tiling";
            this.rowWatermarkImageTiling.Properties.FieldName = "WatermarkImageTiling";
            this.rowWatermarkImageTiling.Properties.RowEdit = this.repositoryItemCheckEdit2;
            // 
            // rowWatermarkImageViewMode
            // 
            this.rowWatermarkImageViewMode.Name = "rowWatermarkImageViewMode";
            this.rowWatermarkImageViewMode.Properties.Caption = "View Mode";
            this.rowWatermarkImageViewMode.Properties.FieldName = "WatermarkImageViewMode";
            this.rowWatermarkImageViewMode.Properties.RowEdit = this.repositoryItemGridLookUpEditViewMode;
            // 
            // rowWatermarkImageTransparency
            // 
            this.rowWatermarkImageTransparency.Name = "rowWatermarkImageTransparency";
            this.rowWatermarkImageTransparency.Properties.Caption = "Transparency";
            this.rowWatermarkImageTransparency.Properties.FieldName = "WatermarkImageTransparency";
            this.rowWatermarkImageTransparency.Properties.RowEdit = this.repositoryItemTrackBarWatermarkTransparency;
            // 
            // categoryRow2
            // 
            this.categoryRow2.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesPictureFilesFolder,
            this.categoryRow23,
            this.categoryRow6,
            this.categoryRow4,
            this.categoryRow3,
            this.categoryRow21,
            this.categoryRow7,
            this.categoryRow13,
            this.categoryRow14,
            this.categoryRow25,
            this.categoryRow26});
            this.categoryRow2.Name = "categoryRow2";
            this.categoryRow2.Properties.Caption = "Amenity Trees";
            // 
            // rowAmenityTreesPictureFilesFolder
            // 
            this.rowAmenityTreesPictureFilesFolder.Name = "rowAmenityTreesPictureFilesFolder";
            this.rowAmenityTreesPictureFilesFolder.Properties.Caption = "Picture Files Folder [Legacy]";
            this.rowAmenityTreesPictureFilesFolder.Properties.FieldName = "AmenityTreesPictureFilesFolder";
            this.rowAmenityTreesPictureFilesFolder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryRow23
            // 
            this.categoryRow23.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesJobRateCriteriaID,
            this.rowAmenityTreesJobDiscountRateCriteriaID});
            this.categoryRow23.Height = 17;
            this.categoryRow23.Name = "categoryRow23";
            this.categoryRow23.Properties.Caption = "Job Rates";
            // 
            // rowAmenityTreesJobRateCriteriaID
            // 
            this.rowAmenityTreesJobRateCriteriaID.Name = "rowAmenityTreesJobRateCriteriaID";
            this.rowAmenityTreesJobRateCriteriaID.Properties.Caption = "Job Rate Criteria";
            this.rowAmenityTreesJobRateCriteriaID.Properties.FieldName = "AmenityTreesJobRateCriteriaID";
            this.rowAmenityTreesJobRateCriteriaID.Properties.RowEdit = this.repositoryItemGridLookUpEdit6;
            // 
            // rowAmenityTreesJobDiscountRateCriteriaID
            // 
            this.rowAmenityTreesJobDiscountRateCriteriaID.Name = "rowAmenityTreesJobDiscountRateCriteriaID";
            this.rowAmenityTreesJobDiscountRateCriteriaID.Properties.Caption = "Job Discount Rate Criteria";
            this.rowAmenityTreesJobDiscountRateCriteriaID.Properties.FieldName = "AmenityTreesJobDiscountRateCriteriaID";
            this.rowAmenityTreesJobDiscountRateCriteriaID.Properties.RowEdit = this.repositoryItemGridLookUpEdit6;
            // 
            // categoryRow6
            // 
            this.categoryRow6.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesWorkOrderLayoutLocation,
            this.rowAmenityTreesWorkOrderDefaultLayoutID,
            this.rowAmenityTreesDefaultContractor,
            this.rowAmenityTreesDefaultCompanyHeader,
            this.categoryRow8});
            this.categoryRow6.Name = "categoryRow6";
            this.categoryRow6.Properties.Caption = "Work Orders";
            // 
            // rowAmenityTreesWorkOrderLayoutLocation
            // 
            this.rowAmenityTreesWorkOrderLayoutLocation.Name = "rowAmenityTreesWorkOrderLayoutLocation";
            this.rowAmenityTreesWorkOrderLayoutLocation.Properties.Caption = "Saved Layout Location";
            this.rowAmenityTreesWorkOrderLayoutLocation.Properties.FieldName = "AmenityTreesWorkOrderLayoutLocation";
            this.rowAmenityTreesWorkOrderLayoutLocation.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesWorkOrderDefaultLayoutID
            // 
            this.rowAmenityTreesWorkOrderDefaultLayoutID.Name = "rowAmenityTreesWorkOrderDefaultLayoutID";
            this.rowAmenityTreesWorkOrderDefaultLayoutID.Properties.Caption = "Default Print Layout";
            this.rowAmenityTreesWorkOrderDefaultLayoutID.Properties.FieldName = "AmenityTreesWorkOrderDefaultLayoutID";
            this.rowAmenityTreesWorkOrderDefaultLayoutID.Properties.RowEdit = this.repositoryItemGridLookUpEdit2;
            // 
            // rowAmenityTreesDefaultContractor
            // 
            this.rowAmenityTreesDefaultContractor.Name = "rowAmenityTreesDefaultContractor";
            this.rowAmenityTreesDefaultContractor.Properties.Caption = "Default Contractor";
            this.rowAmenityTreesDefaultContractor.Properties.FieldName = "AmenityTreesDefaultContractor";
            this.rowAmenityTreesDefaultContractor.Properties.RowEdit = this.repositoryItemGridLookUpEditDefaultContractor;
            // 
            // rowAmenityTreesDefaultCompanyHeader
            // 
            this.rowAmenityTreesDefaultCompanyHeader.Name = "rowAmenityTreesDefaultCompanyHeader";
            this.rowAmenityTreesDefaultCompanyHeader.Properties.Caption = "Default Company Header";
            this.rowAmenityTreesDefaultCompanyHeader.Properties.FieldName = "AmenityTreesDefaultCompanyHeader";
            this.rowAmenityTreesDefaultCompanyHeader.Properties.RowEdit = this.repositoryItemGridLookUpEditDefaultCompanyHeader;
            // 
            // categoryRow8
            // 
            this.categoryRow8.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesWorkOrderMapGenerationMethod,
            this.rowAmenityTreesWorkOrderMapTicked,
            this.rowAmenityTreesWorkOrderPrintShowDetailsOfOtherTrees,
            this.rowAmenityTreesWorkOrderMapTreeHighlightColour,
            this.rowAmenityTreesWorkOrderMapLocation,
            this.rowAmenityTreesWorkOrderDefaultPaperSize,
            this.rowAmenityTreesWorkOrderDefaultLegendPosition,
            this.rowAmenityTreesWorkOrderDefaultNorthArrowPosition,
            this.rowAmenityTreesWorkOrderDefaultScaleBarPosition,
            this.rowAmenityTreesWorkOrderOSCopyright,
            this.rowAmenityTreesWorkOrderOSCopyrightDefaultPosition,
            this.rowAmenityTreesWorkOrderOSCopyrightWidth,
            this.categoryRow16});
            this.categoryRow8.Height = 17;
            this.categoryRow8.Name = "categoryRow8";
            this.categoryRow8.Properties.Caption = "Map Generation";
            // 
            // rowAmenityTreesWorkOrderMapGenerationMethod
            // 
            this.rowAmenityTreesWorkOrderMapGenerationMethod.Name = "rowAmenityTreesWorkOrderMapGenerationMethod";
            this.rowAmenityTreesWorkOrderMapGenerationMethod.Properties.Caption = "Map Generation Method";
            this.rowAmenityTreesWorkOrderMapGenerationMethod.Properties.FieldName = "AmenityTreesWorkOrderMapGenerationMethod";
            this.rowAmenityTreesWorkOrderMapGenerationMethod.Properties.RowEdit = this.repositoryItemLookUpEdit5;
            // 
            // rowAmenityTreesWorkOrderMapTicked
            // 
            this.rowAmenityTreesWorkOrderMapTicked.Name = "rowAmenityTreesWorkOrderMapTicked";
            this.rowAmenityTreesWorkOrderMapTicked.Properties.Caption = "Default Show Map";
            this.rowAmenityTreesWorkOrderMapTicked.Properties.FieldName = "AmenityTreesWorkOrderMapTicked";
            this.rowAmenityTreesWorkOrderMapTicked.Properties.RowEdit = this.repositoryItemCheckEdit2;
            // 
            // rowAmenityTreesWorkOrderPrintShowDetailsOfOtherTrees
            // 
            this.rowAmenityTreesWorkOrderPrintShowDetailsOfOtherTrees.Name = "rowAmenityTreesWorkOrderPrintShowDetailsOfOtherTrees";
            this.rowAmenityTreesWorkOrderPrintShowDetailsOfOtherTrees.Properties.Caption = "Show Other Tree Details";
            this.rowAmenityTreesWorkOrderPrintShowDetailsOfOtherTrees.Properties.FieldName = "AmenityTreesWorkOrderPrintShowDetailsOfOtherTrees";
            this.rowAmenityTreesWorkOrderPrintShowDetailsOfOtherTrees.Properties.RowEdit = this.repositoryItemLookUpEdit1;
            // 
            // rowAmenityTreesWorkOrderMapTreeHighlightColour
            // 
            this.rowAmenityTreesWorkOrderMapTreeHighlightColour.Name = "rowAmenityTreesWorkOrderMapTreeHighlightColour";
            this.rowAmenityTreesWorkOrderMapTreeHighlightColour.Properties.Caption = "Work Order Tree Highlight Colour";
            this.rowAmenityTreesWorkOrderMapTreeHighlightColour.Properties.FieldName = "AmenityTreesWorkOrderMapTreeHighlightColour";
            this.rowAmenityTreesWorkOrderMapTreeHighlightColour.Properties.RowEdit = this.repositoryItemColorEdit3;
            // 
            // rowAmenityTreesWorkOrderMapLocation
            // 
            this.rowAmenityTreesWorkOrderMapLocation.Name = "rowAmenityTreesWorkOrderMapLocation";
            this.rowAmenityTreesWorkOrderMapLocation.Properties.Caption = "Save Location";
            this.rowAmenityTreesWorkOrderMapLocation.Properties.FieldName = "AmenityTreesWorkOrderMapLocation";
            this.rowAmenityTreesWorkOrderMapLocation.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesWorkOrderDefaultPaperSize
            // 
            this.rowAmenityTreesWorkOrderDefaultPaperSize.Name = "rowAmenityTreesWorkOrderDefaultPaperSize";
            this.rowAmenityTreesWorkOrderDefaultPaperSize.Properties.Caption = "Default Paper Size";
            this.rowAmenityTreesWorkOrderDefaultPaperSize.Properties.FieldName = "AmenityTreesWorkOrderDefaultPaperSize";
            this.rowAmenityTreesWorkOrderDefaultPaperSize.Properties.RowEdit = this.repositoryItemGridLookUpEdit3;
            // 
            // rowAmenityTreesWorkOrderDefaultLegendPosition
            // 
            this.rowAmenityTreesWorkOrderDefaultLegendPosition.Name = "rowAmenityTreesWorkOrderDefaultLegendPosition";
            this.rowAmenityTreesWorkOrderDefaultLegendPosition.Properties.Caption = "Default Legend Position";
            this.rowAmenityTreesWorkOrderDefaultLegendPosition.Properties.FieldName = "AmenityTreesWorkOrderDefaultLegendPosition";
            this.rowAmenityTreesWorkOrderDefaultLegendPosition.Properties.RowEdit = this.repositoryItemGridLookUpEdit4;
            // 
            // rowAmenityTreesWorkOrderDefaultNorthArrowPosition
            // 
            this.rowAmenityTreesWorkOrderDefaultNorthArrowPosition.Name = "rowAmenityTreesWorkOrderDefaultNorthArrowPosition";
            this.rowAmenityTreesWorkOrderDefaultNorthArrowPosition.Properties.Caption = "Default North Arrow Position";
            this.rowAmenityTreesWorkOrderDefaultNorthArrowPosition.Properties.FieldName = "AmenityTreesWorkOrderDefaultNorthArrowPosition";
            this.rowAmenityTreesWorkOrderDefaultNorthArrowPosition.Properties.RowEdit = this.repositoryItemGridLookUpEdit4;
            // 
            // rowAmenityTreesWorkOrderDefaultScaleBarPosition
            // 
            this.rowAmenityTreesWorkOrderDefaultScaleBarPosition.Name = "rowAmenityTreesWorkOrderDefaultScaleBarPosition";
            this.rowAmenityTreesWorkOrderDefaultScaleBarPosition.Properties.Caption = "Default Scale Bar Position";
            this.rowAmenityTreesWorkOrderDefaultScaleBarPosition.Properties.FieldName = "AmenityTreesWorkOrderDefaultScaleBarPosition";
            this.rowAmenityTreesWorkOrderDefaultScaleBarPosition.Properties.RowEdit = this.repositoryItemGridLookUpEdit4;
            // 
            // rowAmenityTreesWorkOrderOSCopyright
            // 
            this.rowAmenityTreesWorkOrderOSCopyright.Name = "rowAmenityTreesWorkOrderOSCopyright";
            this.rowAmenityTreesWorkOrderOSCopyright.Properties.Caption = "Default OS Copyright Text";
            this.rowAmenityTreesWorkOrderOSCopyright.Properties.FieldName = "AmenityTreesWorkOrderOSCopyright";
            this.rowAmenityTreesWorkOrderOSCopyright.Properties.RowEdit = this.repositoryItemTextEdit5;
            // 
            // rowAmenityTreesWorkOrderOSCopyrightDefaultPosition
            // 
            this.rowAmenityTreesWorkOrderOSCopyrightDefaultPosition.Name = "rowAmenityTreesWorkOrderOSCopyrightDefaultPosition";
            this.rowAmenityTreesWorkOrderOSCopyrightDefaultPosition.Properties.Caption = "Default OS Copyright Position";
            this.rowAmenityTreesWorkOrderOSCopyrightDefaultPosition.Properties.FieldName = "AmenityTreesWorkOrderOSCopyrightDefaultPosition";
            this.rowAmenityTreesWorkOrderOSCopyrightDefaultPosition.Properties.RowEdit = this.repositoryItemGridLookUpEdit4;
            // 
            // rowAmenityTreesWorkOrderOSCopyrightWidth
            // 
            this.rowAmenityTreesWorkOrderOSCopyrightWidth.Name = "rowAmenityTreesWorkOrderOSCopyrightWidth";
            this.rowAmenityTreesWorkOrderOSCopyrightWidth.Properties.Caption = "Default OS Copyright Width";
            this.rowAmenityTreesWorkOrderOSCopyrightWidth.Properties.FieldName = "AmenityTreesWorkOrderOSCopyrightWidth";
            this.rowAmenityTreesWorkOrderOSCopyrightWidth.Properties.RowEdit = this.repositoryItemSpinEdit7;
            // 
            // categoryRow16
            // 
            this.categoryRow16.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesWorkOrderDefaultMapScale});
            this.categoryRow16.Name = "categoryRow16";
            this.categoryRow16.Properties.Caption = "External Map Generation [Legacy]";
            // 
            // rowAmenityTreesWorkOrderDefaultMapScale
            // 
            this.rowAmenityTreesWorkOrderDefaultMapScale.Name = "rowAmenityTreesWorkOrderDefaultMapScale";
            this.rowAmenityTreesWorkOrderDefaultMapScale.Properties.Caption = "Default Map Scale";
            this.rowAmenityTreesWorkOrderDefaultMapScale.Properties.FieldName = "AmenityTreesWorkOrderDefaultMapScale";
            this.rowAmenityTreesWorkOrderDefaultMapScale.Properties.RowEdit = this.repositoryItemGridLookUpEdit1;
            // 
            // categoryRow4
            // 
            this.categoryRow4.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesMapTreeFilesFolder,
            this.rowAmenityTreesMapBackgroundFolder,
            this.rowAmenityTreesMapBackgroundFolderTablet,
            this.rowAmenityTreesDefaultMappingProjection});
            this.categoryRow4.Name = "categoryRow4";
            this.categoryRow4.Properties.Caption = "Mapping";
            // 
            // rowAmenityTreesMapTreeFilesFolder
            // 
            this.rowAmenityTreesMapTreeFilesFolder.Height = 22;
            this.rowAmenityTreesMapTreeFilesFolder.Name = "rowAmenityTreesMapTreeFilesFolder";
            this.rowAmenityTreesMapTreeFilesFolder.Properties.Caption = "Tree Files Folder [Not Used]";
            this.rowAmenityTreesMapTreeFilesFolder.Properties.FieldName = "AmenityTreesMapTreeFilesFolder";
            this.rowAmenityTreesMapTreeFilesFolder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesMapBackgroundFolder
            // 
            this.rowAmenityTreesMapBackgroundFolder.Height = 20;
            this.rowAmenityTreesMapBackgroundFolder.Name = "rowAmenityTreesMapBackgroundFolder";
            this.rowAmenityTreesMapBackgroundFolder.Properties.Caption = "Server \\ Desktop Background Folder";
            this.rowAmenityTreesMapBackgroundFolder.Properties.FieldName = "AmenityTreesMapBackgroundFolder";
            this.rowAmenityTreesMapBackgroundFolder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesMapBackgroundFolderTablet
            // 
            this.rowAmenityTreesMapBackgroundFolderTablet.Name = "rowAmenityTreesMapBackgroundFolderTablet";
            this.rowAmenityTreesMapBackgroundFolderTablet.Properties.Caption = "Tablet PC Background Folder";
            this.rowAmenityTreesMapBackgroundFolderTablet.Properties.FieldName = "AmenityTreesMapBackgroundFolderTablet";
            this.rowAmenityTreesMapBackgroundFolderTablet.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesDefaultMappingProjection
            // 
            this.rowAmenityTreesDefaultMappingProjection.Name = "rowAmenityTreesDefaultMappingProjection";
            this.rowAmenityTreesDefaultMappingProjection.Properties.Caption = "Default Mapping Projection";
            this.rowAmenityTreesDefaultMappingProjection.Properties.FieldName = "AmenityTreesDefaultMappingProjection";
            this.rowAmenityTreesDefaultMappingProjection.Properties.RowEdit = this.repositoryItemGridLookUpEdit5;
            // 
            // categoryRow3
            // 
            this.categoryRow3.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesReportLayoutLocation});
            this.categoryRow3.Name = "categoryRow3";
            this.categoryRow3.Properties.Caption = "Reporting";
            // 
            // rowAmenityTreesReportLayoutLocation
            // 
            this.rowAmenityTreesReportLayoutLocation.Name = "rowAmenityTreesReportLayoutLocation";
            this.rowAmenityTreesReportLayoutLocation.Properties.Caption = "Saved Layout Location";
            this.rowAmenityTreesReportLayoutLocation.Properties.FieldName = "AmenityTreesReportLayoutLocation";
            this.rowAmenityTreesReportLayoutLocation.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryRow21
            // 
            this.categoryRow21.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow22});
            this.categoryRow21.Name = "categoryRow21";
            this.categoryRow21.Properties.Caption = "Tablet PC";
            // 
            // categoryRow22
            // 
            this.categoryRow22.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMobileInterfaceExportFolder,
            this.rowMobileInterfaceImportFolder});
            this.categoryRow22.Name = "categoryRow22";
            this.categoryRow22.Properties.Caption = "Transfer File Locations";
            // 
            // rowMobileInterfaceExportFolder
            // 
            this.rowMobileInterfaceExportFolder.Name = "rowMobileInterfaceExportFolder";
            this.rowMobileInterfaceExportFolder.Properties.Caption = "Desktop to Tablet";
            this.rowMobileInterfaceExportFolder.Properties.FieldName = "MobileInterfaceExportFolder";
            this.rowMobileInterfaceExportFolder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowMobileInterfaceImportFolder
            // 
            this.rowMobileInterfaceImportFolder.Name = "rowMobileInterfaceImportFolder";
            this.rowMobileInterfaceImportFolder.Properties.Caption = "Tablet to Desktop";
            this.rowMobileInterfaceImportFolder.Properties.FieldName = "MobileInterfaceImportFolder";
            this.rowMobileInterfaceImportFolder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryRow7
            // 
            this.categoryRow7.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow9,
            this.categoryRow10});
            this.categoryRow7.Height = 17;
            this.categoryRow7.Name = "categoryRow7";
            this.categoryRow7.Properties.Caption = "GBM Mobile";
            // 
            // categoryRow9
            // 
            this.categoryRow9.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesExportToGBM_Background_Mapping_Folder,
            this.rowAmenityTreesExportToGBM_Geoset_Folder,
            this.rowAmenityTreesExportToGBM_Profile_Folder,
            this.rowAmenityTreesExportToGBM_GBM_Mobile_XML_Folder,
            this.rowAmenityTreesExportToGBM_Tab_Folder,
            this.rowAmenityTreesExportToGBM_Mid_Mif_Folder,
            this.rowAmenityTreesExportToGBM_Pda_Reference_Data_Folder});
            this.categoryRow9.Name = "categoryRow9";
            this.categoryRow9.Properties.Caption = "Export";
            // 
            // rowAmenityTreesExportToGBM_Background_Mapping_Folder
            // 
            this.rowAmenityTreesExportToGBM_Background_Mapping_Folder.Name = "rowAmenityTreesExportToGBM_Background_Mapping_Folder";
            this.rowAmenityTreesExportToGBM_Background_Mapping_Folder.Properties.Caption = "Background Mapping Location";
            this.rowAmenityTreesExportToGBM_Background_Mapping_Folder.Properties.FieldName = "AmenityTreesExportToGBM_Background_Mapping_Folder";
            this.rowAmenityTreesExportToGBM_Background_Mapping_Folder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesExportToGBM_Geoset_Folder
            // 
            this.rowAmenityTreesExportToGBM_Geoset_Folder.Name = "rowAmenityTreesExportToGBM_Geoset_Folder";
            this.rowAmenityTreesExportToGBM_Geoset_Folder.Properties.Caption = "Geoset File Location";
            this.rowAmenityTreesExportToGBM_Geoset_Folder.Properties.FieldName = "AmenityTreesExportToGBM_Geoset_Folder";
            this.rowAmenityTreesExportToGBM_Geoset_Folder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesExportToGBM_Profile_Folder
            // 
            this.rowAmenityTreesExportToGBM_Profile_Folder.Name = "rowAmenityTreesExportToGBM_Profile_Folder";
            this.rowAmenityTreesExportToGBM_Profile_Folder.Properties.Caption = "Profile File Location";
            this.rowAmenityTreesExportToGBM_Profile_Folder.Properties.FieldName = "AmenityTreesExportToGBM_Profile_Folder";
            this.rowAmenityTreesExportToGBM_Profile_Folder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesExportToGBM_GBM_Mobile_XML_Folder
            // 
            this.rowAmenityTreesExportToGBM_GBM_Mobile_XML_Folder.Name = "rowAmenityTreesExportToGBM_GBM_Mobile_XML_Folder";
            this.rowAmenityTreesExportToGBM_GBM_Mobile_XML_Folder.Properties.Caption = "GbmMobile.XML File Location";
            this.rowAmenityTreesExportToGBM_GBM_Mobile_XML_Folder.Properties.FieldName = "AmenityTreesExportToGBM_GBM_Mobile_XML_Folder";
            this.rowAmenityTreesExportToGBM_GBM_Mobile_XML_Folder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesExportToGBM_Tab_Folder
            // 
            this.rowAmenityTreesExportToGBM_Tab_Folder.Name = "rowAmenityTreesExportToGBM_Tab_Folder";
            this.rowAmenityTreesExportToGBM_Tab_Folder.Properties.Caption = "TAB File Location";
            this.rowAmenityTreesExportToGBM_Tab_Folder.Properties.FieldName = "AmenityTreesExportToGBM_Tab_Folder";
            this.rowAmenityTreesExportToGBM_Tab_Folder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesExportToGBM_Mid_Mif_Folder
            // 
            this.rowAmenityTreesExportToGBM_Mid_Mif_Folder.Name = "rowAmenityTreesExportToGBM_Mid_Mif_Folder";
            this.rowAmenityTreesExportToGBM_Mid_Mif_Folder.Properties.Caption = "MID / MIF File Location";
            this.rowAmenityTreesExportToGBM_Mid_Mif_Folder.Properties.FieldName = "AmenityTreesExportToGBM_Mid_Mif_Folder";
            this.rowAmenityTreesExportToGBM_Mid_Mif_Folder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTreesExportToGBM_Pda_Reference_Data_Folder
            // 
            this.rowAmenityTreesExportToGBM_Pda_Reference_Data_Folder.Name = "rowAmenityTreesExportToGBM_Pda_Reference_Data_Folder";
            this.rowAmenityTreesExportToGBM_Pda_Reference_Data_Folder.Properties.Caption = "PDA Working Directory Location";
            this.rowAmenityTreesExportToGBM_Pda_Reference_Data_Folder.Properties.FieldName = "AmenityTreesExportToGBM_Pda_Reference_Data_Folder";
            this.rowAmenityTreesExportToGBM_Pda_Reference_Data_Folder.Properties.RowEdit = this.repositoryItemTextEdit3;
            // 
            // categoryRow10
            // 
            this.categoryRow10.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesImportFromGBM_Default_Tab_Folder});
            this.categoryRow10.Name = "categoryRow10";
            this.categoryRow10.Properties.Caption = "Import";
            // 
            // rowAmenityTreesImportFromGBM_Default_Tab_Folder
            // 
            this.rowAmenityTreesImportFromGBM_Default_Tab_Folder.Name = "rowAmenityTreesImportFromGBM_Default_Tab_Folder";
            this.rowAmenityTreesImportFromGBM_Default_Tab_Folder.Properties.Caption = "Default Import Folder Location";
            this.rowAmenityTreesImportFromGBM_Default_Tab_Folder.Properties.FieldName = "AmenityTreesImportFromGBM_Default_Tab_Folder";
            this.rowAmenityTreesImportFromGBM_Default_Tab_Folder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryRow13
            // 
            this.categoryRow13.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesVersion4WoodPlan_Incident_Link_Exe,
            this.rowAmenityTrees_TreePicker_Polygon_Area,
            this.rowAmenityTrees_TreePicker_Default_Hedge_Width,
            this.rowMappingShowAmenityTreeObjects,
            this.rowMappingShowEstatePlanObjects,
            this.categoryRow20,
            this.categoryRow19,
            this.categoryRow17,
            this.categoryRow18});
            this.categoryRow13.Name = "categoryRow13";
            this.categoryRow13.Properties.Caption = "Tree Picker Mapping";
            // 
            // rowAmenityTreesVersion4WoodPlan_Incident_Link_Exe
            // 
            this.rowAmenityTreesVersion4WoodPlan_Incident_Link_Exe.Name = "rowAmenityTreesVersion4WoodPlan_Incident_Link_Exe";
            this.rowAmenityTreesVersion4WoodPlan_Incident_Link_Exe.Properties.Caption = "WoodPlan V.4 Mapping Link Exe File Location [Legacy]";
            this.rowAmenityTreesVersion4WoodPlan_Incident_Link_Exe.Properties.FieldName = "AmenityTreesVersion4WoodPlan_Incident_Link_Exe";
            this.rowAmenityTreesVersion4WoodPlan_Incident_Link_Exe.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowAmenityTrees_TreePicker_Polygon_Area
            // 
            this.rowAmenityTrees_TreePicker_Polygon_Area.Name = "rowAmenityTrees_TreePicker_Polygon_Area";
            this.rowAmenityTrees_TreePicker_Polygon_Area.Properties.Caption = "Polygon Area Measurement Unit";
            this.rowAmenityTrees_TreePicker_Polygon_Area.Properties.FieldName = "AmenityTrees_TreePicker_Polygon_Area";
            this.rowAmenityTrees_TreePicker_Polygon_Area.Properties.RowEdit = this.repositoryItemComboBox2;
            // 
            // rowAmenityTrees_TreePicker_Default_Hedge_Width
            // 
            this.rowAmenityTrees_TreePicker_Default_Hedge_Width.Name = "rowAmenityTrees_TreePicker_Default_Hedge_Width";
            this.rowAmenityTrees_TreePicker_Default_Hedge_Width.Properties.Caption = "Adding From Map - Default Hedge Width";
            this.rowAmenityTrees_TreePicker_Default_Hedge_Width.Properties.FieldName = "AmenityTrees_TreePicker_Default_Hedge_Width";
            this.rowAmenityTrees_TreePicker_Default_Hedge_Width.Properties.RowEdit = this.repositoryItemSpinEdit5;
            // 
            // rowMappingShowAmenityTreeObjects
            // 
            this.rowMappingShowAmenityTreeObjects.Name = "rowMappingShowAmenityTreeObjects";
            this.rowMappingShowAmenityTreeObjects.Properties.Caption = "Show Amenity Tree Map Objects Page";
            this.rowMappingShowAmenityTreeObjects.Properties.FieldName = "MappingShowAmenityTreeObjects";
            this.rowMappingShowAmenityTreeObjects.Properties.RowEdit = this.ManagerLoadDataOnOpenRepositoryItemComboBox;
            // 
            // rowMappingShowEstatePlanObjects
            // 
            this.rowMappingShowEstatePlanObjects.Name = "rowMappingShowEstatePlanObjects";
            this.rowMappingShowEstatePlanObjects.Properties.Caption = "Show Asset Management Map Objects Page";
            this.rowMappingShowEstatePlanObjects.Properties.FieldName = "MappingShowEstatePlanObjects";
            this.rowMappingShowEstatePlanObjects.Properties.RowEdit = this.ManagerLoadDataOnOpenRepositoryItemComboBox;
            // 
            // categoryRow20
            // 
            this.categoryRow20.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTrees_TreePicker_Tree_Ref_Format_Used});
            this.categoryRow20.Name = "categoryRow20";
            this.categoryRow20.Properties.Caption = "Map Object Labelling";
            // 
            // rowAmenityTrees_TreePicker_Tree_Ref_Format_Used
            // 
            this.rowAmenityTrees_TreePicker_Tree_Ref_Format_Used.Name = "rowAmenityTrees_TreePicker_Tree_Ref_Format_Used";
            this.rowAmenityTrees_TreePicker_Tree_Ref_Format_Used.Properties.Caption = "Default Tree Reference Structure:";
            this.rowAmenityTrees_TreePicker_Tree_Ref_Format_Used.Properties.FieldName = "AmenityTrees_TreePicker_Tree_Ref_Format_Used";
            this.rowAmenityTrees_TreePicker_Tree_Ref_Format_Used.Properties.RowEdit = this.repositoryItemPopupContainerEdit2;
            // 
            // categoryRow19
            // 
            this.categoryRow19.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTrees_TreePicker_Thematic_Band_Width});
            this.categoryRow19.Name = "categoryRow19";
            this.categoryRow19.Properties.Caption = "Thematics";
            // 
            // rowAmenityTrees_TreePicker_Thematic_Band_Width
            // 
            this.rowAmenityTrees_TreePicker_Thematic_Band_Width.Name = "rowAmenityTrees_TreePicker_Thematic_Band_Width";
            this.rowAmenityTrees_TreePicker_Thematic_Band_Width.Properties.Caption = "Default Continuous Variable Band Width";
            this.rowAmenityTrees_TreePicker_Thematic_Band_Width.Properties.FieldName = "AmenityTrees_TreePicker_Thematic_Band_Width";
            this.rowAmenityTrees_TreePicker_Thematic_Band_Width.Properties.RowEdit = this.repositoryItemSpinEdit3;
            // 
            // categoryRow17
            // 
            this.categoryRow17.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTrees_TreePicker_GPS_CrossHair_Colour,
            this.categoryRow12});
            this.categoryRow17.Name = "categoryRow17";
            this.categoryRow17.Properties.Caption = "GPS";
            // 
            // rowAmenityTrees_TreePicker_GPS_CrossHair_Colour
            // 
            this.rowAmenityTrees_TreePicker_GPS_CrossHair_Colour.Name = "rowAmenityTrees_TreePicker_GPS_CrossHair_Colour";
            this.rowAmenityTrees_TreePicker_GPS_CrossHair_Colour.Properties.Caption = "Crosshair Colour";
            this.rowAmenityTrees_TreePicker_GPS_CrossHair_Colour.Properties.FieldName = "AmenityTrees_TreePicker_GPS_CrossHair_Colour";
            this.rowAmenityTrees_TreePicker_GPS_CrossHair_Colour.Properties.RowEdit = this.repositoryItemColorEdit1;
            // 
            // categoryRow12
            // 
            this.categoryRow12.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour,
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style,
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width});
            this.categoryRow12.Name = "categoryRow12";
            this.categoryRow12.Properties.Caption = "Polygon \\ Polyline Plotter";
            // 
            // rowAmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour
            // 
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour.Name = "rowAmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour";
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour.Properties.Caption = "Line Colour";
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour.Properties.FieldName = "AmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour";
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour.Properties.RowEdit = this.repositoryItemColorEdit2;
            // 
            // rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style
            // 
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style.Name = "rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style";
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style.Properties.Caption = "Line Style";
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style.Properties.FieldName = "AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style";
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style.Properties.RowEdit = this.repositoryItemImageComboBox1;
            // 
            // rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width
            // 
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width.Name = "rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width";
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width.Properties.Caption = "Line Width";
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width.Properties.FieldName = "AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width";
            this.rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width.Properties.RowEdit = this.repositoryItemSpinEdit1;
            // 
            // categoryRow18
            // 
            this.categoryRow18.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius,
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour,
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency});
            this.categoryRow18.Name = "categoryRow18";
            this.categoryRow18.Properties.Caption = "Gazetteer";
            // 
            // rowAmenityTrees_TreePicker_Gazetteer_Search_Radius
            // 
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius.Name = "rowAmenityTrees_TreePicker_Gazetteer_Search_Radius";
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius.Properties.Caption = "Default Search Distance";
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius.Properties.FieldName = "AmenityTrees_TreePicker_Gazetteer_Search_Radius";
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius.Properties.RowEdit = this.repositoryItemSpinEdit4;
            // 
            // rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour
            // 
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour.Name = "rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour";
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour.Properties.Caption = "Search Area Colour";
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour.Properties.FieldName = "AmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour";
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour.Properties.RowEdit = this.repositoryItemColorEdit4;
            // 
            // rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency
            // 
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency.Name = "rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency";
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency.Properties.Caption = "Search Area Transparency";
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency.Properties.FieldName = "AmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency";
            this.rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency.Properties.RowEdit = this.repositoryItemTrackBar1;
            // 
            // categoryRow14
            // 
            this.categoryRow14.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesTreeManagerLoadTreesOnOpen,
            this.categoryRow15,
            this.categoryRow24});
            this.categoryRow14.Name = "categoryRow14";
            this.categoryRow14.Properties.Caption = "Tree Level Settings";
            // 
            // rowAmenityTreesTreeManagerLoadTreesOnOpen
            // 
            this.rowAmenityTreesTreeManagerLoadTreesOnOpen.Name = "rowAmenityTreesTreeManagerLoadTreesOnOpen";
            this.rowAmenityTreesTreeManagerLoadTreesOnOpen.Properties.Caption = "Tree Manager Load Data On Open";
            this.rowAmenityTreesTreeManagerLoadTreesOnOpen.Properties.FieldName = "AmenityTreesTreeManagerLoadTreesOnOpen";
            this.rowAmenityTreesTreeManagerLoadTreesOnOpen.Properties.RowEdit = this.ManagerLoadDataOnOpenRepositoryItemComboBox;
            // 
            // categoryRow15
            // 
            this.categoryRow15.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTrees_Sequence_From_Locality_Length,
            this.rowAmenityTrees_Sequence_From_Locality_Seperator});
            this.categoryRow15.Height = 17;
            this.categoryRow15.Name = "categoryRow15";
            this.categoryRow15.Properties.Caption = "Sequences";
            // 
            // rowAmenityTrees_Sequence_From_Locality_Length
            // 
            this.rowAmenityTrees_Sequence_From_Locality_Length.Name = "rowAmenityTrees_Sequence_From_Locality_Length";
            this.rowAmenityTrees_Sequence_From_Locality_Length.Properties.Caption = "Numeric Length using Locality Code";
            this.rowAmenityTrees_Sequence_From_Locality_Length.Properties.FieldName = "AmenityTrees_Sequence_From_Locality_Length";
            this.rowAmenityTrees_Sequence_From_Locality_Length.Properties.RowEdit = this.repositoryItemSpinEdit2;
            // 
            // rowAmenityTrees_Sequence_From_Locality_Seperator
            // 
            this.rowAmenityTrees_Sequence_From_Locality_Seperator.Name = "rowAmenityTrees_Sequence_From_Locality_Seperator";
            this.rowAmenityTrees_Sequence_From_Locality_Seperator.Properties.Caption = "Seperator Character [Optional]";
            this.rowAmenityTrees_Sequence_From_Locality_Seperator.Properties.FieldName = "AmenityTrees_Sequence_From_Locality_Seperator";
            this.rowAmenityTrees_Sequence_From_Locality_Seperator.Properties.RowEdit = this.repositoryItemTextEdit4;
            // 
            // categoryRow24
            // 
            this.categoryRow24.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesCavatCalculation,
            this.rowAmenityTreesCavatCalculationMethod,
            this.rowAmenityTreesCavatDefaultUnitValueFactor});
            this.categoryRow24.Name = "categoryRow24";
            this.categoryRow24.Properties.Caption = "CAVAT";
            // 
            // rowAmenityTreesCavatCalculation
            // 
            this.rowAmenityTreesCavatCalculation.Name = "rowAmenityTreesCavatCalculation";
            this.rowAmenityTreesCavatCalculation.Properties.Caption = "Calculation Status";
            this.rowAmenityTreesCavatCalculation.Properties.FieldName = "AmenityTreesCavatCalculation";
            this.rowAmenityTreesCavatCalculation.Properties.RowEdit = this.repositoryItemComboBox1;
            // 
            // rowAmenityTreesCavatCalculationMethod
            // 
            this.rowAmenityTreesCavatCalculationMethod.Name = "rowAmenityTreesCavatCalculationMethod";
            this.rowAmenityTreesCavatCalculationMethod.Properties.Caption = "Calculation Method";
            this.rowAmenityTreesCavatCalculationMethod.Properties.FieldName = "AmenityTreesCavatCalculationMethod";
            this.rowAmenityTreesCavatCalculationMethod.Properties.RowEdit = this.repositoryItemComboBox3;
            // 
            // rowAmenityTreesCavatDefaultUnitValueFactor
            // 
            this.rowAmenityTreesCavatDefaultUnitValueFactor.Name = "rowAmenityTreesCavatDefaultUnitValueFactor";
            this.rowAmenityTreesCavatDefaultUnitValueFactor.Properties.Caption = "Default Unit Value Factor";
            this.rowAmenityTreesCavatDefaultUnitValueFactor.Properties.FieldName = "AmenityTreesCavatDefaultUnitValueFactor";
            this.rowAmenityTreesCavatDefaultUnitValueFactor.Properties.RowEdit = this.repositoryItemTextEdit6;
            // 
            // categoryRow25
            // 
            this.categoryRow25.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesInspectionManagerLoadInspectionsOnOpen});
            this.categoryRow25.Name = "categoryRow25";
            this.categoryRow25.Properties.Caption = "Inspection Level Settings";
            // 
            // rowAmenityTreesInspectionManagerLoadInspectionsOnOpen
            // 
            this.rowAmenityTreesInspectionManagerLoadInspectionsOnOpen.Name = "rowAmenityTreesInspectionManagerLoadInspectionsOnOpen";
            this.rowAmenityTreesInspectionManagerLoadInspectionsOnOpen.Properties.Caption = "Inspection Manager Load Data On Open";
            this.rowAmenityTreesInspectionManagerLoadInspectionsOnOpen.Properties.FieldName = "AmenityTreesInspectionManagerLoadInspectionsOnOpen";
            this.rowAmenityTreesInspectionManagerLoadInspectionsOnOpen.Properties.RowEdit = this.ManagerLoadDataOnOpenRepositoryItemComboBox;
            // 
            // categoryRow26
            // 
            this.categoryRow26.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowAmenityTreesActionManagerLoadActionsOnOpen});
            this.categoryRow26.Height = 17;
            this.categoryRow26.Name = "categoryRow26";
            this.categoryRow26.Properties.Caption = "Action Level Settings";
            // 
            // rowAmenityTreesActionManagerLoadActionsOnOpen
            // 
            this.rowAmenityTreesActionManagerLoadActionsOnOpen.Name = "rowAmenityTreesActionManagerLoadActionsOnOpen";
            this.rowAmenityTreesActionManagerLoadActionsOnOpen.Properties.Caption = "Action Manager Load Data On Open";
            this.rowAmenityTreesActionManagerLoadActionsOnOpen.Properties.FieldName = "AmenityTreesActionManagerLoadActionsOnOpen";
            this.rowAmenityTreesActionManagerLoadActionsOnOpen.Properties.RowEdit = this.ManagerLoadDataOnOpenRepositoryItemComboBox;
            // 
            // categoryUtilities
            // 
            this.categoryUtilities.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowDataSynchronisationMachineID,
            this.rowSavedReferenceFiles,
            this.rowUtilitiesSavedReportLayouts,
            this.rowSavedMaps,
            this.rowSavedPictures,
            this.rowSavedSignatures,
            this.categoryPermissionDocuments,
            this.categoryWorkOrderDocuments});
            this.categoryUtilities.Name = "categoryUtilities";
            this.categoryUtilities.Properties.Caption = "Utilities";
            // 
            // rowDataSynchronisationMachineID
            // 
            this.rowDataSynchronisationMachineID.Name = "rowDataSynchronisationMachineID";
            this.rowDataSynchronisationMachineID.Properties.Caption = "Data Sychronisation Machine ID";
            this.rowDataSynchronisationMachineID.Properties.FieldName = "DataSynchronisationMachineID";
            this.rowDataSynchronisationMachineID.Properties.RowEdit = this.repositoryItemSpinEditDataSyncMachineID;
            // 
            // rowSavedReferenceFiles
            // 
            this.rowSavedReferenceFiles.Name = "rowSavedReferenceFiles";
            this.rowSavedReferenceFiles.Properties.Caption = "Saved Reference Files Folder";
            this.rowSavedReferenceFiles.Properties.FieldName = "SavedReferenceFiles";
            this.rowSavedReferenceFiles.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowUtilitiesSavedReportLayouts
            // 
            this.rowUtilitiesSavedReportLayouts.Name = "rowUtilitiesSavedReportLayouts";
            this.rowUtilitiesSavedReportLayouts.Properties.Caption = "Report Layouts Folder";
            this.rowUtilitiesSavedReportLayouts.Properties.FieldName = "UtilitiesSavedReportLayouts";
            this.rowUtilitiesSavedReportLayouts.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowSavedMaps
            // 
            this.rowSavedMaps.Name = "rowSavedMaps";
            this.rowSavedMaps.Properties.Caption = "Saved Maps Folder";
            this.rowSavedMaps.Properties.FieldName = "SavedMaps";
            this.rowSavedMaps.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowSavedPictures
            // 
            this.rowSavedPictures.Name = "rowSavedPictures";
            this.rowSavedPictures.Properties.Caption = "Saved Pictures Folder";
            this.rowSavedPictures.Properties.FieldName = "SavedPictures";
            this.rowSavedPictures.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowSavedSignatures
            // 
            this.rowSavedSignatures.Name = "rowSavedSignatures";
            this.rowSavedSignatures.Properties.Caption = "Saved Signatures Folder";
            this.rowSavedSignatures.Properties.FieldName = "SavedSignatures";
            this.rowSavedSignatures.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryPermissionDocuments
            // 
            this.categoryPermissionDocuments.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowSavedPermissionDocuments,
            this.categoryEmailFromAddress,
            this.categoryEmalCCAddress,
            this.categoryEmailLayoutHTMLFile,
            this.categoryEmailSubjectLine});
            this.categoryPermissionDocuments.Name = "categoryPermissionDocuments";
            this.categoryPermissionDocuments.Properties.Caption = "Permission Documents";
            // 
            // rowSavedPermissionDocuments
            // 
            this.rowSavedPermissionDocuments.Name = "rowSavedPermissionDocuments";
            this.rowSavedPermissionDocuments.Properties.Caption = "Saved Permission Documents Folder";
            this.rowSavedPermissionDocuments.Properties.FieldName = "SavedPermissionDocuments";
            this.rowSavedPermissionDocuments.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryEmailFromAddress
            // 
            this.categoryEmailFromAddress.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowPermissionDocumentEmailFromAddressClient,
            this.rowPermissionDocumentEmailFromAddressCustomer});
            this.categoryEmailFromAddress.Name = "categoryEmailFromAddress";
            this.categoryEmailFromAddress.Properties.Caption = "Email From Address";
            // 
            // rowPermissionDocumentEmailFromAddressClient
            // 
            this.rowPermissionDocumentEmailFromAddressClient.Name = "rowPermissionDocumentEmailFromAddressClient";
            this.rowPermissionDocumentEmailFromAddressClient.Properties.Caption = "Client";
            this.rowPermissionDocumentEmailFromAddressClient.Properties.FieldName = "PermissionDocumentEmailFromAddressClient";
            this.rowPermissionDocumentEmailFromAddressClient.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // rowPermissionDocumentEmailFromAddressCustomer
            // 
            this.rowPermissionDocumentEmailFromAddressCustomer.Name = "rowPermissionDocumentEmailFromAddressCustomer";
            this.rowPermissionDocumentEmailFromAddressCustomer.Properties.Caption = "Customer";
            this.rowPermissionDocumentEmailFromAddressCustomer.Properties.FieldName = "PermissionDocumentEmailFromAddressCustomer";
            this.rowPermissionDocumentEmailFromAddressCustomer.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // categoryEmalCCAddress
            // 
            this.categoryEmalCCAddress.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowPermissionDocumentEmailCCAddressClient,
            this.rowPermissionDocumentEmailCCAddressCustomer});
            this.categoryEmalCCAddress.Name = "categoryEmalCCAddress";
            this.categoryEmalCCAddress.Properties.Caption = "Email CC Address";
            // 
            // rowPermissionDocumentEmailCCAddressClient
            // 
            this.rowPermissionDocumentEmailCCAddressClient.Name = "rowPermissionDocumentEmailCCAddressClient";
            this.rowPermissionDocumentEmailCCAddressClient.Properties.Caption = "Client";
            this.rowPermissionDocumentEmailCCAddressClient.Properties.FieldName = "PermissionDocumentEmailCCAddressClient";
            this.rowPermissionDocumentEmailCCAddressClient.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // rowPermissionDocumentEmailCCAddressCustomer
            // 
            this.rowPermissionDocumentEmailCCAddressCustomer.Name = "rowPermissionDocumentEmailCCAddressCustomer";
            this.rowPermissionDocumentEmailCCAddressCustomer.Properties.Caption = "Customer";
            this.rowPermissionDocumentEmailCCAddressCustomer.Properties.FieldName = "PermissionDocumentEmailCCAddressCustomer";
            this.rowPermissionDocumentEmailCCAddressCustomer.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // categoryEmailLayoutHTMLFile
            // 
            this.categoryEmailLayoutHTMLFile.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowPermissionDocumentEmailHTMLLayoutFileClient,
            this.rowPermissionDocumentEmailHTMLLayoutFileCustomer});
            this.categoryEmailLayoutHTMLFile.Name = "categoryEmailLayoutHTMLFile";
            this.categoryEmailLayoutHTMLFile.Properties.Caption = "Email HTML Layout File";
            // 
            // rowPermissionDocumentEmailHTMLLayoutFileClient
            // 
            this.rowPermissionDocumentEmailHTMLLayoutFileClient.Name = "rowPermissionDocumentEmailHTMLLayoutFileClient";
            this.rowPermissionDocumentEmailHTMLLayoutFileClient.Properties.Caption = "Client";
            this.rowPermissionDocumentEmailHTMLLayoutFileClient.Properties.FieldName = "PermissionDocumentEmailHTMLLayoutFileClient";
            this.rowPermissionDocumentEmailHTMLLayoutFileClient.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowPermissionDocumentEmailHTMLLayoutFileCustomer
            // 
            this.rowPermissionDocumentEmailHTMLLayoutFileCustomer.Name = "rowPermissionDocumentEmailHTMLLayoutFileCustomer";
            this.rowPermissionDocumentEmailHTMLLayoutFileCustomer.Properties.Caption = "Customer";
            this.rowPermissionDocumentEmailHTMLLayoutFileCustomer.Properties.FieldName = "PermissionDocumentEmailHTMLLayoutFileCustomer";
            this.rowPermissionDocumentEmailHTMLLayoutFileCustomer.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // categoryEmailSubjectLine
            // 
            this.categoryEmailSubjectLine.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowPermissionDocumentEmailSubjectLineClient,
            this.rowPermissionDocumentEmailSubjectLineCustomer});
            this.categoryEmailSubjectLine.Name = "categoryEmailSubjectLine";
            this.categoryEmailSubjectLine.Properties.Caption = "Email Subject Line";
            // 
            // rowPermissionDocumentEmailSubjectLineClient
            // 
            this.rowPermissionDocumentEmailSubjectLineClient.Name = "rowPermissionDocumentEmailSubjectLineClient";
            this.rowPermissionDocumentEmailSubjectLineClient.Properties.Caption = "Client";
            this.rowPermissionDocumentEmailSubjectLineClient.Properties.FieldName = "PermissionDocumentEmailSubjectLineClient";
            this.rowPermissionDocumentEmailSubjectLineClient.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // rowPermissionDocumentEmailSubjectLineCustomer
            // 
            this.rowPermissionDocumentEmailSubjectLineCustomer.Name = "rowPermissionDocumentEmailSubjectLineCustomer";
            this.rowPermissionDocumentEmailSubjectLineCustomer.Properties.Caption = "Customer";
            this.rowPermissionDocumentEmailSubjectLineCustomer.Properties.FieldName = "PermissionDocumentEmailSubjectLineCustomer";
            this.rowPermissionDocumentEmailSubjectLineCustomer.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // categoryWorkOrderDocuments
            // 
            this.categoryWorkOrderDocuments.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowSavedWorkOrderDocuments,
            this.rowWorkOrderDocumentEmailFromAddress,
            this.rowWorkOrderDocumentEmailCCAddress,
            this.rowWorkOrderDocumentEmailHTMLLayoutFile,
            this.rowWorkOrderDocumentEmailSubjectLine});
            this.categoryWorkOrderDocuments.Name = "categoryWorkOrderDocuments";
            this.categoryWorkOrderDocuments.Properties.Caption = "Work Order Documents";
            // 
            // rowSavedWorkOrderDocuments
            // 
            this.rowSavedWorkOrderDocuments.Name = "rowSavedWorkOrderDocuments";
            this.rowSavedWorkOrderDocuments.Properties.Caption = "Saved Work Order Documents Folder";
            this.rowSavedWorkOrderDocuments.Properties.FieldName = "SavedWorkOrderDocuments";
            this.rowSavedWorkOrderDocuments.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowWorkOrderDocumentEmailFromAddress
            // 
            this.rowWorkOrderDocumentEmailFromAddress.Name = "rowWorkOrderDocumentEmailFromAddress";
            this.rowWorkOrderDocumentEmailFromAddress.Properties.Caption = "Email From Address";
            this.rowWorkOrderDocumentEmailFromAddress.Properties.FieldName = "WorkOrderDocumentEmailFromAddress";
            this.rowWorkOrderDocumentEmailFromAddress.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // rowWorkOrderDocumentEmailCCAddress
            // 
            this.rowWorkOrderDocumentEmailCCAddress.Name = "rowWorkOrderDocumentEmailCCAddress";
            this.rowWorkOrderDocumentEmailCCAddress.Properties.Caption = "Email CC Address";
            this.rowWorkOrderDocumentEmailCCAddress.Properties.FieldName = "WorkOrderDocumentEmailCCAddress";
            this.rowWorkOrderDocumentEmailCCAddress.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // rowWorkOrderDocumentEmailHTMLLayoutFile
            // 
            this.rowWorkOrderDocumentEmailHTMLLayoutFile.Name = "rowWorkOrderDocumentEmailHTMLLayoutFile";
            this.rowWorkOrderDocumentEmailHTMLLayoutFile.Properties.Caption = "Email HTML Layout File";
            this.rowWorkOrderDocumentEmailHTMLLayoutFile.Properties.FieldName = "WorkOrderDocumentEmailHTMLLayoutFile";
            this.rowWorkOrderDocumentEmailHTMLLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowWorkOrderDocumentEmailSubjectLine
            // 
            this.rowWorkOrderDocumentEmailSubjectLine.Name = "rowWorkOrderDocumentEmailSubjectLine";
            this.rowWorkOrderDocumentEmailSubjectLine.Properties.Caption = "Email Subject Line";
            this.rowWorkOrderDocumentEmailSubjectLine.Properties.FieldName = "WorkOrderDocumentEmailSubjectLine";
            this.rowWorkOrderDocumentEmailSubjectLine.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // categoryRowInformationCentre
            // 
            this.categoryRowInformationCentre.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow28,
            this.categoryRow29,
            this.categoryRow30,
            this.categoryRow31,
            this.categoryRowWinterMaintenance,
            this.categoryTenderRegister,
            this.categorySummerMainteance});
            this.categoryRowInformationCentre.Name = "categoryRowInformationCentre";
            this.categoryRowInformationCentre.Properties.Caption = "Information Centre";
            // 
            // categoryRow28
            // 
            this.categoryRow28.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowEstatePlanInspectionManagerLoadInspectionsOnOpen});
            this.categoryRow28.Name = "categoryRow28";
            this.categoryRow28.Properties.Caption = "Inspection Level Settings";
            // 
            // rowEstatePlanInspectionManagerLoadInspectionsOnOpen
            // 
            this.rowEstatePlanInspectionManagerLoadInspectionsOnOpen.Name = "rowEstatePlanInspectionManagerLoadInspectionsOnOpen";
            this.rowEstatePlanInspectionManagerLoadInspectionsOnOpen.Properties.Caption = "Inspection Manager Load Data On Open";
            this.rowEstatePlanInspectionManagerLoadInspectionsOnOpen.Properties.FieldName = "EstatePlanInspectionManagerLoadInspectionsOnOpen";
            this.rowEstatePlanInspectionManagerLoadInspectionsOnOpen.Properties.RowEdit = this.ManagerLoadDataOnOpenRepositoryItemComboBox;
            // 
            // categoryRow29
            // 
            this.categoryRow29.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowEstatePlanAssetManagerLoadAssetsOnOpen});
            this.categoryRow29.Name = "categoryRow29";
            this.categoryRow29.Properties.Caption = "Asset Level Settings";
            // 
            // rowEstatePlanAssetManagerLoadAssetsOnOpen
            // 
            this.rowEstatePlanAssetManagerLoadAssetsOnOpen.Name = "rowEstatePlanAssetManagerLoadAssetsOnOpen";
            this.rowEstatePlanAssetManagerLoadAssetsOnOpen.Properties.Caption = "Asset Manager Load Data On Open";
            this.rowEstatePlanAssetManagerLoadAssetsOnOpen.Properties.FieldName = "EstatePlanAssetManagerLoadAssetsOnOpen";
            this.rowEstatePlanAssetManagerLoadAssetsOnOpen.Properties.RowEdit = this.ManagerLoadDataOnOpenRepositoryItemComboBox;
            // 
            // categoryRow30
            // 
            this.categoryRow30.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowEstatePlanActionManagerLoadActionsOnOpen});
            this.categoryRow30.Name = "categoryRow30";
            this.categoryRow30.Properties.Caption = "Action Level Settings";
            // 
            // rowEstatePlanActionManagerLoadActionsOnOpen
            // 
            this.rowEstatePlanActionManagerLoadActionsOnOpen.Name = "rowEstatePlanActionManagerLoadActionsOnOpen";
            this.rowEstatePlanActionManagerLoadActionsOnOpen.Properties.Caption = "Action Manager Load Data On Open";
            this.rowEstatePlanActionManagerLoadActionsOnOpen.Properties.FieldName = "EstatePlanActionManagerLoadActionsOnOpen";
            this.rowEstatePlanActionManagerLoadActionsOnOpen.Properties.RowEdit = this.ManagerLoadDataOnOpenRepositoryItemComboBox;
            // 
            // categoryRow31
            // 
            this.categoryRow31.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGroundControlSiteDrawingsPath});
            this.categoryRow31.Name = "categoryRow31";
            this.categoryRow31.Properties.Caption = "Site Level Settings";
            // 
            // rowGroundControlSiteDrawingsPath
            // 
            this.rowGroundControlSiteDrawingsPath.Name = "rowGroundControlSiteDrawingsPath";
            this.rowGroundControlSiteDrawingsPath.Properties.Caption = "Site Drawings Folder";
            this.rowGroundControlSiteDrawingsPath.Properties.FieldName = "GroundControlSiteDrawingsPath";
            this.rowGroundControlSiteDrawingsPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryRowWinterMaintenance
            // 
            this.categoryRowWinterMaintenance.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowSMTPMailServerAddress,
            this.rowSMTPMailServerUsername,
            this.rowSMTPMailServerPassword,
            this.rowSMTPMailServerPort,
            this.rowTextAnywhereUsername,
            this.rowTextAnywherePassword,
            this.rowGrittingDefaultVatRate,
            this.rowSaltStockControlPeriodID,
            this.rowGrittingCalloutPictureFilesFolder,
            this.rowGrittingCalloutPictureFilesFolderInternal,
            this.rowGrittingSavedCalloutSignatureFolder,
            this.rowGrittingReportLayouts,
            this.categoryRowForecastImport,
            this.categoryFloatingSiteForecastEmail,
            this.categoryRowGrittingEmails,
            this.categoryRowClientAuthorisationEmail,
            this.categoryRowGrittingConfirmations,
            this.categoryRowTeamEmailReport,
            this.categoryRouteOptimization,
            this.categoryGrittingCompletedEmails,
            this.categoryRowSelfBillingInvoices,
            this.categoryRowSnowClearanceTeamPO,
            this.categoryRowFinance,
            this.categoryRowClientJobBreakdownReport,
            this.categoryRowMissingJobSheetNumbers});
            this.categoryRowWinterMaintenance.Name = "categoryRowWinterMaintenance";
            this.categoryRowWinterMaintenance.Properties.Caption = "Winter Maintenance";
            // 
            // rowSMTPMailServerAddress
            // 
            this.rowSMTPMailServerAddress.Name = "rowSMTPMailServerAddress";
            this.rowSMTPMailServerAddress.Properties.Caption = "SMTP Mail Server Address";
            this.rowSMTPMailServerAddress.Properties.FieldName = "SMTPMailServerAddress";
            // 
            // rowSMTPMailServerUsername
            // 
            this.rowSMTPMailServerUsername.Name = "rowSMTPMailServerUsername";
            this.rowSMTPMailServerUsername.Properties.Caption = "SMTP Mail Server Username";
            this.rowSMTPMailServerUsername.Properties.FieldName = "SMTPMailServerUsername";
            // 
            // rowSMTPMailServerPassword
            // 
            this.rowSMTPMailServerPassword.Name = "rowSMTPMailServerPassword";
            this.rowSMTPMailServerPassword.Properties.Caption = "SMTP Mail Server Password";
            this.rowSMTPMailServerPassword.Properties.FieldName = "SMTPMailServerPassword";
            // 
            // rowSMTPMailServerPort
            // 
            this.rowSMTPMailServerPort.Name = "rowSMTPMailServerPort";
            this.rowSMTPMailServerPort.Properties.Caption = "SMTP Mail Server Port";
            this.rowSMTPMailServerPort.Properties.FieldName = "SMTPMailServerPort";
            this.rowSMTPMailServerPort.Properties.RowEdit = this.repositoryItemTextEditNumericOnly;
            // 
            // rowTextAnywhereUsername
            // 
            this.rowTextAnywhereUsername.Name = "rowTextAnywhereUsername";
            this.rowTextAnywhereUsername.Properties.Caption = "TextAnyWhere Username";
            this.rowTextAnywhereUsername.Properties.FieldName = "TextAnywhereUsername";
            // 
            // rowTextAnywherePassword
            // 
            this.rowTextAnywherePassword.Name = "rowTextAnywherePassword";
            this.rowTextAnywherePassword.Properties.Caption = "TextAnyWhere Password";
            this.rowTextAnywherePassword.Properties.FieldName = "TextAnywherePassword";
            // 
            // rowGrittingDefaultVatRate
            // 
            this.rowGrittingDefaultVatRate.Name = "rowGrittingDefaultVatRate";
            this.rowGrittingDefaultVatRate.Properties.Caption = "Default VAT Rate";
            this.rowGrittingDefaultVatRate.Properties.FieldName = "GrittingDefaultVatRate";
            this.rowGrittingDefaultVatRate.Properties.RowEdit = this.repositoryItemSpinEdit6;
            // 
            // rowSaltStockControlPeriodID
            // 
            this.rowSaltStockControlPeriodID.Name = "rowSaltStockControlPeriodID";
            this.rowSaltStockControlPeriodID.Properties.Caption = "Salt Stock Control Period";
            this.rowSaltStockControlPeriodID.Properties.FieldName = "SaltStockControlPeriodID";
            this.rowSaltStockControlPeriodID.Properties.RowEdit = this.repositoryItemLookUpEdit4;
            // 
            // rowGrittingCalloutPictureFilesFolder
            // 
            this.rowGrittingCalloutPictureFilesFolder.Name = "rowGrittingCalloutPictureFilesFolder";
            this.rowGrittingCalloutPictureFilesFolder.Properties.Caption = "Callout Pictures Folder [External]";
            this.rowGrittingCalloutPictureFilesFolder.Properties.FieldName = "GrittingCalloutPictureFilesFolder";
            this.rowGrittingCalloutPictureFilesFolder.Properties.RowEdit = this.repositoryItemTextEditCalloutPicturesExternal;
            // 
            // rowGrittingCalloutPictureFilesFolderInternal
            // 
            this.rowGrittingCalloutPictureFilesFolderInternal.Name = "rowGrittingCalloutPictureFilesFolderInternal";
            this.rowGrittingCalloutPictureFilesFolderInternal.Properties.Caption = "Callout Pictures Folder [Internal]";
            this.rowGrittingCalloutPictureFilesFolderInternal.Properties.FieldName = "GrittingCalloutPictureFilesFolderInternal";
            this.rowGrittingCalloutPictureFilesFolderInternal.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowGrittingSavedCalloutSignatureFolder
            // 
            this.rowGrittingSavedCalloutSignatureFolder.Name = "rowGrittingSavedCalloutSignatureFolder";
            this.rowGrittingSavedCalloutSignatureFolder.Properties.Caption = "Gritting Saved Client Signatures Folder";
            this.rowGrittingSavedCalloutSignatureFolder.Properties.FieldName = "GrittingSavedCalloutSignatureFolder";
            this.rowGrittingSavedCalloutSignatureFolder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowGrittingReportLayouts
            // 
            this.rowGrittingReportLayouts.Name = "rowGrittingReportLayouts";
            this.rowGrittingReportLayouts.Properties.Caption = "Gritting Report Layouts Folder";
            this.rowGrittingReportLayouts.Properties.FieldName = "GrittingReportLayouts";
            this.rowGrittingReportLayouts.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryRowForecastImport
            // 
            this.categoryRowForecastImport.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGrittingForecastType1FilePath,
            this.rowGrittingForecastType2FilePath,
            this.rowGrittingForecastTypeFloatingFilePath,
            this.rowGrittingFloatingJobsFilePath});
            this.categoryRowForecastImport.Name = "categoryRowForecastImport";
            this.categoryRowForecastImport.Properties.Caption = "Import Weather Forecasts";
            // 
            // rowGrittingForecastType1FilePath
            // 
            this.rowGrittingForecastType1FilePath.Name = "rowGrittingForecastType1FilePath";
            this.rowGrittingForecastType1FilePath.Properties.Caption = "Forecast Type 1 [XLS] File Location";
            this.rowGrittingForecastType1FilePath.Properties.FieldName = "GrittingForecastType1FilePath";
            this.rowGrittingForecastType1FilePath.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowGrittingForecastType2FilePath
            // 
            this.rowGrittingForecastType2FilePath.Name = "rowGrittingForecastType2FilePath";
            this.rowGrittingForecastType2FilePath.Properties.Caption = "Forecast Type 2 [CSV] File Location";
            this.rowGrittingForecastType2FilePath.Properties.FieldName = "GrittingForecastType2FilePath";
            this.rowGrittingForecastType2FilePath.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowGrittingForecastTypeFloatingFilePath
            // 
            this.rowGrittingForecastTypeFloatingFilePath.Name = "rowGrittingForecastTypeFloatingFilePath";
            this.rowGrittingForecastTypeFloatingFilePath.Properties.Caption = "Floating Sites Forecast [CSV] File Location";
            this.rowGrittingForecastTypeFloatingFilePath.Properties.FieldName = "GrittingForecastTypeFloatingFilePath";
            this.rowGrittingForecastTypeFloatingFilePath.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowGrittingFloatingJobsFilePath
            // 
            this.rowGrittingFloatingJobsFilePath.Name = "rowGrittingFloatingJobsFilePath";
            this.rowGrittingFloatingJobsFilePath.Properties.Caption = "Floating Site Jobs [CSV] File Location";
            this.rowGrittingFloatingJobsFilePath.Properties.FieldName = "GrittingFloatingJobsFilePath";
            this.rowGrittingFloatingJobsFilePath.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // categoryFloatingSiteForecastEmail
            // 
            this.categoryFloatingSiteForecastEmail.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGrittingFloatingSiteEmailHtmlLayoutFile,
            this.rowGrittingFloatingSiteEmailSubjectLine,
            this.rowGrittingFloatingSiteEmailFromAddress,
            this.rowGrittingFloatingSiteEmailCCToEmailAddress});
            this.categoryFloatingSiteForecastEmail.Name = "categoryFloatingSiteForecastEmail";
            this.categoryFloatingSiteForecastEmail.Properties.Caption = "Floating Site Forecast Client Email";
            // 
            // rowGrittingFloatingSiteEmailHtmlLayoutFile
            // 
            this.rowGrittingFloatingSiteEmailHtmlLayoutFile.Name = "rowGrittingFloatingSiteEmailHtmlLayoutFile";
            this.rowGrittingFloatingSiteEmailHtmlLayoutFile.Properties.Caption = "HTML Body Layout File";
            this.rowGrittingFloatingSiteEmailHtmlLayoutFile.Properties.FieldName = "GrittingFloatingSiteEmailHtmlLayoutFile";
            this.rowGrittingFloatingSiteEmailHtmlLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowGrittingFloatingSiteEmailSubjectLine
            // 
            this.rowGrittingFloatingSiteEmailSubjectLine.Name = "rowGrittingFloatingSiteEmailSubjectLine";
            this.rowGrittingFloatingSiteEmailSubjectLine.Properties.Caption = "Subject Line";
            this.rowGrittingFloatingSiteEmailSubjectLine.Properties.FieldName = "GrittingFloatingSiteEmailSubjectLine";
            // 
            // rowGrittingFloatingSiteEmailFromAddress
            // 
            this.rowGrittingFloatingSiteEmailFromAddress.Name = "rowGrittingFloatingSiteEmailFromAddress";
            this.rowGrittingFloatingSiteEmailFromAddress.Properties.Caption = "From Email Address";
            this.rowGrittingFloatingSiteEmailFromAddress.Properties.FieldName = "GrittingFloatingSiteEmailFromAddress";
            // 
            // rowGrittingFloatingSiteEmailCCToEmailAddress
            // 
            this.rowGrittingFloatingSiteEmailCCToEmailAddress.Name = "rowGrittingFloatingSiteEmailCCToEmailAddress";
            this.rowGrittingFloatingSiteEmailCCToEmailAddress.Properties.Caption = "CC To Email Address";
            this.rowGrittingFloatingSiteEmailCCToEmailAddress.Properties.FieldName = "GrittingFloatingSiteEmailCCToEmailAddress";
            // 
            // categoryRowGrittingEmails
            // 
            this.categoryRowGrittingEmails.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGrittingEmailsFromName,
            this.rowGrittingConfirmationCCToEmailAddress,
            this.rowGrittingTeamConfirmationCCToEmailAddress});
            this.categoryRowGrittingEmails.Name = "categoryRowGrittingEmails";
            this.categoryRowGrittingEmails.Properties.Caption = "Gritting Emails";
            // 
            // rowGrittingEmailsFromName
            // 
            this.rowGrittingEmailsFromName.Name = "rowGrittingEmailsFromName";
            this.rowGrittingEmailsFromName.Properties.Caption = "Emails Sent From Default Name";
            this.rowGrittingEmailsFromName.Properties.FieldName = "GrittingEmailsFromName";
            // 
            // rowGrittingConfirmationCCToEmailAddress
            // 
            this.rowGrittingConfirmationCCToEmailAddress.Name = "rowGrittingConfirmationCCToEmailAddress";
            this.rowGrittingConfirmationCCToEmailAddress.Properties.Caption = "Client Emails CC to Email Address";
            this.rowGrittingConfirmationCCToEmailAddress.Properties.FieldName = "GrittingConfirmationCCToEmailAddress";
            // 
            // rowGrittingTeamConfirmationCCToEmailAddress
            // 
            this.rowGrittingTeamConfirmationCCToEmailAddress.Name = "rowGrittingTeamConfirmationCCToEmailAddress";
            this.rowGrittingTeamConfirmationCCToEmailAddress.Properties.Caption = "Team Emails CC to Email Address";
            this.rowGrittingTeamConfirmationCCToEmailAddress.Properties.FieldName = "GrittingTeamConfirmationCCToEmailAddress";
            // 
            // categoryRowClientAuthorisationEmail
            // 
            this.categoryRowClientAuthorisationEmail.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGrittingConfirmationSentPath,
            this.rowGrittingConfirmationReceivedPath,
            this.rowGrittingAuthorisationSentHtmlLayoutFile,
            this.rowGrittingClientEmailAuthorisationSubjectLine,
            this.rowGrittingAuthorisationGeneratedFileType,
            this.rowGrittingAuthorisationOddRowColour,
            this.rowGrittingAuthorisationEvenRowColour});
            this.categoryRowClientAuthorisationEmail.Name = "categoryRowClientAuthorisationEmail";
            this.categoryRowClientAuthorisationEmail.Properties.Caption = "Client Authorisation Email";
            // 
            // rowGrittingConfirmationSentPath
            // 
            this.rowGrittingConfirmationSentPath.Name = "rowGrittingConfirmationSentPath";
            this.rowGrittingConfirmationSentPath.Properties.Caption = "Sent Attachment Saved Folder";
            this.rowGrittingConfirmationSentPath.Properties.FieldName = "GrittingConfirmationSentPath";
            this.rowGrittingConfirmationSentPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowGrittingConfirmationReceivedPath
            // 
            this.rowGrittingConfirmationReceivedPath.Name = "rowGrittingConfirmationReceivedPath";
            this.rowGrittingConfirmationReceivedPath.Properties.Caption = "Reply Attachment Saved Folder";
            this.rowGrittingConfirmationReceivedPath.Properties.FieldName = "GrittingConfirmationReceivedPath";
            this.rowGrittingConfirmationReceivedPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowGrittingAuthorisationSentHtmlLayoutFile
            // 
            this.rowGrittingAuthorisationSentHtmlLayoutFile.Name = "rowGrittingAuthorisationSentHtmlLayoutFile";
            this.rowGrittingAuthorisationSentHtmlLayoutFile.Properties.Caption = "HTML Body Layout File";
            this.rowGrittingAuthorisationSentHtmlLayoutFile.Properties.FieldName = "GrittingAuthorisationSentHtmlLayoutFile";
            this.rowGrittingAuthorisationSentHtmlLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowGrittingClientEmailAuthorisationSubjectLine
            // 
            this.rowGrittingClientEmailAuthorisationSubjectLine.Name = "rowGrittingClientEmailAuthorisationSubjectLine";
            this.rowGrittingClientEmailAuthorisationSubjectLine.Properties.Caption = "Subject Line";
            this.rowGrittingClientEmailAuthorisationSubjectLine.Properties.FieldName = "GrittingClientEmailAuthorisationSubjectLine";
            // 
            // rowGrittingAuthorisationGeneratedFileType
            // 
            this.rowGrittingAuthorisationGeneratedFileType.Name = "rowGrittingAuthorisationGeneratedFileType";
            this.rowGrittingAuthorisationGeneratedFileType.Properties.Caption = "Generated Spreadsheet File Type";
            this.rowGrittingAuthorisationGeneratedFileType.Properties.FieldName = "GrittingAuthorisationGeneratedFileType";
            this.rowGrittingAuthorisationGeneratedFileType.Properties.RowEdit = this.repositoryItemComboBox4;
            // 
            // rowGrittingAuthorisationOddRowColour
            // 
            this.rowGrittingAuthorisationOddRowColour.Name = "rowGrittingAuthorisationOddRowColour";
            this.rowGrittingAuthorisationOddRowColour.Properties.Caption = "Generated Spreadsheet Odd Row Back Colour";
            this.rowGrittingAuthorisationOddRowColour.Properties.FieldName = "GrittingAuthorisationOddRowColour";
            this.rowGrittingAuthorisationOddRowColour.Properties.RowEdit = this.repositoryItemColorEditSpreadsheetRowBackColour;
            // 
            // rowGrittingAuthorisationEvenRowColour
            // 
            this.rowGrittingAuthorisationEvenRowColour.Name = "rowGrittingAuthorisationEvenRowColour";
            this.rowGrittingAuthorisationEvenRowColour.Properties.Caption = "Generated Spreadsheet Even Row Back Colour";
            this.rowGrittingAuthorisationEvenRowColour.Properties.FieldName = "GrittingAuthorisationEvenRowColour";
            this.rowGrittingAuthorisationEvenRowColour.Properties.RowEdit = this.repositoryItemColorEditSpreadsheetRowBackColour;
            // 
            // categoryRowGrittingConfirmations
            // 
            this.categoryRowGrittingConfirmations.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGrittingConfirmationSentHtmlLayoutFile,
            this.rowGrittingClientEmailDailyReportSubjectLine});
            this.categoryRowGrittingConfirmations.Name = "categoryRowGrittingConfirmations";
            this.categoryRowGrittingConfirmations.Properties.Caption = "Client Confirmation Email";
            // 
            // rowGrittingConfirmationSentHtmlLayoutFile
            // 
            this.rowGrittingConfirmationSentHtmlLayoutFile.Name = "rowGrittingConfirmationSentHtmlLayoutFile";
            this.rowGrittingConfirmationSentHtmlLayoutFile.Properties.Caption = "HTML Body Layout File";
            this.rowGrittingConfirmationSentHtmlLayoutFile.Properties.FieldName = "GrittingConfirmationSentHtmlLayoutFile";
            this.rowGrittingConfirmationSentHtmlLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowGrittingClientEmailDailyReportSubjectLine
            // 
            this.rowGrittingClientEmailDailyReportSubjectLine.Name = "rowGrittingClientEmailDailyReportSubjectLine";
            this.rowGrittingClientEmailDailyReportSubjectLine.Properties.Caption = "Subject Line";
            this.rowGrittingClientEmailDailyReportSubjectLine.Properties.FieldName = "GrittingClientEmailDailyReportSubjectLine";
            // 
            // categoryRowTeamEmailReport
            // 
            this.categoryRowTeamEmailReport.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGrittingTeamReportHtmlLayoutFile,
            this.rowTeamEmailReportSubjectLine,
            this.rowGrittingTeamReportEmailFromAddress});
            this.categoryRowTeamEmailReport.Name = "categoryRowTeamEmailReport";
            this.categoryRowTeamEmailReport.Properties.Caption = "Teams Email Report";
            // 
            // rowGrittingTeamReportHtmlLayoutFile
            // 
            this.rowGrittingTeamReportHtmlLayoutFile.Name = "rowGrittingTeamReportHtmlLayoutFile";
            this.rowGrittingTeamReportHtmlLayoutFile.Properties.Caption = "HTML Body Layout File";
            this.rowGrittingTeamReportHtmlLayoutFile.Properties.FieldName = "GrittingTeamReportHtmlLayoutFile";
            this.rowGrittingTeamReportHtmlLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowTeamEmailReportSubjectLine
            // 
            this.rowTeamEmailReportSubjectLine.Name = "rowTeamEmailReportSubjectLine";
            this.rowTeamEmailReportSubjectLine.Properties.Caption = "Subject Line";
            this.rowTeamEmailReportSubjectLine.Properties.FieldName = "TeamEmailReportSubjectLine";
            // 
            // rowGrittingTeamReportEmailFromAddress
            // 
            this.rowGrittingTeamReportEmailFromAddress.Name = "rowGrittingTeamReportEmailFromAddress";
            this.rowGrittingTeamReportEmailFromAddress.Properties.Caption = "Sent From Email Address";
            this.rowGrittingTeamReportEmailFromAddress.Properties.FieldName = "GrittingTeamReportEmailFromAddress";
            // 
            // categoryRouteOptimization
            // 
            this.categoryRouteOptimization.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGrittingRouteOptimisationURL,
            this.rowGrittingRouteOptimisationUsername,
            this.rowGrittingRouteOptimisationPassword,
            this.rowGrittingRouteOptimisationIniFileSavePath,
            this.rowGrittingRouteOptimisationJobFileSavePath,
            this.rowGrittingRouteOptimisationOptimisedRouteSavePath});
            this.categoryRouteOptimization.Name = "categoryRouteOptimization";
            this.categoryRouteOptimization.Properties.Caption = "Route Optimization";
            // 
            // rowGrittingRouteOptimisationURL
            // 
            this.rowGrittingRouteOptimisationURL.Name = "rowGrittingRouteOptimisationURL";
            this.rowGrittingRouteOptimisationURL.Properties.Caption = "Service URL";
            this.rowGrittingRouteOptimisationURL.Properties.FieldName = "GrittingRouteOptimisationURL";
            // 
            // rowGrittingRouteOptimisationUsername
            // 
            this.rowGrittingRouteOptimisationUsername.Name = "rowGrittingRouteOptimisationUsername";
            this.rowGrittingRouteOptimisationUsername.Properties.Caption = "Service Username";
            this.rowGrittingRouteOptimisationUsername.Properties.FieldName = "GrittingRouteOptimisationUsername";
            // 
            // rowGrittingRouteOptimisationPassword
            // 
            this.rowGrittingRouteOptimisationPassword.Name = "rowGrittingRouteOptimisationPassword";
            this.rowGrittingRouteOptimisationPassword.Properties.Caption = "Service Password";
            this.rowGrittingRouteOptimisationPassword.Properties.FieldName = "GrittingRouteOptimisationPassword";
            // 
            // rowGrittingRouteOptimisationIniFileSavePath
            // 
            this.rowGrittingRouteOptimisationIniFileSavePath.Name = "rowGrittingRouteOptimisationIniFileSavePath";
            this.rowGrittingRouteOptimisationIniFileSavePath.Properties.Caption = "Exported Ini File Save Path";
            this.rowGrittingRouteOptimisationIniFileSavePath.Properties.FieldName = "GrittingRouteOptimisationIniFileSavePath";
            this.rowGrittingRouteOptimisationIniFileSavePath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowGrittingRouteOptimisationJobFileSavePath
            // 
            this.rowGrittingRouteOptimisationJobFileSavePath.Name = "rowGrittingRouteOptimisationJobFileSavePath";
            this.rowGrittingRouteOptimisationJobFileSavePath.Properties.Caption = "Exported Job File Save Path";
            this.rowGrittingRouteOptimisationJobFileSavePath.Properties.FieldName = "GrittingRouteOptimisationJobFileSavePath";
            this.rowGrittingRouteOptimisationJobFileSavePath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowGrittingRouteOptimisationOptimisedRouteSavePath
            // 
            this.rowGrittingRouteOptimisationOptimisedRouteSavePath.Name = "rowGrittingRouteOptimisationOptimisedRouteSavePath";
            this.rowGrittingRouteOptimisationOptimisedRouteSavePath.Properties.Caption = "Optimized Job File Save Path";
            this.rowGrittingRouteOptimisationOptimisedRouteSavePath.Properties.FieldName = "GrittingRouteOptimisationOptimisedRouteSavePath";
            this.rowGrittingRouteOptimisationOptimisedRouteSavePath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryGrittingCompletedEmails
            // 
            this.categoryGrittingCompletedEmails.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGrittingCompletionEmailHtmlLayoutFile,
            this.rowGrittingCompletionEmailFromEmailAddress,
            this.rowGrittingCompletionEmailCCToEmailAddress,
            this.rowGrittingCompletionEmailSubjectLine});
            this.categoryGrittingCompletedEmails.Name = "categoryGrittingCompletedEmails";
            this.categoryGrittingCompletedEmails.Properties.Caption = "Gritting Completed Emails";
            // 
            // rowGrittingCompletionEmailHtmlLayoutFile
            // 
            this.rowGrittingCompletionEmailHtmlLayoutFile.Name = "rowGrittingCompletionEmailHtmlLayoutFile";
            this.rowGrittingCompletionEmailHtmlLayoutFile.Properties.Caption = "HTML Body Layout File";
            this.rowGrittingCompletionEmailHtmlLayoutFile.Properties.FieldName = "GrittingCompletionEmailHtmlLayoutFile";
            this.rowGrittingCompletionEmailHtmlLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowGrittingCompletionEmailFromEmailAddress
            // 
            this.rowGrittingCompletionEmailFromEmailAddress.Name = "rowGrittingCompletionEmailFromEmailAddress";
            this.rowGrittingCompletionEmailFromEmailAddress.Properties.Caption = "Sent From Email Address";
            this.rowGrittingCompletionEmailFromEmailAddress.Properties.FieldName = "GrittingCompletionEmailFromEmailAddress";
            // 
            // rowGrittingCompletionEmailCCToEmailAddress
            // 
            this.rowGrittingCompletionEmailCCToEmailAddress.Name = "rowGrittingCompletionEmailCCToEmailAddress";
            this.rowGrittingCompletionEmailCCToEmailAddress.Properties.Caption = "Sent To CC Address";
            this.rowGrittingCompletionEmailCCToEmailAddress.Properties.FieldName = "GrittingCompletionEmailCCToEmailAddress";
            // 
            // rowGrittingCompletionEmailSubjectLine
            // 
            this.rowGrittingCompletionEmailSubjectLine.Name = "rowGrittingCompletionEmailSubjectLine";
            this.rowGrittingCompletionEmailSubjectLine.Properties.Caption = "Subject Line";
            this.rowGrittingCompletionEmailSubjectLine.Properties.FieldName = "GrittingCompletionEmailSubjectLine";
            // 
            // categoryRowSelfBillingInvoices
            // 
            this.categoryRowSelfBillingInvoices.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowGrittingSelfBillingInvoicePDFPath,
            this.rowGrittingSelfBillingInvoiceEmailHTMLLayoutFile,
            this.rowGrittingSelfBillingInvoiceEmailSubjectLine,
            this.rowGrittingSelfBillingInvoiceEmailFromAddress,
            this.rowGrittingSelfBillingInvoiceEmailCCFromAddress});
            this.categoryRowSelfBillingInvoices.Name = "categoryRowSelfBillingInvoices";
            this.categoryRowSelfBillingInvoices.Properties.Caption = "Griting Team Self-Billing Invoices";
            // 
            // rowGrittingSelfBillingInvoicePDFPath
            // 
            this.rowGrittingSelfBillingInvoicePDFPath.Name = "rowGrittingSelfBillingInvoicePDFPath";
            this.rowGrittingSelfBillingInvoicePDFPath.Properties.Caption = "Generated PDF File Folder";
            this.rowGrittingSelfBillingInvoicePDFPath.Properties.FieldName = "GrittingSelfBillingInvoicePDFPath";
            this.rowGrittingSelfBillingInvoicePDFPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowGrittingSelfBillingInvoiceEmailHTMLLayoutFile
            // 
            this.rowGrittingSelfBillingInvoiceEmailHTMLLayoutFile.Name = "rowGrittingSelfBillingInvoiceEmailHTMLLayoutFile";
            this.rowGrittingSelfBillingInvoiceEmailHTMLLayoutFile.Properties.Caption = "Email HTML Body Layout File";
            this.rowGrittingSelfBillingInvoiceEmailHTMLLayoutFile.Properties.FieldName = "GrittingSelfBillingInvoiceEmailHTMLLayoutFile";
            this.rowGrittingSelfBillingInvoiceEmailHTMLLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowGrittingSelfBillingInvoiceEmailSubjectLine
            // 
            this.rowGrittingSelfBillingInvoiceEmailSubjectLine.Name = "rowGrittingSelfBillingInvoiceEmailSubjectLine";
            this.rowGrittingSelfBillingInvoiceEmailSubjectLine.Properties.Caption = "Email Subject Line";
            this.rowGrittingSelfBillingInvoiceEmailSubjectLine.Properties.FieldName = "GrittingSelfBillingInvoiceEmailSubjectLine";
            // 
            // rowGrittingSelfBillingInvoiceEmailFromAddress
            // 
            this.rowGrittingSelfBillingInvoiceEmailFromAddress.Name = "rowGrittingSelfBillingInvoiceEmailFromAddress";
            this.rowGrittingSelfBillingInvoiceEmailFromAddress.Properties.Caption = "From Email Address";
            this.rowGrittingSelfBillingInvoiceEmailFromAddress.Properties.FieldName = "GrittingSelfBillingInvoiceEmailFromAddress";
            // 
            // rowGrittingSelfBillingInvoiceEmailCCFromAddress
            // 
            this.rowGrittingSelfBillingInvoiceEmailCCFromAddress.Name = "rowGrittingSelfBillingInvoiceEmailCCFromAddress";
            this.rowGrittingSelfBillingInvoiceEmailCCFromAddress.Properties.Caption = "CC To Email Address";
            this.rowGrittingSelfBillingInvoiceEmailCCFromAddress.Properties.FieldName = "GrittingSelfBillingInvoiceEmailCCFromAddress";
            // 
            // categoryRowSnowClearanceTeamPO
            // 
            this.categoryRowSnowClearanceTeamPO.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowSnowClearanceTeamPOPDFPath,
            this.rowSnowClearanceTeamPOEmailHTMLLayoutFile,
            this.rowSnowClearanceTeamPOEmailSubjectLine,
            this.rowSnowClearanceTeamPOEmailFromAddress,
            this.rowSnowClearanceTeamPOEmailCCAddress});
            this.categoryRowSnowClearanceTeamPO.Name = "categoryRowSnowClearanceTeamPO";
            this.categoryRowSnowClearanceTeamPO.Properties.Caption = "Snow Clearance Team Purchase Orders";
            // 
            // rowSnowClearanceTeamPOPDFPath
            // 
            this.rowSnowClearanceTeamPOPDFPath.Name = "rowSnowClearanceTeamPOPDFPath";
            this.rowSnowClearanceTeamPOPDFPath.Properties.Caption = "Generated PDF File Folder";
            this.rowSnowClearanceTeamPOPDFPath.Properties.FieldName = "SnowClearanceTeamPOPDFPath";
            this.rowSnowClearanceTeamPOPDFPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowSnowClearanceTeamPOEmailHTMLLayoutFile
            // 
            this.rowSnowClearanceTeamPOEmailHTMLLayoutFile.Name = "rowSnowClearanceTeamPOEmailHTMLLayoutFile";
            this.rowSnowClearanceTeamPOEmailHTMLLayoutFile.Properties.Caption = "Email HTML Body Layout File";
            this.rowSnowClearanceTeamPOEmailHTMLLayoutFile.Properties.FieldName = "SnowClearanceTeamPOEmailHTMLLayoutFile";
            this.rowSnowClearanceTeamPOEmailHTMLLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowSnowClearanceTeamPOEmailSubjectLine
            // 
            this.rowSnowClearanceTeamPOEmailSubjectLine.Name = "rowSnowClearanceTeamPOEmailSubjectLine";
            this.rowSnowClearanceTeamPOEmailSubjectLine.Properties.Caption = "Email Subject Line";
            this.rowSnowClearanceTeamPOEmailSubjectLine.Properties.FieldName = "SnowClearanceTeamPOEmailSubjectLine";
            // 
            // rowSnowClearanceTeamPOEmailFromAddress
            // 
            this.rowSnowClearanceTeamPOEmailFromAddress.Name = "rowSnowClearanceTeamPOEmailFromAddress";
            this.rowSnowClearanceTeamPOEmailFromAddress.Properties.Caption = "From Email Address";
            this.rowSnowClearanceTeamPOEmailFromAddress.Properties.FieldName = "SnowClearanceTeamPOEmailFromAddress";
            // 
            // rowSnowClearanceTeamPOEmailCCAddress
            // 
            this.rowSnowClearanceTeamPOEmailCCAddress.Name = "rowSnowClearanceTeamPOEmailCCAddress";
            this.rowSnowClearanceTeamPOEmailCCAddress.Properties.Caption = "CC To Email Address";
            this.rowSnowClearanceTeamPOEmailCCAddress.Properties.FieldName = "SnowClearanceTeamPOEmailCCAddress";
            // 
            // categoryRowFinance
            // 
            this.categoryRowFinance.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowFinanceClientInvoiceSSDefaultSavePath,
            this.rowFinanceTeamCostsSSDefaultSavePath,
            this.rowFinanceTeamCostSSGrittingAnalysisCode,
            this.rowFinanceTeamCostSSSnowClearanceAnalysisCode,
            this.rowFinanceTeamCostSSGrittingCostCentre,
            this.rowFinanceTeamCostSSSnowClearanceCostCentre});
            this.categoryRowFinance.Name = "categoryRowFinance";
            this.categoryRowFinance.Properties.Caption = "Finance";
            // 
            // rowFinanceClientInvoiceSSDefaultSavePath
            // 
            this.rowFinanceClientInvoiceSSDefaultSavePath.Name = "rowFinanceClientInvoiceSSDefaultSavePath";
            this.rowFinanceClientInvoiceSSDefaultSavePath.Properties.Caption = "Generated Client Invoice Spreadsheet Folder";
            this.rowFinanceClientInvoiceSSDefaultSavePath.Properties.FieldName = "FinanceClientInvoiceSSDefaultSavePath";
            this.rowFinanceClientInvoiceSSDefaultSavePath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowFinanceTeamCostsSSDefaultSavePath
            // 
            this.rowFinanceTeamCostsSSDefaultSavePath.Name = "rowFinanceTeamCostsSSDefaultSavePath";
            this.rowFinanceTeamCostsSSDefaultSavePath.Properties.Caption = "Generated Team Cost Spreadsheet Folder";
            this.rowFinanceTeamCostsSSDefaultSavePath.Properties.FieldName = "FinanceTeamCostsSSDefaultSavePath";
            this.rowFinanceTeamCostsSSDefaultSavePath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowFinanceTeamCostSSGrittingAnalysisCode
            // 
            this.rowFinanceTeamCostSSGrittingAnalysisCode.Name = "rowFinanceTeamCostSSGrittingAnalysisCode";
            this.rowFinanceTeamCostSSGrittingAnalysisCode.Properties.Caption = "Team Cost Spreadsheet - Gritting Analysis Code";
            this.rowFinanceTeamCostSSGrittingAnalysisCode.Properties.FieldName = "FinanceTeamCostSSGrittingAnalysisCode";
            // 
            // rowFinanceTeamCostSSSnowClearanceAnalysisCode
            // 
            this.rowFinanceTeamCostSSSnowClearanceAnalysisCode.Name = "rowFinanceTeamCostSSSnowClearanceAnalysisCode";
            this.rowFinanceTeamCostSSSnowClearanceAnalysisCode.Properties.Caption = "Team Cost Spreadsheet - Snow Clearance Analysis Code";
            this.rowFinanceTeamCostSSSnowClearanceAnalysisCode.Properties.FieldName = "FinanceTeamCostSSSnowClearanceAnalysisCode";
            // 
            // rowFinanceTeamCostSSGrittingCostCentre
            // 
            this.rowFinanceTeamCostSSGrittingCostCentre.Name = "rowFinanceTeamCostSSGrittingCostCentre";
            this.rowFinanceTeamCostSSGrittingCostCentre.Properties.Caption = "Team Cost Spreadsheet - Gritting Cost Centre Code";
            this.rowFinanceTeamCostSSGrittingCostCentre.Properties.FieldName = "FinanceTeamCostSSGrittingCostCentre";
            // 
            // rowFinanceTeamCostSSSnowClearanceCostCentre
            // 
            this.rowFinanceTeamCostSSSnowClearanceCostCentre.Name = "rowFinanceTeamCostSSSnowClearanceCostCentre";
            this.rowFinanceTeamCostSSSnowClearanceCostCentre.Properties.Caption = "Team Cost Spreadsheet - Snow Clearance Cost Centre Code";
            this.rowFinanceTeamCostSSSnowClearanceCostCentre.Properties.FieldName = "FinanceTeamCostSSSnowClearanceCostCentre";
            // 
            // categoryRowClientJobBreakdownReport
            // 
            this.categoryRowClientJobBreakdownReport.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowClientJobBreakdownReportPDFPath,
            this.rowClientJobBreakdownReportHTMLLayoutFile,
            this.rowClientJobBreakdownReportEmailSubjectLine,
            this.rowClientJobBreakdownReportEmailFromAddress,
            this.rowClientJobBreakdownReportEmailCCAddress});
            this.categoryRowClientJobBreakdownReport.Name = "categoryRowClientJobBreakdownReport";
            this.categoryRowClientJobBreakdownReport.Properties.Caption = "Client Job Breakdown Report";
            // 
            // rowClientJobBreakdownReportPDFPath
            // 
            this.rowClientJobBreakdownReportPDFPath.Name = "rowClientJobBreakdownReportPDFPath";
            this.rowClientJobBreakdownReportPDFPath.Properties.Caption = "Generated PDF File Folder";
            this.rowClientJobBreakdownReportPDFPath.Properties.FieldName = "ClientJobBreakdownReportPDFPath";
            this.rowClientJobBreakdownReportPDFPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowClientJobBreakdownReportHTMLLayoutFile
            // 
            this.rowClientJobBreakdownReportHTMLLayoutFile.Name = "rowClientJobBreakdownReportHTMLLayoutFile";
            this.rowClientJobBreakdownReportHTMLLayoutFile.Properties.Caption = "Email HTML Body Layout File";
            this.rowClientJobBreakdownReportHTMLLayoutFile.Properties.FieldName = "ClientJobBreakdownReportHTMLLayoutFile";
            this.rowClientJobBreakdownReportHTMLLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowClientJobBreakdownReportEmailSubjectLine
            // 
            this.rowClientJobBreakdownReportEmailSubjectLine.Name = "rowClientJobBreakdownReportEmailSubjectLine";
            this.rowClientJobBreakdownReportEmailSubjectLine.Properties.Caption = "Email Subject Line";
            this.rowClientJobBreakdownReportEmailSubjectLine.Properties.FieldName = "ClientJobBreakdownReportEmailSubjectLine";
            // 
            // rowClientJobBreakdownReportEmailFromAddress
            // 
            this.rowClientJobBreakdownReportEmailFromAddress.Name = "rowClientJobBreakdownReportEmailFromAddress";
            this.rowClientJobBreakdownReportEmailFromAddress.Properties.Caption = "From Email Address";
            this.rowClientJobBreakdownReportEmailFromAddress.Properties.FieldName = "ClientJobBreakdownReportEmailFromAddress";
            // 
            // rowClientJobBreakdownReportEmailCCAddress
            // 
            this.rowClientJobBreakdownReportEmailCCAddress.Name = "rowClientJobBreakdownReportEmailCCAddress";
            this.rowClientJobBreakdownReportEmailCCAddress.Properties.Caption = "CC To Email Address";
            this.rowClientJobBreakdownReportEmailCCAddress.Properties.FieldName = "ClientJobBreakdownReportEmailCCAddress";
            // 
            // categoryRowMissingJobSheetNumbers
            // 
            this.categoryRowMissingJobSheetNumbers.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMissingJobNumberSheetSavePath,
            this.rowMissingJobNumberSheetSS_HTMLLayoutFile,
            this.rowMissingJobNumberSheetPDF_HTMLLayoutFile,
            this.rowMissingJobNumberSheetSubjectLine,
            this.rowMissingJobNumberSheetEmailFromAddress,
            this.rowMissingJobNumberSheetEmailCCAddress,
            this.rowMissingJobNumberSheetLoadPath});
            this.categoryRowMissingJobSheetNumbers.Name = "categoryRowMissingJobSheetNumbers";
            this.categoryRowMissingJobSheetNumbers.Properties.Caption = "Missing Job Sheet Numbers";
            // 
            // rowMissingJobNumberSheetSavePath
            // 
            this.rowMissingJobNumberSheetSavePath.Name = "rowMissingJobNumberSheetSavePath";
            this.rowMissingJobNumberSheetSavePath.Properties.Caption = "Generated PDF and Spreadsheet Path";
            this.rowMissingJobNumberSheetSavePath.Properties.FieldName = "MissingJobNumberSheetSavePath";
            this.rowMissingJobNumberSheetSavePath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowMissingJobNumberSheetSS_HTMLLayoutFile
            // 
            this.rowMissingJobNumberSheetSS_HTMLLayoutFile.Name = "rowMissingJobNumberSheetSS_HTMLLayoutFile";
            this.rowMissingJobNumberSheetSS_HTMLLayoutFile.Properties.Caption = "Email Spreadsheet HTML Body Layout File";
            this.rowMissingJobNumberSheetSS_HTMLLayoutFile.Properties.FieldName = "MissingJobNumberSheetSS_HTMLLayoutFile";
            this.rowMissingJobNumberSheetSS_HTMLLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowMissingJobNumberSheetPDF_HTMLLayoutFile
            // 
            this.rowMissingJobNumberSheetPDF_HTMLLayoutFile.Name = "rowMissingJobNumberSheetPDF_HTMLLayoutFile";
            this.rowMissingJobNumberSheetPDF_HTMLLayoutFile.Properties.Caption = "Email PDF HTML Body Layout File";
            this.rowMissingJobNumberSheetPDF_HTMLLayoutFile.Properties.FieldName = "MissingJobNumberSheetPDF_HTMLLayoutFile";
            this.rowMissingJobNumberSheetPDF_HTMLLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowMissingJobNumberSheetSubjectLine
            // 
            this.rowMissingJobNumberSheetSubjectLine.Name = "rowMissingJobNumberSheetSubjectLine";
            this.rowMissingJobNumberSheetSubjectLine.Properties.Caption = "Email Subject Line";
            this.rowMissingJobNumberSheetSubjectLine.Properties.FieldName = "MissingJobNumberSheetSubjectLine";
            // 
            // rowMissingJobNumberSheetEmailFromAddress
            // 
            this.rowMissingJobNumberSheetEmailFromAddress.Name = "rowMissingJobNumberSheetEmailFromAddress";
            this.rowMissingJobNumberSheetEmailFromAddress.Properties.Caption = "From Email Address";
            this.rowMissingJobNumberSheetEmailFromAddress.Properties.FieldName = "MissingJobNumberSheetEmailFromAddress";
            // 
            // rowMissingJobNumberSheetEmailCCAddress
            // 
            this.rowMissingJobNumberSheetEmailCCAddress.Name = "rowMissingJobNumberSheetEmailCCAddress";
            this.rowMissingJobNumberSheetEmailCCAddress.Properties.Caption = "CC To Email Address";
            this.rowMissingJobNumberSheetEmailCCAddress.Properties.FieldName = "MissingJobNumberSheetEmailCCAddress";
            // 
            // rowMissingJobNumberSheetLoadPath
            // 
            this.rowMissingJobNumberSheetLoadPath.Name = "rowMissingJobNumberSheetLoadPath";
            this.rowMissingJobNumberSheetLoadPath.Properties.Caption = "Completed Spreadsheet Import Path";
            this.rowMissingJobNumberSheetLoadPath.Properties.FieldName = "MissingJobNumberSheetLoadPath";
            this.rowMissingJobNumberSheetLoadPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryTenderRegister
            // 
            this.categoryTenderRegister.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowTenderReportLayouts,
            this.categoryRowTenderStaekholderEmail,
            this.categoryRowExtraWorksQuotationContractManagerEmail});
            this.categoryTenderRegister.Name = "categoryTenderRegister";
            this.categoryTenderRegister.Properties.Caption = "Tender Register";
            // 
            // rowTenderReportLayouts
            // 
            this.rowTenderReportLayouts.Name = "rowTenderReportLayouts";
            this.rowTenderReportLayouts.Properties.Caption = "Tender Report Layouts Folder";
            this.rowTenderReportLayouts.Properties.FieldName = "TenderReportLayouts";
            this.rowTenderReportLayouts.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // categoryRowTenderStaekholderEmail
            // 
            this.categoryRowTenderStaekholderEmail.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowTenderStakeholderEmailPDFPath,
            this.rowTenderStakeholderEmailHTMLLayoutFile,
            this.rowTenderStakeholderEmailSubjectLine,
            this.rowTenderStakeholderEmailFromAddress,
            this.rowTenderStakeholderEmailCCAddress});
            this.categoryRowTenderStaekholderEmail.Name = "categoryRowTenderStaekholderEmail";
            this.categoryRowTenderStaekholderEmail.Properties.Caption = "Major Bid Stakeholder Email";
            // 
            // rowTenderStakeholderEmailPDFPath
            // 
            this.rowTenderStakeholderEmailPDFPath.Name = "rowTenderStakeholderEmailPDFPath";
            this.rowTenderStakeholderEmailPDFPath.Properties.Caption = "Generated PDF File Folder";
            this.rowTenderStakeholderEmailPDFPath.Properties.FieldName = "TenderStakeholderEmailPDFPath";
            this.rowTenderStakeholderEmailPDFPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowTenderStakeholderEmailHTMLLayoutFile
            // 
            this.rowTenderStakeholderEmailHTMLLayoutFile.Name = "rowTenderStakeholderEmailHTMLLayoutFile";
            this.rowTenderStakeholderEmailHTMLLayoutFile.Properties.Caption = "Email HTML Body Layout File";
            this.rowTenderStakeholderEmailHTMLLayoutFile.Properties.FieldName = "TenderStakeholderEmailHTMLLayoutFile";
            this.rowTenderStakeholderEmailHTMLLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowTenderStakeholderEmailSubjectLine
            // 
            this.rowTenderStakeholderEmailSubjectLine.Name = "rowTenderStakeholderEmailSubjectLine";
            this.rowTenderStakeholderEmailSubjectLine.Properties.Caption = "Email Subject Line";
            this.rowTenderStakeholderEmailSubjectLine.Properties.FieldName = "TenderStakeholderEmailSubjectLine";
            // 
            // rowTenderStakeholderEmailFromAddress
            // 
            this.rowTenderStakeholderEmailFromAddress.Name = "rowTenderStakeholderEmailFromAddress";
            this.rowTenderStakeholderEmailFromAddress.Properties.Caption = "From Email Address";
            this.rowTenderStakeholderEmailFromAddress.Properties.FieldName = "TenderStakeholderEmailFromAddress";
            // 
            // rowTenderStakeholderEmailCCAddress
            // 
            this.rowTenderStakeholderEmailCCAddress.Name = "rowTenderStakeholderEmailCCAddress";
            this.rowTenderStakeholderEmailCCAddress.Properties.Caption = "CC To Email Address";
            this.rowTenderStakeholderEmailCCAddress.Properties.FieldName = "TenderStakeholderEmailCCAddress";
            // 
            // categoryRowExtraWorksQuotationContractManagerEmail
            // 
            this.categoryRowExtraWorksQuotationContractManagerEmail.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowExtraWorkQuotationEmailPDFPath,
            this.rowExtraWorkQuotationEmailHTMLLayoutFile,
            this.rowExtraWorkQuotationEmailSubjectLine,
            this.rowExtraWorkQuotationEmailFromAddress,
            this.rowExtraWorkQuotationEmailCCAddress});
            this.categoryRowExtraWorksQuotationContractManagerEmail.Name = "categoryRowExtraWorksQuotationContractManagerEmail";
            this.categoryRowExtraWorksQuotationContractManagerEmail.Properties.Caption = "Extra Work Quotation Contract Manager Email";
            // 
            // rowExtraWorkQuotationEmailPDFPath
            // 
            this.rowExtraWorkQuotationEmailPDFPath.Name = "rowExtraWorkQuotationEmailPDFPath";
            this.rowExtraWorkQuotationEmailPDFPath.Properties.Caption = "Generated PDF File Folder";
            this.rowExtraWorkQuotationEmailPDFPath.Properties.FieldName = "ExtraWorkQuotationEmailPDFPath";
            this.rowExtraWorkQuotationEmailPDFPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowExtraWorkQuotationEmailHTMLLayoutFile
            // 
            this.rowExtraWorkQuotationEmailHTMLLayoutFile.Name = "rowExtraWorkQuotationEmailHTMLLayoutFile";
            this.rowExtraWorkQuotationEmailHTMLLayoutFile.Properties.Caption = "Email HTML Body Layout File";
            this.rowExtraWorkQuotationEmailHTMLLayoutFile.Properties.FieldName = "ExtraWorkQuotationEmailHTMLLayoutFile";
            this.rowExtraWorkQuotationEmailHTMLLayoutFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // rowExtraWorkQuotationEmailSubjectLine
            // 
            this.rowExtraWorkQuotationEmailSubjectLine.Name = "rowExtraWorkQuotationEmailSubjectLine";
            this.rowExtraWorkQuotationEmailSubjectLine.Properties.Caption = "Email Subject Line";
            this.rowExtraWorkQuotationEmailSubjectLine.Properties.FieldName = "ExtraWorkQuotationEmailSubjectLine";
            // 
            // rowExtraWorkQuotationEmailFromAddress
            // 
            this.rowExtraWorkQuotationEmailFromAddress.Name = "rowExtraWorkQuotationEmailFromAddress";
            this.rowExtraWorkQuotationEmailFromAddress.Properties.Caption = "From Email Address";
            this.rowExtraWorkQuotationEmailFromAddress.Properties.FieldName = "ExtraWorkQuotationEmailFromAddress";
            // 
            // rowExtraWorkQuotationEmailCCAddress
            // 
            this.rowExtraWorkQuotationEmailCCAddress.Name = "rowExtraWorkQuotationEmailCCAddress";
            this.rowExtraWorkQuotationEmailCCAddress.Properties.Caption = "CC To Email Address";
            this.rowExtraWorkQuotationEmailCCAddress.Properties.FieldName = "ExtraWorkQuotationEmailCCAddress";
            // 
            // categorySummerMainteance
            // 
            this.categorySummerMainteance.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryWorkPermits});
            this.categorySummerMainteance.Name = "categorySummerMainteance";
            this.categorySummerMainteance.Properties.Caption = "Summer Maintenance";
            // 
            // categoryWorkPermits
            // 
            this.categoryWorkPermits.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowArchivaWorkPermitFile});
            this.categoryWorkPermits.Name = "categoryWorkPermits";
            this.categoryWorkPermits.Properties.Caption = "Work Permits";
            // 
            // rowArchivaWorkPermitFile
            // 
            this.rowArchivaWorkPermitFile.Name = "rowArchivaWorkPermitFile";
            this.rowArchivaWorkPermitFile.Properties.Caption = "Archiva Import File";
            this.rowArchivaWorkPermitFile.Properties.FieldName = "ArchivaWorkPermitFile";
            this.rowArchivaWorkPermitFile.Properties.RowEdit = this.repositoryItemButtonEditGetFileName;
            // 
            // categoryHR
            // 
            this.categoryHR.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowHR_QualificationCertificatePath,
            this.rowHR_LinkedDocumentPath,
            this.rowHR_PayrollReportExportSavedFolder});
            this.categoryHR.Name = "categoryHR";
            this.categoryHR.Properties.Caption = "HR";
            // 
            // rowHR_QualificationCertificatePath
            // 
            this.rowHR_QualificationCertificatePath.Name = "rowHR_QualificationCertificatePath";
            this.rowHR_QualificationCertificatePath.Properties.Caption = "Qualification Certificate Folder";
            this.rowHR_QualificationCertificatePath.Properties.FieldName = "HR_QualificationCertificatePath";
            this.rowHR_QualificationCertificatePath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowHR_LinkedDocumentPath
            // 
            this.rowHR_LinkedDocumentPath.Name = "rowHR_LinkedDocumentPath";
            this.rowHR_LinkedDocumentPath.Properties.Caption = "HR Linked Document Path";
            this.rowHR_LinkedDocumentPath.Properties.FieldName = "HR_LinkedDocumentPath";
            this.rowHR_LinkedDocumentPath.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // rowHR_PayrollReportExportSavedFolder
            // 
            this.rowHR_PayrollReportExportSavedFolder.Name = "rowHR_PayrollReportExportSavedFolder";
            this.rowHR_PayrollReportExportSavedFolder.Properties.Caption = "Payroll Report Export Spreadsheet Path";
            this.rowHR_PayrollReportExportSavedFolder.Properties.FieldName = "HR_PayrollReportExportSavedFolder";
            this.rowHR_PayrollReportExportSavedFolder.Properties.RowEdit = this.repositoryItemButtonEdit1;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.ImageOptions.ImageIndex = 5;
            this.btnOK.ImageOptions.ImageList = this.imageCollection1;
            this.btnOK.Location = new System.Drawing.Point(282, 517);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Save_16x16, "Save_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Save_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.cancel_16x16, "cancel_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "cancel_16x16");
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ImageOptions.ImageIndex = 6;
            this.btnCancel.ImageOptions.ImageList = this.imageCollection1;
            this.btnCancel.Location = new System.Drawing.Point(376, 517);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 2);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtpSettingsPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(725, 511);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtpSettingsPage1});
            // 
            // xtpSettingsPage1
            // 
            this.xtpSettingsPage1.Controls.Add(this.popupContainerControlTreeRefStructure);
            this.xtpSettingsPage1.Controls.Add(this.vGridControl1);
            this.xtpSettingsPage1.Name = "xtpSettingsPage1";
            this.xtpSettingsPage1.Size = new System.Drawing.Size(720, 485);
            this.xtpSettingsPage1.Text = "General Settings";
            // 
            // sp00045_GetSystemSettingsTableAdapter
            // 
            this.sp00045_GetSystemSettingsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00127_system_settings_show_other_tree_details_listTableAdapter
            // 
            this.sp00127_system_settings_show_other_tree_details_listTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            // 
            // sp00128_system_settings_data_entry_off_listTableAdapter
            // 
            this.sp00128_system_settings_data_entry_off_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00129_system_settings_live_period_listTableAdapter
            // 
            this.sp00129_system_settings_live_period_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp01239_AT_WorkOrders_Print_Scale_ListTableAdapter
            // 
            this.sp01239_AT_WorkOrders_Print_Scale_ListTableAdapter.ClearBeforeFill = true;
            // 
            // categoryRow11
            // 
            this.categoryRow11.Name = "categoryRow11";
            this.categoryRow11.Properties.Caption = "Tree Picker Mapping";
            // 
            // sp01277_AT_Default_WorkOrder_Layout_ListTableAdapter
            // 
            this.sp01277_AT_Default_WorkOrder_Layout_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter
            // 
            this.sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter.ClearBeforeFill = true;
            // 
            // sp00147_system_settings_screen_position_listTableAdapter
            // 
            this.sp00147_system_settings_screen_position_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00148_system_settings_map_generation_methods_listTableAdapter
            // 
            this.sp00148_system_settings_map_generation_methods_listTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.ShowPopupCloseButton = false;
            // 
            // sp00149_system_settings_mapping_projections_listTableAdapter
            // 
            this.sp00149_system_settings_mapping_projections_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00150_system_settings_data_transfer_mode_listTableAdapter
            // 
            this.sp00150_system_settings_data_transfer_mode_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00023_System_Settings_Rate_Criteria_ListTableAdapter
            // 
            this.sp00023_System_Settings_Rate_Criteria_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp00190_Contractor_List_With_BlankTableAdapter
            // 
            this.sp00190_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter
            // 
            this.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00236_Report_Watermark_Text_DirectionsTableAdapter
            // 
            this.sp00236_Report_Watermark_Text_DirectionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00237_Report_Watermark_Image_AlignmentTableAdapter
            // 
            this.sp00237_Report_Watermark_Image_AlignmentTableAdapter.ClearBeforeFill = true;
            // 
            // sp00238_Report_Watermark_Image_ViewModeTableAdapter
            // 
            this.sp00238_Report_Watermark_Image_ViewModeTableAdapter.ClearBeforeFill = true;
            // 
            // frmSystemSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(729, 546);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSystemSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Information Centre Settings";
            this.Load += new System.EventHandler(this.frmSystemSettings_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00045GetSystemSettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00127systemsettingsshowothertreedetailslistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00128systemsettingsdataentryofflistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00129systemsettingsliveperiodlistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01239ATWorkOrdersPrintScaleListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01277ATDefaultWorkOrderLayoutListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01300ATWorkOrderMapPageSizesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00147systemsettingsscreenpositionlistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00148systemsettingsmapgenerationmethodslistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTreeRefStructure)).EndInit();
            this.popupContainerControlTreeRefStructure.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTreeRefRemovePreceedingChars.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTreeRefRemoveNumberOfChars.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTreeRef2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00149systemsettingsmappingprojectionslistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00150systemsettingsdatatransfermodelistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00023SystemSettingsRateCriteriaListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit6View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDefaultCompanyHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00232CompanyHeaderDropDownListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit7View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDefaultContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerLoadDataOnOpenRepositoryItemComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditGetFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEditSpreadsheetRowBackColour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCalloutPicturesExternal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditDataSyncMachineID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditTextDirections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00236ReportWatermarkTextDirectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditWatermarkTextSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBarWatermarkTransparency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditWatermarkImageAlign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00237ReportWatermarkImageAlignmentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditViewMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00238ReportWatermarkImageViewModeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtpSettingsPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraVerticalGrid.VGridControl vGridControl1;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtpSettingsPage1;
        private System.Windows.Forms.BindingSource sp00045GetSystemSettingsBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00045_GetSystemSettingsTableAdapter sp00045_GetSystemSettingsTableAdapter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDataEntryOff;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLivePeriod;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLiveDBName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStaffImagePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStaffImageThumbPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDefaultPhoto;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderLayoutLocation;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesReportLayoutLocation;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderMapLocation;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderPrintShowDetailsOfOtherTrees;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesMapTreeFilesFolder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesMapBackgroundFolder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesExportToGBM_Background_Mapping_Folder;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow5;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow4;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow3;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow6;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow7;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow8;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private System.Windows.Forms.BindingSource sp00127systemsettingsshowothertreedetailslistBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00127_system_settings_show_other_tree_details_listTableAdapter sp00127_system_settings_show_other_tree_details_listTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private System.Windows.Forms.BindingSource sp00128systemsettingsdataentryofflistBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00128_system_settings_data_entry_off_listTableAdapter sp00128_system_settings_data_entry_off_listTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit4;
        private System.Windows.Forms.BindingSource sp00129systemsettingsliveperiodlistBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00129_system_settings_live_period_listTableAdapter sp00129_system_settings_live_period_listTableAdapter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderDefaultMapScale;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private System.Windows.Forms.BindingSource sp01239ATWorkOrdersPrintScaleListBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01239_AT_WorkOrders_Print_Scale_ListTableAdapter sp01239_AT_WorkOrders_Print_Scale_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colScaleDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colScale;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesExportToGBM_Geoset_Folder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesExportToGBM_Profile_Folder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesExportToGBM_GBM_Mobile_XML_Folder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesExportToGBM_Tab_Folder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesExportToGBM_Mid_Mif_Folder;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow9;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow10;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesImportFromGBM_Default_Tab_Folder;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesExportToGBM_Pda_Reference_Data_Folder;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow11;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_Polygon_Area;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_GPS_CrossHair_Colour;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow13;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow12;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private System.Windows.Forms.ImageList imageListLineStyles;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow14;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow15;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_Sequence_From_Locality_Length;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_Sequence_From_Locality_Seperator;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private System.Windows.Forms.BindingSource sp01277ATDefaultWorkOrderLayoutListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportTypeName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderDefaultLayoutID;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp01277_AT_Default_WorkOrder_Layout_ListTableAdapter sp01277_AT_Default_WorkOrder_Layout_ListTableAdapter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderDefaultPaperSize;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderDefaultLegendPosition;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderDefaultNorthArrowPosition;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderDefaultScaleBarPosition;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderOSCopyrightDefaultPosition;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderOSCopyright;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit3;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit3View;
        private DevExpress.XtraGrid.Columns.GridColumn colPageHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colPageSizeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPageSizeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPageSizeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colPageWidth;
        private System.Windows.Forms.BindingSource sp01300ATWorkOrderMapPageSizesBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit4;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit4View;
        private System.Windows.Forms.BindingSource sp00147systemsettingsscreenpositionlistBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00147_system_settings_screen_position_listTableAdapter sp00147_system_settings_screen_position_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colScreenPosition;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow16;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderMapGenerationMethod;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit5;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderMapTreeHighlightColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit3;
        private System.Windows.Forms.BindingSource sp00148systemsettingsmapgenerationmethodslistBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00148_system_settings_map_generation_methods_listTableAdapter sp00148_system_settings_map_generation_methods_listTableAdapter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesVersion4WoodPlan_Incident_Link_Exe;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderMapTicked;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow17;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow18;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow19;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_Thematic_Band_Width;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar repositoryItemTrackBar1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_Gazetteer_Search_Radius;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit4;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlTreeRefStructure;
        private DevExpress.XtraEditors.SimpleButton btnSetTreeRefStruc;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit textEditTreeRefRemovePreceedingChars;
        private DevExpress.XtraEditors.SpinEdit spinEditTreeRefRemoveNumberOfChars;
        private DevExpress.XtraEditors.CheckEdit checkEditTreeRef1;
        private DevExpress.XtraEditors.CheckEdit checkEditTreeRef3;
        private DevExpress.XtraEditors.CheckEdit checkEditTreeRef2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow20;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_Tree_Ref_Format_Used;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesPictureFilesFolder;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit5;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit5View;
        private System.Windows.Forms.BindingSource sp00149systemsettingsmappingprojectionslistBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00149_system_settings_mapping_projections_listTableAdapter sp00149_system_settings_mapping_projections_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colProjectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colProjectionName;
        private DevExpress.XtraGrid.Columns.GridColumn colProjectionOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colProjectionText;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesDefaultMappingProjection;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSystemDataTransferMode;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit6;
        private System.Windows.Forms.BindingSource sp00150systemsettingsdatatransfermodelistBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00150_system_settings_data_transfer_mode_listTableAdapter sp00150_system_settings_data_transfer_mode_listTableAdapter;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow21;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMobileInterfaceExportFolder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMobileInterfaceImportFolder;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow22;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesMapBackgroundFolderTablet;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow23;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesJobRateCriteriaID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesJobDiscountRateCriteriaID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit6;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit6View;
        private System.Windows.Forms.BindingSource sp00023SystemSettingsRateCriteriaListBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00023_System_Settings_Rate_Criteria_ListTableAdapter sp00023_System_Settings_Rate_Criteria_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colItemValue;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesLinkedDocumentsPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit5;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTrees_TreePicker_Default_Hedge_Width;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesDefaultCompanyHeader;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditDefaultCompanyHeader;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit7View;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditDefaultContractor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesDefaultContractor;
        private System.Windows.Forms.BindingSource sp00190ContractorListWithBlankBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter sp00190_Contractor_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private System.Windows.Forms.BindingSource sp00232CompanyHeaderDropDownListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colintHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colstrDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrSlogan;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow24;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesCavatCalculation;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesCavatCalculationMethod;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesCavatDefaultUnitValueFactor;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox ManagerLoadDataOnOpenRepositoryItemComboBox;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesTreeManagerLoadTreesOnOpen;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesInspectionManagerLoadInspectionsOnOpen;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesActionManagerLoadActionsOnOpen;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow25;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow26;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowInformationCentre;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow28;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowEstatePlanInspectionManagerLoadInspectionsOnOpen;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow29;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowEstatePlanAssetManagerLoadAssetsOnOpen;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow30;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowEstatePlanActionManagerLoadActionsOnOpen;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMappingShowAmenityTreeObjects;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMappingShowEstatePlanObjects;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow31;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGroundControlSiteDrawingsPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGroundControlContractorCertificatesPath;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowWinterMaintenance;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowForecastImport;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingForecastType1FilePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingForecastType2FilePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingConfirmationSentPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingConfirmationReceivedPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingAuthorisationSentHtmlLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingConfirmationSentHtmlLayoutFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditGetFileName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingConfirmationCCToEmailAddress;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit6;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingClientEmailAuthorisationSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingClientEmailDailyReportSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSMTPMailServerAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingTeamReportHtmlLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingEmailsFromName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTextAnywhereUsername;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTextAnywherePassword;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingDefaultVatRate;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowGrittingEmails;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTeamEmailReportSubjectLine;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEditSpreadsheetRowBackColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingAuthorisationGeneratedFileType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingAuthorisationOddRowColour;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingAuthorisationEvenRowColour;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowClientAuthorisationEmail;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowGrittingConfirmations;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowTeamEmailReport;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingCalloutPictureFilesFolder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingTeamConfirmationCCToEmailAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingTeamReportEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowSelfBillingInvoices;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingSelfBillingInvoicePDFPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingSelfBillingInvoiceEmailHTMLLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingSelfBillingInvoiceEmailSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingSelfBillingInvoiceEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingSelfBillingInvoiceEmailCCFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingReportLayouts;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingSavedCalloutSignatureFolder;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowSnowClearanceTeamPO;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSnowClearanceTeamPOPDFPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSnowClearanceTeamPOEmailHTMLLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSnowClearanceTeamPOEmailSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSnowClearanceTeamPOEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSnowClearanceTeamPOEmailCCAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSaltStockControlPeriodID;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowClientJobBreakdownReport;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowClientJobBreakdownReportPDFPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowClientJobBreakdownReportHTMLLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowClientJobBreakdownReportEmailSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowClientJobBreakdownReportEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowClientJobBreakdownReportEmailCCAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSMTPMailServerPassword;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSMTPMailServerUsername;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowFinance;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFinanceClientInvoiceSSDefaultSavePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFinanceTeamCostsSSDefaultSavePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFinanceTeamCostSSGrittingAnalysisCode;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFinanceTeamCostSSSnowClearanceAnalysisCode;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFinanceTeamCostSSGrittingCostCentre;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFinanceTeamCostSSSnowClearanceCostCentre;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericOnly;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSMTPMailServerPort;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowMissingJobSheetNumbers;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMissingJobNumberSheetSavePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMissingJobNumberSheetSS_HTMLLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMissingJobNumberSheetPDF_HTMLLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMissingJobNumberSheetSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMissingJobNumberSheetEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMissingJobNumberSheetEmailCCAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMissingJobNumberSheetLoadPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryTenderRegister;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowTenderStaekholderEmail;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTenderStakeholderEmailPDFPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTenderStakeholderEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTenderStakeholderEmailCCAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTenderStakeholderEmailHTMLLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTenderStakeholderEmailSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTenderReportLayouts;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRowExtraWorksQuotationContractManagerEmail;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowExtraWorkQuotationEmailPDFPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowExtraWorkQuotationEmailHTMLLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowExtraWorkQuotationEmailSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowExtraWorkQuotationEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowExtraWorkQuotationEmailCCAddress;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categorySummerMainteance;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryWorkPermits;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowArchivaWorkPermitFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCalloutPicturesExternal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingCalloutPictureFilesFolderInternal;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditDataSyncMachineID;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryUtilities;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDataSynchronisationMachineID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSavedReferenceFiles;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUtilitiesSavedReportLayouts;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSavedPermissionDocuments;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSavedMaps;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSavedPictures;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSavedSignatures;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryPermissionDocuments;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPermissionDocumentEmailFromAddressClient;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPermissionDocumentEmailFromAddressCustomer;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryEmalCCAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPermissionDocumentEmailCCAddressClient;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPermissionDocumentEmailCCAddressCustomer;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryEmailLayoutHTMLFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPermissionDocumentEmailHTMLLayoutFileClient;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPermissionDocumentEmailHTMLLayoutFileCustomer;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryEmailSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPermissionDocumentEmailSubjectLineClient;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPermissionDocumentEmailSubjectLineCustomer;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryWorkOrderDocuments;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSavedWorkOrderDocuments;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWorkOrderDocumentEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWorkOrderDocumentEmailCCAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWorkOrderDocumentEmailHTMLLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWorkOrderDocumentEmailSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingForecastTypeFloatingFilePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingFloatingJobsFilePath;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryFloatingSiteForecastEmail;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingFloatingSiteEmailHtmlLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingFloatingSiteEmailSubjectLine;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingFloatingSiteEmailFromAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingFloatingSiteEmailCCToEmailAddress;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRouteOptimization;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingRouteOptimisationURL;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingRouteOptimisationUsername;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingRouteOptimisationPassword;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingRouteOptimisationJobFileSavePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingRouteOptimisationIniFileSavePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingRouteOptimisationOptimisedRouteSavePath;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryGrittingCompletedEmails;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingCompletionEmailHtmlLayoutFile;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingCompletionEmailFromEmailAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingCompletionEmailCCToEmailAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowGrittingCompletionEmailSubjectLine;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit7;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAmenityTreesWorkOrderOSCopyrightWidth;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryWatermark;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkShowBehind;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkTextPageRange;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryTextWatermark;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryImageWatermark;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditTextDirections;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkText;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkTextDirection;
        private System.Windows.Forms.BindingSource sp00236ReportWatermarkTextDirectionsBindingSource;
        private WoodPlanDataSetTableAdapters.sp00236_Report_Watermark_Text_DirectionsTableAdapter sp00236_Report_Watermark_Text_DirectionsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.Repository.RepositoryItemFontEdit repositoryItemFontEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditWatermarkTextSize;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar repositoryItemTrackBarWatermarkTransparency;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditWatermarkImageAlign;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkTextFont;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkTextFontSize;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkTextFontBold;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkTextFontItalic;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkTextColour;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkTextTransparency;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkImage;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkImageAlign;
        private System.Windows.Forms.BindingSource sp00237ReportWatermarkImageAlignmentBindingSource;
        private WoodPlanDataSetTableAdapters.sp00237_Report_Watermark_Image_AlignmentTableAdapter sp00237_Report_Watermark_Image_AlignmentTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkImageTiling;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkImageViewMode;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditViewMode;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.BindingSource sp00238ReportWatermarkImageViewModeBindingSource;
        private WoodPlanDataSetTableAdapters.sp00238_Report_Watermark_Image_ViewModeTableAdapter sp00238_Report_Watermark_Image_ViewModeTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowWatermarkImageTransparency;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryHR;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowHR_QualificationCertificatePath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowHR_LinkedDocumentPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowHR_PayrollReportExportSavedFolder;
    }
}
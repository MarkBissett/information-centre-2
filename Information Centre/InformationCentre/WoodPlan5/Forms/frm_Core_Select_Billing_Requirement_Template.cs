using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_Core_Select_Billing_Requirement_Template : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        
        public int intPassedInTemplateHeaderID = 0;       
        public int intSelectedTemplateHeaderID = 0;
        public string strSelectedTemplateHeaderDescription = "";

        GridHitInfo downHitInfo = null;

        #endregion

        public frm_Core_Select_Billing_Requirement_Template()
        {
            InitializeComponent();
        }

        private void frm_Core_Select_Billing_Requirement_Template_Load(object sender, EventArgs e)
        {
            this.FormID = 500270;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp01041_Core_Billing_Requirement_Template_Items_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            
            try
            {
                sp01040_Core_Billing_Requirement_Template_Headers_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01040_Core_Billing_Requirement_Template_Headers_SelectTableAdapter.Fill(dataSet_Common_Functionality.sp01040_Core_Billing_Requirement_Template_Headers_Select);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Template Headers list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInTemplateHeaderID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["BillingRequirementTemplateHeaderID"], intPassedInTemplateHeaderID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        bool internalRowFocusing;

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Billing Requirement Template Headers Available";
                    break;
                case "gridView2":
                    message = "No Billing Requirement Template Items Available - Select a Billing Requirement Template Header to see Related Billing Requirement Template Items";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["BillingRequirementTemplateHeaderID"])) + ',';
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_Common_Functionality.sp01041_Core_Billing_Requirement_Template_Items_Select.Clear();
            }
            else
            {
                try
                {
                    sp01041_Core_Billing_Requirement_Template_Items_SelectTableAdapter.Fill(dataSet_Common_Functionality.sp01041_Core_Billing_Requirement_Template_Items_Select, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related records.\n\nTry selecting a parent record again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSelectedTemplateHeaderID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record from the Template Header list before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedTemplateHeaderID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BillingRequirementTemplateHeaderID"));
                strSelectedTemplateHeaderDescription = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "HeaderDescription"))) ? "Unknown Template Header" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "HeaderDescription")));
            }
        }



    }
}


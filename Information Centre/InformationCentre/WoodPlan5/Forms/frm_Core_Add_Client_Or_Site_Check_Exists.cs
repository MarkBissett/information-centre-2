using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Add_Client_Or_Site_Check_Exists : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string strFindWhat = "client";
        public string strPassedInFilterIDs = "";
        public string strPassedInFilterClientIDs = "";
        public string strPassedInFilterClientDescription = "";

        public int intSelectedID1 = 0;
        public int intSelectedID2 = 0;
        public string strSelectedDescription1 = "";
        public string strSelectedDescription2 = "";
        public string strSelectedDescription3 = "";
        public string strSelectedDescription4 = "";
        public string strSelectedDescription5 = "";
        public string strSelectedDescription6 = "";
        public string strSelectedDescription7 = "";
        public double dblSelectedDouble1 = (double)0.00;
        public double dblSelectedDouble2 = (double)0.00;

        #endregion

        public frm_Core_Add_Client_Or_Site_Check_Exists()
        {
            InitializeComponent();
        }

        private void frm_Core_Add_Client_Or_Site_Check_Exists_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500285;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            Set_Page_Status();
            
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
            Set_Page_Status();
        }

        private void Set_Page_Status()
        {
            switch (strFindWhat.ToLower())
            {
                case "client":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageClient;
                        xtraTabPageClient.PageEnabled = true;
                        xtraTabPageClient.PageVisible = true;
                        xtraTabPageSite.PageEnabled = false;
                        xtraTabPageSite.PageVisible = false;
                        xtraTabPageClientContactPerson.PageEnabled = false;
                        xtraTabPageClientContactPerson.PageVisible = false;

                        sp01047_Core_Search_for_Client_NameTableAdapter.Connection.ConnectionString = strConnectionString;
                        btnFindClient.Enabled = false;
                        btnAcceptClientSelection.Enabled = false;
                    }
                    break;
                case "site":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageSite;
                        xtraTabPageClient.PageEnabled = false;
                        xtraTabPageClient.PageVisible = false;
                        xtraTabPageSite.PageEnabled = true;
                        xtraTabPageSite.PageVisible = true;
                        xtraTabPageClientContactPerson.PageEnabled = false;
                        xtraTabPageClientContactPerson.PageVisible = false;

                        sp01048_Core_Search_for_Site_NameTableAdapter.Connection.ConnectionString = strConnectionString;
                        btnFindSite.Enabled = false;
                        btnAcceptSiteSelection.Enabled = false;

                        if (!String.IsNullOrWhiteSpace(strPassedInFilterClientIDs))
                        {
                            memoExEditClientFilter.EditValue = strPassedInFilterClientDescription;
                            memoExEditClientFilter.Visible = true;
                            labelClientFilter.Visible = true;
                        }
                        else
                        {
                            memoExEditClientFilter.Visible = false;
                            labelClientFilter.Visible = false;
                        }
                    }
                    break;
                case "clientcontactperson":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageSite;
                        xtraTabPageClient.PageEnabled = false;
                        xtraTabPageClient.PageVisible = false;
                        xtraTabPageSite.PageEnabled = false;
                        xtraTabPageSite.PageVisible = false;
                        xtraTabPageClientContactPerson.PageEnabled = true;
                        xtraTabPageClientContactPerson.PageVisible = true;

                        sp01051_Core_Search_For_Client_Contact_PersonTableAdapter.Connection.ConnectionString = strConnectionString;
                        btnFindClientContact.Enabled = false;
                        btnAcceptClientContactSelection.Enabled = false;

                        if (!String.IsNullOrWhiteSpace(strPassedInFilterClientIDs))
                        {
                            memoExEditClientFilter2.EditValue = strPassedInFilterClientDescription;
                            memoExEditClientFilter2.Visible = true;
                            labelClientFilter2.Visible = true;
                        }
                        else
                        {
                            memoExEditClientFilter2.Visible = false;
                            labelClientFilter2.Visible = false;
                        }
                    }
                    break;
            }
        }

        bool internalRowFocusing;

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Matching Clients - enter Search Criteria and click Find";
                    break;
                case "gridView2":
                    message = "No Matching Sites - enter Search Criteria and click Find";
                    break;
                case "gridView3":
                    message = "No Matching Client Contact - enter Search Criteria and click Find";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    btnAcceptClientSelection.Enabled = (view.FocusedRowHandle != GridControl.InvalidRowHandle);
                    break;
                case "gridView2":
                    btnAcceptSiteSelection.Enabled = (view.FocusedRowHandle != GridControl.InvalidRowHandle);
                    break;
                case "gridView3":
                    btnAcceptClientContactSelection.Enabled = (view.FocusedRowHandle != GridControl.InvalidRowHandle);
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion



        #region Clients Page

        private void teClientCriteria_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            btnFindClient.Enabled = (!string.IsNullOrWhiteSpace(te.EditValue.ToString()));
        }

        private void btnFindClient_Click(object sender, EventArgs e)
        {
            string strCriteria = teClientCriteria.EditValue.ToString();
            if (string.IsNullOrWhiteSpace(strCriteria))
            {
                XtraMessageBox.Show("Enter the value to find before proceeding.", "Search", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            switch (cbeClientMatchType.EditValue.ToString())
            {
                case "Client Name Starts With":
                    {
                        strCriteria += "%";
                    }
                    break;
                case "Client Name Contains":
                    {
                        strCriteria = "%" + strCriteria + "%";
                    }
                    break;
                case "Client Name Ends With":
                    {
                        strCriteria = "%" + strCriteria;
                    }
                    break;
            }

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01047_Core_Search_for_Client_NameTableAdapter.Fill(dataSet_Common_Functionality.sp01047_Core_Search_for_Client_Name, strCriteria);
            view.ExpandAllGroups();
            view.EndUpdate();
            if (view.DataRowCount <= 0) XtraMessageBox.Show("No matches found.", "Search", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        
        private void btnAcceptClientSelection_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedID1 = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID"));
                strSelectedDescription1 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client Name" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName")));
            }
            if (intSelectedID1 == 0)
            {
                XtraMessageBox.Show("Select a matched Client before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnAddClient_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancelClient_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        #endregion


        #region Sites Page

        private void teSiteCriteria_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            btnFindSite.Enabled = (!string.IsNullOrWhiteSpace(te.EditValue.ToString()));
        }

        private void btnFindSite_Click(object sender, EventArgs e)
        {
            string strCriteria = teSiteCriteria.EditValue.ToString();
            if (string.IsNullOrWhiteSpace(strCriteria))
            {
                XtraMessageBox.Show("Enter the value to find before proceeding.", "Search", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            switch (cbeSiteMatchType.EditValue.ToString())
            {
                case "Site Name Starts With":
                    {
                        strCriteria += "%";
                    }
                    break;
                case "Site Name Contains":
                    {
                        strCriteria = "%" + strCriteria + "%";
                    }
                    break;
                case "Site Name Ends With":
                    {
                        strCriteria = "%" + strCriteria;
                    }
                    break;
            }

            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            sp01048_Core_Search_for_Site_NameTableAdapter.Fill(dataSet_Common_Functionality.sp01048_Core_Search_for_Site_Name, strCriteria, strPassedInFilterClientIDs);
            view.ExpandAllGroups();
            view.EndUpdate();
            if (view.DataRowCount <= 0) XtraMessageBox.Show("No matches found.", "Search", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void btnAcceptSiteSelection_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedID1 = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteID"));
                intSelectedID2 = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID"));
                strSelectedDescription1 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteName"))) ? "Unknown Site Name" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteName")));
                strSelectedDescription2 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client Name" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName")));
                strSelectedDescription3 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteCode"))) ? "" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteCode")));
                strSelectedDescription4 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteAddress"))) ? "" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteAddress")));
                strSelectedDescription5 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SitePostcode"))) ? "" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SitePostcode")));
                dblSelectedDouble1 = Convert.ToDouble(view.GetRowCellValue(view.FocusedRowHandle, "SiteLocationX"));
                dblSelectedDouble2 = Convert.ToDouble(view.GetRowCellValue(view.FocusedRowHandle, "SiteLocationY"));               
            }
            if (intSelectedID1 == 0)
            {
                XtraMessageBox.Show("Select a matched Site before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnAddSite_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancelSite_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        #endregion


        #region Client Contacts Page

        private void teClientContactCriteria_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            btnFindClientContact.Enabled = (!string.IsNullOrWhiteSpace(te.EditValue.ToString()));
        }

        private void btnFindClientContact_Click(object sender, EventArgs e)
        {
            string strCriteria = teClientContactCriteria.EditValue.ToString();
            if (string.IsNullOrWhiteSpace(strCriteria))
            {
                XtraMessageBox.Show("Enter the value to find before proceeding.", "Search", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            switch (cbeClientContactMatchType.EditValue.ToString())
            {
                case "Contact Name Starts With":
                    {
                        strCriteria += "%";
                    }
                    break;
                case "Contact Name Contains":
                    {
                        strCriteria = "%" + strCriteria + "%";
                    }
                    break;
                case "Contact Name Ends With":
                    {
                        strCriteria = "%" + strCriteria;
                    }
                    break;
            }

            GridView view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            sp01051_Core_Search_For_Client_Contact_PersonTableAdapter.Fill(dataSet_Common_Functionality.sp01051_Core_Search_For_Client_Contact_Person, strCriteria, strPassedInFilterClientIDs);
            view.ExpandAllGroups();
            view.EndUpdate();
            if (view.DataRowCount <= 0) XtraMessageBox.Show("No matches found.", "Search", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void btnAcceptClientContactSelection_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedID1 = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientContactPersonID"));
                intSelectedID2 = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID"));
                strSelectedDescription1 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "PersonName"))) ? "Unknown Contact Person" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "PersonName")));
                strSelectedDescription2 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client Name" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName")));
                strSelectedDescription3 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Position"))) ? "Unknown Position" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Position")));
                strSelectedDescription4 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Title"))) ? "Unknown Title" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Title")));
                strSelectedDescription5 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientContactTelephone"))) ? "" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientContactTelephone")));
                strSelectedDescription6 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientContactMobile"))) ? "" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientContactMobile")));
                strSelectedDescription7 = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientContactEmail"))) ? "" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientContactEmail")));
            }
            if (intSelectedID1 == 0)
            {
                XtraMessageBox.Show("Select a matched Client Contact Person before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnAddClientContact_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancelClientContact_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        #endregion






    }
}


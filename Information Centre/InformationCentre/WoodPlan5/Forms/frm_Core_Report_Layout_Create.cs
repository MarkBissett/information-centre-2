using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace WoodPlan5
{
    public partial class frm_Core_Report_Layout_Create : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intReturnedValue = 0;
        public string strReturnedLayoutName = "";

        #endregion

        public frm_Core_Report_Layout_Create()
        {
            InitializeComponent();
            InitValidationRules();
        }

        private void frm_Core_Report_Layout_Create_Load(object sender, EventArgs e)
        {
            this.FormID = 500296;
 
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            dxValidationProvider1.ValidationMode = ValidationMode.Auto;
            dxValidationProvider1.Validate();

            sp01055_Core_Get_Report_LayoutsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01055_Core_Get_Report_LayoutsTableAdapter.Fill(this.dataSet_Common_Functionality.sp01055_Core_Get_Report_Layouts, 7, 101, 0);
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            gridControl1.Enabled = checkEdit1.Checked;
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void InitValidationRules()
        {
            // Create Rule // 
            ConditionValidationRule notEmptyValidationRule = new ConditionValidationRule();
            notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank;
            notEmptyValidationRule.ErrorText = "Please enter a value [100 characters maximum]";

            // Link Rule to control //
            dxValidationProvider1.SetValidationRule(textEdit1, notEmptyValidationRule);
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {

            strReturnedLayoutName = textEdit1.Text.Trim();
            if (strReturnedLayoutName == null || strReturnedLayoutName == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter a unique descriptor for the new layout in the box provided before proceeding.", "Create Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Check the Layout Descriptor is Unique //
            int intMatchingDescriptors = 0;
            using (var GetValue = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
            {
                GetValue.ChangeConnectionString(strConnectionString);
                try
                {
                    intMatchingDescriptors = Convert.ToInt32(GetValue.sp01056_Core_Report_Add_Layouts_Check_Descriptor_Unique(101, strReturnedLayoutName));
                    if (intMatchingDescriptors > 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The Layout Descriptor [" + strReturnedLayoutName + "] is already linked to another layout!\n\nPlease enter a different descriptor before proceeding.", "Create Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
                catch (Exception) { }
            } 
            
            if (checkEdit1.Checked)
            {
                GridView view = (GridView)gridControl1.MainView;
                if (view.RowCount == 0 || view.FocusedRowHandle < 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select the layout to use as a starting point from the list available before proceeding.", "Create Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                intReturnedValue = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
            }
            else if (checkEdit2.Checked)
            {
                intReturnedValue = 0; // Use a blank layout //
            }
            else
            {
                intReturnedValue = -1; // Use a default layout //
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            intReturnedValue = -1;
            this.DialogResult = DialogResult.No;
            this.Close();

        }




    }
}


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors.DXErrorProvider;  // For Validation Rules //
using DevExpress.Utils;

namespace WoodPlan5
{
    public partial class frmUserScreenSettingsEditLayout : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public WaitDialogForm loadingForm = null;
        public bool boolAllowEdit = false;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        public frmBase frmToProcess;

        #endregion

        public frmUserScreenSettingsEditLayout()
        {
            InitializeComponent();
        }

        private void InitValidationRules()
        {
            // Create Rule // 
            ConditionValidationRule notEmptyValidationRule = new ConditionValidationRule();
            notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank;
            notEmptyValidationRule.ErrorText = "Please enter a value [100 characters maximum]";

            // Link Rule to control //
            dxValidationProvider1.SetValidationRule(LayoutDescriptionTextEdit, notEmptyValidationRule);
        }

        private void frmUserScreenSettingsEditLayout_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            dxValidationProvider1.ValidationMode = ValidationMode.Auto;
            dxValidationProvider1.Validate();

            dataLayoutControl1.BeginUpdate();
            sp00088_edit_user_screen_layoutTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00088_edit_user_screen_layoutTableAdapter.Fill(this.dataSet_Groups.sp00088_edit_user_screen_layout, strRecordIDs);
            dataLayoutControl1.EndUpdate();
            if (strFormMode != "blockedit") InitValidationRules();

            if (loadingForm != null) loadingForm.Close();
        }

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.CancelEdit)
            {
                e.Handled = true;
                this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows.Clear();  // Abort any changes //
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
            else if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.EndEdit)
            {
                e.Handled = true;
                if (SaveChanges(false) == 1)
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Commit any outstanding changes to underlying dataset //

            for (int i = 0; i < this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows.Count; i++)
            {
                switch (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (strFormMode == "blockedit")
                {
                    strMessage = "You are block editing " + Convert.ToString(intRecordCount) + " records on the current screen but you have not saved the block edit changes!\n";
                }
                else
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
            }
            return strMessage;
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private int SaveChanges(Boolean ib_SuccessMessage)
        {
            // Paramters - ib_SuccessMessage - determins if the success message box should be shown at the end of the event //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            // Check to ensure all required fields have values //
            int intErrorFound = 0;
            string strErrorMessage = "";

            DataSet_GroupsTableAdapters.QueriesTableAdapter GetCount = new DataSet_GroupsTableAdapters.QueriesTableAdapter();
            GetCount.ChangeConnectionString(strConnectionString);
            int intMatchingDescriptors = 0;
            for (int i = this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows.Count - 1; i >= 0; i--)
            {
                if (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i].RowState == DataRowState.Added || this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i].RowState == DataRowState.Modified)
                {
                    if (strFormMode != "blockedit")
                    {
                        if (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutDescription"] == DBNull.Value)
                        {
                            intErrorFound = 1;
                            strErrorMessage += "Missing Layout Description\n";
                        }


                        intMatchingDescriptors = Convert.ToInt32(GetCount.sp00085_save_user_screen_Check_Descriptor_Unique(Convert.ToString(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutDescription"]), Convert.ToInt32(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["ScreenID"]), Convert.ToInt32(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutID"])));
                        if (intMatchingDescriptors > 0)
                        {
                            intErrorFound = 2;
                            strErrorMessage += "Duplicate Layout Description - " + this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutDescription"].ToString() + "\n";
                        }
                    }
                    if (intErrorFound > 0)
                    {
                        break;
                    }
                }
            }
            if (intErrorFound > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The following errors are present...\n\n" + strErrorMessage + "\nPlease fix then try re-saving.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }

            // Checks passed successfully so OK to proceed with save //
            loadingForm = new WaitDialogForm("Saving Changes...", "Layouts");
            loadingForm.Show();

            Boolean boolSaveSuccessfull = true;
            int intSaveFailCount = 0;

            int? intLayoutID = null;
            string strLayoutDescription = null;
            string strLayoutRemarks = null;
            int? intDefault = null;

            for (int i = this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows.Count - 1; i >= 0; i--)
            {
                switch (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i].RowState)
                {
                    case DataRowState.Added:  // Handles Add and Block Edit via the strFormMode passed in the parameter string to the SP //
                        boolSaveSuccessfull = true;

                        if (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutDescription"] == DBNull.Value) strLayoutDescription = null;
                        else strLayoutDescription = Convert.ToString(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutDescription"]);

                        if (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutRemarks"] == DBNull.Value) strLayoutRemarks = null;
                        else strLayoutRemarks = Convert.ToString(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutRemarks"]);

                        if (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["DefaultLayout"] == DBNull.Value) intDefault = null;
                        else intDefault = Convert.ToInt32(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["DefaultLayout"]);

                        try
                        {
                            intLayoutID = Convert.ToInt32(sp00088_edit_user_screen_layoutTableAdapter.sp00089_edit_user_screen_layout_save_changes(strFormMode, 0, strRecordIDs, strLayoutDescription, strLayoutRemarks, intDefault));
                        }
                        catch (Exception Ex)
                        {
                            boolSaveSuccessfull = false;
                            intSaveFailCount++;
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while saving. Please try again.\n\nException:" + Ex.Message, "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return 0;
                        }
                        if (boolSaveSuccessfull && strFormMode == "add")  // Update Data on front end with new ID [adding only , not block editing] //
                        {
                            this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutID"] = intLayoutID;
                            strFormMode = "edit";  // Switch form mode so that record is not added on any subsequent saves //
                        }
                        break;

                    case DataRowState.Modified:
                        boolSaveSuccessfull = true;
                        if (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutID"] == DBNull.Value) intLayoutID = null;
                        else intLayoutID = Convert.ToInt32(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutID"]);

                        if (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutDescription"] == DBNull.Value) strLayoutDescription = null;
                        else strLayoutDescription = Convert.ToString(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutDescription"]);

                        if (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutRemarks"] == DBNull.Value) strLayoutRemarks = null;
                        else strLayoutRemarks = Convert.ToString(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["LayoutRemarks"]);

                        if (this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["DefaultLayout"] == DBNull.Value) intDefault = null;
                        else intDefault = Convert.ToInt32(this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i]["DefaultLayout"]);

                        try
                        {
                            sp00088_edit_user_screen_layoutTableAdapter.sp00089_edit_user_screen_layout_save_changes(strFormMode, intLayoutID, "", strLayoutDescription, strLayoutRemarks, intDefault);
                        }
                        catch (Exception Ex)
                        {
                            boolSaveSuccessfull = false;
                            intSaveFailCount++;
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while saving. Please try again.\n\nException:" + Ex.Message, "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return 0;
                        }
                        break;
                }
                if (boolSaveSuccessfull)
                {
                    this.dataSet_Groups.sp00088_edit_user_screen_layout.Rows[i].AcceptChanges();  // Set row status to Unchanged so that update is not fired again unless further changes are made //
                }
            }
            loadingForm.Close();
            if (!boolSaveSuccessfull) // Error Occurred //
            {
                return 0;
            }
            return 1;
        }


    
 
    }
}


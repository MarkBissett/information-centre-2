using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using LocusEffects;

using BaseObjects;
using WoodPlan5.Properties;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_Core_Web_Service_Queue_Viewer : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInDrillDownIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        string i_str_selected_Module_IDs = "";
        string i_str_selected_Modules = "";
        BaseObjects.GridCheckMarksSelection selection3;
        SuperToolTip superToolTipModuleFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsModuleFilter = null;

        string i_str_selected_RecordType_IDs = "";
        string i_str_selected_RecordTypes = "";
        BaseObjects.GridCheckMarksSelection selection2;
        SuperToolTip superToolTipRecordTypeFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsRecordTypeFilter = null;

        string i_str_selected_Status_IDs = "";
        string i_str_selected_Statuses = "";
        BaseObjects.GridCheckMarksSelection selection5;
        SuperToolTip superToolTipStatusFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsStatusFilter = null;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState39;  // Used by Grid View State Facilities //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs39 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        
        private string strDefaultPicturesPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        private DataSet_Selection DS_Selection1;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion
        
        public frm_Core_Web_Service_Queue_Viewer()
        {
            InitializeComponent();
        }

        private void frm_Core_Web_Service_Queue_Viewer_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 8006;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            sp01024_Core_Web_Service_Queue_Items_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "QueueID");

            dateEditFromDate.DateTime = DateTime.Now.AddDays(-1);
            dateEditToDate.DateTime = DateTime.Now;

            sp01025_Core_Web_Service_Queue_Items_For_Queue_RecordTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView4, "QueueItemID");

            sp01026_Core_Web_Service_Queue_Images_For_Queue_RecordTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState39 = new RefreshGridState(gridView39, "ImageID");

            // Add record selection checkboxes to popup Table Filter grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;
            
            sp01021_Core_Web_Service_Queue_Viewer_Module_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01021_Core_Web_Service_Queue_Viewer_Module_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01021_Core_Web_Service_Queue_Viewer_Module_List);
            gridControl3.ForceInitialize();

            // Add record selection checkboxes to popup Table RecordType grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp01022_Core_Web_Service_Queue_Viewer_RecordType_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01022_Core_Web_Service_Queue_Viewer_RecordType_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01022_Core_Web_Service_Queue_Viewer_RecordType_List);
            gridControl2.ForceInitialize();

            // Add record selection checkboxes to popup Table RecordType grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;

            sp01023_Core_Web_Service_Queue_Viewer_Status_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01023_Core_Web_Service_Queue_Viewer_Status_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01023_Core_Web_Service_Queue_Viewer_Status_List);
            gridControl5.ForceInitialize();

            memoExEditValue.EditValue = "";

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                popupContainerControlModuleFilter.Text = "Custom Filter";
                popupContainerControlRecordTypeFilter.Text = "Custom Filter";
                popupContainerControlStatusFilter.Text = "Custom Filter";

                Load_Data();  // Load records //
            }

            popupContainerControlModuleFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlRecordTypeFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlStatusFilter.Size = new System.Drawing.Size(270, 400);

            // Create a SuperToolTip //
            superToolTipModuleFilter = new SuperToolTip();
            superToolTipSetupArgsModuleFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsModuleFilter.Title.Text = "Module Filter - Information";
            superToolTipSetupArgsModuleFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsModuleFilter.Contents.Text = "I store the currently applied Module Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsModuleFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsModuleFilter.Footer.Text = "";
            superToolTipModuleFilter.Setup(superToolTipSetupArgsModuleFilter);
            superToolTipModuleFilter.AllowHtmlText = DefaultBoolean.True;
            popupContainerEditModuleFilter.SuperTip = superToolTipModuleFilter;

            // Create a SuperToolTip //
            superToolTipRecordTypeFilter = new SuperToolTip();
            superToolTipSetupArgsRecordTypeFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsRecordTypeFilter.Title.Text = "Record Type Filter - Information";
            superToolTipSetupArgsRecordTypeFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsRecordTypeFilter.Contents.Text = "I store the currently applied Record Type Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsRecordTypeFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsRecordTypeFilter.Footer.Text = "";
            superToolTipRecordTypeFilter.Setup(superToolTipSetupArgsRecordTypeFilter);
            superToolTipRecordTypeFilter.AllowHtmlText = DefaultBoolean.True;
            popupContainerEditRecordTypeFilter.SuperTip = superToolTipRecordTypeFilter;

            // Create a SuperToolTip //
            superToolTipStatusFilter = new SuperToolTip();
            superToolTipSetupArgsStatusFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsStatusFilter.Title.Text = "Status Filter - Information";
            superToolTipSetupArgsStatusFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsStatusFilter.Contents.Text = "I store the currently applied Status Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsStatusFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsStatusFilter.Footer.Text = "";
            superToolTipStatusFilter.Setup(superToolTipSetupArgsStatusFilter);
            superToolTipStatusFilter.AllowHtmlText = DefaultBoolean.True;
            popupContainerEditStatusFilter.SuperTip = superToolTipStatusFilter;

            emptyEditor = new RepositoryItem();
         }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_Core_Web_Service_Queue_Viewer_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                Load_Data();
            }
            SetMenuStatus();
        }

        private void frm_Core_Web_Service_Queue_Viewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ModuleFilter", i_str_selected_Module_IDs);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "RecordTypeFilter", i_str_selected_RecordType_IDs);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "StatusFilter", i_str_selected_Status_IDs);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate", dateEditFromDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDate", dateEditToDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "strShowErrorOnly", checkEditShowErrorsOnly.Checked.ToString());
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
             // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //
                
                string strItemFilter = "";

                // Module Filter //
                int intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ModuleFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl3.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ModuleID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditModuleFilter.Text = PopupContainerEditModuleFilter_Get_Selected();
                }

                // Record Type Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("RecordTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl2.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["RecordTypeID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditRecordTypeFilter.Text = PopupContainerEditRecordTypeFilter_Get_Selected();
                }

                // Status Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("StatusFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["StatusID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditStatusFilter.Text = PopupContainerEditStatusFilter_Get_Selected();
                }


                // From Date //
                string strFromDate = default_screen_settings.RetrieveSetting("FromDate");
                if (!string.IsNullOrEmpty(strFromDate)) dateEditFromDate.DateTime = Convert.ToDateTime(strFromDate);

                // To Date //
                string strToDate = default_screen_settings.RetrieveSetting("ToDate");
                if (!string.IsNullOrEmpty(strToDate)) dateEditToDate.DateTime = Convert.ToDateTime(strToDate);

                // Show Errors Only //
                string strShowErrorOnly = default_screen_settings.RetrieveSetting("strShowErrorOnly");
                if (!string.IsNullOrEmpty(strShowErrorOnly)) checkEditShowErrorsOnly.Checked = Convert.ToBoolean(strShowErrorOnly);

                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    if (GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesActionManagerLoadActionsOnOpen").ToString() == "On")
                    {
                        bbiRefresh.PerformClick();  // Switched on so load data //
                        GridView view = (GridView)gridControl1.MainView;
                        view.ExpandAllGroups();
                    }
                }
                catch (Exception)
                {
                }
                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                System.Drawing.Point location = dockPanelFilters.PointToScreen(System.Drawing.Point.Empty);
                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            i_str_AddedRecordIDs1 = strNewIDs1;
            i_str_AddedRecordIDs2 = strNewIDs2;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = true;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            int[] intParentRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intParentRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Queue Records //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    //alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    //bsiAdd.Enabled = true;
                    //bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 2)  // Linked Items //
            {
                view = (GridView)gridControl4.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    //alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    //bsiAdd.Enabled = true;
                   // bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        //alItems.Add("iBlockAdd");
                        //bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    //alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    //bsiEdit.Enabled = true;
                    //bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        //alItems.Add("iBlockEdit");
                        //bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 39)  // Linked Images //
            {
                view = (GridView)gridControl39.MainView;
                intRowHandles = view.GetSelectedRows();

                if (iBool_AllowAdd && intParentRowHandles.Length == 1)
                {
                    //alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    //bsiAdd.Enabled = true;
                    //bbiSingleAdd.Enabled = true;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1)
                {
                    //alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    //bsiEdit.Enabled = true;
                    //bbiSingleEdit.Enabled = true;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intRowHandles.Length > 0);

            // Set enabled status of GridView4 navigator custom buttons //
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            
            // Set enabled status of GridView39 navigator custom buttons //
            view = (GridView)gridControl39.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
        }

        private void Block_Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "QueueID")) + ',');
                        }
                        var fChildForm = new frm_Core_Web_Service_Queue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "QueueID")) + ',');
                        }
                        var fChildForm = new frm_Core_Web_Service_Queue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
       }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Actions //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Queue Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Queue Record" : Convert.ToString(intRowHandles.Length) + " Queue Records") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Queue Record" : "these Queue Records") + " will no longer be available for selection and " + (intCount == 1 ? "it" : "they") + " will not be processed by the Web Service Queue if " + (intCount == 1 ? "it has" : "they have") + " not already been processed!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "QueueID")) + ",";
                        }
                        using (var RemoveRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                        {
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            try
                            {
                                RemoveRecords.sp01000_Core_Delete("queue_record", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception) { }
                        }
                        Load_Data();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:  // Linked Items //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl4.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Queue Items to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Queue Item" : Convert.ToString(intRowHandles.Length) + " Linked Queue Items") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Queue Item" : "these Linked Queue Items") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "QueueItemID")) + ",";
                        }
                        using (var RemoveRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                        {
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            try
                            {
                                RemoveRecords.sp01000_Core_Delete("queue_item", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception) { }
                        }
                        LoadLinkedRecords();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 39:  // Linked Tree Pictures //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl39.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Images to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Image" : Convert.ToString(intRowHandles.Length) + " Linked Images") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Linked Image" : "these Linked Images") + " will no longer be available for selection and they will not be processed by the Web Service Queue if they have not already been processed!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            string strImageName = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ImageID")) + ",";
                            }
                            using (var RemoveRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp01000_Core_Delete("queue_image", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();
                            Load_Linked_Pictures();
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "QueueID")) + ',');
                        }
                        var fChildForm = new frm_Core_Web_Service_Queue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private void JSON_View_Data()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Queue Record //
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount != 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    string strData = "";
                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QueueID")) + ',';
                        strData = Convert.ToString(view.GetRowCellValue(intRowHandle, "Data"));
                    }
                    var fChildForm = new frm_Core_JSON_Viewer();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "view";
                    //fChildForm.strCaller = "frm_Core_Web_Service_Queue_Viewer";
                    fChildForm.strPassedInJSON = strData;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Check_Data_For_Errors()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to check before proceeding.", "Check Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intQueueID = 0;
            int intTypeID = 0;
            int intProcessed = 0;
            view.BeginUpdate();
            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "TypeID")) == 5000 || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "TypeID")) == 5001 || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "TypeID")) == 1004)) view.UnselectRow(intRowHandle);
            }
            view.EndSelection();
            view.EndUpdate();

            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to check before proceeding.\n\nNote: The system will de-select any rows not appropriate for checking - only images can be checked currently.", "Check Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strData = "";
            string strRecordID = "";
            int intPictureType = 0;
            string strImageFileName = "";
            string strImageFirstPartOfPath = "";
            string strClientImageFolder = "";
            string strWinterMaintenanceImageFolder = "";

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strImageFirstPartOfPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Pictures and Job Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strWinterMaintenanceImageFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutPictureFilesFolderInternal").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Winter Maintenance Images (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Queue Data...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = intRowHandles.Length / 10;
            int intUpdateProgressTempCount = 0;

            foreach (int intRowHandle in intRowHandles)
            {
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                    System.Windows.Forms.Application.DoEvents();
                }

                intTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "TypeID"));
                intQueueID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "QueueID"));
                intProcessed = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "StatusID"));
                strData = view.GetRowCellValue(intRowHandle, "Data").ToString();
                if (string.IsNullOrWhiteSpace(strData)) continue;  // Move to next record //

                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "TypeID")) != 1004)  // Skip for Winter Maintenance images as they are all stored in the same folder //
                {
                    using (var sr = new StringReader(strData))
                    using (var jr = new Newtonsoft.Json.JsonTextReader(sr))
                    {
                        var js = new Newtonsoft.Json.JsonSerializer();
                        var u = js.Deserialize<SOMA_Image_Record>(jr);
                        if (string.IsNullOrEmpty(u.jobId))
                        {
                            gridView1.SetRowCellValue(intRowHandle, "CalculatedStatus", "Missing Parent Record ID");
                            continue;
                        }
                        else
                        {
                            strRecordID = (intTypeID == 5000 ? u.jobId : u.visitId);
                            strImageFileName = u.filename;
                            intPictureType = u.pictureType;
                        }
                    }
                    switch (intPictureType)
                    {
                        case 0:
                            intPictureType = 1;  // Start Picture //
                            break;
                        case 1:
                            intPictureType = 2;  // End Pricture //
                            break;
                        case 2:
                            intPictureType = 99;  // Visit Signature //
                            break;
                        case 4:
                            intPictureType = 4;  // Visit No Access Picture
                            break;
                        case 5:
                            intPictureType = 3;  // Visit In-Progress Picture //
                            break;
                        default:
                            break;
                    }
                    using (var conn = new SqlConnection(strConnectionString))  // Get Client Path //
                    {
                        conn.Open();
                        try
                        {
                            using (var cmd = new SqlCommand())
                            {
                                cmd.CommandText = "sp_Web_Service_OM_00017_Get_Client_Image_Folder_Name";
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@RecordID", strRecordID));
                                cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intPictureType));
                                cmd.Connection = conn;
                                strClientImageFolder = cmd.ExecuteScalar().ToString();

                                if (strClientImageFolder == "ERROR")
                                {
                                    gridView1.SetRowCellValue(intRowHandle, "CalculatedStatus", "Parent Missing or Client Image Folder Not Set");
                                    continue;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            gridView1.SetRowCellValue(intRowHandle, "CalculatedStatus", "Problem accessing Client Image Folder");
                            continue;
                        }
                        conn.Close();
                    }
                }
                else
                {
                    using (var sr = new StringReader(strData))
                    using (var jr = new Newtonsoft.Json.JsonTextReader(sr))
                    {
                        var js = new Newtonsoft.Json.JsonSerializer();
                        var u = js.Deserialize<SOMA_Image_Record>(jr);
                        if (string.IsNullOrEmpty(u.CalloutID))
                        {
                            gridView1.SetRowCellValue(intRowHandle, "CalculatedStatus", "Missing Parent Record ID");
                            continue;
                        }
                        else
                        {
                            strRecordID = u.CalloutID;
                            strImageFileName = u.filename;
                            intPictureType = u.pictureType;
                        }
                    }
                }
                // Check if Queue - Image Table has matching row //
                try
                {
                    DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter GetCount = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                    GetCount.ChangeConnectionString(strConnectionString);
                    if (Convert.ToInt32(GetCount.sp06435_OM_Queue_Viewer_Check_Image_Data(intQueueID)) <= 0)
                    {
                        gridView1.SetRowCellValue(intRowHandle, "CalculatedStatus", "Linked Image Data Missing");
                        continue;
                    }
                }
                catch (Exception ex)
                {
                }

                // Parent record and Image folder set so see if image on file system //
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "TypeID")) != 1004)  // Summer Maintenance //
                {
                    strClientImageFolder = Path.Combine(strImageFirstPartOfPath, strClientImageFolder);
                    if (!File.Exists(Path.Combine(strClientImageFolder, strImageFileName)))
                    {
                        gridView1.SetRowCellValue(intRowHandle, "CalculatedStatus", "File Not Created on File System");
                        continue;
                    }
                }
                else  // Winter Maintenance //
                {
                    if (!File.Exists(Path.Combine(strWinterMaintenanceImageFolder, strImageFileName)))
                    {
                        gridView1.SetRowCellValue(intRowHandle, "CalculatedStatus", "File Not Created on File System");
                        continue;
                    }
                }
                gridView1.SetRowCellValue(intRowHandle, "CalculatedStatus", "Valid To Retry Upload");
                continue;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }

        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Queue Records - Adjust any filters then reload data";
                    break;
                case "gridView4":
                    message = "No Linked Items Available - Select one or more Queue Records to view Linked Items";
                    break;
                case "gridView39":
                    message = "No Linked Images Available - Select one or more Queue Records to view Linked Items";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    if (splitContainerControl2.PanelVisibility == SplitPanelVisibility.Both)
                    {
                        LoadLinkedRecords();
                        view = (GridView)gridControl4.MainView;
                        view.ExpandAllGroups();

                        Load_Linked_Pictures();
                        view = (GridView)gridControl39.MainView;
                        view.ExpandAllGroups();
                        Set_Selected_Count();
                    }
                    break;
                case "gridView39":
                    if (view.SelectedRowsCount == 1)
                    {
                        LoadImagePreview();
                    }
                    else
                    {
                        pictureEditPreview.Image = null;
                    }
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "ErrorCount":
                    {
                        int intError = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ErrorCount"));
                        if (intError >= 10)
                        {
                            //e.Appearance.BackColor = Color.LightCoral;
                            //e.Appearance.BackColor2 = Color.Red;
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                        else if (intError >= 1)
                        {
                            e.Appearance.BackColor = Color.Khaki;
                            e.Appearance.BackColor2 = Color.DarkOrange;
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "CalculatedStatus":
                    {
                        string strStatus = view.GetRowCellValue(e.RowHandle, "CalculatedStatus").ToString();
                        if (strStatus == "Valid To Retry Upload")
                        {
                            //e.Appearance.BackColor = Color.LightCoral;
                            //e.Appearance.BackColor2 = Color.Red;
                            e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                            e.Appearance.BackColor2 = Color.PaleGreen;
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                        else if (string.IsNullOrWhiteSpace(strStatus))
                        {
                            // No Formatting colour //
                        }
                        else // Error //
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Set_Selected_Count()
        {
            GridView view = (GridView)gridControl1.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            if (intSelectedCount == 0)
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Queue Records Selected" : "<color=red>" + intSelectedCount.ToString() + " Queue Records</Color> Selected");
                }
                bsiSelectedCount.Caption = strText;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("json viewer".Equals(e.Button.Tag))
                    {
                        JSON_View_Data();
                    }
                    else if ("check".Equals(e.Button.Tag))
                    {
                        Check_Data_For_Errors();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView4 Linked Items

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Grid - Linked Images

        private void gridView39_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            //if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView39_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView39_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 39;
            SetMenuStatus();
        }

        private void gridView39_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 39;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl39_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl39.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void LoadImagePreview()
        {
            GridView view = (GridView)gridControl39.MainView;
            if (view.SelectedRowsCount != 1) 
            {
               return;
            }
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Image Preview...");
            }
            try
            {
                int intImageID = Convert.ToInt32(view.GetFocusedRowCellValue("ImageID"));
                SqlDataAdapter sdaQueueImageData = null;
                DataSet dsQueueImageData = null;
                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = null;
                    cmd = new SqlCommand("sp01027_Core_Web_Service_Queue_Image_For_Preview", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ImageID", intImageID));
                    sdaQueueImageData = new SqlDataAdapter(cmd);
                    dsQueueImageData = new DataSet("NewDataSet");
                    sdaQueueImageData.Fill(dsQueueImageData, "Table");
                    conn.Close();
                }
                if (dsQueueImageData.Tables[0].Rows.Count <= 0) 
                {
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    return;  // Abort //
                }
                MemoryStream streamImage = null;
                int ImageID = 0;
                DataRow dr = dsQueueImageData.Tables[0].Rows[0];
                streamImage = StripExcessDataFromJpeg(new MemoryStream((byte[])dr["Image"]));
                if (streamImage.Length <= 0)
                {
                    if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    return;  // Abort //
                }

                var image = Image.FromStream(streamImage);
                pictureEditPreview.Image = image;

            }
            catch (Exception ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred previewing the image - error: " + ex.Message, "Preview Image", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

        }

        public static MemoryStream StripExcessDataFromJpeg(MemoryStream ms)
        {
            MemoryStream ms2 = new MemoryStream();          
            BinaryReader br = new BinaryReader(ms);
            br.BaseStream.Position = 0;  // Move back to start position in memory stream as it is an the end of the stream after reading //
            try
            {
                bool done = false;
                long count = 0;
                //The file must be read until the end of the file (in which case there was no image) or until the jpeg file is found. //
                while ((count < br.BaseStream.Length) && !done)
                {
                    // has to be read one at a time so not to consume more than needed. //
                    count++;
                    if (br.ReadByte() == 0xFF)
                    {
                        count++;
                        if (br.ReadByte() == 0xD8)
                        {
                            done = true;
                            ms2 = WriteJpegBinaryToStream(br);
                        }
                    }
                }
                if (!done) ms2 = WriteJpegBinaryToStream(br);
            }
            catch (Exception ex)
            {
            }
            br.Close();
            return ms2;
            
        }
        private static MemoryStream WriteJpegBinaryToStream(BinaryReader br)
        {
            var ms = new MemoryStream();
            // Write back the jpeg header already consumed in the stream //
            byte[] jpegHeader = { 0xFF, 0xD8 };
            ms.Write(jpegHeader, 0, jpegHeader.Length);
            long count = 0;
            while (count < br.BaseStream.Length)
            {
                long bytesToRead = 1024; // Read 1kb at a time, increase if dealing with larger files. //
                if (bytesToRead + count > br.BaseStream.Length)
                {
                    bytesToRead = br.BaseStream.Length - count;
                }
                byte[] bytes = new byte[bytesToRead];
                br.Read(bytes, 0, bytes.Length);
                count += bytesToRead;
                ms.Write(bytes, 0, bytes.Length);
            }
            return ms;
        }

        #endregion


        #region Table Filter Panel

        private void popupContainerEditModuleFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditModuleFilter_Get_Selected();
        }

        private void btnModuleFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditModuleFilter_Get_Selected()
        {
            i_str_selected_Module_IDs = "";    // Reset any prior values first //
            i_str_selected_Modules = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                superToolTipSetupArgsModuleFilter.Contents.Text = "I store the currently applied Module Filter.\n\nNo Filter Set.";
                i_str_selected_Module_IDs = "";
                return "No Module Filter";
            }
            else if (selection3.SelectedCount <= 0)
            {
                superToolTipSetupArgsModuleFilter.Contents.Text = "I store the currently applied Module Filter.\n\nNo Filter Set.";
                i_str_selected_Module_IDs = "";
                return "No Module Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Module_IDs += Convert.ToString(view.GetRowCellValue(i, "ModuleID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Modules = Convert.ToString(view.GetRowCellValue(i, "ModuleName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Modules += ", " + Convert.ToString(view.GetRowCellValue(i, "ModuleName"));
                        }
                        intCount++;
                    }
                }
                // Update Filter control's tooltip //
                string strTooltipText = i_str_selected_Modules.Replace(", ", "\n");
                superToolTipSetupArgsModuleFilter.Contents.Text = "I store the currently applied Module Filter.\n\n" + strTooltipText;
            }
            return i_str_selected_Modules;
        }

        #endregion


        #region Record Type Filter Panel

        private void popupContainerEditRecordTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditRecordTypeFilter_Get_Selected();
        }

        private void btnRecordTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditRecordTypeFilter_Get_Selected()
        {
            i_str_selected_RecordType_IDs = "";    // Reset any prior values first //
            i_str_selected_RecordTypes = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                superToolTipSetupArgsRecordTypeFilter.Contents.Text = "I store the currently applied Record Type Filter.\n\nNo Filter Set.";
                i_str_selected_RecordType_IDs = "";
                return "No Record Type Filter";
            }
            else if (selection2.SelectedCount <= 0)
            {
                superToolTipSetupArgsRecordTypeFilter.Contents.Text = "I store the currently applied Record Type Filter.\n\nNo Filter Set.";
                i_str_selected_RecordType_IDs = "";
                return "No Record Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_RecordType_IDs += Convert.ToString(view.GetRowCellValue(i, "RecordTypeID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_RecordTypes = Convert.ToString(view.GetRowCellValue(i, "RecordType"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_RecordTypes += ", " + Convert.ToString(view.GetRowCellValue(i, "RecordType"));
                        }
                        intCount++;
                    }
                }
                // Update Filter control's tooltip //
                string strTooltipText = i_str_selected_RecordTypes.Replace(", ", "\n");
                superToolTipSetupArgsRecordTypeFilter.Contents.Text = "I store the currently applied Record Type Filter.\n\n" + strTooltipText;
            }
            return i_str_selected_RecordTypes;
        }

        #endregion


        #region Status Filter Panel

        private void popupContainerEditStatusFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditStatusFilter_Get_Selected();
        }

        private void btnStatusFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditStatusFilter_Get_Selected()
        {
            i_str_selected_Status_IDs = "";    // Reset any prior values first //
            i_str_selected_Statuses = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                superToolTipSetupArgsStatusFilter.Contents.Text = "I store the currently applied Status Filter.\n\nNo Filter Set.";
                i_str_selected_Status_IDs = "";
                return "No Status Filter";
            }
            else if (selection5.SelectedCount <= 0)
            {
                superToolTipSetupArgsStatusFilter.Contents.Text = "I store the currently applied Status Filter.\n\nNo Filter Set.";
                i_str_selected_Status_IDs = "";
                return "No Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Status_IDs += Convert.ToString(view.GetRowCellValue(i, "StatusID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Statuses = Convert.ToString(view.GetRowCellValue(i, "Status"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Statuses += ", " + Convert.ToString(view.GetRowCellValue(i, "Status"));
                        }
                        intCount++;
                    }
                }
                // Update Filter control's tooltip //
                string strTooltipText = i_str_selected_Statuses.Replace(", ", "\n");
                superToolTipSetupArgsStatusFilter.Contents.Text = "I store the currently applied Status Filter.\n\n" + strTooltipText;
            }
            return i_str_selected_Statuses;
        }

        #endregion


        private void memoExEditValue_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString().ToLower())
            {
                case "clear":
                    {
                        memoExEditValue.EditValue = "";
                    }
                    break;
                default:
                    break;
            }

        }

        private void btnClearAllFilters_Click(object sender, EventArgs e)
        {
            Clear_Custom_Filter_Labels("", true);
        }

        private void Clear_Custom_Filter_Labels(string strCurrentControl, bool boolClearAll)
        {
            if (strCurrentControl != "popupContainerEditModuleFilter" && (popupContainerEditModuleFilter.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                i_str_selected_Module_IDs = "";
                popupContainerEditModuleFilter.Text = "No Module Filter";
                superToolTipSetupArgsModuleFilter.Contents.Text = "I store the currently applied Module Filter.\n\nNo Filter Set.";
            }
            if (strCurrentControl != "popupContainerEditRecordTypeFilter" && (popupContainerEditRecordTypeFilter.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                i_str_selected_RecordType_IDs = "";
                popupContainerEditRecordTypeFilter.Text = "No Module Filter";
                superToolTipSetupArgsRecordTypeFilter.Contents.Text = "I store the currently applied Record Type Filter.\n\nNo Filter Set.";
            }
            if (strCurrentControl != "popupContainerEditStatusFilter" && (popupContainerEditStatusFilter.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                i_str_selected_Status_IDs = "";
                popupContainerEditModuleFilter.Text = "No Module Filter";
                superToolTipSetupArgsStatusFilter.Contents.Text = "I store the currently applied Status Filter.\n\nNo Filter Set.";
            }
            if (strCurrentControl != "memoExEditValue" && (boolClearAll))
            {
                memoExEditValue.EditValue = "";
            }
        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Queue Records...");
            }

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            int intShowLastInspection = (checkEditShowErrorsOnly.Checked ? 1 : 0);
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            if (popupContainerEditModuleFilter.Text == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Inspections //
            {
                sp01024_Core_Web_Service_Queue_Items_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01024_Core_Web_Service_Queue_Items_List, strPassedInDrillDownIDs, "", "", "", dtFromDate, dtToDate, 0, "");
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                sp01024_Core_Web_Service_Queue_Items_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01024_Core_Web_Service_Queue_Items_List, "", i_str_selected_Module_IDs, i_str_selected_RecordType_IDs, i_str_selected_Status_IDs, dtFromDate, dtToDate, (checkEditShowErrorsOnly.Checked ? 1 : 0), (string.IsNullOrWhiteSpace(memoExEditValue.EditValue.ToString()) ? "" : memoExEditValue.EditValue.ToString()));
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["QueueID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (splitContainerControl2.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["QueueID"])) + ',';
            }

            //Populate Linked Documents //
            gridControl4.MainView.BeginUpdate();
            this.RefreshGridViewState2.SaveViewInfo();
            if (intCount == 0)
            {
                this.dataSet_AT.sp00220_Linked_Documents_List.Clear();
            }
            else
            {
                sp01025_Core_Web_Service_Queue_Items_For_Queue_RecordTableAdapter.Fill(dataSet_Common_Functionality.sp01025_Core_Web_Service_Queue_Items_For_Queue_Record, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl4.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl4.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["QueueItemID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }
        }

        private void Load_Linked_Pictures()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["QueueID"])) + ',';
            }

            gridControl39.BeginUpdate();
            this.RefreshGridViewState39.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp01026_Core_Web_Service_Queue_Images_For_Queue_RecordTableAdapter.Fill(dataSet_Common_Functionality.sp01026_Core_Web_Service_Queue_Images_For_Queue_Record,strSelectedIDs);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState39.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl39.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs39 != "")
            {
                strArray = i_str_AddedRecordIDs39.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl39.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ImageID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs39 = "";
            }
        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        private void splitContainerControl2_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) LoadLinkedRecords();
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void bciFilterData_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (bciFilterData.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Queue Records to filter by before proceeding.", "Filter Selected Queue Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControl1.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControl1.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControl1.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_AT.sp01200_TreeListALL.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControl1.EndUpdate();
        }






    }
}


namespace WoodPlan5
{
    /// <summary>
    /// 
    /// </summary>
    partial class frmUserSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUserSettings));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colPartID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.vGridControl1 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.sp00064GetStaffApplicationSettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.rpiMessageTimerSpinEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.rpiShowPhotoPanelCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemFontEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp00047AvailableStartUpScreensForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditEstatePlanInspectionControlPanel = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.categoryRow3 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowFirstScreenLoadedID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow2 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowShowToolTips = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowShowTipsOnStartup = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowShowConfirmations = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMinimiseToTaskBar = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowShowPhotoPopup = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDefaultFontName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDefaultFontSize = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow5 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowEstatePlanInspectionControlPanel = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.sp00064GetStaffApplicationSettingsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00064GetStaffApplicationSettingsTableAdapter();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.rowToDoPopupAlert = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.sp00068_Get_Magnifier_SettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00068_Get_Magnifier_SettingsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00068_Get_Magnifier_SettingsTableAdapter();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtpSettingsPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtpSettingsPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.vGridControl2 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.rpiMagnifierShapeLookupEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.sp00070GetMagnifierShapeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.categoryRow4 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMagnifierID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUserID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUserType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLocationX = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLocationY = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowCloseOnMouseUp = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDoubleBuffered = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowHideMouseCursor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRememberLastPoint = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowReturnToOrigin = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTopMostWindow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMagnifierWidth = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMagnifierHeight = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowZoomFactor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSpeedFactor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowStaffID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowMagnifierShape = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.sp00070_Get_Magnifier_ShapeTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00070_Get_Magnifier_ShapeTableAdapter();
            this.sp00047AvailableStartUpScreensForUserTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00047AvailableStartUpScreensForUserTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00064GetStaffApplicationSettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiMessageTimerSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiShowPhotoPanelCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00047AvailableStartUpScreensForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditEstatePlanInspectionControlPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00068_Get_Magnifier_SettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtpSettingsPage1.SuspendLayout();
            this.xtpSettingsPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiMagnifierShapeLookupEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00070GetMagnifierShapeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(600, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 487);
            this.barDockControlBottom.Size = new System.Drawing.Size(600, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 487);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(600, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 487);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colPartID
            // 
            this.colPartID.Caption = "Screen ID";
            this.colPartID.FieldName = "PartID";
            this.colPartID.Name = "colPartID";
            // 
            // vGridControl1
            // 
            this.vGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.vGridControl1.DataSource = this.sp00064GetStaffApplicationSettingsBindingSource;
            this.vGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridControl1.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.vGridControl1.Location = new System.Drawing.Point(0, 0);
            this.vGridControl1.MenuManager = this.barManager1;
            this.vGridControl1.Name = "vGridControl1";
            this.vGridControl1.OptionsView.AutoScaleBands = true;
            this.vGridControl1.OptionsView.FixRowHeaderPanelWidth = true;
            this.vGridControl1.RecordWidth = 118;
            this.vGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpiMessageTimerSpinEdit,
            this.rpiShowPhotoPanelCheckEdit,
            this.repositoryItemFontEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckEdit5,
            this.repositoryItemSpinEdit1,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemCheckEditEstatePlanInspectionControlPanel});
            this.vGridControl1.RowHeaderWidth = 82;
            this.vGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow1,
            this.categoryRow5});
            this.vGridControl1.Size = new System.Drawing.Size(591, 426);
            this.vGridControl1.TabIndex = 0;
            // 
            // sp00064GetStaffApplicationSettingsBindingSource
            // 
            this.sp00064GetStaffApplicationSettingsBindingSource.DataMember = "sp00064GetStaffApplicationSettings";
            this.sp00064GetStaffApplicationSettingsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanManDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rpiMessageTimerSpinEdit
            // 
            this.rpiMessageTimerSpinEdit.AutoHeight = false;
            this.rpiMessageTimerSpinEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rpiMessageTimerSpinEdit.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.rpiMessageTimerSpinEdit.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.rpiMessageTimerSpinEdit.Name = "rpiMessageTimerSpinEdit";
            // 
            // rpiShowPhotoPanelCheckEdit
            // 
            this.rpiShowPhotoPanelCheckEdit.Name = "rpiShowPhotoPanelCheckEdit";
            this.rpiShowPhotoPanelCheckEdit.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.rpiShowPhotoPanelCheckEdit.NullText = "0";
            this.rpiShowPhotoPanelCheckEdit.ValueChecked = 1;
            this.rpiShowPhotoPanelCheckEdit.ValueUnchecked = 0;
            // 
            // repositoryItemFontEdit1
            // 
            this.repositoryItemFontEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemFontEdit1.Name = "repositoryItemFontEdit1";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.NullText = "False";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit2.NullText = "False";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit3.NullText = "0";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit5.NullText = "0";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DataSource = this.sp00047AvailableStartUpScreensForUserBindingSource;
            this.repositoryItemGridLookUpEdit1.DisplayMember = "PartDescription";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEdit1.ValueMember = "PartID";
            // 
            // sp00047AvailableStartUpScreensForUserBindingSource
            // 
            this.sp00047AvailableStartUpScreensForUserBindingSource.DataMember = "sp00047AvailableStartUpScreensForUser";
            this.sp00047AvailableStartUpScreensForUserBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colModuleID,
            this.colModuleName,
            this.colPartDescription,
            this.colPartID,
            this.colPartOrder,
            this.colType,
            this.colTypeDescription});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colPartID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.repositoryItemGridLookUpEdit1View.GroupCount = 2;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colModuleName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPartOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPartDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            // 
            // colModuleName
            // 
            this.colModuleName.Caption = "Module Name";
            this.colModuleName.FieldName = "ModuleName";
            this.colModuleName.Name = "colModuleName";
            this.colModuleName.Width = 196;
            // 
            // colPartDescription
            // 
            this.colPartDescription.Caption = "Screen Name";
            this.colPartDescription.FieldName = "PartDescription";
            this.colPartDescription.Name = "colPartDescription";
            this.colPartDescription.Visible = true;
            this.colPartDescription.VisibleIndex = 0;
            this.colPartDescription.Width = 330;
            // 
            // colPartOrder
            // 
            this.colPartOrder.Caption = "Order";
            this.colPartOrder.FieldName = "PartOrder";
            this.colPartOrder.Name = "colPartOrder";
            // 
            // colType
            // 
            this.colType.Caption = "Screen Type ID";
            this.colType.FieldName = "Type";
            this.colType.Name = "colType";
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Screen Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.Width = 276;
            // 
            // repositoryItemCheckEditEstatePlanInspectionControlPanel
            // 
            this.repositoryItemCheckEditEstatePlanInspectionControlPanel.AutoHeight = false;
            this.repositoryItemCheckEditEstatePlanInspectionControlPanel.Name = "repositoryItemCheckEditEstatePlanInspectionControlPanel";
            this.repositoryItemCheckEditEstatePlanInspectionControlPanel.ValueChecked = 1;
            this.repositoryItemCheckEditEstatePlanInspectionControlPanel.ValueUnchecked = 0;
            // 
            // categoryRow1
            // 
            this.categoryRow1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow3,
            this.categoryRow2});
            this.categoryRow1.Name = "categoryRow1";
            this.categoryRow1.Properties.Caption = "My Application Settings";
            // 
            // categoryRow3
            // 
            this.categoryRow3.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowFirstScreenLoadedID});
            this.categoryRow3.Name = "categoryRow3";
            this.categoryRow3.Properties.Caption = "System Startup";
            // 
            // rowFirstScreenLoadedID
            // 
            this.rowFirstScreenLoadedID.Name = "rowFirstScreenLoadedID";
            this.rowFirstScreenLoadedID.Properties.Caption = "First Screen Loaded";
            this.rowFirstScreenLoadedID.Properties.FieldName = "FirstScreenLoadedID";
            this.rowFirstScreenLoadedID.Properties.RowEdit = this.repositoryItemGridLookUpEdit1;
            // 
            // categoryRow2
            // 
            this.categoryRow2.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowShowToolTips,
            this.rowShowTipsOnStartup,
            this.rowShowConfirmations,
            this.rowMinimiseToTaskBar,
            this.rowShowPhotoPopup,
            this.rowDefaultFontName,
            this.rowDefaultFontSize});
            this.categoryRow2.Name = "categoryRow2";
            this.categoryRow2.Properties.Caption = "System Configuration";
            // 
            // rowShowToolTips
            // 
            this.rowShowToolTips.Name = "rowShowToolTips";
            this.rowShowToolTips.Properties.Caption = "Show Tool Tips";
            this.rowShowToolTips.Properties.FieldName = "ShowToolTips";
            this.rowShowToolTips.Properties.RowEdit = this.repositoryItemCheckEdit1;
            // 
            // rowShowTipsOnStartup
            // 
            this.rowShowTipsOnStartup.Name = "rowShowTipsOnStartup";
            this.rowShowTipsOnStartup.Properties.Caption = "Show Tips On Startup";
            this.rowShowTipsOnStartup.Properties.FieldName = "ShowTipsOnStartup";
            this.rowShowTipsOnStartup.Properties.RowEdit = this.repositoryItemCheckEdit2;
            // 
            // rowShowConfirmations
            // 
            this.rowShowConfirmations.Name = "rowShowConfirmations";
            this.rowShowConfirmations.Properties.Caption = "Show Confirmations";
            this.rowShowConfirmations.Properties.FieldName = "ShowConfirmations";
            this.rowShowConfirmations.Properties.RowEdit = this.repositoryItemCheckEdit3;
            // 
            // rowMinimiseToTaskBar
            // 
            this.rowMinimiseToTaskBar.Height = 15;
            this.rowMinimiseToTaskBar.Name = "rowMinimiseToTaskBar";
            this.rowMinimiseToTaskBar.Properties.Caption = "Minimise To TaskBar";
            this.rowMinimiseToTaskBar.Properties.FieldName = "MinimiseToTaskBar";
            this.rowMinimiseToTaskBar.Properties.RowEdit = this.repositoryItemCheckEdit5;
            // 
            // rowShowPhotoPopup
            // 
            this.rowShowPhotoPopup.Name = "rowShowPhotoPopup";
            this.rowShowPhotoPopup.Properties.Caption = "Show Photo Panel";
            this.rowShowPhotoPopup.Properties.FieldName = "ShowPhotoPopup";
            this.rowShowPhotoPopup.Properties.RowEdit = this.rpiShowPhotoPanelCheckEdit;
            // 
            // rowDefaultFontName
            // 
            this.rowDefaultFontName.Name = "rowDefaultFontName";
            this.rowDefaultFontName.Properties.Caption = "Default Font Name";
            this.rowDefaultFontName.Properties.FieldName = "DefaultFontName";
            this.rowDefaultFontName.Properties.RowEdit = this.repositoryItemFontEdit1;
            // 
            // rowDefaultFontSize
            // 
            this.rowDefaultFontSize.Name = "rowDefaultFontSize";
            this.rowDefaultFontSize.Properties.Caption = "Default Font Size";
            this.rowDefaultFontSize.Properties.FieldName = "DefaultFontSize";
            this.rowDefaultFontSize.Properties.RowEdit = this.repositoryItemSpinEdit1;
            // 
            // categoryRow5
            // 
            this.categoryRow5.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowEstatePlanInspectionControlPanel});
            this.categoryRow5.Name = "categoryRow5";
            this.categoryRow5.Properties.Caption = "Asset Management";
            // 
            // rowEstatePlanInspectionControlPanel
            // 
            this.rowEstatePlanInspectionControlPanel.Name = "rowEstatePlanInspectionControlPanel";
            this.rowEstatePlanInspectionControlPanel.Properties.Caption = "Inspection Control Panel Visible";
            this.rowEstatePlanInspectionControlPanel.Properties.FieldName = "EstatePlanInspectionControlPanel";
            this.rowEstatePlanInspectionControlPanel.Properties.RowEdit = this.repositoryItemCheckEditEstatePlanInspectionControlPanel;
            // 
            // sp00064GetStaffApplicationSettingsTableAdapter
            // 
            this.sp00064GetStaffApplicationSettingsTableAdapter.ClearBeforeFill = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.ImageOptions.ImageIndex = 5;
            this.btnOK.ImageOptions.ImageList = this.imageCollection1;
            this.btnOK.Location = new System.Drawing.Point(423, 458);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Save_16x16, "Save_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Save_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.close_16x16, "close_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "close_16x16");
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ImageOptions.ImageIndex = 6;
            this.btnCancel.ImageOptions.ImageList = this.imageCollection1;
            this.btnCancel.Location = new System.Drawing.Point(517, 458);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // rowToDoPopupAlert
            // 
            this.rowToDoPopupAlert.Name = "rowToDoPopupAlert";
            this.rowToDoPopupAlert.Properties.Caption = "Popup Message Timer (secs)";
            this.rowToDoPopupAlert.Properties.FieldName = "PopupMessageTimer";
            this.rowToDoPopupAlert.Properties.RowEdit = this.rpiMessageTimerSpinEdit;
            // 
            // sp00068_Get_Magnifier_SettingsBindingSource
            // 
            this.sp00068_Get_Magnifier_SettingsBindingSource.DataMember = "sp00068_Get_Magnifier_Settings";
            this.sp00068_Get_Magnifier_SettingsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00068_Get_Magnifier_SettingsTableAdapter
            // 
            this.sp00068_Get_Magnifier_SettingsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 2);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtpSettingsPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(596, 452);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtpSettingsPage1,
            this.xtpSettingsPage2});
            // 
            // xtpSettingsPage1
            // 
            this.xtpSettingsPage1.Controls.Add(this.vGridControl1);
            this.xtpSettingsPage1.Name = "xtpSettingsPage1";
            this.xtpSettingsPage1.Size = new System.Drawing.Size(591, 426);
            this.xtpSettingsPage1.Text = "General Settings";
            // 
            // xtpSettingsPage2
            // 
            this.xtpSettingsPage2.Controls.Add(this.vGridControl2);
            this.xtpSettingsPage2.Name = "xtpSettingsPage2";
            this.xtpSettingsPage2.Size = new System.Drawing.Size(591, 426);
            this.xtpSettingsPage2.Text = "Magnifier Settings";
            // 
            // vGridControl2
            // 
            this.vGridControl2.DataSource = this.sp00068_Get_Magnifier_SettingsBindingSource;
            this.vGridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridControl2.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.vGridControl2.Location = new System.Drawing.Point(0, 0);
            this.vGridControl2.MenuManager = this.barManager1;
            this.vGridControl2.Name = "vGridControl2";
            this.vGridControl2.RecordWidth = 148;
            this.vGridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpiMagnifierShapeLookupEdit,
            this.repositoryItemCheckEdit7,
            this.repositoryItemCheckEdit8,
            this.repositoryItemCheckEdit9,
            this.repositoryItemCheckEdit10,
            this.repositoryItemCheckEdit11,
            this.repositoryItemCheckEdit12});
            this.vGridControl2.RowHeaderWidth = 52;
            this.vGridControl2.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow4});
            this.vGridControl2.Size = new System.Drawing.Size(591, 426);
            this.vGridControl2.TabIndex = 0;
            // 
            // rpiMagnifierShapeLookupEdit
            // 
            this.rpiMagnifierShapeLookupEdit.AutoHeight = false;
            this.rpiMagnifierShapeLookupEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpiMagnifierShapeLookupEdit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MagnifierShapeID", "MagnifierShapeID", 104, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MagnifierShapeDescription", "Shape", 133, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.rpiMagnifierShapeLookupEdit.DataSource = this.sp00070GetMagnifierShapeBindingSource;
            this.rpiMagnifierShapeLookupEdit.DisplayMember = "MagnifierShapeDescription";
            this.rpiMagnifierShapeLookupEdit.Name = "rpiMagnifierShapeLookupEdit";
            this.rpiMagnifierShapeLookupEdit.NullText = "";
            this.rpiMagnifierShapeLookupEdit.ValueMember = "MagnifierShapeID";
            // 
            // sp00070GetMagnifierShapeBindingSource
            // 
            this.sp00070GetMagnifierShapeBindingSource.DataMember = "sp00070_Get_Magnifier_Shape";
            this.sp00070GetMagnifierShapeBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            this.repositoryItemCheckEdit7.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            this.repositoryItemCheckEdit9.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // repositoryItemCheckEdit10
            // 
            this.repositoryItemCheckEdit10.AutoHeight = false;
            this.repositoryItemCheckEdit10.Name = "repositoryItemCheckEdit10";
            this.repositoryItemCheckEdit10.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // repositoryItemCheckEdit11
            // 
            this.repositoryItemCheckEdit11.AutoHeight = false;
            this.repositoryItemCheckEdit11.Name = "repositoryItemCheckEdit11";
            this.repositoryItemCheckEdit11.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // repositoryItemCheckEdit12
            // 
            this.repositoryItemCheckEdit12.AutoHeight = false;
            this.repositoryItemCheckEdit12.Name = "repositoryItemCheckEdit12";
            this.repositoryItemCheckEdit12.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // categoryRow4
            // 
            this.categoryRow4.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMagnifierID,
            this.rowUserID,
            this.rowUserType,
            this.rowLocationX,
            this.rowLocationY,
            this.rowCloseOnMouseUp,
            this.rowDoubleBuffered,
            this.rowHideMouseCursor,
            this.rowRememberLastPoint,
            this.rowReturnToOrigin,
            this.rowTopMostWindow,
            this.rowMagnifierWidth,
            this.rowMagnifierHeight,
            this.rowZoomFactor,
            this.rowSpeedFactor,
            this.rowStaffID,
            this.rowMagnifierShape});
            this.categoryRow4.Name = "categoryRow4";
            this.categoryRow4.Properties.Caption = "Magnifier Settings";
            // 
            // rowMagnifierID
            // 
            this.rowMagnifierID.Name = "rowMagnifierID";
            this.rowMagnifierID.Properties.Caption = "MagnifierID";
            this.rowMagnifierID.Properties.FieldName = "MagnifierID";
            this.rowMagnifierID.Visible = false;
            // 
            // rowUserID
            // 
            this.rowUserID.Name = "rowUserID";
            this.rowUserID.Properties.Caption = "UserID";
            this.rowUserID.Properties.FieldName = "UserID";
            this.rowUserID.Visible = false;
            // 
            // rowUserType
            // 
            this.rowUserType.Name = "rowUserType";
            this.rowUserType.Properties.Caption = "UserType";
            this.rowUserType.Properties.FieldName = "UserType";
            this.rowUserType.Visible = false;
            // 
            // rowLocationX
            // 
            this.rowLocationX.Name = "rowLocationX";
            this.rowLocationX.Properties.Caption = "Location X";
            this.rowLocationX.Properties.FieldName = "LocationX";
            // 
            // rowLocationY
            // 
            this.rowLocationY.Name = "rowLocationY";
            this.rowLocationY.Properties.Caption = "Location Y";
            this.rowLocationY.Properties.FieldName = "LocationY";
            // 
            // rowCloseOnMouseUp
            // 
            this.rowCloseOnMouseUp.Name = "rowCloseOnMouseUp";
            this.rowCloseOnMouseUp.Properties.Caption = "Close On Mouse Up";
            this.rowCloseOnMouseUp.Properties.FieldName = "CloseOnMouseUp";
            this.rowCloseOnMouseUp.Properties.RowEdit = this.repositoryItemCheckEdit7;
            // 
            // rowDoubleBuffered
            // 
            this.rowDoubleBuffered.Height = 17;
            this.rowDoubleBuffered.Name = "rowDoubleBuffered";
            this.rowDoubleBuffered.Properties.Caption = "Double Buffered";
            this.rowDoubleBuffered.Properties.FieldName = "DoubleBuffered";
            this.rowDoubleBuffered.Properties.RowEdit = this.repositoryItemCheckEdit8;
            // 
            // rowHideMouseCursor
            // 
            this.rowHideMouseCursor.Height = 17;
            this.rowHideMouseCursor.Name = "rowHideMouseCursor";
            this.rowHideMouseCursor.Properties.Caption = "Hide Mouse Cursor";
            this.rowHideMouseCursor.Properties.FieldName = "HideMouseCursor";
            this.rowHideMouseCursor.Properties.RowEdit = this.repositoryItemCheckEdit9;
            // 
            // rowRememberLastPoint
            // 
            this.rowRememberLastPoint.Name = "rowRememberLastPoint";
            this.rowRememberLastPoint.Properties.Caption = "Remember Last Point";
            this.rowRememberLastPoint.Properties.FieldName = "RememberLastPoint";
            this.rowRememberLastPoint.Properties.RowEdit = this.repositoryItemCheckEdit10;
            // 
            // rowReturnToOrigin
            // 
            this.rowReturnToOrigin.Name = "rowReturnToOrigin";
            this.rowReturnToOrigin.Properties.Caption = "Return To Origin";
            this.rowReturnToOrigin.Properties.FieldName = "ReturnToOrigin";
            this.rowReturnToOrigin.Properties.RowEdit = this.repositoryItemCheckEdit11;
            // 
            // rowTopMostWindow
            // 
            this.rowTopMostWindow.Name = "rowTopMostWindow";
            this.rowTopMostWindow.Properties.Caption = "Top Most Window";
            this.rowTopMostWindow.Properties.FieldName = "TopMostWindow";
            this.rowTopMostWindow.Properties.RowEdit = this.repositoryItemCheckEdit12;
            // 
            // rowMagnifierWidth
            // 
            this.rowMagnifierWidth.Name = "rowMagnifierWidth";
            this.rowMagnifierWidth.Properties.Caption = "Magnifier Width";
            this.rowMagnifierWidth.Properties.FieldName = "MagnifierWidth";
            // 
            // rowMagnifierHeight
            // 
            this.rowMagnifierHeight.Name = "rowMagnifierHeight";
            this.rowMagnifierHeight.Properties.Caption = "Magnifier Height";
            this.rowMagnifierHeight.Properties.FieldName = "MagnifierHeight";
            // 
            // rowZoomFactor
            // 
            this.rowZoomFactor.Name = "rowZoomFactor";
            this.rowZoomFactor.Properties.Caption = "Zoom Factor";
            this.rowZoomFactor.Properties.FieldName = "ZoomFactor";
            // 
            // rowSpeedFactor
            // 
            this.rowSpeedFactor.Name = "rowSpeedFactor";
            this.rowSpeedFactor.Properties.Caption = "Speed Factor";
            this.rowSpeedFactor.Properties.FieldName = "SpeedFactor";
            // 
            // rowStaffID
            // 
            this.rowStaffID.Name = "rowStaffID";
            this.rowStaffID.Properties.Caption = "StaffID";
            this.rowStaffID.Properties.FieldName = "StaffID";
            this.rowStaffID.Properties.ReadOnly = true;
            this.rowStaffID.Visible = false;
            // 
            // rowMagnifierShape
            // 
            this.rowMagnifierShape.Name = "rowMagnifierShape";
            this.rowMagnifierShape.Properties.Caption = "Magnifier Shape";
            this.rowMagnifierShape.Properties.FieldName = "MagnifierShape";
            this.rowMagnifierShape.Properties.RowEdit = this.rpiMagnifierShapeLookupEdit;
            // 
            // sp00070_Get_Magnifier_ShapeTableAdapter
            // 
            this.sp00070_Get_Magnifier_ShapeTableAdapter.ClearBeforeFill = true;
            // 
            // sp00047AvailableStartUpScreensForUserTableAdapter
            // 
            this.sp00047AvailableStartUpScreensForUserTableAdapter.ClearBeforeFill = true;
            // 
            // frmUserSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 487);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUserSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "My Settings";
            this.Load += new System.EventHandler(this.frmUserSettings_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00064GetStaffApplicationSettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiMessageTimerSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiShowPhotoPanelCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00047AvailableStartUpScreensForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditEstatePlanInspectionControlPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00068_Get_Magnifier_SettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtpSettingsPage1.ResumeLayout(false);
            this.xtpSettingsPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiMagnifierShapeLookupEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00070GetMagnifierShapeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraVerticalGrid.VGridControl vGridControl1;
        private System.Windows.Forms.BindingSource sp00064GetStaffApplicationSettingsBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00064GetStaffApplicationSettingsTableAdapter sp00064GetStaffApplicationSettingsTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowShowToolTips;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowShowTipsOnStartup;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowShowConfirmations;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMinimiseToTaskBar;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowToDoPopupAlert;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit rpiMessageTimerSpinEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rpiShowPhotoPanelCheckEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowShowPhotoPopup;
        private System.Windows.Forms.BindingSource sp00068_Get_Magnifier_SettingsBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00068_Get_Magnifier_SettingsTableAdapter sp00068_Get_Magnifier_SettingsTableAdapter;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtpSettingsPage1;
        private DevExpress.XtraTab.XtraTabPage xtpSettingsPage2;
        private DevExpress.XtraVerticalGrid.VGridControl vGridControl2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMagnifierID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUserID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUserType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLocationX;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLocationY;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCloseOnMouseUp;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDoubleBuffered;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowHideMouseCursor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRememberLastPoint;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowReturnToOrigin;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTopMostWindow;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMagnifierWidth;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMagnifierHeight;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowZoomFactor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSpeedFactor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStaffID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMagnifierShape;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rpiMagnifierShapeLookupEdit;
        private System.Windows.Forms.BindingSource sp00070GetMagnifierShapeBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00070_Get_Magnifier_ShapeTableAdapter sp00070_Get_Magnifier_ShapeTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemFontEdit repositoryItemFontEdit1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDefaultFontSize;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDefaultFontName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit12;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFirstScreenLoadedID;
        public DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private System.Windows.Forms.BindingSource sp00047AvailableStartUpScreensForUserBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleName;
        private DevExpress.XtraGrid.Columns.GridColumn colPartDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPartID;
        private DevExpress.XtraGrid.Columns.GridColumn colPartOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00047AvailableStartUpScreensForUserTableAdapter sp00047AvailableStartUpScreensForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditEstatePlanInspectionControlPanel;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow5;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowEstatePlanInspectionControlPanel;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}
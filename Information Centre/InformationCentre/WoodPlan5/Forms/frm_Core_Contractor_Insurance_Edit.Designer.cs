namespace WoodPlan5
{
    partial class frm_Core_Contractor_Insurance_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Contractor_Insurance_Edit));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.WindowCleaningEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sp00247CoreTeamInsuranceEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.WindowCleaningPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RoofingEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RoofingPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.FencingEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.FencingPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PestControlEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PestControlPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RailArbEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RailArbPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.UtilityArbEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.UtilityArbPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AmenityArbEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AmenityArbPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ConstructionEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ConstructionPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MaintenanceEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.MaintenancePublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SnowClearanceEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SnowClearancePublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GrittingEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LinkedCertificateButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.TeamIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.StartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.GrittingPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SubContractorNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.InsuranceIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.potholeTeamPublicValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PotholeTeamEmployersValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForTeamID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInsuranceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLinkedCertificate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForGrittingPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearancePublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSnowClearanceEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMaintenancePublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMaintenanceEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForConstructionPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForConstructionEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAmenityArbPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAmenityArbEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUtilityArbPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUtilityArbEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRailArbPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRailArbEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPestControlPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPestControlEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFencingPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFencingEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRoofingPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRoofingEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWindowCleaningPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWindowCleaningEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPotholeTeamPublicValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPotholeTeamEmployersValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00247_Core_Team_Insurance_EditTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00247_Core_Team_Insurance_EditTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WindowCleaningEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00247CoreTeamInsuranceEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindowCleaningPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoofingEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoofingPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FencingEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FencingPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PestControlEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PestControlPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RailArbEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RailArbPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmenityArbEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmenityArbPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConstructionEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConstructionPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaintenanceEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaintenancePublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearancePublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedCertificateButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsuranceIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.potholeTeamPublicValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PotholeTeamEmployersValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInsuranceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedCertificate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearancePublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaintenancePublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaintenanceEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConstructionPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConstructionEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmenityArbPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmenityArbEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArbPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArbEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRailArbPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRailArbEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPestControlPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPestControlEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFencingPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFencingEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoofingPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoofingEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWindowCleaningPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWindowCleaningEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPotholeTeamPublicValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPotholeTeamEmployersValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 645);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 619);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 619);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 17;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 645);
            this.barDockControl2.Size = new System.Drawing.Size(628, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 619);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 619);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.WindowCleaningEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.WindowCleaningPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RoofingEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RoofingPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.FencingEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.FencingPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PestControlEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PestControlPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RailArbEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RailArbPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.UtilityArbEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.UtilityArbPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AmenityArbEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AmenityArbPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ConstructionEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ConstructionPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MaintenanceEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.MaintenancePublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearanceEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClearancePublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingEmployersValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedCertificateButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.StartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.InsuranceIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.potholeTeamPublicValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PotholeTeamEmployersValueSpinEdit);
            this.dataLayoutControl1.DataSource = this.sp00247CoreTeamInsuranceEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTeamID,
            this.ItemForInsuranceID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1292, 243, 433, 559);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 619);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // WindowCleaningEmployersValueSpinEdit
            // 
            this.WindowCleaningEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "WindowCleaningEmployersValue", true));
            this.WindowCleaningEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WindowCleaningEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 497);
            this.WindowCleaningEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.WindowCleaningEmployersValueSpinEdit.Name = "WindowCleaningEmployersValueSpinEdit";
            this.WindowCleaningEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WindowCleaningEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.WindowCleaningEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WindowCleaningEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.WindowCleaningEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.WindowCleaningEmployersValueSpinEdit.TabIndex = 21;
            // 
            // sp00247CoreTeamInsuranceEditBindingSource
            // 
            this.sp00247CoreTeamInsuranceEditBindingSource.DataMember = "sp00247_Core_Team_Insurance_Edit";
            this.sp00247CoreTeamInsuranceEditBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // WindowCleaningPublicValueSpinEdit
            // 
            this.WindowCleaningPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "WindowCleaningPublicValue", true));
            this.WindowCleaningPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WindowCleaningPublicValueSpinEdit.Location = new System.Drawing.Point(189, 497);
            this.WindowCleaningPublicValueSpinEdit.MenuManager = this.barManager1;
            this.WindowCleaningPublicValueSpinEdit.Name = "WindowCleaningPublicValueSpinEdit";
            this.WindowCleaningPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WindowCleaningPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.WindowCleaningPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WindowCleaningPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.WindowCleaningPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.WindowCleaningPublicValueSpinEdit.TabIndex = 21;
            // 
            // RoofingEmployersValueSpinEdit
            // 
            this.RoofingEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "RoofingEmployersValue", true));
            this.RoofingEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RoofingEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 473);
            this.RoofingEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.RoofingEmployersValueSpinEdit.Name = "RoofingEmployersValueSpinEdit";
            this.RoofingEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RoofingEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.RoofingEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RoofingEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.RoofingEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.RoofingEmployersValueSpinEdit.TabIndex = 21;
            // 
            // RoofingPublicValueSpinEdit
            // 
            this.RoofingPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "RoofingPublicValue", true));
            this.RoofingPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RoofingPublicValueSpinEdit.Location = new System.Drawing.Point(189, 473);
            this.RoofingPublicValueSpinEdit.MenuManager = this.barManager1;
            this.RoofingPublicValueSpinEdit.Name = "RoofingPublicValueSpinEdit";
            this.RoofingPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RoofingPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.RoofingPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RoofingPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.RoofingPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.RoofingPublicValueSpinEdit.TabIndex = 21;
            // 
            // FencingEmployersValueSpinEdit
            // 
            this.FencingEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "FencingEmployersValue", true));
            this.FencingEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.FencingEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 449);
            this.FencingEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.FencingEmployersValueSpinEdit.Name = "FencingEmployersValueSpinEdit";
            this.FencingEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FencingEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.FencingEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FencingEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.FencingEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.FencingEmployersValueSpinEdit.TabIndex = 21;
            // 
            // FencingPublicValueSpinEdit
            // 
            this.FencingPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "FencingPublicValue", true));
            this.FencingPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.FencingPublicValueSpinEdit.Location = new System.Drawing.Point(189, 449);
            this.FencingPublicValueSpinEdit.MenuManager = this.barManager1;
            this.FencingPublicValueSpinEdit.Name = "FencingPublicValueSpinEdit";
            this.FencingPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FencingPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.FencingPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FencingPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.FencingPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.FencingPublicValueSpinEdit.TabIndex = 21;
            // 
            // PestControlEmployersValueSpinEdit
            // 
            this.PestControlEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "PestControlEmployersValue", true));
            this.PestControlEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PestControlEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 425);
            this.PestControlEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.PestControlEmployersValueSpinEdit.Name = "PestControlEmployersValueSpinEdit";
            this.PestControlEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PestControlEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.PestControlEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PestControlEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.PestControlEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.PestControlEmployersValueSpinEdit.TabIndex = 21;
            // 
            // PestControlPublicValueSpinEdit
            // 
            this.PestControlPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "PestControlPublicValue", true));
            this.PestControlPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PestControlPublicValueSpinEdit.Location = new System.Drawing.Point(189, 425);
            this.PestControlPublicValueSpinEdit.MenuManager = this.barManager1;
            this.PestControlPublicValueSpinEdit.Name = "PestControlPublicValueSpinEdit";
            this.PestControlPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PestControlPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.PestControlPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PestControlPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.PestControlPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.PestControlPublicValueSpinEdit.TabIndex = 21;
            // 
            // RailArbEmployersValueSpinEdit
            // 
            this.RailArbEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "RailArbEmployersValue", true));
            this.RailArbEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RailArbEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 401);
            this.RailArbEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.RailArbEmployersValueSpinEdit.Name = "RailArbEmployersValueSpinEdit";
            this.RailArbEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RailArbEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.RailArbEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RailArbEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.RailArbEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.RailArbEmployersValueSpinEdit.TabIndex = 21;
            // 
            // RailArbPublicValueSpinEdit
            // 
            this.RailArbPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "RailArbPublicValue", true));
            this.RailArbPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RailArbPublicValueSpinEdit.Location = new System.Drawing.Point(189, 401);
            this.RailArbPublicValueSpinEdit.MenuManager = this.barManager1;
            this.RailArbPublicValueSpinEdit.Name = "RailArbPublicValueSpinEdit";
            this.RailArbPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RailArbPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.RailArbPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RailArbPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.RailArbPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.RailArbPublicValueSpinEdit.TabIndex = 21;
            // 
            // UtilityArbEmployersValueSpinEdit
            // 
            this.UtilityArbEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "UtilityArbEmployersValue", true));
            this.UtilityArbEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UtilityArbEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 377);
            this.UtilityArbEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.UtilityArbEmployersValueSpinEdit.Name = "UtilityArbEmployersValueSpinEdit";
            this.UtilityArbEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UtilityArbEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.UtilityArbEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UtilityArbEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.UtilityArbEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.UtilityArbEmployersValueSpinEdit.TabIndex = 21;
            // 
            // UtilityArbPublicValueSpinEdit
            // 
            this.UtilityArbPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "UtilityArbPublicValue", true));
            this.UtilityArbPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UtilityArbPublicValueSpinEdit.Location = new System.Drawing.Point(189, 377);
            this.UtilityArbPublicValueSpinEdit.MenuManager = this.barManager1;
            this.UtilityArbPublicValueSpinEdit.Name = "UtilityArbPublicValueSpinEdit";
            this.UtilityArbPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UtilityArbPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.UtilityArbPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UtilityArbPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.UtilityArbPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.UtilityArbPublicValueSpinEdit.TabIndex = 21;
            // 
            // AmenityArbEmployersValueSpinEdit
            // 
            this.AmenityArbEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "AmenityArbEmployersValue", true));
            this.AmenityArbEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AmenityArbEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 353);
            this.AmenityArbEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.AmenityArbEmployersValueSpinEdit.Name = "AmenityArbEmployersValueSpinEdit";
            this.AmenityArbEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AmenityArbEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.AmenityArbEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AmenityArbEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.AmenityArbEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.AmenityArbEmployersValueSpinEdit.TabIndex = 21;
            // 
            // AmenityArbPublicValueSpinEdit
            // 
            this.AmenityArbPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "AmenityArbPublicValue", true));
            this.AmenityArbPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AmenityArbPublicValueSpinEdit.Location = new System.Drawing.Point(189, 353);
            this.AmenityArbPublicValueSpinEdit.MenuManager = this.barManager1;
            this.AmenityArbPublicValueSpinEdit.Name = "AmenityArbPublicValueSpinEdit";
            this.AmenityArbPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AmenityArbPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.AmenityArbPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AmenityArbPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.AmenityArbPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.AmenityArbPublicValueSpinEdit.TabIndex = 21;
            // 
            // ConstructionEmployersValueSpinEdit
            // 
            this.ConstructionEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "ConstructionEmployersValue", true));
            this.ConstructionEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ConstructionEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 329);
            this.ConstructionEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.ConstructionEmployersValueSpinEdit.Name = "ConstructionEmployersValueSpinEdit";
            this.ConstructionEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ConstructionEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.ConstructionEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ConstructionEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.ConstructionEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.ConstructionEmployersValueSpinEdit.TabIndex = 21;
            // 
            // ConstructionPublicValueSpinEdit
            // 
            this.ConstructionPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "ConstructionPublicValue", true));
            this.ConstructionPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ConstructionPublicValueSpinEdit.Location = new System.Drawing.Point(189, 329);
            this.ConstructionPublicValueSpinEdit.MenuManager = this.barManager1;
            this.ConstructionPublicValueSpinEdit.Name = "ConstructionPublicValueSpinEdit";
            this.ConstructionPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ConstructionPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.ConstructionPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ConstructionPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.ConstructionPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.ConstructionPublicValueSpinEdit.TabIndex = 21;
            // 
            // MaintenanceEmployersValueSpinEdit
            // 
            this.MaintenanceEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "MaintenanceEmployersValue", true));
            this.MaintenanceEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MaintenanceEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 305);
            this.MaintenanceEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.MaintenanceEmployersValueSpinEdit.Name = "MaintenanceEmployersValueSpinEdit";
            this.MaintenanceEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MaintenanceEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.MaintenanceEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MaintenanceEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.MaintenanceEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.MaintenanceEmployersValueSpinEdit.TabIndex = 21;
            // 
            // MaintenancePublicValueSpinEdit
            // 
            this.MaintenancePublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "MaintenancePublicValue", true));
            this.MaintenancePublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MaintenancePublicValueSpinEdit.Location = new System.Drawing.Point(189, 305);
            this.MaintenancePublicValueSpinEdit.MenuManager = this.barManager1;
            this.MaintenancePublicValueSpinEdit.Name = "MaintenancePublicValueSpinEdit";
            this.MaintenancePublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MaintenancePublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.MaintenancePublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MaintenancePublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.MaintenancePublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.MaintenancePublicValueSpinEdit.TabIndex = 21;
            // 
            // SnowClearanceEmployersValueSpinEdit
            // 
            this.SnowClearanceEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "SnowClearanceEmployersValue", true));
            this.SnowClearanceEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SnowClearanceEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 281);
            this.SnowClearanceEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.SnowClearanceEmployersValueSpinEdit.Name = "SnowClearanceEmployersValueSpinEdit";
            this.SnowClearanceEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SnowClearanceEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.SnowClearanceEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SnowClearanceEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.SnowClearanceEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearanceEmployersValueSpinEdit.TabIndex = 21;
            // 
            // SnowClearancePublicValueSpinEdit
            // 
            this.SnowClearancePublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "SnowClearancePublicValue", true));
            this.SnowClearancePublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SnowClearancePublicValueSpinEdit.Location = new System.Drawing.Point(189, 281);
            this.SnowClearancePublicValueSpinEdit.MenuManager = this.barManager1;
            this.SnowClearancePublicValueSpinEdit.Name = "SnowClearancePublicValueSpinEdit";
            this.SnowClearancePublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SnowClearancePublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.SnowClearancePublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SnowClearancePublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.SnowClearancePublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.SnowClearancePublicValueSpinEdit.TabIndex = 21;
            // 
            // GrittingEmployersValueSpinEdit
            // 
            this.GrittingEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "GrittingEmployersValue", true));
            this.GrittingEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.GrittingEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 257);
            this.GrittingEmployersValueSpinEdit.MenuManager = this.barManager1;
            this.GrittingEmployersValueSpinEdit.Name = "GrittingEmployersValueSpinEdit";
            this.GrittingEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GrittingEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.GrittingEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.GrittingEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.GrittingEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.GrittingEmployersValueSpinEdit.TabIndex = 21;
            // 
            // LinkedCertificateButtonEdit
            // 
            this.LinkedCertificateButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "LinkedCertificate", true));
            this.LinkedCertificateButtonEdit.Location = new System.Drawing.Point(177, 189);
            this.LinkedCertificateButtonEdit.MenuManager = this.barManager1;
            this.LinkedCertificateButtonEdit.Name = "LinkedCertificateButtonEdit";
            this.LinkedCertificateButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select File screen.", "select", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to View the linked file.", "view", null, true)});
            this.LinkedCertificateButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedCertificateButtonEdit.Size = new System.Drawing.Size(415, 20);
            this.LinkedCertificateButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedCertificateButtonEdit.TabIndex = 34;
            this.LinkedCertificateButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedCertificateButtonEdit_ButtonClick);
            // 
            // TeamIDTextEdit
            // 
            this.TeamIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "TeamID", true));
            this.TeamIDTextEdit.Location = new System.Drawing.Point(137, 59);
            this.TeamIDTextEdit.MenuManager = this.barManager1;
            this.TeamIDTextEdit.Name = "TeamIDTextEdit";
            this.TeamIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TeamIDTextEdit, true);
            this.TeamIDTextEdit.Size = new System.Drawing.Size(462, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TeamIDTextEdit, optionsSpelling1);
            this.TeamIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TeamIDTextEdit.TabIndex = 33;
            // 
            // EndDateDateEdit
            // 
            this.EndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "EndDate", true));
            this.EndDateDateEdit.EditValue = null;
            this.EndDateDateEdit.Location = new System.Drawing.Point(153, 83);
            this.EndDateDateEdit.MenuManager = this.barManager1;
            this.EndDateDateEdit.Name = "EndDateDateEdit";
            this.EndDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EndDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.EndDateDateEdit.Size = new System.Drawing.Size(126, 20);
            this.EndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EndDateDateEdit.TabIndex = 22;
            this.EndDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EndDateDateEdit_Validating);
            // 
            // StartDateDateEdit
            // 
            this.StartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "StartDate", true));
            this.StartDateDateEdit.EditValue = null;
            this.StartDateDateEdit.Location = new System.Drawing.Point(153, 59);
            this.StartDateDateEdit.MenuManager = this.barManager1;
            this.StartDateDateEdit.Name = "StartDateDateEdit";
            this.StartDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.StartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.StartDateDateEdit.Size = new System.Drawing.Size(126, 20);
            this.StartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.StartDateDateEdit.TabIndex = 21;
            this.StartDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.StartDateDateEdit_Validating);
            // 
            // GrittingPublicValueSpinEdit
            // 
            this.GrittingPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "GrittingPublicValue", true));
            this.GrittingPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.GrittingPublicValueSpinEdit.Location = new System.Drawing.Point(189, 257);
            this.GrittingPublicValueSpinEdit.MenuManager = this.barManager1;
            this.GrittingPublicValueSpinEdit.Name = "GrittingPublicValueSpinEdit";
            this.GrittingPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GrittingPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.GrittingPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.GrittingPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.GrittingPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.GrittingPublicValueSpinEdit.TabIndex = 20;
            // 
            // SubContractorNameButtonEdit
            // 
            this.SubContractorNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "SubContractorName", true));
            this.SubContractorNameButtonEdit.Location = new System.Drawing.Point(153, 35);
            this.SubContractorNameButtonEdit.MenuManager = this.barManager1;
            this.SubContractorNameButtonEdit.Name = "SubContractorNameButtonEdit";
            this.SubContractorNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Select", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Select From List", null, null, true)});
            this.SubContractorNameButtonEdit.Properties.MaxLength = 2147483647;
            this.SubContractorNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SubContractorNameButtonEdit.Size = new System.Drawing.Size(463, 20);
            this.SubContractorNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorNameButtonEdit.TabIndex = 14;
            this.SubContractorNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SubContractorNameButtonEdit_ButtonClick);
            this.SubContractorNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SubContractorNameButtonEdit_Validating);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00247CoreTeamInsuranceEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(153, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // InsuranceIDTextEdit
            // 
            this.InsuranceIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "InsuranceID", true));
            this.InsuranceIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.InsuranceIDTextEdit.Location = new System.Drawing.Point(137, 35);
            this.InsuranceIDTextEdit.MenuManager = this.barManager1;
            this.InsuranceIDTextEdit.Name = "InsuranceIDTextEdit";
            this.InsuranceIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.InsuranceIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.InsuranceIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.InsuranceIDTextEdit, true);
            this.InsuranceIDTextEdit.Size = new System.Drawing.Size(462, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.InsuranceIDTextEdit, optionsSpelling2);
            this.InsuranceIDTextEdit.StyleController = this.dataLayoutControl1;
            this.InsuranceIDTextEdit.TabIndex = 4;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 189);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(556, 364);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling3);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // potholeTeamPublicValueSpinEdit
            // 
            this.potholeTeamPublicValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "PotholePublicValue", true));
            this.potholeTeamPublicValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.potholeTeamPublicValueSpinEdit.Location = new System.Drawing.Point(189, 521);
            this.potholeTeamPublicValueSpinEdit.Name = "potholeTeamPublicValueSpinEdit";
            this.potholeTeamPublicValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.potholeTeamPublicValueSpinEdit.Properties.Mask.EditMask = "c";
            this.potholeTeamPublicValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.potholeTeamPublicValueSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.potholeTeamPublicValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.potholeTeamPublicValueSpinEdit.TabIndex = 21;
            // 
            // PotholeTeamEmployersValueSpinEdit
            // 
            this.PotholeTeamEmployersValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00247CoreTeamInsuranceEditBindingSource, "PotholeEmployersValue", true));
            this.PotholeTeamEmployersValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PotholeTeamEmployersValueSpinEdit.Location = new System.Drawing.Point(455, 521);
            this.PotholeTeamEmployersValueSpinEdit.Name = "PotholeTeamEmployersValueSpinEdit";
            this.PotholeTeamEmployersValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PotholeTeamEmployersValueSpinEdit.Properties.Mask.EditMask = "c";
            this.PotholeTeamEmployersValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PotholeTeamEmployersValueSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.PotholeTeamEmployersValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.PotholeTeamEmployersValueSpinEdit.TabIndex = 21;
            // 
            // ItemForTeamID
            // 
            this.ItemForTeamID.Control = this.TeamIDTextEdit;
            this.ItemForTeamID.CustomizationFormText = "Team ID:";
            this.ItemForTeamID.Location = new System.Drawing.Point(0, 47);
            this.ItemForTeamID.Name = "ItemForTeamID";
            this.ItemForTeamID.Size = new System.Drawing.Size(591, 24);
            this.ItemForTeamID.Text = "Team ID:";
            this.ItemForTeamID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForInsuranceID
            // 
            this.ItemForInsuranceID.Control = this.InsuranceIDTextEdit;
            this.ItemForInsuranceID.CustomizationFormText = "Insurance ID:";
            this.ItemForInsuranceID.Location = new System.Drawing.Point(0, 23);
            this.ItemForInsuranceID.Name = "ItemForInsuranceID";
            this.ItemForInsuranceID.Size = new System.Drawing.Size(591, 24);
            this.ItemForInsuranceID.Text = "Insurance ID:";
            this.ItemForInsuranceID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 619);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.emptySpaceItem2,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.ItemForSubContractorName,
            this.ItemForStartDate,
            this.ItemForEndDate,
            this.emptySpaceItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 599);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 107);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(608, 462);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 416);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem7,
            this.ItemForLinkedCertificate,
            this.layoutControlGroup6});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(560, 368);
            this.layoutControlGroup5.Text = "Details";
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(560, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLinkedCertificate
            // 
            this.ItemForLinkedCertificate.Control = this.LinkedCertificateButtonEdit;
            this.ItemForLinkedCertificate.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedCertificate.Name = "ItemForLinkedCertificate";
            this.ItemForLinkedCertificate.Size = new System.Drawing.Size(560, 24);
            this.ItemForLinkedCertificate.Text = "Linked Certificate:";
            this.ItemForLinkedCertificate.TextSize = new System.Drawing.Size(138, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGrittingPublicValue,
            this.ItemForGrittingEmployersValue,
            this.ItemForSnowClearancePublicValue,
            this.ItemForSnowClearanceEmployersValue,
            this.ItemForMaintenancePublicValue,
            this.ItemForMaintenanceEmployersValue,
            this.ItemForConstructionPublicValue,
            this.ItemForConstructionEmployersValue,
            this.ItemForAmenityArbPublicValue,
            this.ItemForAmenityArbEmployersValue,
            this.ItemForUtilityArbPublicValue,
            this.ItemForUtilityArbEmployersValue,
            this.ItemForRailArbPublicValue,
            this.ItemForRailArbEmployersValue,
            this.ItemForPestControlPublicValue,
            this.ItemForPestControlEmployersValue,
            this.ItemForFencingPublicValue,
            this.ItemForFencingEmployersValue,
            this.ItemForRoofingPublicValue,
            this.ItemForRoofingEmployersValue,
            this.emptySpaceItem1,
            this.ItemForWindowCleaningPublicValue,
            this.ItemForWindowCleaningEmployersValue,
            this.ItemForPotholeTeamPublicValue,
            this.ItemForPotholeTeamEmployersValue});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 34);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(560, 334);
            this.layoutControlGroup6.Text = "Insurance Values:";
            // 
            // ItemForGrittingPublicValue
            // 
            this.ItemForGrittingPublicValue.Control = this.GrittingPublicValueSpinEdit;
            this.ItemForGrittingPublicValue.CustomizationFormText = "Gritting Public Value:";
            this.ItemForGrittingPublicValue.Location = new System.Drawing.Point(0, 0);
            this.ItemForGrittingPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForGrittingPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForGrittingPublicValue.Name = "ItemForGrittingPublicValue";
            this.ItemForGrittingPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForGrittingPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGrittingPublicValue.Text = "Gritting Public:";
            this.ItemForGrittingPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForGrittingEmployersValue
            // 
            this.ItemForGrittingEmployersValue.Control = this.GrittingEmployersValueSpinEdit;
            this.ItemForGrittingEmployersValue.CustomizationFormText = "Gritting Employers Value:";
            this.ItemForGrittingEmployersValue.Location = new System.Drawing.Point(266, 0);
            this.ItemForGrittingEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForGrittingEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForGrittingEmployersValue.Name = "ItemForGrittingEmployersValue";
            this.ItemForGrittingEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForGrittingEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGrittingEmployersValue.Text = "Gritting Employers:";
            this.ItemForGrittingEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForSnowClearancePublicValue
            // 
            this.ItemForSnowClearancePublicValue.Control = this.SnowClearancePublicValueSpinEdit;
            this.ItemForSnowClearancePublicValue.CustomizationFormText = "Snow Clearance Public Value:";
            this.ItemForSnowClearancePublicValue.Location = new System.Drawing.Point(0, 24);
            this.ItemForSnowClearancePublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForSnowClearancePublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForSnowClearancePublicValue.Name = "ItemForSnowClearancePublicValue";
            this.ItemForSnowClearancePublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForSnowClearancePublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSnowClearancePublicValue.Text = "Snow Clearance Public:";
            this.ItemForSnowClearancePublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForSnowClearanceEmployersValue
            // 
            this.ItemForSnowClearanceEmployersValue.Control = this.SnowClearanceEmployersValueSpinEdit;
            this.ItemForSnowClearanceEmployersValue.CustomizationFormText = "Snow Clearance Employers Value:";
            this.ItemForSnowClearanceEmployersValue.Location = new System.Drawing.Point(266, 24);
            this.ItemForSnowClearanceEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForSnowClearanceEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForSnowClearanceEmployersValue.Name = "ItemForSnowClearanceEmployersValue";
            this.ItemForSnowClearanceEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForSnowClearanceEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSnowClearanceEmployersValue.Text = "Snow Clearance Employers:";
            this.ItemForSnowClearanceEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForMaintenancePublicValue
            // 
            this.ItemForMaintenancePublicValue.Control = this.MaintenancePublicValueSpinEdit;
            this.ItemForMaintenancePublicValue.CustomizationFormText = "Maintenance Public Value:";
            this.ItemForMaintenancePublicValue.Location = new System.Drawing.Point(0, 48);
            this.ItemForMaintenancePublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForMaintenancePublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForMaintenancePublicValue.Name = "ItemForMaintenancePublicValue";
            this.ItemForMaintenancePublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForMaintenancePublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForMaintenancePublicValue.Text = "Maintenance Public:";
            this.ItemForMaintenancePublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForMaintenanceEmployersValue
            // 
            this.ItemForMaintenanceEmployersValue.Control = this.MaintenanceEmployersValueSpinEdit;
            this.ItemForMaintenanceEmployersValue.CustomizationFormText = "Maintenance Employers Value:";
            this.ItemForMaintenanceEmployersValue.Location = new System.Drawing.Point(266, 48);
            this.ItemForMaintenanceEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForMaintenanceEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForMaintenanceEmployersValue.Name = "ItemForMaintenanceEmployersValue";
            this.ItemForMaintenanceEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForMaintenanceEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForMaintenanceEmployersValue.Text = "Maintenance Employers:";
            this.ItemForMaintenanceEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForConstructionPublicValue
            // 
            this.ItemForConstructionPublicValue.Control = this.ConstructionPublicValueSpinEdit;
            this.ItemForConstructionPublicValue.CustomizationFormText = "Construction Public Value:";
            this.ItemForConstructionPublicValue.Location = new System.Drawing.Point(0, 72);
            this.ItemForConstructionPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForConstructionPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForConstructionPublicValue.Name = "ItemForConstructionPublicValue";
            this.ItemForConstructionPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForConstructionPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForConstructionPublicValue.Text = "Construction Public:";
            this.ItemForConstructionPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForConstructionEmployersValue
            // 
            this.ItemForConstructionEmployersValue.Control = this.ConstructionEmployersValueSpinEdit;
            this.ItemForConstructionEmployersValue.CustomizationFormText = "Construction Employers Value:";
            this.ItemForConstructionEmployersValue.Location = new System.Drawing.Point(266, 72);
            this.ItemForConstructionEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForConstructionEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForConstructionEmployersValue.Name = "ItemForConstructionEmployersValue";
            this.ItemForConstructionEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForConstructionEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForConstructionEmployersValue.Text = "Construction Employers:";
            this.ItemForConstructionEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForAmenityArbPublicValue
            // 
            this.ItemForAmenityArbPublicValue.Control = this.AmenityArbPublicValueSpinEdit;
            this.ItemForAmenityArbPublicValue.CustomizationFormText = "Amenity Arb Public Value:";
            this.ItemForAmenityArbPublicValue.Location = new System.Drawing.Point(0, 96);
            this.ItemForAmenityArbPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForAmenityArbPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForAmenityArbPublicValue.Name = "ItemForAmenityArbPublicValue";
            this.ItemForAmenityArbPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForAmenityArbPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAmenityArbPublicValue.Text = "Amenity Arb Public:";
            this.ItemForAmenityArbPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForAmenityArbEmployersValue
            // 
            this.ItemForAmenityArbEmployersValue.Control = this.AmenityArbEmployersValueSpinEdit;
            this.ItemForAmenityArbEmployersValue.CustomizationFormText = "Amenity Arb Employers Value:";
            this.ItemForAmenityArbEmployersValue.Location = new System.Drawing.Point(266, 96);
            this.ItemForAmenityArbEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForAmenityArbEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForAmenityArbEmployersValue.Name = "ItemForAmenityArbEmployersValue";
            this.ItemForAmenityArbEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForAmenityArbEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAmenityArbEmployersValue.Text = "Amenity Arb Employers:";
            this.ItemForAmenityArbEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForUtilityArbPublicValue
            // 
            this.ItemForUtilityArbPublicValue.Control = this.UtilityArbPublicValueSpinEdit;
            this.ItemForUtilityArbPublicValue.CustomizationFormText = "Utility Arb Public Value:";
            this.ItemForUtilityArbPublicValue.Location = new System.Drawing.Point(0, 120);
            this.ItemForUtilityArbPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForUtilityArbPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForUtilityArbPublicValue.Name = "ItemForUtilityArbPublicValue";
            this.ItemForUtilityArbPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForUtilityArbPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForUtilityArbPublicValue.Text = "Utility Arb Public:";
            this.ItemForUtilityArbPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForUtilityArbEmployersValue
            // 
            this.ItemForUtilityArbEmployersValue.Control = this.UtilityArbEmployersValueSpinEdit;
            this.ItemForUtilityArbEmployersValue.CustomizationFormText = "Utility Arb Employers Value:";
            this.ItemForUtilityArbEmployersValue.Location = new System.Drawing.Point(266, 120);
            this.ItemForUtilityArbEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForUtilityArbEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForUtilityArbEmployersValue.Name = "ItemForUtilityArbEmployersValue";
            this.ItemForUtilityArbEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForUtilityArbEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForUtilityArbEmployersValue.Text = "Utility Arb Employers:";
            this.ItemForUtilityArbEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForRailArbPublicValue
            // 
            this.ItemForRailArbPublicValue.Control = this.RailArbPublicValueSpinEdit;
            this.ItemForRailArbPublicValue.CustomizationFormText = "Rail Arb Public Value:";
            this.ItemForRailArbPublicValue.Location = new System.Drawing.Point(0, 144);
            this.ItemForRailArbPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForRailArbPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForRailArbPublicValue.Name = "ItemForRailArbPublicValue";
            this.ItemForRailArbPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForRailArbPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRailArbPublicValue.Text = "Rail Arb Public:";
            this.ItemForRailArbPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForRailArbEmployersValue
            // 
            this.ItemForRailArbEmployersValue.Control = this.RailArbEmployersValueSpinEdit;
            this.ItemForRailArbEmployersValue.CustomizationFormText = "Rail Arb Employers Value:";
            this.ItemForRailArbEmployersValue.Location = new System.Drawing.Point(266, 144);
            this.ItemForRailArbEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForRailArbEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForRailArbEmployersValue.Name = "ItemForRailArbEmployersValue";
            this.ItemForRailArbEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForRailArbEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRailArbEmployersValue.Text = "Rail Arb Employers:";
            this.ItemForRailArbEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForPestControlPublicValue
            // 
            this.ItemForPestControlPublicValue.Control = this.PestControlPublicValueSpinEdit;
            this.ItemForPestControlPublicValue.CustomizationFormText = "Pest Control Public Value:";
            this.ItemForPestControlPublicValue.Location = new System.Drawing.Point(0, 168);
            this.ItemForPestControlPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForPestControlPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForPestControlPublicValue.Name = "ItemForPestControlPublicValue";
            this.ItemForPestControlPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForPestControlPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPestControlPublicValue.Text = "Pest Control Public:";
            this.ItemForPestControlPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForPestControlEmployersValue
            // 
            this.ItemForPestControlEmployersValue.Control = this.PestControlEmployersValueSpinEdit;
            this.ItemForPestControlEmployersValue.CustomizationFormText = "Pest Control Employers Value:";
            this.ItemForPestControlEmployersValue.Location = new System.Drawing.Point(266, 168);
            this.ItemForPestControlEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForPestControlEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForPestControlEmployersValue.Name = "ItemForPestControlEmployersValue";
            this.ItemForPestControlEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForPestControlEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPestControlEmployersValue.Text = "Pest Control Employers:";
            this.ItemForPestControlEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForFencingPublicValue
            // 
            this.ItemForFencingPublicValue.Control = this.FencingPublicValueSpinEdit;
            this.ItemForFencingPublicValue.CustomizationFormText = "Fencing Public Value:";
            this.ItemForFencingPublicValue.Location = new System.Drawing.Point(0, 192);
            this.ItemForFencingPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForFencingPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForFencingPublicValue.Name = "ItemForFencingPublicValue";
            this.ItemForFencingPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForFencingPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForFencingPublicValue.Text = "Fencing Public:";
            this.ItemForFencingPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForFencingEmployersValue
            // 
            this.ItemForFencingEmployersValue.Control = this.FencingEmployersValueSpinEdit;
            this.ItemForFencingEmployersValue.CustomizationFormText = "Fencing Employers Value:";
            this.ItemForFencingEmployersValue.Location = new System.Drawing.Point(266, 192);
            this.ItemForFencingEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForFencingEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForFencingEmployersValue.Name = "ItemForFencingEmployersValue";
            this.ItemForFencingEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForFencingEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForFencingEmployersValue.Text = "Fencing Employers:";
            this.ItemForFencingEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForRoofingPublicValue
            // 
            this.ItemForRoofingPublicValue.Control = this.RoofingPublicValueSpinEdit;
            this.ItemForRoofingPublicValue.CustomizationFormText = "Roofing Public Value:";
            this.ItemForRoofingPublicValue.Location = new System.Drawing.Point(0, 216);
            this.ItemForRoofingPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForRoofingPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForRoofingPublicValue.Name = "ItemForRoofingPublicValue";
            this.ItemForRoofingPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForRoofingPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRoofingPublicValue.Text = "Roofing Public:";
            this.ItemForRoofingPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForRoofingEmployersValue
            // 
            this.ItemForRoofingEmployersValue.Control = this.RoofingEmployersValueSpinEdit;
            this.ItemForRoofingEmployersValue.CustomizationFormText = "Roofing Employers Value:";
            this.ItemForRoofingEmployersValue.Location = new System.Drawing.Point(266, 216);
            this.ItemForRoofingEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForRoofingEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForRoofingEmployersValue.Name = "ItemForRoofingEmployersValue";
            this.ItemForRoofingEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForRoofingEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRoofingEmployersValue.Text = "Roofing Employers:";
            this.ItemForRoofingEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(526, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 288);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWindowCleaningPublicValue
            // 
            this.ItemForWindowCleaningPublicValue.Control = this.WindowCleaningPublicValueSpinEdit;
            this.ItemForWindowCleaningPublicValue.CustomizationFormText = "Window Cleaning Public Value:";
            this.ItemForWindowCleaningPublicValue.Location = new System.Drawing.Point(0, 240);
            this.ItemForWindowCleaningPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForWindowCleaningPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForWindowCleaningPublicValue.Name = "ItemForWindowCleaningPublicValue";
            this.ItemForWindowCleaningPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForWindowCleaningPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWindowCleaningPublicValue.Text = "Window Cleaning Public:";
            this.ItemForWindowCleaningPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForWindowCleaningEmployersValue
            // 
            this.ItemForWindowCleaningEmployersValue.Control = this.WindowCleaningEmployersValueSpinEdit;
            this.ItemForWindowCleaningEmployersValue.CustomizationFormText = "Window Cleaning Employers Value:";
            this.ItemForWindowCleaningEmployersValue.Location = new System.Drawing.Point(266, 240);
            this.ItemForWindowCleaningEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForWindowCleaningEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForWindowCleaningEmployersValue.Name = "ItemForWindowCleaningEmployersValue";
            this.ItemForWindowCleaningEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForWindowCleaningEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWindowCleaningEmployersValue.Text = "Window Cleaning Employers:";
            this.ItemForWindowCleaningEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForPotholeTeamPublicValue
            // 
            this.ItemForPotholeTeamPublicValue.Control = this.potholeTeamPublicValueSpinEdit;
            this.ItemForPotholeTeamPublicValue.CustomizationFormText = "Pothole Team Public Value:";
            this.ItemForPotholeTeamPublicValue.Location = new System.Drawing.Point(0, 264);
            this.ItemForPotholeTeamPublicValue.MaxSize = new System.Drawing.Size(266, 24);
            this.ItemForPotholeTeamPublicValue.MinSize = new System.Drawing.Size(266, 24);
            this.ItemForPotholeTeamPublicValue.Name = "ItemForPotholeTeamPublicValue";
            this.ItemForPotholeTeamPublicValue.Size = new System.Drawing.Size(266, 24);
            this.ItemForPotholeTeamPublicValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPotholeTeamPublicValue.Text = "Pothole Team Public:";
            this.ItemForPotholeTeamPublicValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForPotholeTeamEmployersValue
            // 
            this.ItemForPotholeTeamEmployersValue.Control = this.PotholeTeamEmployersValueSpinEdit;
            this.ItemForPotholeTeamEmployersValue.CustomizationFormText = "Pothole Team Employers Value:";
            this.ItemForPotholeTeamEmployersValue.Location = new System.Drawing.Point(266, 264);
            this.ItemForPotholeTeamEmployersValue.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForPotholeTeamEmployersValue.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForPotholeTeamEmployersValue.Name = "ItemForPotholeTeamEmployersValue";
            this.ItemForPotholeTeamEmployersValue.Size = new System.Drawing.Size(260, 24);
            this.ItemForPotholeTeamEmployersValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPotholeTeamEmployersValue.Text = "Pothole Team  Employers:";
            this.ItemForPotholeTeamEmployersValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImage")));
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(560, 368);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(560, 368);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 95);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 12);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(141, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(141, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(141, 23);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(318, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(290, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(141, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForSubContractorName
            // 
            this.ItemForSubContractorName.AllowHide = false;
            this.ItemForSubContractorName.Control = this.SubContractorNameButtonEdit;
            this.ItemForSubContractorName.CustomizationFormText = "Team:";
            this.ItemForSubContractorName.Location = new System.Drawing.Point(0, 23);
            this.ItemForSubContractorName.Name = "ItemForSubContractorName";
            this.ItemForSubContractorName.Size = new System.Drawing.Size(608, 24);
            this.ItemForSubContractorName.Text = "Team:";
            this.ItemForSubContractorName.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.StartDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Insurance Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForStartDate.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForStartDate.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(608, 24);
            this.ItemForStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStartDate.Text = "Insurance Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.EndDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "Insurance End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(0, 71);
            this.ItemForEndDate.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForEndDate.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(608, 24);
            this.ItemForEndDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEndDate.Text = "Insurance End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 569);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(608, 30);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00247_Core_Team_Insurance_EditTableAdapter
            // 
            this.sp00247_Core_Team_Insurance_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_Contractor_Insurance_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 673);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Contractor_Insurance_Edit";
            this.Text = "Edit Team Insurance";
            this.Activated += new System.EventHandler(this.frm_Core_Contractor_Insurance_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Contractor_Insurance_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Contractor_Insurance_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WindowCleaningEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00247CoreTeamInsuranceEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindowCleaningPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoofingEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoofingPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FencingEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FencingPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PestControlEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PestControlPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RailArbEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RailArbPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmenityArbEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmenityArbPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConstructionEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConstructionPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaintenanceEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaintenancePublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearanceEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClearancePublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedCertificateButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsuranceIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.potholeTeamPublicValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PotholeTeamEmployersValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInsuranceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedCertificate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearancePublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClearanceEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaintenancePublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaintenanceEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConstructionPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConstructionEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmenityArbPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmenityArbEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArbPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArbEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRailArbPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRailArbEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPestControlPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPestControlEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFencingPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFencingEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoofingPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoofingEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWindowCleaningPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWindowCleaningEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPotholeTeamPublicValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPotholeTeamEmployersValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInsuranceID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ButtonEdit SubContractorNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorName;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit InsuranceIDTextEdit;
        private DevExpress.XtraEditors.SpinEdit GrittingPublicValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingPublicValue;
        private DevExpress.XtraEditors.DateEdit StartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraEditors.DateEdit EndDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.TextEdit TeamIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamID;
        private System.Windows.Forms.BindingSource sp00247CoreTeamInsuranceEditBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private WoodPlanDataSetTableAdapters.sp00247_Core_Team_Insurance_EditTableAdapter sp00247_Core_Team_Insurance_EditTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit LinkedCertificateButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedCertificate;
        private DevExpress.XtraEditors.SpinEdit GrittingEmployersValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingEmployersValue;
        private DevExpress.XtraEditors.SpinEdit WindowCleaningEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit WindowCleaningPublicValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit RoofingEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit RoofingPublicValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit FencingEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit FencingPublicValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit PestControlEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit PestControlPublicValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit RailArbEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit RailArbPublicValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit UtilityArbEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit UtilityArbPublicValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit AmenityArbEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit AmenityArbPublicValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ConstructionEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ConstructionPublicValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit MaintenanceEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit MaintenancePublicValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SnowClearanceEmployersValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SnowClearancePublicValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearancePublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClearanceEmployersValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMaintenancePublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMaintenanceEmployersValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForConstructionPublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForConstructionEmployersValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmenityArbPublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmenityArbEmployersValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUtilityArbPublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUtilityArbEmployersValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRailArbPublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRailArbEmployersValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPestControlPublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPestControlEmployersValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFencingPublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFencingEmployersValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRoofingPublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRoofingEmployersValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWindowCleaningPublicValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWindowCleaningEmployersValue;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.SpinEdit potholeTeamPublicValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPotholeTeamPublicValue;
        private DevExpress.XtraEditors.SpinEdit PotholeTeamEmployersValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPotholeTeamEmployersValue;
    }
}

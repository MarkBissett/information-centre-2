namespace WoodPlan5
{
    partial class frmBase_Modal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBase_Modal));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiLoadLayout = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveLayout = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveLayoutAs = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 511);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 511);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSaveLayout,
            this.bbiSaveLayoutAs,
            this.bbiLoadLayout});
            // 
            // bar1
            // 
            this.bar1.BarName = "Layout";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLoadLayout),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveLayout, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveLayoutAs)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Custom 2";
            // 
            // bbiLoadLayout
            // 
            this.bbiLoadLayout.Caption = "Load Layout";
            this.bbiLoadLayout.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLoadLayout.Glyph")));
            this.bbiLoadLayout.Id = 29;
            this.bbiLoadLayout.Name = "bbiLoadLayout";
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Load Layout - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to Open the Load Layout screen to select a different screen layout to wo" +
    "rk with.\r\n";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiLoadLayout.SuperTip = superToolTip1;
            this.bbiLoadLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadLayout_ItemClick);
            // 
            // bbiSaveLayout
            // 
            this.bbiSaveLayout.Caption = "Save Layout";
            this.bbiSaveLayout.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveLayout.Glyph")));
            this.bbiSaveLayout.Id = 27;
            this.bbiSaveLayout.Name = "bbiSaveLayout";
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Save Layout - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to save the current screen layout.\r\n\r\nOn saving, you can specify if this" +
    " should be the default layout (automatically loaded on screen open) and you can " +
    "make it available to other users.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiSaveLayout.SuperTip = superToolTip2;
            this.bbiSaveLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveLayout_ItemClick);
            // 
            // bbiSaveLayoutAs
            // 
            this.bbiSaveLayoutAs.Caption = "Save Layout As";
            this.bbiSaveLayoutAs.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveLayoutAs.Glyph")));
            this.bbiSaveLayoutAs.Id = 28;
            this.bbiSaveLayoutAs.Name = "bbiSaveLayoutAs";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Save Layout As - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to save a copy of the current screen layout as a new layout so any chang" +
    "es made do not overwrite the original layout.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiSaveLayoutAs.SuperTip = superToolTip3;
            this.bbiSaveLayoutAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveLayoutAs_ItemClick);
            // 
            // frmBase_Modal
            // 
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frmBase_Modal";
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiSaveLayout;
        private DevExpress.XtraBars.BarButtonItem bbiSaveLayoutAs;
        private DevExpress.XtraBars.BarButtonItem bbiLoadLayout;
    }
}

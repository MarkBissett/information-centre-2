﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Client_Contact_Person_Contact_Info_Add_Fast : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string ContactPerson = "";
        public string Telephone = null;
        public string Mobile = null;
        public string Email = null;

        #endregion

        public frm_Core_Client_Contact_Person_Contact_Info_Add_Fast()
        {
            InitializeComponent();
        }

        private void frm_Core_Client_Contact_Person_Contact_Info_Add_Fast_Load(object sender, EventArgs e)
        {
            this.FormID = 500298;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //

            ContactPersonTextEdit.EditValue = ContactPerson;
            TelephoneTextEdit.EditValue = null;
            MobileTextEdit.EditValue = null;
            EmailMemoEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void EmailMemoEdit_Validating(object sender, CancelEventArgs e)
        {
            MemoEdit me = (MemoEdit)sender;
            if (!string.IsNullOrEmpty(me.EditValue.ToString()))
            {
                // Check email addresses are valid //
                int intInvalidCount = 0;
                string strInvalidEmailAddresses = "";
                string strEmail = me.Text;
                Array arrayItems = strEmail.Split(',');  // Single quotes because char expected for delimeter //
                if (arrayItems.Length > 0)  // multiple email addresses present so check each //
                {
                    foreach (string strElement in arrayItems)
                    {
                        if (!BaseObjects.ExtensionFunctions.IsValidEmail(strElement))
                        {
                            intInvalidCount++;
                            strInvalidEmailAddresses += "--> " + strElement + "\n";
                        }
                    }
                }
                else  // Only 1 email address present //
                {
                    if (!BaseObjects.ExtensionFunctions.IsValidEmail(strEmail))
                    {
                        intInvalidCount++;
                        strInvalidEmailAddresses += "--> " + strEmail + "\n";
                    }
                }
                if (intInvalidCount > 0)
                {
                    string strMessage = "The Following Email Addresses are not in a valid format:\n\n" + strInvalidEmailAddresses + "\nMultiple emails addresses should be separated with a comma. No spaces should be present.";
                    dxErrorProvider1.SetError(EmailMemoEdit, strMessage);
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
                else
                {
                    dxErrorProvider1.SetError(EmailMemoEdit, "");
                }
            }
            else
            {
                dxErrorProvider1.SetError(EmailMemoEdit, "");
            }
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();
            if (dxErrorProvider1.HasErrors)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are present!\n\nTip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (TelephoneTextEdit.EditValue != null) Telephone = TelephoneTextEdit.EditValue.ToString();
            if (MobileTextEdit.EditValue != null) Mobile = MobileTextEdit.EditValue.ToString();
            if (EmailMemoEdit.EditValue != null) Email = EmailMemoEdit.EditValue.ToString();
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }






 





    }
}

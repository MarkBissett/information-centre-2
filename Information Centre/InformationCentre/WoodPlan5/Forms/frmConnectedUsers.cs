using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frmConnectedUsers : BaseObjects.frmBase
    {
        private string strConnectionString = "";
        private Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;

        public frmConnectedUsers()
        {
            InitializeComponent();

            strConnectionString = set.WoodPlanConnectionString;
        }

        void frmConnectedUsers_Load(object sender, System.EventArgs e)
        {
            Set_Grid_Highlighter_Transparent(this.Controls);

            FillCurrentUsersTable();
        }

        private void FillCurrentUsersTable()
        {
            DateTime thisDate = DateTime.Now;
            CultureInfo ci = new CultureInfo("en-GB");

            sp00017GetConnectedUsersTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00017GetConnectedUsersTableAdapter.Fill(this.woodPlanDataSet.sp00017GetConnectedUsers, set.DatabaseName, "");

            lblDetails.Text = "Connections Active on " + thisDate.DayOfWeek.ToString() +
                              " " + thisDate.Day.ToString() +
                              " " + thisDate.ToString("Y") + " at " + thisDate.ToString("T", ci);

            lblConnectionCount.Text = this.woodPlanDataSet.sp00017GetConnectedUsers.Rows.Count + " Connections";
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            FillCurrentUsersTable();
        }

        private void gvActiveConnections_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            string strUser = view.GetFocusedRowCellValue("loginame").ToString().ToUpper();
            char[] delimiters = new char[] { '\\' };
            string[] strArray = strUser.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length == 0)
            {
                bbiDelete.Enabled = false;
                return;
            }
            string strName = strArray[strArray.Length - 1];
            bbiDelete.Enabled = (strName == this.GlobalSettings.Username.ToUpper() ? false : true);  // Disable Delete if current user connection selected //
        }

        private void gvActiveConnections_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();

            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bsiAdd.Enabled = false;
                bsiEdit.Enabled = false;
                bbiSave.Enabled = false;
                bsiDataset.Enabled = false;
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Delete_Record()
        {
            GridView view = (GridView)gcActiveConnections.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            short shortID = Convert.ToInt16(view.GetFocusedRowCellValue("spid"));
            string strUser = view.GetFocusedRowCellValue("loginame").ToString().ToUpper();
            if (strUser == this.GlobalSettings.Username.ToUpper())
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You cannot kill your own database connection!", "Kill Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to kill the selected database connection.\n\nAre you sure you wish to proceed?", "Kill Database Connection", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No) return;

            WoodPlanDataSetTableAdapters.QueriesTableAdapter KillConnection = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
            KillConnection.ChangeConnectionString(strConnectionString);

            try
            {
                KillConnection.sp00018_Kill_Process(shortID);  // Remove the records from the DB in one go //

            }
            catch (Exception)
            {
                return;
            }
            FillCurrentUsersTable();
            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show("Selected Database Connection Killed", "Kill Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void gvActiveConnections_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Connected Users Available");

        }
    }
}


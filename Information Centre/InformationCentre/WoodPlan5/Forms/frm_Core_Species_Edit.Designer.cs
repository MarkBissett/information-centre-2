namespace WoodPlan5
{
    partial class frm_Core_Species_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Species_Edit));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.UtilityArbVisibleCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.sp00170SpeciesItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.AmenityArbVisibleCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.intSpeciesIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strScientificNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.decRiskFactorSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00172SpeciesTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForintSpeciesID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrScientificName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordecRiskFactor = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAmenityArbVisible = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUtilityArbVisible = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00170_Species_ItemTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00170_Species_ItemTableAdapter();
            this.sp00172_Species_Type_ListTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00172_Species_Type_ListTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbVisibleCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00170SpeciesItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmenityArbVisibleCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSpeciesIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strScientificNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.decRiskFactorSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00172SpeciesTypeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSpeciesID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrScientificName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecRiskFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmenityArbVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArbVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 509);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 483);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 483);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 509);
            this.barDockControl2.Size = new System.Drawing.Size(628, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 483);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 483);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.UtilityArbVisibleCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.AmenityArbVisibleCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intSpeciesIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strScientificNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.decRiskFactorSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strTypeGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp00170SpeciesItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintSpeciesID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1150, 309, 450, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 483);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // UtilityArbVisibleCheckEdit
            // 
            this.UtilityArbVisibleCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "UtilityArbVisible", true));
            this.UtilityArbVisibleCheckEdit.Location = new System.Drawing.Point(86, 149);
            this.UtilityArbVisibleCheckEdit.MenuManager = this.barManager1;
            this.UtilityArbVisibleCheckEdit.Name = "UtilityArbVisibleCheckEdit";
            this.UtilityArbVisibleCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.UtilityArbVisibleCheckEdit.Properties.ValueChecked = 1;
            this.UtilityArbVisibleCheckEdit.Properties.ValueUnchecked = 0;
            this.UtilityArbVisibleCheckEdit.Size = new System.Drawing.Size(535, 19);
            this.UtilityArbVisibleCheckEdit.StyleController = this.dataLayoutControl1;
            this.UtilityArbVisibleCheckEdit.TabIndex = 14;
            // 
            // sp00170SpeciesItemBindingSource
            // 
            this.sp00170SpeciesItemBindingSource.DataMember = "sp00170_Species_Item";
            this.sp00170SpeciesItemBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // AmenityArbVisibleCheckEdit
            // 
            this.AmenityArbVisibleCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "AmenityArbVisible", true));
            this.AmenityArbVisibleCheckEdit.Location = new System.Drawing.Point(86, 126);
            this.AmenityArbVisibleCheckEdit.MenuManager = this.barManager1;
            this.AmenityArbVisibleCheckEdit.Name = "AmenityArbVisibleCheckEdit";
            this.AmenityArbVisibleCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.AmenityArbVisibleCheckEdit.Properties.ValueChecked = 1;
            this.AmenityArbVisibleCheckEdit.Properties.ValueUnchecked = 0;
            this.AmenityArbVisibleCheckEdit.Size = new System.Drawing.Size(535, 19);
            this.AmenityArbVisibleCheckEdit.StyleController = this.dataLayoutControl1;
            this.AmenityArbVisibleCheckEdit.TabIndex = 13;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00170SpeciesItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(89, 7);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(178, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 12;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // intSpeciesIDTextEdit
            // 
            this.intSpeciesIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "intSpeciesID", true));
            this.intSpeciesIDTextEdit.Location = new System.Drawing.Point(92, 12);
            this.intSpeciesIDTextEdit.MenuManager = this.barManager1;
            this.intSpeciesIDTextEdit.Name = "intSpeciesIDTextEdit";
            this.intSpeciesIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intSpeciesIDTextEdit, true);
            this.intSpeciesIDTextEdit.Size = new System.Drawing.Size(524, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intSpeciesIDTextEdit, optionsSpelling1);
            this.intSpeciesIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intSpeciesIDTextEdit.TabIndex = 4;
            // 
            // strCodeTextEdit
            // 
            this.strCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "strCode", true));
            this.strCodeTextEdit.Location = new System.Drawing.Point(86, 30);
            this.strCodeTextEdit.MenuManager = this.barManager1;
            this.strCodeTextEdit.Name = "strCodeTextEdit";
            this.strCodeTextEdit.Properties.MaxLength = 15;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strCodeTextEdit, true);
            this.strCodeTextEdit.Size = new System.Drawing.Size(535, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strCodeTextEdit, optionsSpelling2);
            this.strCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strCodeTextEdit.TabIndex = 5;
            // 
            // strNameTextEdit
            // 
            this.strNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "strName", true));
            this.strNameTextEdit.Location = new System.Drawing.Point(86, 54);
            this.strNameTextEdit.MenuManager = this.barManager1;
            this.strNameTextEdit.Name = "strNameTextEdit";
            this.strNameTextEdit.Properties.MaxLength = 50;
            this.strNameTextEdit.Properties.NullValuePrompt = "Enter a Value";
            this.strNameTextEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strNameTextEdit, true);
            this.strNameTextEdit.Size = new System.Drawing.Size(535, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strNameTextEdit, optionsSpelling3);
            this.strNameTextEdit.StyleController = this.dataLayoutControl1;
            this.strNameTextEdit.TabIndex = 6;
            this.strNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strNameTextEdit_Validating);
            // 
            // strScientificNameTextEdit
            // 
            this.strScientificNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "strScientificName", true));
            this.strScientificNameTextEdit.Location = new System.Drawing.Point(86, 78);
            this.strScientificNameTextEdit.MenuManager = this.barManager1;
            this.strScientificNameTextEdit.Name = "strScientificNameTextEdit";
            this.strScientificNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strScientificNameTextEdit, true);
            this.strScientificNameTextEdit.Size = new System.Drawing.Size(535, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strScientificNameTextEdit, optionsSpelling4);
            this.strScientificNameTextEdit.StyleController = this.dataLayoutControl1;
            this.strScientificNameTextEdit.TabIndex = 7;
            // 
            // intOrderSpinEdit
            // 
            this.intOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "intOrder", true));
            this.intOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intOrderSpinEdit.Location = new System.Drawing.Point(86, 172);
            this.intOrderSpinEdit.MenuManager = this.barManager1;
            this.intOrderSpinEdit.Name = "intOrderSpinEdit";
            this.intOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intOrderSpinEdit.Properties.IsFloatValue = false;
            this.intOrderSpinEdit.Properties.Mask.EditMask = "f0";
            this.intOrderSpinEdit.Size = new System.Drawing.Size(535, 20);
            this.intOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.intOrderSpinEdit.TabIndex = 9;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(86, 220);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(535, 119);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling5);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 10;
            // 
            // decRiskFactorSpinEdit
            // 
            this.decRiskFactorSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "decRiskFactor", true));
            this.decRiskFactorSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.decRiskFactorSpinEdit.Location = new System.Drawing.Point(86, 196);
            this.decRiskFactorSpinEdit.MenuManager = this.barManager1;
            this.decRiskFactorSpinEdit.Name = "decRiskFactorSpinEdit";
            this.decRiskFactorSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.decRiskFactorSpinEdit.Properties.Mask.EditMask = "f2";
            this.decRiskFactorSpinEdit.Size = new System.Drawing.Size(535, 20);
            this.decRiskFactorSpinEdit.StyleController = this.dataLayoutControl1;
            this.decRiskFactorSpinEdit.TabIndex = 11;
            // 
            // strTypeGridLookUpEdit
            // 
            this.strTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00170SpeciesItemBindingSource, "strType", true));
            this.strTypeGridLookUpEdit.Location = new System.Drawing.Point(86, 102);
            this.strTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.strTypeGridLookUpEdit.Name = "strTypeGridLookUpEdit";
            this.strTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.strTypeGridLookUpEdit.Properties.DataSource = this.sp00172SpeciesTypeListBindingSource;
            this.strTypeGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.strTypeGridLookUpEdit.Properties.NullText = "";
            this.strTypeGridLookUpEdit.Properties.NullValuePrompt = "Select a value";
            this.strTypeGridLookUpEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.strTypeGridLookUpEdit.Properties.ValueMember = "ItemValue";
            this.strTypeGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.strTypeGridLookUpEdit.Size = new System.Drawing.Size(535, 20);
            this.strTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.strTypeGridLookUpEdit.TabIndex = 8;
            // 
            // sp00172SpeciesTypeListBindingSource
            // 
            this.sp00172SpeciesTypeListBindingSource.DataMember = "sp00172_Species_Type_List";
            this.sp00172SpeciesTypeListBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemDescription,
            this.colItemOrder,
            this.colItemValue});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemDescription
            // 
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 250;
            // 
            // colItemOrder
            // 
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.AllowEdit = false;
            this.colItemOrder.OptionsColumn.AllowFocus = false;
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            this.colItemOrder.Width = 87;
            // 
            // colItemValue
            // 
            this.colItemValue.FieldName = "ItemValue";
            this.colItemValue.Name = "colItemValue";
            this.colItemValue.OptionsColumn.AllowEdit = false;
            this.colItemValue.OptionsColumn.AllowFocus = false;
            this.colItemValue.OptionsColumn.ReadOnly = true;
            this.colItemValue.Width = 72;
            // 
            // ItemForintSpeciesID
            // 
            this.ItemForintSpeciesID.Control = this.intSpeciesIDTextEdit;
            this.ItemForintSpeciesID.CustomizationFormText = "Species ID:";
            this.ItemForintSpeciesID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintSpeciesID.Name = "ItemForintSpeciesID";
            this.ItemForintSpeciesID.Size = new System.Drawing.Size(608, 24);
            this.ItemForintSpeciesID.Text = "Species ID:";
            this.ItemForintSpeciesID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 483);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrCode,
            this.ItemForstrName,
            this.ItemForstrScientificName,
            this.ItemForstrType,
            this.ItemForintOrder,
            this.ItemForstrRemarks,
            this.ItemFordecRiskFactor,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.ItemForAmenityArbVisible,
            this.ItemForUtilityArbVisible});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(618, 473);
            // 
            // ItemForstrCode
            // 
            this.ItemForstrCode.Control = this.strCodeTextEdit;
            this.ItemForstrCode.CustomizationFormText = "Code:";
            this.ItemForstrCode.Location = new System.Drawing.Point(0, 23);
            this.ItemForstrCode.Name = "ItemForstrCode";
            this.ItemForstrCode.Size = new System.Drawing.Size(618, 24);
            this.ItemForstrCode.Text = "Code:";
            this.ItemForstrCode.TextSize = new System.Drawing.Size(76, 13);
            // 
            // ItemForstrName
            // 
            this.ItemForstrName.Control = this.strNameTextEdit;
            this.ItemForstrName.CustomizationFormText = "Name:";
            this.ItemForstrName.Location = new System.Drawing.Point(0, 47);
            this.ItemForstrName.Name = "ItemForstrName";
            this.ItemForstrName.Size = new System.Drawing.Size(618, 24);
            this.ItemForstrName.Text = "Name:";
            this.ItemForstrName.TextSize = new System.Drawing.Size(76, 13);
            // 
            // ItemForstrScientificName
            // 
            this.ItemForstrScientificName.Control = this.strScientificNameTextEdit;
            this.ItemForstrScientificName.CustomizationFormText = "Scientific Name:";
            this.ItemForstrScientificName.Location = new System.Drawing.Point(0, 71);
            this.ItemForstrScientificName.Name = "ItemForstrScientificName";
            this.ItemForstrScientificName.Size = new System.Drawing.Size(618, 24);
            this.ItemForstrScientificName.Text = "Scientific Name:";
            this.ItemForstrScientificName.TextSize = new System.Drawing.Size(76, 13);
            // 
            // ItemForstrType
            // 
            this.ItemForstrType.Control = this.strTypeGridLookUpEdit;
            this.ItemForstrType.CustomizationFormText = "Type:";
            this.ItemForstrType.Location = new System.Drawing.Point(0, 95);
            this.ItemForstrType.Name = "ItemForstrType";
            this.ItemForstrType.Size = new System.Drawing.Size(618, 24);
            this.ItemForstrType.Text = "Type:";
            this.ItemForstrType.TextSize = new System.Drawing.Size(76, 13);
            // 
            // ItemForintOrder
            // 
            this.ItemForintOrder.Control = this.intOrderSpinEdit;
            this.ItemForintOrder.CustomizationFormText = "Order:";
            this.ItemForintOrder.Location = new System.Drawing.Point(0, 165);
            this.ItemForintOrder.Name = "ItemForintOrder";
            this.ItemForintOrder.Size = new System.Drawing.Size(618, 24);
            this.ItemForintOrder.Text = "Order:";
            this.ItemForintOrder.TextSize = new System.Drawing.Size(76, 13);
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 213);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(618, 123);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(76, 13);
            // 
            // ItemFordecRiskFactor
            // 
            this.ItemFordecRiskFactor.Control = this.decRiskFactorSpinEdit;
            this.ItemFordecRiskFactor.CustomizationFormText = "Risk Factor:";
            this.ItemFordecRiskFactor.Location = new System.Drawing.Point(0, 189);
            this.ItemFordecRiskFactor.Name = "ItemFordecRiskFactor";
            this.ItemFordecRiskFactor.Size = new System.Drawing.Size(618, 24);
            this.ItemFordecRiskFactor.Text = "Risk Factor:";
            this.ItemFordecRiskFactor.TextSize = new System.Drawing.Size(76, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 336);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(618, 137);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(82, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(182, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(82, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(82, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(82, 23);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(264, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(354, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAmenityArbVisible
            // 
            this.ItemForAmenityArbVisible.Control = this.AmenityArbVisibleCheckEdit;
            this.ItemForAmenityArbVisible.Location = new System.Drawing.Point(0, 119);
            this.ItemForAmenityArbVisible.Name = "ItemForAmenityArbVisible";
            this.ItemForAmenityArbVisible.Size = new System.Drawing.Size(618, 23);
            this.ItemForAmenityArbVisible.Text = "Amenity Arb:";
            this.ItemForAmenityArbVisible.TextSize = new System.Drawing.Size(76, 13);
            // 
            // ItemForUtilityArbVisible
            // 
            this.ItemForUtilityArbVisible.Control = this.UtilityArbVisibleCheckEdit;
            this.ItemForUtilityArbVisible.Location = new System.Drawing.Point(0, 142);
            this.ItemForUtilityArbVisible.Name = "ItemForUtilityArbVisible";
            this.ItemForUtilityArbVisible.Size = new System.Drawing.Size(618, 23);
            this.ItemForUtilityArbVisible.Text = "Utility Arb:";
            this.ItemForUtilityArbVisible.TextSize = new System.Drawing.Size(76, 13);
            // 
            // sp00170_Species_ItemTableAdapter
            // 
            this.sp00170_Species_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00172_Species_Type_ListTableAdapter
            // 
            this.sp00172_Species_Type_ListTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_Species_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Species_Edit";
            this.Text = "Edit Species";
            this.Activated += new System.EventHandler(this.frm_Core_Species_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Species_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Species_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbVisibleCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00170SpeciesItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmenityArbVisibleCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSpeciesIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strScientificNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.decRiskFactorSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00172SpeciesTypeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSpeciesID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrScientificName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecRiskFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmenityArbVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArbVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit intSpeciesIDTextEdit;
        private System.Windows.Forms.BindingSource sp00170SpeciesItemBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraEditors.TextEdit strCodeTextEdit;
        private DevExpress.XtraEditors.TextEdit strNameTextEdit;
        private DevExpress.XtraEditors.TextEdit strScientificNameTextEdit;
        private DevExpress.XtraEditors.SpinEdit intOrderSpinEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.SpinEdit decRiskFactorSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSpeciesID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrScientificName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordecRiskFactor;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00170_Species_ItemTableAdapter sp00170_Species_ItemTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit strTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private System.Windows.Forms.BindingSource sp00172SpeciesTypeListBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00172_Species_Type_ListTableAdapter sp00172_Species_Type_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colItemValue;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit UtilityArbVisibleCheckEdit;
        private DevExpress.XtraEditors.CheckEdit AmenityArbVisibleCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmenityArbVisible;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUtilityArbVisible;
    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Blending;

using BaseObjects;
using WoodPlan5.Properties;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_Core_JSON_Viewer : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        public string strPassedInJSON = "";
        public string strRecordIDs = "";

       XtraTreeListBlending treeListBlending;
 
        #endregion

        public frm_Core_JSON_Viewer()
        {
            InitializeComponent();
        }

        private void frm_Core_JSON_Viewer_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 8007;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            treeListBlending = new XtraTreeListBlending();
            treeListBlending.TreeListControl = treeList1;
            treeListBlending.AlphaStyles.AddReplace("GroupPanel", 125);
            treeListBlending.AlphaStyles.AddReplace("Empty", 125);
            treeListBlending.AlphaStyles.AddReplace("HeaderPanel", 250);
            treeListBlending.AlphaStyles.AddReplace("EvenRow", 125);
            treeListBlending.AlphaStyles.AddReplace("FocusedRow", 125);
            treeListBlending.AlphaStyles.AddReplace("RowSeparator", 125);
            treeListBlending.AlphaStyles.AddReplace("OddRow", 125);
            treeListBlending.AlphaStyles.AddReplace("GroupFooter", 125);
            treeListBlending.AlphaStyles.AddReplace("SelectedRow", 125);
            treeListBlending.AlphaStyles.AddReplace("GroupRow", 125);
            treeListBlending.AlphaStyles.AddReplace("TopNewRow", 125);
            treeListBlending.AlphaStyles.AddReplace("Row", 125);
            treeListBlending.AlphaStyles.AddReplace("Preview", 125);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            if (strPassedInJSON != "") 
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                
                // Process the JSON //
                memoEditText.EditValue = strPassedInJSON;
                LoadData(memoEditText.EditValue.ToString());
            }
        }

        private void frm_Core_JSON_Viewer_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }


        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = true;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

        }


        private void buttonEditURL_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString())
            {
                case "load":
                    {
                        try
                        {
                            string content = WGet(buttonEditURL.Text);
                            if (String.IsNullOrEmpty(content)) throw new Exception("No data received!");

                            LoadData(content);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        break;
                    }
                default:
                    break;
            }
        }

        private void buttonEditFile_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString())
            {
                case "select":
                    {
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "txt";
                            dlg.Filter = "";
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.ShowDialog() == DialogResult.OK)
                            {
                                buttonEditFile.Text = dlg.FileName;
                            }
                        }
                    }
                    break;
                case "load":
                    {
                        string data = String.Empty;
                        try
                        {
                            using (StreamReader s = new StreamReader(buttonEditFile.Text))
                            {
                                data = s.ReadToEnd();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            return;
                        }
                        LoadData(data);
                        break;
                    }
                default:
                    break;
            }
        }

        private void btnPaste_Click(object sender, EventArgs e)
        {
            try
            {
                if (Clipboard.ContainsText()) memoEditText.EditValue = Clipboard.GetText();
            }
            catch { }
        }

        private void btnLoadText_Click(object sender, EventArgs e)
        {
            LoadData(memoEditText.EditValue.ToString());
        }

        int intCounter = 0;
        private void LoadData(string data)
        {
            try
            {
                BjSJsonObject json = new BjSJsonObject(data);

                treeList1.Nodes.Clear();

                intCounter = 0;
                TreeListNode parentForRootNodes = null;
                ConvertToTreeNode(json, parentForRootNodes, "");
                treeList1.ExpandAll();

                memoEditText.EditValue = json.ToJsonString(false);
                xtraTabControl1.SelectedTabPage = xtraTabPage2;
            }
            catch (Exception ex) 
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred loading the data - error: " + ex.Message, "Load Text", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }


        #region Helpers

        private void ConvertToTreeNode(BjSJsonObject obj, TreeListNode parentForRootNode, string strParentName)
        {
            //TreeListNode rootNode = treeList1.AppendNode(new object[] { String.Format(String.IsNullOrEmpty(strParentName) ? "Object{{{1}}}" : "\"{0}\" : Object{{{1}}}", strParentName, obj.Count), intCounter }, parentForRootNode);
            TreeListNode rootNode = treeList1.AppendNode(new object[] { String.Format(String.IsNullOrEmpty(strParentName) ? "Object{{{1}}}" : "\"{0}\" : Object{{{1}}}", strParentName, "", obj.Count), "", intCounter }, parentForRootNode);
            rootNode.ImageIndex = 0;
            rootNode.SelectImageIndex = 0;
            rootNode.Expanded = true;
            intCounter++;

            foreach (BjSJsonObjectMember member in obj)
            {
                switch (member.ValueKind)
                {
                    case BjSJsonValueKind.Object:
                        {
                            ConvertToTreeNode(member.Value as BjSJsonObject, rootNode, member.Name);
                        }
                        break;
                    case BjSJsonValueKind.Array:
                        {
                            ConvertToTreeNode(member.Value as BjSJsonArray, rootNode, member.Name);
                        }
                        break;
                    case BjSJsonValueKind.String:
                        {
                            //TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("\"{0}\" : \"{1}\"", member.Name, member.Value), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", member.Name), String.Format("{0}", member.Value), intCounter }, rootNode);
                            newNode.ImageIndex = 2;
                            newNode.SelectImageIndex = 2;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    case BjSJsonValueKind.Number:
                        {
                            //TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("\"{0}\" : {1}", member.Name, member.Value), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", member.Name), String.Format("{0}", member.Value), intCounter }, rootNode);
                            newNode.ImageIndex = 3;
                            newNode.SelectImageIndex = 3;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    case BjSJsonValueKind.Boolean:
                        {
                           // TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("\"{0}\" : {1}", member.Name, member.Value), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", member.Name), String.Format("{0}", member.Value), intCounter }, rootNode);
                            newNode.ImageIndex = 4;
                            newNode.SelectImageIndex = 4;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    case BjSJsonValueKind.Null:
                        {
                            //TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("\"{0}\" : null", member.Name), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", member.Name), "Null", intCounter }, rootNode);
                            newNode.ImageIndex = 5;
                            newNode.SelectImageIndex = 5;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    case BjSJsonValueKind.DateTime:
                        {
                            //TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("\"{0}\" : {1}", member.Name, member.Value), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", member.Name), String.Format("{0}", member.Value), intCounter }, rootNode);
                            newNode.ImageIndex = 6;
                            newNode.SelectImageIndex = 6;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    default:
                        break;
                }
            }

            return;
        }

        private void ConvertToTreeNode(BjSJsonArray arr, TreeListNode parentForRootNode, string strParentName)
        {

            //TreeListNode rootNode = treeList1.AppendNode(new object[] { String.Format(String.IsNullOrEmpty(strParentName) ? "Array[{1}]" : "\"{0}\" : Array[{1}]", strParentName, arr.Count), intCounter }, parentForRootNode);
            TreeListNode rootNode = treeList1.AppendNode(new object[] { String.Format(String.IsNullOrEmpty(strParentName) ? "Array[{1}]" : "{0} : Array[{1}]", strParentName, arr.Count), "", intCounter }, parentForRootNode);
            rootNode.ImageIndex = 1;
            rootNode.SelectImageIndex = 1;
            rootNode.Expanded = true;
            intCounter++;

            for (int i = 0; i < arr.Count; i++)
            {
                var obj = arr[i];
                switch (BjSJsonHelper.GetValueKind(obj))
                {
                    case BjSJsonValueKind.Object:
                        {
                            ConvertToTreeNode(obj as BjSJsonObject, rootNode, "");
                        }
                        break;
                    case BjSJsonValueKind.Array:
                        {
                            ConvertToTreeNode(obj as BjSJsonArray, rootNode, "");
                        }
                        break;
                    case BjSJsonValueKind.String:
                        {
                            //TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0} : \"{1}\"", i, obj), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", i, obj), String.Format("{0}", obj), intCounter }, rootNode);
                            newNode.ImageIndex = 2;
                            newNode.SelectImageIndex = 2;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    case BjSJsonValueKind.Number:
                        {
                            //TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0} : {1}", i, obj), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", i), String.Format("{0}", obj), intCounter }, rootNode);
                            newNode.ImageIndex = 3;
                            newNode.SelectImageIndex = 3;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    case BjSJsonValueKind.Boolean:
                        {
                            //TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0} : {1}", i, obj), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", i), String.Format("{0}", obj), intCounter }, rootNode);
                            newNode.ImageIndex = 4;
                            newNode.SelectImageIndex = 4;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    case BjSJsonValueKind.Null:
                        {
                            //TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0} : null", i), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", i), "null", intCounter }, rootNode);
                            newNode.ImageIndex = 5;
                            newNode.SelectImageIndex = 5;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    case BjSJsonValueKind.DateTime:
                        {
                            //TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0} : {1}", i, obj), intCounter }, rootNode);
                            TreeListNode newNode = treeList1.AppendNode(new object[] { String.Format("{0}", i), String.Format("{0}", obj), intCounter }, rootNode);
                            newNode.ImageIndex = 6;
                            newNode.SelectImageIndex = 6;
                            newNode.Expanded = true;
                            intCounter++;
                        }
                        break;
                    default:
                        break;
                }
            }
            return;
        }

        public static string WGet(string url)
        {
            string result = String.Empty;

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
            req.UserAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0";
            req.Timeout = 10000;
            req.Method = "GET";

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            if (res.StatusCode == HttpStatusCode.OK)
            {
                StreamReader s = new StreamReader(res.GetResponseStream());
                result = s.ReadToEnd();
            }
            else
            {
                throw new Exception(String.Format("{0}: {1}", res.StatusCode, res.StatusDescription));
            }

            res.Close();

            return result;
        }

        #endregion

        private void treeList1_CustomDrawEmptyArea(object sender, CustomDrawEmptyAreaEventArgs e)
        {
            //if (treeList1.Nodes.Count > 1) return;
            //string s = "No Data Available - Use the Import page to suppply data.";
            //e.Graphics.DrawString(s, new Font("Tahoma", 10, FontStyle.Bold),
            //  new SolidBrush(Color.Black), e.Bounds);
            //e.Handled = true;

            TreeList treeList = (TreeList)sender;
            if (treeList.Nodes.Count > 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Data Available - Use the Import page to suppply data.", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }







    }
}

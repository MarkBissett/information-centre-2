namespace WoodPlan5
{
    partial class frm_Core_Accident_Select
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Accident_Select));
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00314AccidentSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Accident = new WoodPlan5.DataSet_Accident();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAccidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAccidentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPersonOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedToHSE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateReportedToHSE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colExternalReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSituationRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleManagerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHSQETeamLead = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCorrectiveActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTargetClosureDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleManager = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordFullDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHSQETeamLeadName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemActive = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.sp00314_Accident_SelectTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00314_Accident_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00314AccidentSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(518, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(518, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(518, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemActive,
            this.bbiRefresh});
            this.barManager1.MaxItemId = 33;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00314AccidentSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDateTime});
            this.gridControl1.Size = new System.Drawing.Size(517, 367);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00314AccidentSelectBindingSource
            // 
            this.sp00314AccidentSelectBindingSource.DataMember = "sp00314_Accident_Select";
            this.sp00314AccidentSelectBindingSource.DataSource = this.dataSet_Accident;
            // 
            // dataSet_Accident
            // 
            this.dataSet_Accident.DataSetName = "DataSet_Accident";
            this.dataSet_Accident.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAccidentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colBusinessAreaID1,
            this.colAccidentDate,
            this.colAccidentTypeID,
            this.colAccidentSubTypeID,
            this.colAccidentStatusID,
            this.colReportedByPersonID,
            this.colReportedByPersonTypeID,
            this.colReportedByPersonOther,
            this.colReportedToHSE,
            this.colDateReportedToHSE,
            this.colAccidentDescription,
            this.colExternalReferenceNumber,
            this.colClientReferenceNumber,
            this.colSituationRemarks,
            this.colResponsibleManagerID,
            this.colHSQETeamLead,
            this.colCorrectiveActionRemarks,
            this.colTargetClosureDate,
            this.colClosedDate,
            this.colClosedByStaffID,
            this.colRemarks,
            this.colBusinessAreaName1,
            this.colAccidentType,
            this.colAccidentSubType,
            this.colAccidentStatus,
            this.colReportedByPerson,
            this.colReportedByPersonType,
            this.colResponsibleManager,
            this.colLinkedToRecordFullDescription,
            this.colClientName,
            this.colClientContract,
            this.colSiteName,
            this.colLinkedToRecord,
            this.colLinkedToRecordType,
            this.colClosedByStaff,
            this.colHSQETeamLeadName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colAccidentID
            // 
            this.colAccidentID.Caption = "Accident ID";
            this.colAccidentID.FieldName = "AccidentID";
            this.colAccidentID.Name = "colAccidentID";
            this.colAccidentID.OptionsColumn.AllowEdit = false;
            this.colAccidentID.OptionsColumn.AllowFocus = false;
            this.colAccidentID.OptionsColumn.ReadOnly = true;
            this.colAccidentID.Width = 76;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked To Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 117;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 144;
            // 
            // colBusinessAreaID1
            // 
            this.colBusinessAreaID1.Caption = "Business Area ID";
            this.colBusinessAreaID1.FieldName = "BusinessAreaID";
            this.colBusinessAreaID1.Name = "colBusinessAreaID1";
            this.colBusinessAreaID1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID1.Width = 102;
            // 
            // colAccidentDate
            // 
            this.colAccidentDate.Caption = "Accident Date";
            this.colAccidentDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colAccidentDate.FieldName = "AccidentDate";
            this.colAccidentDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAccidentDate.Name = "colAccidentDate";
            this.colAccidentDate.OptionsColumn.AllowEdit = false;
            this.colAccidentDate.OptionsColumn.AllowFocus = false;
            this.colAccidentDate.OptionsColumn.ReadOnly = true;
            this.colAccidentDate.Visible = true;
            this.colAccidentDate.VisibleIndex = 0;
            this.colAccidentDate.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colAccidentTypeID
            // 
            this.colAccidentTypeID.Caption = "Type ID";
            this.colAccidentTypeID.FieldName = "AccidentTypeID";
            this.colAccidentTypeID.Name = "colAccidentTypeID";
            this.colAccidentTypeID.OptionsColumn.AllowEdit = false;
            this.colAccidentTypeID.OptionsColumn.AllowFocus = false;
            this.colAccidentTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colAccidentSubTypeID
            // 
            this.colAccidentSubTypeID.Caption = "Sub-Type ID";
            this.colAccidentSubTypeID.FieldName = "AccidentSubTypeID";
            this.colAccidentSubTypeID.Name = "colAccidentSubTypeID";
            this.colAccidentSubTypeID.OptionsColumn.AllowEdit = false;
            this.colAccidentSubTypeID.OptionsColumn.AllowFocus = false;
            this.colAccidentSubTypeID.OptionsColumn.ReadOnly = true;
            this.colAccidentSubTypeID.Width = 81;
            // 
            // colAccidentStatusID
            // 
            this.colAccidentStatusID.Caption = "Status ID";
            this.colAccidentStatusID.FieldName = "AccidentStatusID";
            this.colAccidentStatusID.Name = "colAccidentStatusID";
            this.colAccidentStatusID.OptionsColumn.AllowEdit = false;
            this.colAccidentStatusID.OptionsColumn.AllowFocus = false;
            this.colAccidentStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colReportedByPersonID
            // 
            this.colReportedByPersonID.Caption = "Reported By Person ID";
            this.colReportedByPersonID.FieldName = "ReportedByPersonID";
            this.colReportedByPersonID.Name = "colReportedByPersonID";
            this.colReportedByPersonID.OptionsColumn.AllowEdit = false;
            this.colReportedByPersonID.OptionsColumn.AllowFocus = false;
            this.colReportedByPersonID.OptionsColumn.ReadOnly = true;
            this.colReportedByPersonID.Width = 131;
            // 
            // colReportedByPersonTypeID
            // 
            this.colReportedByPersonTypeID.Caption = "Reported By Person Type ID";
            this.colReportedByPersonTypeID.FieldName = "ReportedByPersonTypeID";
            this.colReportedByPersonTypeID.Name = "colReportedByPersonTypeID";
            this.colReportedByPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colReportedByPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colReportedByPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colReportedByPersonTypeID.Width = 158;
            // 
            // colReportedByPersonOther
            // 
            this.colReportedByPersonOther.Caption = "Reported By Person Other";
            this.colReportedByPersonOther.FieldName = "ReportedByPersonOther";
            this.colReportedByPersonOther.Name = "colReportedByPersonOther";
            this.colReportedByPersonOther.OptionsColumn.AllowEdit = false;
            this.colReportedByPersonOther.OptionsColumn.AllowFocus = false;
            this.colReportedByPersonOther.OptionsColumn.ReadOnly = true;
            this.colReportedByPersonOther.Width = 148;
            // 
            // colReportedToHSE
            // 
            this.colReportedToHSE.Caption = "Reported To HSE";
            this.colReportedToHSE.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReportedToHSE.FieldName = "ReportedToHSE";
            this.colReportedToHSE.Name = "colReportedToHSE";
            this.colReportedToHSE.OptionsColumn.AllowEdit = false;
            this.colReportedToHSE.OptionsColumn.AllowFocus = false;
            this.colReportedToHSE.OptionsColumn.ReadOnly = true;
            this.colReportedToHSE.Visible = true;
            this.colReportedToHSE.VisibleIndex = 16;
            this.colReportedToHSE.Width = 103;
            // 
            // colDateReportedToHSE
            // 
            this.colDateReportedToHSE.Caption = "Reported To HSE Date";
            this.colDateReportedToHSE.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateReportedToHSE.FieldName = "DateReportedToHSE";
            this.colDateReportedToHSE.Name = "colDateReportedToHSE";
            this.colDateReportedToHSE.OptionsColumn.AllowEdit = false;
            this.colDateReportedToHSE.OptionsColumn.AllowFocus = false;
            this.colDateReportedToHSE.OptionsColumn.ReadOnly = true;
            this.colDateReportedToHSE.Visible = true;
            this.colDateReportedToHSE.VisibleIndex = 15;
            this.colDateReportedToHSE.Width = 129;
            // 
            // colAccidentDescription
            // 
            this.colAccidentDescription.Caption = "Accident Description";
            this.colAccidentDescription.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colAccidentDescription.FieldName = "AccidentDescription";
            this.colAccidentDescription.Name = "colAccidentDescription";
            this.colAccidentDescription.OptionsColumn.ReadOnly = true;
            this.colAccidentDescription.Visible = true;
            this.colAccidentDescription.VisibleIndex = 5;
            this.colAccidentDescription.Width = 215;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colExternalReferenceNumber
            // 
            this.colExternalReferenceNumber.Caption = "External Reference #";
            this.colExternalReferenceNumber.FieldName = "ExternalReferenceNumber";
            this.colExternalReferenceNumber.Name = "colExternalReferenceNumber";
            this.colExternalReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colExternalReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colExternalReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colExternalReferenceNumber.Visible = true;
            this.colExternalReferenceNumber.VisibleIndex = 6;
            this.colExternalReferenceNumber.Width = 125;
            // 
            // colClientReferenceNumber
            // 
            this.colClientReferenceNumber.Caption = "Client Reference #";
            this.colClientReferenceNumber.FieldName = "ClientReferenceNumber";
            this.colClientReferenceNumber.Name = "colClientReferenceNumber";
            this.colClientReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colClientReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colClientReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colClientReferenceNumber.Visible = true;
            this.colClientReferenceNumber.VisibleIndex = 17;
            this.colClientReferenceNumber.Width = 112;
            // 
            // colSituationRemarks
            // 
            this.colSituationRemarks.Caption = "Situation";
            this.colSituationRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSituationRemarks.FieldName = "SituationRemarks";
            this.colSituationRemarks.Name = "colSituationRemarks";
            this.colSituationRemarks.OptionsColumn.ReadOnly = true;
            this.colSituationRemarks.Visible = true;
            this.colSituationRemarks.VisibleIndex = 7;
            this.colSituationRemarks.Width = 167;
            // 
            // colResponsibleManagerID
            // 
            this.colResponsibleManagerID.Caption = "Responsible Manager ID";
            this.colResponsibleManagerID.FieldName = "ResponsibleManagerID";
            this.colResponsibleManagerID.Name = "colResponsibleManagerID";
            this.colResponsibleManagerID.OptionsColumn.AllowEdit = false;
            this.colResponsibleManagerID.OptionsColumn.AllowFocus = false;
            this.colResponsibleManagerID.OptionsColumn.ReadOnly = true;
            this.colResponsibleManagerID.Width = 137;
            // 
            // colHSQETeamLead
            // 
            this.colHSQETeamLead.Caption = "HSQE Team Lead ID";
            this.colHSQETeamLead.FieldName = "HSQETeamLead";
            this.colHSQETeamLead.Name = "colHSQETeamLead";
            this.colHSQETeamLead.OptionsColumn.AllowEdit = false;
            this.colHSQETeamLead.OptionsColumn.AllowFocus = false;
            this.colHSQETeamLead.OptionsColumn.ReadOnly = true;
            this.colHSQETeamLead.Width = 117;
            // 
            // colCorrectiveActionRemarks
            // 
            this.colCorrectiveActionRemarks.Caption = "Corrective Actions";
            this.colCorrectiveActionRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colCorrectiveActionRemarks.FieldName = "CorrectiveActionRemarks";
            this.colCorrectiveActionRemarks.Name = "colCorrectiveActionRemarks";
            this.colCorrectiveActionRemarks.OptionsColumn.ReadOnly = true;
            this.colCorrectiveActionRemarks.Visible = true;
            this.colCorrectiveActionRemarks.VisibleIndex = 8;
            this.colCorrectiveActionRemarks.Width = 174;
            // 
            // colTargetClosureDate
            // 
            this.colTargetClosureDate.Caption = "Target Closure Date";
            this.colTargetClosureDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colTargetClosureDate.FieldName = "TargetClosureDate";
            this.colTargetClosureDate.Name = "colTargetClosureDate";
            this.colTargetClosureDate.OptionsColumn.AllowEdit = false;
            this.colTargetClosureDate.OptionsColumn.AllowFocus = false;
            this.colTargetClosureDate.OptionsColumn.ReadOnly = true;
            this.colTargetClosureDate.Visible = true;
            this.colTargetClosureDate.VisibleIndex = 11;
            this.colTargetClosureDate.Width = 118;
            // 
            // colClosedDate
            // 
            this.colClosedDate.Caption = "Closed Date";
            this.colClosedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClosedDate.FieldName = "ClosedDate";
            this.colClosedDate.Name = "colClosedDate";
            this.colClosedDate.OptionsColumn.AllowEdit = false;
            this.colClosedDate.OptionsColumn.AllowFocus = false;
            this.colClosedDate.OptionsColumn.ReadOnly = true;
            this.colClosedDate.Visible = true;
            this.colClosedDate.VisibleIndex = 12;
            this.colClosedDate.Width = 79;
            // 
            // colClosedByStaffID
            // 
            this.colClosedByStaffID.Caption = "Closed By Staff ID";
            this.colClosedByStaffID.FieldName = "ClosedByStaffID";
            this.colClosedByStaffID.Name = "colClosedByStaffID";
            this.colClosedByStaffID.OptionsColumn.AllowEdit = false;
            this.colClosedByStaffID.OptionsColumn.AllowFocus = false;
            this.colClosedByStaffID.OptionsColumn.ReadOnly = true;
            this.colClosedByStaffID.Width = 109;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 25;
            // 
            // colBusinessAreaName1
            // 
            this.colBusinessAreaName1.Caption = "Business Area";
            this.colBusinessAreaName1.FieldName = "BusinessAreaName";
            this.colBusinessAreaName1.Name = "colBusinessAreaName1";
            this.colBusinessAreaName1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName1.Visible = true;
            this.colBusinessAreaName1.VisibleIndex = 4;
            this.colBusinessAreaName1.Width = 98;
            // 
            // colAccidentType
            // 
            this.colAccidentType.Caption = "Type";
            this.colAccidentType.FieldName = "AccidentType";
            this.colAccidentType.Name = "colAccidentType";
            this.colAccidentType.OptionsColumn.AllowEdit = false;
            this.colAccidentType.OptionsColumn.AllowFocus = false;
            this.colAccidentType.OptionsColumn.ReadOnly = true;
            this.colAccidentType.Visible = true;
            this.colAccidentType.VisibleIndex = 1;
            this.colAccidentType.Width = 66;
            // 
            // colAccidentSubType
            // 
            this.colAccidentSubType.Caption = "Sub-Type";
            this.colAccidentSubType.FieldName = "AccidentSubType";
            this.colAccidentSubType.Name = "colAccidentSubType";
            this.colAccidentSubType.OptionsColumn.AllowEdit = false;
            this.colAccidentSubType.OptionsColumn.AllowFocus = false;
            this.colAccidentSubType.OptionsColumn.ReadOnly = true;
            this.colAccidentSubType.Visible = true;
            this.colAccidentSubType.VisibleIndex = 2;
            this.colAccidentSubType.Width = 115;
            // 
            // colAccidentStatus
            // 
            this.colAccidentStatus.Caption = "Status";
            this.colAccidentStatus.FieldName = "AccidentStatus";
            this.colAccidentStatus.Name = "colAccidentStatus";
            this.colAccidentStatus.OptionsColumn.AllowEdit = false;
            this.colAccidentStatus.OptionsColumn.AllowFocus = false;
            this.colAccidentStatus.OptionsColumn.ReadOnly = true;
            this.colAccidentStatus.Visible = true;
            this.colAccidentStatus.VisibleIndex = 3;
            this.colAccidentStatus.Width = 55;
            // 
            // colReportedByPerson
            // 
            this.colReportedByPerson.Caption = "Reported By Person";
            this.colReportedByPerson.FieldName = "ReportedByPerson";
            this.colReportedByPerson.Name = "colReportedByPerson";
            this.colReportedByPerson.OptionsColumn.AllowEdit = false;
            this.colReportedByPerson.OptionsColumn.AllowFocus = false;
            this.colReportedByPerson.OptionsColumn.ReadOnly = true;
            this.colReportedByPerson.Visible = true;
            this.colReportedByPerson.VisibleIndex = 13;
            this.colReportedByPerson.Width = 117;
            // 
            // colReportedByPersonType
            // 
            this.colReportedByPersonType.Caption = "Reported By Person Type";
            this.colReportedByPersonType.FieldName = "ReportedByPersonType";
            this.colReportedByPersonType.Name = "colReportedByPersonType";
            this.colReportedByPersonType.OptionsColumn.AllowEdit = false;
            this.colReportedByPersonType.OptionsColumn.AllowFocus = false;
            this.colReportedByPersonType.OptionsColumn.ReadOnly = true;
            this.colReportedByPersonType.Visible = true;
            this.colReportedByPersonType.VisibleIndex = 14;
            this.colReportedByPersonType.Width = 144;
            // 
            // colResponsibleManager
            // 
            this.colResponsibleManager.Caption = "Responsible Manager";
            this.colResponsibleManager.FieldName = "ResponsibleManager";
            this.colResponsibleManager.Name = "colResponsibleManager";
            this.colResponsibleManager.OptionsColumn.AllowEdit = false;
            this.colResponsibleManager.OptionsColumn.AllowFocus = false;
            this.colResponsibleManager.OptionsColumn.ReadOnly = true;
            this.colResponsibleManager.Visible = true;
            this.colResponsibleManager.VisibleIndex = 9;
            this.colResponsibleManager.Width = 123;
            // 
            // colLinkedToRecordFullDescription
            // 
            this.colLinkedToRecordFullDescription.Caption = "Linked To Record (Full)";
            this.colLinkedToRecordFullDescription.FieldName = "LinkedToRecordFullDescription";
            this.colLinkedToRecordFullDescription.Name = "colLinkedToRecordFullDescription";
            this.colLinkedToRecordFullDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordFullDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordFullDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordFullDescription.Visible = true;
            this.colLinkedToRecordFullDescription.VisibleIndex = 24;
            this.colLinkedToRecordFullDescription.Width = 161;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 18;
            this.colClientName.Width = 78;
            // 
            // colClientContract
            // 
            this.colClientContract.Caption = "Client Contract";
            this.colClientContract.FieldName = "ClientContract";
            this.colClientContract.Name = "colClientContract";
            this.colClientContract.OptionsColumn.AllowEdit = false;
            this.colClientContract.OptionsColumn.AllowFocus = false;
            this.colClientContract.OptionsColumn.ReadOnly = true;
            this.colClientContract.Visible = true;
            this.colClientContract.VisibleIndex = 19;
            this.colClientContract.Width = 93;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 20;
            // 
            // colLinkedToRecord
            // 
            this.colLinkedToRecord.Caption = "Linked To Record";
            this.colLinkedToRecord.FieldName = "LinkedToRecord";
            this.colLinkedToRecord.Name = "colLinkedToRecord";
            this.colLinkedToRecord.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecord.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecord.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecord.Visible = true;
            this.colLinkedToRecord.VisibleIndex = 21;
            this.colLinkedToRecord.Width = 103;
            // 
            // colLinkedToRecordType
            // 
            this.colLinkedToRecordType.Caption = "Linked To Record Type";
            this.colLinkedToRecordType.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType.Name = "colLinkedToRecordType";
            this.colLinkedToRecordType.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType.Visible = true;
            this.colLinkedToRecordType.VisibleIndex = 22;
            this.colLinkedToRecordType.Width = 130;
            // 
            // colClosedByStaff
            // 
            this.colClosedByStaff.Caption = "Closed By Staff";
            this.colClosedByStaff.FieldName = "ClosedByStaff";
            this.colClosedByStaff.Name = "colClosedByStaff";
            this.colClosedByStaff.OptionsColumn.AllowEdit = false;
            this.colClosedByStaff.OptionsColumn.AllowFocus = false;
            this.colClosedByStaff.OptionsColumn.ReadOnly = true;
            this.colClosedByStaff.Visible = true;
            this.colClosedByStaff.VisibleIndex = 23;
            this.colClosedByStaff.Width = 96;
            // 
            // colHSQETeamLeadName
            // 
            this.colHSQETeamLeadName.Caption = "HSQE Team Lead";
            this.colHSQETeamLeadName.FieldName = "HSQETeamLeadName";
            this.colHSQETeamLeadName.Name = "colHSQETeamLeadName";
            this.colHSQETeamLeadName.OptionsColumn.AllowEdit = false;
            this.colHSQETeamLeadName.OptionsColumn.AllowFocus = false;
            this.colHSQETeamLeadName.OptionsColumn.ReadOnly = true;
            this.colHSQETeamLeadName.Visible = true;
            this.colHSQETeamLeadName.VisibleIndex = 10;
            this.colHSQETeamLeadName.Width = 103;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(350, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(431, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemActive),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Custom 3";
            // 
            // barEditItemActive
            // 
            this.barEditItemActive.Caption = "Active Only:";
            this.barEditItemActive.Edit = this.repositoryItemCheckEdit2;
            this.barEditItemActive.EditValue = 1;
            this.barEditItemActive.EditWidth = 20;
            this.barEditItemActive.Id = 31;
            this.barEditItemActive.ItemAppearance.Hovered.BackColor = System.Drawing.Color.Transparent;
            this.barEditItemActive.ItemAppearance.Hovered.Options.UseBackColor = true;
            this.barEditItemActive.ItemAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.barEditItemActive.ItemAppearance.Normal.Options.UseBackColor = true;
            this.barEditItemActive.ItemAppearance.Pressed.BackColor = System.Drawing.Color.Transparent;
            this.barEditItemActive.ItemAppearance.Pressed.Options.UseBackColor = true;
            this.barEditItemActive.ItemInMenuAppearance.Hovered.BackColor = System.Drawing.Color.Transparent;
            this.barEditItemActive.ItemInMenuAppearance.Hovered.Options.UseBackColor = true;
            this.barEditItemActive.ItemInMenuAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.barEditItemActive.ItemInMenuAppearance.Normal.Options.UseBackColor = true;
            this.barEditItemActive.ItemInMenuAppearance.Pressed.BackColor = System.Drawing.Color.Transparent;
            this.barEditItemActive.ItemInMenuAppearance.Pressed.Options.UseBackColor = true;
            this.barEditItemActive.Name = "barEditItemActive";
            this.barEditItemActive.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit2.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit2.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit2.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "";
            this.repositoryItemCheckEdit2.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 32;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.LargeImage")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 25);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(517, 367);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // sp00314_Accident_SelectTableAdapter
            // 
            this.sp00314_Accident_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Accident_Select
            // 
            this.ClientSize = new System.Drawing.Size(518, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_Core_Accident_Select";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Accident";
            this.Load += new System.EventHandler(this.frm_Core_Accident_Select_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00314AccidentSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp00314AccidentSelectBindingSource;
        private DataSet_Accident dataSet_Accident;
        private DataSet_AccidentTableAdapters.sp00314_Accident_SelectTableAdapter sp00314_Accident_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPersonOther;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedToHSE;
        private DevExpress.XtraGrid.Columns.GridColumn colDateReportedToHSE;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSituationRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleManagerID;
        private DevExpress.XtraGrid.Columns.GridColumn colHSQETeamLead;
        private DevExpress.XtraGrid.Columns.GridColumn colCorrectiveActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTargetClosureDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName1;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentType;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleManager;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordFullDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContract;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedByStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colHSQETeamLeadName;
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frmBase_Modal : BaseObjects.frmBase
    {
        #region Instance Variables
        
        public DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager;
        public Boolean _KeepWaitFormOpen = true;

        #endregion

        public frmBase_Modal()
        {
            InitializeComponent();
        }

        private void bbiLoadLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmUserScreenSettingsLoad fChildForm = new frmUserScreenSettingsLoad(this);
            this.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                this.LoadedViewID = fChildForm.intLayoutID;
                this.LoadedViewCreatedByID = fChildForm.intLayoutCreatedByID;
                if (this.GlobalSettings.ShowConfirmations == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Screen Layout Loaded Successfully.", "Load Screen Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void bbiSaveLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intLoadedViewID = 0;
            int intLoadedViewCreatedByID = 0;
            frmBase fbBase = this;
            if (fbBase != null)
            {
                intLoadedViewID = fbBase.LoadedViewID;
                intLoadedViewCreatedByID = fbBase.LoadedViewCreatedByID;
                if (intLoadedViewID <= 0)  // Open Save As Form to get a description etc for the layout //
                {
                    frmUserScreenSettingsSave fChildForm = new frmUserScreenSettingsSave(fbBase);
                    this.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    if (fChildForm.ShowDialog() == DialogResult.Yes)
                    {
                        fbBase.LoadedViewID = fChildForm.intLayoutID;
                        fbBase.LoadedViewCreatedByID = fChildForm.intLayoutCreatedByID;
                    }
                }
                else
                {
                    if (intLoadedViewCreatedByID != this.GlobalSettings.UserID && this.GlobalSettings.PersonType != 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("You cannot save changes to the currently selected view as you did not create it.", "Save Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Ok to proceed so update saved layout. //
                    Load_Saved_Layout SaveLayout = new Load_Saved_Layout();
                    SaveLayout.Store_Screen_Object_Settings(this.GlobalSettings.ConnectionString, intLoadedViewID, fbBase, null);
                }
            }
        }

        private void bbiSaveLayoutAs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmBase fbBase = (frmBase)this;
            if (fbBase != null)
            {
                frmUserScreenSettingsSave fChildForm = new frmUserScreenSettingsSave(fbBase);
                this.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.Yes)
                {
                    fbBase.LoadedViewID = fChildForm.intLayoutID;
                    fbBase.LoadedViewCreatedByID = fChildForm.intLayoutCreatedByID;
                    if (this.GlobalSettings.ShowConfirmations == 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Screen Layout Saved.", "Save Screen Layout As", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }



    }
}


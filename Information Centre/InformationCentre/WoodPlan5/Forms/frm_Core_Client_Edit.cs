using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Core_Client_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        public int intMaxOrder = 0;
        private bool ibool_FormEditingCancelled = false;
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultImageFolderOM = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        
        public frm_OM_Tender_Edit frm_Tender_Edit_Screen_To_Update = null;  // Only used if called from the Edit_Tender screen so we can update the correct instance //
        
        #endregion

        public frm_Core_Client_Edit()
        {
            InitializeComponent();
        }

        private void frm_Core_Client_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 30011;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            strDefaultPath = Application.StartupPath;
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultImageFolderOM = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Picture path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Picture path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sp03088_EP_Utility_ARB_Permission_Document_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp03088_EP_Utility_ARB_Permission_Document_TypesTableAdapter.Fill(dataSet_EP_DataEntry.sp03088_EP_Utility_ARB_Permission_Document_Types);

            sp00227_Picklist_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00227_Picklist_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00227_Picklist_List_With_Blank, 158);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp05058_GC_Customer_Relationship_Depths_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp05058_GC_Customer_Relationship_Depths_With_BlankTableAdapter.Fill(dataSet_EP_DataEntry.sp05058_GC_Customer_Relationship_Depths_With_Blank);

            sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04035_GC_Self_Billing_Frequency_Descriptors_With_Blank);

            // Populate Main Dataset //
            sp03002_EP_Client_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp03002_EP_Client_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientCode"] = "";
                        drNewRow["ClientName"] = "";
                        drNewRow["ClientOrder"] = intMaxOrder + 1;
                        drNewRow["DailyGrittingReport"] = 0;
                        drNewRow["GrittingClientPONumberRequired"] = 0;
                        drNewRow["SnowClientPONumberRequired"] = 0;
                        drNewRow["GrittingEveningStartTime"] = Convert.ToDateTime("1900-01-01 20:00:00");
                        drNewRow["GrittingEveningEndTime"] = Convert.ToDateTime("1900-01-01 08:00:00");
                        drNewRow["IsActiveClient"] = 0;
                        drNewRow["UtilityArbPermissionDocumentTypeID"] = 0;
                        drNewRow["CustomerRelationshipDepthID"] = 0;
                        drNewRow["ImagesFolderOM"] = "";
                        drNewRow["ReceivesExternalGrittingEmail"] = 0;
                        drNewRow["IsSummerMaintenanceClient"] = 0;
                        drNewRow["IsWinterMaintenanceClient"] = 0;
                        drNewRow["IsAmenityArbClient"] = 0;
                        drNewRow["IsTest"] = 0;
                        this.dataSet_EP_DataEntry.sp03002_EP_Client_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_EP_DataEntry.sp03002_EP_Client_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientCode"] = "";
                        drNewRow["ClientName"] = "";
                        this.dataSet_EP_DataEntry.sp03002_EP_Client_Edit.Rows.Add(drNewRow);
                        this.dataSet_EP_DataEntry.sp03002_EP_Client_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp03002_EP_Client_EditTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03002_EP_Client_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_EP_DataEntry.sp03002_EP_Client_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Client", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientCodeTextEdit.Focus();

                        ClientCodeTextEdit.Properties.ReadOnly = false;
                        ClientNameTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientCodeTextEdit.Focus();

                        ClientCodeTextEdit.Properties.ReadOnly = true;
                        ClientNameTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientCodeTextEdit.Focus();

                        ClientCodeTextEdit.Properties.ReadOnly = false;
                        ClientNameTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientCodeTextEdit.Focus();

                        ClientCodeTextEdit.Properties.ReadOnly = true;
                        ClientNameTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_EP_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_Core_Client_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Client_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp03002EPClientEditBindingSource.EndEdit();
            try
            {
                this.sp03002_EP_Client_EditTableAdapter.Update(dataSet_EP_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRowView = (DataRowView)sp03002EPClientEditBindingSource.Current;
                var currentRow = (DataSet_EP_DataEntry.sp03002_EP_Client_EditRow)currentRowView.Row;
                if (currentRow != null) strNewIDs = currentRow.ClientID + ";";
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow.strMode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //

                    // See if there is a specific Edit Tender screen open we need to send this record's values to (only happens if the Edit Tender screen opened this screen) //
                    if (frm_Tender_Edit_Screen_To_Update != null)
                    {
                        try
                        {
                            frm_Tender_Edit_Screen_To_Update.SetTenderClientFromAddScreen(currentRow.ClientID, currentRow.ClientName);
                        }
                        catch (Exception) { }
                    }
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_Core_Client_Manager")
                    {
                        var fParentForm = (frm_Core_Client_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "", "", "", "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_EP_DataEntry.sp03002_EP_Client_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_EP_DataEntry.sp03002_EP_Client_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void ClientNameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ClientNameTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else if (this.strFormMode != "blockedit" && !string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                var invalidChars = Path.GetInvalidPathChars();
                string strText = te.EditValue.ToString();
                string invalidCharsRemoved = new string(strText.Where(x => !invalidChars.Contains(x)).ToArray());
                invalidCharsRemoved = invalidCharsRemoved.Replace("\\", "");
                invalidCharsRemoved = invalidCharsRemoved.Replace(".", "");

                if (invalidCharsRemoved != strText)
                {
                    dxErrorProvider1.SetError(ClientNameTextEdit, "Invalid characters entered - corrected value = " + invalidCharsRemoved);
                    e.Cancel = true;
                    return;
                }
                else
                {
                    dxErrorProvider1.SetError(ClientNameTextEdit, "");
                }
            }
            else
            {
                dxErrorProvider1.SetError(ClientNameTextEdit, "");
            }
        }

        private void ClientTypeIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 158, "Client Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00227_Picklist_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00227_Picklist_List_With_Blank, 158);  // 158 = Client Types //
                }
            }
        }

        #endregion


        private void DailyGrittingEmailCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                DailyGrittingReportCheckEdit.Checked = false;
            }
            else
            {
                DailyGrittingEmailShowUnGrittedCheckEdit.Checked = false;
            }
        }

        private void DailyGrittingEmailShowUnGrittedCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                DailyGrittingEmailCheckEdit.Checked = true;
                DailyGrittingReportCheckEdit.Checked = false;
            }
        }

        private void DailyGrittingReportCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                DailyGrittingEmailCheckEdit.Checked = false;
                DailyGrittingEmailShowUnGrittedCheckEdit.Checked = false;
            }
         }

        private void WinterMaintInvoiceIgnoreSSColumnsButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Site Button //
            {
                DataRowView currentRow = (DataRowView)sp03002EPClientEditBindingSource.Current;
                if (currentRow != null)
                {
                    frm_GC_Select_Client_Export_Skip_Columns fChildForm = new frm_GC_Select_Client_Export_Skip_Columns();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strPassedInColumns = currentRow["WinterMaintInvoiceIgnoreSSColumns"].ToString();
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                    {
                        currentRow["WinterMaintInvoiceIgnoreSSColumns"] = fChildForm.strSelectedColumns;
                    }
                }
            }
       }

        private void HeaderImageButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView currentRow = null;
            switch (e.Button.Tag.ToString())
            {
                case "select file":
                    {
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "png";
                            dlg.Filter = "PNG Files (*.png)|*.png|JPEG Files (*.jpeg)|*.jpeg|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif|BMP Files (*.bmp)|*.bmp";
                            if (strDefaultPath != "") dlg.InitialDirectory = strDefaultImageFolderOM;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strDefaultPath != "")
                                        if (!filename.ToLower().StartsWith(strDefaultPath.ToLower()))
                                        {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the application startup folder or one of it's sub-folders.\n\nFile Selection Aborted!", "Select Image File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    strTempFileName = filename.Substring(strDefaultPath.Length);
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                }
                                HeaderImageButtonEdit.Text = strTempResult;
                            }
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter so linked file can be viewed if required //
                        }
                    }
                    break;
                case "view file":
                    {
                        currentRow = (DataRowView)sp03002EPClientEditBindingSource.Current;
                        if (currentRow == null) return;
                        string strFile = currentRow["HeaderImage"].ToString();
                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No Image Linked - select the image first before proceeding.", "View Linked Image", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            System.Diagnostics.Process.Start(strDefaultPath + strFile);
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strDefaultPath + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Image", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
            }
        }

        private void ImagesFolderOMButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString())
            {
                case "choose":
                    {
                        if (strFormMode == "view") return;
                        ButtonEdit edit = sender as ButtonEdit;
                        using (FolderBrowserDialog fbd = new FolderBrowserDialog())
                        {
                            fbd.SelectedPath = (string.IsNullOrWhiteSpace(edit.EditValue.ToString()) ? strDefaultImageFolderOM : edit.EditValue.ToString());
                            fbd.Description = "Select a Folder then click OK.";
                            fbd.ShowNewFolderButton = false;
                            if (fbd.ShowDialog() == DialogResult.OK)
                            {
                                if (strDefaultImageFolderOM != "")
                                {
                                    if (!fbd.SelectedPath.ToLower().StartsWith(strDefaultImageFolderOM.ToLower()))
                                    {
                                        DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Default Linked Pictures Folder] or one of it's sub-folders.\n\nFolder Selection Aborted!", "Select Folder", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        break;
                                    }

                                }
                                string strValue = fbd.SelectedPath.Substring(strDefaultImageFolderOM.Length);
                                if (strValue.StartsWith("\\")) strValue = strValue.Substring(1); // get rid of preceeding backslash //
                                edit.EditValue = strValue;
                            }
                        }
                    }
                    break;
                case "create":
                    {
                        if (strFormMode == "view") return;
                        TextEdit teClientName = (TextEdit)ClientNameTextEdit;
                        if (string.IsNullOrWhiteSpace(teClientName.EditValue.ToString()))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Ensure the Client Name has been entered first then try again.\n\nFolder Creation Aborted!", "Create Folder based on Client Name", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        ButtonEdit beImagesFolderOM = (ButtonEdit)sender;
                        string strPath = Path.Combine(strDefaultImageFolderOM, beImagesFolderOM.EditValue.ToString(), teClientName.EditValue.ToString());
                        try
                        {
                            if (!Directory.Exists(strPath)) Directory.CreateDirectory(strPath);
                            
                            // Create Signatures Sub-Folder //
                            strPath = Path.Combine(strDefaultImageFolderOM, beImagesFolderOM.EditValue.ToString(), teClientName.EditValue.ToString() + "\\Signatures");
                            if (!Directory.Exists(strPath)) Directory.CreateDirectory(strPath);
                            
                            beImagesFolderOM.EditValue = Path.Combine(beImagesFolderOM.EditValue.ToString(), teClientName.EditValue.ToString());  // Store the Folder in the box on screen //
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to create the folder...\n\nError: " + ex.Message + "\n\nTry again. if the problem persists please contact Technical Support.", "Create Folder based on Client Name", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    break;
            }

        }






    }
}


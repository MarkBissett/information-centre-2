namespace WoodPlan5
{
    partial class frm_Core_Client_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Client_Edit));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.IsTestCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.sp03002EPClientEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.IsAmenityArbClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsWinterMaintenanceClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsSummerMaintenanceClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ReceivesExternalGrittingEmailCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DepartmentCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinanceCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ImagesFolderOMButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.HeaderImageButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03088EPUtilityARBPermissionDocumentTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IsUtilityArbClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.CustomerRelationshipDepthIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp05058GCCustomerRelationshipDepthsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IsActiveClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.EmailPasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WinterMaintInvoiceFrequencySpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WinterMaintLastInvoiceDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SnowClientPONumberRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GrittingClientPONumberRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DailyGrittingEmailShowUnGrittedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DailyGrittingEmailCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GrittingEmailFromAddressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DailyGrittingReportCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ClientTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00227PicklistListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GrittingEveningStartTimeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.GrittingEveningEndTimeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDailyGrittingEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingEmailFromAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingClientPONumberRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDailyGrittingEmailShowUnGritted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDailyGrittingReport = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReceivesExternalGrittingEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForGrittingEveningStartTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingEveningEndTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSnowClientPONumberRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForWinterMaintLastInvoiceDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWinterMaintInvoiceFrequency = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWinterMaintInvoiceIgnoreSSColumns = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWinterMaintInvoiceFrequencyDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinanceCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDepartmentCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForImagesFolderOM = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForHeaderImage = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUtilityArbPermissionDocumentTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIsUtilityArbClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIsAmenityArbClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsActiveClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsWinterMaintenanceClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsSummerMaintenanceClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsTest = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerRelationshipDepthID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFormClientOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmailPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp03002_EP_Client_EditTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03002_EP_Client_EditTableAdapter();
            this.sp00227_Picklist_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00227_Picklist_List_With_BlankTableAdapter();
            this.sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter();
            this.sp05058_GC_Customer_Relationship_Depths_With_BlankTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp05058_GC_Customer_Relationship_Depths_With_BlankTableAdapter();
            this.sp03088_EP_Utility_ARB_Permission_Document_TypesTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03088_EP_Utility_ARB_Permission_Document_TypesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IsTestCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03002EPClientEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsAmenityArbClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsWinterMaintenanceClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSummerMaintenanceClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceivesExternalGrittingEmailCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagesFolderOMButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderImageButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03088EPUtilityARBPermissionDocumentTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsUtilityArbClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerRelationshipDepthIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05058GCCustomerRelationshipDepthsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsActiveClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailPasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintInvoiceFrequencySpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintLastInvoiceDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintLastInvoiceDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClientPONumberRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingClientPONumberRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DailyGrittingEmailShowUnGrittedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DailyGrittingEmailCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEmailFromAddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DailyGrittingReportCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00227PicklistListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEveningStartTimeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEveningEndTimeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDailyGrittingEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingEmailFromAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingClientPONumberRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDailyGrittingEmailShowUnGritted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDailyGrittingReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReceivesExternalGrittingEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingEveningStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingEveningEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClientPONumberRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintLastInvoiceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintInvoiceFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintInvoiceIgnoreSSColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintInvoiceFrequencyDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForImagesFolderOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeaderImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArbPermissionDocumentTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsUtilityArbClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsAmenityArbClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsActiveClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsWinterMaintenanceClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSummerMaintenanceClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerRelationshipDepthID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormClientOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(752, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 617);
            this.barDockControlBottom.Size = new System.Drawing.Size(752, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 591);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(752, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 591);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colID2
            // 
            this.colID2.Caption = "ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            // 
            // colValue1
            // 
            this.colValue1.Caption = "Descriptor ID";
            this.colValue1.FieldName = "Value";
            this.colValue1.Name = "colValue1";
            this.colValue1.OptionsColumn.AllowEdit = false;
            this.colValue1.OptionsColumn.AllowFocus = false;
            this.colValue1.OptionsColumn.ReadOnly = true;
            this.colValue1.Width = 96;
            // 
            // colID
            // 
            this.colID.Caption = "Item ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(752, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 617);
            this.barDockControl2.Size = new System.Drawing.Size(752, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 591);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(752, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 591);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.IsTestCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsAmenityArbClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsWinterMaintenanceClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsSummerMaintenanceClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ReceivesExternalGrittingEmailCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DepartmentCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinanceCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ImagesFolderOMButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.HeaderImageButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.IsUtilityArbClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomerRelationshipDepthIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.IsActiveClientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailPasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.WinterMaintInvoiceFrequencySpinEdit);
            this.dataLayoutControl1.Controls.Add(this.WinterMaintLastInvoiceDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SnowClientPONumberRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingClientPONumberRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DailyGrittingEmailShowUnGrittedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DailyGrittingEmailCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingEmailFromAddressTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DailyGrittingReportCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingEveningStartTimeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingEveningEndTimeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp03002EPClientEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1168, 193, 605, 473);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AlignInGroups;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(752, 591);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // IsTestCheckEdit
            // 
            this.IsTestCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "IsTest", true));
            this.IsTestCheckEdit.Location = new System.Drawing.Point(635, 92);
            this.IsTestCheckEdit.MenuManager = this.barManager1;
            this.IsTestCheckEdit.Name = "IsTestCheckEdit";
            this.IsTestCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsTestCheckEdit.Properties.ValueChecked = 1;
            this.IsTestCheckEdit.Properties.ValueUnchecked = 0;
            this.IsTestCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.IsTestCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsTestCheckEdit.TabIndex = 30;
            // 
            // sp03002EPClientEditBindingSource
            // 
            this.sp03002EPClientEditBindingSource.DataMember = "sp03002_EP_Client_Edit";
            this.sp03002EPClientEditBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // IsAmenityArbClientCheckEdit
            // 
            this.IsAmenityArbClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "IsAmenityArbClient", true));
            this.IsAmenityArbClientCheckEdit.Location = new System.Drawing.Point(635, 171);
            this.IsAmenityArbClientCheckEdit.MenuManager = this.barManager1;
            this.IsAmenityArbClientCheckEdit.Name = "IsAmenityArbClientCheckEdit";
            this.IsAmenityArbClientCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsAmenityArbClientCheckEdit.Properties.ValueChecked = 1;
            this.IsAmenityArbClientCheckEdit.Properties.ValueUnchecked = 0;
            this.IsAmenityArbClientCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.IsAmenityArbClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsAmenityArbClientCheckEdit.TabIndex = 27;
            // 
            // IsWinterMaintenanceClientCheckEdit
            // 
            this.IsWinterMaintenanceClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "IsWinterMaintenanceClient", true));
            this.IsWinterMaintenanceClientCheckEdit.Location = new System.Drawing.Point(635, 125);
            this.IsWinterMaintenanceClientCheckEdit.MenuManager = this.barManager1;
            this.IsWinterMaintenanceClientCheckEdit.Name = "IsWinterMaintenanceClientCheckEdit";
            this.IsWinterMaintenanceClientCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsWinterMaintenanceClientCheckEdit.Properties.ValueChecked = 1;
            this.IsWinterMaintenanceClientCheckEdit.Properties.ValueUnchecked = 0;
            this.IsWinterMaintenanceClientCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.IsWinterMaintenanceClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsWinterMaintenanceClientCheckEdit.TabIndex = 26;
            // 
            // IsSummerMaintenanceClientCheckEdit
            // 
            this.IsSummerMaintenanceClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "IsSummerMaintenanceClient", true));
            this.IsSummerMaintenanceClientCheckEdit.Location = new System.Drawing.Point(635, 148);
            this.IsSummerMaintenanceClientCheckEdit.MenuManager = this.barManager1;
            this.IsSummerMaintenanceClientCheckEdit.Name = "IsSummerMaintenanceClientCheckEdit";
            this.IsSummerMaintenanceClientCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsSummerMaintenanceClientCheckEdit.Properties.ValueChecked = 1;
            this.IsSummerMaintenanceClientCheckEdit.Properties.ValueUnchecked = 0;
            this.IsSummerMaintenanceClientCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.IsSummerMaintenanceClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsSummerMaintenanceClientCheckEdit.TabIndex = 26;
            // 
            // ReceivesExternalGrittingEmailCheckEdit
            // 
            this.ReceivesExternalGrittingEmailCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "ReceivesExternalGrittingEmail", true));
            this.ReceivesExternalGrittingEmailCheckEdit.Location = new System.Drawing.Point(241, 412);
            this.ReceivesExternalGrittingEmailCheckEdit.MenuManager = this.barManager1;
            this.ReceivesExternalGrittingEmailCheckEdit.Name = "ReceivesExternalGrittingEmailCheckEdit";
            this.ReceivesExternalGrittingEmailCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ReceivesExternalGrittingEmailCheckEdit.Properties.ValueChecked = 1;
            this.ReceivesExternalGrittingEmailCheckEdit.Properties.ValueUnchecked = 0;
            this.ReceivesExternalGrittingEmailCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.ReceivesExternalGrittingEmailCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReceivesExternalGrittingEmailCheckEdit.TabIndex = 24;
            this.ReceivesExternalGrittingEmailCheckEdit.ToolTip = "Used By Tender Register";
            // 
            // DepartmentCodeTextEdit
            // 
            this.DepartmentCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "DepartmentCode", true));
            this.DepartmentCodeTextEdit.Location = new System.Drawing.Point(233, 796);
            this.DepartmentCodeTextEdit.MenuManager = this.barManager1;
            this.DepartmentCodeTextEdit.Name = "DepartmentCodeTextEdit";
            this.DepartmentCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DepartmentCodeTextEdit, true);
            this.DepartmentCodeTextEdit.Size = new System.Drawing.Size(454, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DepartmentCodeTextEdit, optionsSpelling1);
            this.DepartmentCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.DepartmentCodeTextEdit.TabIndex = 29;
            // 
            // FinanceCodeTextEdit
            // 
            this.FinanceCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "FinanceCode", true));
            this.FinanceCodeTextEdit.Location = new System.Drawing.Point(233, 772);
            this.FinanceCodeTextEdit.MenuManager = this.barManager1;
            this.FinanceCodeTextEdit.Name = "FinanceCodeTextEdit";
            this.FinanceCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinanceCodeTextEdit, true);
            this.FinanceCodeTextEdit.Size = new System.Drawing.Size(454, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinanceCodeTextEdit, optionsSpelling2);
            this.FinanceCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinanceCodeTextEdit.TabIndex = 28;
            // 
            // ImagesFolderOMButtonEdit
            // 
            this.ImagesFolderOMButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "ImagesFolderOM", true));
            this.ImagesFolderOMButtonEdit.Location = new System.Drawing.Point(144, 309);
            this.ImagesFolderOMButtonEdit.MenuManager = this.barManager1;
            this.ImagesFolderOMButtonEdit.Name = "ImagesFolderOMButtonEdit";
            this.ImagesFolderOMButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose Folder", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Select Folder", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Create Folder", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me To Create Folder in current location based upon Client Name", "create", null, true)});
            this.ImagesFolderOMButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ImagesFolderOMButtonEdit.Size = new System.Drawing.Size(555, 20);
            this.ImagesFolderOMButtonEdit.StyleController = this.dataLayoutControl1;
            this.ImagesFolderOMButtonEdit.TabIndex = 27;
            this.ImagesFolderOMButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ImagesFolderOMButtonEdit_ButtonClick);
            // 
            // HeaderImageButtonEdit
            // 
            this.HeaderImageButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "HeaderImage", true));
            this.HeaderImageButtonEdit.Location = new System.Drawing.Point(198, 333);
            this.HeaderImageButtonEdit.MenuManager = this.barManager1;
            this.HeaderImageButtonEdit.Name = "HeaderImageButtonEdit";
            this.HeaderImageButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose File", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleLeft, ((System.Drawing.Image)(resources.GetObject("HeaderImageButtonEdit.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Select File", "select file", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View File", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "view file", null, true)});
            this.HeaderImageButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.HeaderImageButtonEdit.Size = new System.Drawing.Size(501, 22);
            this.HeaderImageButtonEdit.StyleController = this.dataLayoutControl1;
            this.HeaderImageButtonEdit.TabIndex = 13;
            this.HeaderImageButtonEdit.TabStop = false;
            this.HeaderImageButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.HeaderImageButtonEdit_ButtonClick);
            // 
            // UtilityArbPermissionDocumentTypeIDGridLookUpEdit
            // 
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "UtilityArbPermissionDocumentTypeID", true));
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Location = new System.Drawing.Point(198, 309);
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Name = "UtilityArbPermissionDocumentTypeIDGridLookUpEdit";
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Properties.DataSource = this.sp03088EPUtilityARBPermissionDocumentTypesBindingSource;
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Properties.NullText = "";
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Properties.View = this.gridView3;
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Size = new System.Drawing.Size(501, 20);
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.TabIndex = 26;
            // 
            // sp03088EPUtilityARBPermissionDocumentTypesBindingSource
            // 
            this.sp03088EPUtilityARBPermissionDocumentTypesBindingSource.DataMember = "sp03088_EP_Utility_ARB_Permission_Document_Types";
            this.sp03088EPUtilityARBPermissionDocumentTypesBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.gridColumn1});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID2;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Permission Document Type";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 201;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Order";
            this.gridColumn1.FieldName = "RecordOrder";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // IsUtilityArbClientCheckEdit
            // 
            this.IsUtilityArbClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "IsUtilityArbClient", true));
            this.IsUtilityArbClientCheckEdit.Location = new System.Drawing.Point(635, 194);
            this.IsUtilityArbClientCheckEdit.MenuManager = this.barManager1;
            this.IsUtilityArbClientCheckEdit.Name = "IsUtilityArbClientCheckEdit";
            this.IsUtilityArbClientCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsUtilityArbClientCheckEdit.Properties.ValueChecked = 1;
            this.IsUtilityArbClientCheckEdit.Properties.ValueUnchecked = 0;
            this.IsUtilityArbClientCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.IsUtilityArbClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsUtilityArbClientCheckEdit.TabIndex = 25;
            // 
            // CustomerRelationshipDepthIDGridLookUpEdit
            // 
            this.CustomerRelationshipDepthIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "CustomerRelationshipDepthID", true));
            this.CustomerRelationshipDepthIDGridLookUpEdit.Location = new System.Drawing.Point(139, 109);
            this.CustomerRelationshipDepthIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CustomerRelationshipDepthIDGridLookUpEdit.Name = "CustomerRelationshipDepthIDGridLookUpEdit";
            this.CustomerRelationshipDepthIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CustomerRelationshipDepthIDGridLookUpEdit.Properties.DataSource = this.sp05058GCCustomerRelationshipDepthsWithBlankBindingSource;
            this.CustomerRelationshipDepthIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.CustomerRelationshipDepthIDGridLookUpEdit.Properties.NullText = "";
            this.CustomerRelationshipDepthIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.CustomerRelationshipDepthIDGridLookUpEdit.Properties.View = this.gridView1;
            this.CustomerRelationshipDepthIDGridLookUpEdit.Size = new System.Drawing.Size(341, 20);
            this.CustomerRelationshipDepthIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CustomerRelationshipDepthIDGridLookUpEdit.TabIndex = 24;
            this.CustomerRelationshipDepthIDGridLookUpEdit.ToolTip = "Used By Tender Register";
            // 
            // sp05058GCCustomerRelationshipDepthsWithBlankBindingSource
            // 
            this.sp05058GCCustomerRelationshipDepthsWithBlankBindingSource.DataMember = "sp05058_GC_Customer_Relationship_Depths_With_Blank";
            this.sp05058GCCustomerRelationshipDepthsWithBlankBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colRecordOrder,
            this.colDescription1});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colID1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Description";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 315;
            // 
            // IsActiveClientCheckEdit
            // 
            this.IsActiveClientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "IsActiveClient", true));
            this.IsActiveClientCheckEdit.Location = new System.Drawing.Point(635, 69);
            this.IsActiveClientCheckEdit.MenuManager = this.barManager1;
            this.IsActiveClientCheckEdit.Name = "IsActiveClientCheckEdit";
            this.IsActiveClientCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsActiveClientCheckEdit.Properties.ValueChecked = 1;
            this.IsActiveClientCheckEdit.Properties.ValueUnchecked = 0;
            this.IsActiveClientCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.IsActiveClientCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsActiveClientCheckEdit.TabIndex = 23;
            this.IsActiveClientCheckEdit.ToolTip = "Used By Tender Register";
            // 
            // EmailPasswordTextEdit
            // 
            this.EmailPasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "EmailPassword", true));
            this.EmailPasswordTextEdit.Location = new System.Drawing.Point(139, 157);
            this.EmailPasswordTextEdit.MenuManager = this.barManager1;
            this.EmailPasswordTextEdit.Name = "EmailPasswordTextEdit";
            this.EmailPasswordTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmailPasswordTextEdit, true);
            this.EmailPasswordTextEdit.Size = new System.Drawing.Size(341, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmailPasswordTextEdit, optionsSpelling3);
            this.EmailPasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailPasswordTextEdit.TabIndex = 22;
            // 
            // WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit
            // 
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "WinterMaintInvoiceFrequencyDescriptorID", true));
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(233, 713);
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Name = "WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit";
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource;
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Descriptor";
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Properties.ValueMember = "Value";
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Properties.View = this.gridView2;
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(155, 20);
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.TabIndex = 20;
            // 
            // sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource
            // 
            this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource.DataMember = "sp04035_GC_Self_Billing_Frequency_Descriptors_With_Blank";
            this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colValue1,
            this.colDescriptor,
            this.colOrder});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colValue1;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescriptor
            // 
            this.colDescriptor.Caption = "Frequency Descriptor";
            this.colDescriptor.FieldName = "Descriptor";
            this.colDescriptor.Name = "colDescriptor";
            this.colDescriptor.OptionsColumn.AllowEdit = false;
            this.colDescriptor.OptionsColumn.AllowFocus = false;
            this.colDescriptor.OptionsColumn.ReadOnly = true;
            this.colDescriptor.Visible = true;
            this.colDescriptor.VisibleIndex = 0;
            this.colDescriptor.Width = 169;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // WinterMaintInvoiceFrequencySpinEdit
            // 
            this.WinterMaintInvoiceFrequencySpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "WinterMaintInvoiceFrequency", true));
            this.WinterMaintInvoiceFrequencySpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WinterMaintInvoiceFrequencySpinEdit.Location = new System.Drawing.Point(233, 689);
            this.WinterMaintInvoiceFrequencySpinEdit.MenuManager = this.barManager1;
            this.WinterMaintInvoiceFrequencySpinEdit.Name = "WinterMaintInvoiceFrequencySpinEdit";
            this.WinterMaintInvoiceFrequencySpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.WinterMaintInvoiceFrequencySpinEdit.Properties.IsFloatValue = false;
            this.WinterMaintInvoiceFrequencySpinEdit.Properties.Mask.EditMask = "N00";
            this.WinterMaintInvoiceFrequencySpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WinterMaintInvoiceFrequencySpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.WinterMaintInvoiceFrequencySpinEdit.Size = new System.Drawing.Size(155, 20);
            this.WinterMaintInvoiceFrequencySpinEdit.StyleController = this.dataLayoutControl1;
            this.WinterMaintInvoiceFrequencySpinEdit.TabIndex = 20;
            // 
            // WinterMaintLastInvoiceDateDateEdit
            // 
            this.WinterMaintLastInvoiceDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "WinterMaintLastInvoiceDate", true));
            this.WinterMaintLastInvoiceDateDateEdit.EditValue = null;
            this.WinterMaintLastInvoiceDateDateEdit.Location = new System.Drawing.Point(233, 665);
            this.WinterMaintLastInvoiceDateDateEdit.MenuManager = this.barManager1;
            this.WinterMaintLastInvoiceDateDateEdit.Name = "WinterMaintLastInvoiceDateDateEdit";
            this.WinterMaintLastInvoiceDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WinterMaintLastInvoiceDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.WinterMaintLastInvoiceDateDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.WinterMaintLastInvoiceDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.WinterMaintLastInvoiceDateDateEdit.Size = new System.Drawing.Size(454, 20);
            this.WinterMaintLastInvoiceDateDateEdit.StyleController = this.dataLayoutControl1;
            this.WinterMaintLastInvoiceDateDateEdit.TabIndex = 19;
            // 
            // SnowClientPONumberRequiredCheckEdit
            // 
            this.SnowClientPONumberRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "SnowClientPONumberRequired", true));
            this.SnowClientPONumberRequiredCheckEdit.Location = new System.Drawing.Point(230, 586);
            this.SnowClientPONumberRequiredCheckEdit.MenuManager = this.barManager1;
            this.SnowClientPONumberRequiredCheckEdit.Name = "SnowClientPONumberRequiredCheckEdit";
            this.SnowClientPONumberRequiredCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SnowClientPONumberRequiredCheckEdit.Properties.ValueChecked = 1;
            this.SnowClientPONumberRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.SnowClientPONumberRequiredCheckEdit.Size = new System.Drawing.Size(457, 19);
            this.SnowClientPONumberRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.SnowClientPONumberRequiredCheckEdit.TabIndex = 16;
            // 
            // GrittingClientPONumberRequiredCheckEdit
            // 
            this.GrittingClientPONumberRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "GrittingClientPONumberRequired", true));
            this.GrittingClientPONumberRequiredCheckEdit.Location = new System.Drawing.Point(241, 459);
            this.GrittingClientPONumberRequiredCheckEdit.MenuManager = this.barManager1;
            this.GrittingClientPONumberRequiredCheckEdit.Name = "GrittingClientPONumberRequiredCheckEdit";
            this.GrittingClientPONumberRequiredCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GrittingClientPONumberRequiredCheckEdit.Properties.ValueChecked = 1;
            this.GrittingClientPONumberRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.GrittingClientPONumberRequiredCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.GrittingClientPONumberRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.GrittingClientPONumberRequiredCheckEdit.TabIndex = 15;
            // 
            // DailyGrittingEmailShowUnGrittedCheckEdit
            // 
            this.DailyGrittingEmailShowUnGrittedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "DailyGrittingEmailShowUnGritted", true));
            this.DailyGrittingEmailShowUnGrittedCheckEdit.Location = new System.Drawing.Point(241, 366);
            this.DailyGrittingEmailShowUnGrittedCheckEdit.MenuManager = this.barManager1;
            this.DailyGrittingEmailShowUnGrittedCheckEdit.Name = "DailyGrittingEmailShowUnGrittedCheckEdit";
            this.DailyGrittingEmailShowUnGrittedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DailyGrittingEmailShowUnGrittedCheckEdit.Properties.ValueChecked = 1;
            this.DailyGrittingEmailShowUnGrittedCheckEdit.Properties.ValueUnchecked = 0;
            this.DailyGrittingEmailShowUnGrittedCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.DailyGrittingEmailShowUnGrittedCheckEdit.StyleController = this.dataLayoutControl1;
            this.DailyGrittingEmailShowUnGrittedCheckEdit.TabIndex = 14;
            this.DailyGrittingEmailShowUnGrittedCheckEdit.CheckedChanged += new System.EventHandler(this.DailyGrittingEmailShowUnGrittedCheckEdit_CheckedChanged);
            // 
            // DailyGrittingEmailCheckEdit
            // 
            this.DailyGrittingEmailCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "DailyGrittingEmail", true));
            this.DailyGrittingEmailCheckEdit.Location = new System.Drawing.Point(241, 343);
            this.DailyGrittingEmailCheckEdit.MenuManager = this.barManager1;
            this.DailyGrittingEmailCheckEdit.Name = "DailyGrittingEmailCheckEdit";
            this.DailyGrittingEmailCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DailyGrittingEmailCheckEdit.Properties.ValueChecked = 1;
            this.DailyGrittingEmailCheckEdit.Properties.ValueUnchecked = 0;
            this.DailyGrittingEmailCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.DailyGrittingEmailCheckEdit.StyleController = this.dataLayoutControl1;
            this.DailyGrittingEmailCheckEdit.TabIndex = 13;
            this.DailyGrittingEmailCheckEdit.CheckedChanged += new System.EventHandler(this.DailyGrittingEmailCheckEdit_CheckedChanged);
            // 
            // GrittingEmailFromAddressTextEdit
            // 
            this.GrittingEmailFromAddressTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "GrittingEmailFromAddress", true));
            this.GrittingEmailFromAddressTextEdit.Location = new System.Drawing.Point(241, 435);
            this.GrittingEmailFromAddressTextEdit.MenuManager = this.barManager1;
            this.GrittingEmailFromAddressTextEdit.Name = "GrittingEmailFromAddressTextEdit";
            this.GrittingEmailFromAddressTextEdit.Properties.Mask.EditMask = "([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+" +
    "))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)";
            this.GrittingEmailFromAddressTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.GrittingEmailFromAddressTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GrittingEmailFromAddressTextEdit, true);
            this.GrittingEmailFromAddressTextEdit.Size = new System.Drawing.Size(446, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GrittingEmailFromAddressTextEdit, optionsSpelling4);
            this.GrittingEmailFromAddressTextEdit.StyleController = this.dataLayoutControl1;
            this.GrittingEmailFromAddressTextEdit.TabIndex = 12;
            // 
            // DailyGrittingReportCheckEdit
            // 
            this.DailyGrittingReportCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "DailyGrittingReport", true));
            this.DailyGrittingReportCheckEdit.Location = new System.Drawing.Point(241, 389);
            this.DailyGrittingReportCheckEdit.MenuManager = this.barManager1;
            this.DailyGrittingReportCheckEdit.Name = "DailyGrittingReportCheckEdit";
            this.DailyGrittingReportCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DailyGrittingReportCheckEdit.Properties.ValueChecked = 1;
            this.DailyGrittingReportCheckEdit.Properties.ValueUnchecked = 0;
            this.DailyGrittingReportCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.DailyGrittingReportCheckEdit.StyleController = this.dataLayoutControl1;
            this.DailyGrittingReportCheckEdit.TabIndex = 11;
            this.DailyGrittingReportCheckEdit.CheckedChanged += new System.EventHandler(this.DailyGrittingReportCheckEdit_CheckedChanged);
            // 
            // ClientOrderSpinEdit
            // 
            this.ClientOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "ClientOrder", true));
            this.ClientOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientOrderSpinEdit.Location = new System.Drawing.Point(139, 133);
            this.ClientOrderSpinEdit.MenuManager = this.barManager1;
            this.ClientOrderSpinEdit.Name = "ClientOrderSpinEdit";
            this.ClientOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ClientOrderSpinEdit.Properties.IsFloatValue = false;
            this.ClientOrderSpinEdit.Properties.Mask.EditMask = "N00";
            this.ClientOrderSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.ClientOrderSpinEdit.Size = new System.Drawing.Size(341, 20);
            this.ClientOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.ClientOrderSpinEdit.TabIndex = 10;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp03002EPClientEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(139, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 9;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(77, 12);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(539, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling5);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 4;
            // 
            // ClientCodeTextEdit
            // 
            this.ClientCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "ClientCode", true));
            this.ClientCodeTextEdit.Location = new System.Drawing.Point(139, 59);
            this.ClientCodeTextEdit.MenuManager = this.barManager1;
            this.ClientCodeTextEdit.Name = "ClientCodeTextEdit";
            this.ClientCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientCodeTextEdit, true);
            this.ClientCodeTextEdit.Size = new System.Drawing.Size(341, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientCodeTextEdit, optionsSpelling6);
            this.ClientCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientCodeTextEdit.TabIndex = 5;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(139, 35);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(341, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling7);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 6;
            this.ClientNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientNameTextEdit_Validating);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 309);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(663, 519);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling8);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 8;
            // 
            // ClientTypeIDGridLookUpEdit
            // 
            this.ClientTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "ClientTypeID", true));
            this.ClientTypeIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientTypeIDGridLookUpEdit.Location = new System.Drawing.Point(139, 83);
            this.ClientTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ClientTypeIDGridLookUpEdit.Name = "ClientTypeIDGridLookUpEdit";
            this.ClientTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Edit Underlying Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Reload Underlying Data", "reload", null, true)});
            this.ClientTypeIDGridLookUpEdit.Properties.DataSource = this.sp00227PicklistListWithBlankBindingSource;
            this.ClientTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ClientTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ClientTypeIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.ClientTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ClientTypeIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.ClientTypeIDGridLookUpEdit.Size = new System.Drawing.Size(341, 22);
            this.ClientTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ClientTypeIDGridLookUpEdit.TabIndex = 7;
            this.ClientTypeIDGridLookUpEdit.Tag = "158";
            this.ClientTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientTypeIDGridLookUpEdit_ButtonClick);
            // 
            // sp00227PicklistListWithBlankBindingSource
            // 
            this.sp00227PicklistListWithBlankBindingSource.DataMember = "sp00227_Picklist_List_With_Blank";
            this.sp00227PicklistListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colintOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colID;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colintOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 303;
            // 
            // colintOrder
            // 
            this.colintOrder.Caption = "Order";
            this.colintOrder.FieldName = "intOrder";
            this.colintOrder.Name = "colintOrder";
            this.colintOrder.OptionsColumn.AllowEdit = false;
            this.colintOrder.OptionsColumn.AllowFocus = false;
            this.colintOrder.OptionsColumn.ReadOnly = true;
            this.colintOrder.Width = 149;
            // 
            // GrittingEveningStartTimeTextEdit
            // 
            this.GrittingEveningStartTimeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "GrittingEveningStartTime", true));
            this.GrittingEveningStartTimeTextEdit.Location = new System.Drawing.Point(241, 482);
            this.GrittingEveningStartTimeTextEdit.MenuManager = this.barManager1;
            this.GrittingEveningStartTimeTextEdit.Name = "GrittingEveningStartTimeTextEdit";
            this.GrittingEveningStartTimeTextEdit.Properties.DisplayFormat.FormatString = "d";
            this.GrittingEveningStartTimeTextEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GrittingEveningStartTimeTextEdit.Properties.EditFormat.FormatString = "d";
            this.GrittingEveningStartTimeTextEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GrittingEveningStartTimeTextEdit.Properties.Mask.EditMask = "HH:mm";
            this.GrittingEveningStartTimeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.GrittingEveningStartTimeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GrittingEveningStartTimeTextEdit, true);
            this.GrittingEveningStartTimeTextEdit.Size = new System.Drawing.Size(119, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GrittingEveningStartTimeTextEdit, optionsSpelling9);
            this.GrittingEveningStartTimeTextEdit.StyleController = this.dataLayoutControl1;
            this.GrittingEveningStartTimeTextEdit.TabIndex = 17;
            // 
            // GrittingEveningEndTimeTextEdit
            // 
            this.GrittingEveningEndTimeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "GrittingEveningEndTime", true));
            this.GrittingEveningEndTimeTextEdit.Location = new System.Drawing.Point(241, 506);
            this.GrittingEveningEndTimeTextEdit.MenuManager = this.barManager1;
            this.GrittingEveningEndTimeTextEdit.Name = "GrittingEveningEndTimeTextEdit";
            this.GrittingEveningEndTimeTextEdit.Properties.DisplayFormat.FormatString = "d";
            this.GrittingEveningEndTimeTextEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GrittingEveningEndTimeTextEdit.Properties.EditFormat.FormatString = "d";
            this.GrittingEveningEndTimeTextEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GrittingEveningEndTimeTextEdit.Properties.Mask.EditMask = "HH:mm";
            this.GrittingEveningEndTimeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.GrittingEveningEndTimeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GrittingEveningEndTimeTextEdit, true);
            this.GrittingEveningEndTimeTextEdit.Size = new System.Drawing.Size(119, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GrittingEveningEndTimeTextEdit, optionsSpelling10);
            this.GrittingEveningEndTimeTextEdit.StyleController = this.dataLayoutControl1;
            this.GrittingEveningEndTimeTextEdit.TabIndex = 18;
            // 
            // WinterMaintInvoiceIgnoreSSColumnsButtonEdit
            // 
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03002EPClientEditBindingSource, "WinterMaintInvoiceIgnoreSSColumns", true));
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.Location = new System.Drawing.Point(233, 737);
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.MenuManager = this.barManager1;
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.Name = "WinterMaintInvoiceIgnoreSSColumnsButtonEdit";
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", "choose", null, true)});
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.Size = new System.Drawing.Size(454, 20);
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.StyleController = this.dataLayoutControl1;
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.TabIndex = 21;
            this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit_ButtonClick);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(608, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(735, 874);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.ItemForClientName,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlGroup11,
            this.ItemForClientCode,
            this.ItemForClientTypeID,
            this.ItemForCustomerRelationshipDepthID,
            this.ItemFormClientOrder,
            this.ItemForEmailPassword});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(715, 854);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 227);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(715, 617);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(691, 571);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup10,
            this.layoutControlGroup9,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Winter Maintenance";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.emptySpaceItem5,
            this.layoutControlGroup7,
            this.emptySpaceItem6,
            this.layoutControlGroup8});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(667, 523);
            this.layoutControlGroup5.Text = "Winter Maintenance";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Gritting";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDailyGrittingEmail,
            this.ItemForGrittingEmailFromAddress,
            this.ItemForGrittingClientPONumberRequired,
            this.emptySpaceItem9,
            this.ItemForDailyGrittingEmailShowUnGritted,
            this.ItemForDailyGrittingReport,
            this.ItemForReceivesExternalGrittingEmail,
            this.emptySpaceItem10,
            this.ItemForGrittingEveningStartTime,
            this.ItemForGrittingEveningEndTime});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(667, 233);
            this.layoutControlGroup6.Text = "Gritting";
            // 
            // ItemForDailyGrittingEmail
            // 
            this.ItemForDailyGrittingEmail.Control = this.DailyGrittingEmailCheckEdit;
            this.ItemForDailyGrittingEmail.CustomizationFormText = "Daily Gritting Email:";
            this.ItemForDailyGrittingEmail.Location = new System.Drawing.Point(0, 0);
            this.ItemForDailyGrittingEmail.MaxSize = new System.Drawing.Size(273, 23);
            this.ItemForDailyGrittingEmail.MinSize = new System.Drawing.Size(273, 23);
            this.ItemForDailyGrittingEmail.Name = "ItemForDailyGrittingEmail";
            this.ItemForDailyGrittingEmail.Size = new System.Drawing.Size(273, 23);
            this.ItemForDailyGrittingEmail.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDailyGrittingEmail.Text = "Daily Gritting Email:";
            this.ItemForDailyGrittingEmail.TextSize = new System.Drawing.Size(190, 13);
            // 
            // ItemForGrittingEmailFromAddress
            // 
            this.ItemForGrittingEmailFromAddress.Control = this.GrittingEmailFromAddressTextEdit;
            this.ItemForGrittingEmailFromAddress.CustomizationFormText = "Gritting Email From Email Address:";
            this.ItemForGrittingEmailFromAddress.Location = new System.Drawing.Point(0, 92);
            this.ItemForGrittingEmailFromAddress.Name = "ItemForGrittingEmailFromAddress";
            this.ItemForGrittingEmailFromAddress.Size = new System.Drawing.Size(643, 24);
            this.ItemForGrittingEmailFromAddress.Text = "Gritting Email From Email Address:";
            this.ItemForGrittingEmailFromAddress.TextSize = new System.Drawing.Size(190, 13);
            // 
            // ItemForGrittingClientPONumberRequired
            // 
            this.ItemForGrittingClientPONumberRequired.Control = this.GrittingClientPONumberRequiredCheckEdit;
            this.ItemForGrittingClientPONumberRequired.CustomizationFormText = "Client PO Required On Gritting Callouts:";
            this.ItemForGrittingClientPONumberRequired.Location = new System.Drawing.Point(0, 116);
            this.ItemForGrittingClientPONumberRequired.Name = "ItemForGrittingClientPONumberRequired";
            this.ItemForGrittingClientPONumberRequired.Size = new System.Drawing.Size(316, 23);
            this.ItemForGrittingClientPONumberRequired.Text = "Client PO Required On Gritting Callouts:";
            this.ItemForGrittingClientPONumberRequired.TextSize = new System.Drawing.Size(190, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(273, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(370, 92);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDailyGrittingEmailShowUnGritted
            // 
            this.ItemForDailyGrittingEmailShowUnGritted.Control = this.DailyGrittingEmailShowUnGrittedCheckEdit;
            this.ItemForDailyGrittingEmailShowUnGritted.CustomizationFormText = "Un-Gritted Sites on Gritting Report:";
            this.ItemForDailyGrittingEmailShowUnGritted.Location = new System.Drawing.Point(0, 23);
            this.ItemForDailyGrittingEmailShowUnGritted.Name = "ItemForDailyGrittingEmailShowUnGritted";
            this.ItemForDailyGrittingEmailShowUnGritted.Size = new System.Drawing.Size(273, 23);
            this.ItemForDailyGrittingEmailShowUnGritted.Text = "Un-Gritted Sites on Gritting Email:";
            this.ItemForDailyGrittingEmailShowUnGritted.TextSize = new System.Drawing.Size(190, 13);
            // 
            // ItemForDailyGrittingReport
            // 
            this.ItemForDailyGrittingReport.Control = this.DailyGrittingReportCheckEdit;
            this.ItemForDailyGrittingReport.CustomizationFormText = "Daily Gritting Report:";
            this.ItemForDailyGrittingReport.Location = new System.Drawing.Point(0, 46);
            this.ItemForDailyGrittingReport.Name = "ItemForDailyGrittingReport";
            this.ItemForDailyGrittingReport.Size = new System.Drawing.Size(273, 23);
            this.ItemForDailyGrittingReport.Text = "Daily Gritting Report:";
            this.ItemForDailyGrittingReport.TextSize = new System.Drawing.Size(190, 13);
            // 
            // ItemForReceivesExternalGrittingEmail
            // 
            this.ItemForReceivesExternalGrittingEmail.Control = this.ReceivesExternalGrittingEmailCheckEdit;
            this.ItemForReceivesExternalGrittingEmail.Location = new System.Drawing.Point(0, 69);
            this.ItemForReceivesExternalGrittingEmail.Name = "ItemForReceivesExternalGrittingEmail";
            this.ItemForReceivesExternalGrittingEmail.Size = new System.Drawing.Size(273, 23);
            this.ItemForReceivesExternalGrittingEmail.Text = "External Gritting Email:";
            this.ItemForReceivesExternalGrittingEmail.TextSize = new System.Drawing.Size(190, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(316, 116);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(327, 71);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForGrittingEveningStartTime
            // 
            this.ItemForGrittingEveningStartTime.Control = this.GrittingEveningStartTimeTextEdit;
            this.ItemForGrittingEveningStartTime.CustomizationFormText = "Evening Start Time:";
            this.ItemForGrittingEveningStartTime.Location = new System.Drawing.Point(0, 139);
            this.ItemForGrittingEveningStartTime.MaxSize = new System.Drawing.Size(316, 24);
            this.ItemForGrittingEveningStartTime.MinSize = new System.Drawing.Size(316, 24);
            this.ItemForGrittingEveningStartTime.Name = "ItemForGrittingEveningStartTime";
            this.ItemForGrittingEveningStartTime.Size = new System.Drawing.Size(316, 24);
            this.ItemForGrittingEveningStartTime.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGrittingEveningStartTime.Text = "Evening Start Time:";
            this.ItemForGrittingEveningStartTime.TextSize = new System.Drawing.Size(190, 13);
            // 
            // ItemForGrittingEveningEndTime
            // 
            this.ItemForGrittingEveningEndTime.Control = this.GrittingEveningEndTimeTextEdit;
            this.ItemForGrittingEveningEndTime.CustomizationFormText = "Evening End Time:";
            this.ItemForGrittingEveningEndTime.Location = new System.Drawing.Point(0, 163);
            this.ItemForGrittingEveningEndTime.Name = "ItemForGrittingEveningEndTime";
            this.ItemForGrittingEveningEndTime.Size = new System.Drawing.Size(316, 24);
            this.ItemForGrittingEveningEndTime.Text = "Evening End Time:";
            this.ItemForGrittingEveningEndTime.TextSize = new System.Drawing.Size(190, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 233);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(667, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Snow Clearance";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSnowClientPONumberRequired});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 243);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(667, 69);
            this.layoutControlGroup7.Text = "Snow Clearance";
            // 
            // ItemForSnowClientPONumberRequired
            // 
            this.ItemForSnowClientPONumberRequired.Control = this.SnowClientPONumberRequiredCheckEdit;
            this.ItemForSnowClientPONumberRequired.CustomizationFormText = "Client PO Required on Snow Callouts:";
            this.ItemForSnowClientPONumberRequired.Location = new System.Drawing.Point(0, 0);
            this.ItemForSnowClientPONumberRequired.Name = "ItemForSnowClientPONumberRequired";
            this.ItemForSnowClientPONumberRequired.Size = new System.Drawing.Size(643, 23);
            this.ItemForSnowClientPONumberRequired.Text = "Client PO Required on Snow Callouts:";
            this.ItemForSnowClientPONumberRequired.TextSize = new System.Drawing.Size(179, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 312);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(667, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Client Invoicing";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWinterMaintLastInvoiceDate,
            this.ItemForWinterMaintInvoiceFrequency,
            this.ItemForWinterMaintInvoiceIgnoreSSColumns,
            this.ItemForWinterMaintInvoiceFrequencyDescriptorID,
            this.ItemForFinanceCode,
            this.emptySpaceItem7,
            this.ItemForDepartmentCode,
            this.emptySpaceItem11,
            this.emptySpaceItem12});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 322);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(667, 201);
            this.layoutControlGroup8.Text = "Client Invoicing";
            // 
            // ItemForWinterMaintLastInvoiceDate
            // 
            this.ItemForWinterMaintLastInvoiceDate.Control = this.WinterMaintLastInvoiceDateDateEdit;
            this.ItemForWinterMaintLastInvoiceDate.CustomizationFormText = "Last Invoice Date:";
            this.ItemForWinterMaintLastInvoiceDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForWinterMaintLastInvoiceDate.Name = "ItemForWinterMaintLastInvoiceDate";
            this.ItemForWinterMaintLastInvoiceDate.Size = new System.Drawing.Size(643, 24);
            this.ItemForWinterMaintLastInvoiceDate.Text = "Last Invoice Date:";
            this.ItemForWinterMaintLastInvoiceDate.TextSize = new System.Drawing.Size(182, 13);
            // 
            // ItemForWinterMaintInvoiceFrequency
            // 
            this.ItemForWinterMaintInvoiceFrequency.Control = this.WinterMaintInvoiceFrequencySpinEdit;
            this.ItemForWinterMaintInvoiceFrequency.CustomizationFormText = "Invoice Frequency:";
            this.ItemForWinterMaintInvoiceFrequency.Location = new System.Drawing.Point(0, 24);
            this.ItemForWinterMaintInvoiceFrequency.Name = "ItemForWinterMaintInvoiceFrequency";
            this.ItemForWinterMaintInvoiceFrequency.Size = new System.Drawing.Size(344, 24);
            this.ItemForWinterMaintInvoiceFrequency.Text = "Invoice Frequency:";
            this.ItemForWinterMaintInvoiceFrequency.TextSize = new System.Drawing.Size(182, 13);
            // 
            // ItemForWinterMaintInvoiceIgnoreSSColumns
            // 
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.Control = this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit;
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.CustomizationFormText = "Hidden Invoice Spreadsheet Columns:";
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.Location = new System.Drawing.Point(0, 72);
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.MinSize = new System.Drawing.Size(50, 25);
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.Name = "ItemForWinterMaintInvoiceIgnoreSSColumns";
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.Size = new System.Drawing.Size(643, 25);
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.Text = "Hidden Invoice Spreadsheet Columns:";
            this.ItemForWinterMaintInvoiceIgnoreSSColumns.TextSize = new System.Drawing.Size(182, 13);
            // 
            // ItemForWinterMaintInvoiceFrequencyDescriptorID
            // 
            this.ItemForWinterMaintInvoiceFrequencyDescriptorID.Control = this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit;
            this.ItemForWinterMaintInvoiceFrequencyDescriptorID.CustomizationFormText = "Invoice Frequency Descriptor:";
            this.ItemForWinterMaintInvoiceFrequencyDescriptorID.Location = new System.Drawing.Point(0, 48);
            this.ItemForWinterMaintInvoiceFrequencyDescriptorID.Name = "ItemForWinterMaintInvoiceFrequencyDescriptorID";
            this.ItemForWinterMaintInvoiceFrequencyDescriptorID.Size = new System.Drawing.Size(344, 24);
            this.ItemForWinterMaintInvoiceFrequencyDescriptorID.Text = "Invoice Frequency Descriptor:";
            this.ItemForWinterMaintInvoiceFrequencyDescriptorID.TextSize = new System.Drawing.Size(182, 13);
            // 
            // ItemForFinanceCode
            // 
            this.ItemForFinanceCode.Control = this.FinanceCodeTextEdit;
            this.ItemForFinanceCode.Location = new System.Drawing.Point(0, 107);
            this.ItemForFinanceCode.Name = "ItemForFinanceCode";
            this.ItemForFinanceCode.Size = new System.Drawing.Size(643, 24);
            this.ItemForFinanceCode.Text = "Finance Code:";
            this.ItemForFinanceCode.TextSize = new System.Drawing.Size(182, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 97);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(643, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDepartmentCode
            // 
            this.ItemForDepartmentCode.Control = this.DepartmentCodeTextEdit;
            this.ItemForDepartmentCode.Location = new System.Drawing.Point(0, 131);
            this.ItemForDepartmentCode.Name = "ItemForDepartmentCode";
            this.ItemForDepartmentCode.Size = new System.Drawing.Size(643, 24);
            this.ItemForDepartmentCode.Text = "Department Code:";
            this.ItemForDepartmentCode.TextSize = new System.Drawing.Size(182, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(344, 24);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(299, 24);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(344, 48);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(299, 24);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Summer Operations";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForImagesFolderOM});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(667, 523);
            this.layoutControlGroup10.Text = "Summer Maintenance";
            // 
            // ItemForImagesFolderOM
            // 
            this.ItemForImagesFolderOM.Control = this.ImagesFolderOMButtonEdit;
            this.ItemForImagesFolderOM.CustomizationFormText = "Saved Images Folder:";
            this.ItemForImagesFolderOM.Location = new System.Drawing.Point(0, 0);
            this.ItemForImagesFolderOM.Name = "ItemForImagesFolderOM";
            this.ItemForImagesFolderOM.Size = new System.Drawing.Size(667, 523);
            this.ItemForImagesFolderOM.Text = "Saved Images Folder:";
            this.ItemForImagesFolderOM.TextSize = new System.Drawing.Size(105, 13);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Utilities Arb";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForHeaderImage,
            this.ItemForUtilityArbPermissionDocumentTypeID,
            this.emptySpaceItem13});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(667, 523);
            this.layoutControlGroup9.Text = "Utility Arb";
            // 
            // ItemForHeaderImage
            // 
            this.ItemForHeaderImage.Control = this.HeaderImageButtonEdit;
            this.ItemForHeaderImage.CustomizationFormText = "Header Image:";
            this.ItemForHeaderImage.Location = new System.Drawing.Point(0, 24);
            this.ItemForHeaderImage.Name = "ItemForHeaderImage";
            this.ItemForHeaderImage.Size = new System.Drawing.Size(667, 26);
            this.ItemForHeaderImage.Text = "Header Image:";
            this.ItemForHeaderImage.TextSize = new System.Drawing.Size(159, 13);
            // 
            // ItemForUtilityArbPermissionDocumentTypeID
            // 
            this.ItemForUtilityArbPermissionDocumentTypeID.Control = this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit;
            this.ItemForUtilityArbPermissionDocumentTypeID.CustomizationFormText = "Utilities Arb Permission Doc Type:";
            this.ItemForUtilityArbPermissionDocumentTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForUtilityArbPermissionDocumentTypeID.Name = "ItemForUtilityArbPermissionDocumentTypeID";
            this.ItemForUtilityArbPermissionDocumentTypeID.Size = new System.Drawing.Size(667, 24);
            this.ItemForUtilityArbPermissionDocumentTypeID.Text = "Utilities Arb Permission Doc Type:";
            this.ItemForUtilityArbPermissionDocumentTypeID.TextSize = new System.Drawing.Size(159, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 50);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(667, 473);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(667, 523);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(667, 523);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.AllowHide = false;
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(472, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(124, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 217);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(715, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 844);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(715, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(127, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(127, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(127, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(127, 23);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(327, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(388, 23);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIsUtilityArbClient,
            this.emptySpaceItem14,
            this.ItemForIsAmenityArbClient,
            this.ItemForIsActiveClient,
            this.ItemForIsWinterMaintenanceClient,
            this.ItemForIsSummerMaintenanceClient,
            this.ItemForIsTest});
            this.layoutControlGroup11.Location = new System.Drawing.Point(472, 23);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(243, 194);
            this.layoutControlGroup11.Text = "Visibility";
            // 
            // ItemForIsUtilityArbClient
            // 
            this.ItemForIsUtilityArbClient.Control = this.IsUtilityArbClientCheckEdit;
            this.ItemForIsUtilityArbClient.CustomizationFormText = "Utilities Arb Client:";
            this.ItemForIsUtilityArbClient.Location = new System.Drawing.Point(0, 125);
            this.ItemForIsUtilityArbClient.Name = "ItemForIsUtilityArbClient";
            this.ItemForIsUtilityArbClient.Size = new System.Drawing.Size(219, 23);
            this.ItemForIsUtilityArbClient.Text = "Utilities Arb Client:";
            this.ItemForIsUtilityArbClient.TextSize = new System.Drawing.Size(136, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 46);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(219, 10);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIsAmenityArbClient
            // 
            this.ItemForIsAmenityArbClient.Control = this.IsAmenityArbClientCheckEdit;
            this.ItemForIsAmenityArbClient.Location = new System.Drawing.Point(0, 102);
            this.ItemForIsAmenityArbClient.Name = "ItemForIsAmenityArbClient";
            this.ItemForIsAmenityArbClient.Size = new System.Drawing.Size(219, 23);
            this.ItemForIsAmenityArbClient.Text = "Amenity Arb Client:";
            this.ItemForIsAmenityArbClient.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForIsActiveClient
            // 
            this.ItemForIsActiveClient.Control = this.IsActiveClientCheckEdit;
            this.ItemForIsActiveClient.CustomizationFormText = "Active Client:";
            this.ItemForIsActiveClient.Location = new System.Drawing.Point(0, 0);
            this.ItemForIsActiveClient.Name = "ItemForIsActiveClient";
            this.ItemForIsActiveClient.Size = new System.Drawing.Size(219, 23);
            this.ItemForIsActiveClient.Text = "Acitve Client:";
            this.ItemForIsActiveClient.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForIsWinterMaintenanceClient
            // 
            this.ItemForIsWinterMaintenanceClient.Control = this.IsWinterMaintenanceClientCheckEdit;
            this.ItemForIsWinterMaintenanceClient.Location = new System.Drawing.Point(0, 56);
            this.ItemForIsWinterMaintenanceClient.Name = "ItemForIsWinterMaintenanceClient";
            this.ItemForIsWinterMaintenanceClient.Size = new System.Drawing.Size(219, 23);
            this.ItemForIsWinterMaintenanceClient.Text = "Winter Maintenance Client:";
            this.ItemForIsWinterMaintenanceClient.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForIsSummerMaintenanceClient
            // 
            this.ItemForIsSummerMaintenanceClient.Control = this.IsSummerMaintenanceClientCheckEdit;
            this.ItemForIsSummerMaintenanceClient.Location = new System.Drawing.Point(0, 79);
            this.ItemForIsSummerMaintenanceClient.MaxSize = new System.Drawing.Size(219, 23);
            this.ItemForIsSummerMaintenanceClient.MinSize = new System.Drawing.Size(219, 23);
            this.ItemForIsSummerMaintenanceClient.Name = "ItemForIsSummerMaintenanceClient";
            this.ItemForIsSummerMaintenanceClient.Size = new System.Drawing.Size(219, 23);
            this.ItemForIsSummerMaintenanceClient.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIsSummerMaintenanceClient.Text = "Summer Maintenance Client:";
            this.ItemForIsSummerMaintenanceClient.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForIsTest
            // 
            this.ItemForIsTest.Control = this.IsTestCheckEdit;
            this.ItemForIsTest.Location = new System.Drawing.Point(0, 23);
            this.ItemForIsTest.Name = "ItemForIsTest";
            this.ItemForIsTest.Size = new System.Drawing.Size(219, 23);
            this.ItemForIsTest.Text = "Test Client:";
            this.ItemForIsTest.TextSize = new System.Drawing.Size(136, 13);
            // 
            // ItemForClientCode
            // 
            this.ItemForClientCode.Control = this.ClientCodeTextEdit;
            this.ItemForClientCode.CustomizationFormText = "Client Code:";
            this.ItemForClientCode.Location = new System.Drawing.Point(0, 47);
            this.ItemForClientCode.Name = "ItemForClientCode";
            this.ItemForClientCode.Size = new System.Drawing.Size(472, 24);
            this.ItemForClientCode.Text = "Client Code:";
            this.ItemForClientCode.TextSize = new System.Drawing.Size(124, 13);
            // 
            // ItemForClientTypeID
            // 
            this.ItemForClientTypeID.Control = this.ClientTypeIDGridLookUpEdit;
            this.ItemForClientTypeID.CustomizationFormText = "Client Type:";
            this.ItemForClientTypeID.Location = new System.Drawing.Point(0, 71);
            this.ItemForClientTypeID.Name = "ItemForClientTypeID";
            this.ItemForClientTypeID.Size = new System.Drawing.Size(472, 26);
            this.ItemForClientTypeID.Text = "Client Type:";
            this.ItemForClientTypeID.TextSize = new System.Drawing.Size(124, 13);
            // 
            // ItemForCustomerRelationshipDepthID
            // 
            this.ItemForCustomerRelationshipDepthID.Control = this.CustomerRelationshipDepthIDGridLookUpEdit;
            this.ItemForCustomerRelationshipDepthID.CustomizationFormText = "Client Relationship Depth:";
            this.ItemForCustomerRelationshipDepthID.Location = new System.Drawing.Point(0, 97);
            this.ItemForCustomerRelationshipDepthID.Name = "ItemForCustomerRelationshipDepthID";
            this.ItemForCustomerRelationshipDepthID.Size = new System.Drawing.Size(472, 24);
            this.ItemForCustomerRelationshipDepthID.Text = "Client Relationship Depth:";
            this.ItemForCustomerRelationshipDepthID.TextSize = new System.Drawing.Size(124, 13);
            // 
            // ItemFormClientOrder
            // 
            this.ItemFormClientOrder.Control = this.ClientOrderSpinEdit;
            this.ItemFormClientOrder.CustomizationFormText = "Client Order:";
            this.ItemFormClientOrder.Location = new System.Drawing.Point(0, 121);
            this.ItemFormClientOrder.Name = "ItemFormClientOrder";
            this.ItemFormClientOrder.Size = new System.Drawing.Size(472, 24);
            this.ItemFormClientOrder.Text = "Client Order:";
            this.ItemFormClientOrder.TextSize = new System.Drawing.Size(124, 13);
            // 
            // ItemForEmailPassword
            // 
            this.ItemForEmailPassword.Control = this.EmailPasswordTextEdit;
            this.ItemForEmailPassword.CustomizationFormText = "Email Password:";
            this.ItemForEmailPassword.Location = new System.Drawing.Point(0, 145);
            this.ItemForEmailPassword.Name = "ItemForEmailPassword";
            this.ItemForEmailPassword.Size = new System.Drawing.Size(472, 72);
            this.ItemForEmailPassword.Text = "Email Password:";
            this.ItemForEmailPassword.TextSize = new System.Drawing.Size(124, 13);
            // 
            // sp03002_EP_Client_EditTableAdapter
            // 
            this.sp03002_EP_Client_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp00227_Picklist_List_With_BlankTableAdapter
            // 
            this.sp00227_Picklist_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter
            // 
            this.sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp05058_GC_Customer_Relationship_Depths_With_BlankTableAdapter
            // 
            this.sp05058_GC_Customer_Relationship_Depths_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp03088_EP_Utility_ARB_Permission_Document_TypesTableAdapter
            // 
            this.sp03088_EP_Utility_ARB_Permission_Document_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Client_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(752, 647);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Client_Edit";
            this.Text = "Edit Client";
            this.Activated += new System.EventHandler(this.frm_Core_Client_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Client_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Client_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IsTestCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03002EPClientEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsAmenityArbClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsWinterMaintenanceClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSummerMaintenanceClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceivesExternalGrittingEmailCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagesFolderOMButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderImageButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbPermissionDocumentTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03088EPUtilityARBPermissionDocumentTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsUtilityArbClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerRelationshipDepthIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05058GCCustomerRelationshipDepthsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsActiveClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailPasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintInvoiceFrequencySpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintLastInvoiceDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintLastInvoiceDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SnowClientPONumberRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingClientPONumberRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DailyGrittingEmailShowUnGrittedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DailyGrittingEmailCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEmailFromAddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DailyGrittingReportCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00227PicklistListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEveningStartTimeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEveningEndTimeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintInvoiceIgnoreSSColumnsButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDailyGrittingEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingEmailFromAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingClientPONumberRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDailyGrittingEmailShowUnGritted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDailyGrittingReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReceivesExternalGrittingEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingEveningStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingEveningEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSnowClientPONumberRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintLastInvoiceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintInvoiceFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintInvoiceIgnoreSSColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintInvoiceFrequencyDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForImagesFolderOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeaderImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArbPermissionDocumentTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsUtilityArbClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsAmenityArbClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsActiveClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsWinterMaintenanceClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSummerMaintenanceClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerRelationshipDepthID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFormClientOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailPassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private System.Windows.Forms.BindingSource sp03002EPClientEditBindingSource;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private DevExpress.XtraEditors.TextEdit ClientCodeTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientTypeID;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03002_EP_Client_EditTableAdapter sp03002_EP_Client_EditTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.GridLookUpEdit ClientTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private System.Windows.Forms.BindingSource sp00227PicklistListWithBlankBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00227_Picklist_List_With_BlankTableAdapter sp00227_Picklist_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colintOrder;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.SpinEdit ClientOrderSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemFormClientOrder;
        private DevExpress.XtraEditors.CheckEdit DailyGrittingReportCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDailyGrittingReport;
        private DevExpress.XtraEditors.TextEdit GrittingEmailFromAddressTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingEmailFromAddress;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.CheckEdit DailyGrittingEmailCheckEdit;
        private DevExpress.XtraEditors.CheckEdit DailyGrittingEmailShowUnGrittedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDailyGrittingEmailShowUnGritted;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDailyGrittingEmail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.CheckEdit GrittingClientPONumberRequiredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingClientPONumberRequired;
        private DevExpress.XtraEditors.CheckEdit SnowClientPONumberRequiredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSnowClientPONumberRequired;
        private DevExpress.XtraEditors.TextEdit GrittingEveningStartTimeTextEdit;
        private DevExpress.XtraEditors.TextEdit GrittingEveningEndTimeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingEveningStartTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingEveningEndTime;
        private DevExpress.XtraEditors.DateEdit WinterMaintLastInvoiceDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWinterMaintLastInvoiceDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraEditors.SpinEdit WinterMaintInvoiceFrequencySpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWinterMaintInvoiceFrequency;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWinterMaintInvoiceIgnoreSSColumns;
        private DevExpress.XtraEditors.GridLookUpEdit WinterMaintInvoiceFrequencyDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWinterMaintInvoiceFrequencyDescriptorID;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private System.Windows.Forms.BindingSource sp04035GCSelfBillingFrequencyDescriptorsWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter sp04035_GC_Self_Billing_Frequency_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit EmailPasswordTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailPassword;
        private DevExpress.XtraEditors.ButtonEdit WinterMaintInvoiceIgnoreSSColumnsButtonEdit;
        private DevExpress.XtraEditors.CheckEdit IsActiveClientCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsActiveClient;
        private DevExpress.XtraEditors.GridLookUpEdit CustomerRelationshipDepthIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerRelationshipDepthID;
        private System.Windows.Forms.BindingSource sp05058GCCustomerRelationshipDepthsWithBlankBindingSource;
        private DataSet_EP_DataEntryTableAdapters.sp05058_GC_Customer_Relationship_Depths_With_BlankTableAdapter sp05058_GC_Customer_Relationship_Depths_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraEditors.CheckEdit IsUtilityArbClientCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsUtilityArbClient;
        private DevExpress.XtraEditors.GridLookUpEdit UtilityArbPermissionDocumentTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUtilityArbPermissionDocumentTypeID;
        private System.Windows.Forms.BindingSource sp03088EPUtilityARBPermissionDocumentTypesBindingSource;
        private DataSet_EP_DataEntryTableAdapters.sp03088_EP_Utility_ARB_Permission_Document_TypesTableAdapter sp03088_EP_Utility_ARB_Permission_Document_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.ButtonEdit HeaderImageButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHeaderImage;
        private DevExpress.XtraEditors.ButtonEdit ImagesFolderOMButtonEdit;
        private DevExpress.XtraEditors.TextEdit DepartmentCodeTextEdit;
        private DevExpress.XtraEditors.TextEdit FinanceCodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinanceCode;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDepartmentCode;
        private DevExpress.XtraEditors.CheckEdit ReceivesExternalGrittingEmailCheckEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReceivesExternalGrittingEmail;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.CheckEdit IsWinterMaintenanceClientCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsSummerMaintenanceClientCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsSummerMaintenanceClient;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsWinterMaintenanceClient;
        private DevExpress.XtraEditors.CheckEdit IsAmenityArbClientCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForImagesFolderOM;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsAmenityArbClient;
        private DevExpress.XtraEditors.CheckEdit IsTestCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsTest;
    }
}

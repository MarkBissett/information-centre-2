namespace WoodPlan5
{
    partial class frm_Core_Master_Job_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Master_Job_Manager));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00204JobRateManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRateCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colIsOperationsJob = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsAmenityArbJob = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsUtilityArbJob = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsExtraWorksJob = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06159OMJobManagerLinkedJobSubTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colIsExtraWorksJob1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsOperationsJob1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp06178OMJobManagerLinkedJobSubTypeCompetenciesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobCompetencyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobTypeID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompetencyDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp06195OMJobManagerLinkedMasterJobAttributesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMasterJobAttributeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasterJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditorMask = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryMinValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDataEntryMaxValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPicklistHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnScreenLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAttributeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRemarks5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPicklistDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeJobSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAttributeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueFormula = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueFormulaEvaluateLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueFormulaEvaluateLocationDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueFormulaEvaluateOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHideFromApp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07162UTJobManagerLinkedDefaultEquipmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDefaultRequiredEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07161UTJobManagerLinkedDefaultMaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDefaultRequiredMaterialID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellPerUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsInStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colWarningLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReorderLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07234UTLinkedReferenceFilesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReferenceFileID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditDocumentPath = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTimeShortTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEditDomcumentRemarks = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl14 = new DevExpress.XtraGrid.GridControl();
            this.sp06313OMJobTypeVisitTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Core = new WoodPlan5.DataSet_OM_Core();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitTypeJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobDescription4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageJobRates = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControlJobRates = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlJobRate = new DevExpress.XtraGrid.GridControl();
            this.sp00205JobRateManagerJobRatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewJobRate = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colValidTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRateTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRateUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRateTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridControlJobRateCriteria = new DevExpress.XtraGrid.GridControl();
            this.sp00206JobRateManagerJobRateParametersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewJobRateCriteria = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrJobcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrJobdescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRateTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRateDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmonRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.coldecRateUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colstrContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrCriteriaDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintCriteriaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintParameterID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintRateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintRateType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.sp00204_Job_Rate_ManagerTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00204_Job_Rate_ManagerTableAdapter();
            this.sp07162_UT_Job_Manager_Linked_Default_EquipmentTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07162_UT_Job_Manager_Linked_Default_EquipmentTableAdapter();
            this.sp07161_UT_Job_Manager_Linked_Default_MaterialsTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07161_UT_Job_Manager_Linked_Default_MaterialsTableAdapter();
            this.sp07234_UT_Linked_Reference_FilesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07234_UT_Linked_Reference_FilesTableAdapter();
            this.sp06159_OM_Job_Manager_Linked_Job_Sub_TypesTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06159_OM_Job_Manager_Linked_Job_Sub_TypesTableAdapter();
            this.sp06178_OM_Job_Manager_Linked_Job_Sub_Type_CompetenciesTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06178_OM_Job_Manager_Linked_Job_Sub_Type_CompetenciesTableAdapter();
            this.sp06195_OM_Job_Manager_Linked_Master_Job_AttributesTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06195_OM_Job_Manager_Linked_Master_Job_AttributesTableAdapter();
            this.sp06313_OM_Job_Type_Visit_TypesTableAdapter = new WoodPlan5.DataSet_OM_CoreTableAdapters.sp06313_OM_Job_Type_Visit_TypesTableAdapter();
            this.sp00205_Job_Rate_Manager_Job_RatesTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00205_Job_Rate_Manager_Job_RatesTableAdapter();
            this.sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00204JobRateManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06159OMJobManagerLinkedJobSubTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06178OMJobManagerLinkedJobSubTypeCompetenciesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06195OMJobManagerLinkedMasterJobAttributesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07162UTJobManagerLinkedDefaultEquipmentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07161UTJobManagerLinkedDefaultMaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07234UTLinkedReferenceFilesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditDocumentPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTimeShortTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditDomcumentRemarks)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06313OMJobTypeVisitTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            this.xtraTabPageJobRates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlJobRates)).BeginInit();
            this.splitContainerControlJobRates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJobRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00205JobRateManagerJobRatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJobRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJobRateCriteria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00206JobRateManagerJobRateParametersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJobRateCriteria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1214, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 581);
            this.barDockControlBottom.Size = new System.Drawing.Size(1214, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 547);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1214, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 547);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.barEditItem1});
            this.barManager1.MaxItemId = 35;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemDateEdit1,
            this.repositoryItemPopupContainerEditDateRange});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 2;
            this.colActive.Width = 49;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00204JobRateManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1214, 304);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00204JobRateManagerBindingSource
            // 
            this.sp00204JobRateManagerBindingSource.DataMember = "sp00204_Job_Rate_Manager";
            this.sp00204JobRateManagerBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16");
            this.imageCollection1.Images.SetKeyName(4, "Picklist_Manager_16.png");
            this.imageCollection1.InsertGalleryImage("topbottomrules_16x16.png", "images/conditional%20formatting/topbottomrules_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/conditional%20formatting/topbottomrules_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "topbottomrules_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.colJobCode,
            this.colJobDescription,
            this.colDefaultWorkUnits,
            this.colLinkedRateCount,
            this.colRemarks,
            this.colDisabled,
            this.colIsOperationsJob,
            this.colIsAmenityArbJob,
            this.colIsUtilityArbJob,
            this.colIsExtraWorksJob});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            this.colJobID.Width = 52;
            // 
            // colJobCode
            // 
            this.colJobCode.Caption = "Job Code";
            this.colJobCode.FieldName = "JobCode";
            this.colJobCode.Name = "colJobCode";
            this.colJobCode.OptionsColumn.AllowEdit = false;
            this.colJobCode.OptionsColumn.AllowFocus = false;
            this.colJobCode.OptionsColumn.ReadOnly = true;
            this.colJobCode.Visible = true;
            this.colJobCode.VisibleIndex = 1;
            this.colJobCode.Width = 131;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Name";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 0;
            this.colJobDescription.Width = 384;
            // 
            // colDefaultWorkUnits
            // 
            this.colDefaultWorkUnits.Caption = "Default Work Units";
            this.colDefaultWorkUnits.FieldName = "DefaultWorkUnits";
            this.colDefaultWorkUnits.Name = "colDefaultWorkUnits";
            this.colDefaultWorkUnits.OptionsColumn.AllowEdit = false;
            this.colDefaultWorkUnits.OptionsColumn.AllowFocus = false;
            this.colDefaultWorkUnits.OptionsColumn.ReadOnly = true;
            this.colDefaultWorkUnits.Width = 111;
            // 
            // colLinkedRateCount
            // 
            this.colLinkedRateCount.Caption = "Linked Rates";
            this.colLinkedRateCount.FieldName = "LinkedRateCount";
            this.colLinkedRateCount.Name = "colLinkedRateCount";
            this.colLinkedRateCount.OptionsColumn.AllowEdit = false;
            this.colLinkedRateCount.OptionsColumn.AllowFocus = false;
            this.colLinkedRateCount.OptionsColumn.ReadOnly = true;
            this.colLinkedRateCount.Width = 82;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 7;
            this.colRemarks.Width = 162;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 2;
            this.colDisabled.Width = 61;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colIsOperationsJob
            // 
            this.colIsOperationsJob.Caption = "Operations Job";
            this.colIsOperationsJob.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsOperationsJob.FieldName = "IsOperationsJob";
            this.colIsOperationsJob.Name = "colIsOperationsJob";
            this.colIsOperationsJob.OptionsColumn.AllowEdit = false;
            this.colIsOperationsJob.OptionsColumn.AllowFocus = false;
            this.colIsOperationsJob.OptionsColumn.ReadOnly = true;
            this.colIsOperationsJob.Visible = true;
            this.colIsOperationsJob.VisibleIndex = 3;
            this.colIsOperationsJob.Width = 92;
            // 
            // colIsAmenityArbJob
            // 
            this.colIsAmenityArbJob.Caption = "Amenity Arb Job";
            this.colIsAmenityArbJob.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsAmenityArbJob.FieldName = "IsAmenityArbJob";
            this.colIsAmenityArbJob.Name = "colIsAmenityArbJob";
            this.colIsAmenityArbJob.OptionsColumn.AllowEdit = false;
            this.colIsAmenityArbJob.OptionsColumn.AllowFocus = false;
            this.colIsAmenityArbJob.OptionsColumn.ReadOnly = true;
            this.colIsAmenityArbJob.Visible = true;
            this.colIsAmenityArbJob.VisibleIndex = 5;
            this.colIsAmenityArbJob.Width = 98;
            // 
            // colIsUtilityArbJob
            // 
            this.colIsUtilityArbJob.Caption = "Utility Arb Job";
            this.colIsUtilityArbJob.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsUtilityArbJob.FieldName = "IsUtilityArbJob";
            this.colIsUtilityArbJob.Name = "colIsUtilityArbJob";
            this.colIsUtilityArbJob.OptionsColumn.AllowEdit = false;
            this.colIsUtilityArbJob.OptionsColumn.AllowFocus = false;
            this.colIsUtilityArbJob.OptionsColumn.ReadOnly = true;
            this.colIsUtilityArbJob.Visible = true;
            this.colIsUtilityArbJob.VisibleIndex = 6;
            this.colIsUtilityArbJob.Width = 86;
            // 
            // colIsExtraWorksJob
            // 
            this.colIsExtraWorksJob.Caption = "Extra Works Job";
            this.colIsExtraWorksJob.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsExtraWorksJob.FieldName = "IsExtraWorksJob";
            this.colIsExtraWorksJob.Name = "colIsExtraWorksJob";
            this.colIsExtraWorksJob.OptionsColumn.AllowEdit = false;
            this.colIsExtraWorksJob.OptionsColumn.AllowFocus = false;
            this.colIsExtraWorksJob.OptionsColumn.ReadOnly = true;
            this.colIsExtraWorksJob.Visible = true;
            this.colIsExtraWorksJob.VisibleIndex = 4;
            this.colIsExtraWorksJob.Width = 98;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "d";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 34);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl2.Panel2.Text = "Linked Documents";
            this.splitContainerControl2.Size = new System.Drawing.Size(1214, 546);
            this.splitContainerControl2.SplitterPosition = 236;
            this.splitContainerControl2.TabIndex = 5;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1214, 304);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage4;
            this.xtraTabControl1.Size = new System.Drawing.Size(1210, 232);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage4,
            this.xtraTabPage2,
            this.xtraTabPage1,
            this.xtraTabPage3,
            this.xtraTabPage7,
            this.xtraTabPageJobRates});
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1205, 206);
            this.xtraTabPage4.Text = "Linked Job Sub-Types";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl4);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Operation Manager Use Only";
            this.splitContainerControl1.Size = new System.Drawing.Size(1205, 206);
            this.splitContainerControl1.SplitterPosition = 597;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp06159OMJobManagerLinkedJobSubTypesBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Deleted Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemTextEditNumeric,
            this.repositoryItemCheckEdit3});
            this.gridControl4.Size = new System.Drawing.Size(597, 206);
            this.gridControl4.TabIndex = 3;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06159OMJobManagerLinkedJobSubTypesBindingSource
            // 
            this.sp06159OMJobManagerLinkedJobSubTypesBindingSource.DataMember = "sp06159_OM_Job_Manager_Linked_Job_Sub_Types";
            this.sp06159OMJobManagerLinkedJobSubTypesBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobSubTypeID,
            this.colJobTypeID2,
            this.colDescription2,
            this.colRecordOrder,
            this.colRemarks3,
            this.colJobDescription3,
            this.colDisabled1,
            this.colIsExtraWorksJob1,
            this.colIsOperationsJob1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 101;
            // 
            // colJobTypeID2
            // 
            this.colJobTypeID2.Caption = "Job Type ID";
            this.colJobTypeID2.FieldName = "JobTypeID";
            this.colJobTypeID2.Name = "colJobTypeID2";
            this.colJobTypeID2.OptionsColumn.AllowEdit = false;
            this.colJobTypeID2.OptionsColumn.AllowFocus = false;
            this.colJobTypeID2.OptionsColumn.ReadOnly = true;
            this.colJobTypeID2.Width = 79;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Job Sub-Type Description";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 381;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Record Order";
            this.colRecordOrder.ColumnEdit = this.repositoryItemTextEditNumeric;
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Visible = true;
            this.colRecordOrder.VisibleIndex = 1;
            this.colRecordOrder.Width = 99;
            // 
            // repositoryItemTextEditNumeric
            // 
            this.repositoryItemTextEditNumeric.AutoHeight = false;
            this.repositoryItemTextEditNumeric.Mask.EditMask = "n0";
            this.repositoryItemTextEditNumeric.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric.Name = "repositoryItemTextEditNumeric";
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colJobDescription3
            // 
            this.colJobDescription3.Caption = "Linked To";
            this.colJobDescription3.FieldName = "JobDescription";
            this.colJobDescription3.Name = "colJobDescription3";
            this.colJobDescription3.OptionsColumn.AllowEdit = false;
            this.colJobDescription3.OptionsColumn.AllowFocus = false;
            this.colJobDescription3.OptionsColumn.ReadOnly = true;
            this.colJobDescription3.Visible = true;
            this.colJobDescription3.VisibleIndex = 3;
            this.colJobDescription3.Width = 381;
            // 
            // colDisabled1
            // 
            this.colDisabled1.Caption = "Disabled";
            this.colDisabled1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colDisabled1.FieldName = "Disabled";
            this.colDisabled1.Name = "colDisabled1";
            this.colDisabled1.OptionsColumn.AllowEdit = false;
            this.colDisabled1.OptionsColumn.AllowFocus = false;
            this.colDisabled1.Visible = true;
            this.colDisabled1.VisibleIndex = 2;
            this.colDisabled1.Width = 59;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colIsExtraWorksJob1
            // 
            this.colIsExtraWorksJob1.Caption = "Extra Works Job";
            this.colIsExtraWorksJob1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colIsExtraWorksJob1.FieldName = "IsExtraWorksJob";
            this.colIsExtraWorksJob1.Name = "colIsExtraWorksJob1";
            this.colIsExtraWorksJob1.OptionsColumn.AllowEdit = false;
            this.colIsExtraWorksJob1.OptionsColumn.AllowFocus = false;
            this.colIsExtraWorksJob1.Visible = true;
            this.colIsExtraWorksJob1.VisibleIndex = 3;
            this.colIsExtraWorksJob1.Width = 98;
            // 
            // colIsOperationsJob1
            // 
            this.colIsOperationsJob1.Caption = "Maintenance Job";
            this.colIsOperationsJob1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colIsOperationsJob1.FieldName = "IsOperationsJob";
            this.colIsOperationsJob1.Name = "colIsOperationsJob1";
            this.colIsOperationsJob1.OptionsColumn.AllowEdit = false;
            this.colIsOperationsJob1.OptionsColumn.AllowFocus = false;
            this.colIsOperationsJob1.Visible = true;
            this.colIsOperationsJob1.VisibleIndex = 4;
            this.colIsOperationsJob1.Width = 100;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage5;
            this.xtraTabControl2.Size = new System.Drawing.Size(577, 203);
            this.xtraTabControl2.TabIndex = 5;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage5,
            this.xtraTabPage6});
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.gridControl6);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(572, 177);
            this.xtraTabPage5.Text = "Linked Job Competencies";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp06178OMJobManagerLinkedJobSubTypeCompetenciesBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Deleted Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl6.Size = new System.Drawing.Size(572, 177);
            this.gridControl6.TabIndex = 4;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp06178OMJobManagerLinkedJobSubTypeCompetenciesBindingSource
            // 
            this.sp06178OMJobManagerLinkedJobSubTypeCompetenciesBindingSource.DataMember = "sp06178_OM_Job_Manager_Linked_Job_Sub_Type_Competencies";
            this.sp06178OMJobManagerLinkedJobSubTypeCompetenciesBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobCompetencyID,
            this.colJobSubTypeID1,
            this.colQualificationTypeID,
            this.colRemarks4,
            this.colJobTypeID3,
            this.colJobSubTypeDescription,
            this.colJobTypeDescription,
            this.colCompetencyDescription,
            this.colRequirementLevelDescription,
            this.colQualificationSubType,
            this.colQualificationSubTypeID,
            this.colQualificationType});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 2;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobSubTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQualificationType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQualificationSubType, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colJobCompetencyID
            // 
            this.colJobCompetencyID.Caption = "Job Competency ID";
            this.colJobCompetencyID.FieldName = "JobCompetencyID";
            this.colJobCompetencyID.Name = "colJobCompetencyID";
            this.colJobCompetencyID.OptionsColumn.AllowEdit = false;
            this.colJobCompetencyID.OptionsColumn.AllowFocus = false;
            this.colJobCompetencyID.OptionsColumn.ReadOnly = true;
            this.colJobCompetencyID.Width = 115;
            // 
            // colJobSubTypeID1
            // 
            this.colJobSubTypeID1.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID1.FieldName = "JobSubTypeID";
            this.colJobSubTypeID1.Name = "colJobSubTypeID1";
            this.colJobSubTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID1.Width = 101;
            // 
            // colQualificationTypeID
            // 
            this.colQualificationTypeID.Caption = "Qualification Type ID";
            this.colQualificationTypeID.FieldName = "QualificationTypeID";
            this.colQualificationTypeID.Name = "colQualificationTypeID";
            this.colQualificationTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeID.Width = 121;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 3;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colJobTypeID3
            // 
            this.colJobTypeID3.Caption = "Job Type ID";
            this.colJobTypeID3.FieldName = "JobTypeID";
            this.colJobTypeID3.Name = "colJobTypeID3";
            this.colJobTypeID3.OptionsColumn.AllowEdit = false;
            this.colJobTypeID3.OptionsColumn.AllowFocus = false;
            this.colJobTypeID3.OptionsColumn.ReadOnly = true;
            this.colJobTypeID3.Width = 79;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type Description";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 4;
            this.colJobSubTypeDescription.Width = 291;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type Description";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 5;
            this.colJobTypeDescription.Width = 293;
            // 
            // colCompetencyDescription
            // 
            this.colCompetencyDescription.Caption = "Competency Description";
            this.colCompetencyDescription.FieldName = "CompetencyDescription";
            this.colCompetencyDescription.Name = "colCompetencyDescription";
            this.colCompetencyDescription.OptionsColumn.AllowEdit = false;
            this.colCompetencyDescription.OptionsColumn.AllowFocus = false;
            this.colCompetencyDescription.OptionsColumn.ReadOnly = true;
            this.colCompetencyDescription.Width = 264;
            // 
            // colRequirementLevelDescription
            // 
            this.colRequirementLevelDescription.Caption = "Requirement Level";
            this.colRequirementLevelDescription.FieldName = "RequirementLevelDescription";
            this.colRequirementLevelDescription.Name = "colRequirementLevelDescription";
            this.colRequirementLevelDescription.OptionsColumn.AllowEdit = false;
            this.colRequirementLevelDescription.OptionsColumn.AllowFocus = false;
            this.colRequirementLevelDescription.OptionsColumn.ReadOnly = true;
            this.colRequirementLevelDescription.Visible = true;
            this.colRequirementLevelDescription.VisibleIndex = 2;
            this.colRequirementLevelDescription.Width = 122;
            // 
            // colQualificationSubType
            // 
            this.colQualificationSubType.Caption = "Qualification Sub-Type";
            this.colQualificationSubType.FieldName = "QualificationSubType";
            this.colQualificationSubType.Name = "colQualificationSubType";
            this.colQualificationSubType.OptionsColumn.AllowEdit = false;
            this.colQualificationSubType.OptionsColumn.AllowFocus = false;
            this.colQualificationSubType.OptionsColumn.ReadOnly = true;
            this.colQualificationSubType.Visible = true;
            this.colQualificationSubType.VisibleIndex = 1;
            this.colQualificationSubType.Width = 175;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.Caption = "Qualification Sub-Type ID";
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationSubTypeID.Width = 143;
            // 
            // colQualificationType
            // 
            this.colQualificationType.Caption = "Qualification Type";
            this.colQualificationType.FieldName = "QualificationType";
            this.colQualificationType.Name = "colQualificationType";
            this.colQualificationType.OptionsColumn.AllowEdit = false;
            this.colQualificationType.OptionsColumn.AllowFocus = false;
            this.colQualificationType.OptionsColumn.ReadOnly = true;
            this.colQualificationType.Visible = true;
            this.colQualificationType.VisibleIndex = 0;
            this.colQualificationType.Width = 160;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.gridControl7);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(572, 177);
            this.xtraTabPage6.Text = "Linked Extra Attributes";
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp06195OMJobManagerLinkedMasterJobAttributesBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Deleted Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Master Attribute Picklist Manager", "picklists")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemTextEdit2DP3,
            this.repositoryItemCheckEdit2});
            this.gridControl7.Size = new System.Drawing.Size(572, 177);
            this.gridControl7.TabIndex = 5;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp06195OMJobManagerLinkedMasterJobAttributesBindingSource
            // 
            this.sp06195OMJobManagerLinkedMasterJobAttributesBindingSource.DataMember = "sp06195_OM_Job_Manager_Linked_Master_Job_Attributes";
            this.sp06195OMJobManagerLinkedMasterJobAttributesBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMasterJobAttributeID,
            this.colMasterJobSubTypeID,
            this.colDataTypeID,
            this.colEditorTypeID,
            this.colEditorMask,
            this.colDataEntryMinValue,
            this.colDataEntryMaxValue,
            this.colDataEntryRequired,
            this.colPicklistHeaderID,
            this.colOnScreenLabel,
            this.colAttributeOrder,
            this.colDefaultValue,
            this.colRemarks5,
            this.colDataType,
            this.colEditorType,
            this.colPicklistDescription,
            this.colJobSubTypeDescription1,
            this.colJobTypeDescription1,
            this.colJobTypeJobSubType,
            this.colJobTypeID4,
            this.colActive,
            this.colAttributeName,
            this.colValueFormula,
            this.colValueFormulaEvaluateLocation,
            this.colValueFormulaEvaluateLocationDesc,
            this.colValueFormulaEvaluateOrder,
            this.colHideFromApp});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView7.FormatRules.Add(gridFormatRule1);
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeJobSubType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAttributeOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAttributeName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOnScreenLabel, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.DoubleClick += new System.EventHandler(this.gridView7_DoubleClick);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colMasterJobAttributeID
            // 
            this.colMasterJobAttributeID.Caption = "Master Job Attribute ID";
            this.colMasterJobAttributeID.FieldName = "MasterJobAttributeID";
            this.colMasterJobAttributeID.Name = "colMasterJobAttributeID";
            this.colMasterJobAttributeID.OptionsColumn.AllowEdit = false;
            this.colMasterJobAttributeID.OptionsColumn.AllowFocus = false;
            this.colMasterJobAttributeID.OptionsColumn.ReadOnly = true;
            this.colMasterJobAttributeID.Width = 134;
            // 
            // colMasterJobSubTypeID
            // 
            this.colMasterJobSubTypeID.Caption = "Master Job Sub-Type ID";
            this.colMasterJobSubTypeID.FieldName = "MasterJobSubTypeID";
            this.colMasterJobSubTypeID.Name = "colMasterJobSubTypeID";
            this.colMasterJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colMasterJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colMasterJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colMasterJobSubTypeID.Width = 137;
            // 
            // colDataTypeID
            // 
            this.colDataTypeID.Caption = "Data Type ID";
            this.colDataTypeID.FieldName = "DataTypeID";
            this.colDataTypeID.Name = "colDataTypeID";
            this.colDataTypeID.OptionsColumn.AllowEdit = false;
            this.colDataTypeID.OptionsColumn.AllowFocus = false;
            this.colDataTypeID.OptionsColumn.ReadOnly = true;
            this.colDataTypeID.Width = 85;
            // 
            // colEditorTypeID
            // 
            this.colEditorTypeID.Caption = "Editor Type ID";
            this.colEditorTypeID.FieldName = "EditorTypeID";
            this.colEditorTypeID.Name = "colEditorTypeID";
            this.colEditorTypeID.OptionsColumn.AllowEdit = false;
            this.colEditorTypeID.OptionsColumn.AllowFocus = false;
            this.colEditorTypeID.OptionsColumn.ReadOnly = true;
            this.colEditorTypeID.Width = 90;
            // 
            // colEditorMask
            // 
            this.colEditorMask.Caption = "Editor Mask";
            this.colEditorMask.FieldName = "EditorMask";
            this.colEditorMask.Name = "colEditorMask";
            this.colEditorMask.OptionsColumn.AllowEdit = false;
            this.colEditorMask.OptionsColumn.AllowFocus = false;
            this.colEditorMask.OptionsColumn.ReadOnly = true;
            this.colEditorMask.Visible = true;
            this.colEditorMask.VisibleIndex = 7;
            this.colEditorMask.Width = 121;
            // 
            // colDataEntryMinValue
            // 
            this.colDataEntryMinValue.Caption = "Data Entry Min Value";
            this.colDataEntryMinValue.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colDataEntryMinValue.FieldName = "DataEntryMinValue";
            this.colDataEntryMinValue.Name = "colDataEntryMinValue";
            this.colDataEntryMinValue.OptionsColumn.AllowEdit = false;
            this.colDataEntryMinValue.OptionsColumn.AllowFocus = false;
            this.colDataEntryMinValue.OptionsColumn.ReadOnly = true;
            this.colDataEntryMinValue.Visible = true;
            this.colDataEntryMinValue.VisibleIndex = 8;
            this.colDataEntryMinValue.Width = 121;
            // 
            // repositoryItemTextEdit2DP3
            // 
            this.repositoryItemTextEdit2DP3.AutoHeight = false;
            this.repositoryItemTextEdit2DP3.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP3.Name = "repositoryItemTextEdit2DP3";
            // 
            // colDataEntryMaxValue
            // 
            this.colDataEntryMaxValue.Caption = "Data Entry Max Value";
            this.colDataEntryMaxValue.ColumnEdit = this.repositoryItemTextEdit2DP3;
            this.colDataEntryMaxValue.FieldName = "DataEntryMaxValue";
            this.colDataEntryMaxValue.Name = "colDataEntryMaxValue";
            this.colDataEntryMaxValue.OptionsColumn.AllowEdit = false;
            this.colDataEntryMaxValue.OptionsColumn.AllowFocus = false;
            this.colDataEntryMaxValue.OptionsColumn.ReadOnly = true;
            this.colDataEntryMaxValue.Visible = true;
            this.colDataEntryMaxValue.VisibleIndex = 9;
            this.colDataEntryMaxValue.Width = 125;
            // 
            // colDataEntryRequired
            // 
            this.colDataEntryRequired.Caption = "Data Entry Required";
            this.colDataEntryRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDataEntryRequired.FieldName = "DataEntryRequired";
            this.colDataEntryRequired.Name = "colDataEntryRequired";
            this.colDataEntryRequired.OptionsColumn.AllowEdit = false;
            this.colDataEntryRequired.OptionsColumn.AllowFocus = false;
            this.colDataEntryRequired.OptionsColumn.ReadOnly = true;
            this.colDataEntryRequired.Visible = true;
            this.colDataEntryRequired.VisibleIndex = 6;
            this.colDataEntryRequired.Width = 119;
            // 
            // colPicklistHeaderID
            // 
            this.colPicklistHeaderID.Caption = "Picklist Header ID";
            this.colPicklistHeaderID.FieldName = "PicklistHeaderID";
            this.colPicklistHeaderID.Name = "colPicklistHeaderID";
            this.colPicklistHeaderID.OptionsColumn.AllowEdit = false;
            this.colPicklistHeaderID.OptionsColumn.AllowFocus = false;
            this.colPicklistHeaderID.OptionsColumn.ReadOnly = true;
            this.colPicklistHeaderID.Width = 104;
            // 
            // colOnScreenLabel
            // 
            this.colOnScreenLabel.Caption = "On-Screen Label";
            this.colOnScreenLabel.FieldName = "OnScreenLabel";
            this.colOnScreenLabel.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colOnScreenLabel.Name = "colOnScreenLabel";
            this.colOnScreenLabel.OptionsColumn.AllowEdit = false;
            this.colOnScreenLabel.OptionsColumn.AllowFocus = false;
            this.colOnScreenLabel.OptionsColumn.ReadOnly = true;
            this.colOnScreenLabel.Visible = true;
            this.colOnScreenLabel.VisibleIndex = 1;
            this.colOnScreenLabel.Width = 170;
            // 
            // colAttributeOrder
            // 
            this.colAttributeOrder.Caption = "Attribute Order";
            this.colAttributeOrder.FieldName = "AttributeOrder";
            this.colAttributeOrder.Name = "colAttributeOrder";
            this.colAttributeOrder.OptionsColumn.AllowEdit = false;
            this.colAttributeOrder.OptionsColumn.AllowFocus = false;
            this.colAttributeOrder.OptionsColumn.ReadOnly = true;
            this.colAttributeOrder.Visible = true;
            this.colAttributeOrder.VisibleIndex = 3;
            this.colAttributeOrder.Width = 108;
            // 
            // colDefaultValue
            // 
            this.colDefaultValue.Caption = "Default Value";
            this.colDefaultValue.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colDefaultValue.FieldName = "DefaultValue";
            this.colDefaultValue.Name = "colDefaultValue";
            this.colDefaultValue.OptionsColumn.ReadOnly = true;
            this.colDefaultValue.Visible = true;
            this.colDefaultValue.VisibleIndex = 10;
            this.colDefaultValue.Width = 85;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colRemarks5
            // 
            this.colRemarks5.Caption = "Remarks";
            this.colRemarks5.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks5.FieldName = "Remarks";
            this.colRemarks5.Name = "colRemarks5";
            this.colRemarks5.OptionsColumn.ReadOnly = true;
            this.colRemarks5.Visible = true;
            this.colRemarks5.VisibleIndex = 12;
            // 
            // colDataType
            // 
            this.colDataType.Caption = "Data Type";
            this.colDataType.FieldName = "DataType";
            this.colDataType.Name = "colDataType";
            this.colDataType.OptionsColumn.AllowEdit = false;
            this.colDataType.OptionsColumn.AllowFocus = false;
            this.colDataType.OptionsColumn.ReadOnly = true;
            this.colDataType.Visible = true;
            this.colDataType.VisibleIndex = 4;
            this.colDataType.Width = 88;
            // 
            // colEditorType
            // 
            this.colEditorType.Caption = "Editor Type";
            this.colEditorType.FieldName = "EditorType";
            this.colEditorType.Name = "colEditorType";
            this.colEditorType.OptionsColumn.AllowEdit = false;
            this.colEditorType.OptionsColumn.AllowFocus = false;
            this.colEditorType.OptionsColumn.ReadOnly = true;
            this.colEditorType.Visible = true;
            this.colEditorType.VisibleIndex = 5;
            this.colEditorType.Width = 91;
            // 
            // colPicklistDescription
            // 
            this.colPicklistDescription.Caption = "Picklist Description";
            this.colPicklistDescription.FieldName = "PicklistDescription";
            this.colPicklistDescription.Name = "colPicklistDescription";
            this.colPicklistDescription.OptionsColumn.AllowEdit = false;
            this.colPicklistDescription.OptionsColumn.AllowFocus = false;
            this.colPicklistDescription.OptionsColumn.ReadOnly = true;
            this.colPicklistDescription.Visible = true;
            this.colPicklistDescription.VisibleIndex = 11;
            this.colPicklistDescription.Width = 183;
            // 
            // colJobSubTypeDescription1
            // 
            this.colJobSubTypeDescription1.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription1.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription1.Name = "colJobSubTypeDescription1";
            this.colJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription1.Width = 87;
            // 
            // colJobTypeDescription1
            // 
            this.colJobTypeDescription1.Caption = "Job Type";
            this.colJobTypeDescription1.FieldName = "JobTypeDescription";
            this.colJobTypeDescription1.Name = "colJobTypeDescription1";
            this.colJobTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription1.OptionsColumn.ReadOnly = true;
            // 
            // colJobTypeJobSubType
            // 
            this.colJobTypeJobSubType.Caption = "Job Type \\ Job Sub-Type";
            this.colJobTypeJobSubType.FieldName = "JobTypeJobSubType";
            this.colJobTypeJobSubType.Name = "colJobTypeJobSubType";
            this.colJobTypeJobSubType.OptionsColumn.AllowEdit = false;
            this.colJobTypeJobSubType.OptionsColumn.AllowFocus = false;
            this.colJobTypeJobSubType.OptionsColumn.ReadOnly = true;
            this.colJobTypeJobSubType.Visible = true;
            this.colJobTypeJobSubType.VisibleIndex = 12;
            this.colJobTypeJobSubType.Width = 325;
            // 
            // colJobTypeID4
            // 
            this.colJobTypeID4.Caption = "Job Type ID";
            this.colJobTypeID4.FieldName = "JobTypeID";
            this.colJobTypeID4.Name = "colJobTypeID4";
            this.colJobTypeID4.OptionsColumn.AllowEdit = false;
            this.colJobTypeID4.OptionsColumn.AllowFocus = false;
            this.colJobTypeID4.OptionsColumn.ReadOnly = true;
            this.colJobTypeID4.Width = 79;
            // 
            // colAttributeName
            // 
            this.colAttributeName.Caption = "Attribute Name";
            this.colAttributeName.FieldName = "AttributeName";
            this.colAttributeName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAttributeName.Name = "colAttributeName";
            this.colAttributeName.OptionsColumn.AllowEdit = false;
            this.colAttributeName.OptionsColumn.AllowFocus = false;
            this.colAttributeName.OptionsColumn.ReadOnly = true;
            this.colAttributeName.Visible = true;
            this.colAttributeName.VisibleIndex = 0;
            this.colAttributeName.Width = 132;
            // 
            // colValueFormula
            // 
            this.colValueFormula.Caption = "Formula";
            this.colValueFormula.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colValueFormula.FieldName = "ValueFormula";
            this.colValueFormula.Name = "colValueFormula";
            this.colValueFormula.OptionsColumn.ReadOnly = true;
            this.colValueFormula.Visible = true;
            this.colValueFormula.VisibleIndex = 14;
            // 
            // colValueFormulaEvaluateLocation
            // 
            this.colValueFormulaEvaluateLocation.Caption = "Formula Evaluate Location ID";
            this.colValueFormulaEvaluateLocation.FieldName = "ValueFormulaEvaluateLocation";
            this.colValueFormulaEvaluateLocation.Name = "colValueFormulaEvaluateLocation";
            this.colValueFormulaEvaluateLocation.OptionsColumn.AllowEdit = false;
            this.colValueFormulaEvaluateLocation.OptionsColumn.AllowFocus = false;
            this.colValueFormulaEvaluateLocation.OptionsColumn.ReadOnly = true;
            this.colValueFormulaEvaluateLocation.Width = 159;
            // 
            // colValueFormulaEvaluateLocationDesc
            // 
            this.colValueFormulaEvaluateLocationDesc.Caption = "Formula Evaluate Location";
            this.colValueFormulaEvaluateLocationDesc.FieldName = "ValueFormulaEvaluateLocationDesc";
            this.colValueFormulaEvaluateLocationDesc.Name = "colValueFormulaEvaluateLocationDesc";
            this.colValueFormulaEvaluateLocationDesc.OptionsColumn.AllowEdit = false;
            this.colValueFormulaEvaluateLocationDesc.OptionsColumn.AllowFocus = false;
            this.colValueFormulaEvaluateLocationDesc.OptionsColumn.ReadOnly = true;
            this.colValueFormulaEvaluateLocationDesc.Visible = true;
            this.colValueFormulaEvaluateLocationDesc.VisibleIndex = 15;
            this.colValueFormulaEvaluateLocationDesc.Width = 145;
            // 
            // colValueFormulaEvaluateOrder
            // 
            this.colValueFormulaEvaluateOrder.Caption = "Formula Evaluate Order";
            this.colValueFormulaEvaluateOrder.FieldName = "ValueFormulaEvaluateOrder";
            this.colValueFormulaEvaluateOrder.Name = "colValueFormulaEvaluateOrder";
            this.colValueFormulaEvaluateOrder.OptionsColumn.AllowEdit = false;
            this.colValueFormulaEvaluateOrder.OptionsColumn.AllowFocus = false;
            this.colValueFormulaEvaluateOrder.OptionsColumn.ReadOnly = true;
            this.colValueFormulaEvaluateOrder.Visible = true;
            this.colValueFormulaEvaluateOrder.VisibleIndex = 16;
            this.colValueFormulaEvaluateOrder.Width = 133;
            // 
            // colHideFromApp
            // 
            this.colHideFromApp.Caption = "Hide From App";
            this.colHideFromApp.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colHideFromApp.FieldName = "HideFromApp";
            this.colHideFromApp.Name = "colHideFromApp";
            this.colHideFromApp.OptionsColumn.AllowEdit = false;
            this.colHideFromApp.OptionsColumn.AllowFocus = false;
            this.colHideFromApp.OptionsColumn.ReadOnly = true;
            this.colHideFromApp.Visible = true;
            this.colHideFromApp.VisibleIndex = 13;
            this.colHideFromApp.Width = 89;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl5);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1205, 206);
            this.xtraTabPage2.Text = "Linked Default Equipment";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07162UTJobManagerLinkedDefaultEquipmentBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record(s)", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemTextEditCurrency});
            this.gridControl5.Size = new System.Drawing.Size(1205, 206);
            this.gridControl5.TabIndex = 2;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07162UTJobManagerLinkedDefaultEquipmentBindingSource
            // 
            this.sp07162UTJobManagerLinkedDefaultEquipmentBindingSource.DataMember = "sp07162_UT_Job_Manager_Linked_Default_Equipment";
            this.sp07162UTJobManagerLinkedDefaultEquipmentBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDefaultRequiredEquipmentID,
            this.colJobTypeID,
            this.colEquipmentID,
            this.colRemarks1,
            this.colGUID,
            this.colDescription1,
            this.colCode,
            this.colCostPerHour,
            this.colSellPerHour,
            this.colJobDescription1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colDefaultRequiredEquipmentID
            // 
            this.colDefaultRequiredEquipmentID.Caption = "Default Equipment ID";
            this.colDefaultRequiredEquipmentID.FieldName = "DefaultRequiredEquipmentID";
            this.colDefaultRequiredEquipmentID.Name = "colDefaultRequiredEquipmentID";
            this.colDefaultRequiredEquipmentID.OptionsColumn.AllowEdit = false;
            this.colDefaultRequiredEquipmentID.OptionsColumn.AllowFocus = false;
            this.colDefaultRequiredEquipmentID.OptionsColumn.ReadOnly = true;
            this.colDefaultRequiredEquipmentID.Width = 123;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colEquipmentID
            // 
            this.colEquipmentID.Caption = "Equipment ID";
            this.colEquipmentID.FieldName = "EquipmentID";
            this.colEquipmentID.Name = "colEquipmentID";
            this.colEquipmentID.OptionsColumn.AllowEdit = false;
            this.colEquipmentID.OptionsColumn.AllowFocus = false;
            this.colEquipmentID.OptionsColumn.ReadOnly = true;
            this.colEquipmentID.Width = 85;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 4;
            this.colRemarks1.Width = 147;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Equipment Description";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 292;
            // 
            // colCode
            // 
            this.colCode.Caption = "Equipment Code";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            this.colCode.OptionsColumn.AllowEdit = false;
            this.colCode.OptionsColumn.AllowFocus = false;
            this.colCode.OptionsColumn.ReadOnly = true;
            this.colCode.Visible = true;
            this.colCode.VisibleIndex = 1;
            this.colCode.Width = 121;
            // 
            // colCostPerHour
            // 
            this.colCostPerHour.Caption = "Cost Per Hour";
            this.colCostPerHour.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCostPerHour.FieldName = "CostPerHour";
            this.colCostPerHour.Name = "colCostPerHour";
            this.colCostPerHour.OptionsColumn.AllowEdit = false;
            this.colCostPerHour.OptionsColumn.AllowFocus = false;
            this.colCostPerHour.OptionsColumn.ReadOnly = true;
            this.colCostPerHour.Visible = true;
            this.colCostPerHour.VisibleIndex = 2;
            this.colCostPerHour.Width = 88;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colSellPerHour
            // 
            this.colSellPerHour.Caption = "Sell Per Hour";
            this.colSellPerHour.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSellPerHour.FieldName = "SellPerHour";
            this.colSellPerHour.Name = "colSellPerHour";
            this.colSellPerHour.OptionsColumn.AllowEdit = false;
            this.colSellPerHour.OptionsColumn.AllowFocus = false;
            this.colSellPerHour.OptionsColumn.ReadOnly = true;
            this.colSellPerHour.Visible = true;
            this.colSellPerHour.VisibleIndex = 3;
            this.colSellPerHour.Width = 82;
            // 
            // colJobDescription1
            // 
            this.colJobDescription1.Caption = "Linked To";
            this.colJobDescription1.FieldName = "JobDescription";
            this.colJobDescription1.Name = "colJobDescription1";
            this.colJobDescription1.OptionsColumn.AllowEdit = false;
            this.colJobDescription1.OptionsColumn.AllowFocus = false;
            this.colJobDescription1.OptionsColumn.ReadOnly = true;
            this.colJobDescription1.Visible = true;
            this.colJobDescription1.VisibleIndex = 6;
            this.colJobDescription1.Width = 320;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1205, 206);
            this.xtraTabPage1.Text = "Linked Default Materials";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl3;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1205, 206);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp07161UTJobManagerLinkedDefaultMaterialsBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record(s)", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEdit2DP});
            this.gridControl3.Size = new System.Drawing.Size(1205, 206);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp07161UTJobManagerLinkedDefaultMaterialsBindingSource
            // 
            this.sp07161UTJobManagerLinkedDefaultMaterialsBindingSource.DataMember = "sp07161_UT_Job_Manager_Linked_Default_Materials";
            this.sp07161UTJobManagerLinkedDefaultMaterialsBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDefaultRequiredMaterialID,
            this.colJobTypeID1,
            this.colMaterialID,
            this.colRemarks2,
            this.colGUID1,
            this.colDescription,
            this.colCode1,
            this.colCostPerUnit,
            this.colSellPerUnit,
            this.colUnitDescriptorID,
            this.colUnitsInStock,
            this.colWarningLevel,
            this.colReorderLevel,
            this.colJobDescription2,
            this.colUnitDescriptor});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDefaultRequiredMaterialID
            // 
            this.colDefaultRequiredMaterialID.Caption = "Default Material ID";
            this.colDefaultRequiredMaterialID.FieldName = "DefaultRequiredMaterialID";
            this.colDefaultRequiredMaterialID.Name = "colDefaultRequiredMaterialID";
            this.colDefaultRequiredMaterialID.OptionsColumn.AllowEdit = false;
            this.colDefaultRequiredMaterialID.OptionsColumn.AllowFocus = false;
            this.colDefaultRequiredMaterialID.OptionsColumn.ReadOnly = true;
            this.colDefaultRequiredMaterialID.Width = 111;
            // 
            // colJobTypeID1
            // 
            this.colJobTypeID1.Caption = "Job ID";
            this.colJobTypeID1.FieldName = "JobTypeID";
            this.colJobTypeID1.Name = "colJobTypeID1";
            this.colJobTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobTypeID1.OptionsColumn.ReadOnly = true;
            // 
            // colMaterialID
            // 
            this.colMaterialID.Caption = "Material ID";
            this.colMaterialID.FieldName = "MaterialID";
            this.colMaterialID.Name = "colMaterialID";
            this.colMaterialID.OptionsColumn.AllowEdit = false;
            this.colMaterialID.OptionsColumn.AllowFocus = false;
            this.colMaterialID.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.AllowEdit = false;
            this.colRemarks2.OptionsColumn.AllowFocus = false;
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 8;
            this.colRemarks2.Width = 139;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Material Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 277;
            // 
            // colCode1
            // 
            this.colCode1.Caption = "Material Code";
            this.colCode1.FieldName = "Code";
            this.colCode1.Name = "colCode1";
            this.colCode1.OptionsColumn.AllowEdit = false;
            this.colCode1.OptionsColumn.AllowFocus = false;
            this.colCode1.OptionsColumn.ReadOnly = true;
            this.colCode1.Visible = true;
            this.colCode1.VisibleIndex = 1;
            this.colCode1.Width = 123;
            // 
            // colCostPerUnit
            // 
            this.colCostPerUnit.Caption = "Cost Per Unit";
            this.colCostPerUnit.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colCostPerUnit.FieldName = "CostPerUnit";
            this.colCostPerUnit.Name = "colCostPerUnit";
            this.colCostPerUnit.OptionsColumn.AllowEdit = false;
            this.colCostPerUnit.OptionsColumn.AllowFocus = false;
            this.colCostPerUnit.OptionsColumn.ReadOnly = true;
            this.colCostPerUnit.Visible = true;
            this.colCostPerUnit.VisibleIndex = 3;
            this.colCostPerUnit.Width = 84;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colSellPerUnit
            // 
            this.colSellPerUnit.Caption = "Sell Per Unit";
            this.colSellPerUnit.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colSellPerUnit.FieldName = "SellPerUnit";
            this.colSellPerUnit.Name = "colSellPerUnit";
            this.colSellPerUnit.OptionsColumn.AllowEdit = false;
            this.colSellPerUnit.OptionsColumn.AllowFocus = false;
            this.colSellPerUnit.OptionsColumn.ReadOnly = true;
            this.colSellPerUnit.Visible = true;
            this.colSellPerUnit.VisibleIndex = 4;
            this.colSellPerUnit.Width = 78;
            // 
            // colUnitDescriptorID
            // 
            this.colUnitDescriptorID.Caption = "Unit Description ID";
            this.colUnitDescriptorID.FieldName = "UnitDescriptorID";
            this.colUnitDescriptorID.Name = "colUnitDescriptorID";
            this.colUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colUnitDescriptorID.Width = 110;
            // 
            // colUnitsInStock
            // 
            this.colUnitsInStock.Caption = "Units In Stock";
            this.colUnitsInStock.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colUnitsInStock.FieldName = "UnitsInStock";
            this.colUnitsInStock.Name = "colUnitsInStock";
            this.colUnitsInStock.OptionsColumn.AllowEdit = false;
            this.colUnitsInStock.OptionsColumn.AllowFocus = false;
            this.colUnitsInStock.OptionsColumn.ReadOnly = true;
            this.colUnitsInStock.Visible = true;
            this.colUnitsInStock.VisibleIndex = 5;
            this.colUnitsInStock.Width = 87;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colWarningLevel
            // 
            this.colWarningLevel.Caption = "Warning Level";
            this.colWarningLevel.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colWarningLevel.FieldName = "WarningLevel";
            this.colWarningLevel.Name = "colWarningLevel";
            this.colWarningLevel.OptionsColumn.AllowEdit = false;
            this.colWarningLevel.OptionsColumn.AllowFocus = false;
            this.colWarningLevel.OptionsColumn.ReadOnly = true;
            this.colWarningLevel.Visible = true;
            this.colWarningLevel.VisibleIndex = 6;
            this.colWarningLevel.Width = 89;
            // 
            // colReorderLevel
            // 
            this.colReorderLevel.Caption = "Reorder Level";
            this.colReorderLevel.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colReorderLevel.FieldName = "ReorderLevel";
            this.colReorderLevel.Name = "colReorderLevel";
            this.colReorderLevel.OptionsColumn.AllowEdit = false;
            this.colReorderLevel.OptionsColumn.AllowFocus = false;
            this.colReorderLevel.OptionsColumn.ReadOnly = true;
            this.colReorderLevel.Visible = true;
            this.colReorderLevel.VisibleIndex = 7;
            this.colReorderLevel.Width = 88;
            // 
            // colJobDescription2
            // 
            this.colJobDescription2.Caption = "Linked To";
            this.colJobDescription2.FieldName = "JobDescription";
            this.colJobDescription2.Name = "colJobDescription2";
            this.colJobDescription2.OptionsColumn.AllowEdit = false;
            this.colJobDescription2.OptionsColumn.AllowFocus = false;
            this.colJobDescription2.OptionsColumn.ReadOnly = true;
            this.colJobDescription2.Visible = true;
            this.colJobDescription2.VisibleIndex = 9;
            this.colJobDescription2.Width = 343;
            // 
            // colUnitDescriptor
            // 
            this.colUnitDescriptor.Caption = "Unit Descriptor";
            this.colUnitDescriptor.FieldName = "UnitDescriptor";
            this.colUnitDescriptor.Name = "colUnitDescriptor";
            this.colUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colUnitDescriptor.Visible = true;
            this.colUnitDescriptor.VisibleIndex = 2;
            this.colUnitDescriptor.Width = 107;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl2);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1205, 206);
            this.xtraTabPage3.Text = "Linked Reference Files";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07234UTLinkedReferenceFilesBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Deleted Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEditDomcumentRemarks,
            this.repositoryItemHyperLinkEditDocumentPath,
            this.repositoryItemTextEditDateTimeShortTime});
            this.gridControl2.Size = new System.Drawing.Size(1205, 206);
            this.gridControl2.TabIndex = 2;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07234UTLinkedReferenceFilesBindingSource
            // 
            this.sp07234UTLinkedReferenceFilesBindingSource.DataMember = "sp07234_UT_Linked_Reference_Files";
            this.sp07234UTLinkedReferenceFilesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReferenceFileID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.gridColumn1,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colReferenceFileID
            // 
            this.colReferenceFileID.Caption = "Reference File ID";
            this.colReferenceFileID.FieldName = "ReferenceFileID";
            this.colReferenceFileID.Name = "colReferenceFileID";
            this.colReferenceFileID.OptionsColumn.AllowEdit = false;
            this.colReferenceFileID.OptionsColumn.AllowFocus = false;
            this.colReferenceFileID.OptionsColumn.ReadOnly = true;
            this.colReferenceFileID.Width = 104;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked To Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 117;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 144;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Linked File";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEditDocumentPath;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 1;
            this.colDocumentPath.Width = 278;
            // 
            // repositoryItemHyperLinkEditDocumentPath
            // 
            this.repositoryItemHyperLinkEditDocumentPath.AutoHeight = false;
            this.repositoryItemHyperLinkEditDocumentPath.Name = "repositoryItemHyperLinkEditDocumentPath";
            this.repositoryItemHyperLinkEditDocumentPath.SingleClick = true;
            this.repositoryItemHyperLinkEditDocumentPath.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditDocumentPath_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "File Extension";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Width = 87;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Description";
            this.gridColumn1.FieldName = "Description";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 223;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.ColumnEdit = this.repositoryItemTextEditDateTimeShortTime;
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 3;
            this.colDateAdded.Width = 101;
            // 
            // repositoryItemTextEditDateTimeShortTime
            // 
            this.repositoryItemTextEditDateTimeShortTime.AutoHeight = false;
            this.repositoryItemTextEditDateTimeShortTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTimeShortTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTimeShortTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTimeShortTime.Name = "repositoryItemTextEditDateTimeShortTime";
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Visible = true;
            this.colLinkedRecordDescription.VisibleIndex = 5;
            this.colLinkedRecordDescription.Width = 364;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 2;
            this.colAddedByStaffName.Width = 123;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEditDomcumentRemarks;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 4;
            // 
            // repositoryItemMemoExEditDomcumentRemarks
            // 
            this.repositoryItemMemoExEditDomcumentRemarks.AutoHeight = false;
            this.repositoryItemMemoExEditDomcumentRemarks.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditDomcumentRemarks.Name = "repositoryItemMemoExEditDomcumentRemarks";
            this.repositoryItemMemoExEditDomcumentRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEditDomcumentRemarks.ShowIcon = false;
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.gridControl14);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(1205, 206);
            this.xtraTabPage7.Text = "Linked Visit Types";
            // 
            // gridControl14
            // 
            this.gridControl14.DataSource = this.sp06313OMJobTypeVisitTypesBindingSource;
            this.gridControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl14.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl14.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl14_EmbeddedNavigator_ButtonClick);
            this.gridControl14.Location = new System.Drawing.Point(0, 0);
            this.gridControl14.MainView = this.gridView14;
            this.gridControl14.MenuManager = this.barManager1;
            this.gridControl14.Name = "gridControl14";
            this.gridControl14.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11});
            this.gridControl14.Size = new System.Drawing.Size(1205, 206);
            this.gridControl14.TabIndex = 2;
            this.gridControl14.UseEmbeddedNavigator = true;
            this.gridControl14.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView14});
            // 
            // sp06313OMJobTypeVisitTypesBindingSource
            // 
            this.sp06313OMJobTypeVisitTypesBindingSource.DataMember = "sp06313_OM_Job_Type_Visit_Types";
            this.sp06313OMJobTypeVisitTypesBindingSource.DataSource = this.dataSet_OM_Core;
            // 
            // dataSet_OM_Core
            // 
            this.dataSet_OM_Core.DataSetName = "DataSet_OM_Core";
            this.dataSet_OM_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitTypeJobID,
            this.colJobTypeID5,
            this.colVisitTypeID,
            this.colRemarks6,
            this.colJobDescription4,
            this.colVisitTypeDescription});
            this.gridView14.GridControl = this.gridControl14;
            this.gridView14.GroupCount = 1;
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView14.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.MultiSelect = true;
            this.gridView14.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription4, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView14.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView14.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView14.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView14.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView14.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView14_MouseUp);
            this.gridView14.DoubleClick += new System.EventHandler(this.gridView14_DoubleClick);
            this.gridView14.GotFocus += new System.EventHandler(this.gridView14_GotFocus);
            // 
            // colVisitTypeJobID
            // 
            this.colVisitTypeJobID.Caption = "Visit Type Job ID";
            this.colVisitTypeJobID.FieldName = "VisitTypeJobID";
            this.colVisitTypeJobID.Name = "colVisitTypeJobID";
            this.colVisitTypeJobID.OptionsColumn.AllowEdit = false;
            this.colVisitTypeJobID.OptionsColumn.AllowFocus = false;
            this.colVisitTypeJobID.OptionsColumn.ReadOnly = true;
            this.colVisitTypeJobID.Width = 99;
            // 
            // colJobTypeID5
            // 
            this.colJobTypeID5.Caption = "Job Type ID";
            this.colJobTypeID5.FieldName = "JobTypeID";
            this.colJobTypeID5.Name = "colJobTypeID5";
            this.colJobTypeID5.OptionsColumn.AllowEdit = false;
            this.colJobTypeID5.OptionsColumn.AllowFocus = false;
            this.colJobTypeID5.OptionsColumn.ReadOnly = true;
            this.colJobTypeID5.Width = 77;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type ID";
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.OptionsColumn.AllowEdit = false;
            this.colVisitTypeID.OptionsColumn.AllowFocus = false;
            this.colVisitTypeID.OptionsColumn.ReadOnly = true;
            this.colVisitTypeID.Width = 79;
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit11;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.OptionsColumn.ReadOnly = true;
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 1;
            this.colRemarks6.Width = 227;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // colJobDescription4
            // 
            this.colJobDescription4.Caption = "Linked to Job";
            this.colJobDescription4.FieldName = "JobDescription";
            this.colJobDescription4.Name = "colJobDescription4";
            this.colJobDescription4.OptionsColumn.AllowEdit = false;
            this.colJobDescription4.OptionsColumn.AllowFocus = false;
            this.colJobDescription4.OptionsColumn.ReadOnly = true;
            this.colJobDescription4.Visible = true;
            this.colJobDescription4.VisibleIndex = 2;
            this.colJobDescription4.Width = 255;
            // 
            // colVisitTypeDescription
            // 
            this.colVisitTypeDescription.Caption = "Visit Type";
            this.colVisitTypeDescription.FieldName = "VisitTypeDescription";
            this.colVisitTypeDescription.Name = "colVisitTypeDescription";
            this.colVisitTypeDescription.OptionsColumn.AllowEdit = false;
            this.colVisitTypeDescription.OptionsColumn.AllowFocus = false;
            this.colVisitTypeDescription.OptionsColumn.ReadOnly = true;
            this.colVisitTypeDescription.Visible = true;
            this.colVisitTypeDescription.VisibleIndex = 0;
            this.colVisitTypeDescription.Width = 233;
            // 
            // xtraTabPageJobRates
            // 
            this.xtraTabPageJobRates.Controls.Add(this.splitContainerControlJobRates);
            this.xtraTabPageJobRates.Name = "xtraTabPageJobRates";
            this.xtraTabPageJobRates.Size = new System.Drawing.Size(1205, 206);
            this.xtraTabPageJobRates.Text = "Amenity Trees Job Rates";
            // 
            // splitContainerControlJobRates
            // 
            this.splitContainerControlJobRates.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControlJobRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControlJobRates.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControlJobRates.Name = "splitContainerControlJobRates";
            this.splitContainerControlJobRates.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControlJobRates.Panel1.Controls.Add(this.gridControlJobRate);
            this.splitContainerControlJobRates.Panel1.ShowCaption = true;
            this.splitContainerControlJobRates.Panel1.Text = "Job Rates";
            this.splitContainerControlJobRates.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControlJobRates.Panel2.Controls.Add(this.gridControlJobRateCriteria);
            this.splitContainerControlJobRates.Panel2.ShowCaption = true;
            this.splitContainerControlJobRates.Panel2.Text = "Linked Contractors \\ Criteria";
            this.splitContainerControlJobRates.Size = new System.Drawing.Size(1205, 206);
            this.splitContainerControlJobRates.SplitterPosition = 815;
            this.splitContainerControlJobRates.TabIndex = 0;
            this.splitContainerControlJobRates.Text = "splitContainerControl3";
            // 
            // gridControlJobRate
            // 
            this.gridControlJobRate.DataSource = this.sp00205JobRateManagerJobRatesBindingSource;
            this.gridControlJobRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "refresh"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Adjust Rates by Uplift Amount", "adjust rates")});
            this.gridControlJobRate.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlJobRate_EmbeddedNavigator_ButtonClick);
            this.gridControlJobRate.Location = new System.Drawing.Point(0, 0);
            this.gridControlJobRate.MainView = this.gridViewJobRate;
            this.gridControlJobRate.MenuManager = this.barManager1;
            this.gridControlJobRate.Name = "gridControlJobRate";
            this.gridControlJobRate.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEditDateTime3});
            this.gridControlJobRate.Size = new System.Drawing.Size(811, 182);
            this.gridControlJobRate.TabIndex = 1;
            this.gridControlJobRate.UseEmbeddedNavigator = true;
            this.gridControlJobRate.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewJobRate});
            // 
            // sp00205JobRateManagerJobRatesBindingSource
            // 
            this.sp00205JobRateManagerJobRatesBindingSource.DataMember = "sp00205_Job_Rate_Manager_Job_Rates";
            this.sp00205JobRateManagerJobRatesBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridViewJobRate
            // 
            this.gridViewJobRate.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRateID,
            this.colJobID1,
            this.colValidFrom,
            this.colValidTo,
            this.colJobRate,
            this.colJobCode1,
            this.gridColumn2,
            this.colRateTypeID,
            this.colRateUnits,
            this.colRateTypeDescription,
            this.colRateDescription,
            this.gridColumn3});
            this.gridViewJobRate.GridControl = this.gridControlJobRate;
            this.gridViewJobRate.GroupCount = 1;
            this.gridViewJobRate.Name = "gridViewJobRate";
            this.gridViewJobRate.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewJobRate.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewJobRate.OptionsLayout.StoreAppearance = true;
            this.gridViewJobRate.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewJobRate.OptionsSelection.MultiSelect = true;
            this.gridViewJobRate.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewJobRate.OptionsView.ColumnAutoWidth = false;
            this.gridViewJobRate.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewJobRate.OptionsView.ShowGroupPanel = false;
            this.gridViewJobRate.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRateTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRateDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colValidFrom, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colValidTo, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewJobRate.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewJobRate_CustomRowCellEdit);
            this.gridViewJobRate.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewJobRate.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewJobRate_SelectionChanged);
            this.gridViewJobRate.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewJobRate.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewJobRate.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewJobRate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewJobRate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewJobRate_MouseUp);
            this.gridViewJobRate.DoubleClick += new System.EventHandler(this.gridViewJobRate_DoubleClick);
            this.gridViewJobRate.GotFocus += new System.EventHandler(this.gridViewJobRate_GotFocus);
            // 
            // colRateID
            // 
            this.colRateID.Caption = "Rate ID";
            this.colRateID.FieldName = "RateID";
            this.colRateID.Name = "colRateID";
            this.colRateID.OptionsColumn.AllowEdit = false;
            this.colRateID.OptionsColumn.AllowFocus = false;
            this.colRateID.OptionsColumn.ReadOnly = true;
            this.colRateID.Width = 58;
            // 
            // colJobID1
            // 
            this.colJobID1.Caption = "Job ID";
            this.colJobID1.FieldName = "JobID";
            this.colJobID1.Name = "colJobID1";
            this.colJobID1.OptionsColumn.AllowEdit = false;
            this.colJobID1.OptionsColumn.AllowFocus = false;
            this.colJobID1.OptionsColumn.ReadOnly = true;
            this.colJobID1.Width = 52;
            // 
            // colValidFrom
            // 
            this.colValidFrom.Caption = "Valid From";
            this.colValidFrom.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colValidFrom.FieldName = "ValidFrom";
            this.colValidFrom.Name = "colValidFrom";
            this.colValidFrom.OptionsColumn.AllowEdit = false;
            this.colValidFrom.OptionsColumn.AllowFocus = false;
            this.colValidFrom.OptionsColumn.ReadOnly = true;
            this.colValidFrom.Visible = true;
            this.colValidFrom.VisibleIndex = 2;
            this.colValidFrom.Width = 90;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colValidTo
            // 
            this.colValidTo.Caption = "Valid To";
            this.colValidTo.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colValidTo.FieldName = "ValidTo";
            this.colValidTo.Name = "colValidTo";
            this.colValidTo.OptionsColumn.AllowEdit = false;
            this.colValidTo.OptionsColumn.AllowFocus = false;
            this.colValidTo.OptionsColumn.ReadOnly = true;
            this.colValidTo.Visible = true;
            this.colValidTo.VisibleIndex = 3;
            this.colValidTo.Width = 76;
            // 
            // colJobRate
            // 
            this.colJobRate.Caption = "Rate";
            this.colJobRate.ColumnEdit = this.repositoryItemTextEdit2;
            this.colJobRate.FieldName = "JobRate";
            this.colJobRate.Name = "colJobRate";
            this.colJobRate.OptionsColumn.AllowEdit = false;
            this.colJobRate.OptionsColumn.AllowFocus = false;
            this.colJobRate.OptionsColumn.ReadOnly = true;
            this.colJobRate.Visible = true;
            this.colJobRate.VisibleIndex = 4;
            this.colJobRate.Width = 82;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colJobCode1
            // 
            this.colJobCode1.Caption = "Job Code";
            this.colJobCode1.FieldName = "JobCode";
            this.colJobCode1.Name = "colJobCode1";
            this.colJobCode1.OptionsColumn.AllowEdit = false;
            this.colJobCode1.OptionsColumn.AllowFocus = false;
            this.colJobCode1.OptionsColumn.ReadOnly = true;
            this.colJobCode1.Width = 66;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Job Description";
            this.gridColumn2.FieldName = "JobDescription";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 228;
            // 
            // colRateTypeID
            // 
            this.colRateTypeID.Caption = "Rate Type ID";
            this.colRateTypeID.FieldName = "RateTypeID";
            this.colRateTypeID.Name = "colRateTypeID";
            this.colRateTypeID.OptionsColumn.AllowEdit = false;
            this.colRateTypeID.OptionsColumn.AllowFocus = false;
            this.colRateTypeID.OptionsColumn.ReadOnly = true;
            this.colRateTypeID.Width = 85;
            // 
            // colRateUnits
            // 
            this.colRateUnits.Caption = "Applied To";
            this.colRateUnits.ColumnEdit = this.repositoryItemTextEdit4;
            this.colRateUnits.FieldName = "RateUnits";
            this.colRateUnits.Name = "colRateUnits";
            this.colRateUnits.OptionsColumn.AllowEdit = false;
            this.colRateUnits.OptionsColumn.AllowFocus = false;
            this.colRateUnits.OptionsColumn.ReadOnly = true;
            this.colRateUnits.Visible = true;
            this.colRateUnits.VisibleIndex = 5;
            this.colRateUnits.Width = 110;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "Work Units > ######0.00";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // colRateTypeDescription
            // 
            this.colRateTypeDescription.Caption = "Rate Type";
            this.colRateTypeDescription.FieldName = "RateTypeDescription";
            this.colRateTypeDescription.Name = "colRateTypeDescription";
            this.colRateTypeDescription.OptionsColumn.AllowEdit = false;
            this.colRateTypeDescription.OptionsColumn.AllowFocus = false;
            this.colRateTypeDescription.OptionsColumn.ReadOnly = true;
            this.colRateTypeDescription.Visible = true;
            this.colRateTypeDescription.VisibleIndex = 0;
            this.colRateTypeDescription.Width = 109;
            // 
            // colRateDescription
            // 
            this.colRateDescription.Caption = "Rate Description";
            this.colRateDescription.FieldName = "RateDescription";
            this.colRateDescription.Name = "colRateDescription";
            this.colRateDescription.OptionsColumn.AllowEdit = false;
            this.colRateDescription.OptionsColumn.AllowFocus = false;
            this.colRateDescription.OptionsColumn.ReadOnly = true;
            this.colRateDescription.Visible = true;
            this.colRateDescription.VisibleIndex = 1;
            this.colRateDescription.Width = 272;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Remarks";
            this.gridColumn3.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.gridColumn3.FieldName = "Remarks";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 6;
            this.gridColumn3.Width = 159;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // gridControlJobRateCriteria
            // 
            this.gridControlJobRateCriteria.DataSource = this.sp00206JobRateManagerJobRateParametersBindingSource;
            this.gridControlJobRateCriteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlJobRateCriteria.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "refresh")});
            this.gridControlJobRateCriteria.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlJobRateCriteria_EmbeddedNavigator_ButtonClick);
            this.gridControlJobRateCriteria.Location = new System.Drawing.Point(0, 0);
            this.gridControlJobRateCriteria.MainView = this.gridViewJobRateCriteria;
            this.gridControlJobRateCriteria.MenuManager = this.barManager1;
            this.gridControlJobRateCriteria.Name = "gridControlJobRateCriteria";
            this.gridControlJobRateCriteria.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit5});
            this.gridControlJobRateCriteria.Size = new System.Drawing.Size(380, 182);
            this.gridControlJobRateCriteria.TabIndex = 1;
            this.gridControlJobRateCriteria.UseEmbeddedNavigator = true;
            this.gridControlJobRateCriteria.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewJobRateCriteria});
            // 
            // sp00206JobRateManagerJobRateParametersBindingSource
            // 
            this.sp00206JobRateManagerJobRateParametersBindingSource.DataMember = "sp00206_Job_Rate_Manager_Job_Rate_Parameters";
            this.sp00206JobRateManagerJobRateParametersBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridViewJobRateCriteria
            // 
            this.gridViewJobRateCriteria.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colstrJobcode,
            this.colstrJobdescription,
            this.colstrRateTypeDescription,
            this.colstrRateDesc,
            this.colmonRate,
            this.coldecRateUnits,
            this.colstrContractorName,
            this.colstrCriteriaDescription,
            this.colintCriteriaID,
            this.colintContractorID,
            this.colintParameterID,
            this.colintRateID,
            this.colintJobID,
            this.colintRateType});
            this.gridViewJobRateCriteria.GridControl = this.gridControlJobRateCriteria;
            this.gridViewJobRateCriteria.GroupCount = 1;
            this.gridViewJobRateCriteria.Name = "gridViewJobRateCriteria";
            this.gridViewJobRateCriteria.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewJobRateCriteria.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewJobRateCriteria.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewJobRateCriteria.OptionsLayout.StoreAppearance = true;
            this.gridViewJobRateCriteria.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewJobRateCriteria.OptionsSelection.MultiSelect = true;
            this.gridViewJobRateCriteria.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewJobRateCriteria.OptionsView.ColumnAutoWidth = false;
            this.gridViewJobRateCriteria.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewJobRateCriteria.OptionsView.ShowGroupPanel = false;
            this.gridViewJobRateCriteria.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrJobdescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstrRateTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewJobRateCriteria.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewJobRateCriteria_CustomRowCellEdit);
            this.gridViewJobRateCriteria.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewJobRateCriteria.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewJobRateCriteria.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewJobRateCriteria.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewJobRateCriteria.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewJobRateCriteria.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewJobRateCriteria.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewJobRateCriteria_MouseUp);
            this.gridViewJobRateCriteria.DoubleClick += new System.EventHandler(this.gridViewJobRateCriteria_DoubleClick);
            this.gridViewJobRateCriteria.GotFocus += new System.EventHandler(this.gridViewJobRateCriteria_GotFocus);
            // 
            // colstrJobcode
            // 
            this.colstrJobcode.Caption = "Job Code";
            this.colstrJobcode.FieldName = "strJobcode";
            this.colstrJobcode.Name = "colstrJobcode";
            this.colstrJobcode.OptionsColumn.AllowEdit = false;
            this.colstrJobcode.OptionsColumn.AllowFocus = false;
            this.colstrJobcode.OptionsColumn.ReadOnly = true;
            this.colstrJobcode.Width = 66;
            // 
            // colstrJobdescription
            // 
            this.colstrJobdescription.Caption = "Job Name";
            this.colstrJobdescription.FieldName = "strJobdescription";
            this.colstrJobdescription.Name = "colstrJobdescription";
            this.colstrJobdescription.OptionsColumn.AllowEdit = false;
            this.colstrJobdescription.OptionsColumn.AllowFocus = false;
            this.colstrJobdescription.OptionsColumn.ReadOnly = true;
            this.colstrJobdescription.Width = 248;
            // 
            // colstrRateTypeDescription
            // 
            this.colstrRateTypeDescription.Caption = "Rate Type";
            this.colstrRateTypeDescription.FieldName = "strRateTypeDescription";
            this.colstrRateTypeDescription.Name = "colstrRateTypeDescription";
            this.colstrRateTypeDescription.OptionsColumn.AllowEdit = false;
            this.colstrRateTypeDescription.OptionsColumn.AllowFocus = false;
            this.colstrRateTypeDescription.OptionsColumn.ReadOnly = true;
            this.colstrRateTypeDescription.Visible = true;
            this.colstrRateTypeDescription.VisibleIndex = 0;
            this.colstrRateTypeDescription.Width = 106;
            // 
            // colstrRateDesc
            // 
            this.colstrRateDesc.Caption = "Rate Description";
            this.colstrRateDesc.FieldName = "strRateDesc";
            this.colstrRateDesc.Name = "colstrRateDesc";
            this.colstrRateDesc.OptionsColumn.AllowEdit = false;
            this.colstrRateDesc.OptionsColumn.AllowFocus = false;
            this.colstrRateDesc.OptionsColumn.ReadOnly = true;
            this.colstrRateDesc.Visible = true;
            this.colstrRateDesc.VisibleIndex = 1;
            this.colstrRateDesc.Width = 272;
            // 
            // colmonRate
            // 
            this.colmonRate.Caption = "Rate";
            this.colmonRate.ColumnEdit = this.repositoryItemTextEdit3;
            this.colmonRate.FieldName = "monRate";
            this.colmonRate.Name = "colmonRate";
            this.colmonRate.OptionsColumn.AllowEdit = false;
            this.colmonRate.OptionsColumn.AllowFocus = false;
            this.colmonRate.OptionsColumn.ReadOnly = true;
            this.colmonRate.Visible = true;
            this.colmonRate.VisibleIndex = 2;
            this.colmonRate.Width = 65;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "c";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // coldecRateUnits
            // 
            this.coldecRateUnits.Caption = "Applied To";
            this.coldecRateUnits.ColumnEdit = this.repositoryItemTextEdit5;
            this.coldecRateUnits.FieldName = "decRateUnits";
            this.coldecRateUnits.Name = "coldecRateUnits";
            this.coldecRateUnits.OptionsColumn.AllowEdit = false;
            this.coldecRateUnits.OptionsColumn.AllowFocus = false;
            this.coldecRateUnits.OptionsColumn.ReadOnly = true;
            this.coldecRateUnits.Visible = true;
            this.coldecRateUnits.VisibleIndex = 3;
            this.coldecRateUnits.Width = 110;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "Work Units > ######0.00";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // colstrContractorName
            // 
            this.colstrContractorName.Caption = "Contractor";
            this.colstrContractorName.FieldName = "strContractorName";
            this.colstrContractorName.Name = "colstrContractorName";
            this.colstrContractorName.OptionsColumn.AllowEdit = false;
            this.colstrContractorName.OptionsColumn.AllowFocus = false;
            this.colstrContractorName.OptionsColumn.ReadOnly = true;
            this.colstrContractorName.Visible = true;
            this.colstrContractorName.VisibleIndex = 4;
            this.colstrContractorName.Width = 187;
            // 
            // colstrCriteriaDescription
            // 
            this.colstrCriteriaDescription.Caption = "Criteria";
            this.colstrCriteriaDescription.FieldName = "strCriteriaDescription";
            this.colstrCriteriaDescription.Name = "colstrCriteriaDescription";
            this.colstrCriteriaDescription.OptionsColumn.AllowEdit = false;
            this.colstrCriteriaDescription.OptionsColumn.AllowFocus = false;
            this.colstrCriteriaDescription.OptionsColumn.ReadOnly = true;
            this.colstrCriteriaDescription.Visible = true;
            this.colstrCriteriaDescription.VisibleIndex = 5;
            this.colstrCriteriaDescription.Width = 213;
            // 
            // colintCriteriaID
            // 
            this.colintCriteriaID.Caption = "Criteria ID";
            this.colintCriteriaID.FieldName = "intCriteriaID";
            this.colintCriteriaID.Name = "colintCriteriaID";
            this.colintCriteriaID.OptionsColumn.AllowEdit = false;
            this.colintCriteriaID.OptionsColumn.AllowFocus = false;
            this.colintCriteriaID.OptionsColumn.ReadOnly = true;
            this.colintCriteriaID.Width = 70;
            // 
            // colintContractorID
            // 
            this.colintContractorID.Caption = "Contractor ID";
            this.colintContractorID.FieldName = "intContractorID";
            this.colintContractorID.Name = "colintContractorID";
            this.colintContractorID.OptionsColumn.AllowEdit = false;
            this.colintContractorID.OptionsColumn.AllowFocus = false;
            this.colintContractorID.OptionsColumn.ReadOnly = true;
            this.colintContractorID.Width = 87;
            // 
            // colintParameterID
            // 
            this.colintParameterID.Caption = "Parameter ID";
            this.colintParameterID.FieldName = "intParameterID";
            this.colintParameterID.Name = "colintParameterID";
            this.colintParameterID.OptionsColumn.AllowEdit = false;
            this.colintParameterID.OptionsColumn.AllowFocus = false;
            this.colintParameterID.OptionsColumn.ReadOnly = true;
            this.colintParameterID.Width = 85;
            // 
            // colintRateID
            // 
            this.colintRateID.Caption = "Rate ID";
            this.colintRateID.FieldName = "intRateID";
            this.colintRateID.Name = "colintRateID";
            this.colintRateID.OptionsColumn.AllowEdit = false;
            this.colintRateID.OptionsColumn.AllowFocus = false;
            this.colintRateID.OptionsColumn.ReadOnly = true;
            this.colintRateID.Width = 58;
            // 
            // colintJobID
            // 
            this.colintJobID.Caption = "Job ID";
            this.colintJobID.FieldName = "intJobID";
            this.colintJobID.Name = "colintJobID";
            this.colintJobID.OptionsColumn.AllowEdit = false;
            this.colintJobID.OptionsColumn.AllowFocus = false;
            this.colintJobID.OptionsColumn.ReadOnly = true;
            this.colintJobID.Width = 52;
            // 
            // colintRateType
            // 
            this.colintRateType.Caption = "Rate Type ID";
            this.colintRateType.FieldName = "intRateType";
            this.colintRateType.Name = "colintRateType";
            this.colintRateType.OptionsColumn.AllowEdit = false;
            this.colintRateType.OptionsColumn.AllowFocus = false;
            this.colintRateType.OptionsColumn.ReadOnly = true;
            this.colintRateType.Width = 85;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Date Filter";
            this.barEditItem1.Edit = this.repositoryItemDateEdit1;
            this.barEditItem1.Id = 33;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // sp00204_Job_Rate_ManagerTableAdapter
            // 
            this.sp00204_Job_Rate_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp07162_UT_Job_Manager_Linked_Default_EquipmentTableAdapter
            // 
            this.sp07162_UT_Job_Manager_Linked_Default_EquipmentTableAdapter.ClearBeforeFill = true;
            // 
            // sp07161_UT_Job_Manager_Linked_Default_MaterialsTableAdapter
            // 
            this.sp07161_UT_Job_Manager_Linked_Default_MaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07234_UT_Linked_Reference_FilesTableAdapter
            // 
            this.sp07234_UT_Linked_Reference_FilesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06159_OM_Job_Manager_Linked_Job_Sub_TypesTableAdapter
            // 
            this.sp06159_OM_Job_Manager_Linked_Job_Sub_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06178_OM_Job_Manager_Linked_Job_Sub_Type_CompetenciesTableAdapter
            // 
            this.sp06178_OM_Job_Manager_Linked_Job_Sub_Type_CompetenciesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06195_OM_Job_Manager_Linked_Master_Job_AttributesTableAdapter
            // 
            this.sp06195_OM_Job_Manager_Linked_Master_Job_AttributesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06313_OM_Job_Type_Visit_TypesTableAdapter
            // 
            this.sp06313_OM_Job_Type_Visit_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp00205_Job_Rate_Manager_Job_RatesTableAdapter
            // 
            this.sp00205_Job_Rate_Manager_Job_RatesTableAdapter.ClearBeforeFill = true;
            // 
            // sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter
            // 
            this.sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Master_Job_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1214, 581);
            this.Controls.Add(this.splitContainerControl2);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Master_Job_Manager";
            this.Text = "Master Job Manager";
            this.Activated += new System.EventHandler(this.frm_Core_Master_Job_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Master_Job_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Master_Job_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00204JobRateManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06159OMJobManagerLinkedJobSubTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06178OMJobManagerLinkedJobSubTypeCompetenciesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06195OMJobManagerLinkedMasterJobAttributesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07162UTJobManagerLinkedDefaultEquipmentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07161UTJobManagerLinkedDefaultMaterialsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07234UTLinkedReferenceFilesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditDocumentPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTimeShortTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditDomcumentRemarks)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06313OMJobTypeVisitTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            this.xtraTabPageJobRates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlJobRates)).EndInit();
            this.splitContainerControlJobRates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJobRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00205JobRateManagerJobRatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJobRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJobRateCriteria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00206JobRateManagerJobRateParametersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJobRateCriteria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private System.Windows.Forms.BindingSource sp00204JobRateManagerBindingSource;
        private DataSet_ATTableAdapters.sp00204_Job_Rate_ManagerTableAdapter sp00204_Job_Rate_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRateCount;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private System.Windows.Forms.BindingSource sp07162UTJobManagerLinkedDefaultEquipmentBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultRequiredEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerHour;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerHour;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DataSet_UTTableAdapters.sp07162_UT_Job_Manager_Linked_Default_EquipmentTableAdapter sp07162_UT_Job_Manager_Linked_Default_EquipmentTableAdapter;
        private System.Windows.Forms.BindingSource sp07161UTJobManagerLinkedDefaultMaterialsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultRequiredMaterialID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsInStock;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colReorderLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitDescriptor;
        private DataSet_UTTableAdapters.sp07161_UT_Job_Manager_Linked_Default_MaterialsTableAdapter sp07161_UT_Job_Manager_Linked_Default_MaterialsTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceFileID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditDocumentPath;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTimeShortTime;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditDomcumentRemarks;
        private System.Windows.Forms.BindingSource sp07234UTLinkedReferenceFilesBindingSource;
        private DataSet_UTTableAdapters.sp07234_UT_Linked_Reference_FilesTableAdapter sp07234_UT_Linked_Reference_FilesTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.Columns.GridColumn colIsOperationsJob;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private System.Windows.Forms.BindingSource sp06159OMJobManagerLinkedJobSubTypesBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription3;
        private DataSet_OM_JobTableAdapters.sp06159_OM_Job_Manager_Linked_Job_Sub_TypesTableAdapter sp06159_OM_Job_Manager_Linked_Job_Sub_TypesTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private System.Windows.Forms.BindingSource sp06178OMJobManagerLinkedJobSubTypeCompetenciesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCompetencyID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCompetencyDescription;
        private DataSet_OM_JobTableAdapters.sp06178_OM_Job_Manager_Linked_Job_Sub_Type_CompetenciesTableAdapter sp06178_OM_Job_Manager_Linked_Job_Sub_Type_CompetenciesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationType;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private System.Windows.Forms.BindingSource sp06195OMJobManagerLinkedMasterJobAttributesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colMasterJobAttributeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMasterJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDataTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEditorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEditorMask;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryMinValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP3;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryMaxValue;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryRequired;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colPicklistHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colOnScreenLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colAttributeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks5;
        private DevExpress.XtraGrid.Columns.GridColumn colDataType;
        private DevExpress.XtraGrid.Columns.GridColumn colEditorType;
        private DevExpress.XtraGrid.Columns.GridColumn colPicklistDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeJobSubType;
        private DataSet_OM_JobTableAdapters.sp06195_OM_Job_Manager_Linked_Master_Job_AttributesTableAdapter sp06195_OM_Job_Manager_Linked_Master_Job_AttributesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.XtraGrid.GridControl gridControl14;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private System.Windows.Forms.BindingSource sp06313OMJobTypeVisitTypesBindingSource;
        private DataSet_OM_Core dataSet_OM_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID5;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription4;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeDescription;
        private DataSet_OM_CoreTableAdapters.sp06313_OM_Job_Type_Visit_TypesTableAdapter sp06313_OM_Job_Type_Visit_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colIsAmenityArbJob;
        private DevExpress.XtraGrid.Columns.GridColumn colIsUtilityArbJob;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageJobRates;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControlJobRates;
        private DevExpress.XtraGrid.GridControl gridControlJobRate;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewJobRate;
        private DevExpress.XtraGrid.Columns.GridColumn colRateID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colValidFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colValidTo;
        private DevExpress.XtraGrid.Columns.GridColumn colJobRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colRateTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRateUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colRateTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraGrid.GridControl gridControlJobRateCriteria;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewJobRateCriteria;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobcode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrJobdescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRateTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRateDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colmonRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn coldecRateUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colstrContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colstrCriteriaDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colintCriteriaID;
        private DevExpress.XtraGrid.Columns.GridColumn colintContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colintParameterID;
        private DevExpress.XtraGrid.Columns.GridColumn colintRateID;
        private DevExpress.XtraGrid.Columns.GridColumn colintJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colintRateType;
        private System.Windows.Forms.BindingSource sp00205JobRateManagerJobRatesBindingSource;
        private System.Windows.Forms.BindingSource sp00206JobRateManagerJobRateParametersBindingSource;
        private DataSet_ATTableAdapters.sp00205_Job_Rate_Manager_Job_RatesTableAdapter sp00205_Job_Rate_Manager_Job_RatesTableAdapter;
        private DataSet_ATTableAdapters.sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraGrid.Columns.GridColumn colAttributeName;
        private DevExpress.XtraGrid.Columns.GridColumn colValueFormula;
        private DevExpress.XtraGrid.Columns.GridColumn colValueFormulaEvaluateLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colValueFormulaEvaluateLocationDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colValueFormulaEvaluateOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colHideFromApp;
        private DevExpress.XtraGrid.Columns.GridColumn colIsExtraWorksJob;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colIsExtraWorksJob1;
        private DevExpress.XtraGrid.Columns.GridColumn colIsOperationsJob1;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Core_Signature_Capture : DevExpress.XtraEditors.XtraForm
    {
        #region Instance Variables

        public string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string _PassedInName = "";
        public bool _PersonsNameRequired = true;
        public bool _PersonsNameVisible = true;

        public Image _returnedImage = null;
        public string _returnedPersonsName = "";
        
        private bool ChangesOutstanding = false;
        private Point? _Previous = null;
        private Color _color = Color.Black;
        private Pen _Pen = new Pen(System.Drawing.Color.Black);

        #endregion

        public frm_Core_Signature_Capture()
        {
            InitializeComponent();
        }

        private void frm_Core_Signature_Capture_Load(object sender, EventArgs e)
        {
            labelControlPersonsName.Visible = _PersonsNameVisible;
            PersonsNameTextEdit.Visible = _PersonsNameVisible;
            PersonsNameTextEdit.Enabled = _PersonsNameVisible;
            PersonsNameTextEdit.Text = _PassedInName;

            _Pen.Width = (float)3;
            beiPenColour.EditValue = Color.Black;

            try
            {
                pictureEdit1.Cursor = BaseObjects.LoadCursor.LoadCustomCursor(Application.StartupPath + "\\Pencil.cur");
            }
            catch (Exception)
            {
                pictureEdit1.Cursor = Cursors.Arrow;
            }
        }


        #region Picture Box

        private void pictureEdit1_MouseDown(object sender, MouseEventArgs e)
        {
            _Previous = e.Location;
            pictureEdit1_MouseMove(sender, e);
        }

        private void pictureEdit1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_Previous != null)
            {
                if (pictureEdit1.Image == null)
                {
                    Bitmap bmp = new Bitmap(pictureEdit1.Width, pictureEdit1.Height);
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.Clear(Color.White);
                    }
                    pictureEdit1.Image = bmp;
                }
                using (Graphics g = Graphics.FromImage(pictureEdit1.Image))
                {
                    g.DrawLine(_Pen, _Previous.Value, e.Location);
                }
                pictureEdit1.Invalidate();
                _Previous = e.Location;
                ChangesOutstanding = true;
            }
        }

        private void pictureEdit1_MouseUp(object sender, MouseEventArgs e)
        {
            _Previous = null;
        }

        private void repositoryItemColorPickEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ColorPickEdit cpe = (ColorPickEdit)sender;
            _Pen.Color = (Color)cpe.Color;
        }

        private void repositoryItemSpinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            _Pen.Width = (float)Convert.ToDecimal(se.EditValue);
        }

        private void pictureEdit1_ImageChanged(object sender, EventArgs e)
        {
            PictureEdit pe = (PictureEdit)sender;
            if (pe.Image != null)
            {
                bbiRotateAntiClockwise.Enabled = true;
                bbiRotateClockwise.Enabled = true;
                pictureEdit1.Size = new Size((pe.Image.Width < 200 ? 200 : pe.Image.Width), (pe.Image.Height < 200 ? 200 : pe.Image.Height));
                ChangesOutstanding = true;
                bbiClearDrawing.Enabled = true;
            }
            else
            {
                bbiRotateAntiClockwise.Enabled = false;
                bbiRotateClockwise.Enabled = false;
                //pictureEdit1.Size = new Size(200, 200);
                bbiClearDrawing.Enabled = false;
            }
        }
        
        private void bbiRotateAntiClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pictureEdit1.Image = RotateImage(pictureEdit1.Image, RotateFlipType.Rotate270FlipNone);
            pictureEdit1.Size = new Size((pictureEdit1.Image.Width < 200 ? 200 : pictureEdit1.Image.Width), (pictureEdit1.Image.Height < 200 ? 200 : pictureEdit1.Image.Height));
            ChangesOutstanding = true;
        }
        private void bbiRotateClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pictureEdit1.Image = RotateImage(pictureEdit1.Image, RotateFlipType.Rotate90FlipNone);
            pictureEdit1.Size = new Size((pictureEdit1.Image.Width < 200 ? 200 : pictureEdit1.Image.Width), (pictureEdit1.Image.Height < 200 ? 200 : pictureEdit1.Image.Height));
            ChangesOutstanding = true;
        }
        public static Image RotateImage(Image img, RotateFlipType rotate_type)
        {
            img.RotateFlip(rotate_type);
            return img;
        }

        private void bbiClearDrawing_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear any captured signature - proceed?", "Clear Signature", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                pictureEdit1.Image = null;
                pictureEdit1.Refresh();
                
                bbiRotateAntiClockwise.Enabled = false;
                bbiRotateClockwise.Enabled = false;
                ChangesOutstanding = false;
                bbiClearDrawing.Enabled = false;
            }
        }

        #endregion


        private void bbiCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pictureEdit1.Image = null;
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void bbiSavePicture_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (_PersonsNameRequired && string.IsNullOrWhiteSpace(PersonsNameTextEdit.Text))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save signature, no Person Signing's Name has been entered.", " Save Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (pictureEdit1.Image == null)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save signature, no signature has been captured.", " Save Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            _returnedImage = pictureEdit1.Image;
            _returnedPersonsName = PersonsNameTextEdit.Text;
            pictureEdit1.Image = null;
            ChangesOutstanding = false;
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void frm_Core_Signature_Capture_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (pictureEdit1.Image != null && ChangesOutstanding)
            {
                string strMessage = "There are one or more outstanding changes.\n\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        bbiSavePicture.PerformClick();
                        break;
                }
            }
        }



    }
}
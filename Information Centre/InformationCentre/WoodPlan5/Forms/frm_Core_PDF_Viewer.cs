﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WoodPlan5
{
    public partial class frm_Core_PDF_Viewer : BaseObjects.frmBase
    {
        #region Instance Variables...

        public string strPDFFile = "";

        #endregion

        public frm_Core_PDF_Viewer()
        {
            InitializeComponent();
        }

        private void frm_Core_PDF_Viewer_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(strPDFFile))
            {
                XtraMessageBox.Show("No PDF File specified for opening!", "Load PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                this.pdfViewer1.LoadDocument(strPDFFile);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("An error occurred while opening PDF File...\n\n" + ex.Message, "Load PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }



    }
}


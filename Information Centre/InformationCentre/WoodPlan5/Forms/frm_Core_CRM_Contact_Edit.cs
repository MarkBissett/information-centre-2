using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Core_CRM_Contact_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        public int intRecordTypeID = 0;  // 24 = Gritting, 25 = Snow Clearance, 201 = Operations Job, 301 = Tender //
        public int intClientID = 0;
        public string strClientName = "";

        private bool ibool_ignoreValidation = true;  // Toggles validation on and off for Calculations //
        private bool ibool_FormStillLoading = true;

        #endregion

        public frm_Core_CRM_Contact_Edit()
        {
            InitializeComponent();
        }

        private void frm_Core_CRM_Contact_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 400108;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp05088_CRM_Linked_Record_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp05088_CRM_Linked_Record_TypesTableAdapter.Fill(woodPlanDataSet.sp05088_CRM_Linked_Record_Types);

            sp05084_GC_CRM_StatusesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp05084_GC_CRM_StatusesTableAdapter.Fill(woodPlanDataSet.sp05084_GC_CRM_Statuses);

            sp05085_GC_CRM_Contact_DirectionTableAdapter.Connection.ConnectionString = strConnectionString;
            sp05085_GC_CRM_Contact_DirectionTableAdapter.Fill(woodPlanDataSet.sp05085_GC_CRM_Contact_Direction);

            sp05086_GC_CRM_Contact_Methods_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp05086_GC_CRM_Contact_Methods_With_BlankTableAdapter.Fill(woodPlanDataSet.sp05086_GC_CRM_Contact_Methods_With_Blank);

            sp05087_GC_CRM_Contact_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp05087_GC_CRM_Contact_Types_With_BlankTableAdapter.Fill(woodPlanDataSet.sp05087_GC_CRM_Contact_Types_With_Blank);

            // Populate Main Dataset //
            this.sp05082_CRM_Contact_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp05082_CRM_Contact_Item.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["CRMID"] = 0;
                        drNewRow["ClientID"] = intClientID;
                        drNewRow["ClientContactID"] = 0;
                        drNewRow["ClientName"] = strClientName;
                        drNewRow["LinkedToRecordID"] = intLinkedToRecordID;
                        drNewRow["LinkedRecordDescription"] = strLinkedToRecordDesc;
                        drNewRow["LinkedToRecordTypeID"] = intRecordTypeID;
                        drNewRow["CreatedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["CreatedByStaffName"] = this.GlobalSettings.UserID;
                        drNewRow["ContactMadeDateTime"] = DateTime.Now;
                        drNewRow["ContactMethodID"] = 0;
                        drNewRow["ContactTypeID"] = 0;
                        drNewRow["StatusID"] = 0;
                        drNewRow["ContactDirectionID"] = 1;
                        this.woodPlanDataSet.sp05082_CRM_Contact_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp05082_CRM_Contact_Item.NewRow();
                        drNewRow["strMode"] = "blockadd";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["CRMID"] = 0;
                        drNewRow["ClientContactID"] = 0;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedRecordDescription"] = "Not Applicable - Block Adding";
                        drNewRow["LinkedToRecordTypeID"] = intRecordTypeID;
                        drNewRow["CreatedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["ContactMadeDateTime"] = DateTime.Now;
                        drNewRow["ContactMethodID"] = 0;
                        drNewRow["ContactTypeID"] = 0;
                        drNewRow["StatusID"] = 0;
                        drNewRow["ContactDirectionID"] = 1;
                        this.woodPlanDataSet.sp05082_CRM_Contact_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp05082_CRM_Contact_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["CRMID"] = 0;
                        drNewRow["ClientID"] = 0;
                        drNewRow["ClientContactID"] = 0;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedToRecordTypeID"] = 0;
                        this.woodPlanDataSet.sp05082_CRM_Contact_Item.Rows.Add(drNewRow);
                        this.woodPlanDataSet.sp05082_CRM_Contact_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp05082_CRM_Contact_ItemTableAdapter.Fill(this.woodPlanDataSet.sp05082_CRM_Contact_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.woodPlanDataSet.sp05082_CRM_Contact_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " CRM Customer Contact", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            ibool_FormStillLoading = true;  // Prevent validated code from firing on certain editors //
            
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ContactedByStaffNameButtonEdit.Focus();

                        buttonEdit1.Properties.ReadOnly = false;
                        buttonEdit1.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ContactedByStaffNameButtonEdit.Focus();

                        buttonEdit1.Properties.ReadOnly = true;
                        buttonEdit1.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        DescriptionTextEdit.Focus();

                        buttonEdit1.Properties.ReadOnly = false;
                        buttonEdit1.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        DescriptionTextEdit.Focus();

                        buttonEdit1.Properties.ReadOnly = true;
                        buttonEdit1.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            Set_Linked_Fields_Enabled_Status();

            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.woodPlanDataSet.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_Core_CRM_Contact_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_CRM_Contact_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp05082CRMContactItemBindingSource.EndEdit();
            try
            {
                this.sp05082_CRM_Contact_ItemTableAdapter.Update(woodPlanDataSet);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["CRMID"]) + ";";
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_GC_Callout_Manager")
                    {
                        var fParentForm = (frm_GC_Callout_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, "", "", "", "", strNewIDs, "");
                    }
                    if (frmChild.Name == "frm_GC_Snow_Callout_Manager")
                    {
                        var fParentForm = (frm_GC_Snow_Callout_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, "", "", "", "", strNewIDs, "", "");
                    }
                    if (frmChild.Name == "frm_Core_Client_Manager")
                    {
                        var fParentForm = (frm_Core_Client_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, "", "", "", "", "", "", strNewIDs, "");
                    }
                    if (frmChild.Name == "frm_Core_CRM_Manager")
                    {
                        var fParentForm = (frm_Core_CRM_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "");
                    }
                    if (frmChild.Name == "frm_OM_Visit_Manager")
                    {
                        var fParentForm = (frm_OM_Visit_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(55, Utils.enmFocusedGrid.CRM, strNewIDs);
                    }
                    if (frmChild.Name == "frm_OM_Job_Manager")
                    {
                        var fParentForm = (frm_OM_Job_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.CRM, strNewIDs);
                    }
                    if (frmChild.Name == "frm_OM_Tender_Manager")
                    {
                        var fParentForm = (frm_OM_Tender_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(4, Utils.enmFocusedGrid.CRM, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.woodPlanDataSet.sp05082_CRM_Contact_Item.Rows.Count; i++)
            {
                switch (this.woodPlanDataSet.sp05082_CRM_Contact_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else Set_Linked_Fields_Enabled_Status();
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void ClientNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
                if (currentRow == null) return;
                int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));

                frm_EP_Select_Client fChildForm = new frm_EP_Select_Client();
                fChildForm.intOriginalClientID = intClientID;
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    currentRow["ClientID"] = fChildForm.intSelectedClientID;
                    currentRow["ClientName"] = fChildForm.strSelectedClientName;
                    currentRow["ClientContactID"] = 0;
                    currentRow["ClientContact"] = "";
                    this.sp05082CRMContactItemBindingSource.EndEdit();
                }
            }
        }
        private void ClientNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "");
            }
            Set_Linked_Fields_Enabled_Status();
        }

        private void ContactDueDateTimeDateEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (XtraMessageBox.Show("You are about to clear the entered date!\n\nAre you sure you wish to proceed?", "Clear Date", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    ContactDueDateTimeDateEdit.EditValue = null;
                }
            }
        }
        private void ContactDueDateTimeDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de1 = (DateEdit)sender;
            DateTime? dt1 = Convert.ToDateTime(de1.DateTime);
            DateTime? dt2 = Convert.ToDateTime(ContactMadeDateTimeDateEdit.DateTime);

            if (this.strFormMode != "blockadd" && this.strFormMode != "blockedit" && dt1 == DateTime.MinValue && dt2 == DateTime.MinValue)
            {
                dxErrorProvider1.SetError(ContactDueDateTimeDateEdit, "Select\\Enter a value in either " + ItemForContactDueDateTime.Text + " or " + ItemForContactMadeDateTime.Text + " or a value in both.");
                dxErrorProvider1.SetError(ContactMadeDateTimeDateEdit, "Select\\Enter a value in either " + ItemForContactDueDateTime.Text + " or " + ItemForContactMadeDateTime.Text + " or a value in both.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContactDueDateTimeDateEdit, "");
                dxErrorProvider1.SetError(ContactMadeDateTimeDateEdit, "");
            }
        }

        private void ContactMadeDateTimeDateEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (XtraMessageBox.Show("You are about to clear the entered date!\n\nAre you sure you wish to proceed?", "Clear Date", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    ContactMadeDateTimeDateEdit.EditValue = null;
                }
            }
        }
        private void ContactMadeDateTimeDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateTime? dt1 = Convert.ToDateTime(ContactDueDateTimeDateEdit.DateTime);
            
            DateEdit de2 = (DateEdit)sender;
            DateTime? dt2 = Convert.ToDateTime(de2.DateTime);
            
            if (this.strFormMode != "blockadd" && this.strFormMode != "blockedit" && dt1 == DateTime.MinValue && dt2 == DateTime.MinValue)
            {
                dxErrorProvider1.SetError(ContactDueDateTimeDateEdit, "Select\\Enter a value in either " + ItemForContactDueDateTime.Text + " or " + ItemForContactMadeDateTime.Text + " or a value in both.");
                dxErrorProvider1.SetError(ContactMadeDateTimeDateEdit, "Select\\Enter a value in either " + ItemForContactDueDateTime.Text + " or " + ItemForContactMadeDateTime.Text + " or a value in both.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContactDueDateTimeDateEdit, "");
                dxErrorProvider1.SetError(ContactMadeDateTimeDateEdit, "");
            }
        }

        private void buttonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                #region Code

                DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
                if (currentRow == null) return;
                string strRecordTypeID = currentRow["LinkedToRecordTypeID"].ToString();
                int intOriginalID = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
                switch (strRecordTypeID)
                {
                    case "24":  // Gritting Callout // 
                        {
                            frm_GC_Select_Gritting_Callout fChildForm1 = new frm_GC_Select_Gritting_Callout();
                            fChildForm1.GlobalSettings = this.GlobalSettings;
                            if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                                this.sp05082CRMContactItemBindingSource.EndEdit();
                            }
                        }
                        break;
                    case "25":  // Snow Clearance Callout // 
                        {
                            frm_GC_Select_Snow_Callout fChildForm1 = new frm_GC_Select_Snow_Callout();
                            fChildForm1.GlobalSettings = this.GlobalSettings;
                            if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                                this.sp05082CRMContactItemBindingSource.EndEdit();
                            }
                        }
                        break;
                    case "201":  // Operations Manager Job // 
                        {
                            frm_GC_Select_Snow_Callout fChildForm1 = new frm_GC_Select_Snow_Callout();
                            fChildForm1.GlobalSettings = this.GlobalSettings;
                            var fChildForm = new frm_OM_Select_Job();
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.intOriginalJobID = intOriginalID;
                            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                            {
                                if (intOriginalID == fChildForm.intSelectedJobID) return;
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedJobID;
                                currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription;
                                this.sp05082CRMContactItemBindingSource.EndEdit();
                            }
                        }
                        break;
                    case "301":  // Tender // 
                        {
                            var fChildForm = new frm_OM_Select_Tender();
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.intOriginalParentID = 0;
                            fChildForm.intOriginalChildID = intOriginalID;
                            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                            {
                                if (intOriginalID == fChildForm.intSelectedChildID) return;
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedChildID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedChildDescriptionFull;
                                this.sp05082CRMContactItemBindingSource.EndEdit();
                            }
                        }
                        break;
                    default:
                        break;
                }
                #endregion
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (XtraMessageBox.Show("You are about to clear the " + layoutControlItem2.Text + "!\n\nAre you sure you wish to proceed?", "Clear Value", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["LinkedToRecordID"] = 0;
                        currentRow["LinkedRecordDescription"] = "";
                        this.sp05082CRMContactItemBindingSource.EndEdit();
                    }
                }
            }
        }

        private void DescriptionTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "");
            }
        }

        private void ContactedByStaffNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                /*DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
                if (currentRow == null) return;

                int intContactedByID = (string.IsNullOrEmpty(currentRow["ContactedByStaffID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ContactedByStaffID"]));
                frm_GC_Select_Stakeholder fChildForm = new frm_GC_Select_Stakeholder();
                fChildForm.intOriginalID = intContactedByID;
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    currentRow["ContactedByStaffID"] = fChildForm.intSelectedID;
                    currentRow["ContactedByStaffName"] = fChildForm.strSelectedValue;
                    this.sp05082CRMContactItemBindingSource.EndEdit();
                }*/
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (XtraMessageBox.Show("You are about to clear the " + ItemForContactedByStaffName.Text + "!\n\nAre you sure you wish to proceed?", "Clear Value", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["ContactedByStaffID"] = 0;
                        currentRow["ContactedByStaffName"] = "";
                        this.sp05082CRMContactItemBindingSource.EndEdit();
                    }
                }
            }
        }

        private void ClientContactButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                /*DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
                if (currentRow == null) return;
                frm_GC_Select_Client_Contact_Person fChildForm = new frm_GC_Select_Client_Contact_Person();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInClientIDs = currentRow["ClientID"].ToString() + ",";
                fChildForm.intOriginalID = Convert.ToInt32(currentRow["ClientID"]);
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    currentRow["ClientContactID"] = fChildForm.intSelectedID;
                    currentRow["ClientContact"] = fChildForm.strSelectedValue;
                    this.sp05082CRMContactItemBindingSource.EndEdit();

                }*/
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (XtraMessageBox.Show("You are about to clear the " + ItemForClientContact.Text + "!\n\nAre you sure you wish to proceed?", "Clear Value", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["ClientContactID"] = 0;
                        currentRow["ClientContact"] = "";
                        this.sp05082CRMContactItemBindingSource.EndEdit();
                    }
                }
            }
        }

        private void LinkedToRecordTypeIDLookupEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
         }
        private void LinkedToRecordTypeIDLookupEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            intLinkedToRecordID = Convert.ToInt32(glue.EditValue);
            DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
            if (currentRow == null) return;
            if (intLinkedToRecordID == 0)
            {
                currentRow["LinkedToRecordID"] = 0;
                currentRow["LinkedRecordDescription"] = "";
                currentRow["LinkedToRecordID"] = 0;
                currentRow["LinkedRecordDescription"] = "";
                this.sp05082CRMContactItemBindingSource.EndEdit();
            }
            else
            {
                currentRow["LinkedToRecordID"] = 0;
                currentRow["LinkedRecordDescription"] = "";
                this.sp05082CRMContactItemBindingSource.EndEdit();
            }
            Set_Linked_Fields_Enabled_Status();
        }

        #endregion


        private void Set_Linked_Fields_Enabled_Status()
        {
            DataRowView currentRow = (DataRowView)sp05082CRMContactItemBindingSource.Current;
            if (currentRow == null) return;
            if (currentRow["LinkedToRecordTypeID"].ToString() == "0")
            {
                buttonEdit1.Properties.ReadOnly = true;
                buttonEdit1.Properties.Buttons[0].Enabled = false;
            }
            else
            {
                buttonEdit1.Properties.ReadOnly = false;
                buttonEdit1.Properties.Buttons[0].Enabled = true;
            }
            if (currentRow["ClientID"].ToString() == "0")
            {
                ClientContactButtonEdit.Properties.ReadOnly = true;
                ClientContactButtonEdit.Properties.Buttons[0].Enabled = false;
            }
            else
            {
                ClientContactButtonEdit.Properties.ReadOnly = false;
                ClientContactButtonEdit.Properties.Buttons[0].Enabled = true;
            }
        }






   
    }
}


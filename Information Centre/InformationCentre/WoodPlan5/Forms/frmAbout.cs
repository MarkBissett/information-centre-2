﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frmAbout : frmBase
    {
        public frmAbout()
        {
            InitializeComponent();
        }

        private void frmAbout_Load(object sender, EventArgs e)
        {
            System.Reflection.Assembly oAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo oFileVersionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(oAssembly.Location);
            Version v = oAssembly.GetName().Version;
            labelControlVersion.Text = "Version " + v.ToString().Substring(0, v.ToString().LastIndexOf('.'));  // Remove the last ".0" from end //
            labelControlCopyright.Text = oFileVersionInfo.LegalCopyright;
        }
    }
}
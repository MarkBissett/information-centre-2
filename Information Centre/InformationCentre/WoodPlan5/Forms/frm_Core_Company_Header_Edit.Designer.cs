namespace WoodPlan5
{
    partial class frm_Core_Company_Header_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Company_Header_Edit));
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.sp00231CompanyHeaderItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.intHeaderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strCompanyNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strSloganTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strTelephone1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strTelephone2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strMobile1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strMobile2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strFax1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strFax2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strEmail1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strEmail2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strWebSite1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strWebSite2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForintHeaderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrCompanyName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrSlogan = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrTelephone1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrTelephone2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrMobile1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrMobile2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrFax1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrFax2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrEmail1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrEmail2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrWebSite1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrWebSite2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator4 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator5 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00231_Company_Header_ItemTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00231_Company_Header_ItemTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp00231CompanyHeaderItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intHeaderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strCompanyNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strSloganTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTelephone1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTelephone2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMobile1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMobile2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strFax1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strFax2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmail1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmail2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strWebSite1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strWebSite2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintHeaderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrSlogan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTelephone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTelephone2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMobile1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMobile2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrFax1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrFax2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmail1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmail2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrWebSite1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrWebSite2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 498);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 472);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 472);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 498);
            this.barDockControl2.Size = new System.Drawing.Size(628, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 472);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 472);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intHeaderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strCompanyNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strSloganTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strTelephone1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strTelephone2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strMobile1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strMobile2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strFax1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strFax2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strEmail1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strEmail2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strWebSite1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strWebSite2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.DataSource = this.sp00231CompanyHeaderItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintHeaderID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 472);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00231CompanyHeaderItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(96, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(190, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 28;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // sp00231CompanyHeaderItemBindingSource
            // 
            this.sp00231CompanyHeaderItemBindingSource.DataMember = "sp00231_Company_Header_Item";
            this.sp00231CompanyHeaderItemBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // intHeaderIDTextEdit
            // 
            this.intHeaderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "intHeaderID", true));
            this.intHeaderIDTextEdit.Location = new System.Drawing.Point(107, 12);
            this.intHeaderIDTextEdit.MenuManager = this.barManager1;
            this.intHeaderIDTextEdit.Name = "intHeaderIDTextEdit";
            this.intHeaderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intHeaderIDTextEdit, true);
            this.intHeaderIDTextEdit.Size = new System.Drawing.Size(492, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intHeaderIDTextEdit, optionsSpelling1);
            this.intHeaderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intHeaderIDTextEdit.TabIndex = 4;
            // 
            // strDescriptionTextEdit
            // 
            this.strDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strDescription", true));
            this.strDescriptionTextEdit.Location = new System.Drawing.Point(94, 35);
            this.strDescriptionTextEdit.MenuManager = this.barManager1;
            this.strDescriptionTextEdit.Name = "strDescriptionTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strDescriptionTextEdit, true);
            this.strDescriptionTextEdit.Size = new System.Drawing.Size(505, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strDescriptionTextEdit, optionsSpelling2);
            this.strDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.strDescriptionTextEdit.TabIndex = 5;
            this.strDescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strDescriptionTextEdit_Validating);
            // 
            // strCompanyNameTextEdit
            // 
            this.strCompanyNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strCompanyName", true));
            this.strCompanyNameTextEdit.Location = new System.Drawing.Point(94, 59);
            this.strCompanyNameTextEdit.MenuManager = this.barManager1;
            this.strCompanyNameTextEdit.Name = "strCompanyNameTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strCompanyNameTextEdit, true);
            this.strCompanyNameTextEdit.Size = new System.Drawing.Size(505, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strCompanyNameTextEdit, optionsSpelling3);
            this.strCompanyNameTextEdit.StyleController = this.dataLayoutControl1;
            this.strCompanyNameTextEdit.TabIndex = 6;
            this.strCompanyNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strCompanyNameTextEdit_Validating);
            // 
            // strSloganTextEdit
            // 
            this.strSloganTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strSlogan", true));
            this.strSloganTextEdit.Location = new System.Drawing.Point(94, 83);
            this.strSloganTextEdit.MenuManager = this.barManager1;
            this.strSloganTextEdit.Name = "strSloganTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strSloganTextEdit, true);
            this.strSloganTextEdit.Size = new System.Drawing.Size(505, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strSloganTextEdit, optionsSpelling4);
            this.strSloganTextEdit.StyleController = this.dataLayoutControl1;
            this.strSloganTextEdit.TabIndex = 7;
            // 
            // strAddressLine1TextEdit
            // 
            this.strAddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strAddressLine1", true));
            this.strAddressLine1TextEdit.Location = new System.Drawing.Point(118, 187);
            this.strAddressLine1TextEdit.MenuManager = this.barManager1;
            this.strAddressLine1TextEdit.Name = "strAddressLine1TextEdit";
            this.strAddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine1TextEdit, true);
            this.strAddressLine1TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine1TextEdit, optionsSpelling5);
            this.strAddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine1TextEdit.TabIndex = 8;
            // 
            // strAddressLine2TextEdit
            // 
            this.strAddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strAddressLine2", true));
            this.strAddressLine2TextEdit.Location = new System.Drawing.Point(118, 211);
            this.strAddressLine2TextEdit.MenuManager = this.barManager1;
            this.strAddressLine2TextEdit.Name = "strAddressLine2TextEdit";
            this.strAddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine2TextEdit, true);
            this.strAddressLine2TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine2TextEdit, optionsSpelling6);
            this.strAddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine2TextEdit.TabIndex = 9;
            // 
            // strAddressLine3TextEdit
            // 
            this.strAddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strAddressLine3", true));
            this.strAddressLine3TextEdit.Location = new System.Drawing.Point(118, 235);
            this.strAddressLine3TextEdit.MenuManager = this.barManager1;
            this.strAddressLine3TextEdit.Name = "strAddressLine3TextEdit";
            this.strAddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine3TextEdit, true);
            this.strAddressLine3TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine3TextEdit, optionsSpelling7);
            this.strAddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine3TextEdit.TabIndex = 10;
            // 
            // strAddressLine4TextEdit
            // 
            this.strAddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strAddressLine4", true));
            this.strAddressLine4TextEdit.Location = new System.Drawing.Point(118, 259);
            this.strAddressLine4TextEdit.MenuManager = this.barManager1;
            this.strAddressLine4TextEdit.Name = "strAddressLine4TextEdit";
            this.strAddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine4TextEdit, true);
            this.strAddressLine4TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine4TextEdit, optionsSpelling8);
            this.strAddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine4TextEdit.TabIndex = 11;
            // 
            // strAddressLine5TextEdit
            // 
            this.strAddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strAddressLine5", true));
            this.strAddressLine5TextEdit.Location = new System.Drawing.Point(118, 283);
            this.strAddressLine5TextEdit.MenuManager = this.barManager1;
            this.strAddressLine5TextEdit.Name = "strAddressLine5TextEdit";
            this.strAddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine5TextEdit, true);
            this.strAddressLine5TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine5TextEdit, optionsSpelling9);
            this.strAddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine5TextEdit.TabIndex = 12;
            // 
            // strPostcodeTextEdit
            // 
            this.strPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strPostcode", true));
            this.strPostcodeTextEdit.Location = new System.Drawing.Point(118, 307);
            this.strPostcodeTextEdit.MenuManager = this.barManager1;
            this.strPostcodeTextEdit.Name = "strPostcodeTextEdit";
            this.strPostcodeTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strPostcodeTextEdit, true);
            this.strPostcodeTextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strPostcodeTextEdit, optionsSpelling10);
            this.strPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strPostcodeTextEdit.TabIndex = 13;
            // 
            // strTelephone1TextEdit
            // 
            this.strTelephone1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strTelephone1", true));
            this.strTelephone1TextEdit.Location = new System.Drawing.Point(118, 187);
            this.strTelephone1TextEdit.MenuManager = this.barManager1;
            this.strTelephone1TextEdit.Name = "strTelephone1TextEdit";
            this.strTelephone1TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strTelephone1TextEdit, true);
            this.strTelephone1TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strTelephone1TextEdit, optionsSpelling11);
            this.strTelephone1TextEdit.StyleController = this.dataLayoutControl1;
            this.strTelephone1TextEdit.TabIndex = 14;
            // 
            // strTelephone2TextEdit
            // 
            this.strTelephone2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strTelephone2", true));
            this.strTelephone2TextEdit.Location = new System.Drawing.Point(118, 211);
            this.strTelephone2TextEdit.MenuManager = this.barManager1;
            this.strTelephone2TextEdit.Name = "strTelephone2TextEdit";
            this.strTelephone2TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strTelephone2TextEdit, true);
            this.strTelephone2TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strTelephone2TextEdit, optionsSpelling12);
            this.strTelephone2TextEdit.StyleController = this.dataLayoutControl1;
            this.strTelephone2TextEdit.TabIndex = 15;
            // 
            // strMobile1TextEdit
            // 
            this.strMobile1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strMobile1", true));
            this.strMobile1TextEdit.Location = new System.Drawing.Point(118, 237);
            this.strMobile1TextEdit.MenuManager = this.barManager1;
            this.strMobile1TextEdit.Name = "strMobile1TextEdit";
            this.strMobile1TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strMobile1TextEdit, true);
            this.strMobile1TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strMobile1TextEdit, optionsSpelling13);
            this.strMobile1TextEdit.StyleController = this.dataLayoutControl1;
            this.strMobile1TextEdit.TabIndex = 16;
            // 
            // strMobile2TextEdit
            // 
            this.strMobile2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strMobile2", true));
            this.strMobile2TextEdit.Location = new System.Drawing.Point(118, 261);
            this.strMobile2TextEdit.MenuManager = this.barManager1;
            this.strMobile2TextEdit.Name = "strMobile2TextEdit";
            this.strMobile2TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strMobile2TextEdit, true);
            this.strMobile2TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strMobile2TextEdit, optionsSpelling14);
            this.strMobile2TextEdit.StyleController = this.dataLayoutControl1;
            this.strMobile2TextEdit.TabIndex = 17;
            // 
            // strFax1TextEdit
            // 
            this.strFax1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strFax1", true));
            this.strFax1TextEdit.Location = new System.Drawing.Point(118, 287);
            this.strFax1TextEdit.MenuManager = this.barManager1;
            this.strFax1TextEdit.Name = "strFax1TextEdit";
            this.strFax1TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strFax1TextEdit, true);
            this.strFax1TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strFax1TextEdit, optionsSpelling15);
            this.strFax1TextEdit.StyleController = this.dataLayoutControl1;
            this.strFax1TextEdit.TabIndex = 18;
            // 
            // strFax2TextEdit
            // 
            this.strFax2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strFax2", true));
            this.strFax2TextEdit.Location = new System.Drawing.Point(118, 311);
            this.strFax2TextEdit.MenuManager = this.barManager1;
            this.strFax2TextEdit.Name = "strFax2TextEdit";
            this.strFax2TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strFax2TextEdit, true);
            this.strFax2TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strFax2TextEdit, optionsSpelling16);
            this.strFax2TextEdit.StyleController = this.dataLayoutControl1;
            this.strFax2TextEdit.TabIndex = 19;
            // 
            // strEmail1TextEdit
            // 
            this.strEmail1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strEmail1", true));
            this.strEmail1TextEdit.Location = new System.Drawing.Point(118, 337);
            this.strEmail1TextEdit.MenuManager = this.barManager1;
            this.strEmail1TextEdit.Name = "strEmail1TextEdit";
            this.strEmail1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strEmail1TextEdit, true);
            this.strEmail1TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strEmail1TextEdit, optionsSpelling17);
            this.strEmail1TextEdit.StyleController = this.dataLayoutControl1;
            this.strEmail1TextEdit.TabIndex = 20;
            // 
            // strEmail2TextEdit
            // 
            this.strEmail2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strEmail2", true));
            this.strEmail2TextEdit.Location = new System.Drawing.Point(118, 361);
            this.strEmail2TextEdit.MenuManager = this.barManager1;
            this.strEmail2TextEdit.Name = "strEmail2TextEdit";
            this.strEmail2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strEmail2TextEdit, true);
            this.strEmail2TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strEmail2TextEdit, optionsSpelling18);
            this.strEmail2TextEdit.StyleController = this.dataLayoutControl1;
            this.strEmail2TextEdit.TabIndex = 21;
            // 
            // strWebSite1TextEdit
            // 
            this.strWebSite1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strWebSite1", true));
            this.strWebSite1TextEdit.Location = new System.Drawing.Point(118, 387);
            this.strWebSite1TextEdit.MenuManager = this.barManager1;
            this.strWebSite1TextEdit.Name = "strWebSite1TextEdit";
            this.strWebSite1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strWebSite1TextEdit, true);
            this.strWebSite1TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strWebSite1TextEdit, optionsSpelling19);
            this.strWebSite1TextEdit.StyleController = this.dataLayoutControl1;
            this.strWebSite1TextEdit.TabIndex = 22;
            // 
            // strWebSite2TextEdit
            // 
            this.strWebSite2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strWebSite2", true));
            this.strWebSite2TextEdit.Location = new System.Drawing.Point(118, 411);
            this.strWebSite2TextEdit.MenuManager = this.barManager1;
            this.strWebSite2TextEdit.Name = "strWebSite2TextEdit";
            this.strWebSite2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strWebSite2TextEdit, true);
            this.strWebSite2TextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strWebSite2TextEdit, optionsSpelling20);
            this.strWebSite2TextEdit.StyleController = this.dataLayoutControl1;
            this.strWebSite2TextEdit.TabIndex = 23;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00231CompanyHeaderItemBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 187);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(539, 254);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling21);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 27;
            // 
            // ItemForintHeaderID
            // 
            this.ItemForintHeaderID.Control = this.intHeaderIDTextEdit;
            this.ItemForintHeaderID.CustomizationFormText = "Header ID:";
            this.ItemForintHeaderID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintHeaderID.Name = "ItemForintHeaderID";
            this.ItemForintHeaderID.Size = new System.Drawing.Size(591, 24);
            this.ItemForintHeaderID.Text = "Header ID:";
            this.ItemForintHeaderID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(611, 477);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrCompanyName,
            this.ItemForstrSlogan,
            this.ItemForstrDescription,
            this.layoutControlGroup4,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(591, 457);
            // 
            // ItemForstrCompanyName
            // 
            this.ItemForstrCompanyName.Control = this.strCompanyNameTextEdit;
            this.ItemForstrCompanyName.CustomizationFormText = "Company Name:";
            this.ItemForstrCompanyName.Location = new System.Drawing.Point(0, 47);
            this.ItemForstrCompanyName.Name = "ItemForstrCompanyName";
            this.ItemForstrCompanyName.Size = new System.Drawing.Size(591, 24);
            this.ItemForstrCompanyName.Text = "Company Name:";
            this.ItemForstrCompanyName.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrSlogan
            // 
            this.ItemForstrSlogan.Control = this.strSloganTextEdit;
            this.ItemForstrSlogan.CustomizationFormText = "Slogan:";
            this.ItemForstrSlogan.Location = new System.Drawing.Point(0, 71);
            this.ItemForstrSlogan.Name = "ItemForstrSlogan";
            this.ItemForstrSlogan.Size = new System.Drawing.Size(591, 24);
            this.ItemForstrSlogan.Text = "Slogan:";
            this.ItemForstrSlogan.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrDescription
            // 
            this.ItemForstrDescription.Control = this.strDescriptionTextEdit;
            this.ItemForstrDescription.CustomizationFormText = "Description:";
            this.ItemForstrDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForstrDescription.Name = "ItemForstrDescription";
            this.ItemForstrDescription.Size = new System.Drawing.Size(591, 24);
            this.ItemForstrDescription.Text = "Description:";
            this.ItemForstrDescription.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 105);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(591, 352);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(567, 306);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup5,
            this.layoutControlGroup7});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Contact Numbers";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrTelephone1,
            this.ItemForstrTelephone2,
            this.ItemForstrMobile1,
            this.ItemForstrMobile2,
            this.ItemForstrFax1,
            this.ItemForstrFax2,
            this.ItemForstrEmail1,
            this.ItemForstrEmail2,
            this.ItemForstrWebSite1,
            this.ItemForstrWebSite2,
            this.simpleSeparator2,
            this.simpleSeparator3,
            this.simpleSeparator4,
            this.simpleSeparator5,
            this.emptySpaceItem2});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(543, 258);
            this.layoutControlGroup6.Text = "Contact Numbers";
            // 
            // ItemForstrTelephone1
            // 
            this.ItemForstrTelephone1.Control = this.strTelephone1TextEdit;
            this.ItemForstrTelephone1.CustomizationFormText = "Telephone 1:";
            this.ItemForstrTelephone1.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrTelephone1.Name = "ItemForstrTelephone1";
            this.ItemForstrTelephone1.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrTelephone1.Text = "Telephone 1:";
            this.ItemForstrTelephone1.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrTelephone2
            // 
            this.ItemForstrTelephone2.Control = this.strTelephone2TextEdit;
            this.ItemForstrTelephone2.CustomizationFormText = "Telephone 2:";
            this.ItemForstrTelephone2.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrTelephone2.Name = "ItemForstrTelephone2";
            this.ItemForstrTelephone2.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrTelephone2.Text = "Telephone 2:";
            this.ItemForstrTelephone2.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrMobile1
            // 
            this.ItemForstrMobile1.Control = this.strMobile1TextEdit;
            this.ItemForstrMobile1.CustomizationFormText = "Mobile Tel 1:";
            this.ItemForstrMobile1.Location = new System.Drawing.Point(0, 50);
            this.ItemForstrMobile1.Name = "ItemForstrMobile1";
            this.ItemForstrMobile1.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrMobile1.Text = "Mobile Tel 1:";
            this.ItemForstrMobile1.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrMobile2
            // 
            this.ItemForstrMobile2.Control = this.strMobile2TextEdit;
            this.ItemForstrMobile2.CustomizationFormText = "Mobile Tel 2:";
            this.ItemForstrMobile2.Location = new System.Drawing.Point(0, 74);
            this.ItemForstrMobile2.Name = "ItemForstrMobile2";
            this.ItemForstrMobile2.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrMobile2.Text = "Mobile Tel 2:";
            this.ItemForstrMobile2.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrFax1
            // 
            this.ItemForstrFax1.Control = this.strFax1TextEdit;
            this.ItemForstrFax1.CustomizationFormText = "Fax 1:";
            this.ItemForstrFax1.Location = new System.Drawing.Point(0, 100);
            this.ItemForstrFax1.Name = "ItemForstrFax1";
            this.ItemForstrFax1.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrFax1.Text = "Fax 1:";
            this.ItemForstrFax1.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrFax2
            // 
            this.ItemForstrFax2.Control = this.strFax2TextEdit;
            this.ItemForstrFax2.CustomizationFormText = "Fax 2:";
            this.ItemForstrFax2.Location = new System.Drawing.Point(0, 124);
            this.ItemForstrFax2.Name = "ItemForstrFax2";
            this.ItemForstrFax2.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrFax2.Text = "Fax 2:";
            this.ItemForstrFax2.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrEmail1
            // 
            this.ItemForstrEmail1.Control = this.strEmail1TextEdit;
            this.ItemForstrEmail1.CustomizationFormText = "Email 1:";
            this.ItemForstrEmail1.Location = new System.Drawing.Point(0, 150);
            this.ItemForstrEmail1.Name = "ItemForstrEmail1";
            this.ItemForstrEmail1.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrEmail1.Text = "Email 1:";
            this.ItemForstrEmail1.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrEmail2
            // 
            this.ItemForstrEmail2.Control = this.strEmail2TextEdit;
            this.ItemForstrEmail2.CustomizationFormText = "Email 2:";
            this.ItemForstrEmail2.Location = new System.Drawing.Point(0, 174);
            this.ItemForstrEmail2.Name = "ItemForstrEmail2";
            this.ItemForstrEmail2.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrEmail2.Text = "Email 2:";
            this.ItemForstrEmail2.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrWebSite1
            // 
            this.ItemForstrWebSite1.Control = this.strWebSite1TextEdit;
            this.ItemForstrWebSite1.CustomizationFormText = "Website 1:";
            this.ItemForstrWebSite1.Location = new System.Drawing.Point(0, 200);
            this.ItemForstrWebSite1.Name = "ItemForstrWebSite1";
            this.ItemForstrWebSite1.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrWebSite1.Text = "Website 1:";
            this.ItemForstrWebSite1.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrWebSite2
            // 
            this.ItemForstrWebSite2.Control = this.strWebSite2TextEdit;
            this.ItemForstrWebSite2.CustomizationFormText = "Website 2:";
            this.ItemForstrWebSite2.Location = new System.Drawing.Point(0, 224);
            this.ItemForstrWebSite2.Name = "ItemForstrWebSite2";
            this.ItemForstrWebSite2.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrWebSite2.Text = "Website 2:";
            this.ItemForstrWebSite2.TextSize = new System.Drawing.Size(79, 13);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.CustomizationFormText = "simpleSeparator2";
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 48);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(543, 2);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator3";
            this.simpleSeparator3.Location = new System.Drawing.Point(0, 98);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(543, 2);
            // 
            // simpleSeparator4
            // 
            this.simpleSeparator4.AllowHotTrack = false;
            this.simpleSeparator4.CustomizationFormText = "simpleSeparator4";
            this.simpleSeparator4.Location = new System.Drawing.Point(0, 148);
            this.simpleSeparator4.Name = "simpleSeparator4";
            this.simpleSeparator4.Size = new System.Drawing.Size(543, 2);
            // 
            // simpleSeparator5
            // 
            this.simpleSeparator5.AllowHotTrack = false;
            this.simpleSeparator5.CustomizationFormText = "simpleSeparator5";
            this.simpleSeparator5.Location = new System.Drawing.Point(0, 198);
            this.simpleSeparator5.Name = "simpleSeparator5";
            this.simpleSeparator5.Size = new System.Drawing.Size(543, 2);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 248);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(543, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Address";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrAddressLine1,
            this.ItemForstrAddressLine2,
            this.ItemForstrAddressLine3,
            this.ItemForstrAddressLine4,
            this.ItemForstrAddressLine5,
            this.ItemForstrPostcode,
            this.emptySpaceItem1});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(543, 258);
            this.layoutControlGroup5.Text = "Address";
            // 
            // ItemForstrAddressLine1
            // 
            this.ItemForstrAddressLine1.Control = this.strAddressLine1TextEdit;
            this.ItemForstrAddressLine1.CustomizationFormText = "Address Line 1:";
            this.ItemForstrAddressLine1.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrAddressLine1.Name = "ItemForstrAddressLine1";
            this.ItemForstrAddressLine1.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrAddressLine1.Text = "Address Line 1:";
            this.ItemForstrAddressLine1.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrAddressLine2
            // 
            this.ItemForstrAddressLine2.Control = this.strAddressLine2TextEdit;
            this.ItemForstrAddressLine2.CustomizationFormText = "Address Line 2:";
            this.ItemForstrAddressLine2.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrAddressLine2.Name = "ItemForstrAddressLine2";
            this.ItemForstrAddressLine2.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrAddressLine2.Text = "Address Line 2:";
            this.ItemForstrAddressLine2.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrAddressLine3
            // 
            this.ItemForstrAddressLine3.Control = this.strAddressLine3TextEdit;
            this.ItemForstrAddressLine3.CustomizationFormText = "Address Line 3:";
            this.ItemForstrAddressLine3.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrAddressLine3.Name = "ItemForstrAddressLine3";
            this.ItemForstrAddressLine3.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrAddressLine3.Text = "Address Line 3:";
            this.ItemForstrAddressLine3.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrAddressLine4
            // 
            this.ItemForstrAddressLine4.Control = this.strAddressLine4TextEdit;
            this.ItemForstrAddressLine4.CustomizationFormText = "Address Line 4:";
            this.ItemForstrAddressLine4.Location = new System.Drawing.Point(0, 72);
            this.ItemForstrAddressLine4.Name = "ItemForstrAddressLine4";
            this.ItemForstrAddressLine4.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrAddressLine4.Text = "Address Line 4:";
            this.ItemForstrAddressLine4.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrAddressLine5
            // 
            this.ItemForstrAddressLine5.Control = this.strAddressLine5TextEdit;
            this.ItemForstrAddressLine5.CustomizationFormText = "Address Line 5:";
            this.ItemForstrAddressLine5.Location = new System.Drawing.Point(0, 96);
            this.ItemForstrAddressLine5.Name = "ItemForstrAddressLine5";
            this.ItemForstrAddressLine5.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrAddressLine5.Text = "Address Line 5:";
            this.ItemForstrAddressLine5.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForstrPostcode
            // 
            this.ItemForstrPostcode.Control = this.strPostcodeTextEdit;
            this.ItemForstrPostcode.CustomizationFormText = "Postcode:";
            this.ItemForstrPostcode.Location = new System.Drawing.Point(0, 120);
            this.ItemForstrPostcode.Name = "ItemForstrPostcode";
            this.ItemForstrPostcode.Size = new System.Drawing.Size(543, 24);
            this.ItemForstrPostcode.Text = "Postcode:";
            this.ItemForstrPostcode.TextSize = new System.Drawing.Size(79, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 144);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(543, 114);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup7.CaptionImage")));
            this.layoutControlGroup7.CustomizationFormText = "Remarks";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(543, 258);
            this.layoutControlGroup7.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(543, 258);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(84, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(194, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(278, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(313, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(84, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(84, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(84, 23);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 95);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(591, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00231_Company_Header_ItemTableAdapter
            // 
            this.sp00231_Company_Header_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_Company_Header_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 526);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Company_Header_Edit";
            this.Text = "Edit Company Header";
            this.Activated += new System.EventHandler(this.frm_Core_Company_Header_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Company_Header_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Company_Header_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp00231CompanyHeaderItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intHeaderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strCompanyNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strSloganTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTelephone1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTelephone2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMobile1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMobile2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strFax1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strFax2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmail1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmail2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strWebSite1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strWebSite2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintHeaderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrSlogan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTelephone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTelephone2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMobile1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMobile2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrFax1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrFax2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmail1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmail2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrWebSite1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrWebSite2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit intHeaderIDTextEdit;
        private System.Windows.Forms.BindingSource sp00231CompanyHeaderItemBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraEditors.TextEdit strDescriptionTextEdit;
        private DevExpress.XtraEditors.TextEdit strCompanyNameTextEdit;
        private DevExpress.XtraEditors.TextEdit strSloganTextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine1TextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine2TextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit strPostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit strTelephone1TextEdit;
        private DevExpress.XtraEditors.TextEdit strTelephone2TextEdit;
        private DevExpress.XtraEditors.TextEdit strMobile1TextEdit;
        private DevExpress.XtraEditors.TextEdit strMobile2TextEdit;
        private DevExpress.XtraEditors.TextEdit strFax1TextEdit;
        private DevExpress.XtraEditors.TextEdit strFax2TextEdit;
        private DevExpress.XtraEditors.TextEdit strEmail1TextEdit;
        private DevExpress.XtraEditors.TextEdit strEmail2TextEdit;
        private DevExpress.XtraEditors.TextEdit strWebSite1TextEdit;
        private DevExpress.XtraEditors.TextEdit strWebSite2TextEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintHeaderID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrCompanyName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrSlogan;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00231_Company_Header_ItemTableAdapter sp00231_Company_Header_ItemTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrTelephone1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrTelephone2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrMobile1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrMobile2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrFax1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrFax2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrEmail1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrEmail2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrWebSite1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrWebSite2;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator4;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

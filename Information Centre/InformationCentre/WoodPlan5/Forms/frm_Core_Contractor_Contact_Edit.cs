using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Core_Contractor_Contact_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;
        
        public int intContractorID = 0;
        
        #endregion
        
        public frm_Core_Contractor_Contact_Edit()
        {
            InitializeComponent();
        }

        private void frm_Core_Contractor_Contact_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 80012;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp04026_Contractor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp04026_Contractor_List_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04026_Contractor_List_With_Blank);

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp00187_Contractor_Contact_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Populate Main Dataset //
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00187_Contractor_Contact_Item.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strContactName"] = "";
                        drNewRow["intContractorID"] = intContractorID;
                        drNewRow["GrittingEmail"] = 0;
                        drNewRow["IsSelfBillingEmail"] = 0;
                        drNewRow["IsSnowMobileTel"] = 0;
                        drNewRow["IsActiveTotalViewUser"] = 0;
                        drNewRow["IsOMSelfBillingEmail"] = 0;
                        this.woodPlanDataSet.sp00187_Contractor_Contact_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00187_Contractor_Contact_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["intContractorID"] = 0;
                        this.woodPlanDataSet.sp00187_Contractor_Contact_Item.Rows.Add(drNewRow);
                        this.woodPlanDataSet.sp00187_Contractor_Contact_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp00187_Contractor_Contact_ItemTableAdapter.Fill(this.woodPlanDataSet.sp00187_Contractor_Contact_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.woodPlanDataSet.sp00187_Contractor_Contact_Item.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Team Contact", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            ibool_FormStillLoading = true;  // Prevent validated code from firing on certain editors //
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        intContractorIDGridLookUpEdit.Focus();

                        intContractorIDGridLookUpEdit.Properties.ReadOnly = false;
                        strContactNameTextEdit.Properties.ReadOnly = false;
                        strTelTextEdit.Properties.ReadOnly = false;
                        strMobileTelTextEdit.Properties.ReadOnly = false;
                        strFaxTextEdit.Properties.ReadOnly = false;
                        strEmailTextEdit.Properties.ReadOnly = false;
                        bDefaultCheckEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        intContractorIDGridLookUpEdit.Focus();

                        intContractorIDGridLookUpEdit.Properties.ReadOnly = false;
                        strContactNameTextEdit.Properties.ReadOnly = false;
                        strTelTextEdit.Properties.ReadOnly = false;
                        strMobileTelTextEdit.Properties.ReadOnly = false;
                        strFaxTextEdit.Properties.ReadOnly = false;
                        strEmailTextEdit.Properties.ReadOnly = false;
                        bDefaultCheckEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        strContactNameTextEdit.Focus();

                        intContractorIDGridLookUpEdit.Properties.ReadOnly = false;
                        strContactNameTextEdit.Properties.ReadOnly = false;
                        strTelTextEdit.Properties.ReadOnly = false;
                        strMobileTelTextEdit.Properties.ReadOnly = false;
                        strFaxTextEdit.Properties.ReadOnly = false;
                        strEmailTextEdit.Properties.ReadOnly = false;
                        bDefaultCheckEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        bDefaultCheckEdit.Focus();

                        intContractorIDGridLookUpEdit.Properties.ReadOnly = true;
                        strContactNameTextEdit.Properties.ReadOnly = true;
                        strTelTextEdit.Properties.ReadOnly = true;
                        strMobileTelTextEdit.Properties.ReadOnly = true;
                        strFaxTextEdit.Properties.ReadOnly = true;
                        strEmailTextEdit.Properties.ReadOnly = true;
                        bDefaultCheckEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            
            ibool_ignoreValidation = true;
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.woodPlanDataSet.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_Core_Contractor_Contact_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Contractor_Contact_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;

            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            //if (dxErrorProvider1.HasErrors)
            if (dxErrorProvider1.HasErrorsOfType(ErrorType.Default))
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp00187ContractorContactItemBindingSource.EndEdit();
            try
            {
                this.sp00187_Contractor_Contact_ItemTableAdapter.Update(woodPlanDataSet);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp00187ContractorContactItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["intContactID"]) + ";";
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_Core_Contractor_Manager")
                    {
                        var fParentForm = (frm_Core_Contractor_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs, "", "", "", "", "", "", "", "", "", "");
                    }
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.woodPlanDataSet.sp00187_Contractor_Contact_Item.Rows.Count; i++)
            {
                switch (this.woodPlanDataSet.sp00187_Contractor_Contact_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Set_Total_View_Active_Field_Enabled();

            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            //foreach (Control c in dxValidationProvider.GetInvalidControls())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    //strErrorMessages += "--> " + item.Text.ToString() + "  " + dxValidationProvider.GetValidationRule(c).ErrorText + "\n";
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void bDefaultCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void bDefaultCheckEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;
            Set_Error_Message_Status();
        }
        private void bDefaultCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
        }

        private void intContractorIDSpinEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 1, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp04026_Contractor_List_With_BlankTableAdapter.Fill(dataSet_GC_DataEntry.sp04026_Contractor_List_With_Blank);
                }
            }
        }

        private void intContractorIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void intContractorIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int? intValue = Convert.ToInt32(glue.EditValue);
            if (this.strFormMode != "blockedit" && intValue == 0)
            {
                dxErrorProvider1.SetError(intContractorIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(intContractorIDGridLookUpEdit, "");
            }
        }
        private void intContractorIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Set_Total_View_Active_Field_Enabled();
        }

        private void strContactNameTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void strContactNameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(strContactNameTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(strContactNameTextEdit, "");
            }
        }
        private void strContactNameTextEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Set_Total_View_Active_Field_Enabled();
        }

        private void bDisabledCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void bDisabledCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Set_Total_View_Active_Field_Enabled();
        }

        private void strMobileTelTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void strMobileTelTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;
            Set_Error_Message_Status();
        }
        private void strMobileTelTextEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Set_Total_View_Active_Field_Enabled();
        }

        private void strEmailTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void strEmailTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;
            Set_Error_Message_Status();
        }
        private void strEmailTextEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Set_Total_View_Active_Field_Enabled();
        }


        private void Set_Total_View_Active_Field_Enabled()
        {
            DataRowView currentRow = (DataRowView)sp00187ContractorContactItemBindingSource.Current;
            if (currentRow == null) return;
            string strEmail = currentRow["strEmail"].ToString();
            string strContactName = currentRow["strContactName"].ToString();
            string strMobileTel = currentRow["strMobileTel"].ToString();

            int intContractorID = 0;
            try { intContractorID = Convert.ToInt32(currentRow["intContractorID"]); } catch (Exception) { };

            int bDisabled = 0; 
            try { bDisabled = Convert.ToInt32(currentRow["bDisabled"]); } catch (Exception) { };

            int intIsActiveTotalViewUser = 0;
            try { intIsActiveTotalViewUser = Convert.ToInt32(currentRow["IsActiveTotalViewUser"]); } catch (Exception) { };

            if (string.IsNullOrWhiteSpace(strEmail) || string.IsNullOrWhiteSpace(strContactName) || string.IsNullOrWhiteSpace(strMobileTel) || intContractorID <= 0 || bDisabled == 1)
            {
                IsActiveTotalViewUserCheckEdit.Properties.ReadOnly = true;
                if (intIsActiveTotalViewUser > 0)
                {
                    currentRow["IsActiveTotalViewUser"] = 0;
                    this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                }
            }
            else //if (intIsActiveTotalViewUser > 0)
            {
                // Everything present so see if any other contacts share this email address and are marked as TotalViewActive - if yes clear the flag and disable //
                int intCount = 0;
                int intContactID = 0;
                try { intContactID = Convert.ToInt32(currentRow["intContactID"]); }
                catch (Exception) { };

                using (var GetCount = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                {
                    GetCount.ChangeConnectionString(strConnectionString);
                    try
                    {
                        intCount = Convert.ToInt32(GetCount.sp01061_Core_Contractor_Contact_Check_Total_View_Active_Valid(intContactID, strEmail));
                    }
                    catch (Exception) { }
                }
                if (intCount > 0)
                {
                    IsActiveTotalViewUserCheckEdit.Properties.ReadOnly = true;
                    currentRow["IsActiveTotalViewUser"] = 0;
                    this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                }
                else
                {
                    IsActiveTotalViewUserCheckEdit.Properties.ReadOnly = false;
                }
            //}
            //else
            //{
            //    IsActiveTotalViewUserCheckEdit.Properties.ReadOnly = false;
            }
        }
        
        private void Set_Error_Message_Status()
        {
            string message = "";
            string strMobileTel = strMobileTelTextEdit.EditValue.ToString();
            if (!Classes.FormatCheck.mobileIsValid(strMobileTel, bDefaultCheckEdit.Checked, out message) && this.strFormMode != "blockedit")
            {
                dxErrorProvider1.SetError(strMobileTelTextEdit, message);
            }
            else
            {
                dxErrorProvider1.SetError(strMobileTelTextEdit, "");
            }

            string strEmail = strEmailTextEdit.EditValue.ToString();
            if (!Classes.FormatCheck.emailIsValid(strEmail, bDefaultCheckEdit.Checked, out message) && this.strFormMode != "blockedit")
            {
                dxErrorProvider1.SetError(strEmailTextEdit, message);
            }
            else
            {
                dxErrorProvider1.SetError(strEmailTextEdit, "");
            }

            if (this.strFormMode != "blockedit" && bDefaultCheckEdit.Checked && (string.IsNullOrWhiteSpace(strMobileTel) || string.IsNullOrWhiteSpace(strEmail)))
            {
                dxErrorProvider1.SetError(bDefaultCheckEdit, "Mobile Number and Email address are mandatory for the default contact", ErrorType.Information);
            }
            else
            {
                dxErrorProvider1.SetError(bDefaultCheckEdit, "");
            }
        }

        private void IsActiveTotalViewUserCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void IsActiveTotalViewUserCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Set_Total_View_Active_Field_Enabled();
        }

        #endregion





    }
}


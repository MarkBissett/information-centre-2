namespace WoodPlan5
{
    partial class frm_Core_Site_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Site_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colWorkspaceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SiteImageFileButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.sp03007EPSiteEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.btnViewKML = new DevExpress.XtraEditors.SimpleButton();
            this.SiteKMLDataMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.GeoFenceDistanceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.COAddressMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.btnViewOnMap = new DevExpress.XtraEditors.SimpleButton();
            this.IsSnowClearanceSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientsSiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientsSiteCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HubIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LocationYSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LocationXSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GrittingSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.CompanyIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04002GCSelectCompanyListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SiteTextMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp03044EPAssetManagerListSimpleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAssetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetSubTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModelNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLifeSpanValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLifeSpanValueDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentConditionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConditionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextVisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp03067EPSiteEditLinkedInspectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInspectionEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTimeDeductions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalTimeDeductionsRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colTotalTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEdit1 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03063EPMappingWorkspacesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCreatedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colWorkspaceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SiteTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00227PicklistListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp03009EPClientListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactPersonTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactPersonPositionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteTelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteMobileTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteFaxTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.XCoordinateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.YCoordinateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SiteEmailMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SitePostcodeTextEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForContactPerson = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteMobile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteFax = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSiteText = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactPersonPosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForHubID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrittingSiteCheckEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIsSnowClearanceSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteImageFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSitePostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCOAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteKMLData = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForViewKML = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientsSiteCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientsSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForXCoordinate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForYCoordinate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGeoFenceDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp03007_EP_Site_EditTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03007_EP_Site_EditTableAdapter();
            this.sp00227_Picklist_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00227_Picklist_List_With_BlankTableAdapter();
            this.sp03009_EP_Client_List_With_BlankTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03009_EP_Client_List_With_BlankTableAdapter();
            this.sp03063_EP_Mapping_Workspaces_With_BlankTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03063_EP_Mapping_Workspaces_With_BlankTableAdapter();
            this.sp03067_EP_Site_Edit_Linked_InspectionsTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03067_EP_Site_Edit_Linked_InspectionsTableAdapter();
            this.sp03044_EP_Asset_Manager_List_SimpleTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03044_EP_Asset_Manager_List_SimpleTableAdapter();
            this.sp04002_GC_Select_Company_ListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04002_GC_Select_Company_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SiteImageFileButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03007EPSiteEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteKMLDataMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeoFenceDistanceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.COAddressMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSnowClearanceSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientsSiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientsSiteCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HubIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04002GCSelectCompanyListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteTextMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03044EPAssetManagerListSimpleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03067EPSiteEditLinkedInspectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03063EPMappingWorkspacesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00227PicklistListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03009EPClientListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonPositionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteTelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteMobileTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteFaxTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XCoordinateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YCoordinateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteEmailMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHubID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingSiteCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSnowClearanceSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteImageFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCOAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteKMLData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForViewKML)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientsSiteCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientsSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForXCoordinate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYCoordinate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGeoFenceDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(945, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 673);
            this.barDockControlBottom.Size = new System.Drawing.Size(945, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 647);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(945, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 647);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colWorkspaceID
            // 
            this.colWorkspaceID.Caption = "Workspace ID";
            this.colWorkspaceID.FieldName = "WorkspaceID";
            this.colWorkspaceID.Name = "colWorkspaceID";
            this.colWorkspaceID.OptionsColumn.AllowEdit = false;
            this.colWorkspaceID.OptionsColumn.AllowFocus = false;
            this.colWorkspaceID.OptionsColumn.ReadOnly = true;
            this.colWorkspaceID.Width = 109;
            // 
            // colID
            // 
            this.colID.Caption = "Item ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Width = 74;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.barButtonItem1});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.bar2.Text = "Tools";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Show Map";
            this.barButtonItem1.Id = 15;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "View On Map - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to view the current record on the map.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barButtonItem1.SuperTip = superToolTip4;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(945, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 673);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(945, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 647);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(945, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 647);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "info_16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SiteImageFileButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.btnViewKML);
            this.dataLayoutControl1.Controls.Add(this.SiteKMLDataMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.GeoFenceDistanceSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.COAddressMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.btnViewOnMap);
            this.dataLayoutControl1.Controls.Add(this.IsSnowClearanceSiteCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientsSiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientsSiteCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HubIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationYSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationXSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GrittingSiteCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.CompanyIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteTextMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl2);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.gridLookUpEdit1);
            this.dataLayoutControl1.Controls.Add(this.SiteTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactPersonTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactPersonPositionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteTelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteMobileTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteFaxTextEdit);
            this.dataLayoutControl1.Controls.Add(this.XCoordinateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.YCoordinateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteEmailMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SitePostcodeTextEdit);
            this.dataLayoutControl1.DataSource = this.sp03007EPSiteEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(975, 104, 616, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(945, 647);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SiteImageFileButtonEdit
            // 
            this.SiteImageFileButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteImageFile", true));
            this.SiteImageFileButtonEdit.Location = new System.Drawing.Point(141, 383);
            this.SiteImageFileButtonEdit.MenuManager = this.barManager1;
            this.SiteImageFileButtonEdit.Name = "SiteImageFileButtonEdit";
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.SiteImageFileButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose File", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Select File", "select file", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View File", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "view file", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteImageFileButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SiteImageFileButtonEdit.Size = new System.Drawing.Size(751, 20);
            this.SiteImageFileButtonEdit.StyleController = this.dataLayoutControl1;
            this.SiteImageFileButtonEdit.TabIndex = 13;
            this.SiteImageFileButtonEdit.TabStop = false;
            this.SiteImageFileButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteImageFileButtonEdit_ButtonClick);
            // 
            // sp03007EPSiteEditBindingSource
            // 
            this.sp03007EPSiteEditBindingSource.DataMember = "sp03007_EP_Site_Edit";
            this.sp03007EPSiteEditBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnViewKML
            // 
            this.btnViewKML.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnViewKML.ImageOptions.Image")));
            this.btnViewKML.Location = new System.Drawing.Point(782, 313);
            this.btnViewKML.Name = "btnViewKML";
            this.btnViewKML.Size = new System.Drawing.Size(110, 22);
            this.btnViewKML.StyleController = this.dataLayoutControl1;
            this.btnViewKML.TabIndex = 47;
            this.btnViewKML.Text = "View KML on Map";
            this.btnViewKML.Click += new System.EventHandler(this.btnViewKML_Click);
            // 
            // SiteKMLDataMemoEdit
            // 
            this.SiteKMLDataMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteKMLData", true));
            this.SiteKMLDataMemoEdit.Location = new System.Drawing.Point(36, 339);
            this.SiteKMLDataMemoEdit.MenuManager = this.barManager1;
            this.SiteKMLDataMemoEdit.Name = "SiteKMLDataMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteKMLDataMemoEdit, true);
            this.SiteKMLDataMemoEdit.Size = new System.Drawing.Size(856, 238);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteKMLDataMemoEdit, optionsSpelling1);
            this.SiteKMLDataMemoEdit.StyleController = this.dataLayoutControl1;
            this.SiteKMLDataMemoEdit.TabIndex = 46;
            // 
            // GeoFenceDistanceSpinEdit
            // 
            this.GeoFenceDistanceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "GeoFenceDistance", true));
            this.GeoFenceDistanceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.GeoFenceDistanceSpinEdit.Location = new System.Drawing.Point(117, 735);
            this.GeoFenceDistanceSpinEdit.MenuManager = this.barManager1;
            this.GeoFenceDistanceSpinEdit.Name = "GeoFenceDistanceSpinEdit";
            this.GeoFenceDistanceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GeoFenceDistanceSpinEdit.Properties.IsFloatValue = false;
            this.GeoFenceDistanceSpinEdit.Properties.Mask.EditMask = "######0 Metres";
            this.GeoFenceDistanceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.GeoFenceDistanceSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.GeoFenceDistanceSpinEdit.Size = new System.Drawing.Size(113, 20);
            this.GeoFenceDistanceSpinEdit.StyleController = this.dataLayoutControl1;
            this.GeoFenceDistanceSpinEdit.TabIndex = 44;
            // 
            // COAddressMemoEdit
            // 
            this.COAddressMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "COAddress", true));
            this.COAddressMemoEdit.Location = new System.Drawing.Point(141, 467);
            this.COAddressMemoEdit.MenuManager = this.barManager1;
            this.COAddressMemoEdit.Name = "COAddressMemoEdit";
            this.COAddressMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.COAddressMemoEdit, true);
            this.COAddressMemoEdit.Size = new System.Drawing.Size(751, 91);
            this.scSpellChecker.SetSpellCheckerOptions(this.COAddressMemoEdit, optionsSpelling2);
            this.COAddressMemoEdit.StyleController = this.dataLayoutControl1;
            this.COAddressMemoEdit.TabIndex = 43;
            // 
            // btnViewOnMap
            // 
            this.btnViewOnMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnViewOnMap.ImageOptions.Image")));
            this.btnViewOnMap.Location = new System.Drawing.Point(12, 759);
            this.btnViewOnMap.Name = "btnViewOnMap";
            this.btnViewOnMap.Size = new System.Drawing.Size(104, 22);
            this.btnViewOnMap.StyleController = this.dataLayoutControl1;
            this.btnViewOnMap.TabIndex = 42;
            this.btnViewOnMap.Text = "View on Map";
            this.btnViewOnMap.Click += new System.EventHandler(this.btnViewOnMap_Click);
            // 
            // IsSnowClearanceSiteCheckEdit
            // 
            this.IsSnowClearanceSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "IsSnowClearanceSite", true));
            this.IsSnowClearanceSiteCheckEdit.Location = new System.Drawing.Point(141, 360);
            this.IsSnowClearanceSiteCheckEdit.MenuManager = this.barManager1;
            this.IsSnowClearanceSiteCheckEdit.Name = "IsSnowClearanceSiteCheckEdit";
            this.IsSnowClearanceSiteCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsSnowClearanceSiteCheckEdit.Properties.ValueChecked = 1;
            this.IsSnowClearanceSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.IsSnowClearanceSiteCheckEdit.Size = new System.Drawing.Size(87, 19);
            this.IsSnowClearanceSiteCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsSnowClearanceSiteCheckEdit.TabIndex = 41;
            // 
            // ClientsSiteIDTextEdit
            // 
            this.ClientsSiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "ClientsSiteID", true));
            this.ClientsSiteIDTextEdit.Location = new System.Drawing.Point(117, 209);
            this.ClientsSiteIDTextEdit.MenuManager = this.barManager1;
            this.ClientsSiteIDTextEdit.Name = "ClientsSiteIDTextEdit";
            this.ClientsSiteIDTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientsSiteIDTextEdit, true);
            this.ClientsSiteIDTextEdit.Size = new System.Drawing.Size(186, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientsSiteIDTextEdit, optionsSpelling3);
            this.ClientsSiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientsSiteIDTextEdit.TabIndex = 40;
            // 
            // ClientsSiteCodeTextEdit
            // 
            this.ClientsSiteCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "ClientsSiteCode", true));
            this.ClientsSiteCodeTextEdit.Location = new System.Drawing.Point(117, 185);
            this.ClientsSiteCodeTextEdit.MenuManager = this.barManager1;
            this.ClientsSiteCodeTextEdit.Name = "ClientsSiteCodeTextEdit";
            this.ClientsSiteCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientsSiteCodeTextEdit, true);
            this.ClientsSiteCodeTextEdit.Size = new System.Drawing.Size(186, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientsSiteCodeTextEdit, optionsSpelling4);
            this.ClientsSiteCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientsSiteCodeTextEdit.TabIndex = 39;
            // 
            // HubIDTextEdit
            // 
            this.HubIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "HubID", true));
            this.HubIDTextEdit.Location = new System.Drawing.Point(141, 313);
            this.HubIDTextEdit.MenuManager = this.barManager1;
            this.HubIDTextEdit.Name = "HubIDTextEdit";
            this.HubIDTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HubIDTextEdit, true);
            this.HubIDTextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HubIDTextEdit, optionsSpelling5);
            this.HubIDTextEdit.StyleController = this.dataLayoutControl1;
            this.HubIDTextEdit.TabIndex = 37;
            // 
            // SiteOrderSpinEdit
            // 
            this.SiteOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteOrder", true));
            this.SiteOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SiteOrderSpinEdit.Location = new System.Drawing.Point(117, 161);
            this.SiteOrderSpinEdit.MenuManager = this.barManager1;
            this.SiteOrderSpinEdit.Name = "SiteOrderSpinEdit";
            this.SiteOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SiteOrderSpinEdit.Properties.IsFloatValue = false;
            this.SiteOrderSpinEdit.Properties.Mask.EditMask = "N00";
            this.SiteOrderSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.SiteOrderSpinEdit.Size = new System.Drawing.Size(799, 20);
            this.SiteOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.SiteOrderSpinEdit.TabIndex = 36;
            // 
            // LocationYSpinEdit
            // 
            this.LocationYSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "LocationY", true));
            this.LocationYSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LocationYSpinEdit.Location = new System.Drawing.Point(117, 711);
            this.LocationYSpinEdit.MenuManager = this.barManager1;
            this.LocationYSpinEdit.Name = "LocationYSpinEdit";
            this.LocationYSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LocationYSpinEdit.Properties.ReadOnly = true;
            this.LocationYSpinEdit.Size = new System.Drawing.Size(186, 20);
            this.LocationYSpinEdit.StyleController = this.dataLayoutControl1;
            this.LocationYSpinEdit.TabIndex = 35;
            // 
            // LocationXSpinEdit
            // 
            this.LocationXSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "LocationX", true));
            this.LocationXSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LocationXSpinEdit.Location = new System.Drawing.Point(117, 687);
            this.LocationXSpinEdit.MenuManager = this.barManager1;
            this.LocationXSpinEdit.Name = "LocationXSpinEdit";
            this.LocationXSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LocationXSpinEdit.Properties.ReadOnly = true;
            this.LocationXSpinEdit.Size = new System.Drawing.Size(186, 20);
            this.LocationXSpinEdit.StyleController = this.dataLayoutControl1;
            this.LocationXSpinEdit.TabIndex = 34;
            // 
            // GrittingSiteCheckEdit
            // 
            this.GrittingSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "GrittingSite", true));
            this.GrittingSiteCheckEdit.Location = new System.Drawing.Point(141, 337);
            this.GrittingSiteCheckEdit.MenuManager = this.barManager1;
            this.GrittingSiteCheckEdit.Name = "GrittingSiteCheckEdit";
            this.GrittingSiteCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GrittingSiteCheckEdit.Properties.ValueChecked = 1;
            this.GrittingSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.GrittingSiteCheckEdit.Size = new System.Drawing.Size(87, 19);
            this.GrittingSiteCheckEdit.StyleController = this.dataLayoutControl1;
            this.GrittingSiteCheckEdit.TabIndex = 38;
            // 
            // CompanyIDGridLookUpEdit
            // 
            this.CompanyIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "CompanyID", true));
            this.CompanyIDGridLookUpEdit.Location = new System.Drawing.Point(117, 61);
            this.CompanyIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CompanyIDGridLookUpEdit.Name = "CompanyIDGridLookUpEdit";
            editorButtonImageOptions3.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions4.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.CompanyIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CompanyIDGridLookUpEdit.Properties.DataSource = this.sp04002GCSelectCompanyListBindingSource;
            this.CompanyIDGridLookUpEdit.Properties.DisplayMember = "CompanyName";
            this.CompanyIDGridLookUpEdit.Properties.NullText = "";
            this.CompanyIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.CompanyIDGridLookUpEdit.Properties.ValueMember = "CompanyID";
            this.CompanyIDGridLookUpEdit.Size = new System.Drawing.Size(799, 22);
            this.CompanyIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CompanyIDGridLookUpEdit.TabIndex = 33;
            this.CompanyIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CompanyIDGridLookUpEdit_ButtonClick);
            // 
            // sp04002GCSelectCompanyListBindingSource
            // 
            this.sp04002GCSelectCompanyListBindingSource.DataMember = "sp04002_GC_Select_Company_List";
            this.sp04002GCSelectCompanyListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompanyCode,
            this.colCompanyID,
            this.colCompanyName});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 1;
            this.colCompanyCode.Width = 126;
            // 
            // colCompanyID
            // 
            this.colCompanyID.Caption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 88;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 0;
            this.colCompanyName.Width = 261;
            // 
            // SiteTextMemoEdit
            // 
            this.SiteTextMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteText", true));
            this.SiteTextMemoEdit.Location = new System.Drawing.Point(141, 502);
            this.SiteTextMemoEdit.MenuManager = this.barManager1;
            this.SiteTextMemoEdit.Name = "SiteTextMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteTextMemoEdit, true);
            this.SiteTextMemoEdit.Size = new System.Drawing.Size(751, 65);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteTextMemoEdit, optionsSpelling6);
            this.SiteTextMemoEdit.StyleController = this.dataLayoutControl1;
            this.SiteTextMemoEdit.TabIndex = 32;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp03044EPAssetManagerListSimpleBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Delete Selected Record(s) ", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(36, 313);
            this.gridControl2.MainView = this.gridView3;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControl2.Size = new System.Drawing.Size(856, 264);
            this.gridControl2.TabIndex = 31;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp03044EPAssetManagerListSimpleBindingSource
            // 
            this.sp03044EPAssetManagerListSimpleBindingSource.DataMember = "sp03044_EP_Asset_Manager_List_Simple";
            this.sp03044EPAssetManagerListSimpleBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridView3
            // 
            this.gridView3.ActiveFilterEnabled = false;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAssetID,
            this.colSiteID1,
            this.colSiteName1,
            this.colSiteCode1,
            this.colClientName2,
            this.colClientCode2,
            this.colAssetTypeID,
            this.colAssetTypeDescription,
            this.colAssetTypeOrder,
            this.colAssetSubTypeID,
            this.colAssetSubTypeDescription,
            this.colAssetSubTypeOrder,
            this.colXCoordinate,
            this.colYCoordinate,
            this.colPolygonXY,
            this.colArea,
            this.colLength,
            this.colWidth,
            this.colStatusID,
            this.colStatusDescription,
            this.colPartNumber,
            this.colSerialNumber,
            this.colModelNumber,
            this.colAssetNumber,
            this.colLifeSpanValue,
            this.colLifeSpanValueDescriptor,
            this.colCurrentConditionID,
            this.colConditionDescription,
            this.colLastVisitDate,
            this.colNextVisitDate,
            this.gridColumn1,
            this.colMapID});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.ColumnFilterPopupMaxRecordsCount = 10000;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssetNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colAssetID
            // 
            this.colAssetID.Caption = "Asset ID";
            this.colAssetID.FieldName = "AssetID";
            this.colAssetID.Name = "colAssetID";
            this.colAssetID.OptionsColumn.AllowEdit = false;
            this.colAssetID.OptionsColumn.AllowFocus = false;
            this.colAssetID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "Site Code";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            // 
            // colClientCode2
            // 
            this.colClientCode2.Caption = "Client Code";
            this.colClientCode2.FieldName = "ClientCode";
            this.colClientCode2.Name = "colClientCode2";
            this.colClientCode2.OptionsColumn.AllowEdit = false;
            this.colClientCode2.OptionsColumn.AllowFocus = false;
            this.colClientCode2.OptionsColumn.ReadOnly = true;
            // 
            // colAssetTypeID
            // 
            this.colAssetTypeID.Caption = "Asset Type ID";
            this.colAssetTypeID.FieldName = "AssetTypeID";
            this.colAssetTypeID.Name = "colAssetTypeID";
            this.colAssetTypeID.OptionsColumn.AllowEdit = false;
            this.colAssetTypeID.OptionsColumn.AllowFocus = false;
            this.colAssetTypeID.OptionsColumn.ReadOnly = true;
            this.colAssetTypeID.Width = 106;
            // 
            // colAssetTypeDescription
            // 
            this.colAssetTypeDescription.Caption = "Asset Type";
            this.colAssetTypeDescription.FieldName = "AssetTypeDescription";
            this.colAssetTypeDescription.Name = "colAssetTypeDescription";
            this.colAssetTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetTypeDescription.Width = 129;
            // 
            // colAssetTypeOrder
            // 
            this.colAssetTypeOrder.Caption = "Asset Type Order";
            this.colAssetTypeOrder.FieldName = "AssetTypeOrder";
            this.colAssetTypeOrder.Name = "colAssetTypeOrder";
            this.colAssetTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetTypeOrder.OptionsColumn.ReadOnly = true;
            this.colAssetTypeOrder.Width = 131;
            // 
            // colAssetSubTypeID
            // 
            this.colAssetSubTypeID.Caption = "Asset Sub-Type ID";
            this.colAssetSubTypeID.FieldName = "AssetSubTypeID";
            this.colAssetSubTypeID.Name = "colAssetSubTypeID";
            this.colAssetSubTypeID.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeID.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeID.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeID.Width = 115;
            // 
            // colAssetSubTypeDescription
            // 
            this.colAssetSubTypeDescription.Caption = "Asset Sub-Type";
            this.colAssetSubTypeDescription.FieldName = "AssetSubTypeDescription";
            this.colAssetSubTypeDescription.Name = "colAssetSubTypeDescription";
            this.colAssetSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeDescription.Visible = true;
            this.colAssetSubTypeDescription.VisibleIndex = 0;
            this.colAssetSubTypeDescription.Width = 120;
            // 
            // colAssetSubTypeOrder
            // 
            this.colAssetSubTypeOrder.Caption = "Asset Sub-Type Order";
            this.colAssetSubTypeOrder.FieldName = "AssetSubTypeOrder";
            this.colAssetSubTypeOrder.Name = "colAssetSubTypeOrder";
            this.colAssetSubTypeOrder.OptionsColumn.AllowEdit = false;
            this.colAssetSubTypeOrder.OptionsColumn.AllowFocus = false;
            this.colAssetSubTypeOrder.OptionsColumn.ReadOnly = true;
            this.colAssetSubTypeOrder.Width = 121;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "X Coordinate";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Y Coordinate";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            // 
            // colPolygonXY
            // 
            this.colPolygonXY.Caption = "Poly Coordinates";
            this.colPolygonXY.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colPolygonXY.FieldName = "PolygonXY";
            this.colPolygonXY.Name = "colPolygonXY";
            this.colPolygonXY.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area";
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            // 
            // colLength
            // 
            this.colLength.Caption = "Length";
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.OptionsColumn.AllowEdit = false;
            this.colLength.OptionsColumn.AllowFocus = false;
            this.colLength.OptionsColumn.ReadOnly = true;
            // 
            // colWidth
            // 
            this.colWidth.Caption = "Width";
            this.colWidth.FieldName = "Width";
            this.colWidth.Name = "colWidth";
            this.colWidth.OptionsColumn.AllowEdit = false;
            this.colWidth.OptionsColumn.AllowFocus = false;
            this.colWidth.OptionsColumn.ReadOnly = true;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 5;
            // 
            // colPartNumber
            // 
            this.colPartNumber.Caption = "Part Number";
            this.colPartNumber.FieldName = "PartNumber";
            this.colPartNumber.Name = "colPartNumber";
            this.colPartNumber.OptionsColumn.AllowEdit = false;
            this.colPartNumber.OptionsColumn.AllowFocus = false;
            this.colPartNumber.OptionsColumn.ReadOnly = true;
            this.colPartNumber.Visible = true;
            this.colPartNumber.VisibleIndex = 2;
            this.colPartNumber.Width = 102;
            // 
            // colSerialNumber
            // 
            this.colSerialNumber.Caption = "Serial Number";
            this.colSerialNumber.FieldName = "SerialNumber";
            this.colSerialNumber.Name = "colSerialNumber";
            this.colSerialNumber.OptionsColumn.AllowEdit = false;
            this.colSerialNumber.OptionsColumn.AllowFocus = false;
            this.colSerialNumber.OptionsColumn.ReadOnly = true;
            this.colSerialNumber.Visible = true;
            this.colSerialNumber.VisibleIndex = 3;
            this.colSerialNumber.Width = 102;
            // 
            // colModelNumber
            // 
            this.colModelNumber.Caption = "Model Number";
            this.colModelNumber.FieldName = "ModelNumber";
            this.colModelNumber.Name = "colModelNumber";
            this.colModelNumber.OptionsColumn.AllowEdit = false;
            this.colModelNumber.OptionsColumn.AllowFocus = false;
            this.colModelNumber.OptionsColumn.ReadOnly = true;
            this.colModelNumber.Visible = true;
            this.colModelNumber.VisibleIndex = 4;
            this.colModelNumber.Width = 109;
            // 
            // colAssetNumber
            // 
            this.colAssetNumber.Caption = "Asset Number";
            this.colAssetNumber.FieldName = "AssetNumber";
            this.colAssetNumber.Name = "colAssetNumber";
            this.colAssetNumber.OptionsColumn.AllowEdit = false;
            this.colAssetNumber.OptionsColumn.AllowFocus = false;
            this.colAssetNumber.OptionsColumn.ReadOnly = true;
            this.colAssetNumber.Visible = true;
            this.colAssetNumber.VisibleIndex = 1;
            this.colAssetNumber.Width = 134;
            // 
            // colLifeSpanValue
            // 
            this.colLifeSpanValue.Caption = "Life Span";
            this.colLifeSpanValue.FieldName = "LifeSpanValue";
            this.colLifeSpanValue.Name = "colLifeSpanValue";
            this.colLifeSpanValue.OptionsColumn.AllowEdit = false;
            this.colLifeSpanValue.OptionsColumn.AllowFocus = false;
            this.colLifeSpanValue.OptionsColumn.ReadOnly = true;
            this.colLifeSpanValue.Visible = true;
            this.colLifeSpanValue.VisibleIndex = 6;
            this.colLifeSpanValue.Width = 68;
            // 
            // colLifeSpanValueDescriptor
            // 
            this.colLifeSpanValueDescriptor.Caption = "Life Span Description";
            this.colLifeSpanValueDescriptor.FieldName = "LifeSpanValueDescriptor";
            this.colLifeSpanValueDescriptor.Name = "colLifeSpanValueDescriptor";
            this.colLifeSpanValueDescriptor.OptionsColumn.AllowEdit = false;
            this.colLifeSpanValueDescriptor.OptionsColumn.AllowFocus = false;
            this.colLifeSpanValueDescriptor.OptionsColumn.ReadOnly = true;
            this.colLifeSpanValueDescriptor.Visible = true;
            this.colLifeSpanValueDescriptor.VisibleIndex = 7;
            this.colLifeSpanValueDescriptor.Width = 123;
            // 
            // colCurrentConditionID
            // 
            this.colCurrentConditionID.Caption = "Current Condition ID";
            this.colCurrentConditionID.FieldName = "CurrentConditionID";
            this.colCurrentConditionID.Name = "colCurrentConditionID";
            this.colCurrentConditionID.OptionsColumn.AllowEdit = false;
            this.colCurrentConditionID.OptionsColumn.AllowFocus = false;
            this.colCurrentConditionID.OptionsColumn.ReadOnly = true;
            this.colCurrentConditionID.Width = 129;
            // 
            // colConditionDescription
            // 
            this.colConditionDescription.Caption = "Current Condition";
            this.colConditionDescription.FieldName = "ConditionDescription";
            this.colConditionDescription.Name = "colConditionDescription";
            this.colConditionDescription.OptionsColumn.AllowEdit = false;
            this.colConditionDescription.OptionsColumn.AllowFocus = false;
            this.colConditionDescription.OptionsColumn.ReadOnly = true;
            this.colConditionDescription.Visible = true;
            this.colConditionDescription.VisibleIndex = 8;
            this.colConditionDescription.Width = 136;
            // 
            // colLastVisitDate
            // 
            this.colLastVisitDate.Caption = "Last Visit Date";
            this.colLastVisitDate.FieldName = "LastVisitDate";
            this.colLastVisitDate.Name = "colLastVisitDate";
            this.colLastVisitDate.OptionsColumn.AllowEdit = false;
            this.colLastVisitDate.OptionsColumn.AllowFocus = false;
            this.colLastVisitDate.OptionsColumn.ReadOnly = true;
            this.colLastVisitDate.Visible = true;
            this.colLastVisitDate.VisibleIndex = 9;
            this.colLastVisitDate.Width = 96;
            // 
            // colNextVisitDate
            // 
            this.colNextVisitDate.Caption = "Next Visit Date";
            this.colNextVisitDate.FieldName = "NextVisitDate";
            this.colNextVisitDate.Name = "colNextVisitDate";
            this.colNextVisitDate.OptionsColumn.AllowEdit = false;
            this.colNextVisitDate.OptionsColumn.AllowFocus = false;
            this.colNextVisitDate.OptionsColumn.ReadOnly = true;
            this.colNextVisitDate.Visible = true;
            this.colNextVisitDate.VisibleIndex = 10;
            this.colNextVisitDate.Width = 98;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Remarks";
            this.gridColumn1.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn1.FieldName = "Remarks";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 11;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.Visible = true;
            this.colMapID.VisibleIndex = 12;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp03067EPSiteEditLinkedInspectionsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(36, 313);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.gridControl1.Size = new System.Drawing.Size(856, 264);
            this.gridControl1.TabIndex = 30;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp03067EPSiteEditLinkedInspectionsBindingSource
            // 
            this.sp03067EPSiteEditLinkedInspectionsBindingSource.DataMember = "sp03067_EP_Site_Edit_Linked_Inspections";
            this.sp03067EPSiteEditLinkedInspectionsBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID,
            this.colSiteID,
            this.colSiteName,
            this.colClientID1,
            this.colInspectionDate,
            this.colInspectorID,
            this.colInspector,
            this.colInspectionStartTime,
            this.colInspectionEndTime,
            this.colTotalTimeDeductions,
            this.colTotalTimeDeductionsRemarks,
            this.colTotalTimeTaken,
            this.colRemarks2});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            this.colInspectionID.Width = 94;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Visible = true;
            this.colInspectionDate.VisibleIndex = 0;
            this.colInspectionDate.Width = 107;
            // 
            // colInspectorID
            // 
            this.colInspectorID.Caption = "Inspector ID";
            this.colInspectorID.FieldName = "InspectorID";
            this.colInspectorID.Name = "colInspectorID";
            this.colInspectorID.OptionsColumn.AllowEdit = false;
            this.colInspectorID.OptionsColumn.AllowFocus = false;
            this.colInspectorID.OptionsColumn.ReadOnly = true;
            this.colInspectorID.Width = 111;
            // 
            // colInspector
            // 
            this.colInspector.Caption = "Inspector";
            this.colInspector.FieldName = "Inspector";
            this.colInspector.Name = "colInspector";
            this.colInspector.OptionsColumn.AllowEdit = false;
            this.colInspector.OptionsColumn.AllowFocus = false;
            this.colInspector.OptionsColumn.ReadOnly = true;
            this.colInspector.Visible = true;
            this.colInspector.VisibleIndex = 1;
            this.colInspector.Width = 127;
            // 
            // colInspectionStartTime
            // 
            this.colInspectionStartTime.Caption = "Start Date \\ Time";
            this.colInspectionStartTime.ColumnEdit = this.repositoryItemTextEdit1;
            this.colInspectionStartTime.FieldName = "InspectionStartTime";
            this.colInspectionStartTime.Name = "colInspectionStartTime";
            this.colInspectionStartTime.OptionsColumn.AllowEdit = false;
            this.colInspectionStartTime.OptionsColumn.AllowFocus = false;
            this.colInspectionStartTime.OptionsColumn.ReadOnly = true;
            this.colInspectionStartTime.Visible = true;
            this.colInspectionStartTime.VisibleIndex = 2;
            this.colInspectionStartTime.Width = 111;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colInspectionEndTime
            // 
            this.colInspectionEndTime.Caption = "End Date Time";
            this.colInspectionEndTime.ColumnEdit = this.repositoryItemTextEdit1;
            this.colInspectionEndTime.FieldName = "InspectionEndTime";
            this.colInspectionEndTime.Name = "colInspectionEndTime";
            this.colInspectionEndTime.OptionsColumn.AllowEdit = false;
            this.colInspectionEndTime.OptionsColumn.AllowFocus = false;
            this.colInspectionEndTime.OptionsColumn.ReadOnly = true;
            this.colInspectionEndTime.Visible = true;
            this.colInspectionEndTime.VisibleIndex = 3;
            this.colInspectionEndTime.Width = 109;
            // 
            // colTotalTimeDeductions
            // 
            this.colTotalTimeDeductions.Caption = "Total Time Deductions";
            this.colTotalTimeDeductions.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTotalTimeDeductions.FieldName = "TotalTimeDeductions";
            this.colTotalTimeDeductions.Name = "colTotalTimeDeductions";
            this.colTotalTimeDeductions.OptionsColumn.AllowEdit = false;
            this.colTotalTimeDeductions.OptionsColumn.AllowFocus = false;
            this.colTotalTimeDeductions.OptionsColumn.ReadOnly = true;
            this.colTotalTimeDeductions.Visible = true;
            this.colTotalTimeDeductions.VisibleIndex = 4;
            this.colTotalTimeDeductions.Width = 131;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "######0.00 Hours";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colTotalTimeDeductionsRemarks
            // 
            this.colTotalTimeDeductionsRemarks.Caption = "Time Deduction Remarks";
            this.colTotalTimeDeductionsRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colTotalTimeDeductionsRemarks.FieldName = "TotalTimeDeductionsRemarks";
            this.colTotalTimeDeductionsRemarks.Name = "colTotalTimeDeductionsRemarks";
            this.colTotalTimeDeductionsRemarks.OptionsColumn.ReadOnly = true;
            this.colTotalTimeDeductionsRemarks.Visible = true;
            this.colTotalTimeDeductionsRemarks.VisibleIndex = 5;
            this.colTotalTimeDeductionsRemarks.Width = 151;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colTotalTimeTaken
            // 
            this.colTotalTimeTaken.Caption = "Total Time Taken";
            this.colTotalTimeTaken.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTotalTimeTaken.FieldName = "TotalTimeTaken";
            this.colTotalTimeTaken.Name = "colTotalTimeTaken";
            this.colTotalTimeTaken.OptionsColumn.AllowEdit = false;
            this.colTotalTimeTaken.OptionsColumn.AllowFocus = false;
            this.colTotalTimeTaken.OptionsColumn.ReadOnly = true;
            this.colTotalTimeTaken.Visible = true;
            this.colTotalTimeTaken.VisibleIndex = 6;
            this.colTotalTimeTaken.Width = 106;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 7;
            this.colRemarks2.Width = 132;
            // 
            // gridLookUpEdit1
            // 
            this.gridLookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "MappingWorkspaceID", true));
            this.gridLookUpEdit1.Location = new System.Drawing.Point(117, 615);
            this.gridLookUpEdit1.MenuManager = this.barManager1;
            this.gridLookUpEdit1.Name = "gridLookUpEdit1";
            this.gridLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEdit1.Properties.DataSource = this.sp03063EPMappingWorkspacesWithBlankBindingSource;
            this.gridLookUpEdit1.Properties.DisplayMember = "WorkspaceName";
            this.gridLookUpEdit1.Properties.NullText = "";
            this.gridLookUpEdit1.Properties.PopupView = this.gridView1;
            this.gridLookUpEdit1.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridLookUpEdit1.Properties.ValueMember = "WorkspaceID";
            this.gridLookUpEdit1.Size = new System.Drawing.Size(799, 20);
            this.gridLookUpEdit1.StyleController = this.dataLayoutControl1;
            this.gridLookUpEdit1.TabIndex = 29;
            // 
            // sp03063EPMappingWorkspacesWithBlankBindingSource
            // 
            this.sp03063EPMappingWorkspacesWithBlankBindingSource.DataMember = "sp03063_EP_Mapping_Workspaces_With_Blank";
            this.sp03063EPMappingWorkspacesWithBlankBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCreatedByID,
            this.colCreatedByName,
            this.colRemarks1,
            this.colWorkspaceID,
            this.colWorkspaceName});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colWorkspaceID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCreatedByName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCreatedByID
            // 
            this.colCreatedByID.Caption = "Created By ID";
            this.colCreatedByID.FieldName = "CreatedByID";
            this.colCreatedByID.Name = "colCreatedByID";
            this.colCreatedByID.OptionsColumn.AllowEdit = false;
            this.colCreatedByID.OptionsColumn.AllowFocus = false;
            this.colCreatedByID.OptionsColumn.ReadOnly = true;
            this.colCreatedByID.Width = 130;
            // 
            // colCreatedByName
            // 
            this.colCreatedByName.Caption = "Created By";
            this.colCreatedByName.FieldName = "CreatedByName";
            this.colCreatedByName.Name = "colCreatedByName";
            this.colCreatedByName.OptionsColumn.AllowEdit = false;
            this.colCreatedByName.OptionsColumn.AllowFocus = false;
            this.colCreatedByName.OptionsColumn.ReadOnly = true;
            this.colCreatedByName.Width = 113;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 1;
            this.colRemarks1.Width = 119;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colWorkspaceName
            // 
            this.colWorkspaceName.Caption = "Workspace Name";
            this.colWorkspaceName.FieldName = "WorkspaceName";
            this.colWorkspaceName.Name = "colWorkspaceName";
            this.colWorkspaceName.OptionsColumn.AllowEdit = false;
            this.colWorkspaceName.OptionsColumn.AllowFocus = false;
            this.colWorkspaceName.OptionsColumn.ReadOnly = true;
            this.colWorkspaceName.Visible = true;
            this.colWorkspaceName.VisibleIndex = 0;
            this.colWorkspaceName.Width = 252;
            // 
            // SiteTypeIDGridLookUpEdit
            // 
            this.SiteTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteTypeID", true));
            this.SiteTypeIDGridLookUpEdit.Location = new System.Drawing.Point(117, 87);
            this.SiteTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SiteTypeIDGridLookUpEdit.Name = "SiteTypeIDGridLookUpEdit";
            editorButtonImageOptions5.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions6.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.SiteTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteTypeIDGridLookUpEdit.Properties.DataSource = this.sp00227PicklistListWithBlankBindingSource;
            this.SiteTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SiteTypeIDGridLookUpEdit.Properties.NullText = "";
            this.SiteTypeIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit2View;
            this.SiteTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SiteTypeIDGridLookUpEdit.Size = new System.Drawing.Size(799, 22);
            this.SiteTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SiteTypeIDGridLookUpEdit.TabIndex = 28;
            this.SiteTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteTypeIDGridLookUpEdit_ButtonClick);
            // 
            // sp00227PicklistListWithBlankBindingSource
            // 
            this.sp00227PicklistListWithBlankBindingSource.DataMember = "sp00227_Picklist_List_With_Blank";
            this.sp00227PicklistListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit2View
            // 
            this.gridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colintOrder});
            this.gridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit2View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit2View.Name = "gridLookUpEdit2View";
            this.gridLookUpEdit2View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colintOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 303;
            // 
            // colintOrder
            // 
            this.colintOrder.Caption = "Order";
            this.colintOrder.FieldName = "intOrder";
            this.colintOrder.Name = "colintOrder";
            this.colintOrder.OptionsColumn.AllowEdit = false;
            this.colintOrder.OptionsColumn.AllowFocus = false;
            this.colintOrder.OptionsColumn.ReadOnly = true;
            this.colintOrder.Width = 149;
            // 
            // ClientIDGridLookUpEdit
            // 
            this.ClientIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "ClientID", true));
            this.ClientIDGridLookUpEdit.Location = new System.Drawing.Point(117, 35);
            this.ClientIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ClientIDGridLookUpEdit.Name = "ClientIDGridLookUpEdit";
            editorButtonImageOptions7.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions8.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.ClientIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientIDGridLookUpEdit.Properties.DataSource = this.sp03009EPClientListWithBlankBindingSource;
            this.ClientIDGridLookUpEdit.Properties.DisplayMember = "ClientName";
            this.ClientIDGridLookUpEdit.Properties.NullText = "";
            this.ClientIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.ClientIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.ClientIDGridLookUpEdit.Properties.ValueMember = "ClientID";
            this.ClientIDGridLookUpEdit.Size = new System.Drawing.Size(799, 22);
            this.ClientIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDGridLookUpEdit.TabIndex = 27;
            this.ClientIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientIDGridLookUpEdit_ButtonClick);
            this.ClientIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientIDGridLookUpEdit_Validating);
            // 
            // sp03009EPClientListWithBlankBindingSource
            // 
            this.sp03009EPClientListWithBlankBindingSource.DataMember = "sp03009_EP_Client_List_With_Blank";
            this.sp03009EPClientListWithBlankBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientCode,
            this.colClientID,
            this.colClientName,
            this.colClientTypeDescription,
            this.colClientTypeID,
            this.colRemarks});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colClientID;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Visible = true;
            this.colClientCode.VisibleIndex = 1;
            this.colClientCode.Width = 108;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 271;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 2;
            this.colClientTypeDescription.Width = 132;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            this.colClientTypeID.Width = 101;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            this.colRemarks.Width = 67;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp03007EPSiteEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(117, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 26;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteName", true));
            this.SiteNameTextEdit.Location = new System.Drawing.Point(117, 113);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(799, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling7);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 7;
            this.SiteNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteNameTextEdit_Validating);
            // 
            // SiteCodeTextEdit
            // 
            this.SiteCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteCode", true));
            this.SiteCodeTextEdit.Location = new System.Drawing.Point(117, 137);
            this.SiteCodeTextEdit.MenuManager = this.barManager1;
            this.SiteCodeTextEdit.Name = "SiteCodeTextEdit";
            this.SiteCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteCodeTextEdit, true);
            this.SiteCodeTextEdit.Size = new System.Drawing.Size(799, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteCodeTextEdit, optionsSpelling8);
            this.SiteCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteCodeTextEdit.TabIndex = 8;
            // 
            // ContactPersonTextEdit
            // 
            this.ContactPersonTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "ContactPerson", true));
            this.ContactPersonTextEdit.Location = new System.Drawing.Point(141, 313);
            this.ContactPersonTextEdit.MenuManager = this.barManager1;
            this.ContactPersonTextEdit.Name = "ContactPersonTextEdit";
            this.ContactPersonTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactPersonTextEdit, true);
            this.ContactPersonTextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactPersonTextEdit, optionsSpelling9);
            this.ContactPersonTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactPersonTextEdit.TabIndex = 9;
            // 
            // ContactPersonPositionTextEdit
            // 
            this.ContactPersonPositionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "ContactPersonPosition", true));
            this.ContactPersonPositionTextEdit.Location = new System.Drawing.Point(141, 337);
            this.ContactPersonPositionTextEdit.MenuManager = this.barManager1;
            this.ContactPersonPositionTextEdit.Name = "ContactPersonPositionTextEdit";
            this.ContactPersonPositionTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactPersonPositionTextEdit, true);
            this.ContactPersonPositionTextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactPersonPositionTextEdit, optionsSpelling10);
            this.ContactPersonPositionTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactPersonPositionTextEdit.TabIndex = 10;
            // 
            // SiteAddressLine1TextEdit
            // 
            this.SiteAddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteAddressLine1", true));
            this.SiteAddressLine1TextEdit.Location = new System.Drawing.Point(141, 313);
            this.SiteAddressLine1TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine1TextEdit.Name = "SiteAddressLine1TextEdit";
            this.SiteAddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine1TextEdit, true);
            this.SiteAddressLine1TextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine1TextEdit, optionsSpelling11);
            this.SiteAddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine1TextEdit.TabIndex = 11;
            // 
            // SiteAddressLine2TextEdit
            // 
            this.SiteAddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteAddressLine2", true));
            this.SiteAddressLine2TextEdit.Location = new System.Drawing.Point(141, 337);
            this.SiteAddressLine2TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine2TextEdit.Name = "SiteAddressLine2TextEdit";
            this.SiteAddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine2TextEdit, true);
            this.SiteAddressLine2TextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine2TextEdit, optionsSpelling12);
            this.SiteAddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine2TextEdit.TabIndex = 12;
            // 
            // SiteAddressLine3TextEdit
            // 
            this.SiteAddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteAddressLine3", true));
            this.SiteAddressLine3TextEdit.Location = new System.Drawing.Point(141, 361);
            this.SiteAddressLine3TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine3TextEdit.Name = "SiteAddressLine3TextEdit";
            this.SiteAddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine3TextEdit, true);
            this.SiteAddressLine3TextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine3TextEdit, optionsSpelling13);
            this.SiteAddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine3TextEdit.TabIndex = 13;
            // 
            // SiteAddressLine4TextEdit
            // 
            this.SiteAddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteAddressLine4", true));
            this.SiteAddressLine4TextEdit.Location = new System.Drawing.Point(141, 385);
            this.SiteAddressLine4TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine4TextEdit.Name = "SiteAddressLine4TextEdit";
            this.SiteAddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine4TextEdit, true);
            this.SiteAddressLine4TextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine4TextEdit, optionsSpelling14);
            this.SiteAddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine4TextEdit.TabIndex = 14;
            // 
            // SiteAddressLine5TextEdit
            // 
            this.SiteAddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteAddressLine5", true));
            this.SiteAddressLine5TextEdit.Location = new System.Drawing.Point(141, 409);
            this.SiteAddressLine5TextEdit.MenuManager = this.barManager1;
            this.SiteAddressLine5TextEdit.Name = "SiteAddressLine5TextEdit";
            this.SiteAddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressLine5TextEdit, true);
            this.SiteAddressLine5TextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressLine5TextEdit, optionsSpelling15);
            this.SiteAddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressLine5TextEdit.TabIndex = 15;
            // 
            // SiteTelephoneTextEdit
            // 
            this.SiteTelephoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteTelephone", true));
            this.SiteTelephoneTextEdit.Location = new System.Drawing.Point(141, 361);
            this.SiteTelephoneTextEdit.MenuManager = this.barManager1;
            this.SiteTelephoneTextEdit.Name = "SiteTelephoneTextEdit";
            this.SiteTelephoneTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteTelephoneTextEdit, true);
            this.SiteTelephoneTextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteTelephoneTextEdit, optionsSpelling16);
            this.SiteTelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteTelephoneTextEdit.TabIndex = 17;
            // 
            // SiteMobileTextEdit
            // 
            this.SiteMobileTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteMobile", true));
            this.SiteMobileTextEdit.Location = new System.Drawing.Point(141, 385);
            this.SiteMobileTextEdit.MenuManager = this.barManager1;
            this.SiteMobileTextEdit.Name = "SiteMobileTextEdit";
            this.SiteMobileTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteMobileTextEdit, true);
            this.SiteMobileTextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteMobileTextEdit, optionsSpelling17);
            this.SiteMobileTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteMobileTextEdit.TabIndex = 18;
            // 
            // SiteFaxTextEdit
            // 
            this.SiteFaxTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteFax", true));
            this.SiteFaxTextEdit.Location = new System.Drawing.Point(141, 409);
            this.SiteFaxTextEdit.MenuManager = this.barManager1;
            this.SiteFaxTextEdit.Name = "SiteFaxTextEdit";
            this.SiteFaxTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteFaxTextEdit, true);
            this.SiteFaxTextEdit.Size = new System.Drawing.Size(751, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteFaxTextEdit, optionsSpelling18);
            this.SiteFaxTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteFaxTextEdit.TabIndex = 19;
            // 
            // XCoordinateSpinEdit
            // 
            this.XCoordinateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "XCoordinate", true));
            this.XCoordinateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.XCoordinateSpinEdit.Location = new System.Drawing.Point(117, 639);
            this.XCoordinateSpinEdit.MenuManager = this.barManager1;
            this.XCoordinateSpinEdit.Name = "XCoordinateSpinEdit";
            this.XCoordinateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.XCoordinateSpinEdit.Properties.ReadOnly = true;
            this.XCoordinateSpinEdit.Size = new System.Drawing.Size(186, 20);
            this.XCoordinateSpinEdit.StyleController = this.dataLayoutControl1;
            this.XCoordinateSpinEdit.TabIndex = 21;
            // 
            // YCoordinateSpinEdit
            // 
            this.YCoordinateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "YCoordinate", true));
            this.YCoordinateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.YCoordinateSpinEdit.Location = new System.Drawing.Point(117, 663);
            this.YCoordinateSpinEdit.MenuManager = this.barManager1;
            this.YCoordinateSpinEdit.Name = "YCoordinateSpinEdit";
            this.YCoordinateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.YCoordinateSpinEdit.Properties.ReadOnly = true;
            this.YCoordinateSpinEdit.Size = new System.Drawing.Size(186, 20);
            this.YCoordinateSpinEdit.StyleController = this.dataLayoutControl1;
            this.YCoordinateSpinEdit.TabIndex = 22;
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteID", true));
            this.SiteIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SiteIDTextEdit.Location = new System.Drawing.Point(130, 59);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.SiteIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(469, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling19);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 5;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 313);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(856, 264);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling20);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 23;
            // 
            // SiteEmailMemoEdit
            // 
            this.SiteEmailMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SiteEmail", true));
            this.SiteEmailMemoEdit.Location = new System.Drawing.Point(141, 433);
            this.SiteEmailMemoEdit.MenuManager = this.barManager1;
            this.SiteEmailMemoEdit.Name = "SiteEmailMemoEdit";
            this.SiteEmailMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteEmailMemoEdit, true);
            this.SiteEmailMemoEdit.Size = new System.Drawing.Size(751, 65);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteEmailMemoEdit, optionsSpelling21);
            this.SiteEmailMemoEdit.StyleController = this.dataLayoutControl1;
            this.SiteEmailMemoEdit.TabIndex = 20;
            this.SiteEmailMemoEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteEmailMemoEdit_Validating);
            // 
            // SitePostcodeTextEdit
            // 
            this.SitePostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp03007EPSiteEditBindingSource, "SitePostcode", true));
            this.SitePostcodeTextEdit.Location = new System.Drawing.Point(141, 433);
            this.SitePostcodeTextEdit.MenuManager = this.barManager1;
            this.SitePostcodeTextEdit.Name = "SitePostcodeTextEdit";
            this.SitePostcodeTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Lookup Lat \\ Long", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "", "lookup", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SitePostcodeTextEdit.Properties.MaxLength = 20;
            this.SitePostcodeTextEdit.Size = new System.Drawing.Size(253, 20);
            this.SitePostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.SitePostcodeTextEdit.TabIndex = 16;
            this.SitePostcodeTextEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SitePostcodeTextEdit_ButtonClick);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.CustomizationFormText = "Site ID:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 47);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(591, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(928, 793);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteName,
            this.ItemForSiteCode,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlGroup4,
            this.emptySpaceItem4,
            this.layoutControlItem7,
            this.ItemForSiteOrder,
            this.ItemForClientsSiteCode,
            this.emptySpaceItem13,
            this.ItemForClientsSiteID});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(908, 593);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.AllowHide = false;
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 101);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(908, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteCode
            // 
            this.ItemForSiteCode.Control = this.SiteCodeTextEdit;
            this.ItemForSiteCode.CustomizationFormText = "Site Code:";
            this.ItemForSiteCode.Location = new System.Drawing.Point(0, 125);
            this.ItemForSiteCode.Name = "ItemForSiteCode";
            this.ItemForSiteCode.Size = new System.Drawing.Size(908, 24);
            this.ItemForSiteCode.Text = "Site Code:";
            this.ItemForSiteCode.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(105, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(105, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(105, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(105, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(305, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(603, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHide = false;
            this.layoutControlItem2.Control = this.ClientIDGridLookUpEdit;
            this.layoutControlItem2.CustomizationFormText = "Client:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(908, 26);
            this.layoutControlItem2.Text = "Client:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AllowHide = false;
            this.layoutControlItem3.Control = this.SiteTypeIDGridLookUpEdit;
            this.layoutControlItem3.CustomizationFormText = "Site Type:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 75);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(908, 26);
            this.layoutControlItem3.Text = "Site Type:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 231);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(908, 362);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup1.SelectedTabPageIndex = 2;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(884, 316);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup10,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup8,
            this.layoutControlGroup9,
            this.layoutControlGroup11,
            this.layoutControlGroup7});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup6.CaptionImageOptions.Image")));
            this.layoutControlGroup6.CustomizationFormText = "Contact";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForContactPerson,
            this.ItemForSiteTelephone,
            this.ItemForSiteMobile,
            this.ItemForSiteFax,
            this.emptySpaceItem7,
            this.ItemForSiteText,
            this.ItemForSiteEmail,
            this.ItemForContactPersonPosition});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(860, 268);
            this.layoutControlGroup6.Text = "Contact";
            // 
            // ItemForContactPerson
            // 
            this.ItemForContactPerson.Control = this.ContactPersonTextEdit;
            this.ItemForContactPerson.CustomizationFormText = "Contact Person:";
            this.ItemForContactPerson.Location = new System.Drawing.Point(0, 0);
            this.ItemForContactPerson.Name = "ItemForContactPerson";
            this.ItemForContactPerson.Size = new System.Drawing.Size(860, 24);
            this.ItemForContactPerson.Text = "Contact Person:";
            this.ItemForContactPerson.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteTelephone
            // 
            this.ItemForSiteTelephone.Control = this.SiteTelephoneTextEdit;
            this.ItemForSiteTelephone.CustomizationFormText = "Site Telephone:";
            this.ItemForSiteTelephone.Location = new System.Drawing.Point(0, 48);
            this.ItemForSiteTelephone.Name = "ItemForSiteTelephone";
            this.ItemForSiteTelephone.Size = new System.Drawing.Size(860, 24);
            this.ItemForSiteTelephone.Text = "Site Telephone:";
            this.ItemForSiteTelephone.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteMobile
            // 
            this.ItemForSiteMobile.Control = this.SiteMobileTextEdit;
            this.ItemForSiteMobile.CustomizationFormText = "Site Mobile:";
            this.ItemForSiteMobile.Location = new System.Drawing.Point(0, 72);
            this.ItemForSiteMobile.Name = "ItemForSiteMobile";
            this.ItemForSiteMobile.Size = new System.Drawing.Size(860, 24);
            this.ItemForSiteMobile.Text = "Site Mobile:";
            this.ItemForSiteMobile.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteFax
            // 
            this.ItemForSiteFax.Control = this.SiteFaxTextEdit;
            this.ItemForSiteFax.CustomizationFormText = "Site Fax:";
            this.ItemForSiteFax.Location = new System.Drawing.Point(0, 96);
            this.ItemForSiteFax.Name = "ItemForSiteFax";
            this.ItemForSiteFax.Size = new System.Drawing.Size(860, 24);
            this.ItemForSiteFax.Text = "Site Fax:";
            this.ItemForSiteFax.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 258);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(860, 10);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSiteText
            // 
            this.ItemForSiteText.Control = this.SiteTextMemoEdit;
            this.ItemForSiteText.CustomizationFormText = "Site Text:";
            this.ItemForSiteText.Location = new System.Drawing.Point(0, 189);
            this.ItemForSiteText.MaxSize = new System.Drawing.Size(0, 69);
            this.ItemForSiteText.MinSize = new System.Drawing.Size(118, 69);
            this.ItemForSiteText.Name = "ItemForSiteText";
            this.ItemForSiteText.Size = new System.Drawing.Size(860, 69);
            this.ItemForSiteText.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSiteText.Text = "Site Text:";
            this.ItemForSiteText.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteEmail
            // 
            this.ItemForSiteEmail.Control = this.SiteEmailMemoEdit;
            this.ItemForSiteEmail.CustomizationFormText = "Site Email:";
            this.ItemForSiteEmail.Location = new System.Drawing.Point(0, 120);
            this.ItemForSiteEmail.MaxSize = new System.Drawing.Size(0, 69);
            this.ItemForSiteEmail.MinSize = new System.Drawing.Size(118, 69);
            this.ItemForSiteEmail.Name = "ItemForSiteEmail";
            this.ItemForSiteEmail.Size = new System.Drawing.Size(860, 69);
            this.ItemForSiteEmail.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSiteEmail.Text = "Site Email:";
            this.ItemForSiteEmail.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForContactPersonPosition
            // 
            this.ItemForContactPersonPosition.Control = this.ContactPersonPositionTextEdit;
            this.ItemForContactPersonPosition.CustomizationFormText = "Person Position:";
            this.ItemForContactPersonPosition.Location = new System.Drawing.Point(0, 24);
            this.ItemForContactPersonPosition.Name = "ItemForContactPersonPosition";
            this.ItemForContactPersonPosition.Size = new System.Drawing.Size(860, 24);
            this.ItemForContactPersonPosition.Text = "Person Position:";
            this.ItemForContactPersonPosition.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Gritting";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForHubID,
            this.ItemForGrittingSiteCheckEdit,
            this.emptySpaceItem3,
            this.emptySpaceItem11,
            this.ItemForIsSnowClearanceSite,
            this.ItemForSiteImageFile});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(860, 268);
            this.layoutControlGroup10.Text = "Winter Maintenance";
            // 
            // ItemForHubID
            // 
            this.ItemForHubID.Control = this.HubIDTextEdit;
            this.ItemForHubID.CustomizationFormText = "Hub ID:";
            this.ItemForHubID.Location = new System.Drawing.Point(0, 0);
            this.ItemForHubID.Name = "ItemForHubID";
            this.ItemForHubID.Size = new System.Drawing.Size(860, 24);
            this.ItemForHubID.Text = "Hub ID:";
            this.ItemForHubID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForGrittingSiteCheckEdit
            // 
            this.ItemForGrittingSiteCheckEdit.Control = this.GrittingSiteCheckEdit;
            this.ItemForGrittingSiteCheckEdit.CustomizationFormText = "Gritting Site:";
            this.ItemForGrittingSiteCheckEdit.Location = new System.Drawing.Point(0, 24);
            this.ItemForGrittingSiteCheckEdit.MaxSize = new System.Drawing.Size(196, 23);
            this.ItemForGrittingSiteCheckEdit.MinSize = new System.Drawing.Size(196, 23);
            this.ItemForGrittingSiteCheckEdit.Name = "ItemForGrittingSiteCheckEdit";
            this.ItemForGrittingSiteCheckEdit.Size = new System.Drawing.Size(196, 23);
            this.ItemForGrittingSiteCheckEdit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGrittingSiteCheckEdit.Text = "Gritting Site:";
            this.ItemForGrittingSiteCheckEdit.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 94);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(860, 174);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(196, 24);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(664, 46);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIsSnowClearanceSite
            // 
            this.ItemForIsSnowClearanceSite.Control = this.IsSnowClearanceSiteCheckEdit;
            this.ItemForIsSnowClearanceSite.CustomizationFormText = "Snow Clearance Site:";
            this.ItemForIsSnowClearanceSite.Location = new System.Drawing.Point(0, 47);
            this.ItemForIsSnowClearanceSite.Name = "ItemForIsSnowClearanceSite";
            this.ItemForIsSnowClearanceSite.Size = new System.Drawing.Size(196, 23);
            this.ItemForIsSnowClearanceSite.Text = "Snow Clearance Site:";
            this.ItemForIsSnowClearanceSite.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteImageFile
            // 
            this.ItemForSiteImageFile.Control = this.SiteImageFileButtonEdit;
            this.ItemForSiteImageFile.Location = new System.Drawing.Point(0, 70);
            this.ItemForSiteImageFile.Name = "ItemForSiteImageFile";
            this.ItemForSiteImageFile.Size = new System.Drawing.Size(860, 24);
            this.ItemForSiteImageFile.Text = "App Site Drawing:";
            this.ItemForSiteImageFile.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImageOptions.Image")));
            this.layoutControlGroup5.CustomizationFormText = "Address";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteAddressLine1,
            this.ItemForSiteAddressLine2,
            this.ItemForSiteAddressLine3,
            this.ItemForSiteAddressLine4,
            this.ItemForSiteAddressLine5,
            this.ItemForSitePostcode,
            this.ItemForCOAddress,
            this.emptySpaceItem6,
            this.emptySpaceItem9,
            this.emptySpaceItem14});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(860, 268);
            this.layoutControlGroup5.Text = "Address";
            // 
            // ItemForSiteAddressLine1
            // 
            this.ItemForSiteAddressLine1.Control = this.SiteAddressLine1TextEdit;
            this.ItemForSiteAddressLine1.CustomizationFormText = "Address Line 1:";
            this.ItemForSiteAddressLine1.Location = new System.Drawing.Point(0, 0);
            this.ItemForSiteAddressLine1.Name = "ItemForSiteAddressLine1";
            this.ItemForSiteAddressLine1.Size = new System.Drawing.Size(860, 24);
            this.ItemForSiteAddressLine1.Text = "Address Line 1:";
            this.ItemForSiteAddressLine1.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteAddressLine2
            // 
            this.ItemForSiteAddressLine2.Control = this.SiteAddressLine2TextEdit;
            this.ItemForSiteAddressLine2.CustomizationFormText = "Address Line 2:";
            this.ItemForSiteAddressLine2.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteAddressLine2.Name = "ItemForSiteAddressLine2";
            this.ItemForSiteAddressLine2.Size = new System.Drawing.Size(860, 24);
            this.ItemForSiteAddressLine2.Text = "Address Line 2:";
            this.ItemForSiteAddressLine2.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteAddressLine3
            // 
            this.ItemForSiteAddressLine3.Control = this.SiteAddressLine3TextEdit;
            this.ItemForSiteAddressLine3.CustomizationFormText = "Address Line 3:";
            this.ItemForSiteAddressLine3.Location = new System.Drawing.Point(0, 48);
            this.ItemForSiteAddressLine3.Name = "ItemForSiteAddressLine3";
            this.ItemForSiteAddressLine3.Size = new System.Drawing.Size(860, 24);
            this.ItemForSiteAddressLine3.Text = "Address Line 3:";
            this.ItemForSiteAddressLine3.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteAddressLine4
            // 
            this.ItemForSiteAddressLine4.Control = this.SiteAddressLine4TextEdit;
            this.ItemForSiteAddressLine4.CustomizationFormText = "Address Line 4:";
            this.ItemForSiteAddressLine4.Location = new System.Drawing.Point(0, 72);
            this.ItemForSiteAddressLine4.Name = "ItemForSiteAddressLine4";
            this.ItemForSiteAddressLine4.Size = new System.Drawing.Size(860, 24);
            this.ItemForSiteAddressLine4.Text = "Address Line 4:";
            this.ItemForSiteAddressLine4.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteAddressLine5
            // 
            this.ItemForSiteAddressLine5.Control = this.SiteAddressLine5TextEdit;
            this.ItemForSiteAddressLine5.CustomizationFormText = "Address Line 5:";
            this.ItemForSiteAddressLine5.Location = new System.Drawing.Point(0, 96);
            this.ItemForSiteAddressLine5.Name = "ItemForSiteAddressLine5";
            this.ItemForSiteAddressLine5.Size = new System.Drawing.Size(860, 24);
            this.ItemForSiteAddressLine5.Text = "Address Line 5:";
            this.ItemForSiteAddressLine5.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSitePostcode
            // 
            this.ItemForSitePostcode.Control = this.SitePostcodeTextEdit;
            this.ItemForSitePostcode.CustomizationFormText = "Postcode:";
            this.ItemForSitePostcode.Location = new System.Drawing.Point(0, 120);
            this.ItemForSitePostcode.MaxSize = new System.Drawing.Size(362, 24);
            this.ItemForSitePostcode.MinSize = new System.Drawing.Size(362, 24);
            this.ItemForSitePostcode.Name = "ItemForSitePostcode";
            this.ItemForSitePostcode.Size = new System.Drawing.Size(362, 24);
            this.ItemForSitePostcode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSitePostcode.Text = "Postcode:";
            this.ItemForSitePostcode.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForCOAddress
            // 
            this.ItemForCOAddress.Control = this.COAddressMemoEdit;
            this.ItemForCOAddress.Location = new System.Drawing.Point(0, 154);
            this.ItemForCOAddress.MaxSize = new System.Drawing.Size(0, 95);
            this.ItemForCOAddress.MinSize = new System.Drawing.Size(119, 95);
            this.ItemForCOAddress.Name = "ItemForCOAddress";
            this.ItemForCOAddress.Size = new System.Drawing.Size(860, 95);
            this.ItemForCOAddress.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCOAddress.Text = "C\\O Address:";
            this.ItemForCOAddress.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 249);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(860, 19);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 144);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(860, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(362, 120);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(498, 24);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Linked Site Inspections";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(860, 268);
            this.layoutControlGroup8.Text = "Linked Site Inspections";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl1;
            this.layoutControlItem5.CustomizationFormText = "Linked Site Inspection Grid:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(860, 268);
            this.layoutControlItem5.Text = "Linked Site Inspection Grid:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Linked Assets";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(860, 268);
            this.layoutControlGroup9.Text = "Linked Assets";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridControl2;
            this.layoutControlItem6.CustomizationFormText = "Linked Assets Grid:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(860, 268);
            this.layoutControlItem6.Text = "Linked Assets Grid:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteKMLData,
            this.ItemForViewKML,
            this.emptySpaceItem15});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(860, 268);
            this.layoutControlGroup11.Text = "Site KML";
            // 
            // ItemForSiteKMLData
            // 
            this.ItemForSiteKMLData.Control = this.SiteKMLDataMemoEdit;
            this.ItemForSiteKMLData.Location = new System.Drawing.Point(0, 26);
            this.ItemForSiteKMLData.Name = "ItemForSiteKMLData";
            this.ItemForSiteKMLData.Size = new System.Drawing.Size(860, 242);
            this.ItemForSiteKMLData.Text = "Site KML:";
            this.ItemForSiteKMLData.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForSiteKMLData.TextVisible = false;
            // 
            // ItemForViewKML
            // 
            this.ItemForViewKML.Control = this.btnViewKML;
            this.ItemForViewKML.Location = new System.Drawing.Point(746, 0);
            this.ItemForViewKML.MaxSize = new System.Drawing.Size(114, 26);
            this.ItemForViewKML.MinSize = new System.Drawing.Size(114, 26);
            this.ItemForViewKML.Name = "ItemForViewKML";
            this.ItemForViewKML.Size = new System.Drawing.Size(114, 26);
            this.ItemForViewKML.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForViewKML.Text = "View KML File:";
            this.ItemForViewKML.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForViewKML.TextVisible = false;
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(746, 26);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup7.CaptionImageOptions.Image")));
            this.layoutControlGroup7.CustomizationFormText = "Remarks";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(860, 268);
            this.layoutControlGroup7.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(860, 268);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 221);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(908, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.CompanyIDGridLookUpEdit;
            this.layoutControlItem7.CustomizationFormText = "Company:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(908, 26);
            this.layoutControlItem7.Text = "Company:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteOrder
            // 
            this.ItemForSiteOrder.Control = this.SiteOrderSpinEdit;
            this.ItemForSiteOrder.CustomizationFormText = "Site Order:";
            this.ItemForSiteOrder.Location = new System.Drawing.Point(0, 149);
            this.ItemForSiteOrder.Name = "ItemForSiteOrder";
            this.ItemForSiteOrder.Size = new System.Drawing.Size(908, 24);
            this.ItemForSiteOrder.Text = "Site Order:";
            this.ItemForSiteOrder.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForClientsSiteCode
            // 
            this.ItemForClientsSiteCode.Control = this.ClientsSiteCodeTextEdit;
            this.ItemForClientsSiteCode.CustomizationFormText = "Clients Site Code:";
            this.ItemForClientsSiteCode.Location = new System.Drawing.Point(0, 173);
            this.ItemForClientsSiteCode.MaxSize = new System.Drawing.Size(295, 24);
            this.ItemForClientsSiteCode.MinSize = new System.Drawing.Size(295, 24);
            this.ItemForClientsSiteCode.Name = "ItemForClientsSiteCode";
            this.ItemForClientsSiteCode.Size = new System.Drawing.Size(295, 24);
            this.ItemForClientsSiteCode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientsSiteCode.Text = "Clients Site Code:";
            this.ItemForClientsSiteCode.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(295, 173);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(613, 48);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientsSiteID
            // 
            this.ItemForClientsSiteID.Control = this.ClientsSiteIDTextEdit;
            this.ItemForClientsSiteID.CustomizationFormText = "Cients Site ID:";
            this.ItemForClientsSiteID.Location = new System.Drawing.Point(0, 197);
            this.ItemForClientsSiteID.Name = "ItemForClientsSiteID";
            this.ItemForClientsSiteID.Size = new System.Drawing.Size(295, 24);
            this.ItemForClientsSiteID.Text = "Cients Site ID:";
            this.ItemForClientsSiteID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForXCoordinate,
            this.emptySpaceItem5,
            this.layoutControlItem4,
            this.layoutControlItem10,
            this.emptySpaceItem8,
            this.emptySpaceItem12,
            this.ItemForYCoordinate,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.ItemForGeoFenceDistance,
            this.emptySpaceItem10});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 593);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(908, 180);
            // 
            // ItemForXCoordinate
            // 
            this.ItemForXCoordinate.Control = this.XCoordinateSpinEdit;
            this.ItemForXCoordinate.CustomizationFormText = "X Coordinate:";
            this.ItemForXCoordinate.Location = new System.Drawing.Point(0, 34);
            this.ItemForXCoordinate.MaxSize = new System.Drawing.Size(295, 24);
            this.ItemForXCoordinate.MinSize = new System.Drawing.Size(295, 24);
            this.ItemForXCoordinate.Name = "ItemForXCoordinate";
            this.ItemForXCoordinate.Size = new System.Drawing.Size(295, 24);
            this.ItemForXCoordinate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForXCoordinate.Text = "X Coordinate:";
            this.ItemForXCoordinate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(908, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridLookUpEdit1;
            this.layoutControlItem4.CustomizationFormText = "Mapping Workspace:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 10);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(908, 24);
            this.layoutControlItem4.Text = "Mapping Workspace:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnViewOnMap;
            this.layoutControlItem10.CustomizationFormText = "View on Map Button";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 154);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(108, 26);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(108, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(108, 26);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "View on Map Button";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(108, 154);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(800, 26);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(295, 34);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(613, 96);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForYCoordinate
            // 
            this.ItemForYCoordinate.Control = this.YCoordinateSpinEdit;
            this.ItemForYCoordinate.CustomizationFormText = "Y Coordinate:";
            this.ItemForYCoordinate.Location = new System.Drawing.Point(0, 58);
            this.ItemForYCoordinate.Name = "ItemForYCoordinate";
            this.ItemForYCoordinate.Size = new System.Drawing.Size(295, 24);
            this.ItemForYCoordinate.Text = "Y Coordinate:";
            this.ItemForYCoordinate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.LocationXSpinEdit;
            this.layoutControlItem8.CustomizationFormText = "Latitude:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 82);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem8.Text = "Latitude:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.LocationYSpinEdit;
            this.layoutControlItem9.CustomizationFormText = "Longitude";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 106);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem9.Text = "Longitude";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForGeoFenceDistance
            // 
            this.ItemForGeoFenceDistance.Control = this.GeoFenceDistanceSpinEdit;
            this.ItemForGeoFenceDistance.Location = new System.Drawing.Point(0, 130);
            this.ItemForGeoFenceDistance.MaxSize = new System.Drawing.Size(222, 24);
            this.ItemForGeoFenceDistance.MinSize = new System.Drawing.Size(222, 24);
            this.ItemForGeoFenceDistance.Name = "ItemForGeoFenceDistance";
            this.ItemForGeoFenceDistance.Size = new System.Drawing.Size(222, 24);
            this.ItemForGeoFenceDistance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGeoFenceDistance.Text = "Geo Fence Distance:";
            this.ItemForGeoFenceDistance.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(222, 130);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(686, 24);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp03007_EP_Site_EditTableAdapter
            // 
            this.sp03007_EP_Site_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp00227_Picklist_List_With_BlankTableAdapter
            // 
            this.sp00227_Picklist_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp03009_EP_Client_List_With_BlankTableAdapter
            // 
            this.sp03009_EP_Client_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp03063_EP_Mapping_Workspaces_With_BlankTableAdapter
            // 
            this.sp03063_EP_Mapping_Workspaces_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp03067_EP_Site_Edit_Linked_InspectionsTableAdapter
            // 
            this.sp03067_EP_Site_Edit_Linked_InspectionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp03044_EP_Asset_Manager_List_SimpleTableAdapter
            // 
            this.sp03044_EP_Asset_Manager_List_SimpleTableAdapter.ClearBeforeFill = true;
            // 
            // sp04002_GC_Select_Company_ListTableAdapter
            // 
            this.sp04002_GC_Select_Company_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Site_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(945, 703);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Site_Edit";
            this.Text = "Edit Site";
            this.Activated += new System.EventHandler(this.frm_Core_Site_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Site_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Site_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SiteImageFileButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03007EPSiteEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteKMLDataMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeoFenceDistanceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.COAddressMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSnowClearanceSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientsSiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientsSiteCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HubIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04002GCSelectCompanyListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteTextMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03044EPAssetManagerListSimpleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03067EPSiteEditLinkedInspectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03063EPMappingWorkspacesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00227PicklistListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03009EPClientListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonPositionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteTelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteMobileTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteFaxTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XCoordinateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YCoordinateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteEmailMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHubID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrittingSiteCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSnowClearanceSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteImageFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCOAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteKMLData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForViewKML)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientsSiteCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientsSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForXCoordinate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYCoordinate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGeoFenceDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private System.Windows.Forms.BindingSource sp03007EPSiteEditBindingSource;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteCodeTextEdit;
        private DevExpress.XtraEditors.TextEdit ContactPersonTextEdit;
        private DevExpress.XtraEditors.TextEdit ContactPersonPositionTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine1TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine2TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteAddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit SiteTelephoneTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteMobileTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteFaxTextEdit;
        private DevExpress.XtraEditors.SpinEdit XCoordinateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit YCoordinateSpinEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactPerson;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactPersonPosition;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSitePostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteTelephone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteMobile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteFax;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForXCoordinate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForYCoordinate;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03007_EP_Site_EditTableAdapter sp03007_EP_Site_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit SiteTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit2View;
        private DevExpress.XtraEditors.GridLookUpEdit ClientIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddressLine1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00227PicklistListWithBlankBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colintOrder;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00227_Picklist_List_With_BlankTableAdapter sp00227_Picklist_List_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp03009EPClientListWithBlankBindingSource;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03009_EP_Client_List_With_BlankTableAdapter sp03009_EP_Client_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.BindingSource sp03063EPMappingWorkspacesWithBlankBindingSource;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03063_EP_Mapping_Workspaces_With_BlankTableAdapter sp03063_EP_Mapping_Workspaces_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkspaceID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkspaceName;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.BindingSource sp03067EPSiteEditLinkedInspectionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspector;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeDeductions;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeDeductionsRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03067_EP_Site_Edit_Linked_InspectionsTableAdapter sp03067_EP_Site_Edit_Linked_InspectionsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private System.Windows.Forms.BindingSource sp03044EPAssetManagerListSimpleBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetSubTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colModelNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLifeSpanValue;
        private DevExpress.XtraGrid.Columns.GridColumn colLifeSpanValueDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentConditionID;
        private DevExpress.XtraGrid.Columns.GridColumn colConditionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLastVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNextVisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03044_EP_Asset_Manager_List_SimpleTableAdapter sp03044_EP_Asset_Manager_List_SimpleTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.MemoEdit SiteTextMemoEdit;
        private DevExpress.XtraEditors.MemoEdit SiteEmailMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteText;
        private DevExpress.XtraEditors.GridLookUpEdit CompanyIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SpinEdit LocationYSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LocationXSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DataSet_GC_Core dataSet_GC_Core;
        private System.Windows.Forms.BindingSource sp04002GCSelectCompanyListBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04002_GC_Select_Company_ListTableAdapter sp04002_GC_Select_Company_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraEditors.SpinEdit SiteOrderSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteOrder;
        private DevExpress.XtraEditors.TextEdit HubIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHubID;
        private DevExpress.XtraEditors.CheckEdit GrittingSiteCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrittingSiteCheckEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.TextEdit ClientsSiteCodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientsSiteCode;
        private DevExpress.XtraEditors.TextEdit ClientsSiteIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientsSiteID;
        private DevExpress.XtraEditors.CheckEdit IsSnowClearanceSiteCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsSnowClearanceSite;
        private DevExpress.XtraEditors.ButtonEdit SitePostcodeTextEdit;
        private DevExpress.XtraEditors.SimpleButton btnViewOnMap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.MemoEdit COAddressMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCOAddress;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraEditors.SpinEdit GeoFenceDistanceSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGeoFenceDistance;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraEditors.MemoEdit SiteKMLDataMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteKMLData;
        private DevExpress.XtraEditors.SimpleButton btnViewKML;
        private DevExpress.XtraLayout.LayoutControlItem ItemForViewKML;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraEditors.ButtonEdit SiteImageFileButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteImageFile;
    }
}

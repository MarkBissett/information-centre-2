namespace WoodPlan5
{
    partial class frm_Core_Sequence_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Sequence_Edit));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SiteNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.sp00164SequenceItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.ClientNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.intIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.strSequenceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intLengthOfNumericPartSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strFieldGridLookupEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00165SequencePicklistFieldsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrTable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTable_Field = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserFriendlyField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserFriendlyTable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBlanksSequenceCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBlankSequenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.strRemarksMemoExEdit = new DevExpress.XtraEditors.MemoEdit();
            this.strTableTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForintID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrField = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrSequence = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintLengthOfNumericPart = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrTable = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp03009EPClientListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP_DataEntry = new WoodPlan5.DataSet_EP_DataEntry();
            this.sp00164_Sequence_ItemTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00164_Sequence_ItemTableAdapter();
            this.sp00165_Sequence_Picklist_Fields_ListTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00165_Sequence_Picklist_Fields_ListTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.sp03009_EP_Client_List_With_BlankTableAdapter = new WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03009_EP_Client_List_With_BlankTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00164SequenceItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strSequenceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intLengthOfNumericPartSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strFieldGridLookupEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00165SequencePicklistFieldsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTableTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrSequence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintLengthOfNumericPart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03009EPClientListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(813, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 411);
            this.barDockControlBottom.Size = new System.Drawing.Size(813, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 385);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(813, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 385);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(813, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 411);
            this.barDockControl2.Size = new System.Drawing.Size(813, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 385);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(813, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 385);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SiteNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.intIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.strSequenceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intLengthOfNumericPartSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strFieldGridLookupEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.strTableTextEdit);
            this.dataLayoutControl1.DataSource = this.sp00164SequenceItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(813, 385);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SiteNameButtonEdit
            // 
            this.SiteNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00164SequenceItemBindingSource, "SiteName", true));
            this.SiteNameButtonEdit.Location = new System.Drawing.Point(142, 170);
            this.SiteNameButtonEdit.MenuManager = this.barManager1;
            this.SiteNameButtonEdit.Name = "SiteNameButtonEdit";
            this.SiteNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to Select Site", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click to Clear Site", "clear", null, true)});
            this.SiteNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SiteNameButtonEdit.Size = new System.Drawing.Size(664, 20);
            this.SiteNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameButtonEdit.TabIndex = 14;
            this.SiteNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteNameButtonEdit_ButtonClick);
            // 
            // sp00164SequenceItemBindingSource
            // 
            this.sp00164SequenceItemBindingSource.DataMember = "sp00164_Sequence_Item";
            this.sp00164SequenceItemBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ClientNameButtonEdit
            // 
            this.ClientNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00164SequenceItemBindingSource, "ClientName", true));
            this.ClientNameButtonEdit.Location = new System.Drawing.Point(142, 146);
            this.ClientNameButtonEdit.MenuManager = this.barManager1;
            this.ClientNameButtonEdit.Name = "ClientNameButtonEdit";
            this.ClientNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to Select Client", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to Clear Client", "clear", null, true)});
            this.ClientNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientNameButtonEdit.Size = new System.Drawing.Size(664, 20);
            this.ClientNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameButtonEdit.TabIndex = 13;
            this.ClientNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientNameButtonEdit_ButtonClick);
            // 
            // intIDSpinEdit
            // 
            this.intIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00164SequenceItemBindingSource, "intID", true));
            this.intIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intIDSpinEdit.Location = new System.Drawing.Point(148, 12);
            this.intIDSpinEdit.MenuManager = this.barManager1;
            this.intIDSpinEdit.Name = "intIDSpinEdit";
            this.intIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intIDSpinEdit.Properties.IsFloatValue = false;
            this.intIDSpinEdit.Properties.Mask.EditMask = "N00";
            this.intIDSpinEdit.Properties.NullValuePrompt = "Value Calculated on Saving Record";
            this.intIDSpinEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.intIDSpinEdit.Properties.ReadOnly = true;
            this.intIDSpinEdit.Size = new System.Drawing.Size(468, 20);
            this.intIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.intIDSpinEdit.TabIndex = 4;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00164SequenceItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(145, 7);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(178, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 10;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // strSequenceTextEdit
            // 
            this.strSequenceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00164SequenceItemBindingSource, "strSequence", true));
            this.strSequenceTextEdit.Location = new System.Drawing.Point(142, 88);
            this.strSequenceTextEdit.MenuManager = this.barManager1;
            this.strSequenceTextEdit.Name = "strSequenceTextEdit";
            this.strSequenceTextEdit.Properties.MaxLength = 17;
            this.strSequenceTextEdit.Properties.NullValuePrompt = "No Sequence [Number Sequentially]";
            this.strSequenceTextEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strSequenceTextEdit, true);
            this.strSequenceTextEdit.Size = new System.Drawing.Size(664, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strSequenceTextEdit, optionsSpelling1);
            this.strSequenceTextEdit.StyleController = this.dataLayoutControl1;
            this.strSequenceTextEdit.TabIndex = 7;
            this.strSequenceTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strSequenceTextEdit_Validating);
            // 
            // intLengthOfNumericPartSpinEdit
            // 
            this.intLengthOfNumericPartSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00164SequenceItemBindingSource, "intLengthOfNumericPart", true));
            this.intLengthOfNumericPartSpinEdit.EditValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.intLengthOfNumericPartSpinEdit.Location = new System.Drawing.Point(142, 112);
            this.intLengthOfNumericPartSpinEdit.MenuManager = this.barManager1;
            this.intLengthOfNumericPartSpinEdit.Name = "intLengthOfNumericPartSpinEdit";
            this.intLengthOfNumericPartSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intLengthOfNumericPartSpinEdit.Properties.Mask.EditMask = "f0";
            this.intLengthOfNumericPartSpinEdit.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.intLengthOfNumericPartSpinEdit.Properties.MinValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.intLengthOfNumericPartSpinEdit.Properties.NullValuePrompt = "Enter a value [Range 3 - 20]";
            this.intLengthOfNumericPartSpinEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.intLengthOfNumericPartSpinEdit.Size = new System.Drawing.Size(664, 20);
            this.intLengthOfNumericPartSpinEdit.StyleController = this.dataLayoutControl1;
            this.intLengthOfNumericPartSpinEdit.TabIndex = 8;
            this.intLengthOfNumericPartSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.intLengthOfNumericPartSpinEdit_Validating);
            // 
            // strFieldGridLookupEdit
            // 
            this.strFieldGridLookupEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00164SequenceItemBindingSource, "strField", true));
            this.strFieldGridLookupEdit.Location = new System.Drawing.Point(142, 54);
            this.strFieldGridLookupEdit.MenuManager = this.barManager1;
            this.strFieldGridLookupEdit.Name = "strFieldGridLookupEdit";
            this.strFieldGridLookupEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.strFieldGridLookupEdit.Properties.DataSource = this.sp00165SequencePicklistFieldsListBindingSource;
            this.strFieldGridLookupEdit.Properties.DisplayMember = "Table_Field";
            this.strFieldGridLookupEdit.Properties.NullText = "";
            this.strFieldGridLookupEdit.Properties.NullValuePrompt = "Select a value";
            this.strFieldGridLookupEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.strFieldGridLookupEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.strFieldGridLookupEdit.Properties.ValueMember = "strField";
            this.strFieldGridLookupEdit.Properties.View = this.gridView1;
            this.strFieldGridLookupEdit.Size = new System.Drawing.Size(664, 20);
            this.strFieldGridLookupEdit.StyleController = this.dataLayoutControl1;
            this.strFieldGridLookupEdit.TabIndex = 6;
            this.strFieldGridLookupEdit.TabStop = false;
            this.strFieldGridLookupEdit.EditValueChanged += new System.EventHandler(this.strFieldGridLookupEdit_EditValueChanged);
            this.strFieldGridLookupEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strFieldGridLookupEdit_Validating);
            // 
            // sp00165SequencePicklistFieldsListBindingSource
            // 
            this.sp00165SequencePicklistFieldsListBindingSource.DataMember = "sp00165_Sequence_Picklist_Fields_List";
            this.sp00165SequencePicklistFieldsListBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colstrTable,
            this.colstrField,
            this.colstrID,
            this.colstrRemarks,
            this.colTable_Field,
            this.colUserFriendlyField,
            this.colUserFriendlyTable,
            this.colBlanksSequenceCount,
            this.colBlankSequenceID,
            this.colModuleName});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colModuleName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colUserFriendlyTable, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colUserFriendlyField, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colstrTable
            // 
            this.colstrTable.Caption = "Actual Table Name";
            this.colstrTable.FieldName = "strTable";
            this.colstrTable.Name = "colstrTable";
            this.colstrTable.OptionsColumn.AllowEdit = false;
            this.colstrTable.OptionsColumn.AllowFocus = false;
            this.colstrTable.OptionsColumn.ReadOnly = true;
            this.colstrTable.Width = 156;
            // 
            // colstrField
            // 
            this.colstrField.Caption = "Actual Field Name";
            this.colstrField.FieldName = "strField";
            this.colstrField.Name = "colstrField";
            this.colstrField.OptionsColumn.AllowEdit = false;
            this.colstrField.OptionsColumn.AllowFocus = false;
            this.colstrField.OptionsColumn.ReadOnly = true;
            this.colstrField.Width = 139;
            // 
            // colstrID
            // 
            this.colstrID.Caption = "ID";
            this.colstrID.FieldName = "strID";
            this.colstrID.Name = "colstrID";
            this.colstrID.OptionsColumn.AllowEdit = false;
            this.colstrID.OptionsColumn.AllowFocus = false;
            this.colstrID.OptionsColumn.ReadOnly = true;
            // 
            // colstrRemarks
            // 
            this.colstrRemarks.Caption = "Remarks";
            this.colstrRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colstrRemarks.FieldName = "strRemarks";
            this.colstrRemarks.Name = "colstrRemarks";
            this.colstrRemarks.OptionsColumn.ReadOnly = true;
            this.colstrRemarks.Visible = true;
            this.colstrRemarks.VisibleIndex = 2;
            this.colstrRemarks.Width = 167;
            // 
            // colTable_Field
            // 
            this.colTable_Field.Caption = "Table \\ Field Name";
            this.colTable_Field.FieldName = "Table_Field";
            this.colTable_Field.Name = "colTable_Field";
            this.colTable_Field.OptionsColumn.AllowEdit = false;
            this.colTable_Field.OptionsColumn.AllowFocus = false;
            this.colTable_Field.OptionsColumn.ReadOnly = true;
            this.colTable_Field.Width = 292;
            // 
            // colUserFriendlyField
            // 
            this.colUserFriendlyField.Caption = "Field Name";
            this.colUserFriendlyField.FieldName = "UserFriendlyField";
            this.colUserFriendlyField.Name = "colUserFriendlyField";
            this.colUserFriendlyField.OptionsColumn.AllowEdit = false;
            this.colUserFriendlyField.OptionsColumn.AllowFocus = false;
            this.colUserFriendlyField.OptionsColumn.ReadOnly = true;
            this.colUserFriendlyField.Visible = true;
            this.colUserFriendlyField.VisibleIndex = 1;
            this.colUserFriendlyField.Width = 181;
            // 
            // colUserFriendlyTable
            // 
            this.colUserFriendlyTable.Caption = "Table Name";
            this.colUserFriendlyTable.FieldName = "UserFriendlyTable";
            this.colUserFriendlyTable.Name = "colUserFriendlyTable";
            this.colUserFriendlyTable.OptionsColumn.AllowEdit = false;
            this.colUserFriendlyTable.OptionsColumn.AllowFocus = false;
            this.colUserFriendlyTable.OptionsColumn.ReadOnly = true;
            this.colUserFriendlyTable.Visible = true;
            this.colUserFriendlyTable.VisibleIndex = 0;
            this.colUserFriendlyTable.Width = 172;
            // 
            // colBlanksSequenceCount
            // 
            this.colBlanksSequenceCount.Caption = "Blank Sequence Count";
            this.colBlanksSequenceCount.FieldName = "BlanksSequenceCount";
            this.colBlanksSequenceCount.Name = "colBlanksSequenceCount";
            this.colBlanksSequenceCount.OptionsColumn.AllowEdit = false;
            this.colBlanksSequenceCount.OptionsColumn.AllowFocus = false;
            this.colBlanksSequenceCount.OptionsColumn.ReadOnly = true;
            this.colBlanksSequenceCount.Width = 128;
            // 
            // colBlankSequenceID
            // 
            this.colBlankSequenceID.Caption = "Blank Sequence ID";
            this.colBlankSequenceID.FieldName = "BlankSequenceID";
            this.colBlankSequenceID.Name = "colBlankSequenceID";
            this.colBlankSequenceID.OptionsColumn.AllowEdit = false;
            this.colBlankSequenceID.OptionsColumn.AllowFocus = false;
            this.colBlankSequenceID.OptionsColumn.ReadOnly = true;
            // 
            // colModuleName
            // 
            this.colModuleName.Caption = "Module";
            this.colModuleName.FieldName = "ModuleName";
            this.colModuleName.Name = "colModuleName";
            this.colModuleName.OptionsColumn.AllowEdit = false;
            this.colModuleName.OptionsColumn.AllowFocus = false;
            this.colModuleName.OptionsColumn.ReadOnly = true;
            this.colModuleName.Visible = true;
            this.colModuleName.VisibleIndex = 3;
            this.colModuleName.Width = 155;
            // 
            // strRemarksMemoExEdit
            // 
            this.strRemarksMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00164SequenceItemBindingSource, "strRemarks", true));
            this.strRemarksMemoExEdit.Location = new System.Drawing.Point(142, 204);
            this.strRemarksMemoExEdit.MenuManager = this.barManager1;
            this.strRemarksMemoExEdit.Name = "strRemarksMemoExEdit";
            this.strRemarksMemoExEdit.Properties.MaxLength = 500;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoExEdit, true);
            this.strRemarksMemoExEdit.Size = new System.Drawing.Size(664, 164);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoExEdit, optionsSpelling2);
            this.strRemarksMemoExEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoExEdit.TabIndex = 9;
            // 
            // strTableTextEdit
            // 
            this.strTableTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00164SequenceItemBindingSource, "strTable", true));
            this.strTableTextEdit.Location = new System.Drawing.Point(142, 30);
            this.strTableTextEdit.MenuManager = this.barManager1;
            this.strTableTextEdit.Name = "strTableTextEdit";
            this.strTableTextEdit.Properties.NullValuePrompt = "Read Only - Value set by Linked To Field";
            this.strTableTextEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.strTableTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strTableTextEdit, true);
            this.strTableTextEdit.Size = new System.Drawing.Size(664, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strTableTextEdit, optionsSpelling3);
            this.strTableTextEdit.StyleController = this.dataLayoutControl1;
            this.strTableTextEdit.TabIndex = 5;
            this.strTableTextEdit.TabStop = false;
            // 
            // ItemForintID
            // 
            this.ItemForintID.Control = this.intIDSpinEdit;
            this.ItemForintID.CustomizationFormText = "ID:";
            this.ItemForintID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintID.Name = "ItemForintID";
            this.ItemForintID.Size = new System.Drawing.Size(608, 24);
            this.ItemForintID.Text = "ID:";
            this.ItemForintID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(813, 385);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrField,
            this.ItemForstrSequence,
            this.ItemForintLengthOfNumericPart,
            this.ItemForstrRemarks,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.ItemForstrTable,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.layoutControlItem4,
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(803, 375);
            // 
            // ItemForstrField
            // 
            this.ItemForstrField.AllowHide = false;
            this.ItemForstrField.AllowHtmlStringInCaption = true;
            this.ItemForstrField.Control = this.strFieldGridLookupEdit;
            this.ItemForstrField.CustomizationFormText = "Linked to Field:";
            this.ItemForstrField.Location = new System.Drawing.Point(0, 47);
            this.ItemForstrField.Name = "ItemForstrField";
            this.ItemForstrField.Size = new System.Drawing.Size(803, 24);
            this.ItemForstrField.Text = "Linked to Field:";
            this.ItemForstrField.TextSize = new System.Drawing.Size(132, 13);
            // 
            // ItemForstrSequence
            // 
            this.ItemForstrSequence.AllowHide = false;
            this.ItemForstrSequence.AllowHtmlStringInCaption = true;
            this.ItemForstrSequence.Control = this.strSequenceTextEdit;
            this.ItemForstrSequence.CustomizationFormText = "Sequence:";
            this.ItemForstrSequence.Location = new System.Drawing.Point(0, 81);
            this.ItemForstrSequence.Name = "ItemForstrSequence";
            this.ItemForstrSequence.Size = new System.Drawing.Size(803, 24);
            this.ItemForstrSequence.Text = "Sequence:";
            this.ItemForstrSequence.TextSize = new System.Drawing.Size(132, 13);
            // 
            // ItemForintLengthOfNumericPart
            // 
            this.ItemForintLengthOfNumericPart.AllowHide = false;
            this.ItemForintLengthOfNumericPart.AllowHtmlStringInCaption = true;
            this.ItemForintLengthOfNumericPart.Control = this.intLengthOfNumericPartSpinEdit;
            this.ItemForintLengthOfNumericPart.CustomizationFormText = "Length of Calculated Digits:";
            this.ItemForintLengthOfNumericPart.Location = new System.Drawing.Point(0, 105);
            this.ItemForintLengthOfNumericPart.Name = "ItemForintLengthOfNumericPart";
            this.ItemForintLengthOfNumericPart.Size = new System.Drawing.Size(803, 24);
            this.ItemForintLengthOfNumericPart.Text = "Length of Calculated Digits:";
            this.ItemForintLengthOfNumericPart.TextSize = new System.Drawing.Size(132, 13);
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.AllowHtmlStringInCaption = true;
            this.ItemForstrRemarks.Control = this.strRemarksMemoExEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 197);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(803, 168);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(132, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 365);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(803, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AllowHtmlStringInCaption = true;
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(138, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(182, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(138, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(138, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(138, 23);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(320, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(483, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrTable
            // 
            this.ItemForstrTable.AllowHide = false;
            this.ItemForstrTable.AllowHtmlStringInCaption = true;
            this.ItemForstrTable.Control = this.strTableTextEdit;
            this.ItemForstrTable.CustomizationFormText = "Linked to Table:";
            this.ItemForstrTable.Location = new System.Drawing.Point(0, 23);
            this.ItemForstrTable.Name = "ItemForstrTable";
            this.ItemForstrTable.Size = new System.Drawing.Size(803, 24);
            this.ItemForstrTable.Text = "Linked to Table:";
            this.ItemForstrTable.TextSize = new System.Drawing.Size(132, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 129);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(803, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 187);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(803, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(803, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AllowHtmlStringInCaption = true;
            this.layoutControlItem4.Control = this.ClientNameButtonEdit;
            this.layoutControlItem4.CustomizationFormText = "Linked to Client [<i>Optional</i>]:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 139);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(803, 24);
            this.layoutControlItem4.Text = "Linked to Client [<i>Optional</i>]:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(132, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHtmlStringInCaption = true;
            this.layoutControlItem2.Control = this.SiteNameButtonEdit;
            this.layoutControlItem2.CustomizationFormText = "Linked to Site [<i>Optional</i>]:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 163);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(803, 24);
            this.layoutControlItem2.Text = "Linked to Site [<i>Optional</i>]:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(132, 13);
            // 
            // sp03009EPClientListWithBlankBindingSource
            // 
            this.sp03009EPClientListWithBlankBindingSource.DataMember = "sp03009_EP_Client_List_With_Blank";
            this.sp03009EPClientListWithBlankBindingSource.DataSource = this.dataSet_EP_DataEntry;
            // 
            // dataSet_EP_DataEntry
            // 
            this.dataSet_EP_DataEntry.DataSetName = "DataSet_EP_DataEntry";
            this.dataSet_EP_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00164_Sequence_ItemTableAdapter
            // 
            this.sp00164_Sequence_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00165_Sequence_Picklist_Fields_ListTableAdapter
            // 
            this.sp00165_Sequence_Picklist_Fields_ListTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            this.dxErrorProvider1.DataSource = this.sp00164SequenceItemBindingSource;
            // 
            // sp03009_EP_Client_List_With_BlankTableAdapter
            // 
            this.sp03009_EP_Client_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16");
            // 
            // frm_Core_Sequence_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(813, 439);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Sequence_Edit";
            this.Text = "Edit Sequence";
            this.Activated += new System.EventHandler(this.frm_Core_Sequence_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Sequence_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Sequence_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00164SequenceItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strSequenceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intLengthOfNumericPartSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strFieldGridLookupEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00165SequencePicklistFieldsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTableTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrSequence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintLengthOfNumericPart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03009EPClientListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SpinEdit intIDSpinEdit;
        private System.Windows.Forms.BindingSource sp00164SequenceItemBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraEditors.TextEdit strSequenceTextEdit;
        private DevExpress.XtraEditors.SpinEdit intLengthOfNumericPartSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrTable;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrField;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrSequence;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintLengthOfNumericPart;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00164_Sequence_ItemTableAdapter sp00164_Sequence_ItemTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit strFieldGridLookupEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoExEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.BindingSource sp00165SequencePicklistFieldsListBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00165_Sequence_Picklist_Fields_ListTableAdapter sp00165_Sequence_Picklist_Fields_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrTable;
        private DevExpress.XtraGrid.Columns.GridColumn colstrField;
        private DevExpress.XtraGrid.Columns.GridColumn colstrID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTable_Field;
        private DevExpress.XtraGrid.Columns.GridColumn colUserFriendlyField;
        private DevExpress.XtraGrid.Columns.GridColumn colUserFriendlyTable;
        private DevExpress.XtraEditors.TextEdit strTableTextEdit;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraGrid.Columns.GridColumn colBlanksSequenceCount;
        private DevExpress.XtraGrid.Columns.GridColumn colBlankSequenceID;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DataSet_EP_DataEntry dataSet_EP_DataEntry;
        private System.Windows.Forms.BindingSource sp03009EPClientListWithBlankBindingSource;
        private WoodPlan5.DataSet_EP_DataEntryTableAdapters.sp03009_EP_Client_List_With_BlankTableAdapter sp03009_EP_Client_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit SiteNameButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit ClientNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

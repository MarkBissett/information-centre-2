using System.Windows.Forms;
using BaseObjects;

namespace WoodPlan5
{
    partial class frmSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSplash));
            this.sp00001PopulateSwitchboardTableAdapter1 = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00001PopulateSwitchboardTableAdapter();
            this.timerSplash = new System.Windows.Forms.Timer(this.components);
            this.dbPanel = new DevExpress.XtraEditors.PanelControl();
            this.lblProgress = new DevExpress.XtraEditors.LabelControl();
            this.label5 = new System.Windows.Forms.Label();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.styleController1 = new DevExpress.XtraEditors.StyleController(this.components);
            this.cboDatabaseDetails = new BaseObjects.ctrlExtendedComboBoxEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.lblSeconds = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUsername = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00019GetUserDetailsForStartUpBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00019GetUserDetailsForStartUpTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00019GetUserDetailsForStartUpTableAdapter();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.sp00071_get_available_loginsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00071_get_available_loginsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00071_get_available_loginsTableAdapter();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbPanel)).BeginInit();
            this.dbPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDatabaseDetails.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00019GetUserDetailsForStartUpBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00071_get_available_loginsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(365, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 294);
            this.barDockControlBottom.Size = new System.Drawing.Size(365, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 294);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(365, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 294);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // sp00001PopulateSwitchboardTableAdapter1
            // 
            this.sp00001PopulateSwitchboardTableAdapter1.ClearBeforeFill = true;
            // 
            // timerSplash
            // 
            this.timerSplash.Enabled = true;
            this.timerSplash.Interval = 1000;
            this.timerSplash.Tick += new System.EventHandler(this.timerSplash_Tick);
            // 
            // dbPanel
            // 
            this.dbPanel.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dbPanel.Appearance.Options.UseBackColor = true;
            this.dbPanel.Controls.Add(this.lblProgress);
            this.dbPanel.Controls.Add(this.label5);
            this.dbPanel.Controls.Add(this.progressBarControl1);
            this.dbPanel.Controls.Add(this.cboDatabaseDetails);
            this.dbPanel.Controls.Add(this.btnCancel);
            this.dbPanel.Controls.Add(this.txtPassword);
            this.dbPanel.Controls.Add(this.label4);
            this.dbPanel.Controls.Add(this.btnOK);
            this.dbPanel.Controls.Add(this.lblSeconds);
            this.dbPanel.Controls.Add(this.label2);
            this.dbPanel.Controls.Add(this.label1);
            this.dbPanel.Controls.Add(this.txtUsername);
            this.dbPanel.Controls.Add(this.label3);
            this.dbPanel.Location = new System.Drawing.Point(1, 166);
            this.dbPanel.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.dbPanel.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dbPanel.Name = "dbPanel";
            this.dbPanel.Size = new System.Drawing.Size(365, 117);
            this.dbPanel.TabIndex = 9;
            // 
            // lblProgress
            // 
            this.lblProgress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblProgress.Location = new System.Drawing.Point(123, 84);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(121, 13);
            this.lblProgress.TabIndex = 15;
            this.lblProgress.Text = "Initialising Switchboard...";
            this.lblProgress.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Password:";
            this.label5.Visible = false;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(30, 66);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.progressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.progressBarControl1.Size = new System.Drawing.Size(307, 13);
            this.progressBarControl1.StyleController = this.styleController1;
            this.progressBarControl1.TabIndex = 14;
            // 
            // cboDatabaseDetails
            // 
            this.cboDatabaseDetails.Location = new System.Drawing.Point(77, 8);
            this.cboDatabaseDetails.MenuManager = this.barManager1;
            this.cboDatabaseDetails.Name = "cboDatabaseDetails";
            this.cboDatabaseDetails.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDatabaseDetails.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboDatabaseDetails.Size = new System.Drawing.Size(269, 22);
            this.cboDatabaseDetails.StyleController = this.styleController1;
            this.cboDatabaseDetails.TabIndex = 14;
            this.cboDatabaseDetails.SelectedIndexChanged += new System.EventHandler(this.cboDatabaseDetails_SelectedIndexChanged);
            this.cboDatabaseDetails.Click += new System.EventHandler(this.cboDatabaseDetails_Click);
            this.cboDatabaseDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboDatabaseDetails_KeyDown);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(188, 86);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(56, 23);
            this.btnCancel.StyleController = this.styleController1;
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Enabled = false;
            this.txtPassword.Location = new System.Drawing.Point(77, 59);
            this.txtPassword.MenuManager = this.barManager1;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.PasswordChar = '*';
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtPassword, true);
            this.txtPassword.Size = new System.Drawing.Size(269, 22);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtPassword, optionsSpelling1);
            this.txtPassword.StyleController = this.styleController1;
            this.txtPassword.TabIndex = 17;
            this.txtPassword.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Username:";
            this.label4.Visible = false;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(122, 86);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(56, 23);
            this.btnOK.StyleController = this.styleController1;
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            this.btnOK.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnOK_KeyDown);
            // 
            // lblSeconds
            // 
            this.lblSeconds.AutoSize = true;
            this.lblSeconds.Location = new System.Drawing.Point(301, 91);
            this.lblSeconds.Name = "lblSeconds";
            this.lblSeconds.Size = new System.Drawing.Size(13, 13);
            this.lblSeconds.TabIndex = 11;
            this.lblSeconds.Text = "0";
            this.lblSeconds.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(255, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Login in";
            this.label2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(315, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Seconds";
            this.label1.Visible = false;
            // 
            // txtUsername
            // 
            this.txtUsername.Enabled = false;
            this.txtUsername.Location = new System.Drawing.Point(77, 33);
            this.txtUsername.MenuManager = this.barManager1;
            this.txtUsername.Name = "txtUsername";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtUsername, true);
            this.txtUsername.Size = new System.Drawing.Size(269, 22);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtUsername, optionsSpelling2);
            this.txtUsername.StyleController = this.styleController1;
            this.txtUsername.TabIndex = 15;
            this.txtUsername.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Database:";
            // 
            // lblVersion
            // 
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.ForeColor = System.Drawing.Color.Green;
            this.lblVersion.Location = new System.Drawing.Point(198, 150);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(148, 13);
            this.lblVersion.TabIndex = 10;
            this.lblVersion.Text = "1.0.0.0";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00019GetUserDetailsForStartUpBindingSource
            // 
            this.sp00019GetUserDetailsForStartUpBindingSource.DataMember = "sp00019GetUserDetailsForStartUp";
            this.sp00019GetUserDetailsForStartUpBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00019GetUserDetailsForStartUpTableAdapter
            // 
            this.sp00019GetUserDetailsForStartUpTableAdapter.ClearBeforeFill = true;
            // 
            // lblCopyright
            // 
            this.lblCopyright.BackColor = System.Drawing.Color.Transparent;
            this.lblCopyright.ForeColor = System.Drawing.Color.Gray;
            this.lblCopyright.Location = new System.Drawing.Point(17, 150);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(216, 13);
            this.lblCopyright.TabIndex = 11;
            this.lblCopyright.Text = "Copyright";
            // 
            // sp00071_get_available_loginsBindingSource
            // 
            this.sp00071_get_available_loginsBindingSource.DataMember = "sp00071_get_available_logins";
            this.sp00071_get_available_loginsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00071_get_available_loginsTableAdapter
            // 
            this.sp00071_get_available_loginsTableAdapter.ClearBeforeFill = true;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(5, -1);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.AllowFocused = false;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Size = new System.Drawing.Size(356, 158);
            this.pictureEdit1.TabIndex = 14;
            // 
            // frmSplash
            // 
            this.AcceptButton = this.btnOK;
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(365, 294);
            this.ControlBox = false;
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.dbPanel);
            this.Controls.Add(this.pictureEdit1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmSplash";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.Load += new System.EventHandler(this.frmSplash_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.pictureEdit1, 0);
            this.Controls.SetChildIndex(this.dbPanel, 0);
            this.Controls.SetChildIndex(this.lblVersion, 0);
            this.Controls.SetChildIndex(this.lblCopyright, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbPanel)).EndInit();
            this.dbPanel.ResumeLayout(false);
            this.dbPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDatabaseDetails.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00019GetUserDetailsForStartUpBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00071_get_available_loginsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00001PopulateSwitchboardTableAdapter sp00001PopulateSwitchboardTableAdapter1;
        private System.Windows.Forms.Timer timerSplash;
        private Label label3;
        private DevExpress.XtraEditors.PanelControl dbPanel;
        private Label lblSeconds;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private WoodPlanDataSet woodPlanDataSet;
        private BindingSource sp00019GetUserDetailsForStartUpBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00019GetUserDetailsForStartUpTableAdapter sp00019GetUserDetailsForStartUpTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private Label lblVersion;
        private Label lblCopyright;
        private DevExpress.XtraEditors.LabelControl lblProgress;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private BindingSource sp00071_get_available_loginsBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00071_get_available_loginsTableAdapter sp00071_get_available_loginsTableAdapter;
        private ctrlExtendedComboBoxEdit cboDatabaseDetails;
        private Label label2;
        private Label label1;
        private DevExpress.XtraEditors.TextEdit txtUsername;
        private Label label4;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private Label label5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.StyleController styleController1;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
    }
}
namespace WoodPlan5
{
    partial class frm_Core_CRM_Contact_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_CRM_Contact_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ClientContactButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.sp05082CRMContactItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ContactDirectionIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp05085GCCRMContactDirectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ContactedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CreatedByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp05084GCCRMStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ContactTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp05087GCCRMContactTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ContactMethodIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp05086GCCRMContactMethodsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ContactMadeDateTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ContactDueDateTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ClientContactIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.DescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ContactedByStaffNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.LinkedToRecordIDGridLookUpEdit = new DevExpress.XtraEditors.TextEdit();
            this.CRMIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToRecordTypeIDLookupEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp05088CRMLinkedRecordTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colintOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForClientContactID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCRMID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToRecordID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForContactTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactMethodID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactDirectionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactDueDateTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactedByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLinkedToRecordTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForContactMadeDateTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientContact = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.sp05082_CRM_Contact_ItemTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05082_CRM_Contact_ItemTableAdapter();
            this.sp05084_GC_CRM_StatusesTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05084_GC_CRM_StatusesTableAdapter();
            this.sp05085_GC_CRM_Contact_DirectionTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05085_GC_CRM_Contact_DirectionTableAdapter();
            this.sp05086_GC_CRM_Contact_Methods_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05086_GC_CRM_Contact_Methods_With_BlankTableAdapter();
            this.sp05087_GC_CRM_Contact_Types_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05087_GC_CRM_Contact_Types_With_BlankTableAdapter();
            this.sp05088_CRM_Linked_Record_TypesTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05088_CRM_Linked_Record_TypesTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05082CRMContactItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactDirectionIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05085GCCRMContactDirectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05084GCCRMStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05087GCCRMContactTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactMethodIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05086GCCRMContactMethodsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactMadeDateTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactMadeDateTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactDueDateTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactDueDateTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactedByStaffNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CRMIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordTypeIDLookupEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05088CRMLinkedRecordTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCRMID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactMethodID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactDirectionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactDueDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactedByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactMadeDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(724, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 516);
            this.barDockControlBottom.Size = new System.Drawing.Size(724, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 490);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(724, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 490);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // colintID
            // 
            this.colintID.Caption = "ID";
            this.colintID.FieldName = "intID";
            this.colintID.Name = "colintID";
            this.colintID.OptionsColumn.AllowEdit = false;
            this.colintID.OptionsColumn.AllowFocus = false;
            this.colintID.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(724, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 516);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(724, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 490);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(724, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 490);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ClientContactButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactDirectionIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactMethodIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactMadeDateTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactDueDateTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContactIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.buttonEdit1);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.DescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactedByStaffNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToRecordIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CRMIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToRecordTypeIDLookupEdit);
            this.dataLayoutControl1.DataSource = this.sp05082CRMContactItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientContactID,
            this.ItemForContactedByStaffID,
            this.ItemForCreatedByStaffID,
            this.ItemForCRMID,
            this.ItemForLinkedToRecordID,
            this.ItemForClientID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1373, 160, 518, 707);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(724, 490);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ClientContactButtonEdit
            // 
            this.ClientContactButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ClientContact", true));
            this.ClientContactButtonEdit.Location = new System.Drawing.Point(123, 59);
            this.ClientContactButtonEdit.MenuManager = this.barManager1;
            this.ClientContactButtonEdit.Name = "ClientContactButtonEdit";
            this.ClientContactButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Select From List", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear Value", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Selection", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientContactButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientContactButtonEdit.Size = new System.Drawing.Size(395, 20);
            this.ClientContactButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientContactButtonEdit.TabIndex = 28;
            this.ClientContactButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientContactButtonEdit_ButtonClick);
            // 
            // sp05082CRMContactItemBindingSource
            // 
            this.sp05082CRMContactItemBindingSource.DataMember = "sp05082_CRM_Contact_Item";
            this.sp05082CRMContactItemBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(114, 59);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(485, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling1);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 27;
            // 
            // ClientNameButtonEdit
            // 
            this.ClientNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ClientName", true));
            this.ClientNameButtonEdit.Location = new System.Drawing.Point(123, 35);
            this.ClientNameButtonEdit.MenuManager = this.barManager1;
            this.ClientNameButtonEdit.Name = "ClientNameButtonEdit";
            this.ClientNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Select From List", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientNameButtonEdit.Size = new System.Drawing.Size(395, 20);
            this.ClientNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameButtonEdit.TabIndex = 26;
            this.ClientNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientNameButtonEdit_ButtonClick);
            this.ClientNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientNameButtonEdit_Validating);
            // 
            // ContactDirectionIDGridLookUpEdit
            // 
            this.ContactDirectionIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ContactDirectionID", true));
            this.ContactDirectionIDGridLookUpEdit.Location = new System.Drawing.Point(147, 389);
            this.ContactDirectionIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContactDirectionIDGridLookUpEdit.Name = "ContactDirectionIDGridLookUpEdit";
            this.ContactDirectionIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContactDirectionIDGridLookUpEdit.Properties.DataSource = this.sp05085GCCRMContactDirectionBindingSource;
            this.ContactDirectionIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ContactDirectionIDGridLookUpEdit.Properties.NullText = "";
            this.ContactDirectionIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.ContactDirectionIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ContactDirectionIDGridLookUpEdit.Size = new System.Drawing.Size(373, 20);
            this.ContactDirectionIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContactDirectionIDGridLookUpEdit.TabIndex = 25;
            // 
            // sp05085GCCRMContactDirectionBindingSource
            // 
            this.sp05085GCCRMContactDirectionBindingSource.DataMember = "sp05085_GC_CRM_Contact_Direction";
            this.sp05085GCCRMContactDirectionBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Description";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 198;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Record Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 100;
            // 
            // ContactedByStaffIDTextEdit
            // 
            this.ContactedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ContactedByStaffID", true));
            this.ContactedByStaffIDTextEdit.Location = new System.Drawing.Point(126, 290);
            this.ContactedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.ContactedByStaffIDTextEdit.Name = "ContactedByStaffIDTextEdit";
            this.ContactedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactedByStaffIDTextEdit, true);
            this.ContactedByStaffIDTextEdit.Size = new System.Drawing.Size(473, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactedByStaffIDTextEdit, optionsSpelling2);
            this.ContactedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactedByStaffIDTextEdit.TabIndex = 24;
            // 
            // CreatedByStaffNameTextEdit
            // 
            this.CreatedByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "CreatedByStaffName", true));
            this.CreatedByStaffNameTextEdit.Location = new System.Drawing.Point(147, 413);
            this.CreatedByStaffNameTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffNameTextEdit.Name = "CreatedByStaffNameTextEdit";
            this.CreatedByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffNameTextEdit, true);
            this.CreatedByStaffNameTextEdit.Size = new System.Drawing.Size(373, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffNameTextEdit, optionsSpelling3);
            this.CreatedByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffNameTextEdit.TabIndex = 23;
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(123, 250);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(476, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling4);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 22;
            // 
            // StatusIDGridLookUpEdit
            // 
            this.StatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "StatusID", true));
            this.StatusIDGridLookUpEdit.Location = new System.Drawing.Point(147, 317);
            this.StatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIDGridLookUpEdit.Name = "StatusIDGridLookUpEdit";
            this.StatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusIDGridLookUpEdit.Properties.DataSource = this.sp05084GCCRMStatusesBindingSource;
            this.StatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusIDGridLookUpEdit.Properties.NullText = "";
            this.StatusIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.StatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusIDGridLookUpEdit.Size = new System.Drawing.Size(373, 20);
            this.StatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDGridLookUpEdit.TabIndex = 21;
            // 
            // sp05084GCCRMStatusesBindingSource
            // 
            this.sp05084GCCRMStatusesBindingSource.DataMember = "sp05084_GC_CRM_Statuses";
            this.sp05084GCCRMStatusesBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colDescription,
            this.colRecordOrder});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 198;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Record Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Width = 100;
            // 
            // ContactTypeIDGridLookUpEdit
            // 
            this.ContactTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ContactTypeID", true));
            this.ContactTypeIDGridLookUpEdit.Location = new System.Drawing.Point(147, 341);
            this.ContactTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContactTypeIDGridLookUpEdit.Name = "ContactTypeIDGridLookUpEdit";
            this.ContactTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContactTypeIDGridLookUpEdit.Properties.DataSource = this.sp05087GCCRMContactTypesWithBlankBindingSource;
            this.ContactTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ContactTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ContactTypeIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.ContactTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ContactTypeIDGridLookUpEdit.Size = new System.Drawing.Size(373, 20);
            this.ContactTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContactTypeIDGridLookUpEdit.TabIndex = 20;
            // 
            // sp05087GCCRMContactTypesWithBlankBindingSource
            // 
            this.sp05087GCCRMContactTypesWithBlankBindingSource.DataMember = "sp05087_GC_CRM_Contact_Types_With_Blank";
            this.sp05087GCCRMContactTypesWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn7;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Description";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 198;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Record Order";
            this.gridColumn9.FieldName = "RecordOrder";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 100;
            // 
            // ContactMethodIDGridLookUpEdit
            // 
            this.ContactMethodIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ContactMethodID", true));
            this.ContactMethodIDGridLookUpEdit.Location = new System.Drawing.Point(147, 365);
            this.ContactMethodIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContactMethodIDGridLookUpEdit.Name = "ContactMethodIDGridLookUpEdit";
            this.ContactMethodIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContactMethodIDGridLookUpEdit.Properties.DataSource = this.sp05086GCCRMContactMethodsWithBlankBindingSource;
            this.ContactMethodIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ContactMethodIDGridLookUpEdit.Properties.NullText = "";
            this.ContactMethodIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.ContactMethodIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ContactMethodIDGridLookUpEdit.Size = new System.Drawing.Size(373, 20);
            this.ContactMethodIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContactMethodIDGridLookUpEdit.TabIndex = 19;
            // 
            // sp05086GCCRMContactMethodsWithBlankBindingSource
            // 
            this.sp05086GCCRMContactMethodsWithBlankBindingSource.DataMember = "sp05086_GC_CRM_Contact_Methods_With_Blank";
            this.sp05086GCCRMContactMethodsWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn4;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Description";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 198;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Record Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 100;
            // 
            // ContactMadeDateTimeDateEdit
            // 
            this.ContactMadeDateTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ContactMadeDateTime", true));
            this.ContactMadeDateTimeDateEdit.EditValue = null;
            this.ContactMadeDateTimeDateEdit.Location = new System.Drawing.Point(123, 107);
            this.ContactMadeDateTimeDateEdit.MenuManager = this.barManager1;
            this.ContactMadeDateTimeDateEdit.Name = "ContactMadeDateTimeDateEdit";
            this.ContactMadeDateTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContactMadeDateTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ContactMadeDateTimeDateEdit.Properties.Mask.EditMask = "g";
            this.ContactMadeDateTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ContactMadeDateTimeDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.ContactMadeDateTimeDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.ContactMadeDateTimeDateEdit.Size = new System.Drawing.Size(141, 20);
            this.ContactMadeDateTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.ContactMadeDateTimeDateEdit.TabIndex = 18;
            this.ContactMadeDateTimeDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContactMadeDateTimeDateEdit_ButtonClick);
            this.ContactMadeDateTimeDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContactMadeDateTimeDateEdit_Validating);
            // 
            // ContactDueDateTimeDateEdit
            // 
            this.ContactDueDateTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ContactDueDateTime", true));
            this.ContactDueDateTimeDateEdit.EditValue = null;
            this.ContactDueDateTimeDateEdit.Location = new System.Drawing.Point(123, 83);
            this.ContactDueDateTimeDateEdit.MenuManager = this.barManager1;
            this.ContactDueDateTimeDateEdit.Name = "ContactDueDateTimeDateEdit";
            this.ContactDueDateTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Clear Value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContactDueDateTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ContactDueDateTimeDateEdit.Properties.Mask.EditMask = "g";
            this.ContactDueDateTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ContactDueDateTimeDateEdit.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.ContactDueDateTimeDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.ContactDueDateTimeDateEdit.Size = new System.Drawing.Size(141, 20);
            this.ContactDueDateTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.ContactDueDateTimeDateEdit.TabIndex = 17;
            this.ContactDueDateTimeDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContactDueDateTimeDateEdit_ButtonClick);
            this.ContactDueDateTimeDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContactDueDateTimeDateEdit_Validating);
            // 
            // ClientContactIDTextEdit
            // 
            this.ClientContactIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ClientContactID", true));
            this.ClientContactIDTextEdit.Location = new System.Drawing.Point(126, 290);
            this.ClientContactIDTextEdit.MenuManager = this.barManager1;
            this.ClientContactIDTextEdit.Name = "ClientContactIDTextEdit";
            this.ClientContactIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContactIDTextEdit, true);
            this.ClientContactIDTextEdit.Size = new System.Drawing.Size(473, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContactIDTextEdit, optionsSpelling5);
            this.ClientContactIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContactIDTextEdit.TabIndex = 16;
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "LinkedRecordDescription", true));
            this.buttonEdit1.Location = new System.Drawing.Point(123, 213);
            this.buttonEdit1.MenuManager = this.barManager1;
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Select From List", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Clear Selection", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEdit1.Properties.MaxLength = 100;
            this.buttonEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEdit1.Size = new System.Drawing.Size(395, 20);
            this.buttonEdit1.StyleController = this.dataLayoutControl1;
            this.buttonEdit1.TabIndex = 14;
            this.buttonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_ButtonClick);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp05082CRMContactItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(124, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(183, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // DescriptionTextEdit
            // 
            this.DescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "Description", true));
            this.DescriptionTextEdit.Location = new System.Drawing.Point(123, 131);
            this.DescriptionTextEdit.MenuManager = this.barManager1;
            this.DescriptionTextEdit.Name = "DescriptionTextEdit";
            this.DescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DescriptionTextEdit, true);
            this.DescriptionTextEdit.Size = new System.Drawing.Size(589, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DescriptionTextEdit, optionsSpelling6);
            this.DescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.DescriptionTextEdit.TabIndex = 9;
            this.DescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DescriptionTextEdit_Validating);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 317);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(652, 127);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling7);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // ContactedByStaffNameButtonEdit
            // 
            this.ContactedByStaffNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "ContactedByStaffName", true));
            this.ContactedByStaffNameButtonEdit.Location = new System.Drawing.Point(123, 155);
            this.ContactedByStaffNameButtonEdit.MenuManager = this.barManager1;
            this.ContactedByStaffNameButtonEdit.Name = "ContactedByStaffNameButtonEdit";
            editorButtonImageOptions8.Location = DevExpress.XtraEditors.ImageLocation.Default;
            this.ContactedByStaffNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Select From List", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Clear Selection", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContactedByStaffNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ContactedByStaffNameButtonEdit.Size = new System.Drawing.Size(395, 20);
            this.ContactedByStaffNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ContactedByStaffNameButtonEdit.TabIndex = 7;
            this.ContactedByStaffNameButtonEdit.TabStop = false;
            this.ContactedByStaffNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContactedByStaffNameButtonEdit_ButtonClick);
            // 
            // LinkedToRecordIDGridLookUpEdit
            // 
            this.LinkedToRecordIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "LinkedToRecordID", true));
            this.LinkedToRecordIDGridLookUpEdit.EditValue = "";
            this.LinkedToRecordIDGridLookUpEdit.Location = new System.Drawing.Point(123, 258);
            this.LinkedToRecordIDGridLookUpEdit.MenuManager = this.barManager1;
            this.LinkedToRecordIDGridLookUpEdit.Name = "LinkedToRecordIDGridLookUpEdit";
            this.LinkedToRecordIDGridLookUpEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToRecordIDGridLookUpEdit, true);
            this.LinkedToRecordIDGridLookUpEdit.Size = new System.Drawing.Size(476, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToRecordIDGridLookUpEdit, optionsSpelling8);
            this.LinkedToRecordIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToRecordIDGridLookUpEdit.TabIndex = 5;
            this.LinkedToRecordIDGridLookUpEdit.TabStop = false;
            // 
            // CRMIDTextEdit
            // 
            this.CRMIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "CRMID", true));
            this.CRMIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CRMIDTextEdit.Location = new System.Drawing.Point(123, 234);
            this.CRMIDTextEdit.MenuManager = this.barManager1;
            this.CRMIDTextEdit.Name = "CRMIDTextEdit";
            this.CRMIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CRMIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CRMIDTextEdit, true);
            this.CRMIDTextEdit.Size = new System.Drawing.Size(476, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CRMIDTextEdit, optionsSpelling9);
            this.CRMIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CRMIDTextEdit.TabIndex = 4;
            // 
            // LinkedToRecordTypeIDLookupEdit
            // 
            this.LinkedToRecordTypeIDLookupEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp05082CRMContactItemBindingSource, "LinkedToRecordTypeID", true));
            this.LinkedToRecordTypeIDLookupEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LinkedToRecordTypeIDLookupEdit.Location = new System.Drawing.Point(123, 189);
            this.LinkedToRecordTypeIDLookupEdit.MenuManager = this.barManager1;
            this.LinkedToRecordTypeIDLookupEdit.Name = "LinkedToRecordTypeIDLookupEdit";
            this.LinkedToRecordTypeIDLookupEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LinkedToRecordTypeIDLookupEdit.Properties.DataSource = this.sp05088CRMLinkedRecordTypesBindingSource;
            this.LinkedToRecordTypeIDLookupEdit.Properties.DisplayMember = "strDescription";
            this.LinkedToRecordTypeIDLookupEdit.Properties.NullText = "";
            this.LinkedToRecordTypeIDLookupEdit.Properties.PopupView = this.gridView4;
            this.LinkedToRecordTypeIDLookupEdit.Properties.ValueMember = "intID";
            this.LinkedToRecordTypeIDLookupEdit.Size = new System.Drawing.Size(395, 20);
            this.LinkedToRecordTypeIDLookupEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToRecordTypeIDLookupEdit.TabIndex = 6;
            this.LinkedToRecordTypeIDLookupEdit.TabStop = false;
            this.LinkedToRecordTypeIDLookupEdit.EditValueChanged += new System.EventHandler(this.LinkedToRecordTypeIDLookupEdit_EditValueChanged);
            this.LinkedToRecordTypeIDLookupEdit.Validated += new System.EventHandler(this.LinkedToRecordTypeIDLookupEdit_Validated);
            // 
            // sp05088CRMLinkedRecordTypesBindingSource
            // 
            this.sp05088CRMLinkedRecordTypesBindingSource.DataMember = "sp05088_CRM_Linked_Record_Types";
            this.sp05088CRMLinkedRecordTypesBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colintID,
            this.colintOrder,
            this.colstrDescription});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colintID;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colintOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colintOrder
            // 
            this.colintOrder.Caption = "Order";
            this.colintOrder.FieldName = "intOrder";
            this.colintOrder.Name = "colintOrder";
            this.colintOrder.OptionsColumn.AllowEdit = false;
            this.colintOrder.OptionsColumn.AllowFocus = false;
            this.colintOrder.OptionsColumn.ReadOnly = true;
            // 
            // colstrDescription
            // 
            this.colstrDescription.Caption = "Type";
            this.colstrDescription.FieldName = "strDescription";
            this.colstrDescription.Name = "colstrDescription";
            this.colstrDescription.OptionsColumn.AllowEdit = false;
            this.colstrDescription.OptionsColumn.AllowFocus = false;
            this.colstrDescription.OptionsColumn.ReadOnly = true;
            this.colstrDescription.Visible = true;
            this.colstrDescription.VisibleIndex = 0;
            this.colstrDescription.Width = 181;
            // 
            // ItemForClientContactID
            // 
            this.ItemForClientContactID.Control = this.ClientContactIDTextEdit;
            this.ItemForClientContactID.CustomizationFormText = "Client Contact ID:";
            this.ItemForClientContactID.Location = new System.Drawing.Point(0, 278);
            this.ItemForClientContactID.Name = "ItemForClientContactID";
            this.ItemForClientContactID.Size = new System.Drawing.Size(591, 24);
            this.ItemForClientContactID.Text = "Client Contact ID:";
            this.ItemForClientContactID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForContactedByStaffID
            // 
            this.ItemForContactedByStaffID.Control = this.ContactedByStaffIDTextEdit;
            this.ItemForContactedByStaffID.CustomizationFormText = "Contacted By Staff ID:";
            this.ItemForContactedByStaffID.Location = new System.Drawing.Point(0, 278);
            this.ItemForContactedByStaffID.Name = "ItemForContactedByStaffID";
            this.ItemForContactedByStaffID.Size = new System.Drawing.Size(591, 24);
            this.ItemForContactedByStaffID.Text = "Contacted By Staff ID:";
            this.ItemForContactedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.CustomizationFormText = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 278);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(591, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCRMID
            // 
            this.ItemForCRMID.Control = this.CRMIDTextEdit;
            this.ItemForCRMID.CustomizationFormText = "CRM ID:";
            this.ItemForCRMID.Location = new System.Drawing.Point(0, 278);
            this.ItemForCRMID.Name = "ItemForCRMID";
            this.ItemForCRMID.Size = new System.Drawing.Size(591, 24);
            this.ItemForCRMID.Text = "CRM ID:";
            this.ItemForCRMID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToRecordID
            // 
            this.ItemForLinkedToRecordID.Control = this.LinkedToRecordIDGridLookUpEdit;
            this.ItemForLinkedToRecordID.CustomizationFormText = "Linked To Record ID:";
            this.ItemForLinkedToRecordID.Location = new System.Drawing.Point(0, 278);
            this.ItemForLinkedToRecordID.Name = "ItemForLinkedToRecordID";
            this.ItemForLinkedToRecordID.Size = new System.Drawing.Size(591, 24);
            this.ItemForLinkedToRecordID.Text = "Linked To Record ID:";
            this.ItemForLinkedToRecordID.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 47);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(591, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(724, 490);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroup4,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.ItemForContactDueDateTime,
            this.ItemForDescription,
            this.ItemForContactedByStaffName,
            this.emptySpaceItem3,
            this.emptySpaceItem2,
            this.ItemForLinkedToRecordTypeID,
            this.ItemForClientName,
            this.emptySpaceItem7,
            this.ItemForContactMadeDateTime,
            this.emptySpaceItem8,
            this.ItemForClientContact,
            this.emptySpaceItem9,
            this.emptySpaceItem10,
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(704, 470);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 460);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(704, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 235);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(704, 225);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(680, 179);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStatusID,
            this.emptySpaceItem6,
            this.emptySpaceItem11,
            this.ItemForContactTypeID,
            this.ItemForContactMethodID,
            this.ItemForContactDirectionID,
            this.ItemForCreatedByStaffName});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(656, 131);
            this.layoutControlGroup5.Text = "Details";
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDGridLookUpEdit;
            this.ItemForStatusID.CustomizationFormText = "Status:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 0);
            this.ItemForStatusID.MaxSize = new System.Drawing.Size(488, 24);
            this.ItemForStatusID.MinSize = new System.Drawing.Size(488, 24);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(488, 24);
            this.ItemForStatusID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStatusID.Text = "Status:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(656, 11);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(488, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(168, 120);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForContactTypeID
            // 
            this.ItemForContactTypeID.Control = this.ContactTypeIDGridLookUpEdit;
            this.ItemForContactTypeID.CustomizationFormText = "Contact Type:";
            this.ItemForContactTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForContactTypeID.Name = "ItemForContactTypeID";
            this.ItemForContactTypeID.Size = new System.Drawing.Size(488, 24);
            this.ItemForContactTypeID.Text = "Contact Type:";
            this.ItemForContactTypeID.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForContactMethodID
            // 
            this.ItemForContactMethodID.Control = this.ContactMethodIDGridLookUpEdit;
            this.ItemForContactMethodID.CustomizationFormText = "Contact Method:";
            this.ItemForContactMethodID.Location = new System.Drawing.Point(0, 48);
            this.ItemForContactMethodID.Name = "ItemForContactMethodID";
            this.ItemForContactMethodID.Size = new System.Drawing.Size(488, 24);
            this.ItemForContactMethodID.Text = "Contact Method:";
            this.ItemForContactMethodID.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForContactDirectionID
            // 
            this.ItemForContactDirectionID.Control = this.ContactDirectionIDGridLookUpEdit;
            this.ItemForContactDirectionID.CustomizationFormText = "Contact Direction:";
            this.ItemForContactDirectionID.Location = new System.Drawing.Point(0, 72);
            this.ItemForContactDirectionID.Name = "ItemForContactDirectionID";
            this.ItemForContactDirectionID.Size = new System.Drawing.Size(488, 24);
            this.ItemForContactDirectionID.Text = "Contact Direction:";
            this.ItemForContactDirectionID.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForCreatedByStaffName
            // 
            this.ItemForCreatedByStaffName.Control = this.CreatedByStaffNameTextEdit;
            this.ItemForCreatedByStaffName.CustomizationFormText = "Created By:";
            this.ItemForCreatedByStaffName.Location = new System.Drawing.Point(0, 96);
            this.ItemForCreatedByStaffName.Name = "ItemForCreatedByStaffName";
            this.ItemForCreatedByStaffName.Size = new System.Drawing.Size(488, 24);
            this.ItemForCreatedByStaffName.Text = "Created By:";
            this.ItemForCreatedByStaffName.TextSize = new System.Drawing.Size(108, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImageOptions.Image")));
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(656, 131);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(656, 131);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(112, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(112, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(112, 23);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(299, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(405, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(112, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(187, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForContactDueDateTime
            // 
            this.ItemForContactDueDateTime.Control = this.ContactDueDateTimeDateEdit;
            this.ItemForContactDueDateTime.CustomizationFormText = "Contact Due:";
            this.ItemForContactDueDateTime.Location = new System.Drawing.Point(0, 71);
            this.ItemForContactDueDateTime.MaxSize = new System.Drawing.Size(256, 24);
            this.ItemForContactDueDateTime.MinSize = new System.Drawing.Size(256, 24);
            this.ItemForContactDueDateTime.Name = "ItemForContactDueDateTime";
            this.ItemForContactDueDateTime.Size = new System.Drawing.Size(256, 24);
            this.ItemForContactDueDateTime.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForContactDueDateTime.Text = "Contact Due:";
            this.ItemForContactDueDateTime.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.AllowHide = false;
            this.ItemForDescription.Control = this.DescriptionTextEdit;
            this.ItemForDescription.CustomizationFormText = "Link Description:";
            this.ItemForDescription.Location = new System.Drawing.Point(0, 119);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(704, 24);
            this.ItemForDescription.Text = "Contact Description:";
            this.ItemForDescription.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForContactedByStaffName
            // 
            this.ItemForContactedByStaffName.AllowHide = false;
            this.ItemForContactedByStaffName.Control = this.ContactedByStaffNameButtonEdit;
            this.ItemForContactedByStaffName.CustomizationFormText = "GC Contact Person:";
            this.ItemForContactedByStaffName.Location = new System.Drawing.Point(0, 143);
            this.ItemForContactedByStaffName.MaxSize = new System.Drawing.Size(510, 24);
            this.ItemForContactedByStaffName.MinSize = new System.Drawing.Size(510, 24);
            this.ItemForContactedByStaffName.Name = "ItemForContactedByStaffName";
            this.ItemForContactedByStaffName.Size = new System.Drawing.Size(510, 24);
            this.ItemForContactedByStaffName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForContactedByStaffName.Text = "GC Contact Person:";
            this.ItemForContactedByStaffName.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(704, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 225);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(704, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLinkedToRecordTypeID
            // 
            this.ItemForLinkedToRecordTypeID.Control = this.LinkedToRecordTypeIDLookupEdit;
            this.ItemForLinkedToRecordTypeID.CustomizationFormText = "Linked Record Type:";
            this.ItemForLinkedToRecordTypeID.Location = new System.Drawing.Point(0, 177);
            this.ItemForLinkedToRecordTypeID.MaxSize = new System.Drawing.Size(510, 24);
            this.ItemForLinkedToRecordTypeID.MinSize = new System.Drawing.Size(510, 24);
            this.ItemForLinkedToRecordTypeID.Name = "ItemForLinkedToRecordTypeID";
            this.ItemForLinkedToRecordTypeID.Size = new System.Drawing.Size(510, 24);
            this.ItemForLinkedToRecordTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLinkedToRecordTypeID.Text = "Linked Record Type:";
            this.ItemForLinkedToRecordTypeID.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameButtonEdit;
            this.ItemForClientName.CustomizationFormText = "Client:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientName.MaxSize = new System.Drawing.Size(510, 24);
            this.ItemForClientName.MinSize = new System.Drawing.Size(510, 24);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(510, 24);
            this.ItemForClientName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientName.Text = "Client:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(256, 71);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(448, 48);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForContactMadeDateTime
            // 
            this.ItemForContactMadeDateTime.Control = this.ContactMadeDateTimeDateEdit;
            this.ItemForContactMadeDateTime.CustomizationFormText = "Contact Made:";
            this.ItemForContactMadeDateTime.Location = new System.Drawing.Point(0, 95);
            this.ItemForContactMadeDateTime.MaxSize = new System.Drawing.Size(256, 24);
            this.ItemForContactMadeDateTime.MinSize = new System.Drawing.Size(256, 24);
            this.ItemForContactMadeDateTime.Name = "ItemForContactMadeDateTime";
            this.ItemForContactMadeDateTime.Size = new System.Drawing.Size(256, 24);
            this.ItemForContactMadeDateTime.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForContactMadeDateTime.Text = "Contact Made:";
            this.ItemForContactMadeDateTime.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(510, 23);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(194, 48);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientContact
            // 
            this.ItemForClientContact.Control = this.ClientContactButtonEdit;
            this.ItemForClientContact.CustomizationFormText = "Client Contact Person:";
            this.ItemForClientContact.Location = new System.Drawing.Point(0, 47);
            this.ItemForClientContact.Name = "ItemForClientContact";
            this.ItemForClientContact.Size = new System.Drawing.Size(510, 24);
            this.ItemForClientContact.Text = "Client Contact Person:";
            this.ItemForClientContact.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(510, 143);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(194, 24);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(510, 177);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(194, 48);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHide = false;
            this.layoutControlItem2.Control = this.buttonEdit1;
            this.layoutControlItem2.CustomizationFormText = "Linked To Record:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 201);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(510, 24);
            this.layoutControlItem2.Text = "Linked To Record:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(108, 13);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp05082_CRM_Contact_ItemTableAdapter
            // 
            this.sp05082_CRM_Contact_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp05084_GC_CRM_StatusesTableAdapter
            // 
            this.sp05084_GC_CRM_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp05085_GC_CRM_Contact_DirectionTableAdapter
            // 
            this.sp05085_GC_CRM_Contact_DirectionTableAdapter.ClearBeforeFill = true;
            // 
            // sp05086_GC_CRM_Contact_Methods_With_BlankTableAdapter
            // 
            this.sp05086_GC_CRM_Contact_Methods_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp05087_GC_CRM_Contact_Types_With_BlankTableAdapter
            // 
            this.sp05087_GC_CRM_Contact_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp05088_CRM_Linked_Record_TypesTableAdapter
            // 
            this.sp05088_CRM_Linked_Record_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_CRM_Contact_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(724, 544);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_CRM_Contact_Edit";
            this.Text = "Edit CRM Contact";
            this.Activated += new System.EventHandler(this.frm_Core_CRM_Contact_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_CRM_Contact_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_CRM_Contact_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05082CRMContactItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactDirectionIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05085GCCRMContactDirectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05084GCCRMStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05087GCCRMContactTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactMethodIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05086GCCRMContactMethodsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactMadeDateTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactMadeDateTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactDueDateTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactDueDateTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactedByStaffNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CRMIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordTypeIDLookupEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05088CRMLinkedRecordTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCRMID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactMethodID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactDirectionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactDueDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactedByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactMadeDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.TextEdit DescriptionTextEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCRMID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToRecordID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToRecordTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactedByStaffName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;
        private DevExpress.XtraEditors.ButtonEdit ContactedByStaffNameButtonEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit LinkedToRecordIDGridLookUpEdit;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit CRMIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientContactIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContactID;
        private DevExpress.XtraEditors.DateEdit ContactDueDateTimeDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactDueDateTime;
        private DevExpress.XtraEditors.DateEdit ContactMadeDateTimeDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactMadeDateTime;
        private DevExpress.XtraEditors.GridLookUpEdit ContactMethodIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactMethodID;
        private DevExpress.XtraEditors.GridLookUpEdit ContactTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactTypeID;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffName;
        private DevExpress.XtraEditors.TextEdit ContactedByStaffIDTextEdit;
        private System.Windows.Forms.BindingSource sp05082CRMContactItemBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactedByStaffID;
        private WoodPlanDataSetTableAdapters.sp05082_CRM_Contact_ItemTableAdapter sp05082_CRM_Contact_ItemTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit ContactDirectionIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactDirectionID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource sp05084GCCRMStatusesBindingSource;
        private WoodPlanDataSetTableAdapters.sp05084_GC_CRM_StatusesTableAdapter sp05084_GC_CRM_StatusesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private System.Windows.Forms.BindingSource sp05085GCCRMContactDirectionBindingSource;
        private WoodPlanDataSetTableAdapters.sp05085_GC_CRM_Contact_DirectionTableAdapter sp05085_GC_CRM_Contact_DirectionTableAdapter;
        private System.Windows.Forms.BindingSource sp05086GCCRMContactMethodsWithBlankBindingSource;
        private WoodPlanDataSetTableAdapters.sp05086_GC_CRM_Contact_Methods_With_BlankTableAdapter sp05086_GC_CRM_Contact_Methods_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.BindingSource sp05087GCCRMContactTypesWithBlankBindingSource;
        private WoodPlanDataSetTableAdapters.sp05087_GC_CRM_Contact_Types_With_BlankTableAdapter sp05087_GC_CRM_Contact_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.GridLookUpEdit LinkedToRecordTypeIDLookupEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colintID;
        private DevExpress.XtraGrid.Columns.GridColumn colintOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colstrDescription;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit ClientNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private System.Windows.Forms.BindingSource sp05088CRMLinkedRecordTypesBindingSource;
        private WoodPlanDataSetTableAdapters.sp05088_CRM_Linked_Record_TypesTableAdapter sp05088_CRM_Linked_Record_TypesTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit ClientContactButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContact;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
    }
}

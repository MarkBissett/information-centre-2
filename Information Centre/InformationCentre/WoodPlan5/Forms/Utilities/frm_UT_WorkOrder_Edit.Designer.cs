namespace WoodPlan5
{
    partial class frm_UT_WorkOrder_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_WorkOrder_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPermissionDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPDFFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colPermissionEmailed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colEmailedToClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailedToCustomerDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLandownerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryScreenName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07247UTWorkOrderEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp07318UTWorkOrderStatusListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WorkOrderPDFHyperLinkEdit = new DevExpress.XtraEditors.HyperLinkEdit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07272UTWorkOrderEditLinkedMapsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDateTimeCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colMapOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureTakenByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderTeamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMobileTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CreatedByTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalCostTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PaddedWorkOrderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.WorkOrderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colDateScheduled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colApprovedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemBillingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSpeciesList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateSubContractorSentDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.DateCompletedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ReferenceNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.DateCreatedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ItemForWorkOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDateSubContractorSent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateCompleted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkOrderPDF = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDateCreated = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemforCreatedBy = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPaddedWorkOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator6 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07247_UT_Work_Order_EditTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07247_UT_Work_Order_EditTableAdapter();
            this.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter();
            this.sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter();
            this.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter();
            this.sp07318_UT_Work_Order_Status_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07318_UT_Work_Order_Status_ListTableAdapter();
            this.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07247UTWorkOrderEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07318UTWorkOrderStatusListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderPDFHyperLinkEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07272UTWorkOrderEditLinkedMapsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaddedWorkOrderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateSubContractorSentDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateSubContractorSentDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateSubContractorSent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCompleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaddedWorkOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1143, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 718);
            this.barDockControlBottom.Size = new System.Drawing.Size(1143, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1143, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 692);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.barButtonItemPrint});
            this.barManager2.MaxItemId = 28;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(487, 172);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint)});
            this.bar2.Text = "Discounts";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Create Work Order Document";
            this.barButtonItemPrint.Id = 27;
            this.barButtonItemPrint.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.ImageOptions.Image")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1143, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 718);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1143, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1143, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 692);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(7, "arrow_down_blue_round.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl4);
            this.dataLayoutControl1.Controls.Add(this.StatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkOrderPDFHyperLinkEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl3);
            this.dataLayoutControl1.Controls.Add(this.gridControl2);
            this.dataLayoutControl1.Controls.Add(this.CreatedByTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PaddedWorkOrderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.WorkOrderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer1);
            this.dataLayoutControl1.Controls.Add(this.DateSubContractorSentDateEdit);
            this.dataLayoutControl1.Controls.Add(this.DateCompletedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReferenceNumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.DateCreatedDateEdit);
            this.dataLayoutControl1.DataSource = this.sp07247UTWorkOrderEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWorkOrderID,
            this.ItemForSubContractorID,
            this.ItemForCreatedByStaffID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(223, 264, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1143, 692);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(36, 400);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemCheckEdit3});
            this.gridControl4.Size = new System.Drawing.Size(1071, 256);
            this.gridControl4.TabIndex = 42;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource
            // 
            this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource.DataMember = "sp07352_UT_Work_Order_Linked_Permission_Documents";
            this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPermissionDocumentID,
            this.colDateRaised1,
            this.colPDFFile,
            this.colPermissionEmailed,
            this.colEmailedToClientDate,
            this.colEmailedToCustomerDate,
            this.colRemarks3,
            this.colLandownerName,
            this.colClientID,
            this.colDataEntryScreenName,
            this.colReportLayoutName});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLandownerName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView4_CustomRowCellEdit);
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView4_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView4_ShowingEditor);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colPermissionDocumentID
            // 
            this.colPermissionDocumentID.Caption = "Permission Document ID";
            this.colPermissionDocumentID.FieldName = "PermissionDocumentID";
            this.colPermissionDocumentID.Name = "colPermissionDocumentID";
            this.colPermissionDocumentID.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentID.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentID.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentID.Width = 136;
            // 
            // colDateRaised1
            // 
            this.colDateRaised1.Caption = "Date Raised";
            this.colDateRaised1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateRaised1.FieldName = "DateRaised";
            this.colDateRaised1.Name = "colDateRaised1";
            this.colDateRaised1.OptionsColumn.AllowEdit = false;
            this.colDateRaised1.OptionsColumn.AllowFocus = false;
            this.colDateRaised1.OptionsColumn.ReadOnly = true;
            this.colDateRaised1.Visible = true;
            this.colDateRaised1.VisibleIndex = 0;
            this.colDateRaised1.Width = 109;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colPDFFile
            // 
            this.colPDFFile.Caption = "PDF File";
            this.colPDFFile.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colPDFFile.FieldName = "PDFFile";
            this.colPDFFile.Name = "colPDFFile";
            this.colPDFFile.OptionsColumn.ReadOnly = true;
            this.colPDFFile.Visible = true;
            this.colPDFFile.VisibleIndex = 2;
            this.colPDFFile.Width = 270;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colPermissionEmailed
            // 
            this.colPermissionEmailed.Caption = "Permission Emailed";
            this.colPermissionEmailed.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colPermissionEmailed.FieldName = "PermissionEmailed";
            this.colPermissionEmailed.Name = "colPermissionEmailed";
            this.colPermissionEmailed.OptionsColumn.AllowEdit = false;
            this.colPermissionEmailed.OptionsColumn.AllowFocus = false;
            this.colPermissionEmailed.OptionsColumn.ReadOnly = true;
            this.colPermissionEmailed.Visible = true;
            this.colPermissionEmailed.VisibleIndex = 3;
            this.colPermissionEmailed.Width = 110;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colEmailedToClientDate
            // 
            this.colEmailedToClientDate.Caption = "Emailed To Client";
            this.colEmailedToClientDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEmailedToClientDate.FieldName = "EmailedToClientDate";
            this.colEmailedToClientDate.Name = "colEmailedToClientDate";
            this.colEmailedToClientDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToClientDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToClientDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToClientDate.Visible = true;
            this.colEmailedToClientDate.VisibleIndex = 4;
            this.colEmailedToClientDate.Width = 102;
            // 
            // colEmailedToCustomerDate
            // 
            this.colEmailedToCustomerDate.Caption = "Emailed To Customer";
            this.colEmailedToCustomerDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEmailedToCustomerDate.FieldName = "EmailedToCustomerDate";
            this.colEmailedToCustomerDate.Name = "colEmailedToCustomerDate";
            this.colEmailedToCustomerDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToCustomerDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToCustomerDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToCustomerDate.Visible = true;
            this.colEmailedToCustomerDate.VisibleIndex = 5;
            this.colEmailedToCustomerDate.Width = 121;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colLandownerName
            // 
            this.colLandownerName.Caption = "Landowner Name";
            this.colLandownerName.FieldName = "LandownerName";
            this.colLandownerName.Name = "colLandownerName";
            this.colLandownerName.OptionsColumn.AllowEdit = false;
            this.colLandownerName.OptionsColumn.AllowFocus = false;
            this.colLandownerName.OptionsColumn.ReadOnly = true;
            this.colLandownerName.Visible = true;
            this.colLandownerName.VisibleIndex = 1;
            this.colLandownerName.Width = 184;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Width = 62;
            // 
            // colDataEntryScreenName
            // 
            this.colDataEntryScreenName.Caption = "Data Entry Screen";
            this.colDataEntryScreenName.FieldName = "DataEntryScreenName";
            this.colDataEntryScreenName.Name = "colDataEntryScreenName";
            this.colDataEntryScreenName.OptionsColumn.AllowEdit = false;
            this.colDataEntryScreenName.OptionsColumn.AllowFocus = false;
            this.colDataEntryScreenName.OptionsColumn.ReadOnly = true;
            this.colDataEntryScreenName.Width = 109;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Report Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.Width = 120;
            // 
            // StatusIDGridLookUpEdit
            // 
            this.StatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "StatusID", true));
            this.StatusIDGridLookUpEdit.Location = new System.Drawing.Point(145, 175);
            this.StatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIDGridLookUpEdit.Name = "StatusIDGridLookUpEdit";
            this.StatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusIDGridLookUpEdit.Properties.DataSource = this.sp07318UTWorkOrderStatusListBindingSource;
            this.StatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusIDGridLookUpEdit.Properties.NullText = "";
            this.StatusIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.StatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusIDGridLookUpEdit.Size = new System.Drawing.Size(950, 20);
            this.StatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDGridLookUpEdit.TabIndex = 50;
            // 
            // sp07247UTWorkOrderEditBindingSource
            // 
            this.sp07247UTWorkOrderEditBindingSource.DataMember = "sp07247_UT_Work_Order_Edit";
            this.sp07247UTWorkOrderEditBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // sp07318UTWorkOrderStatusListBindingSource
            // 
            this.sp07318UTWorkOrderStatusListBindingSource.DataMember = "sp07318_UT_Work_Order_Status_List";
            this.sp07318UTWorkOrderStatusListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colID,
            this.colRecordOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 230;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // WorkOrderPDFHyperLinkEdit
            // 
            this.WorkOrderPDFHyperLinkEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "WorkOrderPDF", true));
            this.WorkOrderPDFHyperLinkEdit.Location = new System.Drawing.Point(145, 271);
            this.WorkOrderPDFHyperLinkEdit.MenuManager = this.barManager1;
            this.WorkOrderPDFHyperLinkEdit.Name = "WorkOrderPDFHyperLinkEdit";
            this.WorkOrderPDFHyperLinkEdit.Properties.ReadOnly = true;
            this.WorkOrderPDFHyperLinkEdit.Size = new System.Drawing.Size(950, 20);
            this.WorkOrderPDFHyperLinkEdit.StyleController = this.dataLayoutControl1;
            this.WorkOrderPDFHyperLinkEdit.TabIndex = 49;
            this.WorkOrderPDFHyperLinkEdit.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.WorkOrderPDFHyperLinkEdit_OpenLink);
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(138, 137);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(969, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling1);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 47;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp07272UTWorkOrderEditLinkedMapsBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Actions to Work Order", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Remove Selected Linked Action(s) from Work Order", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Move Item Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Move Item Down", "down")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(36, 400);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEditDateTime});
            this.gridControl3.Size = new System.Drawing.Size(1071, 256);
            this.gridControl3.TabIndex = 41;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp07272UTWorkOrderEditLinkedMapsBindingSource
            // 
            this.sp07272UTWorkOrderEditLinkedMapsBindingSource.DataMember = "sp07272_UT_Work_Order_Edit_Linked_Maps";
            this.sp07272UTWorkOrderEditLinkedMapsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDateTimeCreated,
            this.colDescription,
            this.colLinkedMapID,
            this.colLinkedToRecordID,
            this.colMap,
            this.colMapOrder,
            this.colPaddedWorkOrderID3,
            this.colPictureTakenByName,
            this.colRemarks});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMapOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView3_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView3_ShowingEditor);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDateTimeCreated
            // 
            this.colDateTimeCreated.Caption = "Created On";
            this.colDateTimeCreated.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateTimeCreated.FieldName = "DateTimeCreated";
            this.colDateTimeCreated.Name = "colDateTimeCreated";
            this.colDateTimeCreated.OptionsColumn.AllowEdit = false;
            this.colDateTimeCreated.OptionsColumn.AllowFocus = false;
            this.colDateTimeCreated.OptionsColumn.ReadOnly = true;
            this.colDateTimeCreated.Visible = true;
            this.colDateTimeCreated.VisibleIndex = 3;
            this.colDateTimeCreated.Width = 77;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 288;
            // 
            // colLinkedMapID
            // 
            this.colLinkedMapID.Caption = "Linked Map ID";
            this.colLinkedMapID.FieldName = "LinkedMapID";
            this.colLinkedMapID.Name = "colLinkedMapID";
            this.colLinkedMapID.OptionsColumn.AllowEdit = false;
            this.colLinkedMapID.OptionsColumn.AllowFocus = false;
            this.colLinkedMapID.OptionsColumn.ReadOnly = true;
            this.colLinkedMapID.Width = 88;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked to Work Order ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 137;
            // 
            // colMap
            // 
            this.colMap.Caption = "Map";
            this.colMap.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colMap.FieldName = "Map";
            this.colMap.Name = "colMap";
            this.colMap.OptionsColumn.ReadOnly = true;
            this.colMap.Visible = true;
            this.colMap.VisibleIndex = 4;
            this.colMap.Width = 307;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            // 
            // colMapOrder
            // 
            this.colMapOrder.Caption = "Order";
            this.colMapOrder.FieldName = "MapOrder";
            this.colMapOrder.Name = "colMapOrder";
            this.colMapOrder.OptionsColumn.AllowEdit = false;
            this.colMapOrder.OptionsColumn.AllowFocus = false;
            this.colMapOrder.OptionsColumn.ReadOnly = true;
            this.colMapOrder.Visible = true;
            this.colMapOrder.VisibleIndex = 0;
            this.colMapOrder.Width = 62;
            // 
            // colPaddedWorkOrderID3
            // 
            this.colPaddedWorkOrderID3.Caption = "Work Order #";
            this.colPaddedWorkOrderID3.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID3.Name = "colPaddedWorkOrderID3";
            this.colPaddedWorkOrderID3.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID3.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID3.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID3.Width = 101;
            // 
            // colPictureTakenByName
            // 
            this.colPictureTakenByName.Caption = "Created By";
            this.colPictureTakenByName.FieldName = "PictureTakenByName";
            this.colPictureTakenByName.Name = "colPictureTakenByName";
            this.colPictureTakenByName.OptionsColumn.AllowEdit = false;
            this.colPictureTakenByName.OptionsColumn.AllowFocus = false;
            this.colPictureTakenByName.OptionsColumn.ReadOnly = true;
            this.colPictureTakenByName.Visible = true;
            this.colPictureTakenByName.VisibleIndex = 2;
            this.colPictureTakenByName.Width = 91;
            // 
            // colRemarks
            // 
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            this.repositoryItemMemoExEdit3.EditValueChanged += new System.EventHandler(this.repositoryItemMemoExEdit3_EditValueChanged);
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Actions to Work Order", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Remove Selected Linked Action(s) from Work Order", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(36, 400);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit2});
            this.gridControl2.Size = new System.Drawing.Size(1071, 256);
            this.gridControl2.TabIndex = 40;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07271UTWorkOrderEditLinkedSubContractorsBindingSource
            // 
            this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource.DataMember = "sp07271_UT_Work_Order_Edit_Linked_SubContractors";
            this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderTeamID,
            this.colWorkOrderID1,
            this.colSubContractorID,
            this.colRemarks1,
            this.colGUID1,
            this.colSubContractorName,
            this.colInternalContractor,
            this.colMobileTel,
            this.colTelephone1,
            this.colTelephone2,
            this.colPostcode,
            this.colPaddedWorkOrderID2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colWorkOrderTeamID
            // 
            this.colWorkOrderTeamID.Caption = "Work Order Team ID";
            this.colWorkOrderTeamID.FieldName = "WorkOrderTeamID";
            this.colWorkOrderTeamID.Name = "colWorkOrderTeamID";
            this.colWorkOrderTeamID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderTeamID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderTeamID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderTeamID.Width = 120;
            // 
            // colWorkOrderID1
            // 
            this.colWorkOrderID1.Caption = "Work Order ID";
            this.colWorkOrderID1.FieldName = "WorkOrderID";
            this.colWorkOrderID1.Name = "colWorkOrderID1";
            this.colWorkOrderID1.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID1.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID1.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID1.Width = 91;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Sub-Contractor ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            this.colSubContractorID.Width = 109;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 6;
            this.colRemarks1.Width = 210;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            this.repositoryItemMemoExEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemMemoExEdit1_EditValueChanged_1);
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Sub-Contractor Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 0;
            this.colSubContractorName.Width = 288;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal";
            this.colInternalContractor.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Visible = true;
            this.colInternalContractor.VisibleIndex = 1;
            this.colInternalContractor.Width = 61;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colMobileTel
            // 
            this.colMobileTel.Caption = "Mobile Telephone";
            this.colMobileTel.FieldName = "MobileTel";
            this.colMobileTel.Name = "colMobileTel";
            this.colMobileTel.OptionsColumn.AllowEdit = false;
            this.colMobileTel.OptionsColumn.AllowFocus = false;
            this.colMobileTel.OptionsColumn.ReadOnly = true;
            this.colMobileTel.Visible = true;
            this.colMobileTel.VisibleIndex = 2;
            this.colMobileTel.Width = 117;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Visible = true;
            this.colTelephone1.VisibleIndex = 3;
            this.colTelephone1.Width = 97;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Visible = true;
            this.colTelephone2.VisibleIndex = 4;
            this.colTelephone2.Width = 98;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 5;
            // 
            // colPaddedWorkOrderID2
            // 
            this.colPaddedWorkOrderID2.Caption = "Work Order #";
            this.colPaddedWorkOrderID2.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID2.Name = "colPaddedWorkOrderID2";
            this.colPaddedWorkOrderID2.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID2.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID2.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID2.Width = 88;
            // 
            // CreatedByTextEdit
            // 
            this.CreatedByTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "CreatedBy", true));
            this.CreatedByTextEdit.Location = new System.Drawing.Point(145, 199);
            this.CreatedByTextEdit.MenuManager = this.barManager1;
            this.CreatedByTextEdit.Name = "CreatedByTextEdit";
            this.CreatedByTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByTextEdit, true);
            this.CreatedByTextEdit.Size = new System.Drawing.Size(950, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByTextEdit, optionsSpelling2);
            this.CreatedByTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByTextEdit.TabIndex = 48;
            // 
            // TotalCostTextEdit
            // 
            this.TotalCostTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "TotalCost", true));
            this.TotalCostTextEdit.Location = new System.Drawing.Point(145, 247);
            this.TotalCostTextEdit.MenuManager = this.barManager1;
            this.TotalCostTextEdit.Name = "TotalCostTextEdit";
            this.TotalCostTextEdit.Properties.Mask.EditMask = "c";
            this.TotalCostTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalCostTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalCostTextEdit, true);
            this.TotalCostTextEdit.Size = new System.Drawing.Size(950, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalCostTextEdit, optionsSpelling3);
            this.TotalCostTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostTextEdit.TabIndex = 46;
            // 
            // SubContractorIDTextEdit
            // 
            this.SubContractorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "SubContractorID", true));
            this.SubContractorIDTextEdit.Location = new System.Drawing.Point(145, 269);
            this.SubContractorIDTextEdit.MenuManager = this.barManager1;
            this.SubContractorIDTextEdit.Name = "SubContractorIDTextEdit";
            this.SubContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubContractorIDTextEdit, true);
            this.SubContractorIDTextEdit.Size = new System.Drawing.Size(950, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubContractorIDTextEdit, optionsSpelling4);
            this.SubContractorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorIDTextEdit.TabIndex = 45;
            // 
            // PaddedWorkOrderIDTextEdit
            // 
            this.PaddedWorkOrderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "PaddedWorkOrderID", true));
            this.PaddedWorkOrderIDTextEdit.Location = new System.Drawing.Point(121, 71);
            this.PaddedWorkOrderIDTextEdit.MenuManager = this.barManager1;
            this.PaddedWorkOrderIDTextEdit.Name = "PaddedWorkOrderIDTextEdit";
            this.PaddedWorkOrderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PaddedWorkOrderIDTextEdit, true);
            this.PaddedWorkOrderIDTextEdit.Size = new System.Drawing.Size(448, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PaddedWorkOrderIDTextEdit, optionsSpelling5);
            this.PaddedWorkOrderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PaddedWorkOrderIDTextEdit.TabIndex = 43;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07247UTWorkOrderEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(241, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 40;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // WorkOrderIDTextEdit
            // 
            this.WorkOrderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "WorkOrderID", true));
            this.WorkOrderIDTextEdit.Location = new System.Drawing.Point(121, 94);
            this.WorkOrderIDTextEdit.MenuManager = this.barManager1;
            this.WorkOrderIDTextEdit.Name = "WorkOrderIDTextEdit";
            this.WorkOrderIDTextEdit.Properties.Mask.EditMask = "0000000";
            this.WorkOrderIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.WorkOrderIDTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WorkOrderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkOrderIDTextEdit, true);
            this.WorkOrderIDTextEdit.Size = new System.Drawing.Size(427, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkOrderIDTextEdit, optionsSpelling6);
            this.WorkOrderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkOrderIDTextEdit.TabIndex = 4;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(36, 400);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1071, 256);
            this.gridSplitContainer1.TabIndex = 42;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Actions to Work Order", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Remove Selected Linked Action(s) from Work Order", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Move Item Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Move Item Down", "down")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemDateEdit2,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1071, 256);
            this.gridControl1.TabIndex = 39;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07249UTWorkOrderManagerLinkedActionsEditBindingSource
            // 
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource.DataMember = "sp07249_UT_Work_Order_Manager_Linked_Actions_Edit";
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colSurveyedTreeID,
            this.colJobTypeID,
            this.colReferenceNumber,
            this.colDateRaised,
            this.colDateScheduled,
            this.colDateCompleted,
            this.colApproved,
            this.colApprovedByStaffID,
            this.colEstimatedLabourCost,
            this.colActualLabourCost,
            this.colEstimatedMaterialsCost,
            this.colActualMaterialsCost,
            this.colEstimatedEquipmentCost,
            this.colActualEquipmentCost,
            this.colEstimatedTotalCost,
            this.colActualTotalCost,
            this.colWorkOrderID,
            this.colSelfBillingInvoiceID,
            this.colFinanceSystemBillingID,
            this.colRemarks2,
            this.colGUID,
            this.colLinkedRecordDescription,
            this.colJobDescription,
            this.colEstimatedTotalSell,
            this.colActualTotalSell,
            this.colEstimatedLabourSell,
            this.colActualLabourSell,
            this.colEstimatedEquipmentSell,
            this.colActualEquipmentSell,
            this.colEstimatedMaterialsSell,
            this.colActualMaterialsSell,
            this.colPaddedWorkOrderID,
            this.colWorkOrderSortOrder,
            this.colRegionName,
            this.colRegionNumber,
            this.colSubAreaName,
            this.colSubAreaNumber,
            this.colPrimaryName,
            this.colPrimaryNumber,
            this.colFeederName,
            this.colFeederNumber,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colPoleNumber,
            this.colTreeReferenceNumber,
            this.colStumpTreatmentNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentSpraying,
            this.colTreeReplacementNone,
            this.colTreeReplacementStandards,
            this.colTreeReplacementWhips,
            this.colActualHours,
            this.colEstimatedHours,
            this.colPossibleLiveWork,
            this.colTeamOnHold,
            this.colTreeSpeciesList,
            this.colHeldUpType,
            this.colIsShutdownRequired});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colWorkOrderSortOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyedTreeID
            // 
            this.colSurveyedTreeID.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID.Name = "colSurveyedTreeID";
            this.colSurveyedTreeID.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID.Width = 106;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 3;
            this.colReferenceNumber.Width = 112;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemDateEdit2;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 5;
            this.colDateRaised.Width = 99;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.Mask.EditMask = "g";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // colDateScheduled
            // 
            this.colDateScheduled.Caption = "Date Scheduled";
            this.colDateScheduled.ColumnEdit = this.repositoryItemDateEdit2;
            this.colDateScheduled.FieldName = "DateScheduled";
            this.colDateScheduled.Name = "colDateScheduled";
            this.colDateScheduled.OptionsColumn.AllowEdit = false;
            this.colDateScheduled.OptionsColumn.AllowFocus = false;
            this.colDateScheduled.OptionsColumn.ReadOnly = true;
            this.colDateScheduled.Visible = true;
            this.colDateScheduled.VisibleIndex = 6;
            this.colDateScheduled.Width = 96;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemDateEdit2;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 4;
            this.colDateCompleted.Width = 98;
            // 
            // colApproved
            // 
            this.colApproved.Caption = "Approved";
            this.colApproved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colApproved.FieldName = "Approved";
            this.colApproved.Name = "colApproved";
            this.colApproved.OptionsColumn.AllowEdit = false;
            this.colApproved.OptionsColumn.AllowFocus = false;
            this.colApproved.OptionsColumn.ReadOnly = true;
            this.colApproved.Visible = true;
            this.colApproved.VisibleIndex = 7;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colApprovedByStaffID
            // 
            this.colApprovedByStaffID.Caption = "Apporved By Staff ID";
            this.colApprovedByStaffID.FieldName = "ApprovedByStaffID";
            this.colApprovedByStaffID.Name = "colApprovedByStaffID";
            this.colApprovedByStaffID.OptionsColumn.AllowEdit = false;
            this.colApprovedByStaffID.OptionsColumn.AllowFocus = false;
            this.colApprovedByStaffID.OptionsColumn.ReadOnly = true;
            this.colApprovedByStaffID.Width = 124;
            // 
            // colEstimatedLabourCost
            // 
            this.colEstimatedLabourCost.Caption = "Est. Labour Cost";
            this.colEstimatedLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colEstimatedLabourCost.FieldName = "EstimatedLabourCost";
            this.colEstimatedLabourCost.Name = "colEstimatedLabourCost";
            this.colEstimatedLabourCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedLabourCost", "{0:c2}")});
            this.colEstimatedLabourCost.Visible = true;
            this.colEstimatedLabourCost.VisibleIndex = 14;
            this.colEstimatedLabourCost.Width = 101;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colActualLabourCost
            // 
            this.colActualLabourCost.Caption = "Act. Labour Cost";
            this.colActualLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colActualLabourCost.FieldName = "ActualLabourCost";
            this.colActualLabourCost.Name = "colActualLabourCost";
            this.colActualLabourCost.OptionsColumn.AllowEdit = false;
            this.colActualLabourCost.OptionsColumn.AllowFocus = false;
            this.colActualLabourCost.OptionsColumn.ReadOnly = true;
            this.colActualLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualLabourCost", "{0:c2}")});
            this.colActualLabourCost.Visible = true;
            this.colActualLabourCost.VisibleIndex = 15;
            this.colActualLabourCost.Width = 102;
            // 
            // colEstimatedMaterialsCost
            // 
            this.colEstimatedMaterialsCost.Caption = "Est. Material Cost";
            this.colEstimatedMaterialsCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colEstimatedMaterialsCost.FieldName = "EstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.Name = "colEstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedMaterialsCost", "{0:c2}")});
            this.colEstimatedMaterialsCost.Visible = true;
            this.colEstimatedMaterialsCost.VisibleIndex = 22;
            this.colEstimatedMaterialsCost.Width = 106;
            // 
            // colActualMaterialsCost
            // 
            this.colActualMaterialsCost.Caption = "Act. Material Cost";
            this.colActualMaterialsCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colActualMaterialsCost.FieldName = "ActualMaterialsCost";
            this.colActualMaterialsCost.Name = "colActualMaterialsCost";
            this.colActualMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualMaterialsCost", "{0:c2}")});
            this.colActualMaterialsCost.Visible = true;
            this.colActualMaterialsCost.VisibleIndex = 23;
            this.colActualMaterialsCost.Width = 107;
            // 
            // colEstimatedEquipmentCost
            // 
            this.colEstimatedEquipmentCost.Caption = "Est. Equip. Cost";
            this.colEstimatedEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colEstimatedEquipmentCost.FieldName = "EstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.Name = "colEstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedEquipmentCost", "{0:c2}")});
            this.colEstimatedEquipmentCost.Visible = true;
            this.colEstimatedEquipmentCost.VisibleIndex = 18;
            this.colEstimatedEquipmentCost.Width = 98;
            // 
            // colActualEquipmentCost
            // 
            this.colActualEquipmentCost.Caption = "Act. Equip. Cost";
            this.colActualEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colActualEquipmentCost.FieldName = "ActualEquipmentCost";
            this.colActualEquipmentCost.Name = "colActualEquipmentCost";
            this.colActualEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualEquipmentCost", "{0:c2}")});
            this.colActualEquipmentCost.Visible = true;
            this.colActualEquipmentCost.VisibleIndex = 19;
            this.colActualEquipmentCost.Width = 99;
            // 
            // colEstimatedTotalCost
            // 
            this.colEstimatedTotalCost.Caption = "Est. Total Cost";
            this.colEstimatedTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colEstimatedTotalCost.FieldName = "EstimatedTotalCost";
            this.colEstimatedTotalCost.Name = "colEstimatedTotalCost";
            this.colEstimatedTotalCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedTotalCost", "{0:c2}")});
            this.colEstimatedTotalCost.Visible = true;
            this.colEstimatedTotalCost.VisibleIndex = 26;
            this.colEstimatedTotalCost.Width = 92;
            // 
            // colActualTotalCost
            // 
            this.colActualTotalCost.Caption = "Act. Total Cost";
            this.colActualTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colActualTotalCost.FieldName = "ActualTotalCost";
            this.colActualTotalCost.Name = "colActualTotalCost";
            this.colActualTotalCost.OptionsColumn.AllowEdit = false;
            this.colActualTotalCost.OptionsColumn.AllowFocus = false;
            this.colActualTotalCost.OptionsColumn.ReadOnly = true;
            this.colActualTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualTotalCost", "{0:c2}")});
            this.colActualTotalCost.Visible = true;
            this.colActualTotalCost.VisibleIndex = 28;
            this.colActualTotalCost.Width = 93;
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.Width = 77;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 82;
            // 
            // colFinanceSystemBillingID
            // 
            this.colFinanceSystemBillingID.Caption = "Finance Billing ID";
            this.colFinanceSystemBillingID.FieldName = "FinanceSystemBillingID";
            this.colFinanceSystemBillingID.Name = "colFinanceSystemBillingID";
            this.colFinanceSystemBillingID.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemBillingID.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemBillingID.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemBillingID.Width = 101;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 8;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To Tree";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Visible = true;
            this.colLinkedRecordDescription.VisibleIndex = 1;
            this.colLinkedRecordDescription.Width = 235;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 2;
            this.colJobDescription.Width = 162;
            // 
            // colEstimatedTotalSell
            // 
            this.colEstimatedTotalSell.Caption = "Est. Total Sell";
            this.colEstimatedTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colEstimatedTotalSell.FieldName = "EstimatedTotalSell";
            this.colEstimatedTotalSell.Name = "colEstimatedTotalSell";
            this.colEstimatedTotalSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedTotalSell", "{0:c2}")});
            this.colEstimatedTotalSell.Visible = true;
            this.colEstimatedTotalSell.VisibleIndex = 27;
            this.colEstimatedTotalSell.Width = 86;
            // 
            // colActualTotalSell
            // 
            this.colActualTotalSell.Caption = "Act. Total Sell";
            this.colActualTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colActualTotalSell.FieldName = "ActualTotalSell";
            this.colActualTotalSell.Name = "colActualTotalSell";
            this.colActualTotalSell.OptionsColumn.AllowEdit = false;
            this.colActualTotalSell.OptionsColumn.AllowFocus = false;
            this.colActualTotalSell.OptionsColumn.ReadOnly = true;
            this.colActualTotalSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualTotalSell", "{0:c2}")});
            this.colActualTotalSell.Visible = true;
            this.colActualTotalSell.VisibleIndex = 29;
            this.colActualTotalSell.Width = 87;
            // 
            // colEstimatedLabourSell
            // 
            this.colEstimatedLabourSell.Caption = "Est. Labour Sell";
            this.colEstimatedLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colEstimatedLabourSell.FieldName = "EstimatedLabourSell";
            this.colEstimatedLabourSell.Name = "colEstimatedLabourSell";
            this.colEstimatedLabourSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedLabourSell", "{0:c2}")});
            this.colEstimatedLabourSell.Visible = true;
            this.colEstimatedLabourSell.VisibleIndex = 16;
            this.colEstimatedLabourSell.Width = 95;
            // 
            // colActualLabourSell
            // 
            this.colActualLabourSell.Caption = "Act. Labour Sell";
            this.colActualLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colActualLabourSell.FieldName = "ActualLabourSell";
            this.colActualLabourSell.Name = "colActualLabourSell";
            this.colActualLabourSell.OptionsColumn.AllowEdit = false;
            this.colActualLabourSell.OptionsColumn.AllowFocus = false;
            this.colActualLabourSell.OptionsColumn.ReadOnly = true;
            this.colActualLabourSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualLabourSell", "{0:c2}")});
            this.colActualLabourSell.Visible = true;
            this.colActualLabourSell.VisibleIndex = 17;
            this.colActualLabourSell.Width = 96;
            // 
            // colEstimatedEquipmentSell
            // 
            this.colEstimatedEquipmentSell.Caption = "Est. Equip. Sell";
            this.colEstimatedEquipmentSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colEstimatedEquipmentSell.FieldName = "EstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.Name = "colEstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedEquipmentSell", "{0:c2}")});
            this.colEstimatedEquipmentSell.Visible = true;
            this.colEstimatedEquipmentSell.VisibleIndex = 20;
            this.colEstimatedEquipmentSell.Width = 92;
            // 
            // colActualEquipmentSell
            // 
            this.colActualEquipmentSell.Caption = "Act. Equip. Sell";
            this.colActualEquipmentSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colActualEquipmentSell.FieldName = "ActualEquipmentSell";
            this.colActualEquipmentSell.Name = "colActualEquipmentSell";
            this.colActualEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualEquipmentSell", "{0:c2}")});
            this.colActualEquipmentSell.Visible = true;
            this.colActualEquipmentSell.VisibleIndex = 21;
            this.colActualEquipmentSell.Width = 93;
            // 
            // colEstimatedMaterialsSell
            // 
            this.colEstimatedMaterialsSell.Caption = "Est. Material Sell";
            this.colEstimatedMaterialsSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colEstimatedMaterialsSell.FieldName = "EstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.Name = "colEstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedMaterialsSell", "{0:c2}")});
            this.colEstimatedMaterialsSell.Visible = true;
            this.colEstimatedMaterialsSell.VisibleIndex = 24;
            this.colEstimatedMaterialsSell.Width = 100;
            // 
            // colActualMaterialsSell
            // 
            this.colActualMaterialsSell.Caption = "Act. Material Sell";
            this.colActualMaterialsSell.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colActualMaterialsSell.FieldName = "ActualMaterialsSell";
            this.colActualMaterialsSell.Name = "colActualMaterialsSell";
            this.colActualMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualMaterialsSell", "{0:c2}")});
            this.colActualMaterialsSell.Visible = true;
            this.colActualMaterialsSell.VisibleIndex = 25;
            this.colActualMaterialsSell.Width = 101;
            // 
            // colPaddedWorkOrderID
            // 
            this.colPaddedWorkOrderID.Caption = "Linked To Work Order";
            this.colPaddedWorkOrderID.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID.Name = "colPaddedWorkOrderID";
            this.colPaddedWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID.Width = 125;
            // 
            // colWorkOrderSortOrder
            // 
            this.colWorkOrderSortOrder.Caption = "Order";
            this.colWorkOrderSortOrder.FieldName = "WorkOrderSortOrder";
            this.colWorkOrderSortOrder.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colWorkOrderSortOrder.Name = "colWorkOrderSortOrder";
            this.colWorkOrderSortOrder.OptionsColumn.AllowEdit = false;
            this.colWorkOrderSortOrder.OptionsColumn.AllowFocus = false;
            this.colWorkOrderSortOrder.OptionsColumn.ReadOnly = true;
            this.colWorkOrderSortOrder.Visible = true;
            this.colWorkOrderSortOrder.VisibleIndex = 0;
            this.colWorkOrderSortOrder.Width = 62;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 30;
            this.colRegionName.Width = 125;
            // 
            // colRegionNumber
            // 
            this.colRegionNumber.Caption = "Region #";
            this.colRegionNumber.FieldName = "RegionNumber";
            this.colRegionNumber.Name = "colRegionNumber";
            this.colRegionNumber.OptionsColumn.AllowEdit = false;
            this.colRegionNumber.OptionsColumn.AllowFocus = false;
            this.colRegionNumber.OptionsColumn.ReadOnly = true;
            this.colRegionNumber.Visible = true;
            this.colRegionNumber.VisibleIndex = 31;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 32;
            this.colSubAreaName.Width = 125;
            // 
            // colSubAreaNumber
            // 
            this.colSubAreaNumber.Caption = "Sub-Area #";
            this.colSubAreaNumber.FieldName = "SubAreaNumber";
            this.colSubAreaNumber.Name = "colSubAreaNumber";
            this.colSubAreaNumber.OptionsColumn.AllowEdit = false;
            this.colSubAreaNumber.OptionsColumn.AllowFocus = false;
            this.colSubAreaNumber.OptionsColumn.ReadOnly = true;
            this.colSubAreaNumber.Visible = true;
            this.colSubAreaNumber.VisibleIndex = 33;
            this.colSubAreaNumber.Width = 77;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 34;
            this.colPrimaryName.Width = 125;
            // 
            // colPrimaryNumber
            // 
            this.colPrimaryNumber.Caption = "Primary #";
            this.colPrimaryNumber.FieldName = "PrimaryNumber";
            this.colPrimaryNumber.Name = "colPrimaryNumber";
            this.colPrimaryNumber.OptionsColumn.AllowEdit = false;
            this.colPrimaryNumber.OptionsColumn.AllowFocus = false;
            this.colPrimaryNumber.OptionsColumn.ReadOnly = true;
            this.colPrimaryNumber.Visible = true;
            this.colPrimaryNumber.VisibleIndex = 35;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 36;
            this.colFeederName.Width = 125;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder #";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.Visible = true;
            this.colFeederNumber.VisibleIndex = 37;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 38;
            this.colCircuitName.Width = 125;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit #";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 39;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole #";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 40;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Tree Ref #";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 41;
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.Visible = true;
            this.colStumpTreatmentNone.VisibleIndex = 43;
            this.colStumpTreatmentNone.Width = 139;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatment - Eco Plugs";
            this.colStumpTreatmentEcoPlugs.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.Visible = true;
            this.colStumpTreatmentEcoPlugs.VisibleIndex = 44;
            this.colStumpTreatmentEcoPlugs.Width = 159;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.colStumpTreatmentPaint.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.Visible = true;
            this.colStumpTreatmentPaint.VisibleIndex = 45;
            this.colStumpTreatmentPaint.Width = 138;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.colStumpTreatmentSpraying.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.Visible = true;
            this.colStumpTreatmentSpraying.VisibleIndex = 46;
            this.colStumpTreatmentSpraying.Width = 156;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement - None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.Visible = true;
            this.colTreeReplacementNone.VisibleIndex = 47;
            this.colTreeReplacementNone.Width = 143;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.Visible = true;
            this.colTreeReplacementStandards.VisibleIndex = 48;
            this.colTreeReplacementStandards.Width = 167;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.Visible = true;
            this.colTreeReplacementWhips.VisibleIndex = 49;
            this.colTreeReplacementWhips.Width = 147;
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualHours", "{0:######0.00 Hours}")});
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 13;
            this.colActualHours.Width = 102;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedHours", "{0:######0.00 Hours}")});
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 12;
            this.colEstimatedHours.Width = 102;
            // 
            // colPossibleLiveWork
            // 
            this.colPossibleLiveWork.Caption = "Possible Live Work";
            this.colPossibleLiveWork.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork.Name = "colPossibleLiveWork";
            this.colPossibleLiveWork.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork.Visible = true;
            this.colPossibleLiveWork.VisibleIndex = 11;
            this.colPossibleLiveWork.Width = 109;
            // 
            // colTeamOnHold
            // 
            this.colTeamOnHold.Caption = "Team On-Hold";
            this.colTeamOnHold.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTeamOnHold.FieldName = "TeamOnHold";
            this.colTeamOnHold.Name = "colTeamOnHold";
            this.colTeamOnHold.Width = 87;
            // 
            // colTreeSpeciesList
            // 
            this.colTreeSpeciesList.Caption = "Tree Species";
            this.colTreeSpeciesList.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colTreeSpeciesList.FieldName = "TreeSpeciesList";
            this.colTreeSpeciesList.Name = "colTreeSpeciesList";
            this.colTreeSpeciesList.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesList.Visible = true;
            this.colTreeSpeciesList.VisibleIndex = 42;
            this.colTreeSpeciesList.Width = 80;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.Caption = "Held-Up Type";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 9;
            this.colHeldUpType.Width = 96;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 10;
            this.colIsShutdownRequired.Width = 113;
            // 
            // DateSubContractorSentDateEdit
            // 
            this.DateSubContractorSentDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "DateSubContractorSent", true));
            this.DateSubContractorSentDateEdit.EditValue = null;
            this.DateSubContractorSentDateEdit.Location = new System.Drawing.Point(145, 199);
            this.DateSubContractorSentDateEdit.MenuManager = this.barManager1;
            this.DateSubContractorSentDateEdit.Name = "DateSubContractorSentDateEdit";
            this.DateSubContractorSentDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateSubContractorSentDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateSubContractorSentDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateSubContractorSentDateEdit.Properties.Mask.EditMask = "g";
            this.DateSubContractorSentDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateSubContractorSentDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateSubContractorSentDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateSubContractorSentDateEdit.Size = new System.Drawing.Size(950, 20);
            this.DateSubContractorSentDateEdit.StyleController = this.dataLayoutControl1;
            this.DateSubContractorSentDateEdit.TabIndex = 9;
            // 
            // DateCompletedDateEdit
            // 
            this.DateCompletedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "DateCompleted", true));
            this.DateCompletedDateEdit.EditValue = null;
            this.DateCompletedDateEdit.Location = new System.Drawing.Point(145, 223);
            this.DateCompletedDateEdit.MenuManager = this.barManager1;
            this.DateCompletedDateEdit.Name = "DateCompletedDateEdit";
            this.DateCompletedDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateCompletedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateCompletedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateCompletedDateEdit.Properties.Mask.EditMask = "g";
            this.DateCompletedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateCompletedDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateCompletedDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateCompletedDateEdit.Size = new System.Drawing.Size(950, 20);
            this.DateCompletedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateCompletedDateEdit.TabIndex = 24;
            this.DateCompletedDateEdit.EditValueChanged += new System.EventHandler(this.DateCompletedDateEdit_EditValueChanged);
            this.DateCompletedDateEdit.Validated += new System.EventHandler(this.DateCompletedDateEdit_Validated);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(48, 175);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(1047, 116);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling7);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 26;
            // 
            // ReferenceNumberButtonEdit
            // 
            this.ReferenceNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "ReferenceNumber", true));
            this.ReferenceNumberButtonEdit.Location = new System.Drawing.Point(670, 71);
            this.ReferenceNumberButtonEdit.MenuManager = this.barManager1;
            this.ReferenceNumberButtonEdit.Name = "ReferenceNumberButtonEdit";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem4.Text = "Sequence Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem5.Text = "Number Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.ReferenceNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Sequence", superToolTip4, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "Number", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ReferenceNumberButtonEdit.Properties.MaxLength = 20;
            this.ReferenceNumberButtonEdit.Size = new System.Drawing.Size(449, 20);
            this.ReferenceNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.ReferenceNumberButtonEdit.TabIndex = 31;
            this.ReferenceNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ReferenceNumberButtonEdit_ButtonClick);
            // 
            // DateCreatedDateEdit
            // 
            this.DateCreatedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "DateCreated", true));
            this.DateCreatedDateEdit.EditValue = null;
            this.DateCreatedDateEdit.Location = new System.Drawing.Point(145, 175);
            this.DateCreatedDateEdit.MenuManager = this.barManager1;
            this.DateCreatedDateEdit.Name = "DateCreatedDateEdit";
            this.DateCreatedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateCreatedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateCreatedDateEdit.Properties.Mask.EditMask = "g";
            this.DateCreatedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateCreatedDateEdit.Properties.ReadOnly = true;
            this.DateCreatedDateEdit.Size = new System.Drawing.Size(950, 20);
            this.DateCreatedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateCreatedDateEdit.TabIndex = 36;
            // 
            // ItemForWorkOrderID
            // 
            this.ItemForWorkOrderID.Control = this.WorkOrderIDTextEdit;
            this.ItemForWorkOrderID.CustomizationFormText = "Work Order ID:";
            this.ItemForWorkOrderID.Location = new System.Drawing.Point(0, 24);
            this.ItemForWorkOrderID.Name = "ItemForWorkOrderID";
            this.ItemForWorkOrderID.Size = new System.Drawing.Size(528, 24);
            this.ItemForWorkOrderID.Text = "Work Order ID:";
            this.ItemForWorkOrderID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForSubContractorID
            // 
            this.ItemForSubContractorID.Control = this.SubContractorIDTextEdit;
            this.ItemForSubContractorID.CustomizationFormText = "SubContractor ID:";
            this.ItemForSubContractorID.Location = new System.Drawing.Point(0, 96);
            this.ItemForSubContractorID.Name = "ItemForSubContractorID";
            this.ItemForSubContractorID.Size = new System.Drawing.Size(1051, 24);
            this.ItemForSubContractorID.Text = "SubContractor ID:";
            this.ItemForSubContractorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.CustomizationFormText = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 0);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(1075, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1143, 692);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup13,
            this.layoutControlItem2,
            this.emptySpaceItem3,
            this.simpleSeparator6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1123, 319);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Work Order Header";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup15,
            this.emptySpaceItem2,
            this.ItemForPaddedWorkOrderID,
            this.ItemForReferenceNumber});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 25);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(1123, 294);
            this.layoutControlGroup13.Text = "Work Order Header";
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.CustomizationFormText = "Details";
            this.layoutControlGroup15.ExpandButtonVisible = true;
            this.layoutControlGroup15.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 34);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(1099, 214);
            this.layoutControlGroup15.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup16;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1075, 168);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup16,
            this.layoutControlGroup10,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.CustomizationFormText = "Details";
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDateSubContractorSent,
            this.ItemForDateCompleted,
            this.ItemForTotalCost,
            this.ItemForWorkOrderPDF,
            this.ItemForStatusID});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Size = new System.Drawing.Size(1051, 120);
            this.layoutControlGroup16.Text = "Details";
            // 
            // ItemForDateSubContractorSent
            // 
            this.ItemForDateSubContractorSent.Control = this.DateSubContractorSentDateEdit;
            this.ItemForDateSubContractorSent.CustomizationFormText = "Issue Date:";
            this.ItemForDateSubContractorSent.Location = new System.Drawing.Point(0, 24);
            this.ItemForDateSubContractorSent.Name = "ItemForDateSubContractorSent";
            this.ItemForDateSubContractorSent.Size = new System.Drawing.Size(1051, 24);
            this.ItemForDateSubContractorSent.Text = "Issue Date:";
            this.ItemForDateSubContractorSent.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForDateCompleted
            // 
            this.ItemForDateCompleted.Control = this.DateCompletedDateEdit;
            this.ItemForDateCompleted.CustomizationFormText = "Completed Date:";
            this.ItemForDateCompleted.Location = new System.Drawing.Point(0, 48);
            this.ItemForDateCompleted.Name = "ItemForDateCompleted";
            this.ItemForDateCompleted.Size = new System.Drawing.Size(1051, 24);
            this.ItemForDateCompleted.Text = "Completed Date:";
            this.ItemForDateCompleted.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForTotalCost
            // 
            this.ItemForTotalCost.Control = this.TotalCostTextEdit;
            this.ItemForTotalCost.CustomizationFormText = "Total Cost:";
            this.ItemForTotalCost.Location = new System.Drawing.Point(0, 72);
            this.ItemForTotalCost.Name = "ItemForTotalCost";
            this.ItemForTotalCost.Size = new System.Drawing.Size(1051, 24);
            this.ItemForTotalCost.Text = "Total Cost:";
            this.ItemForTotalCost.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForWorkOrderPDF
            // 
            this.ItemForWorkOrderPDF.Control = this.WorkOrderPDFHyperLinkEdit;
            this.ItemForWorkOrderPDF.CustomizationFormText = "Work Order PDF:";
            this.ItemForWorkOrderPDF.Location = new System.Drawing.Point(0, 96);
            this.ItemForWorkOrderPDF.Name = "ItemForWorkOrderPDF";
            this.ItemForWorkOrderPDF.Size = new System.Drawing.Size(1051, 24);
            this.ItemForWorkOrderPDF.Text = "Work Order PDF:";
            this.ItemForWorkOrderPDF.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDGridLookUpEdit;
            this.ItemForStatusID.CustomizationFormText = "Status:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 0);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(1051, 24);
            this.ItemForStatusID.Text = "Status:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup10.CaptionImageOptions.Image")));
            this.layoutControlGroup10.CustomizationFormText = "Remarks";
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(1051, 120);
            this.layoutControlGroup10.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(1051, 120);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Creation Info";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDateCreated,
            this.ItemforCreatedBy,
            this.emptySpaceItem1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1051, 120);
            this.layoutControlGroup4.Text = "Creation Info";
            // 
            // ItemForDateCreated
            // 
            this.ItemForDateCreated.Control = this.DateCreatedDateEdit;
            this.ItemForDateCreated.CustomizationFormText = "Date Created:";
            this.ItemForDateCreated.Location = new System.Drawing.Point(0, 0);
            this.ItemForDateCreated.Name = "ItemForDateCreated";
            this.ItemForDateCreated.Size = new System.Drawing.Size(1051, 24);
            this.ItemForDateCreated.Text = "Date Created:";
            this.ItemForDateCreated.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemforCreatedBy
            // 
            this.ItemforCreatedBy.Control = this.CreatedByTextEdit;
            this.ItemforCreatedBy.CustomizationFormText = "Created By:";
            this.ItemforCreatedBy.Location = new System.Drawing.Point(0, 24);
            this.ItemforCreatedBy.Name = "ItemforCreatedBy";
            this.ItemforCreatedBy.Size = new System.Drawing.Size(1051, 24);
            this.ItemforCreatedBy.Text = "Created By:";
            this.ItemforCreatedBy.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1051, 72);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1099, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPaddedWorkOrderID
            // 
            this.ItemForPaddedWorkOrderID.Control = this.PaddedWorkOrderIDTextEdit;
            this.ItemForPaddedWorkOrderID.CustomizationFormText = "Work Order #:";
            this.ItemForPaddedWorkOrderID.Location = new System.Drawing.Point(0, 0);
            this.ItemForPaddedWorkOrderID.Name = "ItemForPaddedWorkOrderID";
            this.ItemForPaddedWorkOrderID.Size = new System.Drawing.Size(549, 24);
            this.ItemForPaddedWorkOrderID.Text = "Work Order #:";
            this.ItemForPaddedWorkOrderID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForReferenceNumber
            // 
            this.ItemForReferenceNumber.Control = this.ReferenceNumberButtonEdit;
            this.ItemForReferenceNumber.CustomizationFormText = "Reference Number:";
            this.ItemForReferenceNumber.Location = new System.Drawing.Point(549, 0);
            this.ItemForReferenceNumber.Name = "ItemForReferenceNumber";
            this.ItemForReferenceNumber.Size = new System.Drawing.Size(550, 24);
            this.ItemForReferenceNumber.Text = "Reference Number:";
            this.ItemForReferenceNumber.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dataNavigator1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(245, 23);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(245, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(878, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator6
            // 
            this.simpleSeparator6.AllowHotTrack = false;
            this.simpleSeparator6.CustomizationFormText = "simpleSeparator6";
            this.simpleSeparator6.Location = new System.Drawing.Point(0, 23);
            this.simpleSeparator6.Name = "simpleSeparator6";
            this.simpleSeparator6.Size = new System.Drawing.Size(1123, 2);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup12,
            this.simpleSeparator1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 319);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1123, 353);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Work";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 2);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(1123, 351);
            this.layoutControlGroup12.Text = "Linked Records";
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(1099, 305);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Work";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1075, 260);
            this.layoutControlGroup5.Text = "Work";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridSplitContainer1;
            this.layoutControlItem1.CustomizationFormText = "Work Grid:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1075, 260);
            this.layoutControlItem1.Text = "Work Grid:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Contractors";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1075, 260);
            this.layoutControlGroup6.Text = "Sub-Contractors";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl2;
            this.layoutControlItem3.CustomizationFormText = "Contractor Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1075, 260);
            this.layoutControlItem3.Text = "Contractor Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Work Maps";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1075, 260);
            this.layoutControlGroup7.Text = "Work Maps";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl3;
            this.layoutControlItem4.CustomizationFormText = "Work Maps Grid:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1075, 260);
            this.layoutControlItem4.Text = "Work Maps Grid:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Permission Documents";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1075, 260);
            this.layoutControlGroup8.Text = "Permission Documents";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl4;
            this.layoutControlItem5.CustomizationFormText = "Permission Document Grid:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1075, 260);
            this.layoutControlItem5.Text = "Permission Document Grid:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(1123, 2);
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07247_UT_Work_Order_EditTableAdapter
            // 
            this.sp07247_UT_Work_Order_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter
            // 
            this.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter
            // 
            this.sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter
            // 
            this.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07318_UT_Work_Order_Status_ListTableAdapter
            // 
            this.sp07318_UT_Work_Order_Status_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter
            // 
            this.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // frm_UT_WorkOrder_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1143, 748);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_WorkOrder_Edit";
            this.Text = "Edit Utilities Work Order";
            this.Activated += new System.EventHandler(this.frm_UT_WorkOrder_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_WorkOrder_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_WorkOrder_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07247UTWorkOrderEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07318UTWorkOrderStatusListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderPDFHyperLinkEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07272UTWorkOrderEditLinkedMapsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaddedWorkOrderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateSubContractorSentDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateSubContractorSentDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateSubContractorSent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCompleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaddedWorkOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit WorkOrderIDTextEdit;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private DevExpress.XtraEditors.DateEdit DateSubContractorSentDateEdit;
        private DevExpress.XtraEditors.DateEdit DateCompletedDateEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit ReferenceNumberButtonEdit;
        private DevExpress.XtraEditors.DateEdit DateCreatedDateEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkOrderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateSubContractorSent;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateCompleted;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReferenceNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateCreated;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator6;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private WoodPlanDataSet woodPlanDataSet;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource sp07247UTWorkOrderEditBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07247_UT_Work_Order_EditTableAdapter sp07247_UT_Work_Order_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit PaddedWorkOrderIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPaddedWorkOrderID;
        private DevExpress.XtraEditors.TextEdit SubContractorIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorID;
        private DevExpress.XtraEditors.TextEdit TotalCostTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCost;
        private DevExpress.XtraEditors.TextEdit CreatedByTextEdit;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemforCreatedBy;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.BindingSource sp07249UTWorkOrderManagerLinkedActionsEditBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colDateScheduled;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colApproved;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemBillingID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private System.Windows.Forms.BindingSource sp07272UTWorkOrderEditLinkedMapsBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource sp07271UTWorkOrderEditLinkedSubContractorsBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DataSet_UT_WorkOrderTableAdapters.sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter;
        private DataSet_UT_WorkOrderTableAdapters.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderTeamID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTel;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colMap;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colMapOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureTakenByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderSortOrder;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.HyperLinkEdit WorkOrderPDFHyperLinkEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkOrderPDF;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private System.Windows.Forms.BindingSource sp07318UTWorkOrderStatusListBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07318_UT_Work_Order_Status_ListTableAdapter sp07318_UT_Work_Order_Status_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDFFile;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionEmailed;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToCustomerDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colLandownerName;
        private DataSet_UT_WorkOrderTableAdapters.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryScreenName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamOnHold;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesList;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
    }
}

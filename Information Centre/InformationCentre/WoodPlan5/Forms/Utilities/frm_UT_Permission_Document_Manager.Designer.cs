namespace WoodPlan5
{
    partial class frm_UT_Permission_Document_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Permission_Document_Manager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07191UTPermissionDocumentManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPermissionDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRaisedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSignatureFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colPDFFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailedToClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailedToCustomerDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSignatureDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmergencyAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestTelephonePoint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestAandE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHotGloveAccessAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colAccessAgreedWithLandOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessDetailsOnMap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadsideAccessOnly = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPOTree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningConservation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWildlifeDesignation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecialInstructions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimarySubName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimarySubNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLVSubName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLVSubNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagedUnitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnvironmentalRANumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandscapeImpactNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionEmailed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostageRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerSalutation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsChipRemove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsChipOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsStackOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToDoActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryScreenName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotPermissionedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandownerRestrictedCut1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric0DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlSurveyors = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp07329UTSurveyorListNoBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Reporting = new WoodPlan5.DataSet_UT_Reporting();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDisplayName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnSurveyorOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07392UTPDLinkedWorkBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionPermissionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLandOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionDocumentDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateMask = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colESQCRCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55Category = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAchievableClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandOwnerRestrictedCut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefusalReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefusalRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Mapping = new WoodPlan5.DataSet_UT_Mapping();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.popupContainerDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07400UTPDUnpermissionedWorkBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDateScheduled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colApprovedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemBillingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleFlail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualHours1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEstimatedHours1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter = new WoodPlan5.DataSet_UT_MappingTableAdapters.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter();
            this.sp07329_UT_Surveyor_List_No_BlankTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07329_UT_Surveyor_List_No_BlankTableAdapter();
            this.sp07191_UT_Permission_Document_ManagerTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07191_UT_Permission_Document_ManagerTableAdapter();
            this.sp07392_UT_PD_Linked_WorkTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07392_UT_PD_Linked_WorkTableAdapter();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07400_UT_PD_Unpermissioned_WorkTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07400_UT_PD_Unpermissioned_WorkTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07191UTPermissionDocumentManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSurveyors)).BeginInit();
            this.popupContainerControlSurveyors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07329UTSurveyorListNoBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07392UTPDLinkedWorkBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateMask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07400UTPDUnpermissionedWorkBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1041, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 542);
            this.barDockControlBottom.Size = new System.Drawing.Size(1041, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 508);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1041, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 508);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.barEditItem1,
            this.popupContainerDateRange,
            this.popupContainerEdit2});
            this.barManager1.MaxItemId = 36;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemDateEdit1,
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemPopupContainerEdit3});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 52;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07191UTPermissionDocumentManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditNumeric0DP,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1036, 242);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07191UTPermissionDocumentManagerBindingSource
            // 
            this.sp07191UTPermissionDocumentManagerBindingSource.DataMember = "sp07191_UT_Permission_Document_Manager";
            this.sp07191UTPermissionDocumentManagerBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(6, "sort2_16.png");
            this.imageCollection1.Images.SetKeyName(7, "defaults_16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPermissionDocumentID,
            this.colDateRaised,
            this.colRaisedByID,
            this.colSignatureFile,
            this.colPDFFile,
            this.colEmailedToClientDate,
            this.colEmailedToCustomerDate,
            this.colRemarks,
            this.colSignatureDate,
            this.colEmergencyAccess,
            this.colNearestTelephonePoint,
            this.colGridReference,
            this.colNearestAandE,
            this.colHotGloveAccessAvailable,
            this.colAccessAgreedWithLandOwner,
            this.colAccessDetailsOnMap,
            this.colRoadsideAccessOnly,
            this.colEstimatedRevisitDate,
            this.colTPOTree,
            this.colPlanningConservation,
            this.colWildlifeDesignation,
            this.colSpecialInstructions,
            this.colPrimarySubName,
            this.colPrimarySubNumber,
            this.colFeederName,
            this.colFeederNumber,
            this.colLineNumber,
            this.colLVSubName,
            this.colLVSubNumber,
            this.colManagedUnitNumber,
            this.colEnvironmentalRANumber,
            this.colLandscapeImpactNumber,
            this.colSiteGridReference,
            this.colPermissionEmailed,
            this.colPostageRequired,
            this.colSentByPost,
            this.colSentByStaffID,
            this.colReferenceNumber,
            this.colOwnerName,
            this.colOwnerSalutation,
            this.colOwnerAddress,
            this.colOwnerPostcode,
            this.colOwnerTelephone,
            this.colOwnerEmail,
            this.colOwnerTypeID,
            this.colSiteAddress,
            this.colContractor,
            this.colArisingsChipRemove,
            this.colArisingsChipOnSite,
            this.colArisingsStackOnSite,
            this.colArisingsOther,
            this.colClientID,
            this.colRaisedByName,
            this.colSentByStaffName,
            this.colLinkedActionCount,
            this.colLinkedToDoActionCount,
            this.colOwnerType,
            this.colClientName,
            this.colDataEntryScreenName,
            this.colReportLayoutName,
            this.colNotPermissionedCount,
            this.colOnHoldCount,
            this.colPermissionedCount,
            this.colCreatedByStaffID,
            this.colLandownerRestrictedCut1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colPermissionDocumentID
            // 
            this.colPermissionDocumentID.Caption = "Permission Document ID";
            this.colPermissionDocumentID.FieldName = "PermissionDocumentID";
            this.colPermissionDocumentID.Name = "colPermissionDocumentID";
            this.colPermissionDocumentID.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentID.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentID.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentID.Width = 136;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 0;
            this.colDateRaised.Width = 108;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colRaisedByID
            // 
            this.colRaisedByID.Caption = "Raised By ID";
            this.colRaisedByID.FieldName = "RaisedByID";
            this.colRaisedByID.Name = "colRaisedByID";
            this.colRaisedByID.OptionsColumn.AllowEdit = false;
            this.colRaisedByID.OptionsColumn.AllowFocus = false;
            this.colRaisedByID.OptionsColumn.ReadOnly = true;
            this.colRaisedByID.Width = 82;
            // 
            // colSignatureFile
            // 
            this.colSignatureFile.Caption = "Signature File";
            this.colSignatureFile.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colSignatureFile.FieldName = "SignatureFile";
            this.colSignatureFile.Name = "colSignatureFile";
            this.colSignatureFile.OptionsColumn.ReadOnly = true;
            this.colSignatureFile.Visible = true;
            this.colSignatureFile.VisibleIndex = 12;
            this.colSignatureFile.Width = 106;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colPDFFile
            // 
            this.colPDFFile.Caption = "PDF File";
            this.colPDFFile.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colPDFFile.FieldName = "PDFFile";
            this.colPDFFile.Name = "colPDFFile";
            this.colPDFFile.OptionsColumn.ReadOnly = true;
            this.colPDFFile.Visible = true;
            this.colPDFFile.VisibleIndex = 13;
            this.colPDFFile.Width = 118;
            // 
            // colEmailedToClientDate
            // 
            this.colEmailedToClientDate.Caption = "Email to Client Date";
            this.colEmailedToClientDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEmailedToClientDate.FieldName = "EmailedToClientDate";
            this.colEmailedToClientDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEmailedToClientDate.Name = "colEmailedToClientDate";
            this.colEmailedToClientDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToClientDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToClientDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToClientDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEmailedToClientDate.Visible = true;
            this.colEmailedToClientDate.VisibleIndex = 19;
            this.colEmailedToClientDate.Width = 114;
            // 
            // colEmailedToCustomerDate
            // 
            this.colEmailedToCustomerDate.Caption = "Emailed To Customer Date";
            this.colEmailedToCustomerDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEmailedToCustomerDate.FieldName = "EmailedToCustomerDate";
            this.colEmailedToCustomerDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEmailedToCustomerDate.Name = "colEmailedToCustomerDate";
            this.colEmailedToCustomerDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToCustomerDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToCustomerDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToCustomerDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEmailedToCustomerDate.Visible = true;
            this.colEmailedToCustomerDate.VisibleIndex = 20;
            this.colEmailedToCustomerDate.Width = 147;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 21;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colSignatureDate
            // 
            this.colSignatureDate.Caption = "Signature Date";
            this.colSignatureDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSignatureDate.FieldName = "SignatureDate";
            this.colSignatureDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSignatureDate.Name = "colSignatureDate";
            this.colSignatureDate.OptionsColumn.AllowEdit = false;
            this.colSignatureDate.OptionsColumn.AllowFocus = false;
            this.colSignatureDate.OptionsColumn.ReadOnly = true;
            this.colSignatureDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSignatureDate.Width = 93;
            // 
            // colEmergencyAccess
            // 
            this.colEmergencyAccess.Caption = "Emergency Date";
            this.colEmergencyAccess.FieldName = "EmergencyAccess";
            this.colEmergencyAccess.Name = "colEmergencyAccess";
            this.colEmergencyAccess.OptionsColumn.AllowEdit = false;
            this.colEmergencyAccess.OptionsColumn.AllowFocus = false;
            this.colEmergencyAccess.OptionsColumn.ReadOnly = true;
            this.colEmergencyAccess.Visible = true;
            this.colEmergencyAccess.VisibleIndex = 22;
            this.colEmergencyAccess.Width = 100;
            // 
            // colNearestTelephonePoint
            // 
            this.colNearestTelephonePoint.Caption = "Nearest Telephone Point";
            this.colNearestTelephonePoint.FieldName = "NearestTelephonePoint";
            this.colNearestTelephonePoint.Name = "colNearestTelephonePoint";
            this.colNearestTelephonePoint.OptionsColumn.AllowEdit = false;
            this.colNearestTelephonePoint.OptionsColumn.AllowFocus = false;
            this.colNearestTelephonePoint.OptionsColumn.ReadOnly = true;
            this.colNearestTelephonePoint.Visible = true;
            this.colNearestTelephonePoint.VisibleIndex = 23;
            this.colNearestTelephonePoint.Width = 139;
            // 
            // colGridReference
            // 
            this.colGridReference.Caption = "Grid Reference";
            this.colGridReference.FieldName = "GridReference";
            this.colGridReference.Name = "colGridReference";
            this.colGridReference.OptionsColumn.AllowEdit = false;
            this.colGridReference.OptionsColumn.AllowFocus = false;
            this.colGridReference.OptionsColumn.ReadOnly = true;
            this.colGridReference.Visible = true;
            this.colGridReference.VisibleIndex = 24;
            this.colGridReference.Width = 93;
            // 
            // colNearestAandE
            // 
            this.colNearestAandE.Caption = "Nearest A & E";
            this.colNearestAandE.FieldName = "NearestAandE";
            this.colNearestAandE.Name = "colNearestAandE";
            this.colNearestAandE.OptionsColumn.AllowEdit = false;
            this.colNearestAandE.OptionsColumn.AllowFocus = false;
            this.colNearestAandE.OptionsColumn.ReadOnly = true;
            this.colNearestAandE.Visible = true;
            this.colNearestAandE.VisibleIndex = 25;
            this.colNearestAandE.Width = 107;
            // 
            // colHotGloveAccessAvailable
            // 
            this.colHotGloveAccessAvailable.Caption = "Hot Glove Access Available";
            this.colHotGloveAccessAvailable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colHotGloveAccessAvailable.FieldName = "HotGloveAccessAvailable";
            this.colHotGloveAccessAvailable.Name = "colHotGloveAccessAvailable";
            this.colHotGloveAccessAvailable.OptionsColumn.AllowEdit = false;
            this.colHotGloveAccessAvailable.OptionsColumn.AllowFocus = false;
            this.colHotGloveAccessAvailable.OptionsColumn.ReadOnly = true;
            this.colHotGloveAccessAvailable.Visible = true;
            this.colHotGloveAccessAvailable.VisibleIndex = 26;
            this.colHotGloveAccessAvailable.Width = 150;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colAccessAgreedWithLandOwner
            // 
            this.colAccessAgreedWithLandOwner.Caption = "Access Agreed with Landowner";
            this.colAccessAgreedWithLandOwner.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAccessAgreedWithLandOwner.FieldName = "AccessAgreedWithLandOwner";
            this.colAccessAgreedWithLandOwner.Name = "colAccessAgreedWithLandOwner";
            this.colAccessAgreedWithLandOwner.OptionsColumn.AllowEdit = false;
            this.colAccessAgreedWithLandOwner.OptionsColumn.AllowFocus = false;
            this.colAccessAgreedWithLandOwner.OptionsColumn.ReadOnly = true;
            this.colAccessAgreedWithLandOwner.Visible = true;
            this.colAccessAgreedWithLandOwner.VisibleIndex = 28;
            this.colAccessAgreedWithLandOwner.Width = 171;
            // 
            // colAccessDetailsOnMap
            // 
            this.colAccessDetailsOnMap.Caption = "Access Details On Map";
            this.colAccessDetailsOnMap.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAccessDetailsOnMap.FieldName = "AccessDetailsOnMap";
            this.colAccessDetailsOnMap.Name = "colAccessDetailsOnMap";
            this.colAccessDetailsOnMap.OptionsColumn.AllowEdit = false;
            this.colAccessDetailsOnMap.OptionsColumn.AllowFocus = false;
            this.colAccessDetailsOnMap.OptionsColumn.ReadOnly = true;
            this.colAccessDetailsOnMap.Visible = true;
            this.colAccessDetailsOnMap.VisibleIndex = 29;
            this.colAccessDetailsOnMap.Width = 129;
            // 
            // colRoadsideAccessOnly
            // 
            this.colRoadsideAccessOnly.Caption = "Roadside Access Only";
            this.colRoadsideAccessOnly.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRoadsideAccessOnly.FieldName = "RoadsideAccessOnly";
            this.colRoadsideAccessOnly.Name = "colRoadsideAccessOnly";
            this.colRoadsideAccessOnly.OptionsColumn.AllowEdit = false;
            this.colRoadsideAccessOnly.OptionsColumn.AllowFocus = false;
            this.colRoadsideAccessOnly.OptionsColumn.ReadOnly = true;
            this.colRoadsideAccessOnly.Visible = true;
            this.colRoadsideAccessOnly.VisibleIndex = 30;
            this.colRoadsideAccessOnly.Width = 126;
            // 
            // colEstimatedRevisitDate
            // 
            this.colEstimatedRevisitDate.Caption = "Estimated Revisit Date";
            this.colEstimatedRevisitDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEstimatedRevisitDate.FieldName = "EstimatedRevisitDate";
            this.colEstimatedRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEstimatedRevisitDate.Name = "colEstimatedRevisitDate";
            this.colEstimatedRevisitDate.OptionsColumn.AllowEdit = false;
            this.colEstimatedRevisitDate.OptionsColumn.AllowFocus = false;
            this.colEstimatedRevisitDate.OptionsColumn.ReadOnly = true;
            this.colEstimatedRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEstimatedRevisitDate.Visible = true;
            this.colEstimatedRevisitDate.VisibleIndex = 31;
            this.colEstimatedRevisitDate.Width = 129;
            // 
            // colTPOTree
            // 
            this.colTPOTree.Caption = "TPO Tree";
            this.colTPOTree.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTPOTree.FieldName = "TPOTree";
            this.colTPOTree.Name = "colTPOTree";
            this.colTPOTree.OptionsColumn.AllowEdit = false;
            this.colTPOTree.OptionsColumn.AllowFocus = false;
            this.colTPOTree.OptionsColumn.ReadOnly = true;
            this.colTPOTree.Visible = true;
            this.colTPOTree.VisibleIndex = 32;
            // 
            // colPlanningConservation
            // 
            this.colPlanningConservation.Caption = "Planning Conservation";
            this.colPlanningConservation.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPlanningConservation.FieldName = "PlanningConservation";
            this.colPlanningConservation.Name = "colPlanningConservation";
            this.colPlanningConservation.OptionsColumn.AllowEdit = false;
            this.colPlanningConservation.OptionsColumn.AllowFocus = false;
            this.colPlanningConservation.OptionsColumn.ReadOnly = true;
            this.colPlanningConservation.Visible = true;
            this.colPlanningConservation.VisibleIndex = 33;
            this.colPlanningConservation.Width = 128;
            // 
            // colWildlifeDesignation
            // 
            this.colWildlifeDesignation.Caption = "Wildlife Designation";
            this.colWildlifeDesignation.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colWildlifeDesignation.FieldName = "WildlifeDesignation";
            this.colWildlifeDesignation.Name = "colWildlifeDesignation";
            this.colWildlifeDesignation.OptionsColumn.AllowEdit = false;
            this.colWildlifeDesignation.OptionsColumn.AllowFocus = false;
            this.colWildlifeDesignation.OptionsColumn.ReadOnly = true;
            this.colWildlifeDesignation.Visible = true;
            this.colWildlifeDesignation.VisibleIndex = 34;
            this.colWildlifeDesignation.Width = 114;
            // 
            // colSpecialInstructions
            // 
            this.colSpecialInstructions.Caption = "Special Instructions";
            this.colSpecialInstructions.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSpecialInstructions.FieldName = "SpecialInstructions";
            this.colSpecialInstructions.Name = "colSpecialInstructions";
            this.colSpecialInstructions.OptionsColumn.ReadOnly = true;
            this.colSpecialInstructions.Visible = true;
            this.colSpecialInstructions.VisibleIndex = 35;
            this.colSpecialInstructions.Width = 114;
            // 
            // colPrimarySubName
            // 
            this.colPrimarySubName.Caption = "Primary Sub Name";
            this.colPrimarySubName.FieldName = "PrimarySubName";
            this.colPrimarySubName.Name = "colPrimarySubName";
            this.colPrimarySubName.OptionsColumn.AllowEdit = false;
            this.colPrimarySubName.OptionsColumn.AllowFocus = false;
            this.colPrimarySubName.OptionsColumn.ReadOnly = true;
            this.colPrimarySubName.Visible = true;
            this.colPrimarySubName.VisibleIndex = 36;
            this.colPrimarySubName.Width = 108;
            // 
            // colPrimarySubNumber
            // 
            this.colPrimarySubNumber.Caption = "Primary Sub #";
            this.colPrimarySubNumber.FieldName = "PrimarySubNumber";
            this.colPrimarySubNumber.Name = "colPrimarySubNumber";
            this.colPrimarySubNumber.OptionsColumn.AllowEdit = false;
            this.colPrimarySubNumber.OptionsColumn.AllowFocus = false;
            this.colPrimarySubNumber.OptionsColumn.ReadOnly = true;
            this.colPrimarySubNumber.Visible = true;
            this.colPrimarySubNumber.VisibleIndex = 37;
            this.colPrimarySubNumber.Width = 89;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 38;
            this.colFeederName.Width = 85;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder #";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.Visible = true;
            this.colFeederNumber.VisibleIndex = 39;
            // 
            // colLineNumber
            // 
            this.colLineNumber.Caption = "Line #";
            this.colLineNumber.FieldName = "LineNumber";
            this.colLineNumber.Name = "colLineNumber";
            this.colLineNumber.OptionsColumn.AllowEdit = false;
            this.colLineNumber.OptionsColumn.AllowFocus = false;
            this.colLineNumber.OptionsColumn.ReadOnly = true;
            this.colLineNumber.Visible = true;
            this.colLineNumber.VisibleIndex = 40;
            // 
            // colLVSubName
            // 
            this.colLVSubName.Caption = "LV Sub Name";
            this.colLVSubName.FieldName = "LVSubName";
            this.colLVSubName.Name = "colLVSubName";
            this.colLVSubName.OptionsColumn.AllowEdit = false;
            this.colLVSubName.OptionsColumn.AllowFocus = false;
            this.colLVSubName.OptionsColumn.ReadOnly = true;
            this.colLVSubName.Visible = true;
            this.colLVSubName.VisibleIndex = 41;
            this.colLVSubName.Width = 83;
            // 
            // colLVSubNumber
            // 
            this.colLVSubNumber.Caption = "LV Sub #";
            this.colLVSubNumber.FieldName = "LVSubNumber";
            this.colLVSubNumber.Name = "colLVSubNumber";
            this.colLVSubNumber.OptionsColumn.AllowEdit = false;
            this.colLVSubNumber.OptionsColumn.AllowFocus = false;
            this.colLVSubNumber.OptionsColumn.ReadOnly = true;
            this.colLVSubNumber.Visible = true;
            this.colLVSubNumber.VisibleIndex = 42;
            // 
            // colManagedUnitNumber
            // 
            this.colManagedUnitNumber.Caption = "Managed Unit #";
            this.colManagedUnitNumber.FieldName = "ManagedUnitNumber";
            this.colManagedUnitNumber.Name = "colManagedUnitNumber";
            this.colManagedUnitNumber.OptionsColumn.AllowEdit = false;
            this.colManagedUnitNumber.OptionsColumn.AllowFocus = false;
            this.colManagedUnitNumber.OptionsColumn.ReadOnly = true;
            this.colManagedUnitNumber.Visible = true;
            this.colManagedUnitNumber.VisibleIndex = 43;
            this.colManagedUnitNumber.Width = 98;
            // 
            // colEnvironmentalRANumber
            // 
            this.colEnvironmentalRANumber.Caption = "Environmental RA #";
            this.colEnvironmentalRANumber.FieldName = "EnvironmentalRANumber";
            this.colEnvironmentalRANumber.Name = "colEnvironmentalRANumber";
            this.colEnvironmentalRANumber.OptionsColumn.AllowEdit = false;
            this.colEnvironmentalRANumber.OptionsColumn.AllowFocus = false;
            this.colEnvironmentalRANumber.OptionsColumn.ReadOnly = true;
            this.colEnvironmentalRANumber.Visible = true;
            this.colEnvironmentalRANumber.VisibleIndex = 44;
            this.colEnvironmentalRANumber.Width = 117;
            // 
            // colLandscapeImpactNumber
            // 
            this.colLandscapeImpactNumber.Caption = "Landscape Impact #";
            this.colLandscapeImpactNumber.FieldName = "LandscapeImpactNumber";
            this.colLandscapeImpactNumber.Name = "colLandscapeImpactNumber";
            this.colLandscapeImpactNumber.OptionsColumn.AllowEdit = false;
            this.colLandscapeImpactNumber.OptionsColumn.AllowFocus = false;
            this.colLandscapeImpactNumber.OptionsColumn.ReadOnly = true;
            this.colLandscapeImpactNumber.Visible = true;
            this.colLandscapeImpactNumber.VisibleIndex = 45;
            this.colLandscapeImpactNumber.Width = 119;
            // 
            // colSiteGridReference
            // 
            this.colSiteGridReference.Caption = "Site Grid Ref.";
            this.colSiteGridReference.FieldName = "SiteGridReference";
            this.colSiteGridReference.Name = "colSiteGridReference";
            this.colSiteGridReference.OptionsColumn.AllowEdit = false;
            this.colSiteGridReference.OptionsColumn.AllowFocus = false;
            this.colSiteGridReference.OptionsColumn.ReadOnly = true;
            this.colSiteGridReference.Visible = true;
            this.colSiteGridReference.VisibleIndex = 46;
            this.colSiteGridReference.Width = 85;
            // 
            // colPermissionEmailed
            // 
            this.colPermissionEmailed.Caption = "Permission Emailed";
            this.colPermissionEmailed.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPermissionEmailed.FieldName = "PermissionEmailed";
            this.colPermissionEmailed.Name = "colPermissionEmailed";
            this.colPermissionEmailed.OptionsColumn.AllowEdit = false;
            this.colPermissionEmailed.OptionsColumn.AllowFocus = false;
            this.colPermissionEmailed.OptionsColumn.ReadOnly = true;
            this.colPermissionEmailed.Visible = true;
            this.colPermissionEmailed.VisibleIndex = 47;
            this.colPermissionEmailed.Width = 110;
            // 
            // colPostageRequired
            // 
            this.colPostageRequired.Caption = "Postage Required";
            this.colPostageRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPostageRequired.FieldName = "PostageRequired";
            this.colPostageRequired.Name = "colPostageRequired";
            this.colPostageRequired.OptionsColumn.AllowEdit = false;
            this.colPostageRequired.OptionsColumn.AllowFocus = false;
            this.colPostageRequired.OptionsColumn.ReadOnly = true;
            this.colPostageRequired.Visible = true;
            this.colPostageRequired.VisibleIndex = 48;
            this.colPostageRequired.Width = 106;
            // 
            // colSentByPost
            // 
            this.colSentByPost.Caption = "Sent By Post";
            this.colSentByPost.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSentByPost.FieldName = "SentByPost";
            this.colSentByPost.Name = "colSentByPost";
            this.colSentByPost.OptionsColumn.AllowEdit = false;
            this.colSentByPost.OptionsColumn.AllowFocus = false;
            this.colSentByPost.OptionsColumn.ReadOnly = true;
            this.colSentByPost.Visible = true;
            this.colSentByPost.VisibleIndex = 49;
            this.colSentByPost.Width = 82;
            // 
            // colSentByStaffID
            // 
            this.colSentByStaffID.Caption = "Sent by Staff ID";
            this.colSentByStaffID.FieldName = "SentByStaffID";
            this.colSentByStaffID.Name = "colSentByStaffID";
            this.colSentByStaffID.OptionsColumn.AllowEdit = false;
            this.colSentByStaffID.OptionsColumn.AllowFocus = false;
            this.colSentByStaffID.OptionsColumn.ReadOnly = true;
            this.colSentByStaffID.Width = 99;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 1;
            this.colReferenceNumber.Width = 124;
            // 
            // colOwnerName
            // 
            this.colOwnerName.Caption = "Owner Name";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.OptionsColumn.AllowEdit = false;
            this.colOwnerName.OptionsColumn.AllowFocus = false;
            this.colOwnerName.OptionsColumn.ReadOnly = true;
            this.colOwnerName.Visible = true;
            this.colOwnerName.VisibleIndex = 4;
            this.colOwnerName.Width = 104;
            // 
            // colOwnerSalutation
            // 
            this.colOwnerSalutation.Caption = "Owner Saluation";
            this.colOwnerSalutation.FieldName = "OwnerSalutation";
            this.colOwnerSalutation.Name = "colOwnerSalutation";
            this.colOwnerSalutation.OptionsColumn.AllowEdit = false;
            this.colOwnerSalutation.OptionsColumn.AllowFocus = false;
            this.colOwnerSalutation.OptionsColumn.ReadOnly = true;
            this.colOwnerSalutation.Visible = true;
            this.colOwnerSalutation.VisibleIndex = 9;
            this.colOwnerSalutation.Width = 100;
            // 
            // colOwnerAddress
            // 
            this.colOwnerAddress.Caption = "Owner Address";
            this.colOwnerAddress.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colOwnerAddress.FieldName = "OwnerAddress";
            this.colOwnerAddress.Name = "colOwnerAddress";
            this.colOwnerAddress.OptionsColumn.ReadOnly = true;
            this.colOwnerAddress.Visible = true;
            this.colOwnerAddress.VisibleIndex = 5;
            this.colOwnerAddress.Width = 134;
            // 
            // colOwnerPostcode
            // 
            this.colOwnerPostcode.Caption = "Owner Postcode";
            this.colOwnerPostcode.FieldName = "OwnerPostcode";
            this.colOwnerPostcode.Name = "colOwnerPostcode";
            this.colOwnerPostcode.OptionsColumn.AllowEdit = false;
            this.colOwnerPostcode.OptionsColumn.AllowFocus = false;
            this.colOwnerPostcode.OptionsColumn.ReadOnly = true;
            this.colOwnerPostcode.Visible = true;
            this.colOwnerPostcode.VisibleIndex = 6;
            this.colOwnerPostcode.Width = 100;
            // 
            // colOwnerTelephone
            // 
            this.colOwnerTelephone.Caption = "Owner Telephone";
            this.colOwnerTelephone.FieldName = "OwnerTelephone";
            this.colOwnerTelephone.Name = "colOwnerTelephone";
            this.colOwnerTelephone.OptionsColumn.AllowEdit = false;
            this.colOwnerTelephone.OptionsColumn.AllowFocus = false;
            this.colOwnerTelephone.OptionsColumn.ReadOnly = true;
            this.colOwnerTelephone.Visible = true;
            this.colOwnerTelephone.VisibleIndex = 7;
            this.colOwnerTelephone.Width = 106;
            // 
            // colOwnerEmail
            // 
            this.colOwnerEmail.Caption = "Owner Email";
            this.colOwnerEmail.FieldName = "OwnerEmail";
            this.colOwnerEmail.Name = "colOwnerEmail";
            this.colOwnerEmail.OptionsColumn.AllowEdit = false;
            this.colOwnerEmail.OptionsColumn.AllowFocus = false;
            this.colOwnerEmail.OptionsColumn.ReadOnly = true;
            this.colOwnerEmail.Visible = true;
            this.colOwnerEmail.VisibleIndex = 8;
            this.colOwnerEmail.Width = 80;
            // 
            // colOwnerTypeID
            // 
            this.colOwnerTypeID.Caption = "Owner Type ID";
            this.colOwnerTypeID.FieldName = "OwnerTypeID";
            this.colOwnerTypeID.Name = "colOwnerTypeID";
            this.colOwnerTypeID.OptionsColumn.AllowEdit = false;
            this.colOwnerTypeID.OptionsColumn.AllowFocus = false;
            this.colOwnerTypeID.OptionsColumn.ReadOnly = true;
            this.colOwnerTypeID.Width = 94;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Site Address";
            this.colSiteAddress.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Visible = true;
            this.colSiteAddress.VisibleIndex = 11;
            this.colSiteAddress.Width = 103;
            // 
            // colContractor
            // 
            this.colContractor.Caption = "Contractor";
            this.colContractor.FieldName = "Contractor";
            this.colContractor.Name = "colContractor";
            this.colContractor.OptionsColumn.AllowEdit = false;
            this.colContractor.OptionsColumn.AllowFocus = false;
            this.colContractor.OptionsColumn.ReadOnly = true;
            this.colContractor.Visible = true;
            this.colContractor.VisibleIndex = 50;
            // 
            // colArisingsChipRemove
            // 
            this.colArisingsChipRemove.Caption = "Arising Chip & Remove";
            this.colArisingsChipRemove.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colArisingsChipRemove.FieldName = "ArisingsChipRemove";
            this.colArisingsChipRemove.Name = "colArisingsChipRemove";
            this.colArisingsChipRemove.OptionsColumn.AllowEdit = false;
            this.colArisingsChipRemove.OptionsColumn.AllowFocus = false;
            this.colArisingsChipRemove.OptionsColumn.ReadOnly = true;
            this.colArisingsChipRemove.Visible = true;
            this.colArisingsChipRemove.VisibleIndex = 51;
            this.colArisingsChipRemove.Width = 129;
            // 
            // colArisingsChipOnSite
            // 
            this.colArisingsChipOnSite.Caption = "Arising Chip On Site";
            this.colArisingsChipOnSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colArisingsChipOnSite.FieldName = "ArisingsChipOnSite";
            this.colArisingsChipOnSite.Name = "colArisingsChipOnSite";
            this.colArisingsChipOnSite.OptionsColumn.AllowEdit = false;
            this.colArisingsChipOnSite.OptionsColumn.AllowFocus = false;
            this.colArisingsChipOnSite.OptionsColumn.ReadOnly = true;
            this.colArisingsChipOnSite.Visible = true;
            this.colArisingsChipOnSite.VisibleIndex = 52;
            this.colArisingsChipOnSite.Width = 115;
            // 
            // colArisingsStackOnSite
            // 
            this.colArisingsStackOnSite.Caption = "Arising Stack On Site";
            this.colArisingsStackOnSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colArisingsStackOnSite.FieldName = "ArisingsStackOnSite";
            this.colArisingsStackOnSite.Name = "colArisingsStackOnSite";
            this.colArisingsStackOnSite.OptionsColumn.AllowEdit = false;
            this.colArisingsStackOnSite.OptionsColumn.AllowFocus = false;
            this.colArisingsStackOnSite.OptionsColumn.ReadOnly = true;
            this.colArisingsStackOnSite.Visible = true;
            this.colArisingsStackOnSite.VisibleIndex = 53;
            this.colArisingsStackOnSite.Width = 120;
            // 
            // colArisingsOther
            // 
            this.colArisingsOther.Caption = "Arising Other";
            this.colArisingsOther.FieldName = "ArisingsOther";
            this.colArisingsOther.Name = "colArisingsOther";
            this.colArisingsOther.OptionsColumn.AllowEdit = false;
            this.colArisingsOther.OptionsColumn.AllowFocus = false;
            this.colArisingsOther.OptionsColumn.ReadOnly = true;
            this.colArisingsOther.Visible = true;
            this.colArisingsOther.VisibleIndex = 54;
            this.colArisingsOther.Width = 84;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colRaisedByName
            // 
            this.colRaisedByName.Caption = "Raised By Name";
            this.colRaisedByName.FieldName = "RaisedByName";
            this.colRaisedByName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRaisedByName.Name = "colRaisedByName";
            this.colRaisedByName.OptionsColumn.AllowEdit = false;
            this.colRaisedByName.OptionsColumn.AllowFocus = false;
            this.colRaisedByName.OptionsColumn.ReadOnly = true;
            this.colRaisedByName.Visible = true;
            this.colRaisedByName.VisibleIndex = 2;
            this.colRaisedByName.Width = 98;
            // 
            // colSentByStaffName
            // 
            this.colSentByStaffName.Caption = "Sent By Staff Name";
            this.colSentByStaffName.FieldName = "SentByStaffName";
            this.colSentByStaffName.Name = "colSentByStaffName";
            this.colSentByStaffName.OptionsColumn.AllowEdit = false;
            this.colSentByStaffName.OptionsColumn.AllowFocus = false;
            this.colSentByStaffName.OptionsColumn.ReadOnly = true;
            this.colSentByStaffName.Visible = true;
            this.colSentByStaffName.VisibleIndex = 55;
            this.colSentByStaffName.Width = 115;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Linked Work Count";
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 14;
            this.colLinkedActionCount.Width = 111;
            // 
            // colLinkedToDoActionCount
            // 
            this.colLinkedToDoActionCount.Caption = "Linked To Do Work";
            this.colLinkedToDoActionCount.FieldName = "LinkedToDoActionCount";
            this.colLinkedToDoActionCount.Name = "colLinkedToDoActionCount";
            this.colLinkedToDoActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedToDoActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedToDoActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedToDoActionCount.Visible = true;
            this.colLinkedToDoActionCount.VisibleIndex = 15;
            this.colLinkedToDoActionCount.Width = 110;
            // 
            // colOwnerType
            // 
            this.colOwnerType.Caption = "Owner Type";
            this.colOwnerType.FieldName = "OwnerType";
            this.colOwnerType.Name = "colOwnerType";
            this.colOwnerType.OptionsColumn.AllowEdit = false;
            this.colOwnerType.OptionsColumn.AllowFocus = false;
            this.colOwnerType.OptionsColumn.ReadOnly = true;
            this.colOwnerType.Visible = true;
            this.colOwnerType.VisibleIndex = 10;
            this.colOwnerType.Width = 80;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 3;
            this.colClientName.Width = 97;
            // 
            // colDataEntryScreenName
            // 
            this.colDataEntryScreenName.Caption = "Data Entry Screen";
            this.colDataEntryScreenName.FieldName = "DataEntryScreenName";
            this.colDataEntryScreenName.Name = "colDataEntryScreenName";
            this.colDataEntryScreenName.OptionsColumn.AllowEdit = false;
            this.colDataEntryScreenName.OptionsColumn.AllowFocus = false;
            this.colDataEntryScreenName.OptionsColumn.ReadOnly = true;
            this.colDataEntryScreenName.Width = 109;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Report Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.Width = 120;
            // 
            // colNotPermissionedCount
            // 
            this.colNotPermissionedCount.Caption = "Linked Refused Work Count";
            this.colNotPermissionedCount.FieldName = "NotPermissionedCount";
            this.colNotPermissionedCount.Name = "colNotPermissionedCount";
            this.colNotPermissionedCount.OptionsColumn.AllowEdit = false;
            this.colNotPermissionedCount.OptionsColumn.AllowFocus = false;
            this.colNotPermissionedCount.OptionsColumn.ReadOnly = true;
            this.colNotPermissionedCount.Visible = true;
            this.colNotPermissionedCount.VisibleIndex = 17;
            this.colNotPermissionedCount.Width = 154;
            // 
            // colOnHoldCount
            // 
            this.colOnHoldCount.Caption = "Linked On-Hold Work Count";
            this.colOnHoldCount.FieldName = "OnHoldCount";
            this.colOnHoldCount.Name = "colOnHoldCount";
            this.colOnHoldCount.OptionsColumn.AllowEdit = false;
            this.colOnHoldCount.OptionsColumn.AllowFocus = false;
            this.colOnHoldCount.OptionsColumn.ReadOnly = true;
            this.colOnHoldCount.Visible = true;
            this.colOnHoldCount.VisibleIndex = 16;
            this.colOnHoldCount.Width = 153;
            // 
            // colPermissionedCount
            // 
            this.colPermissionedCount.Caption = "Linked Permissioned Work Count";
            this.colPermissionedCount.FieldName = "PermissionedCount";
            this.colPermissionedCount.Name = "colPermissionedCount";
            this.colPermissionedCount.OptionsColumn.AllowEdit = false;
            this.colPermissionedCount.OptionsColumn.AllowFocus = false;
            this.colPermissionedCount.OptionsColumn.ReadOnly = true;
            this.colPermissionedCount.Visible = true;
            this.colPermissionedCount.VisibleIndex = 18;
            this.colPermissionedCount.Width = 176;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 114;
            // 
            // colLandownerRestrictedCut1
            // 
            this.colLandownerRestrictedCut1.Caption = "Landowner Restricted Cut";
            this.colLandownerRestrictedCut1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colLandownerRestrictedCut1.FieldName = "LandownerRestrictedCut";
            this.colLandownerRestrictedCut1.Name = "colLandownerRestrictedCut1";
            this.colLandownerRestrictedCut1.OptionsColumn.AllowEdit = false;
            this.colLandownerRestrictedCut1.OptionsColumn.AllowFocus = false;
            this.colLandownerRestrictedCut1.OptionsColumn.ReadOnly = true;
            this.colLandownerRestrictedCut1.Visible = true;
            this.colLandownerRestrictedCut1.VisibleIndex = 27;
            this.colLandownerRestrictedCut1.Width = 144;
            // 
            // repositoryItemTextEditNumeric0DP
            // 
            this.repositoryItemTextEditNumeric0DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric0DP.Mask.EditMask = "n0";
            this.repositoryItemTextEditNumeric0DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric0DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric0DP.Name = "repositoryItemTextEditNumeric0DP";
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlSurveyors);
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl2.Panel2.Text = "Linked Documents";
            this.splitContainerControl2.Size = new System.Drawing.Size(1036, 483);
            this.splitContainerControl2.SplitterPosition = 235;
            this.splitContainerControl2.TabIndex = 5;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // popupContainerControlSurveyors
            // 
            this.popupContainerControlSurveyors.Controls.Add(this.gridControl7);
            this.popupContainerControlSurveyors.Controls.Add(this.btnSurveyorOK);
            this.popupContainerControlSurveyors.Location = new System.Drawing.Point(511, 34);
            this.popupContainerControlSurveyors.Name = "popupContainerControlSurveyors";
            this.popupContainerControlSurveyors.Size = new System.Drawing.Size(200, 178);
            this.popupContainerControlSurveyors.TabIndex = 4;
            // 
            // gridControl7
            // 
            this.gridControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl7.DataSource = this.sp07329UTSurveyorListNoBlankBindingSource;
            this.gridControl7.Location = new System.Drawing.Point(3, 1);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemCheckEdit2});
            this.gridControl7.Size = new System.Drawing.Size(194, 147);
            this.gridControl7.TabIndex = 3;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp07329UTSurveyorListNoBlankBindingSource
            // 
            this.sp07329UTSurveyorListNoBlankBindingSource.DataMember = "sp07329_UT_Surveyor_List_No_Blank";
            this.sp07329UTSurveyorListNoBlankBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // dataSet_UT_Reporting
            // 
            this.dataSet_UT_Reporting.DataSetName = "DataSet_UT_Reporting";
            this.dataSet_UT_Reporting.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDisplayName,
            this.colStaffID,
            this.colForename,
            this.colSurname,
            this.colStaffName,
            this.colActive});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colActive;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colForename, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDisplayName
            // 
            this.colDisplayName.Caption = "Surveyor Surname: Forename";
            this.colDisplayName.FieldName = "DisplayName";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.OptionsColumn.AllowEdit = false;
            this.colDisplayName.OptionsColumn.AllowFocus = false;
            this.colDisplayName.OptionsColumn.ReadOnly = true;
            this.colDisplayName.Visible = true;
            this.colDisplayName.VisibleIndex = 0;
            this.colDisplayName.Width = 183;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Surveyor ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 80;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Width = 97;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Width = 109;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Width = 76;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // btnSurveyorOK
            // 
            this.btnSurveyorOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSurveyorOK.Location = new System.Drawing.Point(3, 152);
            this.btnSurveyorOK.Name = "btnSurveyorOK";
            this.btnSurveyorOK.Size = new System.Drawing.Size(33, 23);
            this.btnSurveyorOK.TabIndex = 2;
            this.btnSurveyorOK.Text = "OK";
            this.btnSurveyorOK.Click += new System.EventHandler(this.btnSurveyorFilterOK_Click);
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(157, 103);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(180, 109);
            this.popupContainerControlDateRange.TabIndex = 3;
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 83);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(34, 23);
            this.btnDateRangeFilterOK.TabIndex = 3;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(174, 76);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Date Range";
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditToDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditFromDate.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(1032, 231);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage5,
            this.xtraTabPage1});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl5);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1027, 202);
            this.xtraTabPage2.Text = "Permissioned Work";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07392UTPDLinkedWorkBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemTextEditDateMask,
            this.repositoryItemCheckEdit3});
            this.gridControl5.Size = new System.Drawing.Size(1027, 202);
            this.gridControl5.TabIndex = 2;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07392UTPDLinkedWorkBindingSource
            // 
            this.sp07392UTPDLinkedWorkBindingSource.DataMember = "sp07392_UT_PD_Linked_Work";
            this.sp07392UTPDLinkedWorkBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionPermissionID,
            this.colPermissionedPoleID,
            this.colActionID,
            this.colPermissionStatusID,
            this.gridColumn9,
            this.gridColumn10,
            this.colLandOwnerName,
            this.colPermissionDocumentDateRaised,
            this.colPoleNumber,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colJobDescription,
            this.colTreeReferenceNumber,
            this.colSurveyID,
            this.colTreeID,
            this.colPoleID,
            this.colStumpTreatmentNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentSpraying,
            this.colTreeReplacementNone,
            this.colTreeReplacementStandards,
            this.colTreeReplacementWhips,
            this.colESQCRCategory,
            this.colG55Category,
            this.colAchievableClearance,
            this.colLinkedSpecies,
            this.colLandOwnerRestrictedCut,
            this.colShutdownRequired,
            this.colJobTypeID,
            this.colReferenceNumber1,
            this.colStatusDescription,
            this.colRefusalReason,
            this.colRefusalRemarks});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReferenceNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView5_CustomDrawCell);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colActionPermissionID
            // 
            this.colActionPermissionID.Caption = "Action Permission ID";
            this.colActionPermissionID.FieldName = "ActionPermissionID";
            this.colActionPermissionID.Name = "colActionPermissionID";
            this.colActionPermissionID.OptionsColumn.AllowEdit = false;
            this.colActionPermissionID.OptionsColumn.AllowFocus = false;
            this.colActionPermissionID.OptionsColumn.ReadOnly = true;
            this.colActionPermissionID.Width = 108;
            // 
            // colPermissionedPoleID
            // 
            this.colPermissionedPoleID.Caption = "Permissioned Pole ID";
            this.colPermissionedPoleID.FieldName = "PermissionedPoleID";
            this.colPermissionedPoleID.Name = "colPermissionedPoleID";
            this.colPermissionedPoleID.OptionsColumn.AllowEdit = false;
            this.colPermissionedPoleID.OptionsColumn.AllowFocus = false;
            this.colPermissionedPoleID.OptionsColumn.ReadOnly = true;
            this.colPermissionedPoleID.Width = 110;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colPermissionStatusID
            // 
            this.colPermissionStatusID.Caption = "Permission Status ID";
            this.colPermissionStatusID.FieldName = "PermissionStatusID";
            this.colPermissionStatusID.Name = "colPermissionStatusID";
            this.colPermissionStatusID.Width = 118;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Permission Document ID";
            this.gridColumn9.FieldName = "PermissionDocumentID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 126;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Remarks";
            this.gridColumn10.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.gridColumn10.FieldName = "Remarks";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 8;
            this.gridColumn10.Width = 152;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colLandOwnerName
            // 
            this.colLandOwnerName.Caption = "Land Owner Name";
            this.colLandOwnerName.FieldName = "LandOwnerName";
            this.colLandOwnerName.Name = "colLandOwnerName";
            this.colLandOwnerName.OptionsColumn.AllowEdit = false;
            this.colLandOwnerName.OptionsColumn.AllowFocus = false;
            this.colLandOwnerName.OptionsColumn.ReadOnly = true;
            this.colLandOwnerName.Width = 144;
            // 
            // colPermissionDocumentDateRaised
            // 
            this.colPermissionDocumentDateRaised.Caption = "Permission Document Date Raised";
            this.colPermissionDocumentDateRaised.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colPermissionDocumentDateRaised.FieldName = "PermissionDocumentDateRaised";
            this.colPermissionDocumentDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPermissionDocumentDateRaised.Name = "colPermissionDocumentDateRaised";
            this.colPermissionDocumentDateRaised.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentDateRaised.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentDateRaised.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPermissionDocumentDateRaised.Width = 173;
            // 
            // repositoryItemTextEditDateMask
            // 
            this.repositoryItemTextEditDateMask.AutoHeight = false;
            this.repositoryItemTextEditDateMask.Mask.EditMask = "g";
            this.repositoryItemTextEditDateMask.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateMask.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateMask.Name = "repositoryItemTextEditDateMask";
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 3;
            this.colPoleNumber.Width = 101;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Width = 132;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 2;
            this.colCircuitNumber.Width = 113;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Work Agreed";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 5;
            this.colJobDescription.Width = 225;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Tree Reference";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 0;
            this.colTreeReferenceNumber.Width = 113;
            // 
            // colSurveyID
            // 
            this.colSurveyID.Caption = "Survey ID";
            this.colSurveyID.FieldName = "SurveyID";
            this.colSurveyID.Name = "colSurveyID";
            this.colSurveyID.OptionsColumn.AllowEdit = false;
            this.colSurveyID.OptionsColumn.AllowFocus = false;
            this.colSurveyID.OptionsColumn.ReadOnly = true;
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.Width = 129;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatment - Eco Plug";
            this.colStumpTreatmentEcoPlugs.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.Width = 144;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.colStumpTreatmentPaint.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.Width = 128;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.colStumpTreatmentSpraying.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.Width = 146;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement - None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.Width = 133;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.colTreeReplacementStandards.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.Width = 157;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.colTreeReplacementWhips.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.Width = 137;
            // 
            // colESQCRCategory
            // 
            this.colESQCRCategory.Caption = "ESQCR Cat";
            this.colESQCRCategory.FieldName = "ESQCRCategory";
            this.colESQCRCategory.Name = "colESQCRCategory";
            this.colESQCRCategory.OptionsColumn.AllowEdit = false;
            this.colESQCRCategory.OptionsColumn.AllowFocus = false;
            this.colESQCRCategory.OptionsColumn.ReadOnly = true;
            this.colESQCRCategory.Visible = true;
            this.colESQCRCategory.VisibleIndex = 7;
            this.colESQCRCategory.Width = 65;
            // 
            // colG55Category
            // 
            this.colG55Category.Caption = "G55/2 Cat";
            this.colG55Category.FieldName = "G55Category";
            this.colG55Category.Name = "colG55Category";
            this.colG55Category.OptionsColumn.AllowEdit = false;
            this.colG55Category.OptionsColumn.AllowFocus = false;
            this.colG55Category.OptionsColumn.ReadOnly = true;
            this.colG55Category.Visible = true;
            this.colG55Category.VisibleIndex = 1;
            this.colG55Category.Width = 66;
            // 
            // colAchievableClearance
            // 
            this.colAchievableClearance.Caption = "Clearance";
            this.colAchievableClearance.FieldName = "AchievableClearance";
            this.colAchievableClearance.Name = "colAchievableClearance";
            this.colAchievableClearance.OptionsColumn.AllowEdit = false;
            this.colAchievableClearance.OptionsColumn.AllowFocus = false;
            this.colAchievableClearance.OptionsColumn.ReadOnly = true;
            this.colAchievableClearance.Visible = true;
            this.colAchievableClearance.VisibleIndex = 6;
            this.colAchievableClearance.Width = 59;
            // 
            // colLinkedSpecies
            // 
            this.colLinkedSpecies.Caption = "Tree Species";
            this.colLinkedSpecies.FieldName = "LinkedSpecies";
            this.colLinkedSpecies.Name = "colLinkedSpecies";
            this.colLinkedSpecies.OptionsColumn.AllowEdit = false;
            this.colLinkedSpecies.OptionsColumn.AllowFocus = false;
            this.colLinkedSpecies.OptionsColumn.ReadOnly = true;
            this.colLinkedSpecies.Visible = true;
            this.colLinkedSpecies.VisibleIndex = 4;
            this.colLinkedSpecies.Width = 146;
            // 
            // colLandOwnerRestrictedCut
            // 
            this.colLandOwnerRestrictedCut.Caption = "Restricted Cut";
            this.colLandOwnerRestrictedCut.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colLandOwnerRestrictedCut.FieldName = "LandOwnerRestrictedCut";
            this.colLandOwnerRestrictedCut.Name = "colLandOwnerRestrictedCut";
            this.colLandOwnerRestrictedCut.OptionsColumn.AllowEdit = false;
            this.colLandOwnerRestrictedCut.OptionsColumn.AllowFocus = false;
            this.colLandOwnerRestrictedCut.OptionsColumn.ReadOnly = true;
            this.colLandOwnerRestrictedCut.Width = 80;
            // 
            // colShutdownRequired
            // 
            this.colShutdownRequired.Caption = "Shutdown Required";
            this.colShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colShutdownRequired.FieldName = "ShutdownRequired";
            this.colShutdownRequired.Name = "colShutdownRequired";
            this.colShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colShutdownRequired.Width = 105;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colReferenceNumber1
            // 
            this.colReferenceNumber1.Caption = "PD Reference Number";
            this.colReferenceNumber1.FieldName = "ReferenceNumber";
            this.colReferenceNumber1.Name = "colReferenceNumber1";
            this.colReferenceNumber1.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber1.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber1.Visible = true;
            this.colReferenceNumber1.VisibleIndex = 8;
            this.colReferenceNumber1.Width = 117;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Permission Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 11;
            this.colStatusDescription.Width = 125;
            // 
            // colRefusalReason
            // 
            this.colRefusalReason.Caption = "Refusal Reason";
            this.colRefusalReason.FieldName = "RefusalReason";
            this.colRefusalReason.Name = "colRefusalReason";
            this.colRefusalReason.OptionsColumn.AllowEdit = false;
            this.colRefusalReason.OptionsColumn.AllowFocus = false;
            this.colRefusalReason.OptionsColumn.ReadOnly = true;
            this.colRefusalReason.Visible = true;
            this.colRefusalReason.VisibleIndex = 9;
            this.colRefusalReason.Width = 96;
            // 
            // colRefusalRemarks
            // 
            this.colRefusalRemarks.Caption = "Refusal Remarks";
            this.colRefusalRemarks.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRefusalRemarks.FieldName = "RefusalRemarks";
            this.colRefusalRemarks.Name = "colRefusalRemarks";
            this.colRefusalRemarks.OptionsColumn.ReadOnly = true;
            this.colRefusalRemarks.Visible = true;
            this.colRefusalRemarks.VisibleIndex = 10;
            this.colRefusalRemarks.Width = 101;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.gridControl6);
            this.xtraTabPage5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage5.ImageOptions.Image")));
            this.xtraTabPage5.ImageOptions.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1027, 202);
            this.xtraTabPage5.Text = "Linked Work Maps";
            // 
            // gridControl6
            // 
            this.gridControl6.AllowDrop = true;
            this.gridControl6.DataSource = this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Preview Map", "view")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemPictureEdit1,
            this.repositoryItemTextEdit1});
            this.gridControl6.Size = new System.Drawing.Size(1027, 202);
            this.gridControl6.TabIndex = 6;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource
            // 
            this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource.DataMember = "sp07202_UT_Mapping_Snapshots_Linked_Snapshots";
            this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource.DataSource = this.dataSet_UT_Mapping;
            // 
            // dataSet_UT_Mapping
            // 
            this.dataSet_UT_Mapping.DataSetName = "DataSet_UT_Mapping";
            this.dataSet_UT_Mapping.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedMapID,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.colMapOrder,
            this.colCreatedByID,
            this.colDateTimeCreated,
            this.gridColumn8,
            this.colCreatedByName,
            this.colLinkedToDescription,
            this.gridColumn12});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 1;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView6.OptionsCustomization.AllowFilter = false;
            this.gridView6.OptionsCustomization.AllowGroup = false;
            this.gridView6.OptionsCustomization.AllowSort = false;
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.RowAutoHeight = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMapOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView6_CustomUnboundColumnData);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseDown);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colLinkedMapID
            // 
            this.colLinkedMapID.Caption = "Linked Map ID";
            this.colLinkedMapID.FieldName = "LinkedMapID";
            this.colLinkedMapID.Name = "colLinkedMapID";
            this.colLinkedMapID.OptionsColumn.AllowEdit = false;
            this.colLinkedMapID.OptionsColumn.AllowFocus = false;
            this.colLinkedMapID.OptionsColumn.ReadOnly = true;
            this.colLinkedMapID.Width = 78;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Linked To Record ID";
            this.gridColumn4.FieldName = "LinkedToRecordID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 107;
            // 
            // gridColumn5
            // 
            this.gridColumn5.FieldName = "LinkedToRecordTypeID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 134;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Map Description";
            this.gridColumn6.FieldName = "Description";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 250;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Saved File";
            this.gridColumn7.FieldName = "DocumentPath";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 152;
            // 
            // colMapOrder
            // 
            this.colMapOrder.Caption = "Order";
            this.colMapOrder.FieldName = "MapOrder";
            this.colMapOrder.Name = "colMapOrder";
            this.colMapOrder.OptionsColumn.AllowEdit = false;
            this.colMapOrder.OptionsColumn.AllowFocus = false;
            this.colMapOrder.OptionsColumn.ReadOnly = true;
            this.colMapOrder.Visible = true;
            this.colMapOrder.VisibleIndex = 0;
            this.colMapOrder.Width = 54;
            // 
            // colCreatedByID
            // 
            this.colCreatedByID.Caption = "Created By ID";
            this.colCreatedByID.FieldName = "CreatedByID";
            this.colCreatedByID.Name = "colCreatedByID";
            this.colCreatedByID.OptionsColumn.AllowEdit = false;
            this.colCreatedByID.OptionsColumn.AllowFocus = false;
            this.colCreatedByID.OptionsColumn.ReadOnly = true;
            this.colCreatedByID.Width = 79;
            // 
            // colDateTimeCreated
            // 
            this.colDateTimeCreated.Caption = "Date Created";
            this.colDateTimeCreated.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDateTimeCreated.FieldName = "DateTimeCreated";
            this.colDateTimeCreated.Name = "colDateTimeCreated";
            this.colDateTimeCreated.OptionsColumn.AllowEdit = false;
            this.colDateTimeCreated.OptionsColumn.AllowFocus = false;
            this.colDateTimeCreated.OptionsColumn.ReadOnly = true;
            this.colDateTimeCreated.Visible = true;
            this.colDateTimeCreated.VisibleIndex = 3;
            this.colDateTimeCreated.Width = 96;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Remarks";
            this.gridColumn8.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn8.FieldName = "Remarks";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colCreatedByName
            // 
            this.colCreatedByName.Caption = "Created By";
            this.colCreatedByName.FieldName = "CreatedByName";
            this.colCreatedByName.Name = "colCreatedByName";
            this.colCreatedByName.OptionsColumn.AllowEdit = false;
            this.colCreatedByName.OptionsColumn.AllowFocus = false;
            this.colCreatedByName.OptionsColumn.ReadOnly = true;
            this.colCreatedByName.Visible = true;
            this.colCreatedByName.VisibleIndex = 2;
            this.colCreatedByName.Width = 114;
            // 
            // colLinkedToDescription
            // 
            this.colLinkedToDescription.Caption = "Linked To";
            this.colLinkedToDescription.FieldName = "LinkedToDescription";
            this.colLinkedToDescription.Name = "colLinkedToDescription";
            this.colLinkedToDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedToDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedToDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedToDescription.Visible = true;
            this.colLinkedToDescription.VisibleIndex = 6;
            this.colLinkedToDescription.Width = 316;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Preview";
            this.gridColumn12.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumn12.FieldName = "gridColumnThumbNail";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn12.OptionsColumn.AllowSize = false;
            this.gridColumn12.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn12.OptionsColumn.FixedWidth = true;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn12.OptionsFilter.AllowFilter = false;
            this.gridColumn12.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 6;
            this.gridColumn12.Width = 49;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage1.ImageOptions.Image")));
            this.xtraTabPage1.ImageOptions.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1027, 202);
            this.xtraTabPage1.Text = "Linked Documents";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl3;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1027, 202);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl3.Size = new System.Drawing.Size(1027, 202);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            this.colDocumentRemarks.Width = 134;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // popupContainerDateRange
            // 
            this.popupContainerDateRange.Caption = "Date Filter";
            this.popupContainerDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.popupContainerDateRange.EditValue = "No Date Filter";
            this.popupContainerDateRange.EditWidth = 155;
            this.popupContainerDateRange.Id = 34;
            this.popupContainerDateRange.Name = "popupContainerDateRange";
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.Caption = "Surveyor Filter";
            this.popupContainerEdit2.Edit = this.repositoryItemPopupContainerEdit3;
            this.popupContainerEdit2.EditValue = "No Surveyor Filter";
            this.popupContainerEdit2.EditWidth = 386;
            this.popupContainerEdit2.Id = 35;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            // 
            // repositoryItemPopupContainerEdit3
            // 
            this.repositoryItemPopupContainerEdit3.AutoHeight = false;
            this.repositoryItemPopupContainerEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit3.Name = "repositoryItemPopupContainerEdit3";
            this.repositoryItemPopupContainerEdit3.PopupControl = this.popupContainerControlSurveyors;
            this.repositoryItemPopupContainerEdit3.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit3.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit3_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Load Data";
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Date Filter";
            this.barEditItem1.Edit = this.repositoryItemDateEdit1;
            this.barEditItem1.Id = 33;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 32);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
            this.xtraTabControl2.Size = new System.Drawing.Size(1041, 509);
            this.xtraTabControl2.TabIndex = 6;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1036, 483);
            this.xtraTabPage3.Text = "Permission Documents";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridSplitContainer1);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1036, 483);
            this.xtraTabPage4.Text = "Unpermissioned Work";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl4;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1036, 483);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp07400UTPDUnpermissionedWorkBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Add to Existing Permission Document", "add to existing"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Records", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record", "view")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEditMoney2,
            this.repositoryItemTextEditHours});
            this.gridControl4.Size = new System.Drawing.Size(1036, 483);
            this.gridControl4.TabIndex = 1;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07400UTPDUnpermissionedWorkBindingSource
            // 
            this.sp07400UTPDUnpermissionedWorkBindingSource.DataMember = "sp07400_UT_PD_Unpermissioned_Work";
            this.sp07400UTPDUnpermissionedWorkBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.colSurveyedTreeID,
            this.gridColumn2,
            this.gridColumn11,
            this.colDateScheduled,
            this.colDateCompleted,
            this.colApproved,
            this.colApprovedByStaffID,
            this.colEstimatedLabourCost,
            this.colActualLabourCost,
            this.colEstimatedMaterialsCost,
            this.colActualMaterialsCost,
            this.colEstimatedEquipmentCost,
            this.colActualEquipmentCost,
            this.colEstimatedTotalCost,
            this.colActualTotalCost,
            this.colWorkOrderID,
            this.colSelfBillingInvoiceID,
            this.colFinanceSystemBillingID,
            this.colRemarks1,
            this.colGUID3,
            this.gridColumn13,
            this.colEstimatedTotalSell,
            this.colActualTotalSell,
            this.colEstimatedLabourSell,
            this.colActualLabourSell,
            this.colEstimatedEquipmentSell,
            this.colActualEquipmentSell,
            this.colEstimatedMaterialsSell,
            this.colActualMaterialsSell,
            this.gridColumn14,
            this.colPossibleFlail,
            this.colRevisitDate,
            this.colActualHours1,
            this.colEstimatedHours1,
            this.colPossibleLiveWork,
            this.colCircuitName1,
            this.colCircuitNumber1,
            this.colClientID1,
            this.colClientName1,
            this.colJobReferenceNumber,
            this.colPoleNumber1,
            this.colTreeReferenceNumber1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReferenceNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Action ID";
            this.gridColumn1.FieldName = "ActionID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyedTreeID
            // 
            this.colSurveyedTreeID.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID.Name = "colSurveyedTreeID";
            this.colSurveyedTreeID.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID.Width = 106;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Job Type ID";
            this.gridColumn2.FieldName = "JobTypeID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 79;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Date Raised";
            this.gridColumn11.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn11.FieldName = "DateRaised";
            this.gridColumn11.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 101;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colDateScheduled
            // 
            this.colDateScheduled.Caption = "Date Scheduled";
            this.colDateScheduled.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateScheduled.FieldName = "DateScheduled";
            this.colDateScheduled.Name = "colDateScheduled";
            this.colDateScheduled.OptionsColumn.AllowEdit = false;
            this.colDateScheduled.OptionsColumn.AllowFocus = false;
            this.colDateScheduled.OptionsColumn.ReadOnly = true;
            this.colDateScheduled.Visible = true;
            this.colDateScheduled.VisibleIndex = 7;
            this.colDateScheduled.Width = 96;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 8;
            this.colDateCompleted.Width = 98;
            // 
            // colApproved
            // 
            this.colApproved.Caption = "Approved";
            this.colApproved.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colApproved.FieldName = "Approved";
            this.colApproved.Name = "colApproved";
            this.colApproved.OptionsColumn.AllowEdit = false;
            this.colApproved.OptionsColumn.AllowFocus = false;
            this.colApproved.OptionsColumn.ReadOnly = true;
            this.colApproved.Visible = true;
            this.colApproved.VisibleIndex = 9;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colApprovedByStaffID
            // 
            this.colApprovedByStaffID.Caption = "Apporved By Staff ID";
            this.colApprovedByStaffID.FieldName = "ApprovedByStaffID";
            this.colApprovedByStaffID.Name = "colApprovedByStaffID";
            this.colApprovedByStaffID.OptionsColumn.AllowEdit = false;
            this.colApprovedByStaffID.OptionsColumn.AllowFocus = false;
            this.colApprovedByStaffID.OptionsColumn.ReadOnly = true;
            this.colApprovedByStaffID.Width = 124;
            // 
            // colEstimatedLabourCost
            // 
            this.colEstimatedLabourCost.Caption = "Est. Labour Cost";
            this.colEstimatedLabourCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedLabourCost.FieldName = "EstimatedLabourCost";
            this.colEstimatedLabourCost.Name = "colEstimatedLabourCost";
            this.colEstimatedLabourCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourCost.Visible = true;
            this.colEstimatedLabourCost.VisibleIndex = 19;
            this.colEstimatedLabourCost.Width = 101;
            // 
            // repositoryItemTextEditMoney2
            // 
            this.repositoryItemTextEditMoney2.AutoHeight = false;
            this.repositoryItemTextEditMoney2.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney2.Name = "repositoryItemTextEditMoney2";
            // 
            // colActualLabourCost
            // 
            this.colActualLabourCost.Caption = "Act. Labour Cost";
            this.colActualLabourCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualLabourCost.FieldName = "ActualLabourCost";
            this.colActualLabourCost.Name = "colActualLabourCost";
            this.colActualLabourCost.OptionsColumn.AllowEdit = false;
            this.colActualLabourCost.OptionsColumn.AllowFocus = false;
            this.colActualLabourCost.OptionsColumn.ReadOnly = true;
            this.colActualLabourCost.Visible = true;
            this.colActualLabourCost.VisibleIndex = 20;
            this.colActualLabourCost.Width = 102;
            // 
            // colEstimatedMaterialsCost
            // 
            this.colEstimatedMaterialsCost.Caption = "Est. Material Cost";
            this.colEstimatedMaterialsCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedMaterialsCost.FieldName = "EstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.Name = "colEstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsCost.Visible = true;
            this.colEstimatedMaterialsCost.VisibleIndex = 27;
            this.colEstimatedMaterialsCost.Width = 106;
            // 
            // colActualMaterialsCost
            // 
            this.colActualMaterialsCost.Caption = "Act. Material Cost";
            this.colActualMaterialsCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualMaterialsCost.FieldName = "ActualMaterialsCost";
            this.colActualMaterialsCost.Name = "colActualMaterialsCost";
            this.colActualMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsCost.Visible = true;
            this.colActualMaterialsCost.VisibleIndex = 28;
            this.colActualMaterialsCost.Width = 107;
            // 
            // colEstimatedEquipmentCost
            // 
            this.colEstimatedEquipmentCost.Caption = "Est. Equip. Cost";
            this.colEstimatedEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedEquipmentCost.FieldName = "EstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.Name = "colEstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentCost.Visible = true;
            this.colEstimatedEquipmentCost.VisibleIndex = 23;
            this.colEstimatedEquipmentCost.Width = 98;
            // 
            // colActualEquipmentCost
            // 
            this.colActualEquipmentCost.Caption = "Act. Equip. Cost";
            this.colActualEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualEquipmentCost.FieldName = "ActualEquipmentCost";
            this.colActualEquipmentCost.Name = "colActualEquipmentCost";
            this.colActualEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentCost.Visible = true;
            this.colActualEquipmentCost.VisibleIndex = 24;
            this.colActualEquipmentCost.Width = 99;
            // 
            // colEstimatedTotalCost
            // 
            this.colEstimatedTotalCost.Caption = "Est. Total Cost";
            this.colEstimatedTotalCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedTotalCost.FieldName = "EstimatedTotalCost";
            this.colEstimatedTotalCost.Name = "colEstimatedTotalCost";
            this.colEstimatedTotalCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalCost.Visible = true;
            this.colEstimatedTotalCost.VisibleIndex = 15;
            this.colEstimatedTotalCost.Width = 92;
            // 
            // colActualTotalCost
            // 
            this.colActualTotalCost.Caption = "Act. Total Cost";
            this.colActualTotalCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualTotalCost.FieldName = "ActualTotalCost";
            this.colActualTotalCost.Name = "colActualTotalCost";
            this.colActualTotalCost.OptionsColumn.AllowEdit = false;
            this.colActualTotalCost.OptionsColumn.AllowFocus = false;
            this.colActualTotalCost.OptionsColumn.ReadOnly = true;
            this.colActualTotalCost.Visible = true;
            this.colActualTotalCost.VisibleIndex = 16;
            this.colActualTotalCost.Width = 93;
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.Width = 77;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 82;
            // 
            // colFinanceSystemBillingID
            // 
            this.colFinanceSystemBillingID.Caption = "Finance Billing ID";
            this.colFinanceSystemBillingID.FieldName = "FinanceSystemBillingID";
            this.colFinanceSystemBillingID.Name = "colFinanceSystemBillingID";
            this.colFinanceSystemBillingID.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemBillingID.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemBillingID.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemBillingID.Width = 101;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 32;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colGUID3
            // 
            this.colGUID3.Caption = "GUID";
            this.colGUID3.FieldName = "GUID";
            this.colGUID3.Name = "colGUID3";
            this.colGUID3.OptionsColumn.AllowEdit = false;
            this.colGUID3.OptionsColumn.AllowFocus = false;
            this.colGUID3.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Job Description";
            this.gridColumn13.FieldName = "JobDescription";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 6;
            this.gridColumn13.Width = 223;
            // 
            // colEstimatedTotalSell
            // 
            this.colEstimatedTotalSell.Caption = "Est. Total Sell";
            this.colEstimatedTotalSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedTotalSell.FieldName = "EstimatedTotalSell";
            this.colEstimatedTotalSell.Name = "colEstimatedTotalSell";
            this.colEstimatedTotalSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalSell.Visible = true;
            this.colEstimatedTotalSell.VisibleIndex = 17;
            this.colEstimatedTotalSell.Width = 86;
            // 
            // colActualTotalSell
            // 
            this.colActualTotalSell.Caption = "Act. Total Sell";
            this.colActualTotalSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualTotalSell.FieldName = "ActualTotalSell";
            this.colActualTotalSell.Name = "colActualTotalSell";
            this.colActualTotalSell.OptionsColumn.AllowEdit = false;
            this.colActualTotalSell.OptionsColumn.AllowFocus = false;
            this.colActualTotalSell.OptionsColumn.ReadOnly = true;
            this.colActualTotalSell.Visible = true;
            this.colActualTotalSell.VisibleIndex = 18;
            this.colActualTotalSell.Width = 87;
            // 
            // colEstimatedLabourSell
            // 
            this.colEstimatedLabourSell.Caption = "Est. Labour Sell";
            this.colEstimatedLabourSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedLabourSell.FieldName = "EstimatedLabourSell";
            this.colEstimatedLabourSell.Name = "colEstimatedLabourSell";
            this.colEstimatedLabourSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourSell.Visible = true;
            this.colEstimatedLabourSell.VisibleIndex = 21;
            this.colEstimatedLabourSell.Width = 95;
            // 
            // colActualLabourSell
            // 
            this.colActualLabourSell.Caption = "Act. Labour Sell";
            this.colActualLabourSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualLabourSell.FieldName = "ActualLabourSell";
            this.colActualLabourSell.Name = "colActualLabourSell";
            this.colActualLabourSell.OptionsColumn.AllowEdit = false;
            this.colActualLabourSell.OptionsColumn.AllowFocus = false;
            this.colActualLabourSell.OptionsColumn.ReadOnly = true;
            this.colActualLabourSell.Visible = true;
            this.colActualLabourSell.VisibleIndex = 22;
            this.colActualLabourSell.Width = 96;
            // 
            // colEstimatedEquipmentSell
            // 
            this.colEstimatedEquipmentSell.Caption = "Est. Equip. Sell";
            this.colEstimatedEquipmentSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedEquipmentSell.FieldName = "EstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.Name = "colEstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentSell.Visible = true;
            this.colEstimatedEquipmentSell.VisibleIndex = 25;
            this.colEstimatedEquipmentSell.Width = 92;
            // 
            // colActualEquipmentSell
            // 
            this.colActualEquipmentSell.Caption = "Act. Equip. Sell";
            this.colActualEquipmentSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualEquipmentSell.FieldName = "ActualEquipmentSell";
            this.colActualEquipmentSell.Name = "colActualEquipmentSell";
            this.colActualEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentSell.Visible = true;
            this.colActualEquipmentSell.VisibleIndex = 26;
            this.colActualEquipmentSell.Width = 93;
            // 
            // colEstimatedMaterialsSell
            // 
            this.colEstimatedMaterialsSell.Caption = "Est. Material Sell";
            this.colEstimatedMaterialsSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedMaterialsSell.FieldName = "EstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.Name = "colEstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsSell.Visible = true;
            this.colEstimatedMaterialsSell.VisibleIndex = 29;
            this.colEstimatedMaterialsSell.Width = 100;
            // 
            // colActualMaterialsSell
            // 
            this.colActualMaterialsSell.Caption = "Act. Material Sell";
            this.colActualMaterialsSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualMaterialsSell.FieldName = "ActualMaterialsSell";
            this.colActualMaterialsSell.Name = "colActualMaterialsSell";
            this.colActualMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsSell.Visible = true;
            this.colActualMaterialsSell.VisibleIndex = 30;
            this.colActualMaterialsSell.Width = 101;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Achievable Clearance";
            this.gridColumn14.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn14.FieldName = "AchievableClearance";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 12;
            this.gridColumn14.Width = 124;
            // 
            // colPossibleFlail
            // 
            this.colPossibleFlail.Caption = "Possible Flail";
            this.colPossibleFlail.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colPossibleFlail.FieldName = "PossibleFlail";
            this.colPossibleFlail.Name = "colPossibleFlail";
            this.colPossibleFlail.OptionsColumn.AllowEdit = false;
            this.colPossibleFlail.OptionsColumn.AllowFocus = false;
            this.colPossibleFlail.OptionsColumn.ReadOnly = true;
            this.colPossibleFlail.Visible = true;
            this.colPossibleFlail.VisibleIndex = 10;
            this.colPossibleFlail.Width = 80;
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revist Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsColumn.AllowEdit = false;
            this.colRevisitDate.OptionsColumn.AllowFocus = false;
            this.colRevisitDate.OptionsColumn.ReadOnly = true;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 31;
            this.colRevisitDate.Width = 77;
            // 
            // colActualHours1
            // 
            this.colActualHours1.Caption = "Actual Hours";
            this.colActualHours1.ColumnEdit = this.repositoryItemTextEditHours;
            this.colActualHours1.FieldName = "ActualHours";
            this.colActualHours1.Name = "colActualHours1";
            this.colActualHours1.OptionsColumn.AllowEdit = false;
            this.colActualHours1.OptionsColumn.AllowFocus = false;
            this.colActualHours1.OptionsColumn.ReadOnly = true;
            this.colActualHours1.Visible = true;
            this.colActualHours1.VisibleIndex = 14;
            this.colActualHours1.Width = 82;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // colEstimatedHours1
            // 
            this.colEstimatedHours1.Caption = "Estimated Hours";
            this.colEstimatedHours1.ColumnEdit = this.repositoryItemTextEditHours;
            this.colEstimatedHours1.FieldName = "EstimatedHours";
            this.colEstimatedHours1.Name = "colEstimatedHours1";
            this.colEstimatedHours1.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours1.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours1.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours1.Visible = true;
            this.colEstimatedHours1.VisibleIndex = 13;
            this.colEstimatedHours1.Width = 99;
            // 
            // colPossibleLiveWork
            // 
            this.colPossibleLiveWork.Caption = "Possible Live Work";
            this.colPossibleLiveWork.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork.Name = "colPossibleLiveWork";
            this.colPossibleLiveWork.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork.Visible = true;
            this.colPossibleLiveWork.VisibleIndex = 11;
            this.colPossibleLiveWork.Width = 109;
            // 
            // colCircuitName1
            // 
            this.colCircuitName1.Caption = "Circuit Name";
            this.colCircuitName1.FieldName = "CircuitName";
            this.colCircuitName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName1.Name = "colCircuitName1";
            this.colCircuitName1.OptionsColumn.AllowEdit = false;
            this.colCircuitName1.OptionsColumn.AllowFocus = false;
            this.colCircuitName1.OptionsColumn.ReadOnly = true;
            this.colCircuitName1.Visible = true;
            this.colCircuitName1.VisibleIndex = 1;
            this.colCircuitName1.Width = 100;
            // 
            // colCircuitNumber1
            // 
            this.colCircuitNumber1.Caption = "Circuit Number";
            this.colCircuitNumber1.FieldName = "CircuitNumber";
            this.colCircuitNumber1.Name = "colCircuitNumber1";
            this.colCircuitNumber1.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber1.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber1.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber1.Visible = true;
            this.colCircuitNumber1.VisibleIndex = 4;
            this.colCircuitNumber1.Width = 91;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 0;
            this.colClientName1.Width = 104;
            // 
            // colJobReferenceNumber
            // 
            this.colJobReferenceNumber.Caption = "Job Reference #";
            this.colJobReferenceNumber.FieldName = "JobReferenceNumber";
            this.colJobReferenceNumber.Name = "colJobReferenceNumber";
            this.colJobReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colJobReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colJobReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colJobReferenceNumber.Visible = true;
            this.colJobReferenceNumber.VisibleIndex = 5;
            this.colJobReferenceNumber.Width = 115;
            // 
            // colPoleNumber1
            // 
            this.colPoleNumber1.Caption = "Pole Number";
            this.colPoleNumber1.FieldName = "PoleNumber";
            this.colPoleNumber1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber1.Name = "colPoleNumber1";
            this.colPoleNumber1.OptionsColumn.AllowEdit = false;
            this.colPoleNumber1.OptionsColumn.AllowFocus = false;
            this.colPoleNumber1.OptionsColumn.ReadOnly = true;
            this.colPoleNumber1.Visible = true;
            this.colPoleNumber1.VisibleIndex = 2;
            this.colPoleNumber1.Width = 94;
            // 
            // colTreeReferenceNumber1
            // 
            this.colTreeReferenceNumber1.Caption = "Tree Reference #";
            this.colTreeReferenceNumber1.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTreeReferenceNumber1.Name = "colTreeReferenceNumber1";
            this.colTreeReferenceNumber1.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber1.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber1.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber1.Visible = true;
            this.colTreeReferenceNumber1.VisibleIndex = 3;
            this.colTreeReferenceNumber1.Width = 120;
            // 
            // sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter
            // 
            this.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07329_UT_Surveyor_List_No_BlankTableAdapter
            // 
            this.sp07329_UT_Surveyor_List_No_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07191_UT_Permission_Document_ManagerTableAdapter
            // 
            this.sp07191_UT_Permission_Document_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp07392_UT_PD_Linked_WorkTableAdapter
            // 
            this.sp07392_UT_PD_Linked_WorkTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // sp07400_UT_PD_Unpermissioned_WorkTableAdapter
            // 
            this.sp07400_UT_PD_Unpermissioned_WorkTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Permission_Document_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1041, 542);
            this.Controls.Add(this.xtraTabControl2);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Permission_Document_Manager";
            this.Text = "Permission Document Manager";
            this.Activated += new System.EventHandler(this.frm_UT_Permission_Document_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Permission_Document_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Permission_Document_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07191UTPermissionDocumentManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSurveyors)).EndInit();
            this.popupContainerControlSurveyors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07329UTSurveyorListNoBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07392UTPDLinkedWorkBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateMask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07400UTPDUnpermissionedWorkBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraBars.BarEditItem popupContainerDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateMask;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric0DP;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedMapID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colMapOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeCreated;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private System.Windows.Forms.BindingSource sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource;
        private DataSet_UT_Mapping dataSet_UT_Mapping;
        private DataSet_UT_MappingTableAdapters.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSurveyors;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraEditors.SimpleButton btnSurveyorOK;
        private DataSet_UT_Reporting dataSet_UT_Reporting;
        private System.Windows.Forms.BindingSource sp07329UTSurveyorListNoBlankBindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07329_UT_Surveyor_List_No_BlankTableAdapter sp07329_UT_Surveyor_List_No_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayName;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private System.Windows.Forms.BindingSource sp07191UTPermissionDocumentManagerBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colSignatureFile;
        private DevExpress.XtraGrid.Columns.GridColumn colPDFFile;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToCustomerDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colSignatureDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmergencyAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestTelephonePoint;
        private DevExpress.XtraGrid.Columns.GridColumn colGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestAandE;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveAccessAvailable;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessAgreedWithLandOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessDetailsOnMap;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadsideAccessOnly;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedRevisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTPOTree;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningConservation;
        private DevExpress.XtraGrid.Columns.GridColumn colWildlifeDesignation;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecialInstructions;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimarySubName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimarySubNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLineNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLVSubName;
        private DevExpress.XtraGrid.Columns.GridColumn colLVSubNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colManagedUnitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colEnvironmentalRANumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLandscapeImpactNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionEmailed;
        private DevExpress.XtraGrid.Columns.GridColumn colPostageRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByPost;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerSalutation;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsChipRemove;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsChipOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsStackOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsOther;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToDoActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerType;
        private DataSet_UT_WorkOrderTableAdapters.sp07191_UT_Permission_Document_ManagerTableAdapter sp07191_UT_Permission_Document_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryScreenName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private System.Windows.Forms.BindingSource sp07392UTPDLinkedWorkBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPermissionID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colLandOwnerRestrictedCut;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colAchievableClearance;
        private DevExpress.XtraGrid.Columns.GridColumn colG55Category;
        private DevExpress.XtraGrid.Columns.GridColumn colESQCRCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DataSet_UT_WorkOrderTableAdapters.sp07392_UT_PD_Linked_WorkTableAdapter sp07392_UT_PD_Linked_WorkTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn colLandOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private System.Windows.Forms.BindingSource sp07400UTPDUnpermissionedWorkBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07400_UT_PD_Unpermissioned_WorkTableAdapter sp07400_UT_PD_Unpermissioned_WorkTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colDateScheduled;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colApproved;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney2;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemBillingID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleFlail;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours1;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colNotPermissionedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldCount;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colRefusalReason;
        private DevExpress.XtraGrid.Columns.GridColumn colRefusalRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colLandownerRestrictedCut1;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Feeder_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInFeederIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;
        private string i_str_selected_region_ids = "";
        private string i_str_selected_region_names = "";

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState8;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs8 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection1;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion

        public frm_UT_Feeder_Manager()
        {
            InitializeComponent();
        }

        private void frm_UT_Feeder_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 10008;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            
            sp07023_UT_Primary_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07023_UT_Primary_FilterTableAdapter.Fill(dataSet_UT.sp07023_UT_Primary_Filter);
            gridControl2.ForceInitialize();
            
            sp07022_UT_Feeder_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "FeederID");

            sp07187_UT_Contracts_Linked_To_CircuitsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState8 = new RefreshGridState(gridView8, "FeederContractID");

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            if (strPassedInFeederIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEdit1.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInFeederIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_UT_Feeder_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                else if (UpdateRefreshStatus == 2)
                {
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_UT_Feeder_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInFeederIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "PrimaryFilter", i_str_selected_region_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Locality Filter //
                int intFoundRow = 0;
                string strDistrictFilter = default_screen_settings.RetrieveSetting("PrimaryFilter");
                if (!string.IsNullOrEmpty(strDistrictFilter))
                {
                    Array arrayDistricts = strDistrictFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewClients = (GridView)gridControl2.MainView;
                    viewClients.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayDistricts)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewClients.LocateByValue(0, viewClients.Columns["PrimaryID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewClients.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewClients.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewClients.EndUpdate();
                    popupContainerEdit1.EditValue = PopupContainerEdit1_Get_Selected();
               }
                bbiRefresh.PerformClick();
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3, string strNewIDs8)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs8 != "") i_str_AddedRecordIDs8 = strNewIDs8;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
            }
            else if (i_int_FocusedGrid == 8)
            {
                view = (GridView)gridControl8.MainView;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Localities //
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 8)  // Linked Circuit Contracts //
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView8 navigator custom buttons //
            view = (GridView)gridControl8.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl8.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl8.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl8.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl8.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl8.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl8.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0 ? true : false);
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0 ? true : false);
        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_region_ids == null) i_str_selected_region_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            if (popupContainerEdit1.EditValue.ToString() == "Custom Filter" && strPassedInFeederIDs != "")  // Load passed in Trees //
            {
                sp07022_UT_Feeder_ManagerTableAdapter.Fill(this.dataSet_UT.sp07022_UT_Feeder_Manager, i_str_selected_region_ids, strPassedInFeederIDs);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                sp07022_UT_Feeder_ManagerTableAdapter.Fill(this.dataSet_UT.sp07022_UT_Feeder_Manager, i_str_selected_region_ids, "");
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["FeederID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["FeederID"])) + ',';
            }

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            // Populate Linked Circuit Contracts //
            gridControl8.MainView.BeginUpdate();
            this.RefreshGridViewState8.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07187_UT_Contracts_Linked_To_Circuits.Clear();
            }
            else
            {
                sp07187_UT_Contracts_Linked_To_CircuitsTableAdapter.Fill(dataSet_UT.sp07187_UT_Contracts_Linked_To_Circuits, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState8.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl8.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs8 != "")
            {
                strArray = i_str_AddedRecordIDs8.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl8.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["FeederContractID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs8 = "";
            }
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Feeders //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Feeder_Edit fChildForm = new frm_UT_Feeder_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "PrimaryID"));
                            fChildForm.intLinkedToRecordID2 = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SubAreaID"));
                            fChildForm.intLinkedToRecordID3 = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "RegionID"));
                            fChildForm.intLinkedToRecordID4 = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "ClientName")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "RegionName")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SubAreaName")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "PrimaryName"));
                        }

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 8:     // Linked Circuit Contracts //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl8.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Circuit_Contract_Edit fChildForm = new frm_UT_Circuit_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "FeederID"));
                            fChildForm.intLinkedToRecordID2 = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "FeederName")) + "  [" + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "FeederNumber")) + "]";
                        }

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 8:     // Related Feeder Contracts //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "FeederID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Circuit_Contract_Edit fChildForm = new frm_UT_Circuit_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Feeders //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "FeederID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Feeder_Edit fChildForm = new frm_UT_Feeder_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 8:     // Linked Feeder Contracts //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl8.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "FeederContractID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Circuit_Contract_Edit fChildForm = new frm_UT_Circuit_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Feeders //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "FeederID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Feeder_Edit fChildForm = new frm_UT_Feeder_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 8:     // Linked Feeder Contracts //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl8.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "FeederContractID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Circuit_Contract_Edit fChildForm = new frm_UT_Circuit_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Feeders //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();

                        // Check user created record or is a super user - if not de-select row //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                        }
                        intRowHandles = view.GetSelectedRows();

                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Feeders to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Feeder" : Convert.ToString(intRowHandles.Length) + " Feeders") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Feeder" : "these Feeders") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "FeederID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("feeder", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 8:  // Linked Feeder Contracts //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl8.MainView;
                        intRowHandles = view.GetSelectedRows();

                        // Check user created record or is a super user - if not de-select row //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                        }
                        intRowHandles = view.GetSelectedRows();

                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Circuit Contracts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Feeder Contract" : Convert.ToString(intRowHandles.Length) + " Linked Feeder Contracts") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Feeder Contract" : "these Linked Feeder Contracts") + " will no longer be available for selection.\n\nCaution: If you are deleting Live contracts, you should either create new live contracts for the selected feeders or make existing non-active contracts active.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "FeederContractID")) + ",";
                            }

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("feeder_contract", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Feeders //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "FeederID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Feeder_Edit fChildForm = new frm_UT_Feeder_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 8:     // Linked Feeder Contracts //
                    {
                        view = (GridView)gridControl8.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "FeederContractID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Circuit_Contract_Edit fChildForm = new frm_UT_Circuit_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Feeders - Adjust any filters then click Refresh button";
                    break;
                case "gridView8":
                    message = "No Linked Feeder Contracts Available - Select one or more Feeders to view Linked Feeder Contracts";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedRecords();
                    view = (GridView)gridControl8.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region Filter Panel

        private void btnClientFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_region_ids = "";    // Reset any prior values first //
            i_str_selected_region_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_region_ids = "";
                return "No Primary Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_region_ids = "";
                return "No Primary Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_region_ids += Convert.ToString(view.GetRowCellValue(i, "PrimaryID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_region_names = Convert.ToString(view.GetRowCellValue(i, "PrimaryName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_region_names += ", " + Convert.ToString(view.GetRowCellValue(i, "PrimaryName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_region_names;
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        #endregion


        #region GridView1
        
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    //bsiDataset.Enabled = true;
                    //bbiDatasetManager.Enabled = true;
                    bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strParentIDs = view.GetRowCellValue(view.FocusedRowHandle, "FeederID").ToString() + ",";

            DataSet_UTTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UTTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            if (view.FocusedColumn.Name == "colLinkedPoleCount")
            {
                string strRecordIDs = GetSetting.sp07035_UT_Feeder_Manager_Get_Linked_Poles(strParentIDs).ToString();
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_pole");
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedPoleCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPoleCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPoleCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPoleCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
           /* GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            string strSelectedIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubAreaID")) + ',';
            }
            try
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "locality");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }*/
        }

        #endregion


        #region GridView8

        private void gridView8_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 8;
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView8_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //
            GridView view = (GridView)sender;
            if (e.Column.ColumnType != typeof(DateTime))
            {
                GridColumnCollection cols = new GridColumnCollection(view);
                GridColumn column = cols.AddField(e.Column.FieldName);
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[column.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values != null)
                {
                    comboBox.Items.AddRange(values);
                    column.ColumnEdit = comboBox;
                }
                DevExpress.XtraGrid.Filter.FilterCustomDialog dlg = new DevExpress.XtraGrid.Filter.FilterCustomDialog(column, false);
                dlg.ShowDialog();
                e.FilterInfo = null;
                e.Handled = true;
                gridControl8.Refresh();
            }
        }

        private void gridView8_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }

        private void gridControl8_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("clone".Equals(e.Button.Tag))
                    {
                        Clone_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Clone_Record()
        {
            GridView view = (GridView)gridControl8.MainView; ;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strRecordIDs = "";
            string strParentIDs = "";
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to clone \\ apply a rate uplift to before proceeding.", "Clone \\ Apply Rate Uplift", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "FeederContractID")) + ',';
                strParentIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "FeederID")) + ',';
            }
            frm_UT_Circuit_Contract_Clone fChildForm = new frm_UT_Circuit_Contract_Clone();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = strRecordIDs;
            fChildForm.strParentIDs = strParentIDs;
            fChildForm.intRecordCount = intCount;
            fChildForm.strFormMode = "edit";
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(fChildForm.strReturnedNewIDs)) i_str_AddedRecordIDs8 = fChildForm.strReturnedNewIDs;
                this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                LoadLinkedRecords();
            }
        }

        #endregion




 

    }
}


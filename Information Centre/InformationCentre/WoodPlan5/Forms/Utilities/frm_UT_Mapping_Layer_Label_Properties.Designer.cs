namespace WoodPlan5
{
    partial class frm_UT_Mapping_Layer_Label_Properties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Mapping_Layer_Label_Properties));
            this.ceDuplicates = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.popupContainerControl_LabelAlignment = new DevExpress.XtraEditors.PopupContainerControl();
            this.alignmentControl1 = new BaseObjects.AlignmentControl();
            this.seLabelRangeTo = new DevExpress.XtraEditors.SpinEdit();
            this.tbcTransparency = new DevExpress.XtraEditors.TrackBarControl();
            this.seLabelRangeFrom = new DevExpress.XtraEditors.SpinEdit();
            this.ceShowOverlaps = new DevExpress.XtraEditors.CheckEdit();
            this.colorEdit1 = new DevExpress.XtraEditors.ColorEdit();
            this.seFontOffset = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.fontEdit1 = new DevExpress.XtraEditors.FontEdit();
            this.seFontSize = new DevExpress.XtraEditors.SpinEdit();
            this.popupContainerEditLabelPosition = new DevExpress.XtraEditors.PopupContainerEdit();
            this.colorEditLabelFontColour = new DevExpress.XtraEditors.ColorEdit();
            this.seFontAngle = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDuplicates.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl_LabelAlignment)).BeginInit();
            this.popupContainerControl_LabelAlignment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seLabelRangeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTransparency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTransparency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seLabelRangeFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowOverlaps.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fontEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabelPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditLabelFontColour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontAngle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(435, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 379);
            this.barDockControlBottom.Size = new System.Drawing.Size(435, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 379);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(435, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 379);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1});
            this.barManager1.MaxItemId = 29;
            // 
            // ceDuplicates
            // 
            this.ceDuplicates.EditValue = 1;
            this.ceDuplicates.Location = new System.Drawing.Point(290, 294);
            this.ceDuplicates.MenuManager = this.barManager1;
            this.ceDuplicates.Name = "ceDuplicates";
            this.ceDuplicates.Properties.Caption = "";
            this.ceDuplicates.Properties.ValueChecked = 1;
            this.ceDuplicates.Properties.ValueUnchecked = 0;
            this.ceDuplicates.Size = new System.Drawing.Size(122, 19);
            this.ceDuplicates.StyleController = this.layoutControl1;
            this.ceDuplicates.TabIndex = 11;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.popupContainerControl_LabelAlignment);
            this.layoutControl1.Controls.Add(this.seLabelRangeTo);
            this.layoutControl1.Controls.Add(this.ceDuplicates);
            this.layoutControl1.Controls.Add(this.tbcTransparency);
            this.layoutControl1.Controls.Add(this.seLabelRangeFrom);
            this.layoutControl1.Controls.Add(this.ceShowOverlaps);
            this.layoutControl1.Controls.Add(this.colorEdit1);
            this.layoutControl1.Controls.Add(this.seFontOffset);
            this.layoutControl1.Controls.Add(this.spinEdit1);
            this.layoutControl1.Controls.Add(this.fontEdit1);
            this.layoutControl1.Controls.Add(this.seFontSize);
            this.layoutControl1.Controls.Add(this.popupContainerEditLabelPosition);
            this.layoutControl1.Controls.Add(this.colorEditLabelFontColour);
            this.layoutControl1.Controls.Add(this.seFontAngle);
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1000, 374, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(431, 332);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // popupContainerControl_LabelAlignment
            // 
            this.popupContainerControl_LabelAlignment.Controls.Add(this.alignmentControl1);
            this.popupContainerControl_LabelAlignment.Location = new System.Drawing.Point(335, 2);
            this.popupContainerControl_LabelAlignment.Name = "popupContainerControl_LabelAlignment";
            this.popupContainerControl_LabelAlignment.Size = new System.Drawing.Size(94, 94);
            this.popupContainerControl_LabelAlignment.TabIndex = 29;
            // 
            // alignmentControl1
            // 
            this.alignmentControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.alignmentControl1.Location = new System.Drawing.Point(0, 0);
            this.alignmentControl1.Name = "alignmentControl1";
            this.alignmentControl1.Size = new System.Drawing.Size(94, 94);
            this.alignmentControl1.TabIndex = 26;
            this.alignmentControl1.AlignmentChanged += new System.EventHandler(this.alignmentControl1_AlignmentChanged);
            // 
            // seLabelRangeTo
            // 
            this.seLabelRangeTo.EditValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.seLabelRangeTo.Location = new System.Drawing.Point(290, 270);
            this.seLabelRangeTo.MenuManager = this.barManager1;
            this.seLabelRangeTo.Name = "seLabelRangeTo";
            this.seLabelRangeTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seLabelRangeTo.Properties.IsFloatValue = false;
            this.seLabelRangeTo.Properties.Mask.EditMask = "#######0 Metres";
            this.seLabelRangeTo.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.seLabelRangeTo.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.seLabelRangeTo.Size = new System.Drawing.Size(122, 20);
            this.seLabelRangeTo.StyleController = this.layoutControl1;
            this.seLabelRangeTo.TabIndex = 9;
            // 
            // tbcTransparency
            // 
            this.tbcTransparency.EditValue = null;
            this.tbcTransparency.Location = new System.Drawing.Point(92, 38);
            this.tbcTransparency.MinimumSize = new System.Drawing.Size(0, 26);
            this.tbcTransparency.Name = "tbcTransparency";
            this.tbcTransparency.Properties.AutoSize = false;
            this.tbcTransparency.Properties.Maximum = 100;
            this.tbcTransparency.Properties.ShowValueToolTip = true;
            this.tbcTransparency.Properties.TickFrequency = 5;
            this.tbcTransparency.Size = new System.Drawing.Size(320, 26);
            this.tbcTransparency.StyleController = this.layoutControl1;
            this.tbcTransparency.TabIndex = 6;
            // 
            // seLabelRangeFrom
            // 
            this.seLabelRangeFrom.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seLabelRangeFrom.Location = new System.Drawing.Point(92, 270);
            this.seLabelRangeFrom.MenuManager = this.barManager1;
            this.seLabelRangeFrom.Name = "seLabelRangeFrom";
            this.seLabelRangeFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seLabelRangeFrom.Properties.IsFloatValue = false;
            this.seLabelRangeFrom.Properties.Mask.EditMask = "#######0 Metres";
            this.seLabelRangeFrom.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.seLabelRangeFrom.Size = new System.Drawing.Size(121, 20);
            this.seLabelRangeFrom.StyleController = this.layoutControl1;
            this.seLabelRangeFrom.TabIndex = 8;
            // 
            // ceShowOverlaps
            // 
            this.ceShowOverlaps.EditValue = 1;
            this.ceShowOverlaps.Location = new System.Drawing.Point(92, 294);
            this.ceShowOverlaps.MenuManager = this.barManager1;
            this.ceShowOverlaps.Name = "ceShowOverlaps";
            this.ceShowOverlaps.Properties.Caption = "";
            this.ceShowOverlaps.Properties.ValueChecked = 1;
            this.ceShowOverlaps.Properties.ValueUnchecked = 0;
            this.ceShowOverlaps.Size = new System.Drawing.Size(121, 19);
            this.ceShowOverlaps.StyleController = this.layoutControl1;
            this.ceShowOverlaps.TabIndex = 31;
            // 
            // colorEdit1
            // 
            this.colorEdit1.EditValue = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.colorEdit1.Location = new System.Drawing.Point(92, 121);
            this.colorEdit1.MenuManager = this.barManager1;
            this.colorEdit1.Name = "colorEdit1";
            this.colorEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEdit1.Properties.StoreColorAsInteger = true;
            this.colorEdit1.Size = new System.Drawing.Size(121, 20);
            this.colorEdit1.StyleController = this.layoutControl1;
            this.colorEdit1.TabIndex = 7;
            // 
            // seFontOffset
            // 
            this.seFontOffset.EditValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.seFontOffset.Location = new System.Drawing.Point(290, 246);
            this.seFontOffset.MenuManager = this.barManager1;
            this.seFontOffset.Name = "seFontOffset";
            this.seFontOffset.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seFontOffset.Properties.IsFloatValue = false;
            this.seFontOffset.Properties.Mask.EditMask = "N00";
            this.seFontOffset.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.seFontOffset.Size = new System.Drawing.Size(122, 20);
            this.seFontOffset.StyleController = this.layoutControl1;
            this.seFontOffset.TabIndex = 30;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(290, 121);
            this.spinEdit1.MenuManager = this.barManager1;
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "N00";
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(122, 20);
            this.spinEdit1.StyleController = this.layoutControl1;
            this.spinEdit1.TabIndex = 8;
            // 
            // fontEdit1
            // 
            this.fontEdit1.EditValue = "Tahoma";
            this.fontEdit1.Location = new System.Drawing.Point(92, 198);
            this.fontEdit1.MenuManager = this.barManager1;
            this.fontEdit1.Name = "fontEdit1";
            this.fontEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fontEdit1.Size = new System.Drawing.Size(121, 20);
            this.fontEdit1.StyleController = this.layoutControl1;
            this.fontEdit1.TabIndex = 5;
            // 
            // seFontSize
            // 
            this.seFontSize.EditValue = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.seFontSize.Location = new System.Drawing.Point(290, 198);
            this.seFontSize.MenuManager = this.barManager1;
            this.seFontSize.Name = "seFontSize";
            this.seFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seFontSize.Properties.IsFloatValue = false;
            this.seFontSize.Properties.Mask.EditMask = "N00";
            this.seFontSize.Properties.MaxValue = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.seFontSize.Properties.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.seFontSize.Size = new System.Drawing.Size(122, 20);
            this.seFontSize.StyleController = this.layoutControl1;
            this.seFontSize.TabIndex = 5;
            // 
            // popupContainerEditLabelPosition
            // 
            this.popupContainerEditLabelPosition.EditValue = "CentreRight";
            this.popupContainerEditLabelPosition.Location = new System.Drawing.Point(92, 246);
            this.popupContainerEditLabelPosition.MenuManager = this.barManager1;
            this.popupContainerEditLabelPosition.Name = "popupContainerEditLabelPosition";
            this.popupContainerEditLabelPosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditLabelPosition.Properties.PopupControl = this.popupContainerControl_LabelAlignment;
            this.popupContainerEditLabelPosition.Properties.PopupFormSize = new System.Drawing.Size(94, 120);
            this.popupContainerEditLabelPosition.Size = new System.Drawing.Size(121, 20);
            this.popupContainerEditLabelPosition.StyleController = this.layoutControl1;
            this.popupContainerEditLabelPosition.TabIndex = 10;
            this.popupContainerEditLabelPosition.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.popupContainerEditLabelPosition_QueryPopUp);
            // 
            // colorEditLabelFontColour
            // 
            this.colorEditLabelFontColour.EditValue = System.Drawing.Color.Black;
            this.colorEditLabelFontColour.Location = new System.Drawing.Point(92, 222);
            this.colorEditLabelFontColour.MenuManager = this.barManager1;
            this.colorEditLabelFontColour.Name = "colorEditLabelFontColour";
            this.colorEditLabelFontColour.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditLabelFontColour.Properties.StoreColorAsInteger = true;
            this.colorEditLabelFontColour.Size = new System.Drawing.Size(121, 20);
            this.colorEditLabelFontColour.StyleController = this.layoutControl1;
            this.colorEditLabelFontColour.TabIndex = 5;
            // 
            // seFontAngle
            // 
            this.seFontAngle.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seFontAngle.Location = new System.Drawing.Point(290, 222);
            this.seFontAngle.MenuManager = this.barManager1;
            this.seFontAngle.Name = "seFontAngle";
            this.seFontAngle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seFontAngle.Properties.IsFloatValue = false;
            this.seFontAngle.Properties.Mask.EditMask = "N00";
            this.seFontAngle.Properties.MaxValue = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.seFontAngle.Size = new System.Drawing.Size(122, 20);
            this.seFontAngle.StyleController = this.layoutControl1;
            this.seFontAngle.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup8,
            this.layoutControlGroup9,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(431, 332);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Label Lines and Text";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(421, 73);
            this.layoutControlGroup4.Text = "Label Lines and Text";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.tbcTransparency;
            this.layoutControlItem3.CustomizationFormText = "Transparency:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(123, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(397, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Transparency:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Label Lines";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 83);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(421, 67);
            this.layoutControlGroup8.Text = "Label Lines";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.colorEdit1;
            this.layoutControlItem4.CustomizationFormText = "Line Colour:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem4.Text = "Line Colour:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.spinEdit1;
            this.layoutControlItem5.CustomizationFormText = "Line Width:";
            this.layoutControlItem5.Location = new System.Drawing.Point(198, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(199, 24);
            this.layoutControlItem5.Text = "Line Width:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Label Text";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 160);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(421, 162);
            this.layoutControlGroup9.Text = "Label Text";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.fontEdit1;
            this.layoutControlItem6.CustomizationFormText = "Font:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem6.Text = "Font:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.seFontSize;
            this.layoutControlItem7.CustomizationFormText = "Font Size:";
            this.layoutControlItem7.Location = new System.Drawing.Point(198, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(199, 24);
            this.layoutControlItem7.Text = "Font Size:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.colorEditLabelFontColour;
            this.layoutControlItem8.CustomizationFormText = "Font Colour:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem8.Text = "Font Colour:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.seFontAngle;
            this.layoutControlItem9.CustomizationFormText = "Font Angle:";
            this.layoutControlItem9.Location = new System.Drawing.Point(198, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(199, 24);
            this.layoutControlItem9.Text = "Font Angle:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.popupContainerEditLabelPosition;
            this.layoutControlItem10.CustomizationFormText = "Position:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem10.Text = "Position:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.seFontOffset;
            this.layoutControlItem11.CustomizationFormText = "Offset:";
            this.layoutControlItem11.Location = new System.Drawing.Point(198, 48);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(199, 24);
            this.layoutControlItem11.Text = "Offset:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.seLabelRangeFrom;
            this.layoutControlItem12.CustomizationFormText = "Visible Range:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem12.Text = "Visible Range:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.seLabelRangeTo;
            this.layoutControlItem13.CustomizationFormText = "To:";
            this.layoutControlItem13.Location = new System.Drawing.Point(198, 72);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(199, 24);
            this.layoutControlItem13.Text = "To:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.ceShowOverlaps;
            this.layoutControlItem14.CustomizationFormText = "Overlapping:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(198, 23);
            this.layoutControlItem14.Text = "Overlapping:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.ceDuplicates;
            this.layoutControlItem15.CustomizationFormText = "Duplicates:";
            this.layoutControlItem15.Location = new System.Drawing.Point(198, 96);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(199, 23);
            this.layoutControlItem15.Text = "Duplicates:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(70, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 73);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(421, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 150);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(421, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(224, 345);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 22);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(134, 345);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(78, 22);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Add...";
            this.barSubItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem1.Glyph")));
            this.barSubItem1.Id = 25;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // frm_UT_Mapping_Layer_Label_Properties
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(435, 379);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_UT_Mapping_Layer_Label_Properties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Label Layer - Properties";
            this.Load += new System.EventHandler(this.frm_UT_Mapping_Layer_Label_Properties_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDuplicates.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl_LabelAlignment)).EndInit();
            this.popupContainerControl_LabelAlignment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.seLabelRangeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTransparency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTransparency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seLabelRangeFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowOverlaps.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fontEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabelPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditLabelFontColour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontAngle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.TrackBarControl tbcTransparency;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.ColorEdit colorEdit1;
        private DevExpress.XtraEditors.FontEdit fontEdit1;
        private DevExpress.XtraEditors.SpinEdit seFontSize;
        private DevExpress.XtraEditors.ColorEdit colorEditLabelFontColour;
        private DevExpress.XtraEditors.SpinEdit seFontAngle;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditLabelPosition;
        private DevExpress.XtraEditors.SpinEdit seFontOffset;
        private DevExpress.XtraEditors.SpinEdit seLabelRangeFrom;
        private DevExpress.XtraEditors.SpinEdit seLabelRangeTo;
        private DevExpress.XtraEditors.CheckEdit ceShowOverlaps;
        private DevExpress.XtraEditors.CheckEdit ceDuplicates;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl_LabelAlignment;
        private BaseObjects.AlignmentControl alignmentControl1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}

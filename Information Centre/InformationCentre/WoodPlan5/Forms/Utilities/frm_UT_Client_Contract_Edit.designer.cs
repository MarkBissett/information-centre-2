﻿namespace WoodPlan5
{
    partial class frm_UT_Client_Contract_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Client_Contract_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lueContractTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07431UTClientContractTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Quote = new WoodPlan5.DataSet_UT_Quote();
            this.sp07428UTContractTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueClientID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07427UTClientListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForContractTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp07428_UT_Contract_Type_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07428_UT_Contract_Type_ListTableAdapter();
            this.sp07431_UT_Client_Contract_Type_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07431_UT_Client_Contract_Type_ListTableAdapter();
            this.sp07427_UT_Client_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07427_UT_Client_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueContractTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07431UTClientContractTypeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07428UTContractTypeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07427UTClientListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1236, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 577);
            this.barDockControlBottom.Size = new System.Drawing.Size(1236, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 551);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1236, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 551);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.ItemForClientID});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1236, 551);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(439, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(263, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.Location = new System.Drawing.Point(451, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueContractTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueClientID);
            this.editFormDataLayoutControlGroup.DataSource = this.sp07431UTClientContractTypeListBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1943, 239, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1236, 551);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // lueContractTypeID
            // 
            this.lueContractTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07431UTClientContractTypeListBindingSource, "ContractTypeID", true));
            this.lueContractTypeID.Location = new System.Drawing.Point(88, 59);
            this.lueContractTypeID.MenuManager = this.barManager1;
            this.lueContractTypeID.Name = "lueContractTypeID";
            this.lueContractTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueContractTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ContractType", "Contract Type", 79, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueContractTypeID.Properties.DataSource = this.sp07428UTContractTypeListBindingSource;
            this.lueContractTypeID.Properties.DisplayMember = "ContractType";
            this.lueContractTypeID.Properties.NullText = "";
            this.lueContractTypeID.Properties.NullValuePrompt = "-- Please Select Contract Type--";
            this.lueContractTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueContractTypeID.Properties.ValueMember = "ContractTypeID";
            this.lueContractTypeID.Size = new System.Drawing.Size(1136, 20);
            this.lueContractTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueContractTypeID.TabIndex = 123;
            this.lueContractTypeID.Tag = "Contract Type";
            this.lueContractTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07431UTClientContractTypeListBindingSource
            // 
            this.sp07431UTClientContractTypeListBindingSource.DataMember = "sp07431_UT_Client_Contract_Type_List";
            this.sp07431UTClientContractTypeListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // dataSet_UT_Quote
            // 
            this.dataSet_UT_Quote.DataSetName = "DataSet_UT_Quote";
            this.dataSet_UT_Quote.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp07428UTContractTypeListBindingSource
            // 
            this.sp07428UTContractTypeListBindingSource.DataMember = "sp07428_UT_Contract_Type_List";
            this.sp07428UTContractTypeListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // lueClientID
            // 
            this.lueClientID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07431UTClientContractTypeListBindingSource, "ClientID", true));
            this.lueClientID.Location = new System.Drawing.Point(88, 35);
            this.lueClientID.MenuManager = this.barManager1;
            this.lueClientID.Name = "lueClientID";
            this.lueClientID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueClientID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ClientCode", "Client Code", 78, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ClientName", "Client Name", 67, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueClientID.Properties.DataSource = this.sp07427UTClientListBindingSource;
            this.lueClientID.Properties.DisplayMember = "ClientName";
            this.lueClientID.Properties.NullText = "";
            this.lueClientID.Properties.NullValuePrompt = "-- Please Select Client --";
            this.lueClientID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueClientID.Properties.ValueMember = "ClientID";
            this.lueClientID.Size = new System.Drawing.Size(1136, 20);
            this.lueClientID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueClientID.TabIndex = 124;
            this.lueClientID.Tag = "Client";
            this.lueClientID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07427UTClientListBindingSource
            // 
            this.sp07427UTClientListBindingSource.DataMember = "sp07427_UT_Client_List";
            this.sp07427UTClientListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForContractTypeID,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 47);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1216, 484);
            // 
            // ItemForContractTypeID
            // 
            this.ItemForContractTypeID.Control = this.lueContractTypeID;
            this.ItemForContractTypeID.CustomizationFormText = "Contract Type:";
            this.ItemForContractTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForContractTypeID.Name = "ItemForContractTypeID";
            this.ItemForContractTypeID.Size = new System.Drawing.Size(1216, 24);
            this.ItemForContractTypeID.Text = "Contract Type:";
            this.ItemForContractTypeID.TextSize = new System.Drawing.Size(73, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1216, 460);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(439, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(702, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(514, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.lueClientID;
            this.ItemForClientID.CustomizationFormText = "Client:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(1216, 24);
            this.ItemForClientID.Text = "Client:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(73, 13);
            // 
            // sp07428_UT_Contract_Type_ListTableAdapter
            // 
            this.sp07428_UT_Contract_Type_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07431_UT_Client_Contract_Type_ListTableAdapter
            // 
            this.sp07431_UT_Client_Contract_Type_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07427_UT_Client_ListTableAdapter
            // 
            this.sp07427_UT_Client_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Client_Contract_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1236, 607);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Client_Contract_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Client Contracts";
            this.Activated += new System.EventHandler(this.frm_UT_Client_Contract_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Client_Contract_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Client_Contract_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueContractTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07431UTClientContractTypeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07428UTContractTypeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07427UTClientListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DataSet_UT_Quote dataSet_UT_Quote;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.BindingSource sp07431UTClientContractTypeListBindingSource;
        private System.Windows.Forms.BindingSource sp07428UTContractTypeListBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07428_UT_Contract_Type_ListTableAdapter sp07428_UT_Contract_Type_ListTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp07431_UT_Client_Contract_Type_ListTableAdapter sp07431_UT_Client_Contract_Type_ListTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.LookUpEdit lueContractTypeID;
        private DevExpress.XtraEditors.LookUpEdit lueClientID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractTypeID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private System.Windows.Forms.BindingSource sp07427UTClientListBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07427_UT_Client_ListTableAdapter sp07427_UT_Client_ListTableAdapter;


    }
}

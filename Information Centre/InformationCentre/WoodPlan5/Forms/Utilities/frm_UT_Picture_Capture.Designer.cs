﻿namespace WoodPlan5
{
    partial class frm_UT_Picture_Capture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Picture_Capture));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bbiTakePicture = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRotateAntiClockwise = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRotateClockwise = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSelectedDrawingTool = new DevExpress.XtraBars.BarSubItem();
            this.bciNoDrawing = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawFreehand = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawRectangle = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawElypsis = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawLine = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawArrow = new DevExpress.XtraBars.BarCheckItem();
            this.beiPenWidth = new DevExpress.XtraBars.BarEditItem();
            this.beiArrowSize = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.beiPenColour = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemColorPickEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit();
            this.bbiClearDrawing = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSavePicture = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClearPicture = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bciViewAdditionalInfo = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.memoEditPictureRemarks = new DevExpress.XtraEditors.MemoEdit();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditPictureRemarks.Properties)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            90,
            0,
            0,
            65536});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            this.repositoryItemSpinEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit1_EditValueChanged);
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar4});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControlRight);
            this.barManager2.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bciViewAdditionalInfo,
            this.bbiTakePicture,
            this.beiPenWidth,
            this.bbiSavePicture,
            this.bbiClearPicture,
            this.barButtonItem1,
            this.barButtonItem2,
            this.bbiRotateAntiClockwise,
            this.bbiRotateClockwise,
            this.bbiClearDrawing,
            this.beiPenColour,
            this.bsiSelectedDrawingTool,
            this.bciNoDrawing,
            this.bciDrawFreehand,
            this.bciDrawRectangle,
            this.bciDrawElypsis,
            this.bciDrawLine,
            this.bciDrawArrow,
            this.beiArrowSize});
            this.barManager2.MaxItemId = 17;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemColorPickEdit1,
            this.repositoryItemSpinEdit2});
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 5";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar4.FloatLocation = new System.Drawing.Point(443, 264);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTakePicture),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateAntiClockwise, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateClockwise),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedDrawingTool, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiPenWidth),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiArrowSize),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiPenColour),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearDrawing),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSavePicture, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearPicture, true)});
            this.bar4.OptionsBar.AllowQuickCustomization = false;
            this.bar4.OptionsBar.DisableClose = true;
            this.bar4.OptionsBar.DisableCustomization = true;
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar4.Text = "Picture Toolbar";
            // 
            // bbiTakePicture
            // 
            this.bbiTakePicture.Caption = "Take Picture";
            this.bbiTakePicture.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiTakePicture.Glyph")));
            this.bbiTakePicture.Id = 16;
            this.bbiTakePicture.Name = "bbiTakePicture";
            this.bbiTakePicture.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Take Picture - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to start the camera so a picture can be captured.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiTakePicture.SuperTip = superToolTip1;
            this.bbiTakePicture.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTakePicture_ItemClick);
            // 
            // bbiRotateAntiClockwise
            // 
            this.bbiRotateAntiClockwise.Caption = "Rotate Ant-clockwise";
            this.bbiRotateAntiClockwise.Enabled = false;
            this.bbiRotateAntiClockwise.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRotateAntiClockwise.Glyph")));
            this.bbiRotateAntiClockwise.Id = 1;
            this.bbiRotateAntiClockwise.Name = "bbiRotateAntiClockwise";
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Rotate Picture - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to rotate the picture anti-clockwise. Click repeatedly to rotate 90 degr" +
    "ees each time.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiRotateAntiClockwise.SuperTip = superToolTip2;
            this.bbiRotateAntiClockwise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAntiClockwise_ItemClick);
            // 
            // bbiRotateClockwise
            // 
            this.bbiRotateClockwise.Caption = "Rotate Clockwise";
            this.bbiRotateClockwise.Enabled = false;
            this.bbiRotateClockwise.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRotateClockwise.Glyph")));
            this.bbiRotateClockwise.Id = 2;
            this.bbiRotateClockwise.Name = "bbiRotateClockwise";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Rotate Picture - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to rotate the picture clockwise. Click repeatedly to rotate 90 degrees e" +
    "ach time.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiRotateClockwise.SuperTip = superToolTip3;
            this.bbiRotateClockwise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateClockwise_ItemClick);
            // 
            // bsiSelectedDrawingTool
            // 
            this.bsiSelectedDrawingTool.Caption = "Draw [None]";
            this.bsiSelectedDrawingTool.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiSelectedDrawingTool.Glyph")));
            this.bsiSelectedDrawingTool.Id = 9;
            this.bsiSelectedDrawingTool.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciNoDrawing),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawFreehand),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawRectangle),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawElypsis),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawLine),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawArrow)});
            this.bsiSelectedDrawingTool.Name = "bsiSelectedDrawingTool";
            this.bsiSelectedDrawingTool.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bciNoDrawing
            // 
            this.bciNoDrawing.Caption = "No Drawing Object";
            this.bciNoDrawing.Glyph = ((System.Drawing.Image)(resources.GetObject("bciNoDrawing.Glyph")));
            this.bciNoDrawing.GroupIndex = 1;
            this.bciNoDrawing.Id = 10;
            this.bciNoDrawing.Name = "bciNoDrawing";
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "No Drawing Object - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to toggle off the ability to draw freehand on top of captured pictures. " +
    "\r\n\r\nIf switched on, click onto the picture with the mouse \\ stylus and draw.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bciNoDrawing.SuperTip = superToolTip4;
            this.bciNoDrawing.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciNoDrawing_CheckedChanged);
            // 
            // bciDrawFreehand
            // 
            this.bciDrawFreehand.Caption = "Freehand";
            this.bciDrawFreehand.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawFreehand.Glyph")));
            this.bciDrawFreehand.GroupIndex = 1;
            this.bciDrawFreehand.Id = 11;
            this.bciDrawFreehand.Name = "bciDrawFreehand";
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem5.Image")));
            toolTipTitleItem5.Text = "Draw Freehand - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to toggle on the ability to draw freehand on top of captured pictures. \r" +
    "\n\r\nIf switched on, click onto the picture with the mouse \\ stylus and draw.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bciDrawFreehand.SuperTip = superToolTip5;
            this.bciDrawFreehand.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawFreehand_CheckedChanged);
            // 
            // bciDrawRectangle
            // 
            this.bciDrawRectangle.Caption = "Rectangle";
            this.bciDrawRectangle.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawRectangle.Glyph")));
            this.bciDrawRectangle.GroupIndex = 1;
            this.bciDrawRectangle.Id = 12;
            this.bciDrawRectangle.Name = "bciDrawRectangle";
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem6.Image")));
            toolTipTitleItem6.Text = "Draw Rectangle - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to toggle on the ability to draw rectangles on top of captured pictures." +
    " \r\n\r\nIf switched on, click onto the picture with the mouse \\ stylus and drag out" +
    " the size of the rectangle.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bciDrawRectangle.SuperTip = superToolTip6;
            this.bciDrawRectangle.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawRectangle_CheckedChanged);
            // 
            // bciDrawElypsis
            // 
            this.bciDrawElypsis.Caption = "Elypsis";
            this.bciDrawElypsis.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawElypsis.Glyph")));
            this.bciDrawElypsis.GroupIndex = 1;
            this.bciDrawElypsis.Id = 13;
            this.bciDrawElypsis.Name = "bciDrawElypsis";
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem7.Image")));
            toolTipTitleItem7.Text = "Draw Elypsis - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to toggle on the ability to draw elypsis on top of captured pictures. \r\n" +
    "\r\nIf switched on, click onto the picture with the mouse \\ stylus and drag out th" +
    "e size of the elypsis.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bciDrawElypsis.SuperTip = superToolTip7;
            this.bciDrawElypsis.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawElypsis_CheckedChanged);
            // 
            // bciDrawLine
            // 
            this.bciDrawLine.Caption = "Line";
            this.bciDrawLine.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawLine.Glyph")));
            this.bciDrawLine.GroupIndex = 1;
            this.bciDrawLine.Id = 14;
            this.bciDrawLine.Name = "bciDrawLine";
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem8.Image")));
            toolTipTitleItem8.Text = "Draw Line - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to toggle on the ability to draw lines on top of captured pictures. \r\n\r\n" +
    "If switched on, click onto the picture with the mouse \\ stylus and drag out the " +
    "size of the line.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bciDrawLine.SuperTip = superToolTip8;
            this.bciDrawLine.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawLine_CheckedChanged);
            // 
            // bciDrawArrow
            // 
            this.bciDrawArrow.Caption = "Arrow";
            this.bciDrawArrow.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawArrow.Glyph")));
            this.bciDrawArrow.GroupIndex = 1;
            this.bciDrawArrow.Id = 15;
            this.bciDrawArrow.Name = "bciDrawArrow";
            toolTipTitleItem9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem9.Image")));
            toolTipTitleItem9.Text = "Draw Arrow - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to toggle on the ability to draw arrows on top of captured pictures. \r\n\r" +
    "\nIf switched on, click onto the picture with the mouse \\ stylus and drag out the" +
    " size of the arrow.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bciDrawArrow.SuperTip = superToolTip9;
            this.bciDrawArrow.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawArrow_CheckedChanged);
            // 
            // beiPenWidth
            // 
            this.beiPenWidth.Caption = "Width:";
            this.beiPenWidth.Edit = this.repositoryItemSpinEdit1;
            this.beiPenWidth.EditValue = 5;
            this.beiPenWidth.Id = 17;
            this.beiPenWidth.Name = "beiPenWidth";
            this.beiPenWidth.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.beiPenWidth.Width = 29;
            // 
            // beiArrowSize
            // 
            this.beiArrowSize.Caption = "Arrow Size";
            this.beiArrowSize.Edit = this.repositoryItemSpinEdit2;
            this.beiArrowSize.EditValue = 3;
            this.beiArrowSize.Enabled = false;
            this.beiArrowSize.Id = 16;
            this.beiArrowSize.Name = "beiArrowSize";
            this.beiArrowSize.Width = 28;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.IsFloatValue = false;
            this.repositoryItemSpinEdit2.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            this.repositoryItemSpinEdit2.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2_EditValueChanged);
            // 
            // beiPenColour
            // 
            this.beiPenColour.Caption = "Colour:";
            this.beiPenColour.Edit = this.repositoryItemColorPickEdit1;
            this.beiPenColour.Id = 4;
            this.beiPenColour.Name = "beiPenColour";
            this.beiPenColour.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.beiPenColour.Width = 45;
            // 
            // repositoryItemColorPickEdit1
            // 
            this.repositoryItemColorPickEdit1.AutoHeight = false;
            this.repositoryItemColorPickEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorPickEdit1.Name = "repositoryItemColorPickEdit1";
            this.repositoryItemColorPickEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemColorPickEdit1_EditValueChanged);
            // 
            // bbiClearDrawing
            // 
            this.bbiClearDrawing.Caption = "Clear Drawing";
            this.bbiClearDrawing.Enabled = false;
            this.bbiClearDrawing.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiClearDrawing.Glyph")));
            this.bbiClearDrawing.Id = 3;
            this.bbiClearDrawing.Name = "bbiClearDrawing";
            toolTipTitleItem10.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem10.Image")));
            toolTipTitleItem10.Text = "Clear Drawing - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to clear any drawing made on top of the picture.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.bbiClearDrawing.SuperTip = superToolTip10;
            this.bbiClearDrawing.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearDrawing_ItemClick);
            // 
            // bbiSavePicture
            // 
            this.bbiSavePicture.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiSavePicture.Caption = "Save";
            this.bbiSavePicture.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSavePicture.Glyph")));
            this.bbiSavePicture.Id = 19;
            this.bbiSavePicture.Name = "bbiSavePicture";
            this.bbiSavePicture.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem11.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem11.Image")));
            toolTipTitleItem11.Text = "Save - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to Save to picture.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.bbiSavePicture.SuperTip = superToolTip11;
            this.bbiSavePicture.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSavePicture_ItemClick);
            // 
            // bbiClearPicture
            // 
            this.bbiClearPicture.Caption = "Clear";
            this.bbiClearPicture.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiClearPicture.Glyph")));
            this.bbiClearPicture.Id = 20;
            this.bbiClearPicture.Name = "bbiClearPicture";
            this.bbiClearPicture.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem12.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem12.Image")));
            toolTipTitleItem12.Text = "Clear - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to Clear the currently viewed picture.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.bbiClearPicture.SuperTip = superToolTip12;
            this.bbiClearPicture.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearPicture_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(684, 26);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(684, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 568);
            this.barDockControl2.Size = new System.Drawing.Size(684, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 568);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(684, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 568);
            // 
            // bciViewAdditionalInfo
            // 
            this.bciViewAdditionalInfo.Caption = "View Additional Info";
            this.bciViewAdditionalInfo.Id = 15;
            this.bciViewAdditionalInfo.Name = "bciViewAdditionalInfo";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Create SRF Permission Document";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 21;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Email To Customer";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 22;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip13.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem13.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Text = "Save Button - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.bbiFormSave.SuperTip = superToolTip13;
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip14.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem14.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Appearance.Options.UseImage = true;
            toolTipTitleItem14.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Text = "Cancel Button - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.bbiFormCancel.SuperTip = superToolTip14;
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip15.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem15.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Appearance.Options.UseImage = true;
            toolTipTitleItem15.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Text = "Form Mode - Information";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.barStaticItemFormMode.SuperTip = superToolTip15;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(684, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 568);
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.memoEditPictureRemarks);
            this.groupControl3.Location = new System.Drawing.Point(0, 495);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(602, 73);
            this.groupControl3.TabIndex = 16;
            this.groupControl3.Text = "Picture Remarks";
            // 
            // memoEditPictureRemarks
            // 
            this.memoEditPictureRemarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEditPictureRemarks.Location = new System.Drawing.Point(2, 21);
            this.memoEditPictureRemarks.Name = "memoEditPictureRemarks";
            this.memoEditPictureRemarks.Size = new System.Drawing.Size(598, 50);
            this.memoEditPictureRemarks.TabIndex = 15;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraScrollableControl1.Controls.Add(this.pictureEdit1);
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 30);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(684, 459);
            this.xtraScrollableControl1.TabIndex = 13;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.AllowScrollViaMouseDrag = false;
            this.pictureEdit1.Properties.NullText = "No Image - Click Take Picture button";
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.pictureEdit1.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.True;
            this.pictureEdit1.Size = new System.Drawing.Size(200, 200);
            this.pictureEdit1.TabIndex = 12;
            this.pictureEdit1.ImageChanged += new System.EventHandler(this.pictureEdit1_ImageChanged);
            this.pictureEdit1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureEdit1_Paint);
            this.pictureEdit1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureEdit1_MouseDown);
            this.pictureEdit1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureEdit1_MouseMove);
            this.pictureEdit1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureEdit1_MouseUp);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(608, 543);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 23;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frm_UT_Picture_Capture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 568);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.standaloneBarDockControl1);
            this.Controls.Add(this.xtraScrollableControl1);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "frm_UT_Picture_Capture";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Take Picture";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Picture_Capture_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Picture_Capture_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditPictureRemarks.Properties)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarCheckItem bciViewAdditionalInfo;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem bbiTakePicture;
        private DevExpress.XtraBars.BarEditItem beiPenWidth;
        private DevExpress.XtraBars.BarButtonItem bbiSavePicture;
        private DevExpress.XtraBars.BarButtonItem bbiClearPicture;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.MemoEdit memoEditPictureRemarks;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAntiClockwise;
        private DevExpress.XtraBars.BarButtonItem bbiRotateClockwise;
        private DevExpress.XtraBars.BarButtonItem bbiClearDrawing;
        private DevExpress.XtraBars.BarEditItem beiPenColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit repositoryItemColorPickEdit1;
        private DevExpress.XtraBars.BarSubItem bsiSelectedDrawingTool;
        private DevExpress.XtraBars.BarCheckItem bciNoDrawing;
        private DevExpress.XtraBars.BarCheckItem bciDrawFreehand;
        private DevExpress.XtraBars.BarCheckItem bciDrawRectangle;
        private DevExpress.XtraBars.BarCheckItem bciDrawElypsis;
        private DevExpress.XtraBars.BarCheckItem bciDrawLine;
        private DevExpress.XtraBars.BarCheckItem bciDrawArrow;
        private DevExpress.XtraBars.BarEditItem beiArrowSize;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
    }
}
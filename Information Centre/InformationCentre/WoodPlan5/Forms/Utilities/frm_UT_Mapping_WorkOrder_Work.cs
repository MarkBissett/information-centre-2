using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Mapping_WorkOrder_Work : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        BaseObjects.GridCheckMarksSelection selection1;
        public string _PassedInSurveyedPoleIDs = "";
        public string _SelectedActionIDs = "";
        public string _DoWhat = "";

        #endregion
        
        public frm_UT_Mapping_WorkOrder_Work()
        {
            InitializeComponent();
        }

        private void frm_UT_Mapping_WorkOrder_Work_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500222;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp07469_UT_Work_Linked_To_Surveyed_PolesTableAdapter.Connection.ConnectionString = strConnectionString;

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            selection1.CheckMarkColumn.Fixed = FixedStyle.Left;

            gridControl1.ForceInitialize();
            LoadData();
            GridView view = (GridView)gridControl1.MainView;
            view.ExpandAllGroups();
            selection1.SelectAll();

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            try
            {
                sp07469_UT_Work_Linked_To_Surveyed_PolesTableAdapter.Fill(dataSet_UT_WorkOrder.sp07469_UT_Work_Linked_To_Surveyed_Poles, _PassedInSurveyedPoleIDs);
            }
            catch (Exception ex)
            {
                view.EndUpdate();
                return;
            }
            view.EndUpdate();
        }

        private void btnCreateNew_Click(object sender, EventArgs e)
        {
            if (selection1.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Tick one or more work records to link to a <b>new</b> work order before proceeding!", "Create New Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }
            _SelectedActionIDs = "";
            GridView view = (GridView)gridControl1.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1) _SelectedActionIDs += view.GetRowCellValue(i, "ActionID").ToString() + ",";
            }
            _DoWhat = "CreateNew";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnAddToExisting_Click(object sender, EventArgs e)
        {
            if (selection1.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Tick one or more work records to add to an <b>existing</b> work order before proceeding!", "Add to Existing Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }
            _SelectedActionIDs = "";
            GridView view = (GridView)gridControl1.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1) _SelectedActionIDs += view.GetRowCellValue(i, "ActionID").ToString() + ",";
            }
            _DoWhat = "AddExisting";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "The selected Poles have no work - try selecting different poles on the map.\n\nNote: Only work not already linked to a work order is shown");
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "PermissionCount")
            {
                int intLinkedActionToDoCount = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "PermissionCount"));

                if (intLinkedActionToDoCount > 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBF, 0xC0, 0xFF, 0xC0); ;
                    e.Appearance.BackColor2 = Color.FromArgb(0xBE, 0x90, 0xEE, 0x90);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        #endregion

 
 


    }
}


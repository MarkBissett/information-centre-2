using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Tree_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToClientID = 0;
        public int intLinkedToCircuitID = 0;
        public int intLinkedToPoleID = 0;
        public string strLinkedToPoleName = "";
        public string strLinkedToPoleNumber = "";
        public string strLinkedToCircuitNumber = "";
        public string strLinkedToClientName = "";
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        bool iBool_AllowDelete = true;
        bool iBool_AllowAdd = true;

        // Passed in when called by mapping screen //
        public bool boolCalledByMapping = false;
        public float flX = (float)0;
        public float flY = (float)0;
        public string strXYPairs = "";
        public float flLatitude = (float)0;
        public float flLongitude = (float)0;
        public string strLatLongPairs = "";
        public decimal decArea = (decimal)0.00;
        public decimal decLength = (decimal)0.00;
        public int intObjectType = 0;
        public string MapID = "";
        public string strMapIDs = "";  // Only required when block editing from the map - used to tell the tree picker which objects to reload //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        int i_int_FocusedGrid = 1;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        string _strLastUsedReferencePrefix = "";
        ColumnHeaderExtender extender;

        #endregion 

        public frm_UT_Tree_Edit()
        {
            InitializeComponent();
        }

        private void frm_UT_Tree_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500015;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp07062_UT_Species_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07062_UT_Species_List_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07062_UT_Species_List_With_Blank);

            sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView2, "TreeSpeciesID");

            sp07006_UT_Circuit_StatusTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07006_UT_Circuit_StatusTableAdapter.Fill(dataSet_UT.sp07006_UT_Circuit_Status);

            sp07058_UT_Size_Band_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07058_UT_Size_Band_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07058_UT_Size_Band_With_Blank);

            sp07059_UT_Tree_Object_Types_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07059_UT_Tree_Object_Types_With_Blank);
            sp07059_UT_Tree_Object_Types_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07059_UT_Tree_Object_Types_With_Blank);

            sp07186_UT_G55_Categories_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07186_UT_G55_Categories_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07186_UT_G55_Categories_With_Blank);

            sp07406_UT_Growth_Rates_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07406_UT_Growth_Rates_With_BlankTableAdapter.Fill(dataSet_UT.sp07406_UT_Growth_Rates_With_Blank);

            LoadLastSavedUserScreenSettings();  // Get any used sequence number prefix //

            // Populate Main Dataset //
            sp07056_UT_Tree_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_Edit.sp07056_UT_Tree_Item.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientID"] = intLinkedToClientID;
                        drNewRow["ClientName"] = strLinkedToClientName;
                        drNewRow["CircuitNumber"] = strLinkedToCircuitNumber;
                        drNewRow["CircuitID"] = intLinkedToCircuitID;
                        drNewRow["PoleName"] = strLinkedToPoleName;
                        drNewRow["PoleNumber"] = strLinkedToPoleNumber;
                        drNewRow["PoleID"] = intLinkedToPoleID;
                        drNewRow["SizeBandID"] = 0;
                        drNewRow["StatusID"] = 1;  // Actual //
                        drNewRow["ReferenceNumber"] = _strLastUsedReferencePrefix;
                        drNewRow["X"] = flX;
                        drNewRow["Y"] = flY;
                        drNewRow["XYPairs"] = strXYPairs;
                        drNewRow["Latitude"] = flLatitude;
                        drNewRow["Longitude"] = flLongitude;
                        drNewRow["LatLongPairs"] = strLatLongPairs;
                        drNewRow["Area"] = decArea;
                        drNewRow["Length"] = decLength;
                        drNewRow["MapID"] = MapID;
                        drNewRow["TypeID"] = intObjectType;
                        drNewRow["G55CategoryID"] = 0;
                        drNewRow["GrowthRateID"] = 0;
                        drNewRow["DeadTree"] = 0;
                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        this.dataSet_UT_Edit.sp07056_UT_Tree_Item.Rows.Add(drNewRow);
                        if (strFormMode == "add" && !string.IsNullOrEmpty(_strLastUsedReferencePrefix))  // Add Suffix number to Reference Number //
                        {
                            Get_Suffix_Number();  // Get next value in sequence //
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_Edit.sp07056_UT_Tree_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["PoleName"] = "";
                        drNewRow["PoleNumber"] = "";
                        drNewRow["ReferenceNumber"] = "";
                        this.dataSet_UT_Edit.sp07056_UT_Tree_Item.Rows.Add(drNewRow);
                        this.dataSet_UT_Edit.sp07056_UT_Tree_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                        iBool_AllowDelete = false;
                        iBool_AllowAdd = false;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp07056_UT_Tree_ItemTableAdapter.Fill(this.dataSet_UT_Edit.sp07056_UT_Tree_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }

                    gridControl1.BeginUpdate();
                    try
                    {
                        sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter.Fill(dataSet_UT_Edit.sp07060_UT_Tree_Species_Linked_To_Tree, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    gridControl1.EndUpdate();
                    if (strFormMode == "view")
                    {
                        iBool_AllowDelete = false;
                        iBool_AllowAdd = false;
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            // Add button to Grid Column (***** IMPORTANT: This is removed on form close to prevent memory leak! *****) //
            DataTable table = new DataTable();
            table.Columns.Add("ColumnName", typeof(string));
            table.Columns.Add("ButtonWidth", typeof(int));
            table.Columns.Add("ButtonHeight", typeof(int));
            table.Columns.Add("ButtonText", typeof(string));
            table.Rows.Add("colSpeciesID", 110, 17, "View Reference Files");

            extender = new ColumnHeaderExtender(gridView2, table);
            extender.AddCustomButton();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_UT_Edit.sp07056_UT_Tree_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Tree", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberButtonEdit.Focus();
 
                        PoleNameButtonEdit.Properties.ReadOnly = false;
                        PoleNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = true;
                        MapIDTextEdit.Properties.ReadOnly = false;
                        XTextEdit.Properties.ReadOnly = false;
                        YTextEdit.Properties.ReadOnly = false;
                        XYPairsMemoEdit.Properties.ReadOnly = false;
                        LatitudeTextEdit.Properties.ReadOnly = false;
                        LongitudeTextEdit.Properties.ReadOnly = false;
                        LatLongPairsMemoEdit.Properties.ReadOnly = false;
                        AreaTextEdit.Properties.ReadOnly = false;
                        LengthTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        StatusIDGridLookUpEdit.Focus();

                        PoleNameButtonEdit.Properties.ReadOnly = true;
                        PoleNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = false;
                        MapIDTextEdit.Properties.ReadOnly = true;
                        XTextEdit.Properties.ReadOnly = true;
                        YTextEdit.Properties.ReadOnly = true;
                        XYPairsMemoEdit.Properties.ReadOnly = true;
                        LatitudeTextEdit.Properties.ReadOnly = true;
                        LongitudeTextEdit.Properties.ReadOnly = true;
                        LatLongPairsMemoEdit.Properties.ReadOnly = true;
                        AreaTextEdit.Properties.ReadOnly = true;
                        LengthTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        StatusIDGridLookUpEdit.Focus();

                        PoleNameButtonEdit.Properties.ReadOnly = false;
                        PoleNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = true;
                        MapIDTextEdit.Properties.ReadOnly = false;
                        XTextEdit.Properties.ReadOnly = false;
                        YTextEdit.Properties.ReadOnly = false;
                        XYPairsMemoEdit.Properties.ReadOnly = false;
                        LatitudeTextEdit.Properties.ReadOnly = false;
                        LongitudeTextEdit.Properties.ReadOnly = false;
                        LatLongPairsMemoEdit.Properties.ReadOnly = false;
                        AreaTextEdit.Properties.ReadOnly = false;
                        LengthTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        StatusIDGridLookUpEdit.Focus();

                        PoleNameButtonEdit.Properties.ReadOnly = true;
                        PoleNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = false;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = false;
                        MapIDTextEdit.Properties.ReadOnly = true;
                        XTextEdit.Properties.ReadOnly = true;
                        YTextEdit.Properties.ReadOnly = true;
                        XYPairsMemoEdit.Properties.ReadOnly = true;
                        LatitudeTextEdit.Properties.ReadOnly = true;
                        LongitudeTextEdit.Properties.ReadOnly = true;
                        LatLongPairsMemoEdit.Properties.ReadOnly = true;
                        AreaTextEdit.Properties.ReadOnly = true;
                        LengthTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9153", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9153:  // Size Bands Picklist //    
                        {
                            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            }
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            sp07060UTTreeSpeciesLinkedToTreeBindingSource.EndEdit();  // Force any pending save from Grid to underlying table adapter //
            
            DataSet dsChanges = this.dataSet_UT_Edit.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            if (!iBool_AllowAdd || strFormMode == "blockedit")
            {
                gridControl1.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intTreeID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp07056UTTreeItemBindingSource.Current;
            if (currentRow != null)
            {
                intTreeID = (currentRow["TreeID"] == null ? 0 : Convert.ToInt32(currentRow["TreeID"]));
            }

            if (i_int_FocusedGrid == 1)  // Trees //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd && strFormMode != "blockedit")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }

        }


        private void frm_UT_Tree_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_UT_Tree_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;

            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                if (!string.IsNullOrEmpty(_strLastUsedReferencePrefix))
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ReferenceNumberPrefix", _strLastUsedReferencePrefix);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }

            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        extender.RemoveCustomButton();  // Attached in Load event //

                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Reference Number Prefix //
                _strLastUsedReferencePrefix = default_screen_settings.RetrieveSetting("ReferenceNumberPrefix");
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            // If adding, check we have at least one species //
            if (this.strFormMode.ToLower() == "add")
            {
                if (view.DataRowCount <= 0)
                {
                    XtraMessageBox.Show("At least one species must be recorded against the tree before the tree can be saved.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return "Error";
                }
                else
                {
                    int intSpeciesID = 0;
                    foreach (DataRow dr in dataSet_UT_Edit.sp07060_UT_Tree_Species_Linked_To_Tree.Rows)
                    {
                        intSpeciesID = (string.IsNullOrEmpty(dr["SpeciesID"].ToString()) ? 0 : Convert.ToInt32(dr["SpeciesID"]));
                        if (intSpeciesID <= 0)
                        {
                            XtraMessageBox.Show("At least one species record has no species selected against it. Please correct before saving.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return "Error";
                        }
                    }
                }
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp07056UTTreeItemBindingSource.EndEdit();
            try
            {
                this.sp07056_UT_Tree_ItemTableAdapter.Update(dataSet_UT_Edit);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp07056UTTreeItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["TreeID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }

                // Pass new ID to any records in the linked Species grid //
                view = (GridView)gridControl1.MainView;
                int intTreeID = Convert.ToInt32(currentRow["TreeID"]);
                //strRecordIDs = intTreeID.ToString() + ",";
                view.BeginUpdate();
                foreach (DataRow dr in dataSet_UT_Edit.sp07060_UT_Tree_Species_Linked_To_Tree.Rows)
                {
                    dr["TreeID"] = intTreeID;
                }
                this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                view.EndUpdate();

            }

            // Pick up all Map IDs and concatenate together so Tree Picker can be updated if it is running //
            string strUpdateIDs = "";
            if (this.strFormMode.ToLower() == "blockedit")
            {
                strUpdateIDs = strMapIDs;
            }
            else
            {
                foreach (DataRow dr in this.dataSet_UT_Edit.sp07056_UT_Tree_Item.Rows)
                {
                    if (!string.IsNullOrEmpty(dr["MapID"].ToString()))
                    {
                        strUpdateIDs += dr["MapID"].ToString() + ",";
                    }
                }
            }

            // Attempt to save any changes to Linked Species... //
            try
            {
                sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked species record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Pole_Manager")
                    {
                        var fParentForm = (frm_UT_Pole_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs, "", "", "", "", "");
                    }
                    if (frmChild.Name == "frm_UT_Mapping" && !string.IsNullOrEmpty(strUpdateIDs))  // Mapping Form //
                    {
                        var fParentForm = (frm_UT_Mapping)frmChild;
                        fParentForm.RefreshMapObjects(strUpdateIDs, 1);
                    }
                    if (frmChild.Name == "frm_UT_Survey_Pole2")
                    {
                        var fParentForm = (frm_UT_Survey_Pole2)frmChild;
                        fParentForm.UpdateFormRefreshStatus(3, strNewIDs, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_UT_Edit.sp07056_UT_Tree_Item.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07056_UT_Tree_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            int intSpeciesNew = 0;
            int intSpeciesModified = 0;
            int intSpeciesDeleted = 0;
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07060_UT_Tree_Species_Linked_To_Tree.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07060_UT_Tree_Species_Linked_To_Tree.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intSpeciesNew++;
                        break;
                    case DataRowState.Modified:
                        intSpeciesModified++;
                        break;
                    case DataRowState.Deleted:
                        intSpeciesDeleted++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intSpeciesNew > 0 || intSpeciesModified > 0 || intSpeciesDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                if (intSpeciesNew > 0) strMessage += Convert.ToString(intSpeciesNew) + " New linked Species record(s)\n";
                if (intSpeciesModified > 0) strMessage += Convert.ToString(intSpeciesModified) + " Updated linked Species record(s)\n";
                if (intSpeciesDeleted > 0) strMessage += Convert.ToString(intSpeciesDeleted) + " Deleted linked Species record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveFirst();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MovePrevious();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveNext();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveLast();
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    FilterGrids();
                    GridView view = (GridView)gridControl1.MainView;
                    view.ExpandAllGroups();
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void PoleNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp07056UTTreeItemBindingSource.Current;
                if (currentRow == null) return;
                int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));
                int intCircuitID = (string.IsNullOrEmpty(currentRow["CircuitID"].ToString()) ? 0 : Convert.ToInt32(currentRow["CircuitID"]));
                int intPoleID = (string.IsNullOrEmpty(currentRow["PoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["PoleID"]));
                
                frm_UT_Select_Pole fChildForm = new frm_UT_Select_Pole();
                fChildForm.intPassedInClientID = intClientID;
                fChildForm.intPassedInCircuitID = intCircuitID;
                fChildForm.intPassedInPoleID = intPoleID;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intFilterUtilityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["CircuitID"] = fChildForm.intSelectedCircuitID;
                    currentRow["ClientID"] = fChildForm.intSelectedClientID;
                    currentRow["PoleID"] = fChildForm.intSelectedID;
                    currentRow["CircuitNumber"] = fChildForm.strSelectedCircuitNumber;
                    currentRow["ClientName"] = fChildForm.strSelectedClientName;
                    currentRow["PoleName"] = fChildForm.strSelectedValue;
                    sp07056UTTreeItemBindingSource.EndEdit();
                }
            }
        }
        private void PoleNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(PoleNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(PoleNameButtonEdit, "");
            }
        }

        private void ReferenceNumberButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = "UT_Tree";
                fChildForm.PassedInFieldName = "ReferenceNumber";
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    ReferenceNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    _strLastUsedReferencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                Get_Suffix_Number();  // Get next value in sequence //  
            }
        }
        private void ReferenceNumberButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ReferenceNumberButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ReferenceNumberButtonEdit, "");
            }
        }
        private void Get_Suffix_Number()
        {
            string strSequence = ReferenceNumberButtonEdit.EditValue.ToString() ?? "";

            SequenceNumberGetNext sequence = new SequenceNumberGetNext();
            string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, "ReferenceNumber", "UT_Tree", strSequence);  // Calculate Next in Sequence //

            if (strNextNumber == "")
            {
                return;
            }
            if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

            int intRequiredLength = strNextNumber.Length;
            int intNextNumber = Convert.ToInt32(strNextNumber);
            int intIncrement = 0;
            string strTempNumber = "";


            // Check if value is already present within dataset //
            DataRowView currentRow = (DataRowView)sp07056UTTreeItemBindingSource.Current;
            if (currentRow == null) return;
            string strCurrentActionID = "";
            if (!string.IsNullOrEmpty(currentRow["TreeID"].ToString())) strCurrentActionID = currentRow["TreeID"].ToString();

            bool boolUniqueValueFound = false;
            bool boolDuplicateFound = false;
            do
            {
                boolDuplicateFound = false;
                strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                }
                foreach (DataRow dr in this.dataSet_UT_Edit.sp07056_UT_Tree_Item.Rows)
                {
                    if (dr["ReferenceNumber"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                    {
                        intIncrement++;
                        boolDuplicateFound = true;
                        break;
                    }
                }
                if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
            } while (!boolUniqueValueFound);
            ReferenceNumberButtonEdit.EditValue = strSequence + strTempNumber;
        }

        private void SizeBandIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 253, "Utilities Size Bands");
                }
                else if ("reload".Equals(e.Button.Tag))
                {

                    sp07058_UT_Size_Band_With_BlankTableAdapter.Fill(dataSet_UT_Edit .sp07058_UT_Size_Band_With_Blank);
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();
                }
            }
        }
        private void SizeBandIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(SizeBandIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SizeBandIDGridLookUpEdit, "");
            }
        }

        private void G55CategoryIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(G55CategoryIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(G55CategoryIDGridLookUpEdit, "");
            }
        }

        private void GrowthRateIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(GrowthRateIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(GrowthRateIDGridLookUpEdit, "");
            }
        }



        #endregion


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            string message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Species NOT Shown When Block Adding and Block Editing" : "No Linked Species Available - Click the Add button to Create Linked Species");
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
            }
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void Add_Record()
        {
            try
            {
                DataRowView currentRow = (DataRowView)sp07056UTTreeItemBindingSource.Current;
                if (currentRow != null)
                {
                    int intTreeID = 0;
                    if (!string.IsNullOrEmpty(currentRow["TreeID"].ToString())) intTreeID = Convert.ToInt32(currentRow["TreeID"]);

                    DataRow drNewRow;
                    drNewRow = this.dataSet_UT_Edit.sp07060_UT_Tree_Species_Linked_To_Tree.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["TreeID"] = intTreeID;
                    drNewRow["SpeciesID"] = 0;
                    drNewRow["NumberOfTrees"] = 1;
                    drNewRow["Percentage"] = 100.00;
                    this.dataSet_UT_Edit.sp07060_UT_Tree_Species_Linked_To_Tree.Rows.Add(drNewRow);
                    SetChangesPendingLabel();
                }
            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new species - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Species", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Species to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Species" : Convert.ToString(intRowHandles.Length) + " Species") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Species" : "these Species") + " will no longer be available for selection!";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                view.BeginUpdate();
                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    view.DeleteRow(intRowHandles[i]);
                }
                view.EndUpdate();

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Species(s) deleted.\n\nImportant Note: Deleted Species are only physically removed from the tree on saving changes.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetChangesPendingLabel();
            }

        }

        private void FilterGrids()
        {
            string strTreeID = "";
            DataRowView currentRow = (DataRowView)sp07056UTTreeItemBindingSource.Current;
            if (currentRow != null)
            {
                strTreeID = (currentRow["TreeID"] == null ? "0" : currentRow["TreeID"].ToString());
            }

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[TreeID] = " + Convert.ToString(strTreeID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        public override void CustomColumnButtonClick(object sender, string columnName)
        {
            GridView view = (GridView)sender;
            if (view.GridControl.MainView.Name != "gridView2") return;  // Only fire for appropriate view //
            if (columnName != "colSpeciesID") return;

            int[] intRowHandles = view.GetSelectedRows();
            int intCurrentRecordID = 0;
            if (intRowHandles.Length > 0)
            {
                intCurrentRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SpeciesID"));
            }

            frm_UT_Reference_Files_View fChildForm = new frm_UT_Reference_Files_View();
            fChildForm.intPassedInParentID = intCurrentRecordID;
            fChildForm.intRecordType = 1;  // Species //
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
            }
        }

 



    }
}


namespace WoodPlan5
{
    partial class frm_UT_Permission_Doc_Edit_UKPN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Permission_Doc_Edit_UKPN));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiAddWorkToPermission = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreatePermissionDocument = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEmailToCustomer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPostedToCustomer = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ArisingsOtherTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp07389UTPDItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.ArisingsStackOnSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ArisingsChipOnSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ArisingsChipRemoveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RoadsideAccessOnlyCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AccessDetailsOnMapCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AccessAgreedWithLandOwnerCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.HotGloveAccessAvailableCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4x4 = new DevExpress.XtraEditors.CheckEdit();
            this.TrafficManagementCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TipperCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ChipperCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.MewpCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.EquipmentMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07402UTPDLinkedAccessMapsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPermissionDocumentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditMapPath = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colMapType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TreePlantingCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ApplyHerbicideCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SurveyFormNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteHazardsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.OwnerTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07170UTLandOwnerTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SignatureDateTextEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.SentByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PermissionDocumentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Mapping = new WoodPlan5.DataSet_UT_Mapping();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InfoLabel1 = new DevExpress.XtraEditors.LabelControl();
            this.EmergencyAccessMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.OwnerAddressMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.SiteAddressMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.SignatureFileButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.OwnerEmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OwnerTelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OwnerPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OwnerSalutationTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.ContractorTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ReferenceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.NearestAandETextEdit = new DevExpress.XtraEditors.TextEdit();
            this.GridReferenceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NearestTelephonePointTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SentByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SentByPostCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PostageRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PermissionEmailedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SignatureDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EmailedToClientDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EmailedToCustomerDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.PDFFileHyperLinkEdit = new DevExpress.XtraEditors.HyperLinkEdit();
            this.SignatureFileHyperLinkEdit = new DevExpress.XtraEditors.HyperLinkEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.RaisedByIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00226StaffListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateRaisedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07392UTPDLinkedWorkBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionPermissionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditStatus = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07197UTPermissionStatusesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLandOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionDocumentDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colESQCRCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55Category = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAchievableClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMeters = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandOwnerRestrictedCut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditWork = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colRefusalReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefusalReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefusalRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.OwnerNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForSentByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPermissionDocumentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractor = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRaisedByID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateRaised = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmergencyAccess = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNearestTelephonePoint = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGridReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNearestAandE = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.autoGeneratedGroup0 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInfoLabel1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForOwnerTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyFormNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerSalutation = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForApplyHerbicide = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTreePlanting = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWarningLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup20 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSignatureFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSignatureDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDFFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPostageRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSentByPost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSentByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPermissionEmailed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmailedToCustomerDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEmailedToClientDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter();
            this.sp00226_Staff_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter();
            this.sp07197_UT_Permission_Statuses_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07197_UT_Permission_Statuses_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07389_UT_PD_ItemTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07389_UT_PD_ItemTableAdapter();
            this.sp07392_UT_PD_Linked_WorkTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07392_UT_PD_Linked_WorkTableAdapter();
            this.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter = new WoodPlan5.DataSet_UT_MappingTableAdapters.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp07170_UT_Land_Owner_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07170_UT_Land_Owner_Types_With_BlankTableAdapter();
            this.sp07402_UT_PD_Linked_Access_MapsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07402_UT_PD_Linked_Access_MapsTableAdapter();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsOtherTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07389UTPDItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsStackOnSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsChipOnSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsChipRemoveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoadsideAccessOnlyCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessDetailsOnMapCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessAgreedWithLandOwnerCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotGloveAccessAvailableCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4x4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficManagementCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipperCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChipperCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MewpCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07402UTPDLinkedAccessMapsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditMapPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreePlantingCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyHerbicideCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyFormNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteHazardsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07170UTLandOwnerTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateTextEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionDocumentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmergencyAccessMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerAddressMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureFileButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerEmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerSalutationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestAandETextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridReferenceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestTelephonePointTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByPostCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostageRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionEmailedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToClientDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToClientDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToCustomerDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToCustomerDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDFFileHyperLinkEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureFileHyperLinkEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaisedByIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07392UTPDLinkedWorkBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07197UTPermissionStatusesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionDocumentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRaisedByID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmergencyAccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestTelephonePoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGridReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestAandE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoGeneratedGroup0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInfoLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyFormNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerSalutation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApplyHerbicide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreePlanting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDFFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostageRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByPost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionEmailed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailedToCustomerDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailedToClientDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07060UTTreeSpeciesLinkedToTreeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(976, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 653);
            this.barDockControlBottom.Size = new System.Drawing.Size(976, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 627);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(976, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 627);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.CheckAsYouTypeOptions.CheckControlsInParentContainer = true;
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colID2
            // 
            this.colID2.Caption = "ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiAddWorkToPermission,
            this.bbiCreatePermissionDocument,
            this.bbiCreateMap,
            this.bbiEmailToCustomer,
            this.bbiPostedToCustomer});
            this.barManager2.MaxItemId = 20;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Tasks Bar";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(590, 162);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddWorkToPermission),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreatePermissionDocument, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEmailToCustomer, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPostedToCustomer, true)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.Text = "Tasks Bar";
            // 
            // bbiAddWorkToPermission
            // 
            this.bbiAddWorkToPermission.Caption = "Add More Jobs";
            this.bbiAddWorkToPermission.Id = 15;
            this.bbiAddWorkToPermission.ImageOptions.ImageIndex = 0;
            this.bbiAddWorkToPermission.Name = "bbiAddWorkToPermission";
            this.bbiAddWorkToPermission.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiAddWorkToPermission.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddWorkToPermission_ItemClick);
            // 
            // bbiCreatePermissionDocument
            // 
            this.bbiCreatePermissionDocument.Caption = "View Permission Document";
            this.bbiCreatePermissionDocument.Id = 16;
            this.bbiCreatePermissionDocument.ImageOptions.ImageIndex = 4;
            this.bbiCreatePermissionDocument.Name = "bbiCreatePermissionDocument";
            this.bbiCreatePermissionDocument.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiCreatePermissionDocument.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreatePermissionDocument_ItemClick);
            // 
            // bbiCreateMap
            // 
            this.bbiCreateMap.Caption = "Create Work Map";
            this.bbiCreateMap.Id = 17;
            this.bbiCreateMap.ImageOptions.ImageIndex = 8;
            this.bbiCreateMap.Name = "bbiCreateMap";
            this.bbiCreateMap.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiCreateMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreateMap_ItemClick);
            // 
            // bbiEmailToCustomer
            // 
            this.bbiEmailToCustomer.Caption = "Email To Customer";
            this.bbiEmailToCustomer.Id = 18;
            this.bbiEmailToCustomer.ImageOptions.ImageIndex = 9;
            this.bbiEmailToCustomer.Name = "bbiEmailToCustomer";
            this.bbiEmailToCustomer.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiEmailToCustomer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEmailToCustomer_ItemClick);
            // 
            // bbiPostedToCustomer
            // 
            this.bbiPostedToCustomer.Caption = "Posted To Customer";
            this.bbiPostedToCustomer.Id = 19;
            this.bbiPostedToCustomer.ImageOptions.ImageIndex = 10;
            this.bbiPostedToCustomer.Name = "bbiPostedToCustomer";
            this.bbiPostedToCustomer.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiPostedToCustomer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPostedToCustomer_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(976, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 653);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(976, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 627);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(976, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 627);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Refresh_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 7);
            this.imageCollection1.Images.SetKeyName(7, "refresh_16x16.png");
            this.imageCollection1.Images.SetKeyName(8, "show_map_16.png");
            this.imageCollection1.InsertGalleryImage("mail_16x16.png", "images/mail/mail_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/mail_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "mail_16x16.png");
            this.imageCollection1.InsertGalleryImage("send_16x16.png", "images/mail/send_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/send_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "send_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ArisingsOtherTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ArisingsStackOnSiteCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ArisingsChipOnSiteCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ArisingsChipRemoveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.RoadsideAccessOnlyCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.AccessDetailsOnMapCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.AccessAgreedWithLandOwnerCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.HotGloveAccessAvailableCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.checkEdit4x4);
            this.dataLayoutControl1.Controls.Add(this.TrafficManagementCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TipperCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ChipperCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.MewpCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.EquipmentMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.labelControl2);
            this.dataLayoutControl1.Controls.Add(this.gridControl5);
            this.dataLayoutControl1.Controls.Add(this.TreePlantingCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ApplyHerbicideCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyFormNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteHazardsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.labelControl31);
            this.dataLayoutControl1.Controls.Add(this.OwnerTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SignatureDateTextEdit2);
            this.dataLayoutControl1.Controls.Add(this.labelControl30);
            this.dataLayoutControl1.Controls.Add(this.SentByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PermissionDocumentIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl4);
            this.dataLayoutControl1.Controls.Add(this.gridControl6);
            this.dataLayoutControl1.Controls.Add(this.InfoLabel1);
            this.dataLayoutControl1.Controls.Add(this.EmergencyAccessMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerAddressMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.SignatureFileButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerEmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerTelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerSalutationTextEdit);
            this.dataLayoutControl1.Controls.Add(this.textEdit4);
            this.dataLayoutControl1.Controls.Add(this.ContractorTextEdit);
            this.dataLayoutControl1.Controls.Add(this.labelControl1);
            this.dataLayoutControl1.Controls.Add(this.ReferenceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.pictureEdit1);
            this.dataLayoutControl1.Controls.Add(this.NearestAandETextEdit);
            this.dataLayoutControl1.Controls.Add(this.GridReferenceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NearestTelephonePointTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SentByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SentByPostCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.PostageRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.PermissionEmailedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SignatureDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailedToClientDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailedToCustomerDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.PDFFileHyperLinkEdit);
            this.dataLayoutControl1.Controls.Add(this.SignatureFileHyperLinkEdit);
            this.dataLayoutControl1.Controls.Add(this.btnSave);
            this.dataLayoutControl1.Controls.Add(this.RaisedByIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DateRaisedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerNameButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp07389UTPDItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSentByStaffID,
            this.ItemForPermissionDocumentID,
            this.ItemForContractor,
            this.ItemForRaisedByID,
            this.ItemForSiteAddress,
            this.ItemForDateRaised,
            this.ItemForEmergencyAccess,
            this.ItemForNearestTelephonePoint,
            this.ItemForGridReference,
            this.ItemForNearestAandE,
            this.layoutControlItem12});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(48, 171, 287, 390);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(976, 627);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ArisingsOtherTextEdit
            // 
            this.ArisingsOtherTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ArisingsOther", true));
            this.ArisingsOtherTextEdit.Location = new System.Drawing.Point(789, 1106);
            this.ArisingsOtherTextEdit.MenuManager = this.barManager1;
            this.ArisingsOtherTextEdit.Name = "ArisingsOtherTextEdit";
            this.ArisingsOtherTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ArisingsOtherTextEdit, true);
            this.ArisingsOtherTextEdit.Size = new System.Drawing.Size(147, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ArisingsOtherTextEdit, optionsSpelling1);
            this.ArisingsOtherTextEdit.StyleController = this.dataLayoutControl1;
            this.ArisingsOtherTextEdit.TabIndex = 13;
            // 
            // sp07389UTPDItemBindingSource
            // 
            this.sp07389UTPDItemBindingSource.DataMember = "sp07389_UT_PD_Item";
            this.sp07389UTPDItemBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ArisingsStackOnSiteCheckEdit
            // 
            this.ArisingsStackOnSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ArisingsStackOnSite", true));
            this.ArisingsStackOnSiteCheckEdit.Location = new System.Drawing.Point(679, 1106);
            this.ArisingsStackOnSiteCheckEdit.MenuManager = this.barManager1;
            this.ArisingsStackOnSiteCheckEdit.Name = "ArisingsStackOnSiteCheckEdit";
            this.ArisingsStackOnSiteCheckEdit.Properties.Caption = "(Data Entry)";
            this.ArisingsStackOnSiteCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.ArisingsStackOnSiteCheckEdit.Properties.ValueChecked = 1;
            this.ArisingsStackOnSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.ArisingsStackOnSiteCheckEdit.Size = new System.Drawing.Size(106, 19);
            this.ArisingsStackOnSiteCheckEdit.StyleController = this.dataLayoutControl1;
            this.ArisingsStackOnSiteCheckEdit.TabIndex = 13;
            // 
            // ArisingsChipOnSiteCheckEdit
            // 
            this.ArisingsChipOnSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ArisingsChipOnSite", true));
            this.ArisingsChipOnSiteCheckEdit.Location = new System.Drawing.Point(536, 1106);
            this.ArisingsChipOnSiteCheckEdit.MenuManager = this.barManager1;
            this.ArisingsChipOnSiteCheckEdit.Name = "ArisingsChipOnSiteCheckEdit";
            this.ArisingsChipOnSiteCheckEdit.Properties.Caption = "(Data Entry)";
            this.ArisingsChipOnSiteCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.ArisingsChipOnSiteCheckEdit.Properties.ValueChecked = 1;
            this.ArisingsChipOnSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.ArisingsChipOnSiteCheckEdit.Size = new System.Drawing.Size(139, 19);
            this.ArisingsChipOnSiteCheckEdit.StyleController = this.dataLayoutControl1;
            this.ArisingsChipOnSiteCheckEdit.TabIndex = 13;
            // 
            // ArisingsChipRemoveCheckEdit
            // 
            this.ArisingsChipRemoveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ArisingsChipRemove", true));
            this.ArisingsChipRemoveCheckEdit.Location = new System.Drawing.Point(414, 1106);
            this.ArisingsChipRemoveCheckEdit.MenuManager = this.barManager1;
            this.ArisingsChipRemoveCheckEdit.Name = "ArisingsChipRemoveCheckEdit";
            this.ArisingsChipRemoveCheckEdit.Properties.Caption = "(Data Entry)";
            this.ArisingsChipRemoveCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.ArisingsChipRemoveCheckEdit.Properties.ValueChecked = 1;
            this.ArisingsChipRemoveCheckEdit.Properties.ValueUnchecked = 0;
            this.ArisingsChipRemoveCheckEdit.Size = new System.Drawing.Size(118, 19);
            this.ArisingsChipRemoveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ArisingsChipRemoveCheckEdit.TabIndex = 13;
            // 
            // RoadsideAccessOnlyCheckEdit
            // 
            this.RoadsideAccessOnlyCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "RoadsideAccessOnly", true));
            this.RoadsideAccessOnlyCheckEdit.Location = new System.Drawing.Point(296, 1106);
            this.RoadsideAccessOnlyCheckEdit.MenuManager = this.barManager1;
            this.RoadsideAccessOnlyCheckEdit.Name = "RoadsideAccessOnlyCheckEdit";
            this.RoadsideAccessOnlyCheckEdit.Properties.Caption = "(Data Entry)";
            this.RoadsideAccessOnlyCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.RoadsideAccessOnlyCheckEdit.Properties.ValueChecked = 1;
            this.RoadsideAccessOnlyCheckEdit.Properties.ValueUnchecked = 0;
            this.RoadsideAccessOnlyCheckEdit.Size = new System.Drawing.Size(114, 19);
            this.RoadsideAccessOnlyCheckEdit.StyleController = this.dataLayoutControl1;
            this.RoadsideAccessOnlyCheckEdit.TabIndex = 13;
            // 
            // AccessDetailsOnMapCheckEdit
            // 
            this.AccessDetailsOnMapCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "AccessDetailsOnMap", true));
            this.AccessDetailsOnMapCheckEdit.Location = new System.Drawing.Point(178, 1106);
            this.AccessDetailsOnMapCheckEdit.MenuManager = this.barManager1;
            this.AccessDetailsOnMapCheckEdit.Name = "AccessDetailsOnMapCheckEdit";
            this.AccessDetailsOnMapCheckEdit.Properties.Caption = "(Data Entry)";
            this.AccessDetailsOnMapCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.AccessDetailsOnMapCheckEdit.Properties.ValueChecked = 1;
            this.AccessDetailsOnMapCheckEdit.Properties.ValueUnchecked = 0;
            this.AccessDetailsOnMapCheckEdit.Size = new System.Drawing.Size(114, 19);
            this.AccessDetailsOnMapCheckEdit.StyleController = this.dataLayoutControl1;
            this.AccessDetailsOnMapCheckEdit.TabIndex = 13;
            // 
            // AccessAgreedWithLandOwnerCheckEdit
            // 
            this.AccessAgreedWithLandOwnerCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "AccessAgreedWithLandOwner", true));
            this.AccessAgreedWithLandOwnerCheckEdit.Location = new System.Drawing.Point(23, 1104);
            this.AccessAgreedWithLandOwnerCheckEdit.MenuManager = this.barManager1;
            this.AccessAgreedWithLandOwnerCheckEdit.Name = "AccessAgreedWithLandOwnerCheckEdit";
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.Appearance.Options.UseBackColor = true;
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.Caption = "(Data Entry)";
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.ValueChecked = 1;
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.ValueUnchecked = 0;
            this.AccessAgreedWithLandOwnerCheckEdit.Size = new System.Drawing.Size(151, 19);
            this.AccessAgreedWithLandOwnerCheckEdit.StyleController = this.dataLayoutControl1;
            this.AccessAgreedWithLandOwnerCheckEdit.TabIndex = 13;
            // 
            // HotGloveAccessAvailableCheckEdit
            // 
            this.HotGloveAccessAvailableCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "HotGloveAccessAvailable", true));
            this.HotGloveAccessAvailableCheckEdit.Location = new System.Drawing.Point(847, 1054);
            this.HotGloveAccessAvailableCheckEdit.MenuManager = this.barManager1;
            this.HotGloveAccessAvailableCheckEdit.Name = "HotGloveAccessAvailableCheckEdit";
            this.HotGloveAccessAvailableCheckEdit.Properties.Caption = "(Data Entry)";
            this.HotGloveAccessAvailableCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.HotGloveAccessAvailableCheckEdit.Properties.ValueChecked = 1;
            this.HotGloveAccessAvailableCheckEdit.Properties.ValueUnchecked = 0;
            this.HotGloveAccessAvailableCheckEdit.Size = new System.Drawing.Size(89, 19);
            this.HotGloveAccessAvailableCheckEdit.StyleController = this.dataLayoutControl1;
            this.HotGloveAccessAvailableCheckEdit.TabIndex = 13;
            // 
            // checkEdit4x4
            // 
            this.checkEdit4x4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "4x4", true));
            this.checkEdit4x4.Location = new System.Drawing.Point(763, 1054);
            this.checkEdit4x4.MenuManager = this.barManager1;
            this.checkEdit4x4.Name = "checkEdit4x4";
            this.checkEdit4x4.Properties.Caption = "(Calculated)";
            this.checkEdit4x4.Properties.ReadOnly = true;
            this.checkEdit4x4.Properties.ValueChecked = 1;
            this.checkEdit4x4.Properties.ValueUnchecked = 0;
            this.checkEdit4x4.Size = new System.Drawing.Size(80, 19);
            this.checkEdit4x4.StyleController = this.dataLayoutControl1;
            this.checkEdit4x4.TabIndex = 13;
            // 
            // TrafficManagementCheckEdit
            // 
            this.TrafficManagementCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "TrafficManagement", true));
            this.TrafficManagementCheckEdit.Location = new System.Drawing.Point(657, 1054);
            this.TrafficManagementCheckEdit.MenuManager = this.barManager1;
            this.TrafficManagementCheckEdit.Name = "TrafficManagementCheckEdit";
            this.TrafficManagementCheckEdit.Properties.Caption = "(Calculated)";
            this.TrafficManagementCheckEdit.Properties.ReadOnly = true;
            this.TrafficManagementCheckEdit.Properties.ValueChecked = 1;
            this.TrafficManagementCheckEdit.Properties.ValueUnchecked = 0;
            this.TrafficManagementCheckEdit.Size = new System.Drawing.Size(102, 19);
            this.TrafficManagementCheckEdit.StyleController = this.dataLayoutControl1;
            this.TrafficManagementCheckEdit.TabIndex = 13;
            // 
            // TipperCheckEdit
            // 
            this.TipperCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Tipper", true));
            this.TipperCheckEdit.Location = new System.Drawing.Point(574, 1054);
            this.TipperCheckEdit.MenuManager = this.barManager1;
            this.TipperCheckEdit.Name = "TipperCheckEdit";
            this.TipperCheckEdit.Properties.Caption = "(Calculated)";
            this.TipperCheckEdit.Properties.ReadOnly = true;
            this.TipperCheckEdit.Properties.ValueChecked = 1;
            this.TipperCheckEdit.Properties.ValueUnchecked = 0;
            this.TipperCheckEdit.Size = new System.Drawing.Size(79, 19);
            this.TipperCheckEdit.StyleController = this.dataLayoutControl1;
            this.TipperCheckEdit.TabIndex = 13;
            // 
            // ChipperCheckEdit
            // 
            this.ChipperCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Chipper", true));
            this.ChipperCheckEdit.Location = new System.Drawing.Point(486, 1054);
            this.ChipperCheckEdit.MenuManager = this.barManager1;
            this.ChipperCheckEdit.Name = "ChipperCheckEdit";
            this.ChipperCheckEdit.Properties.Caption = "(Calculated)";
            this.ChipperCheckEdit.Properties.ReadOnly = true;
            this.ChipperCheckEdit.Properties.ValueChecked = 1;
            this.ChipperCheckEdit.Properties.ValueUnchecked = 0;
            this.ChipperCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.ChipperCheckEdit.StyleController = this.dataLayoutControl1;
            this.ChipperCheckEdit.TabIndex = 13;
            // 
            // MewpCheckEdit
            // 
            this.MewpCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Mewp", true));
            this.MewpCheckEdit.Location = new System.Drawing.Point(398, 1054);
            this.MewpCheckEdit.MenuManager = this.barManager1;
            this.MewpCheckEdit.Name = "MewpCheckEdit";
            this.MewpCheckEdit.Properties.Caption = "(Calculated)";
            this.MewpCheckEdit.Properties.ReadOnly = true;
            this.MewpCheckEdit.Properties.ValueChecked = 1;
            this.MewpCheckEdit.Properties.ValueUnchecked = 0;
            this.MewpCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.MewpCheckEdit.StyleController = this.dataLayoutControl1;
            this.MewpCheckEdit.TabIndex = 13;
            // 
            // EquipmentMemoExEdit
            // 
            this.EquipmentMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Equipment", true));
            this.EquipmentMemoExEdit.Location = new System.Drawing.Point(23, 1054);
            this.EquipmentMemoExEdit.MenuManager = this.barManager1;
            this.EquipmentMemoExEdit.Name = "EquipmentMemoExEdit";
            this.EquipmentMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EquipmentMemoExEdit.Properties.MaxLength = 32000;
            this.EquipmentMemoExEdit.Properties.ReadOnly = true;
            this.EquipmentMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.EquipmentMemoExEdit.Properties.ShowIcon = false;
            this.EquipmentMemoExEdit.Size = new System.Drawing.Size(371, 20);
            this.EquipmentMemoExEdit.StyleController = this.dataLayoutControl1;
            this.EquipmentMemoExEdit.TabIndex = 13;
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.Image")));
            this.labelControl2.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseImage = true;
            this.labelControl2.Appearance.Options.UseImageAlign = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(114, 35);
            this.labelControl2.MinimumSize = new System.Drawing.Size(0, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(578, 22);
            this.labelControl2.StyleController = this.dataLayoutControl1;
            this.labelControl2.TabIndex = 161;
            this.labelControl2.Text = "       There is no Saved Permission Document on this form. Click <b>View Permissi" +
    "on Document</b> button to View and Save";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07402UTPDLinkedAccessMapsBindingSource;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "reload")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(19, 177);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditMapPath});
            this.gridControl5.Size = new System.Drawing.Size(921, 135);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07402UTPDLinkedAccessMapsBindingSource
            // 
            this.sp07402UTPDLinkedAccessMapsBindingSource.DataMember = "sp07402_UT_PD_Linked_Access_Maps";
            this.sp07402UTPDLinkedAccessMapsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPermissionDocumentID1,
            this.colMapPath,
            this.colMapType,
            this.colOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colPermissionDocumentID1
            // 
            this.colPermissionDocumentID1.Caption = "Permisson Document ID";
            this.colPermissionDocumentID1.FieldName = "PermissionDocumentID";
            this.colPermissionDocumentID1.Name = "colPermissionDocumentID1";
            this.colPermissionDocumentID1.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentID1.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentID1.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentID1.Width = 134;
            // 
            // colMapPath
            // 
            this.colMapPath.Caption = "Map Path";
            this.colMapPath.ColumnEdit = this.repositoryItemHyperLinkEditMapPath;
            this.colMapPath.FieldName = "MapPath";
            this.colMapPath.Name = "colMapPath";
            this.colMapPath.OptionsColumn.ReadOnly = true;
            this.colMapPath.Visible = true;
            this.colMapPath.VisibleIndex = 1;
            this.colMapPath.Width = 334;
            // 
            // repositoryItemHyperLinkEditMapPath
            // 
            this.repositoryItemHyperLinkEditMapPath.AutoHeight = false;
            this.repositoryItemHyperLinkEditMapPath.Name = "repositoryItemHyperLinkEditMapPath";
            this.repositoryItemHyperLinkEditMapPath.SingleClick = true;
            this.repositoryItemHyperLinkEditMapPath.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditMapPath_OpenLink);
            // 
            // colMapType
            // 
            this.colMapType.Caption = "Map Type";
            this.colMapType.FieldName = "MapType";
            this.colMapType.Name = "colMapType";
            this.colMapType.OptionsColumn.AllowEdit = false;
            this.colMapType.OptionsColumn.AllowFocus = false;
            this.colMapType.OptionsColumn.ReadOnly = true;
            this.colMapType.Visible = true;
            this.colMapType.VisibleIndex = 0;
            this.colMapType.Width = 117;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // TreePlantingCheckEdit
            // 
            this.TreePlantingCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "TreePlanting", true));
            this.TreePlantingCheckEdit.Location = new System.Drawing.Point(147, 711);
            this.TreePlantingCheckEdit.MenuManager = this.barManager1;
            this.TreePlantingCheckEdit.Name = "TreePlantingCheckEdit";
            this.TreePlantingCheckEdit.Properties.Caption = "Tree Planting:";
            this.TreePlantingCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TreePlantingCheckEdit.Properties.ValueChecked = 1;
            this.TreePlantingCheckEdit.Properties.ValueUnchecked = 0;
            this.TreePlantingCheckEdit.Size = new System.Drawing.Size(90, 19);
            this.TreePlantingCheckEdit.StyleController = this.dataLayoutControl1;
            this.TreePlantingCheckEdit.TabIndex = 5;
            // 
            // ApplyHerbicideCheckEdit
            // 
            this.ApplyHerbicideCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ApplyHerbicide", true));
            this.ApplyHerbicideCheckEdit.Location = new System.Drawing.Point(11, 711);
            this.ApplyHerbicideCheckEdit.MenuManager = this.barManager1;
            this.ApplyHerbicideCheckEdit.Name = "ApplyHerbicideCheckEdit";
            this.ApplyHerbicideCheckEdit.Properties.Caption = "Apply Herbicide:";
            this.ApplyHerbicideCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ApplyHerbicideCheckEdit.Properties.ValueChecked = 1;
            this.ApplyHerbicideCheckEdit.Properties.ValueUnchecked = 0;
            this.ApplyHerbicideCheckEdit.Size = new System.Drawing.Size(100, 19);
            this.ApplyHerbicideCheckEdit.StyleController = this.dataLayoutControl1;
            this.ApplyHerbicideCheckEdit.TabIndex = 4;
            // 
            // SurveyFormNumberTextEdit
            // 
            this.SurveyFormNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SurveyFormNumber", true));
            this.SurveyFormNumberTextEdit.Location = new System.Drawing.Point(582, 774);
            this.SurveyFormNumberTextEdit.MenuManager = this.barManager1;
            this.SurveyFormNumberTextEdit.Name = "SurveyFormNumberTextEdit";
            this.SurveyFormNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyFormNumberTextEdit, true);
            this.SurveyFormNumberTextEdit.Size = new System.Drawing.Size(358, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyFormNumberTextEdit, optionsSpelling2);
            this.SurveyFormNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyFormNumberTextEdit.TabIndex = 7;
            // 
            // SiteHazardsMemoEdit
            // 
            this.SiteHazardsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SiteHazards", true));
            this.SiteHazardsMemoEdit.Location = new System.Drawing.Point(19, 364);
            this.SiteHazardsMemoEdit.MenuManager = this.barManager1;
            this.SiteHazardsMemoEdit.Name = "SiteHazardsMemoEdit";
            this.SiteHazardsMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteHazardsMemoEdit, true);
            this.SiteHazardsMemoEdit.Size = new System.Drawing.Size(921, 49);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteHazardsMemoEdit, optionsSpelling3);
            this.SiteHazardsMemoEdit.StyleController = this.dataLayoutControl1;
            this.SiteHazardsMemoEdit.TabIndex = 2;
            // 
            // labelControl31
            // 
            this.labelControl31.AllowHtmlString = true;
            this.labelControl31.Location = new System.Drawing.Point(19, 967);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(396, 13);
            this.labelControl31.StyleController = this.dataLayoutControl1;
            this.labelControl31.TabIndex = 162;
            this.labelControl31.Text = "<b>Herbicide Approval</b>  I hearby authorise the use of herbacide as detailed ab" +
    "ove.";
            // 
            // OwnerTypeIDGridLookUpEdit
            // 
            this.OwnerTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerTypeID", true));
            this.OwnerTypeIDGridLookUpEdit.Location = new System.Drawing.Point(168, 774);
            this.OwnerTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.OwnerTypeIDGridLookUpEdit.Name = "OwnerTypeIDGridLookUpEdit";
            this.OwnerTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OwnerTypeIDGridLookUpEdit.Properties.DataSource = this.sp07170UTLandOwnerTypesWithBlankBindingSource;
            this.OwnerTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.OwnerTypeIDGridLookUpEdit.Properties.NullText = "";
            this.OwnerTypeIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.OwnerTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.OwnerTypeIDGridLookUpEdit.Size = new System.Drawing.Size(261, 20);
            this.OwnerTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.OwnerTypeIDGridLookUpEdit.TabIndex = 6;
            // 
            // sp07170UTLandOwnerTypesWithBlankBindingSource
            // 
            this.sp07170UTLandOwnerTypesWithBlankBindingSource.DataMember = "sp07170_UT_Land_Owner_Types_With_Blank";
            this.sp07170UTLandOwnerTypesWithBlankBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID2;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Owner Type";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 201;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // SignatureDateTextEdit2
            // 
            this.SignatureDateTextEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SignatureDate", true));
            this.SignatureDateTextEdit2.Location = new System.Drawing.Point(581, 885);
            this.SignatureDateTextEdit2.MenuManager = this.barManager1;
            this.SignatureDateTextEdit2.Name = "SignatureDateTextEdit2";
            this.SignatureDateTextEdit2.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SignatureDateTextEdit2, true);
            this.SignatureDateTextEdit2.Size = new System.Drawing.Size(359, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SignatureDateTextEdit2, optionsSpelling4);
            this.SignatureDateTextEdit2.StyleController = this.dataLayoutControl1;
            this.SignatureDateTextEdit2.TabIndex = 13;
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl30.Appearance.Options.UseFont = true;
            this.labelControl30.Location = new System.Drawing.Point(397, 61);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(98, 19);
            this.labelControl30.StyleController = this.dataLayoutControl1;
            this.labelControl30.TabIndex = 160;
            this.labelControl30.Text = "CONSENT    ";
            // 
            // SentByStaffIDTextEdit
            // 
            this.SentByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SentByStaffID", true));
            this.SentByStaffIDTextEdit.Location = new System.Drawing.Point(172, 194);
            this.SentByStaffIDTextEdit.MenuManager = this.barManager1;
            this.SentByStaffIDTextEdit.Name = "SentByStaffIDTextEdit";
            this.SentByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SentByStaffIDTextEdit, true);
            this.SentByStaffIDTextEdit.Size = new System.Drawing.Size(172, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SentByStaffIDTextEdit, optionsSpelling5);
            this.SentByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SentByStaffIDTextEdit.TabIndex = 110;
            // 
            // PermissionDocumentIDTextEdit
            // 
            this.PermissionDocumentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PermissionDocumentID", true));
            this.PermissionDocumentIDTextEdit.Location = new System.Drawing.Point(172, 194);
            this.PermissionDocumentIDTextEdit.MenuManager = this.barManager1;
            this.PermissionDocumentIDTextEdit.Name = "PermissionDocumentIDTextEdit";
            this.PermissionDocumentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PermissionDocumentIDTextEdit, true);
            this.PermissionDocumentIDTextEdit.Size = new System.Drawing.Size(536, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PermissionDocumentIDTextEdit, optionsSpelling6);
            this.PermissionDocumentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PermissionDocumentIDTextEdit.TabIndex = 4;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(11, 35);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemTextEditDateTime2});
            this.gridControl4.Size = new System.Drawing.Size(937, 306);
            this.gridControl4.TabIndex = 13;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.gridColumn2,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Description";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 283;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 102;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            this.colDocumentRemarks.Width = 134;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // gridControl6
            // 
            this.gridControl6.AllowDrop = true;
            this.gridControl6.DataSource = this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Map", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Map", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Preview Map", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "reload")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(11, 35);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemTextEdit1});
            this.gridControl6.Size = new System.Drawing.Size(937, 306);
            this.gridControl6.TabIndex = 13;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource
            // 
            this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource.DataMember = "sp07202_UT_Mapping_Snapshots_Linked_Snapshots";
            this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource.DataSource = this.dataSet_UT_Mapping;
            // 
            // dataSet_UT_Mapping
            // 
            this.dataSet_UT_Mapping.DataSetName = "DataSet_UT_Mapping";
            this.dataSet_UT_Mapping.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedMapID,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.colMapOrder,
            this.colCreatedByID,
            this.colDateTimeCreated,
            this.gridColumn8,
            this.colCreatedByName,
            this.colLinkedToDescription});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView6.OptionsCustomization.AllowFilter = false;
            this.gridView6.OptionsCustomization.AllowGroup = false;
            this.gridView6.OptionsCustomization.AllowSort = false;
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.RowAutoHeight = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMapOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colLinkedMapID
            // 
            this.colLinkedMapID.Caption = "Linked Map ID";
            this.colLinkedMapID.FieldName = "LinkedMapID";
            this.colLinkedMapID.Name = "colLinkedMapID";
            this.colLinkedMapID.OptionsColumn.AllowEdit = false;
            this.colLinkedMapID.OptionsColumn.AllowFocus = false;
            this.colLinkedMapID.OptionsColumn.ReadOnly = true;
            this.colLinkedMapID.Width = 78;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Linked To Record ID";
            this.gridColumn4.FieldName = "LinkedToRecordID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 107;
            // 
            // gridColumn5
            // 
            this.gridColumn5.FieldName = "LinkedToRecordTypeID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 134;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Map Description";
            this.gridColumn6.FieldName = "Description";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 250;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Saved File";
            this.gridColumn7.FieldName = "DocumentPath";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 152;
            // 
            // colMapOrder
            // 
            this.colMapOrder.Caption = "Order";
            this.colMapOrder.FieldName = "MapOrder";
            this.colMapOrder.Name = "colMapOrder";
            this.colMapOrder.OptionsColumn.AllowEdit = false;
            this.colMapOrder.OptionsColumn.AllowFocus = false;
            this.colMapOrder.OptionsColumn.ReadOnly = true;
            this.colMapOrder.Visible = true;
            this.colMapOrder.VisibleIndex = 0;
            this.colMapOrder.Width = 54;
            // 
            // colCreatedByID
            // 
            this.colCreatedByID.Caption = "Created By ID";
            this.colCreatedByID.FieldName = "CreatedByID";
            this.colCreatedByID.Name = "colCreatedByID";
            this.colCreatedByID.OptionsColumn.AllowEdit = false;
            this.colCreatedByID.OptionsColumn.AllowFocus = false;
            this.colCreatedByID.OptionsColumn.ReadOnly = true;
            this.colCreatedByID.Width = 79;
            // 
            // colDateTimeCreated
            // 
            this.colDateTimeCreated.Caption = "Date Created";
            this.colDateTimeCreated.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDateTimeCreated.FieldName = "DateTimeCreated";
            this.colDateTimeCreated.Name = "colDateTimeCreated";
            this.colDateTimeCreated.OptionsColumn.AllowEdit = false;
            this.colDateTimeCreated.OptionsColumn.AllowFocus = false;
            this.colDateTimeCreated.OptionsColumn.ReadOnly = true;
            this.colDateTimeCreated.Visible = true;
            this.colDateTimeCreated.VisibleIndex = 3;
            this.colDateTimeCreated.Width = 96;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Remarks";
            this.gridColumn8.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn8.FieldName = "Remarks";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colCreatedByName
            // 
            this.colCreatedByName.Caption = "Created By";
            this.colCreatedByName.FieldName = "CreatedByName";
            this.colCreatedByName.Name = "colCreatedByName";
            this.colCreatedByName.OptionsColumn.AllowEdit = false;
            this.colCreatedByName.OptionsColumn.AllowFocus = false;
            this.colCreatedByName.OptionsColumn.ReadOnly = true;
            this.colCreatedByName.Visible = true;
            this.colCreatedByName.VisibleIndex = 2;
            this.colCreatedByName.Width = 114;
            // 
            // colLinkedToDescription
            // 
            this.colLinkedToDescription.Caption = "Linked To";
            this.colLinkedToDescription.FieldName = "LinkedToDescription";
            this.colLinkedToDescription.Name = "colLinkedToDescription";
            this.colLinkedToDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedToDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedToDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedToDescription.Width = 316;
            // 
            // InfoLabel1
            // 
            this.InfoLabel1.Location = new System.Drawing.Point(19, 808);
            this.InfoLabel1.Name = "InfoLabel1";
            this.InfoLabel1.Size = new System.Drawing.Size(921, 39);
            this.InfoLabel1.StyleController = this.dataLayoutControl1;
            this.InfoLabel1.TabIndex = 136;
            this.InfoLabel1.Text = resources.GetString("InfoLabel1.Text");
            // 
            // EmergencyAccessMemoExEdit
            // 
            this.EmergencyAccessMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "EmergencyAccess", true));
            this.EmergencyAccessMemoExEdit.Location = new System.Drawing.Point(147, 61);
            this.EmergencyAccessMemoExEdit.MenuManager = this.barManager1;
            this.EmergencyAccessMemoExEdit.Name = "EmergencyAccessMemoExEdit";
            this.EmergencyAccessMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EmergencyAccessMemoExEdit.Properties.MaxLength = 32000;
            this.EmergencyAccessMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.EmergencyAccessMemoExEdit.Properties.ShowIcon = false;
            this.EmergencyAccessMemoExEdit.Size = new System.Drawing.Size(744, 20);
            this.EmergencyAccessMemoExEdit.StyleController = this.dataLayoutControl1;
            this.EmergencyAccessMemoExEdit.TabIndex = 135;
            this.EmergencyAccessMemoExEdit.Enter += new System.EventHandler(this.EmergencyAccessMemoExEdit_Enter);
            // 
            // OwnerAddressMemoExEdit
            // 
            this.OwnerAddressMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerAddress", true));
            this.OwnerAddressMemoExEdit.Location = new System.Drawing.Point(168, 909);
            this.OwnerAddressMemoExEdit.MenuManager = this.barManager1;
            this.OwnerAddressMemoExEdit.Name = "OwnerAddressMemoExEdit";
            this.OwnerAddressMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OwnerAddressMemoExEdit.Properties.MaxLength = 1000;
            this.OwnerAddressMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.OwnerAddressMemoExEdit.Properties.ShowIcon = false;
            this.OwnerAddressMemoExEdit.Size = new System.Drawing.Size(260, 20);
            this.OwnerAddressMemoExEdit.StyleController = this.dataLayoutControl1;
            this.OwnerAddressMemoExEdit.TabIndex = 10;
            // 
            // SiteAddressMemoExEdit
            // 
            this.SiteAddressMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SiteAddress", true));
            this.SiteAddressMemoExEdit.Location = new System.Drawing.Point(147, 61);
            this.SiteAddressMemoExEdit.MenuManager = this.barManager1;
            this.SiteAddressMemoExEdit.Name = "SiteAddressMemoExEdit";
            this.SiteAddressMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SiteAddressMemoExEdit.Properties.MaxLength = 1000;
            this.SiteAddressMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SiteAddressMemoExEdit.Properties.ShowIcon = false;
            this.SiteAddressMemoExEdit.Size = new System.Drawing.Size(744, 20);
            this.SiteAddressMemoExEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressMemoExEdit.TabIndex = 133;
            // 
            // SignatureFileButtonEdit
            // 
            this.SignatureFileButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SignatureFile", true));
            this.SignatureFileButtonEdit.Location = new System.Drawing.Point(581, 861);
            this.SignatureFileButtonEdit.MenuManager = this.barManager1;
            this.SignatureFileButtonEdit.Name = "SignatureFileButtonEdit";
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem4.Text = "Capture - information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to open the Signature Capture screen.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem5.Text = "View - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to view any saved Signature";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.SignatureFileButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Capture", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "capture", superToolTip4, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "view", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SignatureFileButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SignatureFileButtonEdit.Size = new System.Drawing.Size(359, 20);
            this.SignatureFileButtonEdit.StyleController = this.dataLayoutControl1;
            this.SignatureFileButtonEdit.TabIndex = 12;
            this.SignatureFileButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SignatureFileButtonEdit_ButtonClick);
            // 
            // OwnerEmailTextEdit
            // 
            this.OwnerEmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerEmail", true));
            this.OwnerEmailTextEdit.Location = new System.Drawing.Point(581, 933);
            this.OwnerEmailTextEdit.MenuManager = this.barManager1;
            this.OwnerEmailTextEdit.Name = "OwnerEmailTextEdit";
            this.OwnerEmailTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerEmailTextEdit, true);
            this.OwnerEmailTextEdit.Size = new System.Drawing.Size(359, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerEmailTextEdit, optionsSpelling7);
            this.OwnerEmailTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerEmailTextEdit.TabIndex = 15;
            // 
            // OwnerTelephoneTextEdit
            // 
            this.OwnerTelephoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerTelephone", true));
            this.OwnerTelephoneTextEdit.Location = new System.Drawing.Point(581, 909);
            this.OwnerTelephoneTextEdit.MenuManager = this.barManager1;
            this.OwnerTelephoneTextEdit.Name = "OwnerTelephoneTextEdit";
            this.OwnerTelephoneTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerTelephoneTextEdit, true);
            this.OwnerTelephoneTextEdit.Size = new System.Drawing.Size(359, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerTelephoneTextEdit, optionsSpelling8);
            this.OwnerTelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerTelephoneTextEdit.TabIndex = 14;
            // 
            // OwnerPostcodeTextEdit
            // 
            this.OwnerPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerPostcode", true));
            this.OwnerPostcodeTextEdit.Location = new System.Drawing.Point(168, 933);
            this.OwnerPostcodeTextEdit.MenuManager = this.barManager1;
            this.OwnerPostcodeTextEdit.Name = "OwnerPostcodeTextEdit";
            this.OwnerPostcodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerPostcodeTextEdit, true);
            this.OwnerPostcodeTextEdit.Size = new System.Drawing.Size(260, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerPostcodeTextEdit, optionsSpelling9);
            this.OwnerPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerPostcodeTextEdit.TabIndex = 11;
            // 
            // OwnerSalutationTextEdit
            // 
            this.OwnerSalutationTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerSalutation", true));
            this.OwnerSalutationTextEdit.Location = new System.Drawing.Point(168, 885);
            this.OwnerSalutationTextEdit.MenuManager = this.barManager1;
            this.OwnerSalutationTextEdit.Name = "OwnerSalutationTextEdit";
            this.OwnerSalutationTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerSalutationTextEdit, true);
            this.OwnerSalutationTextEdit.Size = new System.Drawing.Size(260, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerSalutationTextEdit, optionsSpelling10);
            this.OwnerSalutationTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerSalutationTextEdit.TabIndex = 9;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(600, 386);
            this.textEdit4.MenuManager = this.barManager1;
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit4, true);
            this.textEdit4.Size = new System.Drawing.Size(301, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit4, optionsSpelling11);
            this.textEdit4.StyleController = this.dataLayoutControl1;
            this.textEdit4.TabIndex = 119;
            // 
            // ContractorTextEdit
            // 
            this.ContractorTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Contractor", true));
            this.ContractorTextEdit.EditValue = "";
            this.ContractorTextEdit.Location = new System.Drawing.Point(483, 127);
            this.ContractorTextEdit.MenuManager = this.barManager1;
            this.ContractorTextEdit.Name = "ContractorTextEdit";
            this.ContractorTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractorTextEdit, true);
            this.ContractorTextEdit.Size = new System.Drawing.Size(408, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractorTextEdit, optionsSpelling12);
            this.ContractorTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractorTextEdit.TabIndex = 117;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(397, 85);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(551, 46);
            this.labelControl1.StyleController = this.dataLayoutControl1;
            this.labelControl1.TabIndex = 116;
            this.labelControl1.Text = "Tree and Vegetation\r\nClearance Consent Form";
            // 
            // ReferenceNumberTextEdit
            // 
            this.ReferenceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ReferenceNumber", true));
            this.ReferenceNumberTextEdit.Location = new System.Drawing.Point(648, 61);
            this.ReferenceNumberTextEdit.MenuManager = this.barManager1;
            this.ReferenceNumberTextEdit.Name = "ReferenceNumberTextEdit";
            this.ReferenceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReferenceNumberTextEdit, true);
            this.ReferenceNumberTextEdit.Size = new System.Drawing.Size(300, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReferenceNumberTextEdit, optionsSpelling13);
            this.ReferenceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ReferenceNumberTextEdit.TabIndex = 0;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.Location = new System.Drawing.Point(11, 61);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(194, 72);
            this.pictureEdit1.StyleController = this.dataLayoutControl1;
            this.pictureEdit1.TabIndex = 114;
            // 
            // NearestAandETextEdit
            // 
            this.NearestAandETextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "NearestAandE", true));
            this.NearestAandETextEdit.Location = new System.Drawing.Point(147, 61);
            this.NearestAandETextEdit.MenuManager = this.barManager1;
            this.NearestAandETextEdit.Name = "NearestAandETextEdit";
            this.NearestAandETextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NearestAandETextEdit, true);
            this.NearestAandETextEdit.Size = new System.Drawing.Size(744, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NearestAandETextEdit, optionsSpelling14);
            this.NearestAandETextEdit.StyleController = this.dataLayoutControl1;
            this.NearestAandETextEdit.TabIndex = 73;
            // 
            // GridReferenceTextEdit
            // 
            this.GridReferenceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "GridReference", true));
            this.GridReferenceTextEdit.Location = new System.Drawing.Point(147, 61);
            this.GridReferenceTextEdit.MenuManager = this.barManager1;
            this.GridReferenceTextEdit.Name = "GridReferenceTextEdit";
            this.GridReferenceTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GridReferenceTextEdit, true);
            this.GridReferenceTextEdit.Size = new System.Drawing.Size(744, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GridReferenceTextEdit, optionsSpelling15);
            this.GridReferenceTextEdit.StyleController = this.dataLayoutControl1;
            this.GridReferenceTextEdit.TabIndex = 72;
            // 
            // NearestTelephonePointTextEdit
            // 
            this.NearestTelephonePointTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "NearestTelephonePoint", true));
            this.NearestTelephonePointTextEdit.Location = new System.Drawing.Point(147, 61);
            this.NearestTelephonePointTextEdit.MenuManager = this.barManager1;
            this.NearestTelephonePointTextEdit.Name = "NearestTelephonePointTextEdit";
            this.NearestTelephonePointTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NearestTelephonePointTextEdit, true);
            this.NearestTelephonePointTextEdit.Size = new System.Drawing.Size(744, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NearestTelephonePointTextEdit, optionsSpelling16);
            this.NearestTelephonePointTextEdit.StyleController = this.dataLayoutControl1;
            this.NearestTelephonePointTextEdit.TabIndex = 71;
            // 
            // SentByStaffNameTextEdit
            // 
            this.SentByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SentByStaffName", true));
            this.SentByStaffNameTextEdit.Location = new System.Drawing.Point(172, 266);
            this.SentByStaffNameTextEdit.MenuManager = this.barManager1;
            this.SentByStaffNameTextEdit.Name = "SentByStaffNameTextEdit";
            this.SentByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SentByStaffNameTextEdit, true);
            this.SentByStaffNameTextEdit.Size = new System.Drawing.Size(257, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SentByStaffNameTextEdit, optionsSpelling17);
            this.SentByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SentByStaffNameTextEdit.TabIndex = 111;
            // 
            // SentByPostCheckEdit
            // 
            this.SentByPostCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SentByPost", true));
            this.SentByPostCheckEdit.Location = new System.Drawing.Point(172, 219);
            this.SentByPostCheckEdit.MenuManager = this.barManager1;
            this.SentByPostCheckEdit.Name = "SentByPostCheckEdit";
            this.SentByPostCheckEdit.Properties.Caption = "(Calculated)";
            this.SentByPostCheckEdit.Properties.ReadOnly = true;
            this.SentByPostCheckEdit.Properties.ValueChecked = 1;
            this.SentByPostCheckEdit.Properties.ValueUnchecked = 0;
            this.SentByPostCheckEdit.Size = new System.Drawing.Size(257, 19);
            this.SentByPostCheckEdit.StyleController = this.dataLayoutControl1;
            this.SentByPostCheckEdit.TabIndex = 109;
            // 
            // PostageRequiredCheckEdit
            // 
            this.PostageRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PostageRequired", true));
            this.PostageRequiredCheckEdit.Location = new System.Drawing.Point(172, 196);
            this.PostageRequiredCheckEdit.MenuManager = this.barManager1;
            this.PostageRequiredCheckEdit.Name = "PostageRequiredCheckEdit";
            this.PostageRequiredCheckEdit.Properties.Caption = "";
            this.PostageRequiredCheckEdit.Properties.ValueChecked = 1;
            this.PostageRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.PostageRequiredCheckEdit.Size = new System.Drawing.Size(257, 19);
            this.PostageRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.PostageRequiredCheckEdit.TabIndex = 108;
            this.PostageRequiredCheckEdit.CheckedChanged += new System.EventHandler(this.PostageRequiredCheckEdit_CheckedChanged);
            // 
            // PermissionEmailedCheckEdit
            // 
            this.PermissionEmailedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PermissionEmailed", true));
            this.PermissionEmailedCheckEdit.Location = new System.Drawing.Point(172, 173);
            this.PermissionEmailedCheckEdit.MenuManager = this.barManager1;
            this.PermissionEmailedCheckEdit.Name = "PermissionEmailedCheckEdit";
            this.PermissionEmailedCheckEdit.Properties.Caption = "(Calculated)";
            this.PermissionEmailedCheckEdit.Properties.ReadOnly = true;
            this.PermissionEmailedCheckEdit.Properties.ValueChecked = 1;
            this.PermissionEmailedCheckEdit.Properties.ValueUnchecked = 0;
            this.PermissionEmailedCheckEdit.Size = new System.Drawing.Size(257, 19);
            this.PermissionEmailedCheckEdit.StyleController = this.dataLayoutControl1;
            this.PermissionEmailedCheckEdit.TabIndex = 107;
            // 
            // SignatureDateDateEdit
            // 
            this.SignatureDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SignatureDate", true));
            this.SignatureDateDateEdit.EditValue = null;
            this.SignatureDateDateEdit.Location = new System.Drawing.Point(417, 69);
            this.SignatureDateDateEdit.MenuManager = this.barManager1;
            this.SignatureDateDateEdit.Name = "SignatureDateDateEdit";
            this.SignatureDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SignatureDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SignatureDateDateEdit.Properties.Mask.EditMask = "g";
            this.SignatureDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SignatureDateDateEdit.Properties.ReadOnly = true;
            this.SignatureDateDateEdit.Size = new System.Drawing.Size(519, 20);
            this.SignatureDateDateEdit.StyleController = this.dataLayoutControl1;
            this.SignatureDateDateEdit.TabIndex = 69;
            // 
            // EmailedToClientDateDateEdit
            // 
            this.EmailedToClientDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "EmailedToClientDate", true));
            this.EmailedToClientDateDateEdit.EditValue = null;
            this.EmailedToClientDateDateEdit.Location = new System.Drawing.Point(612, 173);
            this.EmailedToClientDateDateEdit.MenuManager = this.barManager1;
            this.EmailedToClientDateDateEdit.Name = "EmailedToClientDateDateEdit";
            this.EmailedToClientDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EmailedToClientDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EmailedToClientDateDateEdit.Properties.Mask.EditMask = "g";
            this.EmailedToClientDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EmailedToClientDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EmailedToClientDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EmailedToClientDateDateEdit.Properties.ReadOnly = true;
            this.EmailedToClientDateDateEdit.Size = new System.Drawing.Size(324, 20);
            this.EmailedToClientDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EmailedToClientDateDateEdit.TabIndex = 66;
            // 
            // EmailedToCustomerDateDateEdit
            // 
            this.EmailedToCustomerDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "EmailedToCustomerDate", true));
            this.EmailedToCustomerDateDateEdit.EditValue = null;
            this.EmailedToCustomerDateDateEdit.Location = new System.Drawing.Point(172, 242);
            this.EmailedToCustomerDateDateEdit.MenuManager = this.barManager1;
            this.EmailedToCustomerDateDateEdit.Name = "EmailedToCustomerDateDateEdit";
            this.EmailedToCustomerDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EmailedToCustomerDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EmailedToCustomerDateDateEdit.Properties.Mask.EditMask = "g";
            this.EmailedToCustomerDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EmailedToCustomerDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EmailedToCustomerDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EmailedToCustomerDateDateEdit.Properties.ReadOnly = true;
            this.EmailedToCustomerDateDateEdit.Size = new System.Drawing.Size(257, 20);
            this.EmailedToCustomerDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EmailedToCustomerDateDateEdit.TabIndex = 65;
            // 
            // PDFFileHyperLinkEdit
            // 
            this.PDFFileHyperLinkEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PDFFile", true));
            this.PDFFileHyperLinkEdit.Location = new System.Drawing.Point(172, 93);
            this.PDFFileHyperLinkEdit.MenuManager = this.barManager1;
            this.PDFFileHyperLinkEdit.Name = "PDFFileHyperLinkEdit";
            this.PDFFileHyperLinkEdit.Properties.ReadOnly = true;
            this.PDFFileHyperLinkEdit.Size = new System.Drawing.Size(764, 20);
            this.PDFFileHyperLinkEdit.StyleController = this.dataLayoutControl1;
            this.PDFFileHyperLinkEdit.TabIndex = 64;
            this.PDFFileHyperLinkEdit.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.PDFFileHyperLinkEdit_OpenLink);
            // 
            // SignatureFileHyperLinkEdit
            // 
            this.SignatureFileHyperLinkEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SignatureFile", true));
            this.SignatureFileHyperLinkEdit.Location = new System.Drawing.Point(172, 69);
            this.SignatureFileHyperLinkEdit.MenuManager = this.barManager1;
            this.SignatureFileHyperLinkEdit.Name = "SignatureFileHyperLinkEdit";
            this.SignatureFileHyperLinkEdit.Properties.ReadOnly = true;
            this.SignatureFileHyperLinkEdit.Size = new System.Drawing.Size(92, 20);
            this.SignatureFileHyperLinkEdit.StyleController = this.dataLayoutControl1;
            this.SignatureFileHyperLinkEdit.TabIndex = 63;
            this.SignatureFileHyperLinkEdit.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.SignatureFileHyperLinkEdit_OpenLink);
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(11, 35);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(99, 22);
            this.btnSave.StyleController = this.dataLayoutControl1;
            this.btnSave.TabIndex = 16;
            this.btnSave.Text = "Save Changes";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // RaisedByIDGridLookUpEdit
            // 
            this.RaisedByIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "RaisedByID", true));
            this.RaisedByIDGridLookUpEdit.Location = new System.Drawing.Point(483, 127);
            this.RaisedByIDGridLookUpEdit.MenuManager = this.barManager1;
            this.RaisedByIDGridLookUpEdit.Name = "RaisedByIDGridLookUpEdit";
            this.RaisedByIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RaisedByIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.RaisedByIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.RaisedByIDGridLookUpEdit.Properties.MaxLength = 50;
            this.RaisedByIDGridLookUpEdit.Properties.NullText = "";
            this.RaisedByIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.RaisedByIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.RaisedByIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.RaisedByIDGridLookUpEdit.Size = new System.Drawing.Size(408, 20);
            this.RaisedByIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RaisedByIDGridLookUpEdit.TabIndex = 54;
            // 
            // sp00226StaffListWithBlankBindingSource
            // 
            this.sp00226StaffListWithBlankBindingSource.DataMember = "sp00226_Staff_List_With_Blank";
            this.sp00226StaffListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Active";
            this.gridColumn14.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn14.FieldName = "Active";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            this.gridColumn14.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Surname: Forename";
            this.gridColumn15.FieldName = "DisplayName";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 225;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Email";
            this.gridColumn16.FieldName = "Email";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 192;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Forename";
            this.gridColumn17.FieldName = "Forename";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 178;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Network ID";
            this.gridColumn18.FieldName = "NetworkID";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 180;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Staff ID";
            this.gridColumn19.FieldName = "StaffID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 59;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Staff Name";
            this.gridColumn20.FieldName = "StaffName";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 231;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Surname";
            this.gridColumn21.FieldName = "Surname";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 191;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "User Type";
            this.gridColumn22.FieldName = "UserType";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 223;
            // 
            // DateRaisedDateEdit
            // 
            this.DateRaisedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "DateRaised", true));
            this.DateRaisedDateEdit.EditValue = null;
            this.DateRaisedDateEdit.Location = new System.Drawing.Point(147, 61);
            this.DateRaisedDateEdit.MenuManager = this.barManager1;
            this.DateRaisedDateEdit.Name = "DateRaisedDateEdit";
            this.DateRaisedDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateRaisedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateRaisedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateRaisedDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateRaisedDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateRaisedDateEdit.Size = new System.Drawing.Size(744, 20);
            this.DateRaisedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateRaisedDateEdit.TabIndex = 52;
            this.DateRaisedDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DateRaisedDateEdit_Validating);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07392UTPDLinkedWorkBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Work to Permission", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Work", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Work Permission", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(11, 435);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDate,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditNumeric2DP,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemGridLookUpEditStatus,
            this.repositoryItemButtonEditWork,
            this.repositoryItemTextEditMeters});
            this.gridControl1.Size = new System.Drawing.Size(937, 272);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07392UTPDLinkedWorkBindingSource
            // 
            this.sp07392UTPDLinkedWorkBindingSource.DataMember = "sp07392_UT_PD_Linked_Work";
            this.sp07392UTPDLinkedWorkBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionPermissionID,
            this.colPermissionedPoleID,
            this.colActionID,
            this.colPermissionStatusID,
            this.colPermissionDocumentID,
            this.colRemarks,
            this.colLandOwnerName,
            this.colPermissionDocumentDateRaised,
            this.colPoleNumber,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colJobDescription,
            this.colTreeReferenceNumber,
            this.colSurveyID,
            this.colTreeID,
            this.colPoleID,
            this.colStumpTreatmentNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentSpraying,
            this.colTreeReplacementNone,
            this.colTreeReplacementStandards,
            this.colTreeReplacementWhips,
            this.colESQCRCategory,
            this.colG55Category,
            this.colAchievableClearance,
            this.colLinkedSpecies,
            this.colLandOwnerRestrictedCut,
            this.colJobTypeID,
            this.gridColumn1,
            this.colRefusalReason,
            this.colRefusalReasonID,
            this.colRefusalRemarks,
            this.colShutdownRequired,
            this.colMapID});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colActionPermissionID
            // 
            this.colActionPermissionID.Caption = "Action Permission ID";
            this.colActionPermissionID.FieldName = "ActionPermissionID";
            this.colActionPermissionID.Name = "colActionPermissionID";
            this.colActionPermissionID.OptionsColumn.AllowEdit = false;
            this.colActionPermissionID.OptionsColumn.AllowFocus = false;
            this.colActionPermissionID.OptionsColumn.ReadOnly = true;
            this.colActionPermissionID.Width = 108;
            // 
            // colPermissionedPoleID
            // 
            this.colPermissionedPoleID.Caption = "Permissioned Pole ID";
            this.colPermissionedPoleID.FieldName = "PermissionedPoleID";
            this.colPermissionedPoleID.Name = "colPermissionedPoleID";
            this.colPermissionedPoleID.OptionsColumn.AllowEdit = false;
            this.colPermissionedPoleID.OptionsColumn.AllowFocus = false;
            this.colPermissionedPoleID.OptionsColumn.ReadOnly = true;
            this.colPermissionedPoleID.Width = 110;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colPermissionStatusID
            // 
            this.colPermissionStatusID.Caption = "Permission Status";
            this.colPermissionStatusID.ColumnEdit = this.repositoryItemGridLookUpEditStatus;
            this.colPermissionStatusID.FieldName = "PermissionStatusID";
            this.colPermissionStatusID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colPermissionStatusID.Name = "colPermissionStatusID";
            this.colPermissionStatusID.Visible = true;
            this.colPermissionStatusID.VisibleIndex = 7;
            this.colPermissionStatusID.Width = 118;
            // 
            // repositoryItemGridLookUpEditStatus
            // 
            this.repositoryItemGridLookUpEditStatus.AutoHeight = false;
            this.repositoryItemGridLookUpEditStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditStatus.DataSource = this.sp07197UTPermissionStatusesListBindingSource;
            this.repositoryItemGridLookUpEditStatus.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditStatus.Name = "repositoryItemGridLookUpEditStatus";
            this.repositoryItemGridLookUpEditStatus.NullText = "";
            this.repositoryItemGridLookUpEditStatus.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditStatus.ValueMember = "ID";
            this.repositoryItemGridLookUpEditStatus.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryItemGridLookUpEditStatus_EditValueChanging);
            // 
            // sp07197UTPermissionStatusesListBindingSource
            // 
            this.sp07197UTPermissionStatusesListBindingSource.DataMember = "sp07197_UT_Permission_Statuses_List";
            this.sp07197UTPermissionStatusesListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colRecordOrder});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Permission Status";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 169;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Width = 45;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Width = 63;
            // 
            // colPermissionDocumentID
            // 
            this.colPermissionDocumentID.Caption = "Permission Document ID";
            this.colPermissionDocumentID.FieldName = "PermissionDocumentID";
            this.colPermissionDocumentID.Name = "colPermissionDocumentID";
            this.colPermissionDocumentID.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentID.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentID.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentID.Width = 126;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 6;
            this.colRemarks.Width = 152;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLandOwnerName
            // 
            this.colLandOwnerName.Caption = "Land Owner Name";
            this.colLandOwnerName.FieldName = "LandOwnerName";
            this.colLandOwnerName.Name = "colLandOwnerName";
            this.colLandOwnerName.OptionsColumn.AllowEdit = false;
            this.colLandOwnerName.OptionsColumn.AllowFocus = false;
            this.colLandOwnerName.OptionsColumn.ReadOnly = true;
            this.colLandOwnerName.Width = 144;
            // 
            // colPermissionDocumentDateRaised
            // 
            this.colPermissionDocumentDateRaised.Caption = "Permission Document Date Raised";
            this.colPermissionDocumentDateRaised.ColumnEdit = this.repositoryItemTextEditDate;
            this.colPermissionDocumentDateRaised.FieldName = "PermissionDocumentDateRaised";
            this.colPermissionDocumentDateRaised.Name = "colPermissionDocumentDateRaised";
            this.colPermissionDocumentDateRaised.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentDateRaised.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentDateRaised.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentDateRaised.Width = 173;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "g";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 0;
            this.colPoleNumber.Width = 124;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Width = 167;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Width = 92;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Description of Work Carried Out";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 3;
            this.colJobDescription.Width = 225;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Tree Reference";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 1;
            this.colTreeReferenceNumber.Width = 115;
            // 
            // colSurveyID
            // 
            this.colSurveyID.Caption = "Survey ID";
            this.colSurveyID.FieldName = "SurveyID";
            this.colSurveyID.Name = "colSurveyID";
            this.colSurveyID.OptionsColumn.AllowEdit = false;
            this.colSurveyID.OptionsColumn.AllowFocus = false;
            this.colSurveyID.OptionsColumn.ReadOnly = true;
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.Width = 129;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatment - Eco Plug";
            this.colStumpTreatmentEcoPlugs.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.Width = 144;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.colStumpTreatmentPaint.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.Width = 128;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.colStumpTreatmentSpraying.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.Width = 146;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement - None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.Width = 133;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.colTreeReplacementStandards.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.Width = 157;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.colTreeReplacementWhips.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.Width = 137;
            // 
            // colESQCRCategory
            // 
            this.colESQCRCategory.Caption = "ESQCR Cat";
            this.colESQCRCategory.FieldName = "ESQCRCategory";
            this.colESQCRCategory.Name = "colESQCRCategory";
            this.colESQCRCategory.OptionsColumn.AllowEdit = false;
            this.colESQCRCategory.OptionsColumn.AllowFocus = false;
            this.colESQCRCategory.OptionsColumn.ReadOnly = true;
            this.colESQCRCategory.Width = 65;
            // 
            // colG55Category
            // 
            this.colG55Category.Caption = "G55/2 Cat";
            this.colG55Category.FieldName = "G55Category";
            this.colG55Category.Name = "colG55Category";
            this.colG55Category.OptionsColumn.AllowEdit = false;
            this.colG55Category.OptionsColumn.AllowFocus = false;
            this.colG55Category.OptionsColumn.ReadOnly = true;
            this.colG55Category.Width = 66;
            // 
            // colAchievableClearance
            // 
            this.colAchievableClearance.Caption = "Clearance";
            this.colAchievableClearance.ColumnEdit = this.repositoryItemTextEditMeters;
            this.colAchievableClearance.FieldName = "AchievableClearance";
            this.colAchievableClearance.Name = "colAchievableClearance";
            this.colAchievableClearance.OptionsColumn.AllowEdit = false;
            this.colAchievableClearance.OptionsColumn.AllowFocus = false;
            this.colAchievableClearance.OptionsColumn.ReadOnly = true;
            this.colAchievableClearance.Visible = true;
            this.colAchievableClearance.VisibleIndex = 4;
            this.colAchievableClearance.Width = 59;
            // 
            // repositoryItemTextEditMeters
            // 
            this.repositoryItemTextEditMeters.AutoHeight = false;
            this.repositoryItemTextEditMeters.Mask.EditMask = "###0.00 M";
            this.repositoryItemTextEditMeters.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMeters.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMeters.Name = "repositoryItemTextEditMeters";
            // 
            // colLinkedSpecies
            // 
            this.colLinkedSpecies.Caption = "Predominant Tree(s) Species";
            this.colLinkedSpecies.FieldName = "LinkedSpecies";
            this.colLinkedSpecies.Name = "colLinkedSpecies";
            this.colLinkedSpecies.OptionsColumn.AllowEdit = false;
            this.colLinkedSpecies.OptionsColumn.AllowFocus = false;
            this.colLinkedSpecies.OptionsColumn.ReadOnly = true;
            this.colLinkedSpecies.Visible = true;
            this.colLinkedSpecies.VisibleIndex = 2;
            this.colLinkedSpecies.Width = 148;
            // 
            // colLandOwnerRestrictedCut
            // 
            this.colLandOwnerRestrictedCut.Caption = "Restricted Cut";
            this.colLandOwnerRestrictedCut.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colLandOwnerRestrictedCut.FieldName = "LandOwnerRestrictedCut";
            this.colLandOwnerRestrictedCut.Name = "colLandOwnerRestrictedCut";
            this.colLandOwnerRestrictedCut.OptionsColumn.AllowEdit = false;
            this.colLandOwnerRestrictedCut.OptionsColumn.AllowFocus = false;
            this.colLandOwnerRestrictedCut.OptionsColumn.ReadOnly = true;
            this.colLandOwnerRestrictedCut.Width = 80;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Edit";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEditWork;
            this.gridColumn1.CustomizationCaption = "Edit Button";
            this.gridColumn1.FieldName = "gridColumn1";
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowSize = false;
            this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn1.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn1.OptionsFilter.AllowFilter = false;
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 8;
            this.gridColumn1.Width = 60;
            // 
            // repositoryItemButtonEditWork
            // 
            this.repositoryItemButtonEditWork.AutoHeight = false;
            this.repositoryItemButtonEditWork.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit Job", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to edit the job [and optionally any linked, equipment and materials]", "edit", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditWork.Name = "repositoryItemButtonEditWork";
            this.repositoryItemButtonEditWork.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditWork.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditWork_ButtonClick);
            // 
            // colRefusalReason
            // 
            this.colRefusalReason.Caption = "Refusal Reason";
            this.colRefusalReason.FieldName = "RefusalReason";
            this.colRefusalReason.Name = "colRefusalReason";
            this.colRefusalReason.OptionsColumn.AllowEdit = false;
            this.colRefusalReason.OptionsColumn.AllowFocus = false;
            this.colRefusalReason.OptionsColumn.ReadOnly = true;
            this.colRefusalReason.Width = 86;
            // 
            // colRefusalReasonID
            // 
            this.colRefusalReasonID.Caption = "Refusal Reason ID";
            this.colRefusalReasonID.FieldName = "RefusalReasonID";
            this.colRefusalReasonID.Name = "colRefusalReasonID";
            this.colRefusalReasonID.OptionsColumn.AllowEdit = false;
            this.colRefusalReasonID.OptionsColumn.AllowFocus = false;
            this.colRefusalReasonID.OptionsColumn.ReadOnly = true;
            this.colRefusalReasonID.Width = 100;
            // 
            // colRefusalRemarks
            // 
            this.colRefusalRemarks.Caption = "Refusal Remarks";
            this.colRefusalRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRefusalRemarks.FieldName = "RefusalRemarks";
            this.colRefusalRemarks.Name = "colRefusalRemarks";
            this.colRefusalRemarks.OptionsColumn.ReadOnly = true;
            this.colRefusalRemarks.Width = 91;
            // 
            // colShutdownRequired
            // 
            this.colShutdownRequired.Caption = "Shutdown";
            this.colShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colShutdownRequired.FieldName = "ShutdownRequired";
            this.colShutdownRequired.Name = "colShutdownRequired";
            this.colShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colShutdownRequired.Visible = true;
            this.colShutdownRequired.VisibleIndex = 5;
            this.colShutdownRequired.Width = 59;
            // 
            // colMapID
            // 
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07389UTPDItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(731, 35);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(217, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Remarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(11, 35);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(937, 1103);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling18);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 18;
            this.strRemarksMemoEdit.TabStop = false;
            this.strRemarksMemoEdit.Enter += new System.EventHandler(this.strRemarksMemoEdit_Enter);
            // 
            // OwnerNameButtonEdit
            // 
            this.OwnerNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerName", true));
            this.OwnerNameButtonEdit.EditValue = "";
            this.OwnerNameButtonEdit.Location = new System.Drawing.Point(168, 861);
            this.OwnerNameButtonEdit.MenuManager = this.barManager1;
            this.OwnerNameButtonEdit.Name = "OwnerNameButtonEdit";
            this.OwnerNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to open Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Unknown", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click to Record Unknown against name", "unknown", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.OwnerNameButtonEdit.Properties.MaxLength = 50;
            this.OwnerNameButtonEdit.Size = new System.Drawing.Size(260, 20);
            this.OwnerNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.OwnerNameButtonEdit.TabIndex = 8;
            this.OwnerNameButtonEdit.TabStop = false;
            this.OwnerNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LandOwnerNameButtonEdit_ButtonClick);
            this.OwnerNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LandOwnerNameButtonEdit_Validating);
            // 
            // ItemForSentByStaffID
            // 
            this.ItemForSentByStaffID.Control = this.SentByStaffIDTextEdit;
            this.ItemForSentByStaffID.CustomizationFormText = "Sent By Staff ID:";
            this.ItemForSentByStaffID.Location = new System.Drawing.Point(0, 182);
            this.ItemForSentByStaffID.Name = "ItemForSentByStaffID";
            this.ItemForSentByStaffID.Size = new System.Drawing.Size(336, 24);
            this.ItemForSentByStaffID.Text = "Sent By Staff ID:";
            this.ItemForSentByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPermissionDocumentID
            // 
            this.ItemForPermissionDocumentID.Control = this.PermissionDocumentIDTextEdit;
            this.ItemForPermissionDocumentID.CustomizationFormText = "Permission Document ID:";
            this.ItemForPermissionDocumentID.Location = new System.Drawing.Point(0, 182);
            this.ItemForPermissionDocumentID.Name = "ItemForPermissionDocumentID";
            this.ItemForPermissionDocumentID.Size = new System.Drawing.Size(700, 24);
            this.ItemForPermissionDocumentID.Text = "Permission Document ID:";
            this.ItemForPermissionDocumentID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForContractor
            // 
            this.ItemForContractor.Control = this.ContractorTextEdit;
            this.ItemForContractor.CustomizationFormText = "Contractor:";
            this.ItemForContractor.Location = new System.Drawing.Point(336, 92);
            this.ItemForContractor.Name = "ItemForContractor";
            this.ItemForContractor.Size = new System.Drawing.Size(548, 24);
            this.ItemForContractor.Text = "Contractor:";
            this.ItemForContractor.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForRaisedByID
            // 
            this.ItemForRaisedByID.Control = this.RaisedByIDGridLookUpEdit;
            this.ItemForRaisedByID.CustomizationFormText = "Raised By:";
            this.ItemForRaisedByID.Location = new System.Drawing.Point(336, 92);
            this.ItemForRaisedByID.Name = "ItemForRaisedByID";
            this.ItemForRaisedByID.Size = new System.Drawing.Size(548, 24);
            this.ItemForRaisedByID.Text = "Surveyors Name:";
            this.ItemForRaisedByID.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForSiteAddress
            // 
            this.ItemForSiteAddress.Control = this.SiteAddressMemoExEdit;
            this.ItemForSiteAddress.CustomizationFormText = "Site Address:";
            this.ItemForSiteAddress.Location = new System.Drawing.Point(0, 158);
            this.ItemForSiteAddress.Name = "ItemForSiteAddress";
            this.ItemForSiteAddress.Size = new System.Drawing.Size(884, 24);
            this.ItemForSiteAddress.Text = "Site Address:";
            this.ItemForSiteAddress.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForDateRaised
            // 
            this.ItemForDateRaised.Control = this.DateRaisedDateEdit;
            this.ItemForDateRaised.CustomizationFormText = "Date Raised:";
            this.ItemForDateRaised.Location = new System.Drawing.Point(0, 158);
            this.ItemForDateRaised.Name = "ItemForDateRaised";
            this.ItemForDateRaised.Size = new System.Drawing.Size(884, 24);
            this.ItemForDateRaised.Text = "Date Raised:";
            this.ItemForDateRaised.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForEmergencyAccess
            // 
            this.ItemForEmergencyAccess.Control = this.EmergencyAccessMemoExEdit;
            this.ItemForEmergencyAccess.CustomizationFormText = "Emergency Access:";
            this.ItemForEmergencyAccess.Location = new System.Drawing.Point(0, 158);
            this.ItemForEmergencyAccess.Name = "ItemForEmergencyAccess";
            this.ItemForEmergencyAccess.Size = new System.Drawing.Size(884, 24);
            this.ItemForEmergencyAccess.Text = "Emergency Access:";
            this.ItemForEmergencyAccess.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForNearestTelephonePoint
            // 
            this.ItemForNearestTelephonePoint.Control = this.NearestTelephonePointTextEdit;
            this.ItemForNearestTelephonePoint.CustomizationFormText = "Nearest Telephone Point:";
            this.ItemForNearestTelephonePoint.Location = new System.Drawing.Point(0, 158);
            this.ItemForNearestTelephonePoint.Name = "ItemForNearestTelephonePoint";
            this.ItemForNearestTelephonePoint.Size = new System.Drawing.Size(884, 24);
            this.ItemForNearestTelephonePoint.Text = "Nearest Telephone Point:";
            this.ItemForNearestTelephonePoint.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForGridReference
            // 
            this.ItemForGridReference.Control = this.GridReferenceTextEdit;
            this.ItemForGridReference.CustomizationFormText = "Six Figure Grid Reference:";
            this.ItemForGridReference.Location = new System.Drawing.Point(0, 158);
            this.ItemForGridReference.Name = "ItemForGridReference";
            this.ItemForGridReference.Size = new System.Drawing.Size(884, 24);
            this.ItemForGridReference.Text = "Six Figure Grid Reference:";
            this.ItemForGridReference.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForNearestAandE
            // 
            this.ItemForNearestAandE.Control = this.NearestAandETextEdit;
            this.ItemForNearestAandE.CustomizationFormText = "Nearest A and E:";
            this.ItemForNearestAandE.Location = new System.Drawing.Point(0, 158);
            this.ItemForNearestAandE.Name = "ItemForNearestAandE";
            this.ItemForNearestAandE.Size = new System.Drawing.Size(884, 24);
            this.ItemForNearestAandE.Text = "Nearest A and E:";
            this.ItemForNearestAandE.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEdit4;
            this.layoutControlItem12.CustomizationFormText = "Contact Details:";
            this.layoutControlItem12.Location = new System.Drawing.Point(441, 159);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(441, 24);
            this.layoutControlItem12.Text = "Contact Details:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroup1.Size = new System.Drawing.Size(959, 1149);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.tabbedControlGroup2.SelectedTabPage = this.autoGeneratedGroup0;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(957, 1147);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.autoGeneratedGroup0,
            this.layoutControlGroup3,
            this.layoutControlGroup5,
            this.layoutControlGroup2,
            this.layoutControlGroup10});
            // 
            // autoGeneratedGroup0
            // 
            this.autoGeneratedGroup0.AllowDrawBackground = false;
            this.autoGeneratedGroup0.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.autoGeneratedGroup0.AppearanceGroup.Options.UseBackColor = true;
            this.autoGeneratedGroup0.AppearanceTabPage.PageClient.BackColor = System.Drawing.Color.White;
            this.autoGeneratedGroup0.AppearanceTabPage.PageClient.Options.UseBackColor = true;
            this.autoGeneratedGroup0.CustomizationFormText = "autoGeneratedGroup0";
            this.autoGeneratedGroup0.GroupBordersVisible = false;
            this.autoGeneratedGroup0.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.emptySpaceItem14,
            this.layoutControlItem2,
            this.ItemForReferenceNumber,
            this.layoutControlItem9,
            this.layoutControlItem16,
            this.layoutControlGroup6,
            this.emptySpaceItem12,
            this.layoutControlGroup8,
            this.layoutControlGroup9,
            this.ItemForApplyHerbicide,
            this.ItemForTreePlanting,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.emptySpaceItem15,
            this.layoutControlItem1,
            this.emptySpaceItem13,
            this.emptySpaceItem1,
            this.ItemForWarningLabel,
            this.emptySpaceItem17,
            this.layoutControlGroup4});
            this.autoGeneratedGroup0.Location = new System.Drawing.Point(0, 0);
            this.autoGeneratedGroup0.Name = "autoGeneratedGroup0";
            this.autoGeneratedGroup0.Size = new System.Drawing.Size(941, 1107);
            this.autoGeneratedGroup0.Text = "Data Entry Form";
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(685, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(35, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnSave;
            this.layoutControlItem3.CustomizationFormText = "Save Button:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(103, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(103, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(103, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Save Button:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.pictureEdit1;
            this.layoutControlItem7.CustomizationFormText = "Client Logo:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(198, 76);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(198, 76);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(198, 76);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Client Logo:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 390);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(941, 10);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Linked Jobs";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 400);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 276);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(104, 276);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(941, 276);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Linked Jobs:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ItemForReferenceNumber
            // 
            this.ItemForReferenceNumber.Control = this.ReferenceNumberTextEdit;
            this.ItemForReferenceNumber.CustomizationFormText = "Job Reference Number:";
            this.ItemForReferenceNumber.Location = new System.Drawing.Point(488, 26);
            this.ItemForReferenceNumber.Name = "ItemForReferenceNumber";
            this.ItemForReferenceNumber.Size = new System.Drawing.Size(453, 24);
            this.ItemForReferenceNumber.Text = "Permission Form No.";
            this.ItemForReferenceNumber.TextSize = new System.Drawing.Size(146, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.labelControl1;
            this.layoutControlItem9.CustomizationFormText = "Header 1:";
            this.layoutControlItem9.Location = new System.Drawing.Point(386, 50);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(555, 52);
            this.layoutControlItem9.Text = "Header 1:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.labelControl30;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(386, 26);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(102, 24);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AllowHtmlStringInCaption = true;
            this.layoutControlGroup6.CustomizationFormText = "<b>Approval</b> - Please enter as appropriate:";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem10,
            this.layoutControlItem18,
            this.ItemForInfoLabel1,
            this.ItemForOwnerName,
            this.layoutControlItem11,
            this.ItemForOwnerAddress,
            this.ItemForOwnerPostcode,
            this.emptySpaceItem4,
            this.emptySpaceItem11,
            this.ItemForOwnerTelephone,
            this.ItemForOwnerEmail,
            this.layoutControlItem17,
            this.layoutControlItem19,
            this.ItemForSurveyFormNumber,
            this.ItemForOwnerSalutation});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 709);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup6.Size = new System.Drawing.Size(941, 248);
            this.layoutControlGroup6.Text = "<b>Approval</b> - Please enter as appropriate:";
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 183);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(925, 10);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AllowHtmlStringInCaption = true;
            this.layoutControlItem18.Control = this.OwnerTypeIDGridLookUpEdit;
            this.layoutControlItem18.CustomizationFormText = "Owner Type:";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(414, 24);
            this.layoutControlItem18.Text = "Owner Type:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForInfoLabel1
            // 
            this.ItemForInfoLabel1.Control = this.InfoLabel1;
            this.ItemForInfoLabel1.CustomizationFormText = "Info Label 1:";
            this.ItemForInfoLabel1.Location = new System.Drawing.Point(0, 34);
            this.ItemForInfoLabel1.MaxSize = new System.Drawing.Size(0, 43);
            this.ItemForInfoLabel1.MinSize = new System.Drawing.Size(882, 43);
            this.ItemForInfoLabel1.Name = "ItemForInfoLabel1";
            this.ItemForInfoLabel1.Size = new System.Drawing.Size(925, 43);
            this.ItemForInfoLabel1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForInfoLabel1.Text = "Info Label 1:";
            this.ItemForInfoLabel1.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForInfoLabel1.TextVisible = false;
            // 
            // ItemForOwnerName
            // 
            this.ItemForOwnerName.AllowHide = false;
            this.ItemForOwnerName.Control = this.OwnerNameButtonEdit;
            this.ItemForOwnerName.CustomizationFormText = "Land Owner Name:";
            this.ItemForOwnerName.Location = new System.Drawing.Point(0, 87);
            this.ItemForOwnerName.Name = "ItemForOwnerName";
            this.ItemForOwnerName.Size = new System.Drawing.Size(413, 24);
            this.ItemForOwnerName.Text = "Print Name:";
            this.ItemForOwnerName.TextSize = new System.Drawing.Size(146, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.SignatureFileButtonEdit;
            this.layoutControlItem11.CustomizationFormText = "Signed:";
            this.layoutControlItem11.Location = new System.Drawing.Point(413, 87);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(512, 24);
            this.layoutControlItem11.Text = "Signature:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForOwnerAddress
            // 
            this.ItemForOwnerAddress.Control = this.OwnerAddressMemoExEdit;
            this.ItemForOwnerAddress.CustomizationFormText = "Address:";
            this.ItemForOwnerAddress.Location = new System.Drawing.Point(0, 135);
            this.ItemForOwnerAddress.Name = "ItemForOwnerAddress";
            this.ItemForOwnerAddress.Size = new System.Drawing.Size(413, 24);
            this.ItemForOwnerAddress.Text = "Address:";
            this.ItemForOwnerAddress.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForOwnerPostcode
            // 
            this.ItemForOwnerPostcode.Control = this.OwnerPostcodeTextEdit;
            this.ItemForOwnerPostcode.CustomizationFormText = "Postcode:";
            this.ItemForOwnerPostcode.Location = new System.Drawing.Point(0, 159);
            this.ItemForOwnerPostcode.Name = "ItemForOwnerPostcode";
            this.ItemForOwnerPostcode.Size = new System.Drawing.Size(413, 24);
            this.ItemForOwnerPostcode.Text = "Postcode:";
            this.ItemForOwnerPostcode.TextSize = new System.Drawing.Size(146, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(925, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 77);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(925, 10);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForOwnerTelephone
            // 
            this.ItemForOwnerTelephone.Control = this.OwnerTelephoneTextEdit;
            this.ItemForOwnerTelephone.CustomizationFormText = "Telephone:";
            this.ItemForOwnerTelephone.Location = new System.Drawing.Point(413, 135);
            this.ItemForOwnerTelephone.Name = "ItemForOwnerTelephone";
            this.ItemForOwnerTelephone.Size = new System.Drawing.Size(512, 24);
            this.ItemForOwnerTelephone.Text = "Telephone:";
            this.ItemForOwnerTelephone.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForOwnerEmail
            // 
            this.ItemForOwnerEmail.Control = this.OwnerEmailTextEdit;
            this.ItemForOwnerEmail.CustomizationFormText = "Email:";
            this.ItemForOwnerEmail.Location = new System.Drawing.Point(413, 159);
            this.ItemForOwnerEmail.Name = "ItemForOwnerEmail";
            this.ItemForOwnerEmail.Size = new System.Drawing.Size(512, 24);
            this.ItemForOwnerEmail.Text = "Email:";
            this.ItemForOwnerEmail.TextSize = new System.Drawing.Size(146, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.SignatureDateTextEdit2;
            this.layoutControlItem17.CustomizationFormText = "Signature Date:";
            this.layoutControlItem17.Location = new System.Drawing.Point(413, 111);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(512, 24);
            this.layoutControlItem17.Text = "Signature Date:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(146, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.labelControl31;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 193);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(925, 17);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // ItemForSurveyFormNumber
            // 
            this.ItemForSurveyFormNumber.Control = this.SurveyFormNumberTextEdit;
            this.ItemForSurveyFormNumber.CustomizationFormText = "Survey Form No:";
            this.ItemForSurveyFormNumber.Location = new System.Drawing.Point(414, 0);
            this.ItemForSurveyFormNumber.Name = "ItemForSurveyFormNumber";
            this.ItemForSurveyFormNumber.Size = new System.Drawing.Size(511, 24);
            this.ItemForSurveyFormNumber.Text = "Survey Form No:";
            this.ItemForSurveyFormNumber.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForOwnerSalutation
            // 
            this.ItemForOwnerSalutation.Control = this.OwnerSalutationTextEdit;
            this.ItemForOwnerSalutation.CustomizationFormText = "Salutation:";
            this.ItemForOwnerSalutation.Location = new System.Drawing.Point(0, 111);
            this.ItemForOwnerSalutation.Name = "ItemForOwnerSalutation";
            this.ItemForOwnerSalutation.Size = new System.Drawing.Size(413, 24);
            this.ItemForOwnerSalutation.Text = "Salutation:";
            this.ItemForOwnerSalutation.TextSize = new System.Drawing.Size(146, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 699);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(941, 10);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.AllowHtmlStringInCaption = true;
            this.layoutControlGroup8.CustomizationFormText = "Site Plan";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 112);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup8.Size = new System.Drawing.Size(941, 177);
            this.layoutControlGroup8.Text = "<b>Site Plan</b> - identified location of the required work and access to the sit" +
    "e (this section to be completed by the contractor)";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridControl5;
            this.layoutControlItem6.CustomizationFormText = "Site Plans Grid:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 139);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(218, 139);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(925, 139);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Site Plans Grid:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.AllowHtmlStringInCaption = true;
            this.layoutControlGroup9.CustomizationFormText = "Site Hazards";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 299);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup9.Size = new System.Drawing.Size(941, 91);
            this.layoutControlGroup9.Text = "<b>Landowner/Occupier</b> - please indicate if there are any hazards or other int" +
    "erferences on site which our team needs to be aware such as old mine shafts";
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.SiteHazardsMemoEdit;
            this.layoutControlItem20.CustomizationFormText = "Site Hazards:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(0, 53);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(128, 53);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(925, 53);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "Site Hazards:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // ItemForApplyHerbicide
            // 
            this.ItemForApplyHerbicide.Control = this.ApplyHerbicideCheckEdit;
            this.ItemForApplyHerbicide.CustomizationFormText = "Apply Herbicide:";
            this.ItemForApplyHerbicide.Location = new System.Drawing.Point(0, 676);
            this.ItemForApplyHerbicide.MaxSize = new System.Drawing.Size(104, 23);
            this.ItemForApplyHerbicide.MinSize = new System.Drawing.Size(104, 23);
            this.ItemForApplyHerbicide.Name = "ItemForApplyHerbicide";
            this.ItemForApplyHerbicide.Size = new System.Drawing.Size(104, 23);
            this.ItemForApplyHerbicide.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForApplyHerbicide.Text = "Apply Herbicide:";
            this.ItemForApplyHerbicide.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForApplyHerbicide.TextVisible = false;
            // 
            // ItemForTreePlanting
            // 
            this.ItemForTreePlanting.Control = this.TreePlantingCheckEdit;
            this.ItemForTreePlanting.CustomizationFormText = "Tree Planting:";
            this.ItemForTreePlanting.Location = new System.Drawing.Point(136, 676);
            this.ItemForTreePlanting.MaxSize = new System.Drawing.Size(94, 23);
            this.ItemForTreePlanting.MinSize = new System.Drawing.Size(94, 23);
            this.ItemForTreePlanting.Name = "ItemForTreePlanting";
            this.ItemForTreePlanting.Size = new System.Drawing.Size(94, 23);
            this.ItemForTreePlanting.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTreePlanting.Text = "Tree Planting:";
            this.ItemForTreePlanting.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForTreePlanting.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(230, 676);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(711, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(104, 676);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(32, 0);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(32, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(32, 23);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.CustomizationFormText = "emptySpaceItem15";
            this.emptySpaceItem15.Location = new System.Drawing.Point(198, 26);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(188, 76);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(720, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(221, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(221, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(221, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 102);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(941, 10);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 289);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(941, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWarningLabel
            // 
            this.ItemForWarningLabel.Control = this.labelControl2;
            this.ItemForWarningLabel.CustomizationFormText = "Warning Label:";
            this.ItemForWarningLabel.Location = new System.Drawing.Point(103, 0);
            this.ItemForWarningLabel.MaxSize = new System.Drawing.Size(582, 26);
            this.ItemForWarningLabel.MinSize = new System.Drawing.Size(582, 26);
            this.ItemForWarningLabel.Name = "ItemForWarningLabel";
            this.ItemForWarningLabel.Size = new System.Drawing.Size(582, 26);
            this.ItemForWarningLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWarningLabel.Text = "Warning Label:";
            this.ItemForWarningLabel.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForWarningLabel.TextVisible = false;
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 957);
            this.emptySpaceItem17.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem17.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(941, 10);
            this.emptySpaceItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem8,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.emptySpaceItem16,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 967);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(941, 140);
            this.layoutControlGroup4.Text = "Other";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.EquipmentMemoExEdit;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 42);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(115, 42);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(375, 42);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "Equipment (Calcualted)";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(111, 13);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.MewpCheckEdit;
            this.layoutControlItem8.Location = new System.Drawing.Point(375, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(88, 42);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(88, 42);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(88, 42);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "MEWP";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(30, 13);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.ChipperCheckEdit;
            this.layoutControlItem13.Location = new System.Drawing.Point(463, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(88, 42);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(88, 42);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(88, 42);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "Chipper";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem13.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(37, 13);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.TipperCheckEdit;
            this.layoutControlItem14.Location = new System.Drawing.Point(551, 0);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(83, 42);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(83, 42);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(83, 42);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "Tipper";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem14.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(30, 13);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.TrafficManagementCheckEdit;
            this.layoutControlItem15.Location = new System.Drawing.Point(634, 0);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(106, 42);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(106, 42);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(106, 42);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "Traffic management";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem15.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(96, 13);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.checkEdit4x4;
            this.layoutControlItem21.Location = new System.Drawing.Point(740, 0);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(84, 42);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(84, 42);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(84, 42);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "4 x 4";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem21.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(24, 13);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.HotGloveAccessAvailableCheckEdit;
            this.layoutControlItem22.Location = new System.Drawing.Point(824, 0);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(93, 42);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(93, 42);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(93, 42);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "Hot Glove access";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem22.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(82, 13);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 42);
            this.emptySpaceItem16.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem16.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(917, 10);
            this.emptySpaceItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.AccessAgreedWithLandOwnerCheckEdit;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(155, 41);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(155, 41);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(155, 42);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "Access agreed with landowner";
            this.layoutControlItem23.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(146, 13);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.AccessDetailsOnMapCheckEdit;
            this.layoutControlItem24.Location = new System.Drawing.Point(155, 52);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(118, 41);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(118, 41);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(118, 42);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "Access details on map";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem24.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(105, 13);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.RoadsideAccessOnlyCheckEdit;
            this.layoutControlItem25.Location = new System.Drawing.Point(273, 52);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(118, 41);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(118, 41);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(118, 42);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "Roadside access only";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem25.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(102, 13);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.ArisingsChipRemoveCheckEdit;
            this.layoutControlItem26.Location = new System.Drawing.Point(391, 52);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(122, 41);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(122, 41);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(122, 42);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "Chip && remove arisings";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem26.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(109, 13);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.ArisingsChipOnSiteCheckEdit;
            this.layoutControlItem27.Location = new System.Drawing.Point(513, 52);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(143, 41);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(143, 41);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(143, 42);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "Chip on site in agreed area";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem27.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(129, 13);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.ArisingsStackOnSiteCheckEdit;
            this.layoutControlItem28.Location = new System.Drawing.Point(656, 52);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(110, 42);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(110, 42);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(110, 42);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "Stack neatly on site";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem28.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(94, 13);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.ArisingsOtherTextEdit;
            this.layoutControlItem29.Location = new System.Drawing.Point(766, 52);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 42);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(54, 42);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(151, 42);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "Other (Data Entry)";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem29.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(91, 13);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImageOptions.Image")));
            this.layoutControlGroup3.CustomizationFormText = "Work Maps";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem8,
            this.layoutControlItem4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(941, 1107);
            this.layoutControlGroup3.Text = "Work Maps";
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 310);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(941, 797);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl6;
            this.layoutControlItem4.CustomizationFormText = "Linked Work Maps:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 310);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(240, 310);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(941, 310);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Linked Work Maps:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImageOptions.Image")));
            this.layoutControlGroup5.CustomizationFormText = "Linked Documents";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.emptySpaceItem9});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(941, 1107);
            this.layoutControlGroup5.Text = "Linked Documents";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl4;
            this.layoutControlItem5.CustomizationFormText = "Linked Documents:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 310);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(240, 310);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(941, 310);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Linked Documents:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 310);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(941, 797);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Other Info";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup20,
            this.layoutControlGroup13,
            this.layoutControlGroup14,
            this.emptySpaceItem7,
            this.emptySpaceItem3,
            this.splitterItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(941, 1107);
            this.layoutControlGroup2.Text = "Other Info";
            // 
            // layoutControlGroup20
            // 
            this.layoutControlGroup20.CustomizationFormText = "Saved Permission Document";
            this.layoutControlGroup20.ExpandButtonVisible = true;
            this.layoutControlGroup20.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup20.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSignatureFile,
            this.ItemForSignatureDate,
            this.ItemForPDFFile});
            this.layoutControlGroup20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup20.Name = "layoutControlGroup20";
            this.layoutControlGroup20.Size = new System.Drawing.Size(941, 94);
            this.layoutControlGroup20.Text = "Saved Permission Document";
            // 
            // ItemForSignatureFile
            // 
            this.ItemForSignatureFile.Control = this.SignatureFileHyperLinkEdit;
            this.ItemForSignatureFile.CustomizationFormText = "Saved Signature File:";
            this.ItemForSignatureFile.Location = new System.Drawing.Point(0, 0);
            this.ItemForSignatureFile.Name = "ItemForSignatureFile";
            this.ItemForSignatureFile.Size = new System.Drawing.Size(245, 24);
            this.ItemForSignatureFile.Text = "Saved Signature File:";
            this.ItemForSignatureFile.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForSignatureDate
            // 
            this.ItemForSignatureDate.Control = this.SignatureDateDateEdit;
            this.ItemForSignatureDate.CustomizationFormText = "Signature Date:";
            this.ItemForSignatureDate.Location = new System.Drawing.Point(245, 0);
            this.ItemForSignatureDate.Name = "ItemForSignatureDate";
            this.ItemForSignatureDate.Size = new System.Drawing.Size(672, 24);
            this.ItemForSignatureDate.Text = "Signature Date:";
            this.ItemForSignatureDate.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForPDFFile
            // 
            this.ItemForPDFFile.Control = this.PDFFileHyperLinkEdit;
            this.ItemForPDFFile.CustomizationFormText = "Saved Permission Document File:";
            this.ItemForPDFFile.Location = new System.Drawing.Point(0, 24);
            this.ItemForPDFFile.Name = "ItemForPDFFile";
            this.ItemForPDFFile.Size = new System.Drawing.Size(917, 24);
            this.ItemForPDFFile.Text = "Saved PD File:";
            this.ItemForPDFFile.TextSize = new System.Drawing.Size(146, 13);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Customer";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPostageRequired,
            this.ItemForSentByPost,
            this.ItemForSentByStaffName,
            this.ItemForPermissionEmailed,
            this.ItemForEmailedToCustomerDate});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 104);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(434, 163);
            this.layoutControlGroup13.Text = "Customer";
            // 
            // ItemForPostageRequired
            // 
            this.ItemForPostageRequired.Control = this.PostageRequiredCheckEdit;
            this.ItemForPostageRequired.CustomizationFormText = "Postage Required:";
            this.ItemForPostageRequired.Location = new System.Drawing.Point(0, 23);
            this.ItemForPostageRequired.Name = "ItemForPostageRequired";
            this.ItemForPostageRequired.Size = new System.Drawing.Size(410, 23);
            this.ItemForPostageRequired.Text = "Postage Required:";
            this.ItemForPostageRequired.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForSentByPost
            // 
            this.ItemForSentByPost.Control = this.SentByPostCheckEdit;
            this.ItemForSentByPost.CustomizationFormText = "Sent By Post:";
            this.ItemForSentByPost.Location = new System.Drawing.Point(0, 46);
            this.ItemForSentByPost.Name = "ItemForSentByPost";
            this.ItemForSentByPost.Size = new System.Drawing.Size(410, 23);
            this.ItemForSentByPost.Text = "Sent By Post:";
            this.ItemForSentByPost.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForSentByStaffName
            // 
            this.ItemForSentByStaffName.Control = this.SentByStaffNameTextEdit;
            this.ItemForSentByStaffName.CustomizationFormText = "Sent By Staff:";
            this.ItemForSentByStaffName.Location = new System.Drawing.Point(0, 93);
            this.ItemForSentByStaffName.Name = "ItemForSentByStaffName";
            this.ItemForSentByStaffName.Size = new System.Drawing.Size(410, 24);
            this.ItemForSentByStaffName.Text = "Sent By Staff:";
            this.ItemForSentByStaffName.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForPermissionEmailed
            // 
            this.ItemForPermissionEmailed.Control = this.PermissionEmailedCheckEdit;
            this.ItemForPermissionEmailed.CustomizationFormText = "Permission Emailed:";
            this.ItemForPermissionEmailed.Location = new System.Drawing.Point(0, 0);
            this.ItemForPermissionEmailed.Name = "ItemForPermissionEmailed";
            this.ItemForPermissionEmailed.Size = new System.Drawing.Size(410, 23);
            this.ItemForPermissionEmailed.Text = "Permission Emailed:";
            this.ItemForPermissionEmailed.TextSize = new System.Drawing.Size(146, 13);
            // 
            // ItemForEmailedToCustomerDate
            // 
            this.ItemForEmailedToCustomerDate.Control = this.EmailedToCustomerDateDateEdit;
            this.ItemForEmailedToCustomerDate.CustomizationFormText = "Emailed \\ Sent To Customer:";
            this.ItemForEmailedToCustomerDate.Location = new System.Drawing.Point(0, 69);
            this.ItemForEmailedToCustomerDate.Name = "ItemForEmailedToCustomerDate";
            this.ItemForEmailedToCustomerDate.Size = new System.Drawing.Size(410, 24);
            this.ItemForEmailedToCustomerDate.Text = "Emailed \\ Sent:";
            this.ItemForEmailedToCustomerDate.TextSize = new System.Drawing.Size(146, 13);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "Client";
            this.layoutControlGroup14.ExpandButtonVisible = true;
            this.layoutControlGroup14.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmailedToClientDate});
            this.layoutControlGroup14.Location = new System.Drawing.Point(440, 104);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(501, 163);
            this.layoutControlGroup14.Text = "Client";
            // 
            // ItemForEmailedToClientDate
            // 
            this.ItemForEmailedToClientDate.Control = this.EmailedToClientDateDateEdit;
            this.ItemForEmailedToClientDate.CustomizationFormText = "Emailed To Client Date:";
            this.ItemForEmailedToClientDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForEmailedToClientDate.Name = "ItemForEmailedToClientDate";
            this.ItemForEmailedToClientDate.Size = new System.Drawing.Size(477, 117);
            this.ItemForEmailedToClientDate.Text = "Emailed To Client Date:";
            this.ItemForEmailedToClientDate.TextSize = new System.Drawing.Size(146, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 94);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(941, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 267);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(941, 840);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(434, 104);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 163);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup10.CaptionImageOptions.Image")));
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(941, 1107);
            this.layoutControlGroup10.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(941, 1107);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp07060UTTreeSpeciesLinkedToTreeBindingSource
            // 
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.DataMember = "sp07060_UT_Tree_Species_Linked_To_Tree";
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter
            // 
            this.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter.ClearBeforeFill = true;
            // 
            // sp00226_Staff_List_With_BlankTableAdapter
            // 
            this.sp00226_Staff_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07197_UT_Permission_Statuses_ListTableAdapter
            // 
            this.sp07197_UT_Permission_Statuses_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp07389_UT_PD_ItemTableAdapter
            // 
            this.sp07389_UT_PD_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07392_UT_PD_Linked_WorkTableAdapter
            // 
            this.sp07392_UT_PD_Linked_WorkTableAdapter.ClearBeforeFill = true;
            // 
            // sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter
            // 
            this.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl6;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl4;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07170_UT_Land_Owner_Types_With_BlankTableAdapter
            // 
            this.sp07170_UT_Land_Owner_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07402_UT_PD_Linked_Access_MapsTableAdapter
            // 
            this.sp07402_UT_PD_Linked_Access_MapsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // frm_UT_Permission_Doc_Edit_UKPN
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(976, 683);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Permission_Doc_Edit_UKPN";
            this.Text = "Edit Permission Document - UKPN Layout";
            this.Activated += new System.EventHandler(this.frm_UT_Permission_Doc_Edit_UKPN_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Permission_Doc_Edit_UKPN_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Permission_Doc_Edit_UKPN_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsOtherTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07389UTPDItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsStackOnSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsChipOnSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsChipRemoveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoadsideAccessOnlyCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessDetailsOnMapCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessAgreedWithLandOwnerCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotGloveAccessAvailableCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4x4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficManagementCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipperCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChipperCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MewpCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07402UTPDLinkedAccessMapsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditMapPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreePlantingCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyHerbicideCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyFormNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteHazardsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07170UTLandOwnerTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateTextEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionDocumentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmergencyAccessMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerAddressMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureFileButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerEmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerSalutationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestAandETextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridReferenceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestTelephonePointTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByPostCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostageRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionEmailedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToClientDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToClientDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToCustomerDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToCustomerDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDFFileHyperLinkEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureFileHyperLinkEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaisedByIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07392UTPDLinkedWorkBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07197UTPermissionStatusesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionDocumentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRaisedByID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmergencyAccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestTelephonePoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGridReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestAandE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoGeneratedGroup0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInfoLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyFormNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerSalutation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApplyHerbicide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreePlanting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDFFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostageRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByPost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionEmailed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailedToCustomerDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailedToClientDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07060UTTreeSpeciesLinkedToTreeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit PermissionDocumentIDTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPermissionDocumentID;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DevExpress.XtraEditors.ButtonEdit OwnerNameButtonEdit;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource sp07060UTTreeSpeciesLinkedToTreeBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DataSet_UT_EditTableAdapters.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter;
        private DevExpress.XtraEditors.DateEdit DateRaisedDateEdit;
        private DevExpress.XtraEditors.GridLookUpEdit RaisedByIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp00226StaffListWithBlankBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter sp00226_Staff_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.HyperLinkEdit SignatureFileHyperLinkEdit;
        private DevExpress.XtraEditors.HyperLinkEdit PDFFileHyperLinkEdit;
        private DevExpress.XtraEditors.DateEdit EmailedToClientDateDateEdit;
        private DevExpress.XtraEditors.DateEdit EmailedToCustomerDateDateEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPermissionID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionStatusID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditStatus;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colLandOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private System.Windows.Forms.BindingSource sp07197UTPermissionStatusesListBindingSource;
        private DataSet_UT_EditTableAdapters.sp07197_UT_Permission_Statuses_ListTableAdapter sp07197_UT_Permission_Statuses_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraEditors.DateEdit SignatureDateDateEdit;
        private DevExpress.XtraEditors.TextEdit NearestTelephonePointTextEdit;
        private DevExpress.XtraEditors.TextEdit NearestAandETextEdit;
        private DevExpress.XtraEditors.TextEdit GridReferenceTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraEditors.CheckEdit PermissionEmailedCheckEdit;
        private DevExpress.XtraEditors.CheckEdit PostageRequiredCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SentByPostCheckEdit;
        private DevExpress.XtraEditors.TextEdit SentByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSentByStaffID;
        private DevExpress.XtraEditors.TextEdit SentByStaffNameTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit ContractorTextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit ReferenceNumberTextEdit;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.BindingSource sp07389UTPDItemBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07389_UT_PD_ItemTableAdapter sp07389_UT_PD_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit OwnerSalutationTextEdit;
        private DevExpress.XtraEditors.TextEdit OwnerPostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit OwnerEmailTextEdit;
        private DevExpress.XtraEditors.TextEdit OwnerTelephoneTextEdit;
        private DevExpress.XtraEditors.ButtonEdit SignatureFileButtonEdit;
        private DevExpress.XtraEditors.GridLookUpEdit OwnerTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.MemoExEdit SiteAddressMemoExEdit;
        private DevExpress.XtraEditors.MemoExEdit OwnerAddressMemoExEdit;
        private DevExpress.XtraEditors.MemoExEdit EmergencyAccessMemoExEdit;
        private DevExpress.XtraEditors.LabelControl InfoLabel1;
        private System.Windows.Forms.BindingSource sp07392UTPDLinkedWorkBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07392_UT_PD_Linked_WorkTableAdapter sp07392_UT_PD_Linked_WorkTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditWork;
        private DevExpress.XtraGrid.Columns.GridColumn colESQCRCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colG55Category;
        private DevExpress.XtraGrid.Columns.GridColumn colAchievableClearance;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colLandOwnerRestrictedCut;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup autoGeneratedGroup0;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReferenceNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRaisedByID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerSalutation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateRaised;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerTelephone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNearestTelephonePoint;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmergencyAccess;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGridReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNearestAandE;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInfoLabel1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostageRequired;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSentByPost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSentByStaffName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPermissionEmailed;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailedToCustomerDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailedToClientDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup20;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSignatureFile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSignatureDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDFFile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAddWorkToPermission;
        private DevExpress.XtraBars.BarButtonItem bbiCreatePermissionDocument;
        private DevExpress.XtraBars.BarButtonItem bbiCreateMap;
        private DevExpress.XtraBars.BarButtonItem bbiEmailToCustomer;
        private DevExpress.XtraBars.BarButtonItem bbiPostedToCustomer;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMeters;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedMapID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colMapOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeCreated;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.BindingSource sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource;
        private DataSet_UT_Mapping dataSet_UT_Mapping;
        private DataSet_UT_MappingTableAdapters.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp07170UTLandOwnerTypesWithBlankBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07170_UT_Land_Owner_Types_With_BlankTableAdapter sp07170_UT_Land_Owner_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colRefusalReason;
        private DevExpress.XtraGrid.Columns.GridColumn colRefusalReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRefusalRemarks;
        private DevExpress.XtraEditors.TextEdit SignatureDateTextEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraEditors.MemoEdit SiteHazardsMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.TextEdit SurveyFormNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyFormNumber;
        private DevExpress.XtraEditors.CheckEdit TreePlantingCheckEdit;
        private DevExpress.XtraEditors.CheckEdit ApplyHerbicideCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForApplyHerbicide;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreePlanting;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.BindingSource sp07402UTPDLinkedAccessMapsBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditMapPath;
        private DataSet_UT_WorkOrderTableAdapters.sp07402_UT_PD_Linked_Access_MapsTableAdapter sp07402_UT_PD_Linked_Access_MapsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colMapPath;
        private DevExpress.XtraGrid.Columns.GridColumn colMapType;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownRequired;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarningLabel;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraEditors.MemoExEdit EquipmentMemoExEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraEditors.CheckEdit MewpCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.CheckEdit ChipperCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.CheckEdit TipperCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.CheckEdit TrafficManagementCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.CheckEdit checkEdit4x4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.CheckEdit HotGloveAccessAvailableCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.CheckEdit AccessAgreedWithLandOwnerCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.CheckEdit AccessDetailsOnMapCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraEditors.CheckEdit RoadsideAccessOnlyCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.CheckEdit ArisingsChipRemoveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.CheckEdit ArisingsChipOnSiteCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.CheckEdit ArisingsStackOnSiteCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraEditors.TextEdit ArisingsOtherTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_UT_Surveyed_Pole_Set_Deferred
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Surveyed_Pole_Set_Deferred));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.DeferralRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DeferralReasonIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07357UTDeferralReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.gridView25 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RevisitDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.DeferredUnitDescriptiorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07046UTInspectionCyclesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DeferredUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeferredUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeferredUnitDescriptiorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRevisitDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeferralReasonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeferralRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter();
            this.sp07357_UT_Deferral_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07357_UT_Deferral_Reasons_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DeferralRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferralReasonIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07357UTDeferralReasonsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07046UTInspectionCyclesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredUnitDescriptiorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferralReasonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferralRemarks)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(602, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 198);
            this.barDockControlBottom.Size = new System.Drawing.Size(602, 26);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 172);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(602, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 172);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "ID";
            this.gridColumn97.FieldName = "ID";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Records Selected For Update: 0";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 30;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.DeferralRemarksMemoEdit);
            this.layoutControl1.Controls.Add(this.DeferralReasonIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.RevisitDateDateEdit);
            this.layoutControl1.Controls.Add(this.DeferredUnitDescriptiorIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.DeferredUnitsSpinEdit);
            this.layoutControl1.Controls.Add(this.btnCancel);
            this.layoutControl1.Controls.Add(this.btnOK);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 26);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(738, 195, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(602, 172);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // DeferralRemarksMemoEdit
            // 
            this.DeferralRemarksMemoEdit.Location = new System.Drawing.Point(128, 55);
            this.DeferralRemarksMemoEdit.MenuManager = this.barManager1;
            this.DeferralRemarksMemoEdit.Name = "DeferralRemarksMemoEdit";
            this.DeferralRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DeferralRemarksMemoEdit, true);
            this.DeferralRemarksMemoEdit.Size = new System.Drawing.Size(467, 84);
            this.scSpellChecker.SetSpellCheckerOptions(this.DeferralRemarksMemoEdit, optionsSpelling2);
            this.DeferralRemarksMemoEdit.StyleController = this.layoutControl1;
            this.DeferralRemarksMemoEdit.TabIndex = 38;
            this.DeferralRemarksMemoEdit.UseOptimizedRendering = true;
            // 
            // DeferralReasonIDGridLookUpEdit
            // 
            this.DeferralReasonIDGridLookUpEdit.Location = new System.Drawing.Point(424, 31);
            this.DeferralReasonIDGridLookUpEdit.MenuManager = this.barManager1;
            this.DeferralReasonIDGridLookUpEdit.Name = "DeferralReasonIDGridLookUpEdit";
            this.DeferralReasonIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeferralReasonIDGridLookUpEdit.Properties.DataSource = this.sp07357UTDeferralReasonsWithBlankBindingSource;
            this.DeferralReasonIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.DeferralReasonIDGridLookUpEdit.Properties.NullText = "";
            this.DeferralReasonIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.DeferralReasonIDGridLookUpEdit.Properties.View = this.gridView25;
            this.DeferralReasonIDGridLookUpEdit.Size = new System.Drawing.Size(171, 20);
            this.DeferralReasonIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.DeferralReasonIDGridLookUpEdit.TabIndex = 37;
            this.DeferralReasonIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DeferralReasonIDGridLookUpEdit_Validating);
            // 
            // sp07357UTDeferralReasonsWithBlankBindingSource
            // 
            this.sp07357UTDeferralReasonsWithBlankBindingSource.DataMember = "sp07357_UT_Deferral_Reasons_With_Blank";
            this.sp07357UTDeferralReasonsWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView25
            // 
            this.gridView25.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99});
            this.gridView25.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn97;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView25.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView25.Name = "gridView25";
            this.gridView25.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView25.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView25.OptionsLayout.StoreAppearance = true;
            this.gridView25.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView25.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView25.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView25.OptionsView.ColumnAutoWidth = false;
            this.gridView25.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView25.OptionsView.ShowGroupPanel = false;
            this.gridView25.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView25.OptionsView.ShowIndicator = false;
            this.gridView25.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn99, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Deferral Reason";
            this.gridColumn98.FieldName = "Description";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Visible = true;
            this.gridColumn98.VisibleIndex = 0;
            this.gridColumn98.Width = 201;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Order";
            this.gridColumn99.FieldName = "RecordOrder";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            // 
            // RevisitDateDateEdit
            // 
            this.RevisitDateDateEdit.EditValue = null;
            this.RevisitDateDateEdit.Location = new System.Drawing.Point(128, 31);
            this.RevisitDateDateEdit.MenuManager = this.barManager1;
            this.RevisitDateDateEdit.Name = "RevisitDateDateEdit";
            this.RevisitDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.RevisitDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RevisitDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RevisitDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RevisitDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.RevisitDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.RevisitDateDateEdit.Size = new System.Drawing.Size(171, 20);
            this.RevisitDateDateEdit.StyleController = this.layoutControl1;
            this.RevisitDateDateEdit.TabIndex = 34;
            this.RevisitDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.RevisitDateDateEdit_Validating);
            // 
            // DeferredUnitDescriptiorIDGridLookUpEdit
            // 
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Location = new System.Drawing.Point(424, 7);
            this.DeferredUnitDescriptiorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Name = "DeferredUnitDescriptiorIDGridLookUpEdit";
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.DataSource = this.sp07046UTInspectionCyclesWithBlankBindingSource;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.NullText = "";
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.View = this.gridView15;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Size = new System.Drawing.Size(171, 20);
            this.DeferredUnitDescriptiorIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.TabIndex = 29;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DeferredUnitDescriptiorIDGridLookUpEdit_Validating);
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Validated += new System.EventHandler(this.DeferredUnitDescriptiorIDGridLookUpEdit_Validated);
            // 
            // sp07046UTInspectionCyclesWithBlankBindingSource
            // 
            this.sp07046UTInspectionCyclesWithBlankBindingSource.DataMember = "sp07046_UT_Inspection_Cycles_With_Blank";
            this.sp07046UTInspectionCyclesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colID,
            this.colRecordOrder1});
            this.gridView15.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colID;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView15.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView15.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView15.OptionsLayout.StoreAppearance = true;
            this.gridView15.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView15.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView15.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView15.OptionsView.ColumnAutoWidth = false;
            this.gridView15.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            this.gridView15.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView15.OptionsView.ShowIndicator = false;
            this.gridView15.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Unit Descriptor";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 162;
            // 
            // colRecordOrder1
            // 
            this.colRecordOrder1.Caption = "Order";
            this.colRecordOrder1.FieldName = "RecordOrder";
            this.colRecordOrder1.Name = "colRecordOrder1";
            this.colRecordOrder1.OptionsColumn.AllowEdit = false;
            this.colRecordOrder1.OptionsColumn.AllowFocus = false;
            this.colRecordOrder1.OptionsColumn.ReadOnly = true;
            // 
            // DeferredUnitsSpinEdit
            // 
            this.DeferredUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DeferredUnitsSpinEdit.Location = new System.Drawing.Point(128, 7);
            this.DeferredUnitsSpinEdit.MenuManager = this.barManager1;
            this.DeferredUnitsSpinEdit.Name = "DeferredUnitsSpinEdit";
            this.DeferredUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeferredUnitsSpinEdit.Properties.Mask.EditMask = "N00";
            this.DeferredUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DeferredUnitsSpinEdit.Properties.MaxLength = 999;
            this.DeferredUnitsSpinEdit.Size = new System.Drawing.Size(171, 20);
            this.DeferredUnitsSpinEdit.StyleController = this.layoutControl1;
            this.DeferredUnitsSpinEdit.TabIndex = 6;
            this.DeferredUnitsSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DeferredUnitsSpinEdit_Validating);
            this.DeferredUnitsSpinEdit.Validated += new System.EventHandler(this.DeferredUnitsSpinEdit_Validated);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(513, 143);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(82, 22);
            this.btnCancel.StyleController = this.layoutControl1;
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(424, 143);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(85, 22);
            this.btnOK.StyleController = this.layoutControl1;
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "Ok";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.ItemForDeferredUnits,
            this.ItemForDeferredUnitDescriptiorID,
            this.ItemForRevisitDate,
            this.ItemForDeferralReasonID,
            this.ItemForDeferralRemarks});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(602, 172);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AllowHide = false;
            this.layoutControlItem1.Control = this.btnOK;
            this.layoutControlItem1.CustomizationFormText = "OK Button";
            this.layoutControlItem1.Location = new System.Drawing.Point(417, 136);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(89, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "OK Button";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 136);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(417, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHide = false;
            this.layoutControlItem2.Control = this.btnCancel;
            this.layoutControlItem2.CustomizationFormText = "Cancel Button";
            this.layoutControlItem2.Location = new System.Drawing.Point(506, 136);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Cancel Button";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // ItemForDeferredUnits
            // 
            this.ItemForDeferredUnits.Control = this.DeferredUnitsSpinEdit;
            this.ItemForDeferredUnits.CustomizationFormText = "Deferred For:";
            this.ItemForDeferredUnits.Location = new System.Drawing.Point(0, 0);
            this.ItemForDeferredUnits.Name = "ItemForDeferredUnits";
            this.ItemForDeferredUnits.Size = new System.Drawing.Size(296, 24);
            this.ItemForDeferredUnits.Text = "Deferred For:";
            this.ItemForDeferredUnits.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForDeferredUnitDescriptiorID
            // 
            this.ItemForDeferredUnitDescriptiorID.Control = this.DeferredUnitDescriptiorIDGridLookUpEdit;
            this.ItemForDeferredUnitDescriptiorID.CustomizationFormText = "Deferred For Descriptor:";
            this.ItemForDeferredUnitDescriptiorID.Location = new System.Drawing.Point(296, 0);
            this.ItemForDeferredUnitDescriptiorID.Name = "ItemForDeferredUnitDescriptiorID";
            this.ItemForDeferredUnitDescriptiorID.Size = new System.Drawing.Size(296, 24);
            this.ItemForDeferredUnitDescriptiorID.Text = "Deferred For Descriptor:";
            this.ItemForDeferredUnitDescriptiorID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForRevisitDate
            // 
            this.ItemForRevisitDate.Control = this.RevisitDateDateEdit;
            this.ItemForRevisitDate.CustomizationFormText = "Revisit Date:";
            this.ItemForRevisitDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForRevisitDate.Name = "ItemForRevisitDate";
            this.ItemForRevisitDate.Size = new System.Drawing.Size(296, 24);
            this.ItemForRevisitDate.Text = "Revisit Date:";
            this.ItemForRevisitDate.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForDeferralReasonID
            // 
            this.ItemForDeferralReasonID.Control = this.DeferralReasonIDGridLookUpEdit;
            this.ItemForDeferralReasonID.CustomizationFormText = "Deferral Reason:";
            this.ItemForDeferralReasonID.Location = new System.Drawing.Point(296, 24);
            this.ItemForDeferralReasonID.Name = "ItemForDeferralReasonID";
            this.ItemForDeferralReasonID.Size = new System.Drawing.Size(296, 24);
            this.ItemForDeferralReasonID.Text = "Deferral Reason:";
            this.ItemForDeferralReasonID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForDeferralRemarks
            // 
            this.ItemForDeferralRemarks.Control = this.DeferralRemarksMemoEdit;
            this.ItemForDeferralRemarks.CustomizationFormText = "Deferral Remarks:";
            this.ItemForDeferralRemarks.Location = new System.Drawing.Point(0, 48);
            this.ItemForDeferralRemarks.Name = "ItemForDeferralRemarks";
            this.ItemForDeferralRemarks.Size = new System.Drawing.Size(592, 88);
            this.ItemForDeferralRemarks.Text = "Deferral Remarks:";
            this.ItemForDeferralRemarks.TextSize = new System.Drawing.Size(118, 13);
            // 
            // sp07046_UT_Inspection_Cycles_With_BlankTableAdapter
            // 
            this.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07357_UT_Deferral_Reasons_With_BlankTableAdapter
            // 
            this.sp07357_UT_Deferral_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Surveyed_Pole_Set_Deferred
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(602, 224);
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_UT_Surveyed_Pole_Set_Deferred";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Set Surveyed Poles As Deferred";
            this.Load += new System.EventHandler(this.frm_UT_Surveyed_Pole_Set_Deferred_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DeferralRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferralReasonIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07357UTDeferralReasonsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07046UTInspectionCyclesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredUnitDescriptiorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferralReasonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferralRemarks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SpinEdit DeferredUnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferredUnits;
        private DevExpress.XtraEditors.GridLookUpEdit DeferredUnitDescriptiorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferredUnitDescriptiorID;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private System.Windows.Forms.BindingSource sp07046UTInspectionCyclesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter sp07046_UT_Inspection_Cycles_With_BlankTableAdapter;
        private DevExpress.XtraEditors.DateEdit RevisitDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRevisitDate;
        private DevExpress.XtraEditors.GridLookUpEdit DeferralReasonIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferralReasonID;
        private System.Windows.Forms.BindingSource sp07357UTDeferralReasonsWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07357_UT_Deferral_Reasons_With_BlankTableAdapter sp07357_UT_Deferral_Reasons_With_BlankTableAdapter;
        private DevExpress.XtraEditors.MemoEdit DeferralRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferralRemarks;
    }
}

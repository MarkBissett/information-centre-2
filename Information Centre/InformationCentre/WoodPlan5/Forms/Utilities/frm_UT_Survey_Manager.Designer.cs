namespace WoodPlan5
{
    partial class frm_UT_Survey_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Survey_Manager));
            this.popupContainerControlClients = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07001UTClientFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnClientFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07086UTSurveyManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSurveyStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMapStartTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastMapRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastMapX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastMapY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapStartType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedPoleCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colSurveyor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastMapRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkspaceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkspaceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExchequerNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalIncome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteApprovedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactiveCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactiveRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTargetSurveyCompletionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTargetPermissionCompletionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTargetWorkCompletionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.popupContainerControlRegions = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07087UTSurveyStatusFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnRegionFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07102UTSurveyManagerLinkedSurveyedPolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateMask = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsSpanClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colInfestationRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHotGloveRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinesmanPossible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementResolved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFiveYearClearanceAchieved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeWithin3Meters = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExchequerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptiorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsBaseClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditRevisitCount = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShortDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpanInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistanceUnder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpanResilient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIvyOnPole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinearCutLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredEstimatedCuttingHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistanceAbove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp07001_UT_Client_FilterTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07001_UT_Client_FilterTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.popupContainerEdit1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.sp07086_UT_Survey_ManagerTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07086_UT_Survey_ManagerTableAdapter();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.sp07087_UT_Survey_Status_FilterTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07087_UT_Survey_Status_FilterTableAdapter();
            this.sp07102_UT_Survey_Manager_Linked_Surveyed_PolesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07102_UT_Survey_Manager_Linked_Surveyed_PolesTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlClients)).BeginInit();
            this.popupContainerControlClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07001UTClientFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07086UTSurveyManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRegions)).BeginInit();
            this.popupContainerControlRegions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07087UTSurveyStatusFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07102UTSurveyManagerLinkedSurveyedPolesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateMask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditRevisitCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1041, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 542);
            this.barDockControlBottom.Size = new System.Drawing.Size(1041, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 508);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1041, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 508);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.popupContainerEdit1,
            this.popupContainerEdit2,
            this.barEditItem1,
            this.popupContainerDateRange});
            this.barManager1.MaxItemId = 35;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemDateEdit1,
            this.repositoryItemPopupContainerEditDateRange});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // popupContainerControlClients
            // 
            this.popupContainerControlClients.Controls.Add(this.gridControl2);
            this.popupContainerControlClients.Controls.Add(this.btnClientFilterOK);
            this.popupContainerControlClients.Location = new System.Drawing.Point(12, 123);
            this.popupContainerControlClients.Name = "popupContainerControlClients";
            this.popupContainerControlClients.Size = new System.Drawing.Size(119, 152);
            this.popupContainerControlClients.TabIndex = 1;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp07001UTClientFilterBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 1);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(113, 121);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07001UTClientFilterBindingSource
            // 
            this.sp07001UTClientFilterBindingSource.DataMember = "sp07001_UT_Client_Filter";
            this.sp07001UTClientFilterBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colClientName,
            this.colClientCode,
            this.colClientTypeID,
            this.colClientTypeDescription,
            this.colRemarks});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 209;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 1;
            this.colClientTypeDescription.Width = 111;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            this.colRemarks.Width = 102;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // btnClientFilterOK
            // 
            this.btnClientFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClientFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnClientFilterOK.Name = "btnClientFilterOK";
            this.btnClientFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnClientFilterOK.TabIndex = 2;
            this.btnClientFilterOK.Text = "OK";
            this.btnClientFilterOK.Click += new System.EventHandler(this.btnClientFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07086UTSurveyManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Reload Data", "reload")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditDateTime});
            this.gridControl1.Size = new System.Drawing.Size(1041, 314);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07086UTSurveyManagerBindingSource
            // 
            this.sp07086UTSurveyManagerBindingSource.DataMember = "sp07086_UT_Survey_Manager";
            this.sp07086UTSurveyManagerBindingSource.DataSource = this.dataSet_UT;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            this.imageCollection1.InsertGalleryImage("attach_16x16.png", "images/mail/attach_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/attach_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "attach_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyID,
            this.colClientID1,
            this.colSurveyDate,
            this.colSurveyStatusID,
            this.colSurveyorID,
            this.colReactive,
            this.colMapStartTypeID,
            this.colLastMapRecordID,
            this.colLastMapX,
            this.colLastMapY,
            this.colRemarks1,
            this.colGUID,
            this.colClientName1,
            this.colClientCode1,
            this.colStatusDescription,
            this.colMapStartType,
            this.colLinkedPoleCount,
            this.colSurveyor,
            this.colLastMapRecordDescription,
            this.colWorkspaceID,
            this.colWorkspaceName,
            this.colClientPONumber,
            this.colExchequerNumber1,
            this.colTotalCost,
            this.colTotalIncome,
            this.colRevisitCount1,
            this.colShutdownCount,
            this.colTrafficManagementCount,
            this.colQuoteApprovedDate,
            this.colQuoteReceivedDate,
            this.colReportedByName,
            this.colReportedByAddress,
            this.colReportedByPostcode,
            this.colReportedByTelephone,
            this.colReportedByEmail,
            this.colIncidentAddress,
            this.colIncidentPostcode,
            this.colReactiveCategory,
            this.colReactiveRemarks,
            this.colContractYear,
            this.colCreatedByStaffID1,
            this.colTargetSurveyCompletionDate,
            this.colTargetPermissionCompletionDate,
            this.colTargetWorkCompletionDate,
            this.colContractType});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurveyDate, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurveyor, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSurveyID
            // 
            this.colSurveyID.Caption = "Survey ID";
            this.colSurveyID.FieldName = "SurveyID";
            this.colSurveyID.Name = "colSurveyID";
            this.colSurveyID.OptionsColumn.AllowEdit = false;
            this.colSurveyID.OptionsColumn.AllowFocus = false;
            this.colSurveyID.OptionsColumn.ReadOnly = true;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyDate
            // 
            this.colSurveyDate.Caption = "Survey Date";
            this.colSurveyDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSurveyDate.FieldName = "SurveyDate";
            this.colSurveyDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyDate.Name = "colSurveyDate";
            this.colSurveyDate.OptionsColumn.AllowEdit = false;
            this.colSurveyDate.OptionsColumn.AllowFocus = false;
            this.colSurveyDate.OptionsColumn.ReadOnly = true;
            this.colSurveyDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyDate.Visible = true;
            this.colSurveyDate.VisibleIndex = 0;
            this.colSurveyDate.Width = 94;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "d";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colSurveyStatusID
            // 
            this.colSurveyStatusID.Caption = "Survey Status ID";
            this.colSurveyStatusID.FieldName = "SurveyStatusID";
            this.colSurveyStatusID.Name = "colSurveyStatusID";
            this.colSurveyStatusID.OptionsColumn.AllowEdit = false;
            this.colSurveyStatusID.OptionsColumn.AllowFocus = false;
            this.colSurveyStatusID.OptionsColumn.ReadOnly = true;
            this.colSurveyStatusID.Width = 103;
            // 
            // colSurveyorID
            // 
            this.colSurveyorID.Caption = "Surveyor ID";
            this.colSurveyorID.FieldName = "SurveyorID";
            this.colSurveyorID.Name = "colSurveyorID";
            this.colSurveyorID.OptionsColumn.AllowEdit = false;
            this.colSurveyorID.OptionsColumn.AllowFocus = false;
            this.colSurveyorID.OptionsColumn.ReadOnly = true;
            this.colSurveyorID.Width = 79;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 3;
            this.colReactive.Width = 67;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colMapStartTypeID
            // 
            this.colMapStartTypeID.Caption = "Map Start Location ID";
            this.colMapStartTypeID.FieldName = "MapStartTypeID";
            this.colMapStartTypeID.Name = "colMapStartTypeID";
            this.colMapStartTypeID.OptionsColumn.AllowEdit = false;
            this.colMapStartTypeID.OptionsColumn.AllowFocus = false;
            this.colMapStartTypeID.OptionsColumn.ReadOnly = true;
            this.colMapStartTypeID.Width = 125;
            // 
            // colLastMapRecordID
            // 
            this.colLastMapRecordID.Caption = "Last Map Record ID";
            this.colLastMapRecordID.FieldName = "LastMapRecordID";
            this.colLastMapRecordID.Name = "colLastMapRecordID";
            this.colLastMapRecordID.OptionsColumn.AllowEdit = false;
            this.colLastMapRecordID.OptionsColumn.AllowFocus = false;
            this.colLastMapRecordID.OptionsColumn.ReadOnly = true;
            this.colLastMapRecordID.Width = 115;
            // 
            // colLastMapX
            // 
            this.colLastMapX.Caption = "Last Map X";
            this.colLastMapX.FieldName = "LastMapX";
            this.colLastMapX.Name = "colLastMapX";
            this.colLastMapX.OptionsColumn.AllowEdit = false;
            this.colLastMapX.OptionsColumn.AllowFocus = false;
            this.colLastMapX.OptionsColumn.ReadOnly = true;
            // 
            // colLastMapY
            // 
            this.colLastMapY.Caption = "Last Map Y";
            this.colLastMapY.FieldName = "LastMapY";
            this.colLastMapY.Name = "colLastMapY";
            this.colLastMapY.OptionsColumn.AllowEdit = false;
            this.colLastMapY.OptionsColumn.AllowFocus = false;
            this.colLastMapY.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 30;
            this.colRemarks1.Width = 289;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 4;
            this.colClientName1.Width = 165;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            this.colClientCode1.Width = 98;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Survey Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 2;
            this.colStatusDescription.Width = 168;
            // 
            // colMapStartType
            // 
            this.colMapStartType.Caption = "Map Start";
            this.colMapStartType.FieldName = "MapStartType";
            this.colMapStartType.Name = "colMapStartType";
            this.colMapStartType.OptionsColumn.AllowEdit = false;
            this.colMapStartType.OptionsColumn.AllowFocus = false;
            this.colMapStartType.OptionsColumn.ReadOnly = true;
            this.colMapStartType.Visible = true;
            this.colMapStartType.VisibleIndex = 28;
            this.colMapStartType.Width = 86;
            // 
            // colLinkedPoleCount
            // 
            this.colLinkedPoleCount.Caption = "Linked Poles";
            this.colLinkedPoleCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedPoleCount.FieldName = "LinkedPoleCount";
            this.colLinkedPoleCount.Name = "colLinkedPoleCount";
            this.colLinkedPoleCount.OptionsColumn.ReadOnly = true;
            this.colLinkedPoleCount.Visible = true;
            this.colLinkedPoleCount.VisibleIndex = 23;
            this.colLinkedPoleCount.Width = 79;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colSurveyor
            // 
            this.colSurveyor.Caption = "Surveyor";
            this.colSurveyor.FieldName = "Surveyor";
            this.colSurveyor.Name = "colSurveyor";
            this.colSurveyor.OptionsColumn.AllowEdit = false;
            this.colSurveyor.OptionsColumn.AllowFocus = false;
            this.colSurveyor.OptionsColumn.ReadOnly = true;
            this.colSurveyor.Visible = true;
            this.colSurveyor.VisibleIndex = 1;
            this.colSurveyor.Width = 159;
            // 
            // colLastMapRecordDescription
            // 
            this.colLastMapRecordDescription.Caption = "Map Start Record";
            this.colLastMapRecordDescription.FieldName = "LastMapRecordDescription";
            this.colLastMapRecordDescription.Name = "colLastMapRecordDescription";
            this.colLastMapRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLastMapRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLastMapRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLastMapRecordDescription.Visible = true;
            this.colLastMapRecordDescription.VisibleIndex = 29;
            this.colLastMapRecordDescription.Width = 318;
            // 
            // colWorkspaceID
            // 
            this.colWorkspaceID.Caption = "Mapping Workspace ID";
            this.colWorkspaceID.FieldName = "WorkspaceID";
            this.colWorkspaceID.Name = "colWorkspaceID";
            this.colWorkspaceID.OptionsColumn.AllowEdit = false;
            this.colWorkspaceID.OptionsColumn.AllowFocus = false;
            this.colWorkspaceID.OptionsColumn.ReadOnly = true;
            this.colWorkspaceID.Width = 131;
            // 
            // colWorkspaceName
            // 
            this.colWorkspaceName.Caption = "Mapping Workspace";
            this.colWorkspaceName.FieldName = "WorkspaceName";
            this.colWorkspaceName.Name = "colWorkspaceName";
            this.colWorkspaceName.OptionsColumn.AllowEdit = false;
            this.colWorkspaceName.OptionsColumn.AllowFocus = false;
            this.colWorkspaceName.OptionsColumn.ReadOnly = true;
            this.colWorkspaceName.Width = 283;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO Number";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 6;
            this.colClientPONumber.Width = 105;
            // 
            // colExchequerNumber1
            // 
            this.colExchequerNumber1.Caption = "Exchequer Number";
            this.colExchequerNumber1.FieldName = "ExchequerNumber";
            this.colExchequerNumber1.Name = "colExchequerNumber1";
            this.colExchequerNumber1.OptionsColumn.AllowEdit = false;
            this.colExchequerNumber1.OptionsColumn.AllowFocus = false;
            this.colExchequerNumber1.OptionsColumn.ReadOnly = true;
            this.colExchequerNumber1.Visible = true;
            this.colExchequerNumber1.VisibleIndex = 7;
            this.colExchequerNumber1.Width = 112;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 21;
            this.colTotalCost.Width = 88;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colTotalIncome
            // 
            this.colTotalIncome.Caption = "Total Income";
            this.colTotalIncome.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalIncome.FieldName = "TotalIncome";
            this.colTotalIncome.Name = "colTotalIncome";
            this.colTotalIncome.OptionsColumn.AllowEdit = false;
            this.colTotalIncome.OptionsColumn.AllowFocus = false;
            this.colTotalIncome.OptionsColumn.ReadOnly = true;
            this.colTotalIncome.Visible = true;
            this.colTotalIncome.VisibleIndex = 22;
            this.colTotalIncome.Width = 86;
            // 
            // colRevisitCount1
            // 
            this.colRevisitCount1.Caption = "Revisit Poles";
            this.colRevisitCount1.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colRevisitCount1.FieldName = "RevisitCount";
            this.colRevisitCount1.Name = "colRevisitCount1";
            this.colRevisitCount1.OptionsColumn.ReadOnly = true;
            this.colRevisitCount1.Visible = true;
            this.colRevisitCount1.VisibleIndex = 24;
            this.colRevisitCount1.Width = 81;
            // 
            // colShutdownCount
            // 
            this.colShutdownCount.Caption = "Shutdowns";
            this.colShutdownCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colShutdownCount.FieldName = "ShutdownCount";
            this.colShutdownCount.Name = "colShutdownCount";
            this.colShutdownCount.OptionsColumn.ReadOnly = true;
            this.colShutdownCount.Visible = true;
            this.colShutdownCount.VisibleIndex = 25;
            // 
            // colTrafficManagementCount
            // 
            this.colTrafficManagementCount.Caption = "Traffic Management";
            this.colTrafficManagementCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colTrafficManagementCount.FieldName = "TrafficManagementCount";
            this.colTrafficManagementCount.Name = "colTrafficManagementCount";
            this.colTrafficManagementCount.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementCount.Visible = true;
            this.colTrafficManagementCount.VisibleIndex = 26;
            this.colTrafficManagementCount.Width = 117;
            // 
            // colQuoteApprovedDate
            // 
            this.colQuoteApprovedDate.Caption = "Quote Approved";
            this.colQuoteApprovedDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colQuoteApprovedDate.FieldName = "QuoteApprovedDate";
            this.colQuoteApprovedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colQuoteApprovedDate.Name = "colQuoteApprovedDate";
            this.colQuoteApprovedDate.OptionsColumn.AllowEdit = false;
            this.colQuoteApprovedDate.OptionsColumn.AllowFocus = false;
            this.colQuoteApprovedDate.OptionsColumn.ReadOnly = true;
            this.colQuoteApprovedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colQuoteApprovedDate.Visible = true;
            this.colQuoteApprovedDate.VisibleIndex = 8;
            this.colQuoteApprovedDate.Width = 101;
            // 
            // colQuoteReceivedDate
            // 
            this.colQuoteReceivedDate.Caption = "Quote Received";
            this.colQuoteReceivedDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colQuoteReceivedDate.FieldName = "QuoteReceivedDate";
            this.colQuoteReceivedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colQuoteReceivedDate.Name = "colQuoteReceivedDate";
            this.colQuoteReceivedDate.OptionsColumn.AllowEdit = false;
            this.colQuoteReceivedDate.OptionsColumn.AllowFocus = false;
            this.colQuoteReceivedDate.OptionsColumn.ReadOnly = true;
            this.colQuoteReceivedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colQuoteReceivedDate.Visible = true;
            this.colQuoteReceivedDate.VisibleIndex = 9;
            this.colQuoteReceivedDate.Width = 98;
            // 
            // colReportedByName
            // 
            this.colReportedByName.Caption = "Reported By Name";
            this.colReportedByName.FieldName = "ReportedByName";
            this.colReportedByName.Name = "colReportedByName";
            this.colReportedByName.OptionsColumn.AllowEdit = false;
            this.colReportedByName.OptionsColumn.AllowFocus = false;
            this.colReportedByName.OptionsColumn.ReadOnly = true;
            this.colReportedByName.Visible = true;
            this.colReportedByName.VisibleIndex = 13;
            this.colReportedByName.Width = 111;
            // 
            // colReportedByAddress
            // 
            this.colReportedByAddress.Caption = "Reported By Address";
            this.colReportedByAddress.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colReportedByAddress.FieldName = "ReportedByAddress";
            this.colReportedByAddress.Name = "colReportedByAddress";
            this.colReportedByAddress.OptionsColumn.ReadOnly = true;
            this.colReportedByAddress.Visible = true;
            this.colReportedByAddress.VisibleIndex = 14;
            this.colReportedByAddress.Width = 123;
            // 
            // colReportedByPostcode
            // 
            this.colReportedByPostcode.Caption = "Reported By Postcode";
            this.colReportedByPostcode.FieldName = "ReportedByPostcode";
            this.colReportedByPostcode.Name = "colReportedByPostcode";
            this.colReportedByPostcode.OptionsColumn.AllowEdit = false;
            this.colReportedByPostcode.OptionsColumn.AllowFocus = false;
            this.colReportedByPostcode.OptionsColumn.ReadOnly = true;
            this.colReportedByPostcode.Visible = true;
            this.colReportedByPostcode.VisibleIndex = 15;
            this.colReportedByPostcode.Width = 128;
            // 
            // colReportedByTelephone
            // 
            this.colReportedByTelephone.Caption = "Reported By Tel.";
            this.colReportedByTelephone.FieldName = "ReportedByTelephone";
            this.colReportedByTelephone.Name = "colReportedByTelephone";
            this.colReportedByTelephone.OptionsColumn.AllowEdit = false;
            this.colReportedByTelephone.OptionsColumn.AllowFocus = false;
            this.colReportedByTelephone.OptionsColumn.ReadOnly = true;
            this.colReportedByTelephone.Visible = true;
            this.colReportedByTelephone.VisibleIndex = 16;
            this.colReportedByTelephone.Width = 102;
            // 
            // colReportedByEmail
            // 
            this.colReportedByEmail.Caption = "Reported By Email";
            this.colReportedByEmail.FieldName = "ReportedByEmail";
            this.colReportedByEmail.Name = "colReportedByEmail";
            this.colReportedByEmail.OptionsColumn.AllowEdit = false;
            this.colReportedByEmail.OptionsColumn.AllowFocus = false;
            this.colReportedByEmail.OptionsColumn.ReadOnly = true;
            this.colReportedByEmail.Visible = true;
            this.colReportedByEmail.VisibleIndex = 17;
            this.colReportedByEmail.Width = 108;
            // 
            // colIncidentAddress
            // 
            this.colIncidentAddress.Caption = "Incident Address";
            this.colIncidentAddress.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colIncidentAddress.FieldName = "IncidentAddress";
            this.colIncidentAddress.Name = "colIncidentAddress";
            this.colIncidentAddress.OptionsColumn.ReadOnly = true;
            this.colIncidentAddress.Visible = true;
            this.colIncidentAddress.VisibleIndex = 18;
            this.colIncidentAddress.Width = 102;
            // 
            // colIncidentPostcode
            // 
            this.colIncidentPostcode.Caption = "Incident Postcode";
            this.colIncidentPostcode.FieldName = "IncidentPostcode";
            this.colIncidentPostcode.Name = "colIncidentPostcode";
            this.colIncidentPostcode.OptionsColumn.AllowEdit = false;
            this.colIncidentPostcode.OptionsColumn.AllowFocus = false;
            this.colIncidentPostcode.OptionsColumn.ReadOnly = true;
            this.colIncidentPostcode.Visible = true;
            this.colIncidentPostcode.VisibleIndex = 19;
            this.colIncidentPostcode.Width = 107;
            // 
            // colReactiveCategory
            // 
            this.colReactiveCategory.Caption = "Reactive Category";
            this.colReactiveCategory.FieldName = "ReactiveCategory";
            this.colReactiveCategory.Name = "colReactiveCategory";
            this.colReactiveCategory.OptionsColumn.AllowEdit = false;
            this.colReactiveCategory.OptionsColumn.AllowFocus = false;
            this.colReactiveCategory.OptionsColumn.ReadOnly = true;
            this.colReactiveCategory.Visible = true;
            this.colReactiveCategory.VisibleIndex = 5;
            this.colReactiveCategory.Width = 111;
            // 
            // colReactiveRemarks
            // 
            this.colReactiveRemarks.Caption = "Reactive Remarks";
            this.colReactiveRemarks.FieldName = "ReactiveRemarks";
            this.colReactiveRemarks.Name = "colReactiveRemarks";
            this.colReactiveRemarks.OptionsColumn.AllowEdit = false;
            this.colReactiveRemarks.OptionsColumn.AllowFocus = false;
            this.colReactiveRemarks.OptionsColumn.ReadOnly = true;
            this.colReactiveRemarks.Visible = true;
            this.colReactiveRemarks.VisibleIndex = 20;
            this.colReactiveRemarks.Width = 107;
            // 
            // colContractYear
            // 
            this.colContractYear.Caption = "Contract Year";
            this.colContractYear.FieldName = "ContractYear";
            this.colContractYear.Name = "colContractYear";
            this.colContractYear.OptionsColumn.AllowEdit = false;
            this.colContractYear.OptionsColumn.AllowFocus = false;
            this.colContractYear.OptionsColumn.ReadOnly = true;
            this.colContractYear.Visible = true;
            this.colContractYear.VisibleIndex = 27;
            this.colContractYear.Width = 88;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 114;
            // 
            // colTargetSurveyCompletionDate
            // 
            this.colTargetSurveyCompletionDate.Caption = "Target Survey Completion";
            this.colTargetSurveyCompletionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colTargetSurveyCompletionDate.FieldName = "TargetSurveyCompletionDate";
            this.colTargetSurveyCompletionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTargetSurveyCompletionDate.Name = "colTargetSurveyCompletionDate";
            this.colTargetSurveyCompletionDate.OptionsColumn.AllowEdit = false;
            this.colTargetSurveyCompletionDate.OptionsColumn.AllowFocus = false;
            this.colTargetSurveyCompletionDate.OptionsColumn.ReadOnly = true;
            this.colTargetSurveyCompletionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colTargetSurveyCompletionDate.Visible = true;
            this.colTargetSurveyCompletionDate.VisibleIndex = 10;
            this.colTargetSurveyCompletionDate.Width = 144;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colTargetPermissionCompletionDate
            // 
            this.colTargetPermissionCompletionDate.Caption = "Target Permission Completion";
            this.colTargetPermissionCompletionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colTargetPermissionCompletionDate.FieldName = "TargetPermissionCompletionDate";
            this.colTargetPermissionCompletionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTargetPermissionCompletionDate.Name = "colTargetPermissionCompletionDate";
            this.colTargetPermissionCompletionDate.OptionsColumn.AllowEdit = false;
            this.colTargetPermissionCompletionDate.OptionsColumn.AllowFocus = false;
            this.colTargetPermissionCompletionDate.OptionsColumn.ReadOnly = true;
            this.colTargetPermissionCompletionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colTargetPermissionCompletionDate.Visible = true;
            this.colTargetPermissionCompletionDate.VisibleIndex = 11;
            this.colTargetPermissionCompletionDate.Width = 160;
            // 
            // colTargetWorkCompletionDate
            // 
            this.colTargetWorkCompletionDate.Caption = "Target Work Completion";
            this.colTargetWorkCompletionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colTargetWorkCompletionDate.FieldName = "TargetWorkCompletionDate";
            this.colTargetWorkCompletionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTargetWorkCompletionDate.Name = "colTargetWorkCompletionDate";
            this.colTargetWorkCompletionDate.OptionsColumn.AllowEdit = false;
            this.colTargetWorkCompletionDate.OptionsColumn.AllowFocus = false;
            this.colTargetWorkCompletionDate.OptionsColumn.ReadOnly = true;
            this.colTargetWorkCompletionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colTargetWorkCompletionDate.Visible = true;
            this.colTargetWorkCompletionDate.VisibleIndex = 12;
            this.colTargetWorkCompletionDate.Width = 135;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 4;
            this.colContractType.Width = 116;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 34);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl2.Panel2.Text = "Linked Documents";
            this.splitContainerControl2.Size = new System.Drawing.Size(1041, 507);
            this.splitContainerControl2.SplitterPosition = 187;
            this.splitContainerControl2.TabIndex = 5;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlRegions);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlClients);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1041, 314);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(301, 124);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(180, 109);
            this.popupContainerControlDateRange.TabIndex = 3;
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 83);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnDateRangeFilterOK.TabIndex = 3;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(174, 76);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Date Range";
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditToDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditFromDate.TabIndex = 0;
            // 
            // popupContainerControlRegions
            // 
            this.popupContainerControlRegions.Controls.Add(this.gridControl4);
            this.popupContainerControlRegions.Controls.Add(this.btnRegionFilterOK);
            this.popupContainerControlRegions.Location = new System.Drawing.Point(137, 123);
            this.popupContainerControlRegions.Name = "popupContainerControlRegions";
            this.popupContainerControlRegions.Size = new System.Drawing.Size(120, 152);
            this.popupContainerControlRegions.TabIndex = 2;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp07087UTSurveyStatusFilterBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 1);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(114, 121);
            this.gridControl4.TabIndex = 3;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07087UTSurveyStatusFilterBindingSource
            // 
            this.sp07087UTSurveyStatusFilterBindingSource.DataMember = "sp07087_UT_Survey_Status_Filter";
            this.sp07087UTSurveyStatusFilterBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStatusID,
            this.colStatusDescription1,
            this.colStatusOrder});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStatusOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colStatusDescription1
            // 
            this.colStatusDescription1.Caption = "Status";
            this.colStatusDescription1.FieldName = "StatusDescription";
            this.colStatusDescription1.Name = "colStatusDescription1";
            this.colStatusDescription1.OptionsColumn.AllowEdit = false;
            this.colStatusDescription1.OptionsColumn.AllowFocus = false;
            this.colStatusDescription1.OptionsColumn.ReadOnly = true;
            this.colStatusDescription1.Visible = true;
            this.colStatusDescription1.VisibleIndex = 0;
            this.colStatusDescription1.Width = 222;
            // 
            // colStatusOrder
            // 
            this.colStatusOrder.Caption = "Order";
            this.colStatusOrder.FieldName = "StatusOrder";
            this.colStatusOrder.Name = "colStatusOrder";
            this.colStatusOrder.OptionsColumn.AllowEdit = false;
            this.colStatusOrder.OptionsColumn.AllowFocus = false;
            this.colStatusOrder.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // btnRegionFilterOK
            // 
            this.btnRegionFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRegionFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnRegionFilterOK.Name = "btnRegionFilterOK";
            this.btnRegionFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnRegionFilterOK.TabIndex = 2;
            this.btnRegionFilterOK.Text = "OK";
            this.btnRegionFilterOK.Click += new System.EventHandler(this.btnRegionFilterOK_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(1037, 183);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage1});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl5);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1032, 154);
            this.xtraTabPage2.Text = "Surveyed Poles";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07102UTSurveyManagerLinkedSurveyedPolesBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Reload Data", "reload")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateMask,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemHyperLinkEditRevisitCount,
            this.repositoryItemTextEditShortDate});
            this.gridControl5.Size = new System.Drawing.Size(1032, 154);
            this.gridControl5.TabIndex = 2;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07102UTSurveyManagerLinkedSurveyedPolesBindingSource
            // 
            this.sp07102UTSurveyManagerLinkedSurveyedPolesBindingSource.DataMember = "sp07102_UT_Survey_Manager_Linked_Surveyed_Poles";
            this.sp07102UTSurveyManagerLinkedSurveyedPolesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyedPoleID,
            this.colSurveyID1,
            this.colPoleID,
            this.colFeederContractID,
            this.colSurveyedDate,
            this.colIsSpanClear,
            this.colInfestationRate,
            this.colIsShutdownRequired,
            this.colHotGloveRequired,
            this.colLinesmanPossible,
            this.colClearanceDistance,
            this.colTrafficManagementRequired,
            this.colTrafficManagementResolved,
            this.colFiveYearClearanceAchieved,
            this.colTreeWithin3Meters,
            this.colTreeClimbable,
            this.colRemarks2,
            this.colGUID1,
            this.colClientName2,
            this.colSurveyDate1,
            this.colSurveyDescription,
            this.colSurveyor1,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colExchequerNumber,
            this.colContractValue,
            this.colPoleNumber,
            this.colPoleLastInspectionDate,
            this.colPoleNextInspectionDate,
            this.colIsTransformer,
            this.colCircuitNumber,
            this.colCircuitName,
            this.colNoWorkRequired,
            this.colSurveyStatus,
            this.colSurveyStatusID1,
            this.colG55CategoryID,
            this.colG55CategoryDescription,
            this.colDeferred,
            this.colDeferredUnitDescriptior,
            this.colDeferredUnitDescriptiorID,
            this.colDeferredUnits,
            this.colIsBaseClimbable,
            this.colRevisitCount,
            this.colRevisitDate,
            this.colMapID,
            this.colSpanInvoiced,
            this.colInvoiceNumber,
            this.colDateInvoiced,
            this.colDeferralReason,
            this.colDeferralRemarks,
            this.colEstimatedHours,
            this.colActualHours,
            this.colAccessMapPath,
            this.colTrafficMapPath,
            this.colClearanceDistanceUnder,
            this.colSpanResilient,
            this.colIvyOnPole,
            this.colCreatedByStaffID,
            this.colHeldUpType,
            this.colLinearCutLength,
            this.colShutdownHours,
            this.colDeferredEstimatedCuttingHours,
            this.colClearanceDistanceAbove});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurveyDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView5_CustomRowCellEdit);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView5_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView5_ShowingEditor);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colSurveyedPoleID
            // 
            this.colSurveyedPoleID.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID.Name = "colSurveyedPoleID";
            this.colSurveyedPoleID.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID.Width = 104;
            // 
            // colSurveyID1
            // 
            this.colSurveyID1.Caption = "Survey ID";
            this.colSurveyID1.FieldName = "SurveyID";
            this.colSurveyID1.Name = "colSurveyID1";
            this.colSurveyID1.OptionsColumn.AllowEdit = false;
            this.colSurveyID1.OptionsColumn.AllowFocus = false;
            this.colSurveyID1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colFeederContractID
            // 
            this.colFeederContractID.Caption = "Feeder Contract ID";
            this.colFeederContractID.FieldName = "FeederContractID";
            this.colFeederContractID.Name = "colFeederContractID";
            this.colFeederContractID.OptionsColumn.AllowEdit = false;
            this.colFeederContractID.OptionsColumn.AllowFocus = false;
            this.colFeederContractID.OptionsColumn.ReadOnly = true;
            this.colFeederContractID.Width = 110;
            // 
            // colSurveyedDate
            // 
            this.colSurveyedDate.Caption = "Date Surveyed";
            this.colSurveyedDate.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colSurveyedDate.FieldName = "SurveyedDate";
            this.colSurveyedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyedDate.Name = "colSurveyedDate";
            this.colSurveyedDate.OptionsColumn.AllowEdit = false;
            this.colSurveyedDate.OptionsColumn.AllowFocus = false;
            this.colSurveyedDate.OptionsColumn.ReadOnly = true;
            this.colSurveyedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyedDate.Visible = true;
            this.colSurveyedDate.VisibleIndex = 6;
            this.colSurveyedDate.Width = 93;
            // 
            // repositoryItemTextEditDateMask
            // 
            this.repositoryItemTextEditDateMask.AutoHeight = false;
            this.repositoryItemTextEditDateMask.Mask.EditMask = "g";
            this.repositoryItemTextEditDateMask.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateMask.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateMask.Name = "repositoryItemTextEditDateMask";
            // 
            // colIsSpanClear
            // 
            this.colIsSpanClear.Caption = "Span Clear";
            this.colIsSpanClear.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsSpanClear.FieldName = "IsSpanClear";
            this.colIsSpanClear.Name = "colIsSpanClear";
            this.colIsSpanClear.OptionsColumn.AllowEdit = false;
            this.colIsSpanClear.OptionsColumn.AllowFocus = false;
            this.colIsSpanClear.OptionsColumn.ReadOnly = true;
            this.colIsSpanClear.Visible = true;
            this.colIsSpanClear.VisibleIndex = 8;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colInfestationRate
            // 
            this.colInfestationRate.Caption = "Infestation Rate";
            this.colInfestationRate.FieldName = "InfestationRate";
            this.colInfestationRate.Name = "colInfestationRate";
            this.colInfestationRate.OptionsColumn.AllowEdit = false;
            this.colInfestationRate.OptionsColumn.AllowFocus = false;
            this.colInfestationRate.OptionsColumn.ReadOnly = true;
            this.colInfestationRate.Visible = true;
            this.colInfestationRate.VisibleIndex = 9;
            this.colInfestationRate.Width = 100;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 11;
            this.colIsShutdownRequired.Width = 115;
            // 
            // colHotGloveRequired
            // 
            this.colHotGloveRequired.Caption = "Hot Glove";
            this.colHotGloveRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colHotGloveRequired.FieldName = "HotGloveRequired";
            this.colHotGloveRequired.Name = "colHotGloveRequired";
            this.colHotGloveRequired.OptionsColumn.AllowEdit = false;
            this.colHotGloveRequired.OptionsColumn.AllowFocus = false;
            this.colHotGloveRequired.OptionsColumn.ReadOnly = true;
            this.colHotGloveRequired.Visible = true;
            this.colHotGloveRequired.VisibleIndex = 14;
            // 
            // colLinesmanPossible
            // 
            this.colLinesmanPossible.Caption = "Linesman";
            this.colLinesmanPossible.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colLinesmanPossible.FieldName = "LinesmanPossible";
            this.colLinesmanPossible.Name = "colLinesmanPossible";
            this.colLinesmanPossible.OptionsColumn.AllowEdit = false;
            this.colLinesmanPossible.OptionsColumn.AllowFocus = false;
            this.colLinesmanPossible.OptionsColumn.ReadOnly = true;
            this.colLinesmanPossible.Visible = true;
            this.colLinesmanPossible.VisibleIndex = 15;
            // 
            // colClearanceDistance
            // 
            this.colClearanceDistance.Caption = "Clearance Distance (Side)";
            this.colClearanceDistance.FieldName = "ClearanceDistance";
            this.colClearanceDistance.Name = "colClearanceDistance";
            this.colClearanceDistance.OptionsColumn.AllowEdit = false;
            this.colClearanceDistance.OptionsColumn.AllowFocus = false;
            this.colClearanceDistance.OptionsColumn.ReadOnly = true;
            this.colClearanceDistance.Visible = true;
            this.colClearanceDistance.VisibleIndex = 19;
            this.colClearanceDistance.Width = 144;
            // 
            // colTrafficManagementRequired
            // 
            this.colTrafficManagementRequired.Caption = "Traffic Management Req.";
            this.colTrafficManagementRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTrafficManagementRequired.FieldName = "TrafficManagementRequired";
            this.colTrafficManagementRequired.Name = "colTrafficManagementRequired";
            this.colTrafficManagementRequired.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementRequired.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementRequired.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementRequired.Visible = true;
            this.colTrafficManagementRequired.VisibleIndex = 22;
            this.colTrafficManagementRequired.Width = 143;
            // 
            // colTrafficManagementResolved
            // 
            this.colTrafficManagementResolved.Caption = "Traffic Management Resolved";
            this.colTrafficManagementResolved.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTrafficManagementResolved.FieldName = "TrafficManagementResolved";
            this.colTrafficManagementResolved.Name = "colTrafficManagementResolved";
            this.colTrafficManagementResolved.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementResolved.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementResolved.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementResolved.Visible = true;
            this.colTrafficManagementResolved.VisibleIndex = 23;
            this.colTrafficManagementResolved.Width = 164;
            // 
            // colFiveYearClearanceAchieved
            // 
            this.colFiveYearClearanceAchieved.Caption = "5 Year Clearance Achieved";
            this.colFiveYearClearanceAchieved.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colFiveYearClearanceAchieved.FieldName = "FiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.Name = "colFiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowEdit = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowFocus = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.ReadOnly = true;
            this.colFiveYearClearanceAchieved.Visible = true;
            this.colFiveYearClearanceAchieved.VisibleIndex = 24;
            this.colFiveYearClearanceAchieved.Width = 150;
            // 
            // colTreeWithin3Meters
            // 
            this.colTreeWithin3Meters.Caption = "Tree Within 3M";
            this.colTreeWithin3Meters.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeWithin3Meters.FieldName = "TreeWithin3Meters";
            this.colTreeWithin3Meters.Name = "colTreeWithin3Meters";
            this.colTreeWithin3Meters.OptionsColumn.AllowEdit = false;
            this.colTreeWithin3Meters.OptionsColumn.AllowFocus = false;
            this.colTreeWithin3Meters.OptionsColumn.ReadOnly = true;
            this.colTreeWithin3Meters.Visible = true;
            this.colTreeWithin3Meters.VisibleIndex = 25;
            this.colTreeWithin3Meters.Width = 93;
            // 
            // colTreeClimbable
            // 
            this.colTreeClimbable.Caption = "Tree Climbable";
            this.colTreeClimbable.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeClimbable.FieldName = "TreeClimbable";
            this.colTreeClimbable.Name = "colTreeClimbable";
            this.colTreeClimbable.OptionsColumn.AllowEdit = false;
            this.colTreeClimbable.OptionsColumn.AllowFocus = false;
            this.colTreeClimbable.OptionsColumn.ReadOnly = true;
            this.colTreeClimbable.Visible = true;
            this.colTreeClimbable.VisibleIndex = 26;
            this.colTreeClimbable.Width = 91;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 30;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 224;
            // 
            // colSurveyDate1
            // 
            this.colSurveyDate1.Caption = "Survey - Date";
            this.colSurveyDate1.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colSurveyDate1.FieldName = "SurveyDate";
            this.colSurveyDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyDate1.Name = "colSurveyDate1";
            this.colSurveyDate1.OptionsColumn.AllowEdit = false;
            this.colSurveyDate1.OptionsColumn.AllowFocus = false;
            this.colSurveyDate1.OptionsColumn.ReadOnly = true;
            this.colSurveyDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyDate1.Width = 88;
            // 
            // colSurveyDescription
            // 
            this.colSurveyDescription.Caption = "Survey - Description";
            this.colSurveyDescription.FieldName = "SurveyDescription";
            this.colSurveyDescription.Name = "colSurveyDescription";
            this.colSurveyDescription.OptionsColumn.AllowEdit = false;
            this.colSurveyDescription.OptionsColumn.AllowFocus = false;
            this.colSurveyDescription.OptionsColumn.ReadOnly = true;
            this.colSurveyDescription.Visible = true;
            this.colSurveyDescription.VisibleIndex = 16;
            this.colSurveyDescription.Width = 268;
            // 
            // colSurveyor1
            // 
            this.colSurveyor1.Caption = "Surveyor";
            this.colSurveyor1.FieldName = "Surveyor";
            this.colSurveyor1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colSurveyor1.Name = "colSurveyor1";
            this.colSurveyor1.OptionsColumn.AllowEdit = false;
            this.colSurveyor1.OptionsColumn.AllowFocus = false;
            this.colSurveyor1.OptionsColumn.ReadOnly = true;
            this.colSurveyor1.Visible = true;
            this.colSurveyor1.VisibleIndex = 46;
            this.colSurveyor1.Width = 132;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start Date";
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractStartDate.Width = 116;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End Date";
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractEndDate.Width = 110;
            // 
            // colExchequerNumber
            // 
            this.colExchequerNumber.Caption = "Exchequer Number";
            this.colExchequerNumber.FieldName = "ExchequerNumber";
            this.colExchequerNumber.Name = "colExchequerNumber";
            this.colExchequerNumber.OptionsColumn.AllowEdit = false;
            this.colExchequerNumber.OptionsColumn.AllowFocus = false;
            this.colExchequerNumber.OptionsColumn.ReadOnly = true;
            this.colExchequerNumber.Width = 112;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Span Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 4;
            this.colContractValue.Width = 74;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 1;
            this.colPoleNumber.Width = 105;
            // 
            // colPoleLastInspectionDate
            // 
            this.colPoleLastInspectionDate.Caption = "Pole Last Inspection";
            this.colPoleLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colPoleLastInspectionDate.FieldName = "PoleLastInspectionDate";
            this.colPoleLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleLastInspectionDate.Name = "colPoleLastInspectionDate";
            this.colPoleLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleLastInspectionDate.Visible = true;
            this.colPoleLastInspectionDate.VisibleIndex = 2;
            this.colPoleLastInspectionDate.Width = 117;
            // 
            // colPoleNextInspectionDate
            // 
            this.colPoleNextInspectionDate.Caption = "Pole Next Inspection";
            this.colPoleNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colPoleNextInspectionDate.FieldName = "PoleNextInspectionDate";
            this.colPoleNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleNextInspectionDate.Name = "colPoleNextInspectionDate";
            this.colPoleNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleNextInspectionDate.Visible = true;
            this.colPoleNextInspectionDate.VisibleIndex = 3;
            this.colPoleNextInspectionDate.Width = 120;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Tranformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 31;
            this.colIsTransformer.Width = 87;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 0;
            this.colCircuitNumber.Width = 111;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Width = 81;
            // 
            // colNoWorkRequired
            // 
            this.colNoWorkRequired.Caption = "No Work Required";
            this.colNoWorkRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNoWorkRequired.FieldName = "NoWorkRequired";
            this.colNoWorkRequired.Name = "colNoWorkRequired";
            this.colNoWorkRequired.OptionsColumn.AllowEdit = false;
            this.colNoWorkRequired.OptionsColumn.AllowFocus = false;
            this.colNoWorkRequired.OptionsColumn.ReadOnly = true;
            this.colNoWorkRequired.Visible = true;
            this.colNoWorkRequired.VisibleIndex = 7;
            this.colNoWorkRequired.Width = 108;
            // 
            // colSurveyStatus
            // 
            this.colSurveyStatus.Caption = "Survey Status";
            this.colSurveyStatus.FieldName = "SurveyStatus";
            this.colSurveyStatus.Name = "colSurveyStatus";
            this.colSurveyStatus.OptionsColumn.AllowEdit = false;
            this.colSurveyStatus.OptionsColumn.AllowFocus = false;
            this.colSurveyStatus.OptionsColumn.ReadOnly = true;
            this.colSurveyStatus.Visible = true;
            this.colSurveyStatus.VisibleIndex = 5;
            this.colSurveyStatus.Width = 96;
            // 
            // colSurveyStatusID1
            // 
            this.colSurveyStatusID1.Caption = "Survey Status ID";
            this.colSurveyStatusID1.FieldName = "SurveyStatusID";
            this.colSurveyStatusID1.Name = "colSurveyStatusID1";
            this.colSurveyStatusID1.OptionsColumn.AllowEdit = false;
            this.colSurveyStatusID1.OptionsColumn.AllowFocus = false;
            this.colSurveyStatusID1.OptionsColumn.ReadOnly = true;
            this.colSurveyStatusID1.Width = 103;
            // 
            // colG55CategoryID
            // 
            this.colG55CategoryID.Caption = "G55 Category ID";
            this.colG55CategoryID.FieldName = "G55CategoryID";
            this.colG55CategoryID.Name = "colG55CategoryID";
            this.colG55CategoryID.OptionsColumn.AllowEdit = false;
            this.colG55CategoryID.OptionsColumn.AllowFocus = false;
            this.colG55CategoryID.OptionsColumn.ReadOnly = true;
            this.colG55CategoryID.Width = 102;
            // 
            // colG55CategoryDescription
            // 
            this.colG55CategoryDescription.Caption = "G55 Category";
            this.colG55CategoryDescription.FieldName = "G55CategoryDescription";
            this.colG55CategoryDescription.Name = "colG55CategoryDescription";
            this.colG55CategoryDescription.OptionsColumn.AllowEdit = false;
            this.colG55CategoryDescription.OptionsColumn.AllowFocus = false;
            this.colG55CategoryDescription.OptionsColumn.ReadOnly = true;
            this.colG55CategoryDescription.Visible = true;
            this.colG55CategoryDescription.VisibleIndex = 16;
            this.colG55CategoryDescription.Width = 88;
            // 
            // colDeferred
            // 
            this.colDeferred.Caption = "Deferred";
            this.colDeferred.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDeferred.FieldName = "Deferred";
            this.colDeferred.Name = "colDeferred";
            this.colDeferred.OptionsColumn.AllowEdit = false;
            this.colDeferred.OptionsColumn.AllowFocus = false;
            this.colDeferred.OptionsColumn.ReadOnly = true;
            this.colDeferred.Visible = true;
            this.colDeferred.VisibleIndex = 32;
            this.colDeferred.Width = 64;
            // 
            // colDeferredUnitDescriptior
            // 
            this.colDeferredUnitDescriptior.Caption = "Deferred Unit Descriptor";
            this.colDeferredUnitDescriptior.FieldName = "DeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.Name = "colDeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptior.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptior.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptior.Visible = true;
            this.colDeferredUnitDescriptior.VisibleIndex = 34;
            this.colDeferredUnitDescriptior.Width = 138;
            // 
            // colDeferredUnitDescriptiorID
            // 
            this.colDeferredUnitDescriptiorID.Caption = "Deferred Unit Descriptor ID";
            this.colDeferredUnitDescriptiorID.FieldName = "DeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.Name = "colDeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptiorID.Width = 152;
            // 
            // colDeferredUnits
            // 
            this.colDeferredUnits.Caption = "Deferred Units";
            this.colDeferredUnits.FieldName = "DeferredUnits";
            this.colDeferredUnits.Name = "colDeferredUnits";
            this.colDeferredUnits.OptionsColumn.AllowEdit = false;
            this.colDeferredUnits.OptionsColumn.AllowFocus = false;
            this.colDeferredUnits.OptionsColumn.ReadOnly = true;
            this.colDeferredUnits.Visible = true;
            this.colDeferredUnits.VisibleIndex = 33;
            this.colDeferredUnits.Width = 91;
            // 
            // colIsBaseClimbable
            // 
            this.colIsBaseClimbable.Caption = "Is Base Climbable";
            this.colIsBaseClimbable.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsBaseClimbable.FieldName = "IsBaseClimbable";
            this.colIsBaseClimbable.Name = "colIsBaseClimbable";
            this.colIsBaseClimbable.OptionsColumn.AllowEdit = false;
            this.colIsBaseClimbable.OptionsColumn.AllowFocus = false;
            this.colIsBaseClimbable.OptionsColumn.ReadOnly = true;
            this.colIsBaseClimbable.Visible = true;
            this.colIsBaseClimbable.VisibleIndex = 27;
            this.colIsBaseClimbable.Width = 104;
            // 
            // colRevisitCount
            // 
            this.colRevisitCount.Caption = "Revisit Action Count";
            this.colRevisitCount.ColumnEdit = this.repositoryItemHyperLinkEditRevisitCount;
            this.colRevisitCount.FieldName = "RevisitCount";
            this.colRevisitCount.Name = "colRevisitCount";
            this.colRevisitCount.OptionsColumn.ReadOnly = true;
            this.colRevisitCount.Visible = true;
            this.colRevisitCount.VisibleIndex = 39;
            this.colRevisitCount.Width = 118;
            // 
            // repositoryItemHyperLinkEditRevisitCount
            // 
            this.repositoryItemHyperLinkEditRevisitCount.AutoHeight = false;
            this.repositoryItemHyperLinkEditRevisitCount.Name = "repositoryItemHyperLinkEditRevisitCount";
            this.repositoryItemHyperLinkEditRevisitCount.SingleClick = true;
            this.repositoryItemHyperLinkEditRevisitCount.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditRevisitCount_OpenLink);
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revisit Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemTextEditShortDate;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsColumn.AllowEdit = false;
            this.colRevisitDate.OptionsColumn.AllowFocus = false;
            this.colRevisitDate.OptionsColumn.ReadOnly = true;
            this.colRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 38;
            this.colRevisitDate.Width = 79;
            // 
            // repositoryItemTextEditShortDate
            // 
            this.repositoryItemTextEditShortDate.AutoHeight = false;
            this.repositoryItemTextEditShortDate.Mask.EditMask = "d";
            this.repositoryItemTextEditShortDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditShortDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShortDate.Name = "repositoryItemTextEditShortDate";
            this.repositoryItemTextEditShortDate.ValidateOnEnterKey = true;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.Visible = true;
            this.colMapID.VisibleIndex = 40;
            // 
            // colSpanInvoiced
            // 
            this.colSpanInvoiced.Caption = "Invoiced";
            this.colSpanInvoiced.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSpanInvoiced.FieldName = "SpanInvoiced";
            this.colSpanInvoiced.Name = "colSpanInvoiced";
            this.colSpanInvoiced.OptionsColumn.AllowEdit = false;
            this.colSpanInvoiced.OptionsColumn.AllowFocus = false;
            this.colSpanInvoiced.OptionsColumn.ReadOnly = true;
            this.colSpanInvoiced.Visible = true;
            this.colSpanInvoiced.VisibleIndex = 41;
            this.colSpanInvoiced.Width = 62;
            // 
            // colInvoiceNumber
            // 
            this.colInvoiceNumber.Caption = "Invoice #";
            this.colInvoiceNumber.FieldName = "InvoiceNumber";
            this.colInvoiceNumber.Name = "colInvoiceNumber";
            this.colInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colInvoiceNumber.Visible = true;
            this.colInvoiceNumber.VisibleIndex = 42;
            // 
            // colDateInvoiced
            // 
            this.colDateInvoiced.Caption = "Date Invoiced";
            this.colDateInvoiced.ColumnEdit = this.repositoryItemTextEditShortDate;
            this.colDateInvoiced.FieldName = "DateInvoiced";
            this.colDateInvoiced.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateInvoiced.Name = "colDateInvoiced";
            this.colDateInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateInvoiced.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateInvoiced.Visible = true;
            this.colDateInvoiced.VisibleIndex = 43;
            this.colDateInvoiced.Width = 88;
            // 
            // colDeferralReason
            // 
            this.colDeferralReason.Caption = "Deferral Reason";
            this.colDeferralReason.FieldName = "DeferralReason";
            this.colDeferralReason.Name = "colDeferralReason";
            this.colDeferralReason.OptionsColumn.AllowEdit = false;
            this.colDeferralReason.OptionsColumn.AllowFocus = false;
            this.colDeferralReason.OptionsColumn.ReadOnly = true;
            this.colDeferralReason.Visible = true;
            this.colDeferralReason.VisibleIndex = 35;
            this.colDeferralReason.Width = 99;
            // 
            // colDeferralRemarks
            // 
            this.colDeferralRemarks.Caption = "Deferral Remarks";
            this.colDeferralRemarks.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colDeferralRemarks.FieldName = "DeferralRemarks";
            this.colDeferralRemarks.Name = "colDeferralRemarks";
            this.colDeferralRemarks.OptionsColumn.AllowEdit = false;
            this.colDeferralRemarks.OptionsColumn.AllowFocus = false;
            this.colDeferralRemarks.OptionsColumn.ReadOnly = true;
            this.colDeferralRemarks.Visible = true;
            this.colDeferralRemarks.VisibleIndex = 37;
            this.colDeferralRemarks.Width = 104;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 17;
            this.colEstimatedHours.Width = 99;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 18;
            this.colActualHours.Width = 82;
            // 
            // colAccessMapPath
            // 
            this.colAccessMapPath.Caption = "Access Map";
            this.colAccessMapPath.ColumnEdit = this.repositoryItemHyperLinkEditRevisitCount;
            this.colAccessMapPath.FieldName = "AccessMapPath";
            this.colAccessMapPath.Name = "colAccessMapPath";
            this.colAccessMapPath.OptionsColumn.ReadOnly = true;
            this.colAccessMapPath.Visible = true;
            this.colAccessMapPath.VisibleIndex = 44;
            this.colAccessMapPath.Width = 100;
            // 
            // colTrafficMapPath
            // 
            this.colTrafficMapPath.Caption = "Traffic Map";
            this.colTrafficMapPath.ColumnEdit = this.repositoryItemHyperLinkEditRevisitCount;
            this.colTrafficMapPath.FieldName = "TrafficMapPath";
            this.colTrafficMapPath.Name = "colTrafficMapPath";
            this.colTrafficMapPath.OptionsColumn.ReadOnly = true;
            this.colTrafficMapPath.Visible = true;
            this.colTrafficMapPath.VisibleIndex = 45;
            this.colTrafficMapPath.Width = 100;
            // 
            // colClearanceDistanceUnder
            // 
            this.colClearanceDistanceUnder.Caption = "Clearance Distance (Under)";
            this.colClearanceDistanceUnder.FieldName = "ClearanceDistanceUnder";
            this.colClearanceDistanceUnder.Name = "colClearanceDistanceUnder";
            this.colClearanceDistanceUnder.OptionsColumn.AllowEdit = false;
            this.colClearanceDistanceUnder.OptionsColumn.AllowFocus = false;
            this.colClearanceDistanceUnder.OptionsColumn.ReadOnly = true;
            this.colClearanceDistanceUnder.Visible = true;
            this.colClearanceDistanceUnder.VisibleIndex = 20;
            this.colClearanceDistanceUnder.Width = 153;
            // 
            // colSpanResilient
            // 
            this.colSpanResilient.Caption = "Is Resilient";
            this.colSpanResilient.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSpanResilient.FieldName = "SpanResilient";
            this.colSpanResilient.Name = "colSpanResilient";
            this.colSpanResilient.OptionsColumn.AllowEdit = false;
            this.colSpanResilient.OptionsColumn.AllowFocus = false;
            this.colSpanResilient.OptionsColumn.ReadOnly = true;
            this.colSpanResilient.Visible = true;
            this.colSpanResilient.VisibleIndex = 28;
            // 
            // colIvyOnPole
            // 
            this.colIvyOnPole.Caption = "Ivy on Pole";
            this.colIvyOnPole.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIvyOnPole.FieldName = "IvyOnPole";
            this.colIvyOnPole.Name = "colIvyOnPole";
            this.colIvyOnPole.OptionsColumn.AllowEdit = false;
            this.colIvyOnPole.OptionsColumn.AllowFocus = false;
            this.colIvyOnPole.OptionsColumn.ReadOnly = true;
            this.colIvyOnPole.Visible = true;
            this.colIvyOnPole.VisibleIndex = 29;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 114;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.Caption = "Held Up Type";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 13;
            this.colHeldUpType.Width = 83;
            // 
            // colLinearCutLength
            // 
            this.colLinearCutLength.Caption = "Linear Cut Length";
            this.colLinearCutLength.FieldName = "LinearCutLength";
            this.colLinearCutLength.Name = "colLinearCutLength";
            this.colLinearCutLength.OptionsColumn.AllowEdit = false;
            this.colLinearCutLength.OptionsColumn.AllowFocus = false;
            this.colLinearCutLength.OptionsColumn.ReadOnly = true;
            this.colLinearCutLength.Visible = true;
            this.colLinearCutLength.VisibleIndex = 10;
            this.colLinearCutLength.Width = 104;
            // 
            // colShutdownHours
            // 
            this.colShutdownHours.Caption = "Shutdown Hours";
            this.colShutdownHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colShutdownHours.FieldName = "ShutdownHours";
            this.colShutdownHours.Name = "colShutdownHours";
            this.colShutdownHours.OptionsColumn.AllowEdit = false;
            this.colShutdownHours.OptionsColumn.AllowFocus = false;
            this.colShutdownHours.OptionsColumn.ReadOnly = true;
            this.colShutdownHours.Visible = true;
            this.colShutdownHours.VisibleIndex = 12;
            this.colShutdownHours.Width = 98;
            // 
            // colDeferredEstimatedCuttingHours
            // 
            this.colDeferredEstimatedCuttingHours.Caption = "Deferred Est. Cutting Hours";
            this.colDeferredEstimatedCuttingHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colDeferredEstimatedCuttingHours.FieldName = "DeferredEstimatedCuttingHours";
            this.colDeferredEstimatedCuttingHours.Name = "colDeferredEstimatedCuttingHours";
            this.colDeferredEstimatedCuttingHours.OptionsColumn.AllowEdit = false;
            this.colDeferredEstimatedCuttingHours.OptionsColumn.AllowFocus = false;
            this.colDeferredEstimatedCuttingHours.OptionsColumn.ReadOnly = true;
            this.colDeferredEstimatedCuttingHours.Visible = true;
            this.colDeferredEstimatedCuttingHours.VisibleIndex = 36;
            this.colDeferredEstimatedCuttingHours.Width = 153;
            // 
            // colClearanceDistanceAbove
            // 
            this.colClearanceDistanceAbove.Caption = "Clearance Distance (Above)";
            this.colClearanceDistanceAbove.FieldName = "ClearanceDistanceAbove";
            this.colClearanceDistanceAbove.Name = "colClearanceDistanceAbove";
            this.colClearanceDistanceAbove.OptionsColumn.AllowEdit = false;
            this.colClearanceDistanceAbove.OptionsColumn.AllowFocus = false;
            this.colClearanceDistanceAbove.OptionsColumn.ReadOnly = true;
            this.colClearanceDistanceAbove.Visible = true;
            this.colClearanceDistanceAbove.VisibleIndex = 21;
            this.colClearanceDistanceAbove.Width = 153;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage1.ImageOptions.Image")));
            this.xtraTabPage1.ImageOptions.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1032, 154);
            this.xtraTabPage1.Text = "Linked Documents";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl3;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1032, 154);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Email Selected Linked Document(s)", "email")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl3.Size = new System.Drawing.Size(1032, 154);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView3_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            this.colDocumentRemarks.Width = 134;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07001_UT_Client_FilterTableAdapter
            // 
            this.sp07001_UT_Client_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit2),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.Caption = "Client Filter";
            this.popupContainerEdit1.Edit = this.repositoryItemPopupContainerEdit1;
            this.popupContainerEdit1.EditValue = "No Client Filter";
            this.popupContainerEdit1.EditWidth = 250;
            this.popupContainerEdit1.Id = 28;
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControlClients;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit1_QueryResultValue);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.Caption = "Status Filter";
            this.popupContainerEdit2.Edit = this.repositoryItemPopupContainerEdit2;
            this.popupContainerEdit2.EditValue = "No Status Filter";
            this.popupContainerEdit2.EditWidth = 250;
            this.popupContainerEdit2.Id = 29;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            this.repositoryItemPopupContainerEdit2.PopupControl = this.popupContainerControlRegions;
            this.repositoryItemPopupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit2_QueryResultValue);
            // 
            // popupContainerDateRange
            // 
            this.popupContainerDateRange.Caption = "Date Filter";
            this.popupContainerDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.popupContainerDateRange.EditValue = "No Date Filter";
            this.popupContainerDateRange.EditWidth = 140;
            this.popupContainerDateRange.Id = 34;
            this.popupContainerDateRange.Name = "popupContainerDateRange";
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Load Surveys";
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // sp07086_UT_Survey_ManagerTableAdapter
            // 
            this.sp07086_UT_Survey_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Date Filter";
            this.barEditItem1.Edit = this.repositoryItemDateEdit1;
            this.barEditItem1.Id = 33;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // sp07087_UT_Survey_Status_FilterTableAdapter
            // 
            this.sp07087_UT_Survey_Status_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp07102_UT_Survey_Manager_Linked_Surveyed_PolesTableAdapter
            // 
            this.sp07102_UT_Survey_Manager_Linked_Surveyed_PolesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // frm_UT_Survey_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1041, 542);
            this.Controls.Add(this.splitContainerControl2);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Survey_Manager";
            this.Text = "Survey Manager";
            this.Activated += new System.EventHandler(this.frm_UT_Survey_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Survey_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Survey_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlClients)).EndInit();
            this.popupContainerControlClients.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07001UTClientFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07086UTSurveyManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRegions)).EndInit();
            this.popupContainerControlRegions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07087UTSurveyStatusFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07102UTSurveyManagerLinkedSurveyedPolesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateMask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditRevisitCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlClients;
        private DevExpress.XtraEditors.SimpleButton btnClientFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07001UTClientFilterBindingSource;
        private DataSet_UTTableAdapters.sp07001_UT_Client_FilterTableAdapter sp07001_UT_Client_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlRegions;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.SimpleButton btnRegionFilterOK;
        private System.Windows.Forms.BindingSource sp07086UTSurveyManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyorID;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colMapStartTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLastMapRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLastMapX;
        private DevExpress.XtraGrid.Columns.GridColumn colLastMapY;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMapStartType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedPoleCount;
        private DataSet_UTTableAdapters.sp07086_UT_Survey_ManagerTableAdapter sp07086_UT_Survey_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyor;
        private DevExpress.XtraBars.BarEditItem popupContainerDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private System.Windows.Forms.BindingSource sp07087UTSurveyStatusFilterBindingSource;
        private DataSet_UTTableAdapters.sp07087_UT_Survey_Status_FilterTableAdapter sp07087_UT_Survey_Status_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusOrder;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private System.Windows.Forms.BindingSource sp07102UTSurveyManagerLinkedSurveyedPolesBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateMask;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSpanClear;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colInfestationRate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colLinesmanPossible;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementResolved;
        private DevExpress.XtraGrid.Columns.GridColumn colFiveYearClearanceAchieved;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeWithin3Meters;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyor1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExchequerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DataSet_UTTableAdapters.sp07102_UT_Survey_Manager_Linked_Surveyed_PolesTableAdapter sp07102_UT_Survey_Manager_Linked_Surveyed_PolesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLastMapRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkspaceID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkspaceName;
        private DevExpress.XtraGrid.Columns.GridColumn colNoWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatusID1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferred;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptior;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptiorID;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colExchequerNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalIncome;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraGrid.Columns.GridColumn colIsBaseClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditRevisitCount;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShortDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpanInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownCount;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementCount;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessMapPath;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficMapPath;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteApprovedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteReceivedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistanceUnder;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colReactiveCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colReactiveRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colSpanResilient;
        private DevExpress.XtraGrid.Columns.GridColumn colIvyOnPole;
        private DevExpress.XtraGrid.Columns.GridColumn colContractYear;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinearCutLength;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTargetSurveyCompletionDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTargetPermissionCompletionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTargetWorkCompletionDate;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownHours;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredEstimatedCuttingHours;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistanceAbove;
    }
}

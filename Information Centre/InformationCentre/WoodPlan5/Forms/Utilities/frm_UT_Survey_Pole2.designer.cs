namespace WoodPlan5
{
    partial class frm_UT_Survey_Pole2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Survey_Pole2));
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue6 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule7 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue7 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule8 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue8 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditSignatureCapture = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bciShowLinkedRecords = new DevExpress.XtraBars.BarCheckItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.bciViewAdditionalInfo = new DevExpress.XtraBars.BarCheckItem();
            this.bbiShow = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageWelcome = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageTakePicture = new DevExpress.XtraTab.XtraTabPage();
            this.btnTakePicture = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07117UTPoleSurveyPicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyPictureID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPicturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDateTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07119UTPoleSurveyHistoricalPicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageGeneralInfo = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ClearanceDistanceAboveIDFGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07114UTSurveyedPoleItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp07356UTClearanceDistancesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView29 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DeferredEstimatedCuttingHoursSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ShutdownHoursSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.HeldUpTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07465UTHeldUpTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView28 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.LinearCutLengthIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07464UTLinearCutLengthsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView21 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IvyOnPoleCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SpanResilientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClearanceDistanceUnderIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AccessMapPathButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.TrafficMapPathButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl26 = new DevExpress.XtraGrid.GridControl();
            this.sp07367UTTrafficManagementForSurveyedPoleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView26 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTrafficManagementID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedPoleID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07366UTTrafficManagementItemsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colRemarks8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit23 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.DeferralRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DeferralReasonIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07357UTDeferralReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView25 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClearanceDistanceGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView24 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RemarksInformationLabel = new DevExpress.XtraEditors.LabelControl();
            this.RevisitDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.InvoiceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateInvoicedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SpanInvoicedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsBaseClimbableCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DeferredUnitDescriptiorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07046UTInspectionCyclesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DeferredUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DeferredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.G55CategoryIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07186UTG55CategoriesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SurveyStatusIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SurveyStatusTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SurveyedPoleIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SurveyIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PoleIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FeederContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SurveyDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.IsSpanClearCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsShutdownRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.HotGloveRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.LinesmanPossibleCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TrafficManagementRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TreeWithin3MetersCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TreeClimbableCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.GUIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PoleNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.InfestationRateGridLookupEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07120UTSurveyInfestationRateListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TrafficManagementResolvedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ItemForGUID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyedPoleID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPoleID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFeederContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForInfestationRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHotGloveRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinesmanPossible = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTreeClimbable = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSpanInvoiced = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateInvoiced = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInvoiceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDeferred = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeferredUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeferredUnitDescriptiorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRevisitDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeferralReasonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeferralRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeferredEstimatedCuttingHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTrafficManagementRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTrafficManagementResolved = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTrafficMapPath = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccessMapPath = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIsSpanClear = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClearanceDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForG55CategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsBaseClimbable = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTreeWithin3Meters = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSpanResilient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIvyOnPole = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHeldUpTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsShutdownRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForShutdownHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinearCutLengthID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClearanceDistanceUnderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClearanceDistanceAboveID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPoleNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPageTrees = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07121UTSurveyedPoleLinkedTreesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSizeBandID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXYPairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatLongPairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DPMetres = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSizeBand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrowthRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeadTree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.xtraTabControl3 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage9 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07122UTSurveyedPoleSpeciesLinkedToTreeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberOfTrees = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric0DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage10 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07123UTSurveyedPoleTreePicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl6 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageTreeWork = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControl5 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage14 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07127UTSurveyedPoleWorkListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDateScheduled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colApprovedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCost = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemBillingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAchievableClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMeters = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPossibleFlail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShortDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEstimatedHours1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryScreenName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl4 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage11 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp07130UTSurveyedPoleWorkPicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShortLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl27 = new DevExpress.XtraGrid.GridControl();
            this.sp07401UTPDLinkedToActionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.gridView27 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPermissionDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRaisedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSignatureFile2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit24 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmergencyAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestTelephonePoint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHotGloveAccessAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colAccessAgreedWithLandOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessDetailsOnMap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadsideAccessOnly = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPOTree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningConservation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWildlifeDesignation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecialInstructions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimarySubName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimarySubNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLVSubName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLVSubNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagedUnitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnvironmentalRANumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandscapeImpactNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionEmailed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostageRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerSalutation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsChipRemove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsChipOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsStackOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToDoActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryScreenName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit0DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPage12 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp07139UTSurveyedTreeWorkLinkedEquipmentReadOnlyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromDefaultRequiredEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage13 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp07140UTSurveyedTreeWorkLinkedMaterialsReadOnlyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionMaterialID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToWork1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnit1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnit1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage15 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp07149UTSurveyedPoleWorkHistoricalListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualHours2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEstimatedHours2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageTreeDefects = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl12 = new DevExpress.XtraGrid.GridControl();
            this.sp07150UTSurveyedTreeDefectListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeDefectID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefectID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefectDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl7 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl13 = new DevExpress.XtraGrid.GridControl();
            this.sp07154UTSurveyedTreeDefectPicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.checkEditNoWorkRequired = new DevExpress.XtraEditors.CheckEdit();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageRiskAssessment = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl22 = new DevExpress.XtraGrid.GridControl();
            this.sp07254UTSurveyedPoleRiskAssessmentListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView22 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRiskAssessmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedPoleID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colPersonCompletingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonCompletingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colElectricalAssessmentVerified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElectricalRiskCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeOfWorkingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermitHolderName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNRSWAOpeningNoticeNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElectricalRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeOfWorking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonCompletingType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonCompletingName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMEWPArialRescuers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmergencyKitLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAirAmbulanceLat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAirAmbulanceLong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBanksMan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmergencyServicesMeetPoint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobilePhoneSignalBars = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestAandE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestLandLine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl23 = new DevExpress.XtraGrid.GridControl();
            this.sp07255UTSurveyedPoleRiskAssessmentQuestionsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView23 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRiskQuestionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit22 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colQuestionNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestionOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasterQuestionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentDateTimeRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colRiskAssessmentType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerText1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerText2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerText3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageFinish = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.ItemForFiveYearClearanceAchieved = new DevExpress.XtraEditors.LabelControl();
            this.FiveYearClearanceAchievedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.groupControlMissingInfo = new DevExpress.XtraEditors.GroupControl();
            this.btnFixFinishSurveyIssue = new DevExpress.XtraEditors.SimpleButton();
            this.labelFinishSurveyWarningInfo = new DevExpress.XtraEditors.LabelControl();
            this.labelFinishSurveyWarning = new DevExpress.XtraEditors.LabelControl();
            this.btnOnHold = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.dataSet_UT_Edit1 = new WoodPlan5.DataSet_UT_Edit();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrior = new DevExpress.XtraEditors.SimpleButton();
            this.sp07114_UT_Surveyed_Pole_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07114_UT_Surveyed_Pole_ItemTableAdapter();
            this.sp07117_UT_Pole_Survey_Pictures_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07117_UT_Pole_Survey_Pictures_ListTableAdapter();
            this.sp07119_UT_Pole_Survey_Historical_Pictures_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07119_UT_Pole_Survey_Historical_Pictures_ListTableAdapter();
            this.sp07120_UT_Survey_Infestation_Rate_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07120_UT_Survey_Infestation_Rate_ListTableAdapter();
            this.sp07121_UT_Surveyed_Pole_Linked_TreesTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07121_UT_Surveyed_Pole_Linked_TreesTableAdapter();
            this.sp07122_UT_Surveyed_Pole_Species_Linked_To_TreeTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07122_UT_Surveyed_Pole_Species_Linked_To_TreeTableAdapter();
            this.sp07123_UT_Surveyed_Pole_Tree_Pictures_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07123_UT_Surveyed_Pole_Tree_Pictures_ListTableAdapter();
            this.sp07127_UT_Surveyed_Pole_Work_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07127_UT_Surveyed_Pole_Work_ListTableAdapter();
            this.sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter();
            this.sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_OnlyTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_OnlyTableAdapter();
            this.sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_OnlyTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_OnlyTableAdapter();
            this.sp07149_UT_Surveyed_Pole_Work_Historical_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07149_UT_Surveyed_Pole_Work_Historical_ListTableAdapter();
            this.sp07150_UT_Surveyed_Tree_Defect_ListTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07150_UT_Surveyed_Tree_Defect_ListTableAdapter();
            this.sp07154_UT_Surveyed_Tree_Defect_Pictures_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07154_UT_Surveyed_Tree_Defect_Pictures_ListTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLinkedRecords = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.xtraTabControl8 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer8 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl16 = new DevExpress.XtraGrid.GridControl();
            this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView16 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleAccessIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn122 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessIssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn123 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer5 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl17 = new DevExpress.XtraGrid.GridControl();
            this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn124 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn125 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn126 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn127 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn128 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn129 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn130 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn132 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn133 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn134 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn135 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer6 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl18 = new DevExpress.XtraGrid.GridControl();
            this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView18 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn136 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn137 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn138 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn139 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn140 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn141 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn142 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn143 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn144 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn145 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn146 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn147 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage8 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer7 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl19 = new DevExpress.XtraGrid.GridControl();
            this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView19 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn148 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn149 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn150 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn151 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn152 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn153 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn154 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn155 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn156 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn157 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn158 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn159 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage16 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl20 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView20 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn160 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn161 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn162 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn163 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn164 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit19 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter();
            this.sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter();
            this.sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter();
            this.sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter();
            this.sp07186_UT_G55_Categories_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07186_UT_G55_Categories_With_BlankTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07254_UT_Surveyed_Pole_Risk_Assessment_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07254_UT_Surveyed_Pole_Risk_Assessment_ListTableAdapter();
            this.sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_ListTableAdapter();
            this.labelControlStep = new DevExpress.XtraEditors.LabelControl();
            this.sp07356_UT_Clearance_Distances_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07356_UT_Clearance_Distances_With_BlankTableAdapter();
            this.sp07357_UT_Deferral_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07357_UT_Deferral_Reasons_With_BlankTableAdapter();
            this.sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter();
            this.sp07366_UT_Traffic_Management_Items_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07366_UT_Traffic_Management_Items_With_BlankTableAdapter();
            this.sp07401_UT_PD_Linked_To_ActionsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07401_UT_PD_Linked_To_ActionsTableAdapter();
            this.sp07464_UT_Linear_Cut_Lengths_With_BlankTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07464_UT_Linear_Cut_Lengths_With_BlankTableAdapter();
            this.sp07465_UT_Held_Up_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07465_UT_Held_Up_Types_With_BlankTableAdapter();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending26 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending12 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending7 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending27 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending9 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending10 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending22 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending23 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSignatureCapture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            this.xtraTabPageTakePicture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07117UTPoleSurveyPicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07119UTPoleSurveyHistoricalPicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            this.xtraTabPageGeneralInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClearanceDistanceAboveIDFGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07114UTSurveyedPoleItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07356UTClearanceDistancesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredEstimatedCuttingHoursSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShutdownHoursSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeldUpTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07465UTHeldUpTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinearCutLengthIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07464UTLinearCutLengthsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IvyOnPoleCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpanResilientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClearanceDistanceUnderIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessMapPathButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficMapPathButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07367UTTrafficManagementForSurveyedPoleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07366UTTrafficManagementItemsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferralRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferralReasonIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07357UTDeferralReasonsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClearanceDistanceGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateInvoicedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateInvoicedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpanInvoicedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsBaseClimbableCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07046UTInspectionCyclesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G55CategoryIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07186UTG55CategoriesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyStatusIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyStatusTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyedPoleIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSpanClearCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsShutdownRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotGloveRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinesmanPossibleCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficManagementRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeWithin3MetersCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeClimbableCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GUIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfestationRateGridLookupEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07120UTSurveyInfestationRateListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficManagementResolvedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGUID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyedPoleID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInfestationRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHotGloveRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinesmanPossible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeClimbable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpanInvoiced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateInvoiced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferred)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredUnitDescriptiorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferralReasonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferralRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredEstimatedCuttingHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrafficManagementRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrafficManagementResolved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrafficMapPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessMapPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSpanClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearanceDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForG55CategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsBaseClimbable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeWithin3Meters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpanResilient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIvyOnPole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeldUpTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsShutdownRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShutdownHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinearCutLengthID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearanceDistanceUnderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearanceDistanceAboveID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleNumber)).BeginInit();
            this.xtraTabPageTrees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07121UTSurveyedPoleLinkedTreesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DPMetres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).BeginInit();
            this.xtraTabControl3.SuspendLayout();
            this.xtraTabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07122UTSurveyedPoleSpeciesLinkedToTreeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.xtraTabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07123UTSurveyedPoleTreePicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl6)).BeginInit();
            this.xtraTabControl6.SuspendLayout();
            this.xtraTabPageTreeWork.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl5)).BeginInit();
            this.xtraTabControl5.SuspendLayout();
            this.xtraTabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07127UTSurveyedPoleWorkListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl4)).BeginInit();
            this.xtraTabControl4.SuspendLayout();
            this.xtraTabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07130UTSurveyedPoleWorkPicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07401UTPDLinkedToActionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit0DP)).BeginInit();
            this.xtraTabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07139UTSurveyedTreeWorkLinkedEquipmentReadOnlyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            this.xtraTabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07140UTSurveyedTreeWorkLinkedMaterialsReadOnlyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            this.xtraTabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07149UTSurveyedPoleWorkHistoricalListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours2)).BeginInit();
            this.xtraTabPageTreeDefects.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07150UTSurveyedTreeDefectListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl7)).BeginInit();
            this.xtraTabControl7.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07154UTSurveyedTreeDefectPicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNoWorkRequired.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            this.xtraTabPageRiskAssessment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07254UTSurveyedPoleRiskAssessmentListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07255UTSurveyedPoleRiskAssessmentQuestionsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.xtraTabPageFinish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FiveYearClearanceAchievedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMissingInfo)).BeginInit();
            this.groupControlMissingInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelLinkedRecords.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl8)).BeginInit();
            this.xtraTabControl8.SuspendLayout();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).BeginInit();
            this.gridSplitContainer8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).BeginInit();
            this.gridSplitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).BeginInit();
            this.gridSplitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).BeginInit();
            this.xtraTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).BeginInit();
            this.gridSplitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).BeginInit();
            this.xtraTabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1092, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 516);
            this.barDockControlBottom.Size = new System.Drawing.Size(1092, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 490);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1092, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 490);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.DockManager = this.dockManager1;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "ID";
            this.gridColumn111.FieldName = "ID";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.AllowEdit = false;
            this.gridColumn111.OptionsColumn.AllowFocus = false;
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "ID";
            this.gridColumn108.FieldName = "ID";
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowFocus = false;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "ID";
            this.gridColumn92.FieldName = "ID";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "ID";
            this.gridColumn52.FieldName = "ID";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "ID";
            this.gridColumn48.FieldName = "ID";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "ID";
            this.gridColumn97.FieldName = "ID";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "ID";
            this.gridColumn68.FieldName = "ID";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemHyperLinkEditSignatureCapture
            // 
            this.repositoryItemHyperLinkEditSignatureCapture.AutoHeight = false;
            this.repositoryItemHyperLinkEditSignatureCapture.LookAndFeel.SkinName = "Blue";
            this.repositoryItemHyperLinkEditSignatureCapture.Name = "repositoryItemHyperLinkEditSignatureCapture";
            this.repositoryItemHyperLinkEditSignatureCapture.SingleClick = true;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bciViewAdditionalInfo,
            this.bbiShow,
            this.bciShowLinkedRecords});
            this.barManager2.MaxItemId = 25;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3,
            this.repositoryItemSpinEdit1});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciShowLinkedRecords, true)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem1);
            superToolTip2.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem2);
            superToolTip3.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bciShowLinkedRecords
            // 
            this.bciShowLinkedRecords.Caption = "Show Linked Records";
            this.bciShowLinkedRecords.Id = 24;
            this.bciShowLinkedRecords.Name = "bciShowLinkedRecords";
            this.bciShowLinkedRecords.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciShowLinkedRecords_CheckedChanged);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem3);
            superToolTip4.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1092, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 516);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1092, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 490);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1092, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 490);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "go-previous_16.png");
            this.imageCollection1.Images.SetKeyName(7, "go-next_16.png");
            this.imageCollection1.Images.SetKeyName(8, "Save_16x16.png");
            this.imageCollection1.Images.SetKeyName(9, "BlockAdd_16x16.png");
            this.imageCollection1.Images.SetKeyName(10, "Refresh_16x16.png");
            this.imageCollection1.InsertGalleryImage("copy_16x16.png", "images/edit/copy_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/copy_16x16.png"), 11);
            this.imageCollection1.Images.SetKeyName(11, "copy_16x16.png");
            this.imageCollection1.InsertGalleryImage("paste_16x16.png", "images/edit/paste_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/paste_16x16.png"), 12);
            this.imageCollection1.Images.SetKeyName(12, "paste_16x16.png");
            this.imageCollection1.Images.SetKeyName(13, "defaults_16.png");
            // 
            // bciViewAdditionalInfo
            // 
            this.bciViewAdditionalInfo.Caption = "View Additional Info";
            this.bciViewAdditionalInfo.Id = 15;
            this.bciViewAdditionalInfo.Name = "bciViewAdditionalInfo";
            // 
            // bbiShow
            // 
            this.bbiShow.Caption = "Show Linked Records";
            this.bbiShow.Id = 23;
            this.bbiShow.Name = "bbiShow";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            90,
            0,
            0,
            65536});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 26);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            this.xtraTabControl1.Size = new System.Drawing.Size(1092, 490);
            this.xtraTabControl1.TabIndex = 13;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageWelcome,
            this.xtraTabPageTakePicture,
            this.xtraTabPageGeneralInfo,
            this.xtraTabPageTrees,
            this.xtraTabPageRiskAssessment,
            this.xtraTabPageFinish});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPageWelcome
            // 
            this.xtraTabPageWelcome.Controls.Add(this.panelControl1);
            this.xtraTabPageWelcome.Controls.Add(this.pictureEdit2);
            this.xtraTabPageWelcome.Controls.Add(this.btnStart);
            this.xtraTabPageWelcome.Name = "xtraTabPageWelcome";
            this.xtraTabPageWelcome.Size = new System.Drawing.Size(1087, 464);
            this.xtraTabPageWelcome.Text = "Welcome";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.pictureEdit3);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(208, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(874, 32);
            this.panelControl1.TabIndex = 18;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.EditValue = ((object)(resources.GetObject("pictureEdit3.EditValue")));
            this.pictureEdit3.Location = new System.Drawing.Point(5, 5);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Size = new System.Drawing.Size(20, 20);
            this.pictureEdit3.TabIndex = 19;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoEllipsis = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(29, 1);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(840, 26);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "Welcome to the Pole Surveying Wizard.\r\nClick the <b>Start Survey</b> button once " +
    "you are ready to proceed...";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(3, 3);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(200, 459);
            this.pictureEdit2.TabIndex = 17;
            // 
            // btnStart
            // 
            this.btnStart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnStart.Appearance.Options.UseFont = true;
            this.btnStart.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.ImageOptions.Image")));
            this.btnStart.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStart.Location = new System.Drawing.Point(208, 68);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(125, 44);
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "Start Survey";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // xtraTabPageTakePicture
            // 
            this.xtraTabPageTakePicture.Controls.Add(this.btnTakePicture);
            this.xtraTabPageTakePicture.Controls.Add(this.xtraTabControl2);
            this.xtraTabPageTakePicture.Controls.Add(this.panelControl2);
            this.xtraTabPageTakePicture.Name = "xtraTabPageTakePicture";
            this.xtraTabPageTakePicture.Size = new System.Drawing.Size(1087, 464);
            this.xtraTabPageTakePicture.Text = "Take Picture(s)";
            // 
            // btnTakePicture
            // 
            this.btnTakePicture.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnTakePicture.Appearance.Options.UseFont = true;
            this.btnTakePicture.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnTakePicture.ImageOptions.Image")));
            this.btnTakePicture.Location = new System.Drawing.Point(11, 48);
            this.btnTakePicture.Name = "btnTakePicture";
            this.btnTakePicture.Size = new System.Drawing.Size(125, 44);
            this.btnTakePicture.TabIndex = 20;
            this.btnTakePicture.Text = "Take Picture";
            this.btnTakePicture.Click += new System.EventHandler(this.btnTakePicture_Click);
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl2.Location = new System.Drawing.Point(146, 46);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
            this.xtraTabControl2.Size = new System.Drawing.Size(938, 414);
            this.xtraTabControl2.TabIndex = 11;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl1);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(933, 388);
            this.xtraTabPage3.Text = "Survey Pole - Current Pictures";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07117UTPoleSurveyPicturesListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(933, 388);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07117UTPoleSurveyPicturesListBindingSource
            // 
            this.sp07117UTPoleSurveyPicturesListBindingSource.DataMember = "sp07117_UT_Pole_Survey_Pictures_List";
            this.sp07117UTPoleSurveyPicturesListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyPictureID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colPictureTypeID,
            this.colPicturePath,
            this.colDateTimeTaken,
            this.colAddedByStaffID,
            this.colGUID,
            this.colRemarks,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTimeTaken, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSurveyPictureID
            // 
            this.colSurveyPictureID.Caption = "Survey Picture ID";
            this.colSurveyPictureID.FieldName = "SurveyPictureID";
            this.colSurveyPictureID.Name = "colSurveyPictureID";
            this.colSurveyPictureID.OptionsColumn.AllowEdit = false;
            this.colSurveyPictureID.OptionsColumn.AllowFocus = false;
            this.colSurveyPictureID.OptionsColumn.ReadOnly = true;
            this.colSurveyPictureID.Width = 105;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked To Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 117;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 144;
            // 
            // colPictureTypeID
            // 
            this.colPictureTypeID.Caption = "Picture Type ID";
            this.colPictureTypeID.FieldName = "PictureTypeID";
            this.colPictureTypeID.Name = "colPictureTypeID";
            this.colPictureTypeID.OptionsColumn.AllowEdit = false;
            this.colPictureTypeID.OptionsColumn.AllowFocus = false;
            this.colPictureTypeID.OptionsColumn.ReadOnly = true;
            this.colPictureTypeID.Width = 95;
            // 
            // colPicturePath
            // 
            this.colPicturePath.Caption = "Picture Path";
            this.colPicturePath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colPicturePath.FieldName = "PicturePath";
            this.colPicturePath.Name = "colPicturePath";
            this.colPicturePath.OptionsColumn.ReadOnly = true;
            this.colPicturePath.Visible = true;
            this.colPicturePath.VisibleIndex = 2;
            this.colPicturePath.Width = 263;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colDateTimeTaken
            // 
            this.colDateTimeTaken.Caption = "Date Taken";
            this.colDateTimeTaken.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDateTimeTaken.FieldName = "DateTimeTaken";
            this.colDateTimeTaken.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateTimeTaken.Name = "colDateTimeTaken";
            this.colDateTimeTaken.OptionsColumn.AllowEdit = false;
            this.colDateTimeTaken.OptionsColumn.AllowFocus = false;
            this.colDateTimeTaken.OptionsColumn.ReadOnly = true;
            this.colDateTimeTaken.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateTimeTaken.Visible = true;
            this.colDateTimeTaken.VisibleIndex = 0;
            this.colDateTimeTaken.Width = 99;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To Survey";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 103;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 1;
            this.colAddedByStaffName.Width = 108;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridControl2);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(933, 388);
            this.xtraTabPage4.Text = "Surveyed Pole - Historical Pictures";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07119UTPoleSurveyHistoricalPicturesListBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit2,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl2.Size = new System.Drawing.Size(933, 388);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07119UTPoleSurveyHistoricalPicturesListBindingSource
            // 
            this.sp07119UTPoleSurveyHistoricalPicturesListBindingSource.DataMember = "sp07119_UT_Pole_Survey_Historical_Pictures_List";
            this.sp07119UTPoleSurveyHistoricalPicturesListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Survey Picture ID";
            this.gridColumn1.FieldName = "SurveyPictureID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 105;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Linked To Record ID";
            this.gridColumn2.FieldName = "LinkedToRecordID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 117;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Linked To Record Type ID";
            this.gridColumn3.FieldName = "LinkedToRecordTypeID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 144;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Picture Type ID";
            this.gridColumn4.FieldName = "PictureTypeID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 95;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Picture Path";
            this.gridColumn5.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.gridColumn5.FieldName = "PicturePath";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 263;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Date Taken";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn6.FieldName = "DateTimeTaken";
            this.gridColumn6.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 99;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "g";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Added By Staff ID";
            this.gridColumn7.FieldName = "AddedByStaffID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 108;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "GUID";
            this.gridColumn8.FieldName = "GUID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Remarks";
            this.gridColumn9.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn9.FieldName = "Remarks";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 4;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Linked To Survey";
            this.gridColumn10.FieldName = "LinkedRecordDescription";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            this.gridColumn10.Width = 384;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Added By";
            this.gridColumn11.FieldName = "AddedByStaffName";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 108;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.pictureEdit4);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Location = new System.Drawing.Point(4, 4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1079, 32);
            this.panelControl2.TabIndex = 19;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.EditValue = ((object)(resources.GetObject("pictureEdit4.EditValue")));
            this.pictureEdit4.Location = new System.Drawing.Point(5, 5);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Size = new System.Drawing.Size(20, 20);
            this.pictureEdit4.TabIndex = 19;
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.labelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl3.AutoEllipsis = true;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(29, 2);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(1044, 27);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = resources.GetString("labelControl3.Text");
            // 
            // xtraTabPageGeneralInfo
            // 
            this.xtraTabPageGeneralInfo.Controls.Add(this.panelControl3);
            this.xtraTabPageGeneralInfo.Controls.Add(this.dataLayoutControl1);
            this.xtraTabPageGeneralInfo.Name = "xtraTabPageGeneralInfo";
            this.xtraTabPageGeneralInfo.Size = new System.Drawing.Size(1087, 464);
            this.xtraTabPageGeneralInfo.Text = "Record General Information";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Controls.Add(this.pictureEdit5);
            this.panelControl3.Controls.Add(this.labelControl2);
            this.panelControl3.Location = new System.Drawing.Point(4, 4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1079, 32);
            this.panelControl3.TabIndex = 20;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.EditValue = ((object)(resources.GetObject("pictureEdit5.EditValue")));
            this.pictureEdit5.Location = new System.Drawing.Point(5, 5);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Size = new System.Drawing.Size(20, 20);
            this.pictureEdit5.TabIndex = 19;
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.labelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl2.AutoEllipsis = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(29, 2);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(1045, 27);
            this.labelControl2.TabIndex = 14;
            this.labelControl2.Text = "Complete the details below.\r\nOnce done, click <b>Next</b> button to proceed to ne" +
    "xt step.";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataLayoutControl1.Controls.Add(this.ClearanceDistanceAboveIDFGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DeferredEstimatedCuttingHoursSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ShutdownHoursSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.HeldUpTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.LinearCutLengthIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.IvyOnPoleCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SpanResilientCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClearanceDistanceUnderIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AccessMapPathButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.TrafficMapPathButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.groupControl2);
            this.dataLayoutControl1.Controls.Add(this.DeferralRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DeferralReasonIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ClearanceDistanceGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksInformationLabel);
            this.dataLayoutControl1.Controls.Add(this.RevisitDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.InvoiceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateInvoicedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SpanInvoicedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsBaseClimbableCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DeferredUnitDescriptiorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DeferredUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DeferredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.G55CategoryIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyStatusIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyStatusTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyedPoleIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PoleIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FeederContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.IsSpanClearCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsShutdownRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.HotGloveRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.LinesmanPossibleCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TrafficManagementRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TreeWithin3MetersCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TreeClimbableCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.GUIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PoleNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.InfestationRateGridLookupEdit);
            this.dataLayoutControl1.Controls.Add(this.TrafficManagementResolvedCheckEdit);
            this.dataLayoutControl1.DataSource = this.sp07114UTSurveyedPoleItemBindingSource;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGUID,
            this.ItemForSurveyedPoleID,
            this.ItemForSurveyID,
            this.ItemForPoleID,
            this.ItemForSurveyStatusID,
            this.ItemForFeederContractID,
            this.ItemForCreatedByStaffID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(3, 42);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(5, 161, 321, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(825, 418);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ClearanceDistanceAboveIDFGridLookUpEdit
            // 
            this.ClearanceDistanceAboveIDFGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "ClearanceDistanceAboveID", true));
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Location = new System.Drawing.Point(704, 228);
            this.ClearanceDistanceAboveIDFGridLookUpEdit.MenuManager = this.barManager1;
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Name = "ClearanceDistanceAboveIDFGridLookUpEdit";
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Properties.DataSource = this.sp07356UTClearanceDistancesWithBlankBindingSource;
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Properties.NullText = "";
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Properties.PopupView = this.gridView29;
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Properties.ValueMember = "ID";
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Size = new System.Drawing.Size(102, 20);
            this.ClearanceDistanceAboveIDFGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ClearanceDistanceAboveIDFGridLookUpEdit.TabIndex = 9;
            this.ClearanceDistanceAboveIDFGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ClearanceDistanceAboveIDFGridLookUpEdit_EditValueChanged);
            this.ClearanceDistanceAboveIDFGridLookUpEdit.Validated += new System.EventHandler(this.ClearanceDistanceAboveIDFGridLookUpEdit_Validated);
            // 
            // sp07114UTSurveyedPoleItemBindingSource
            // 
            this.sp07114UTSurveyedPoleItemBindingSource.DataMember = "sp07114_UT_Surveyed_Pole_Item";
            this.sp07114UTSurveyedPoleItemBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // sp07356UTClearanceDistancesWithBlankBindingSource
            // 
            this.sp07356UTClearanceDistancesWithBlankBindingSource.DataMember = "sp07356_UT_Clearance_Distances_With_Blank";
            this.sp07356UTClearanceDistancesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView29
            // 
            this.gridView29.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113});
            this.gridView29.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn111;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView29.FormatRules.Add(gridFormatRule1);
            this.gridView29.Name = "gridView29";
            this.gridView29.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView29.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView29.OptionsLayout.StoreAppearance = true;
            this.gridView29.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView29.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView29.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView29.OptionsView.ColumnAutoWidth = false;
            this.gridView29.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView29.OptionsView.ShowGroupPanel = false;
            this.gridView29.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView29.OptionsView.ShowIndicator = false;
            this.gridView29.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn113, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "Clearance Distance";
            this.gridColumn112.FieldName = "Description";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.AllowEdit = false;
            this.gridColumn112.OptionsColumn.AllowFocus = false;
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.Visible = true;
            this.gridColumn112.VisibleIndex = 0;
            this.gridColumn112.Width = 201;
            // 
            // gridColumn113
            // 
            this.gridColumn113.Caption = "Order";
            this.gridColumn113.FieldName = "RecordOrder";
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.OptionsColumn.AllowEdit = false;
            this.gridColumn113.OptionsColumn.AllowFocus = false;
            this.gridColumn113.OptionsColumn.ReadOnly = true;
            // 
            // DeferredEstimatedCuttingHoursSpinEdit
            // 
            this.DeferredEstimatedCuttingHoursSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "DeferredEstimatedCuttingHours", true));
            this.DeferredEstimatedCuttingHoursSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DeferredEstimatedCuttingHoursSpinEdit.Location = new System.Drawing.Point(596, 741);
            this.DeferredEstimatedCuttingHoursSpinEdit.MenuManager = this.barManager1;
            this.DeferredEstimatedCuttingHoursSpinEdit.Name = "DeferredEstimatedCuttingHoursSpinEdit";
            this.DeferredEstimatedCuttingHoursSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DeferredEstimatedCuttingHoursSpinEdit.Properties.Mask.EditMask = "f2";
            this.DeferredEstimatedCuttingHoursSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DeferredEstimatedCuttingHoursSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            131072});
            this.DeferredEstimatedCuttingHoursSpinEdit.Size = new System.Drawing.Size(198, 20);
            this.DeferredEstimatedCuttingHoursSpinEdit.StyleController = this.dataLayoutControl1;
            this.DeferredEstimatedCuttingHoursSpinEdit.TabIndex = 29;
            // 
            // ShutdownHoursSpinEdit
            // 
            this.ShutdownHoursSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "ShutdownHours", true));
            this.ShutdownHoursSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ShutdownHoursSpinEdit.Location = new System.Drawing.Point(540, 148);
            this.ShutdownHoursSpinEdit.MenuManager = this.barManager1;
            this.ShutdownHoursSpinEdit.Name = "ShutdownHoursSpinEdit";
            this.ShutdownHoursSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ShutdownHoursSpinEdit.Properties.Mask.EditMask = "f2";
            this.ShutdownHoursSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ShutdownHoursSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            131072});
            this.ShutdownHoursSpinEdit.Size = new System.Drawing.Size(266, 20);
            this.ShutdownHoursSpinEdit.StyleController = this.dataLayoutControl1;
            this.ShutdownHoursSpinEdit.TabIndex = 4;
            this.ShutdownHoursSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ShutdownHoursSpinEdit_Validating);
            // 
            // HeldUpTypeIDGridLookUpEdit
            // 
            this.HeldUpTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "HeldUpTypeID", true));
            this.HeldUpTypeIDGridLookUpEdit.Location = new System.Drawing.Point(169, 125);
            this.HeldUpTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.HeldUpTypeIDGridLookUpEdit.Name = "HeldUpTypeIDGridLookUpEdit";
            this.HeldUpTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HeldUpTypeIDGridLookUpEdit.Properties.DataSource = this.sp07465UTHeldUpTypesWithBlankBindingSource;
            this.HeldUpTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.HeldUpTypeIDGridLookUpEdit.Properties.NullText = "";
            this.HeldUpTypeIDGridLookUpEdit.Properties.PopupView = this.gridView28;
            this.HeldUpTypeIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit5,
            this.repositoryItemMemoExEdit14});
            this.HeldUpTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.HeldUpTypeIDGridLookUpEdit.Size = new System.Drawing.Size(217, 20);
            this.HeldUpTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.HeldUpTypeIDGridLookUpEdit.TabIndex = 2;
            this.HeldUpTypeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.HeldUpTypeIDGridLookUpEdit_EditValueChanged);
            this.HeldUpTypeIDGridLookUpEdit.Validated += new System.EventHandler(this.HeldUpTypeIDGridLookUpEdit_Validated);
            // 
            // sp07465UTHeldUpTypesWithBlankBindingSource
            // 
            this.sp07465UTHeldUpTypesWithBlankBindingSource.DataMember = "sp07465_UT_Held_Up_Types_With_Blank";
            this.sp07465UTHeldUpTypesWithBlankBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView28
            // 
            this.gridView28.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn108,
            this.gridColumn109,
            this.gridColumn110});
            this.gridView28.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn108;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView28.FormatRules.Add(gridFormatRule2);
            this.gridView28.Name = "gridView28";
            this.gridView28.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView28.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView28.OptionsLayout.StoreAppearance = true;
            this.gridView28.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView28.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView28.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView28.OptionsView.ColumnAutoWidth = false;
            this.gridView28.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView28.OptionsView.ShowGroupPanel = false;
            this.gridView28.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView28.OptionsView.ShowIndicator = false;
            this.gridView28.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn110, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "Held Up Type";
            this.gridColumn109.FieldName = "Description";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowFocus = false;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.Visible = true;
            this.gridColumn109.VisibleIndex = 0;
            this.gridColumn109.Width = 201;
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "Order";
            this.gridColumn110.FieldName = "RecordOrder";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowFocus = false;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "f2";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // LinearCutLengthIDGridLookUpEdit
            // 
            this.LinearCutLengthIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "LinearCutLengthID", true));
            this.LinearCutLengthIDGridLookUpEdit.Location = new System.Drawing.Point(169, 252);
            this.LinearCutLengthIDGridLookUpEdit.MenuManager = this.barManager1;
            this.LinearCutLengthIDGridLookUpEdit.Name = "LinearCutLengthIDGridLookUpEdit";
            this.LinearCutLengthIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LinearCutLengthIDGridLookUpEdit.Properties.DataSource = this.sp07464UTLinearCutLengthsWithBlankBindingSource;
            this.LinearCutLengthIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.LinearCutLengthIDGridLookUpEdit.Properties.NullText = "";
            this.LinearCutLengthIDGridLookUpEdit.Properties.PopupView = this.gridView21;
            this.LinearCutLengthIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit5,
            this.repositoryItemMemoExEdit14});
            this.LinearCutLengthIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.LinearCutLengthIDGridLookUpEdit.Size = new System.Drawing.Size(115, 20);
            this.LinearCutLengthIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.LinearCutLengthIDGridLookUpEdit.TabIndex = 10;
            // 
            // sp07464UTLinearCutLengthsWithBlankBindingSource
            // 
            this.sp07464UTLinearCutLengthsWithBlankBindingSource.DataMember = "sp07464_UT_Linear_Cut_Lengths_With_Blank";
            this.sp07464UTLinearCutLengthsWithBlankBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView21
            // 
            this.gridView21.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn94,
            this.colFromValue,
            this.colToValue,
            this.colClientID2,
            this.colRemarks5});
            this.gridView21.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn92;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView21.FormatRules.Add(gridFormatRule3);
            this.gridView21.Name = "gridView21";
            this.gridView21.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView21.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView21.OptionsLayout.StoreAppearance = true;
            this.gridView21.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView21.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView21.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView21.OptionsView.ColumnAutoWidth = false;
            this.gridView21.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView21.OptionsView.ShowGroupPanel = false;
            this.gridView21.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView21.OptionsView.ShowIndicator = false;
            this.gridView21.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn94, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Linear Cut Length";
            this.gridColumn93.FieldName = "Description";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Visible = true;
            this.gridColumn93.VisibleIndex = 0;
            this.gridColumn93.Width = 201;
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Order";
            this.gridColumn94.FieldName = "RecordOrder";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            // 
            // colFromValue
            // 
            this.colFromValue.Caption = "From";
            this.colFromValue.ColumnEdit = this.repositoryItemTextEdit5;
            this.colFromValue.FieldName = "FromValue";
            this.colFromValue.Name = "colFromValue";
            this.colFromValue.OptionsColumn.AllowEdit = false;
            this.colFromValue.OptionsColumn.AllowFocus = false;
            this.colFromValue.OptionsColumn.ReadOnly = true;
            // 
            // colToValue
            // 
            this.colToValue.Caption = "To";
            this.colToValue.ColumnEdit = this.repositoryItemTextEdit5;
            this.colToValue.FieldName = "ToValue";
            this.colToValue.Name = "colToValue";
            this.colToValue.OptionsColumn.AllowEdit = false;
            this.colToValue.OptionsColumn.AllowFocus = false;
            this.colToValue.OptionsColumn.ReadOnly = true;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks5
            // 
            this.colRemarks5.Caption = "Remarks";
            this.colRemarks5.ColumnEdit = this.repositoryItemMemoExEdit14;
            this.colRemarks5.FieldName = "Remarks";
            this.colRemarks5.Name = "colRemarks5";
            this.colRemarks5.OptionsColumn.AllowEdit = false;
            this.colRemarks5.OptionsColumn.AllowFocus = false;
            this.colRemarks5.OptionsColumn.ReadOnly = true;
            this.colRemarks5.Visible = true;
            this.colRemarks5.VisibleIndex = 1;
            this.colRemarks5.Width = 142;
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(407, 125);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(86, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling1);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 29;
            // 
            // IvyOnPoleCheckEdit
            // 
            this.IvyOnPoleCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "IvyOnPole", true));
            this.IvyOnPoleCheckEdit.Location = new System.Drawing.Point(438, 323);
            this.IvyOnPoleCheckEdit.MenuManager = this.barManager1;
            this.IvyOnPoleCheckEdit.Name = "IvyOnPoleCheckEdit";
            this.IvyOnPoleCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IvyOnPoleCheckEdit.Properties.ValueChecked = 1;
            this.IvyOnPoleCheckEdit.Properties.ValueUnchecked = 0;
            this.IvyOnPoleCheckEdit.Size = new System.Drawing.Size(368, 19);
            this.IvyOnPoleCheckEdit.StyleController = this.dataLayoutControl1;
            this.IvyOnPoleCheckEdit.TabIndex = 16;
            // 
            // SpanResilientCheckEdit
            // 
            this.SpanResilientCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "SpanResilient", true));
            this.SpanResilientCheckEdit.Location = new System.Drawing.Point(169, 323);
            this.SpanResilientCheckEdit.MenuManager = this.barManager1;
            this.SpanResilientCheckEdit.Name = "SpanResilientCheckEdit";
            this.SpanResilientCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SpanResilientCheckEdit.Properties.ValueChecked = 1;
            this.SpanResilientCheckEdit.Properties.ValueUnchecked = 0;
            this.SpanResilientCheckEdit.Size = new System.Drawing.Size(115, 19);
            this.SpanResilientCheckEdit.StyleController = this.dataLayoutControl1;
            this.SpanResilientCheckEdit.TabIndex = 15;
            // 
            // ClearanceDistanceUnderIDGridLookUpEdit
            // 
            this.ClearanceDistanceUnderIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "ClearanceDistanceUnderID", true));
            this.ClearanceDistanceUnderIDGridLookUpEdit.Location = new System.Drawing.Point(438, 228);
            this.ClearanceDistanceUnderIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ClearanceDistanceUnderIDGridLookUpEdit.Name = "ClearanceDistanceUnderIDGridLookUpEdit";
            this.ClearanceDistanceUnderIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClearanceDistanceUnderIDGridLookUpEdit.Properties.DataSource = this.sp07356UTClearanceDistancesWithBlankBindingSource;
            this.ClearanceDistanceUnderIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ClearanceDistanceUnderIDGridLookUpEdit.Properties.NullText = "";
            this.ClearanceDistanceUnderIDGridLookUpEdit.Properties.PopupView = this.gridView11;
            this.ClearanceDistanceUnderIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ClearanceDistanceUnderIDGridLookUpEdit.Size = new System.Drawing.Size(112, 20);
            this.ClearanceDistanceUnderIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ClearanceDistanceUnderIDGridLookUpEdit.TabIndex = 8;
            this.ClearanceDistanceUnderIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ClearanceDistanceUnderIDGridLookUpEdit_EditValueChanged);
            this.ClearanceDistanceUnderIDGridLookUpEdit.Validated += new System.EventHandler(this.ClearanceDistanceUnderIDGridLookUpEdit_Validated);
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn52,
            this.gridColumn90,
            this.gridColumn91});
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.gridColumn52;
            gridFormatRule4.Name = "Format0";
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.Value1 = 0;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            this.gridView11.FormatRules.Add(gridFormatRule4);
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView11.OptionsView.ShowIndicator = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn91, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Clearance Distance";
            this.gridColumn90.FieldName = "Description";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Visible = true;
            this.gridColumn90.VisibleIndex = 0;
            this.gridColumn90.Width = 201;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Order";
            this.gridColumn91.FieldName = "RecordOrder";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            // 
            // AccessMapPathButtonEdit
            // 
            this.AccessMapPathButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "AccessMapPath", true));
            this.AccessMapPathButtonEdit.Location = new System.Drawing.Point(169, 356);
            this.AccessMapPathButtonEdit.MenuManager = this.barManager1;
            this.AccessMapPathButtonEdit.Name = "AccessMapPathButtonEdit";
            this.AccessMapPathButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to View any linked Access Map", "view", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Delete", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Clear any linked Access Map", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.AccessMapPathButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.AccessMapPathButtonEdit.Size = new System.Drawing.Size(637, 20);
            this.AccessMapPathButtonEdit.StyleController = this.dataLayoutControl1;
            this.AccessMapPathButtonEdit.TabIndex = 17;
            this.AccessMapPathButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AccessMapPathButtonEdit_ButtonClick);
            // 
            // TrafficMapPathButtonEdit
            // 
            this.TrafficMapPathButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "TrafficMapPath", true));
            this.TrafficMapPathButtonEdit.Location = new System.Drawing.Point(181, 661);
            this.TrafficMapPathButtonEdit.MenuManager = this.barManager1;
            this.TrafficMapPathButtonEdit.Name = "TrafficMapPathButtonEdit";
            this.TrafficMapPathButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to View any linked Traffic Map.", "view", superToolTip1, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to Clear linked Traffic Map", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.TrafficMapPathButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.TrafficMapPathButtonEdit.Size = new System.Drawing.Size(613, 20);
            this.TrafficMapPathButtonEdit.StyleController = this.dataLayoutControl1;
            this.TrafficMapPathButtonEdit.TabIndex = 20;
            this.TrafficMapPathButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TrafficMapPathButtonEdit_ButtonClick);
            // 
            // groupControl2
            // 
            this.groupControl2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.groupControl2.Controls.Add(this.gridControl26);
            this.groupControl2.Location = new System.Drawing.Point(31, 447);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(763, 210);
            this.groupControl2.TabIndex = 39;
            this.groupControl2.Text = "Traffic Management Requirements";
            // 
            // gridControl26
            // 
            this.gridControl26.DataSource = this.sp07367UTTrafficManagementForSurveyedPoleBindingSource;
            this.gridControl26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl26.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl26.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl26.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl26.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl26.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl26.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl26.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl26.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl26.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl26.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl26.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl26.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl26.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl26_EmbeddedNavigator_ButtonClick);
            this.gridControl26.Location = new System.Drawing.Point(23, 1);
            this.gridControl26.MainView = this.gridView26;
            this.gridControl26.MenuManager = this.barManager1;
            this.gridControl26.Name = "gridControl26";
            this.gridControl26.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit23,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemSpinEdit2});
            this.gridControl26.Size = new System.Drawing.Size(738, 207);
            this.gridControl26.TabIndex = 0;
            this.gridControl26.UseEmbeddedNavigator = true;
            this.gridControl26.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView26});
            // 
            // sp07367UTTrafficManagementForSurveyedPoleBindingSource
            // 
            this.sp07367UTTrafficManagementForSurveyedPoleBindingSource.DataMember = "sp07367_UT_Traffic_Management_For_Surveyed_Pole";
            this.sp07367UTTrafficManagementForSurveyedPoleBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView26
            // 
            this.gridView26.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTrafficManagementID,
            this.colSurveyedPoleID2,
            this.colTrafficManagementTypeID,
            this.colAmountRequired,
            this.colRemarks8});
            this.gridView26.GridControl = this.gridControl26;
            this.gridView26.Name = "gridView26";
            this.gridView26.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView26.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView26.OptionsLayout.StoreAppearance = true;
            this.gridView26.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView26.OptionsSelection.MultiSelect = true;
            this.gridView26.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView26.OptionsView.ColumnAutoWidth = false;
            this.gridView26.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView26.OptionsView.ShowGroupPanel = false;
            this.gridView26.ViewCaption = "Traffic Management Requirements";
            this.gridView26.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView26.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView26_SelectionChanged);
            this.gridView26.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView26.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView26.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView26.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView26_MouseUp);
            this.gridView26.DoubleClick += new System.EventHandler(this.gridView26_DoubleClick);
            this.gridView26.GotFocus += new System.EventHandler(this.gridView26_GotFocus);
            // 
            // colTrafficManagementID
            // 
            this.colTrafficManagementID.Caption = "TM ID";
            this.colTrafficManagementID.FieldName = "TrafficManagementID";
            this.colTrafficManagementID.Name = "colTrafficManagementID";
            this.colTrafficManagementID.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementID.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyedPoleID2
            // 
            this.colSurveyedPoleID2.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID2.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID2.Name = "colSurveyedPoleID2";
            this.colSurveyedPoleID2.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID2.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID2.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID2.Width = 104;
            // 
            // colTrafficManagementTypeID
            // 
            this.colTrafficManagementTypeID.Caption = "Traffic Management Requirement";
            this.colTrafficManagementTypeID.ColumnEdit = this.repositoryItemGridLookUpEdit1;
            this.colTrafficManagementTypeID.FieldName = "TrafficManagementTypeID";
            this.colTrafficManagementTypeID.Name = "colTrafficManagementTypeID";
            this.colTrafficManagementTypeID.Visible = true;
            this.colTrafficManagementTypeID.VisibleIndex = 0;
            this.colTrafficManagementTypeID.Width = 310;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DataSource = this.sp07366UTTrafficManagementItemsWithBlankBindingSource;
            this.repositoryItemGridLookUpEdit1.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEdit1.ValueMember = "ID";
            // 
            // sp07366UTTrafficManagementItemsWithBlankBindingSource
            // 
            this.sp07366UTTrafficManagementItemsWithBlankBindingSource.DataMember = "sp07366_UT_Traffic_Management_Items_With_Blank";
            this.sp07366UTTrafficManagementItemsWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn48;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn50, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Traffic Management Requirement";
            this.gridColumn49.FieldName = "Description";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 0;
            this.gridColumn49.Width = 240;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Order";
            this.gridColumn50.FieldName = "RecordOrder";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            // 
            // colAmountRequired
            // 
            this.colAmountRequired.Caption = "Amount";
            this.colAmountRequired.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colAmountRequired.FieldName = "AmountRequired";
            this.colAmountRequired.Name = "colAmountRequired";
            this.colAmountRequired.Visible = true;
            this.colAmountRequired.VisibleIndex = 1;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // colRemarks8
            // 
            this.colRemarks8.Caption = "Remarks";
            this.colRemarks8.ColumnEdit = this.repositoryItemMemoExEdit23;
            this.colRemarks8.FieldName = "Remarks";
            this.colRemarks8.Name = "colRemarks8";
            this.colRemarks8.Visible = true;
            this.colRemarks8.VisibleIndex = 2;
            this.colRemarks8.Width = 220;
            // 
            // repositoryItemMemoExEdit23
            // 
            this.repositoryItemMemoExEdit23.AutoHeight = false;
            this.repositoryItemMemoExEdit23.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit23.Name = "repositoryItemMemoExEdit23";
            this.repositoryItemMemoExEdit23.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit23.ShowIcon = false;
            // 
            // DeferralRemarksMemoEdit
            // 
            this.DeferralRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "DeferralRemarks", true));
            this.DeferralRemarksMemoEdit.Location = new System.Drawing.Point(181, 813);
            this.DeferralRemarksMemoEdit.MenuManager = this.barManager1;
            this.DeferralRemarksMemoEdit.Name = "DeferralRemarksMemoEdit";
            this.DeferralRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DeferralRemarksMemoEdit, true);
            this.DeferralRemarksMemoEdit.Size = new System.Drawing.Size(613, 55);
            this.scSpellChecker.SetSpellCheckerOptions(this.DeferralRemarksMemoEdit, optionsSpelling2);
            this.DeferralRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.DeferralRemarksMemoEdit.TabIndex = 25;
            // 
            // DeferralReasonIDGridLookUpEdit
            // 
            this.DeferralReasonIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "DeferralReasonID", true));
            this.DeferralReasonIDGridLookUpEdit.Location = new System.Drawing.Point(596, 789);
            this.DeferralReasonIDGridLookUpEdit.MenuManager = this.barManager1;
            this.DeferralReasonIDGridLookUpEdit.Name = "DeferralReasonIDGridLookUpEdit";
            this.DeferralReasonIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeferralReasonIDGridLookUpEdit.Properties.DataSource = this.sp07357UTDeferralReasonsWithBlankBindingSource;
            this.DeferralReasonIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.DeferralReasonIDGridLookUpEdit.Properties.NullText = "";
            this.DeferralReasonIDGridLookUpEdit.Properties.PopupView = this.gridView25;
            this.DeferralReasonIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.DeferralReasonIDGridLookUpEdit.Size = new System.Drawing.Size(198, 20);
            this.DeferralReasonIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.DeferralReasonIDGridLookUpEdit.TabIndex = 24;
            this.DeferralReasonIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DeferralReasonIDGridLookUpEdit_Validating);
            // 
            // sp07357UTDeferralReasonsWithBlankBindingSource
            // 
            this.sp07357UTDeferralReasonsWithBlankBindingSource.DataMember = "sp07357_UT_Deferral_Reasons_With_Blank";
            this.sp07357UTDeferralReasonsWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView25
            // 
            this.gridView25.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99});
            this.gridView25.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule5.ApplyToRow = true;
            gridFormatRule5.Column = this.gridColumn97;
            gridFormatRule5.Name = "Format0";
            formatConditionRuleValue5.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue5.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue5.Value1 = 0;
            gridFormatRule5.Rule = formatConditionRuleValue5;
            this.gridView25.FormatRules.Add(gridFormatRule5);
            this.gridView25.Name = "gridView25";
            this.gridView25.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView25.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView25.OptionsLayout.StoreAppearance = true;
            this.gridView25.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView25.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView25.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView25.OptionsView.ColumnAutoWidth = false;
            this.gridView25.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView25.OptionsView.ShowGroupPanel = false;
            this.gridView25.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView25.OptionsView.ShowIndicator = false;
            this.gridView25.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn99, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Deferral Reason";
            this.gridColumn98.FieldName = "Description";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Visible = true;
            this.gridColumn98.VisibleIndex = 0;
            this.gridColumn98.Width = 201;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Order";
            this.gridColumn99.FieldName = "RecordOrder";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            // 
            // ClearanceDistanceGridLookUpEdit
            // 
            this.ClearanceDistanceGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "ClearanceDistance", true));
            this.ClearanceDistanceGridLookUpEdit.Location = new System.Drawing.Point(169, 228);
            this.ClearanceDistanceGridLookUpEdit.MenuManager = this.barManager1;
            this.ClearanceDistanceGridLookUpEdit.Name = "ClearanceDistanceGridLookUpEdit";
            this.ClearanceDistanceGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClearanceDistanceGridLookUpEdit.Properties.DataSource = this.sp07356UTClearanceDistancesWithBlankBindingSource;
            this.ClearanceDistanceGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ClearanceDistanceGridLookUpEdit.Properties.NullText = "";
            this.ClearanceDistanceGridLookUpEdit.Properties.PopupView = this.gridView24;
            this.ClearanceDistanceGridLookUpEdit.Properties.ValueMember = "ID";
            this.ClearanceDistanceGridLookUpEdit.Size = new System.Drawing.Size(115, 20);
            this.ClearanceDistanceGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ClearanceDistanceGridLookUpEdit.TabIndex = 7;
            this.ClearanceDistanceGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ClearanceDistanceGridLookUpEdit_EditValueChanged);
            this.ClearanceDistanceGridLookUpEdit.Validated += new System.EventHandler(this.ClearanceDistanceGridLookUpEdit_Validated);
            // 
            // gridView24
            // 
            this.gridView24.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn68,
            this.gridColumn95,
            this.gridColumn96});
            this.gridView24.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule6.ApplyToRow = true;
            gridFormatRule6.Column = this.gridColumn68;
            gridFormatRule6.Name = "Format0";
            formatConditionRuleValue6.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue6.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue6.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue6.Value1 = 0;
            gridFormatRule6.Rule = formatConditionRuleValue6;
            this.gridView24.FormatRules.Add(gridFormatRule6);
            this.gridView24.Name = "gridView24";
            this.gridView24.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView24.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView24.OptionsLayout.StoreAppearance = true;
            this.gridView24.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView24.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView24.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView24.OptionsView.ColumnAutoWidth = false;
            this.gridView24.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView24.OptionsView.ShowGroupPanel = false;
            this.gridView24.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView24.OptionsView.ShowIndicator = false;
            this.gridView24.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn96, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Clearance Distance";
            this.gridColumn95.FieldName = "Description";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            this.gridColumn95.Visible = true;
            this.gridColumn95.VisibleIndex = 0;
            this.gridColumn95.Width = 201;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Order";
            this.gridColumn96.FieldName = "RecordOrder";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            // 
            // RemarksInformationLabel
            // 
            this.RemarksInformationLabel.AllowHtmlString = true;
            this.RemarksInformationLabel.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.RemarksInformationLabel.Appearance.ImageIndex = 2;
            this.RemarksInformationLabel.Appearance.ImageList = this.imageCollection1;
            this.RemarksInformationLabel.Appearance.Options.UseImageAlign = true;
            this.RemarksInformationLabel.Appearance.Options.UseImageIndex = true;
            this.RemarksInformationLabel.Appearance.Options.UseImageList = true;
            this.RemarksInformationLabel.Location = new System.Drawing.Point(19, 67);
            this.RemarksInformationLabel.Name = "RemarksInformationLabel";
            this.RemarksInformationLabel.Size = new System.Drawing.Size(787, 18);
            this.RemarksInformationLabel.StyleController = this.dataLayoutControl1;
            this.RemarksInformationLabel.TabIndex = 34;
            this.RemarksInformationLabel.Text = "        These Remarks are <b>not</b> shown on the Consent Form";
            // 
            // RevisitDateDateEdit
            // 
            this.RevisitDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "RevisitDate", true));
            this.RevisitDateDateEdit.EditValue = null;
            this.RevisitDateDateEdit.Location = new System.Drawing.Point(181, 789);
            this.RevisitDateDateEdit.MenuManager = this.barManager1;
            this.RevisitDateDateEdit.Name = "RevisitDateDateEdit";
            this.RevisitDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RevisitDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RevisitDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RevisitDateDateEdit.Size = new System.Drawing.Size(261, 20);
            this.RevisitDateDateEdit.StyleController = this.dataLayoutControl1;
            this.RevisitDateDateEdit.TabIndex = 23;
            this.RevisitDateDateEdit.EditValueChanged += new System.EventHandler(this.RevisitDateDateEdit_EditValueChanged);
            this.RevisitDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.RevisitDateDateEdit_Validating);
            // 
            // InvoiceNumberTextEdit
            // 
            this.InvoiceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "InvoiceNumber", true));
            this.InvoiceNumberTextEdit.Location = new System.Drawing.Point(543, 917);
            this.InvoiceNumberTextEdit.MenuManager = this.barManager1;
            this.InvoiceNumberTextEdit.Name = "InvoiceNumberTextEdit";
            this.InvoiceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.InvoiceNumberTextEdit, true);
            this.InvoiceNumberTextEdit.Size = new System.Drawing.Size(222, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.InvoiceNumberTextEdit, optionsSpelling3);
            this.InvoiceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.InvoiceNumberTextEdit.TabIndex = 23;
            // 
            // DateInvoicedDateEdit
            // 
            this.DateInvoicedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "DateInvoiced", true));
            this.DateInvoicedDateEdit.EditValue = null;
            this.DateInvoicedDateEdit.Location = new System.Drawing.Point(169, 917);
            this.DateInvoicedDateEdit.MenuManager = this.barManager1;
            this.DateInvoicedDateEdit.Name = "DateInvoicedDateEdit";
            this.DateInvoicedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateInvoicedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateInvoicedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateInvoicedDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.DateInvoicedDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateInvoicedDateEdit.Size = new System.Drawing.Size(220, 20);
            this.DateInvoicedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateInvoicedDateEdit.TabIndex = 22;
            // 
            // SpanInvoicedCheckEdit
            // 
            this.SpanInvoicedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "SpanInvoiced", true));
            this.SpanInvoicedCheckEdit.Location = new System.Drawing.Point(169, 894);
            this.SpanInvoicedCheckEdit.MenuManager = this.barManager1;
            this.SpanInvoicedCheckEdit.Name = "SpanInvoicedCheckEdit";
            this.SpanInvoicedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SpanInvoicedCheckEdit.Properties.ValueChecked = 1;
            this.SpanInvoicedCheckEdit.Properties.ValueUnchecked = 0;
            this.SpanInvoicedCheckEdit.Size = new System.Drawing.Size(596, 19);
            this.SpanInvoicedCheckEdit.StyleController = this.dataLayoutControl1;
            this.SpanInvoicedCheckEdit.TabIndex = 21;
            // 
            // IsBaseClimbableCheckEdit
            // 
            this.IsBaseClimbableCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "IsBaseClimbable", true));
            this.IsBaseClimbableCheckEdit.Location = new System.Drawing.Point(438, 300);
            this.IsBaseClimbableCheckEdit.MenuManager = this.barManager1;
            this.IsBaseClimbableCheckEdit.Name = "IsBaseClimbableCheckEdit";
            this.IsBaseClimbableCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsBaseClimbableCheckEdit.Properties.ValueChecked = 1;
            this.IsBaseClimbableCheckEdit.Properties.ValueUnchecked = 0;
            this.IsBaseClimbableCheckEdit.Size = new System.Drawing.Size(368, 19);
            this.IsBaseClimbableCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsBaseClimbableCheckEdit.TabIndex = 14;
            // 
            // DeferredUnitDescriptiorIDGridLookUpEdit
            // 
            this.DeferredUnitDescriptiorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "DeferredUnitDescriptiorID", true));
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Location = new System.Drawing.Point(596, 765);
            this.DeferredUnitDescriptiorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Name = "DeferredUnitDescriptiorIDGridLookUpEdit";
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.DataSource = this.sp07046UTInspectionCyclesWithBlankBindingSource;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.NullText = "";
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.PopupView = this.gridView15;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Size = new System.Drawing.Size(198, 20);
            this.DeferredUnitDescriptiorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.TabIndex = 22;
            this.DeferredUnitDescriptiorIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.DeferredUnitDescriptiorIDGridLookUpEdit_EditValueChanged);
            this.DeferredUnitDescriptiorIDGridLookUpEdit.Validated += new System.EventHandler(this.DeferredUnitDescriptiorIDGridLookUpEdit_Validated);
            // 
            // sp07046UTInspectionCyclesWithBlankBindingSource
            // 
            this.sp07046UTInspectionCyclesWithBlankBindingSource.DataMember = "sp07046_UT_Inspection_Cycles_With_Blank";
            this.sp07046UTInspectionCyclesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colID,
            this.colRecordOrder1});
            this.gridView15.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule7.ApplyToRow = true;
            gridFormatRule7.Column = this.colID;
            gridFormatRule7.Name = "Format0";
            formatConditionRuleValue7.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue7.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue7.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue7.Value1 = 0;
            gridFormatRule7.Rule = formatConditionRuleValue7;
            this.gridView15.FormatRules.Add(gridFormatRule7);
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView15.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView15.OptionsLayout.StoreAppearance = true;
            this.gridView15.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView15.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView15.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView15.OptionsView.ColumnAutoWidth = false;
            this.gridView15.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            this.gridView15.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView15.OptionsView.ShowIndicator = false;
            this.gridView15.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Unit Descriptor";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 162;
            // 
            // colRecordOrder1
            // 
            this.colRecordOrder1.Caption = "Order";
            this.colRecordOrder1.FieldName = "RecordOrder";
            this.colRecordOrder1.Name = "colRecordOrder1";
            this.colRecordOrder1.OptionsColumn.AllowEdit = false;
            this.colRecordOrder1.OptionsColumn.AllowFocus = false;
            this.colRecordOrder1.OptionsColumn.ReadOnly = true;
            // 
            // DeferredUnitsSpinEdit
            // 
            this.DeferredUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "DeferredUnits", true));
            this.DeferredUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DeferredUnitsSpinEdit.Location = new System.Drawing.Point(181, 765);
            this.DeferredUnitsSpinEdit.MenuManager = this.barManager1;
            this.DeferredUnitsSpinEdit.Name = "DeferredUnitsSpinEdit";
            this.DeferredUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DeferredUnitsSpinEdit.Properties.IsFloatValue = false;
            this.DeferredUnitsSpinEdit.Properties.Mask.EditMask = "N00";
            this.DeferredUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DeferredUnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.DeferredUnitsSpinEdit.Size = new System.Drawing.Size(261, 20);
            this.DeferredUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.DeferredUnitsSpinEdit.TabIndex = 21;
            this.DeferredUnitsSpinEdit.EditValueChanged += new System.EventHandler(this.DeferredUnitsSpinEdit_EditValueChanged);
            this.DeferredUnitsSpinEdit.Validated += new System.EventHandler(this.DeferredUnitsSpinEdit_Validated);
            // 
            // DeferredCheckEdit
            // 
            this.DeferredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "Deferred", true));
            this.DeferredCheckEdit.Location = new System.Drawing.Point(181, 741);
            this.DeferredCheckEdit.MenuManager = this.barManager1;
            this.DeferredCheckEdit.Name = "DeferredCheckEdit";
            this.DeferredCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DeferredCheckEdit.Properties.ValueChecked = 1;
            this.DeferredCheckEdit.Properties.ValueUnchecked = 0;
            this.DeferredCheckEdit.Size = new System.Drawing.Size(261, 19);
            this.DeferredCheckEdit.StyleController = this.dataLayoutControl1;
            this.DeferredCheckEdit.TabIndex = 20;
            this.DeferredCheckEdit.CheckedChanged += new System.EventHandler(this.DeferredCheckEdit_CheckedChanged);
            this.DeferredCheckEdit.EditValueChanged += new System.EventHandler(this.DeferredCheckEdit_EditValueChanged);
            this.DeferredCheckEdit.Validated += new System.EventHandler(this.DeferredCheckEdit_Validated);
            // 
            // G55CategoryIDGridLookUpEdit
            // 
            this.G55CategoryIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "G55CategoryID", true));
            this.G55CategoryIDGridLookUpEdit.Location = new System.Drawing.Point(169, 276);
            this.G55CategoryIDGridLookUpEdit.MenuManager = this.barManager1;
            this.G55CategoryIDGridLookUpEdit.Name = "G55CategoryIDGridLookUpEdit";
            this.G55CategoryIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.G55CategoryIDGridLookUpEdit.Properties.DataSource = this.sp07186UTG55CategoriesWithBlankBindingSource;
            this.G55CategoryIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.G55CategoryIDGridLookUpEdit.Properties.NullText = "";
            this.G55CategoryIDGridLookUpEdit.Properties.PopupView = this.gridView14;
            this.G55CategoryIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.G55CategoryIDGridLookUpEdit.Size = new System.Drawing.Size(115, 20);
            this.G55CategoryIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.G55CategoryIDGridLookUpEdit.TabIndex = 12;
            this.G55CategoryIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.G55CategoryIDGridLookUpEdit_Validating);
            // 
            // sp07186UTG55CategoriesWithBlankBindingSource
            // 
            this.sp07186UTG55CategoriesWithBlankBindingSource.DataMember = "sp07186_UT_G55_Categories_With_Blank";
            this.sp07186UTG55CategoriesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription3,
            this.colID1,
            this.colRecordOrder2});
            this.gridView14.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule8.ApplyToRow = true;
            gridFormatRule8.Column = this.colID1;
            gridFormatRule8.Name = "Format0";
            formatConditionRuleValue8.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue8.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue8.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue8.Value1 = 0;
            gridFormatRule8.Rule = formatConditionRuleValue8;
            this.gridView14.FormatRules.Add(gridFormatRule8);
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView14.OptionsView.ShowIndicator = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription3
            // 
            this.colDescription3.Caption = "G55 Category";
            this.colDescription3.FieldName = "Description";
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.OptionsColumn.AllowEdit = false;
            this.colDescription3.OptionsColumn.AllowFocus = false;
            this.colDescription3.OptionsColumn.ReadOnly = true;
            this.colDescription3.Visible = true;
            this.colDescription3.VisibleIndex = 0;
            this.colDescription3.Width = 147;
            // 
            // colRecordOrder2
            // 
            this.colRecordOrder2.Caption = "Order";
            this.colRecordOrder2.FieldName = "RecordOrder";
            this.colRecordOrder2.Name = "colRecordOrder2";
            this.colRecordOrder2.OptionsColumn.AllowEdit = false;
            this.colRecordOrder2.OptionsColumn.AllowFocus = false;
            this.colRecordOrder2.OptionsColumn.ReadOnly = true;
            // 
            // SurveyStatusIDTextEdit
            // 
            this.SurveyStatusIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "SurveyStatusID", true));
            this.SurveyStatusIDTextEdit.Location = new System.Drawing.Point(169, 274);
            this.SurveyStatusIDTextEdit.MenuManager = this.barManager1;
            this.SurveyStatusIDTextEdit.Name = "SurveyStatusIDTextEdit";
            this.SurveyStatusIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyStatusIDTextEdit, true);
            this.SurveyStatusIDTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyStatusIDTextEdit, optionsSpelling4);
            this.SurveyStatusIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyStatusIDTextEdit.TabIndex = 24;
            // 
            // SurveyStatusTextEdit
            // 
            this.SurveyStatusTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "SurveyStatus", true));
            this.SurveyStatusTextEdit.Location = new System.Drawing.Point(638, 67);
            this.SurveyStatusTextEdit.MenuManager = this.barManager1;
            this.SurveyStatusTextEdit.Name = "SurveyStatusTextEdit";
            this.SurveyStatusTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyStatusTextEdit, true);
            this.SurveyStatusTextEdit.Size = new System.Drawing.Size(168, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyStatusTextEdit, optionsSpelling5);
            this.SurveyStatusTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyStatusTextEdit.TabIndex = 1;
            // 
            // SurveyedPoleIDTextEdit
            // 
            this.SurveyedPoleIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "SurveyedPoleID", true));
            this.SurveyedPoleIDTextEdit.Location = new System.Drawing.Point(159, -23);
            this.SurveyedPoleIDTextEdit.MenuManager = this.barManager1;
            this.SurveyedPoleIDTextEdit.Name = "SurveyedPoleIDTextEdit";
            this.SurveyedPoleIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyedPoleIDTextEdit, true);
            this.SurveyedPoleIDTextEdit.Size = new System.Drawing.Size(650, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyedPoleIDTextEdit, optionsSpelling6);
            this.SurveyedPoleIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyedPoleIDTextEdit.TabIndex = 4;
            // 
            // SurveyIDTextEdit
            // 
            this.SurveyIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "SurveyID", true));
            this.SurveyIDTextEdit.Location = new System.Drawing.Point(159, 1);
            this.SurveyIDTextEdit.MenuManager = this.barManager1;
            this.SurveyIDTextEdit.Name = "SurveyIDTextEdit";
            this.SurveyIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyIDTextEdit, true);
            this.SurveyIDTextEdit.Size = new System.Drawing.Size(650, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyIDTextEdit, optionsSpelling7);
            this.SurveyIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyIDTextEdit.TabIndex = 5;
            // 
            // PoleIDTextEdit
            // 
            this.PoleIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "PoleID", true));
            this.PoleIDTextEdit.Location = new System.Drawing.Point(159, 12);
            this.PoleIDTextEdit.MenuManager = this.barManager1;
            this.PoleIDTextEdit.Name = "PoleIDTextEdit";
            this.PoleIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PoleIDTextEdit, true);
            this.PoleIDTextEdit.Size = new System.Drawing.Size(667, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PoleIDTextEdit, optionsSpelling8);
            this.PoleIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PoleIDTextEdit.TabIndex = 6;
            // 
            // FeederContractIDTextEdit
            // 
            this.FeederContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "FeederContractID", true));
            this.FeederContractIDTextEdit.Location = new System.Drawing.Point(169, 112);
            this.FeederContractIDTextEdit.MenuManager = this.barManager1;
            this.FeederContractIDTextEdit.Name = "FeederContractIDTextEdit";
            this.FeederContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FeederContractIDTextEdit, true);
            this.FeederContractIDTextEdit.Size = new System.Drawing.Size(461, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FeederContractIDTextEdit, optionsSpelling9);
            this.FeederContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.FeederContractIDTextEdit.TabIndex = 7;
            // 
            // SurveyDateDateEdit
            // 
            this.SurveyDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "SurveyDate", true));
            this.SurveyDateDateEdit.EditValue = null;
            this.SurveyDateDateEdit.Location = new System.Drawing.Point(169, 67);
            this.SurveyDateDateEdit.MenuManager = this.barManager1;
            this.SurveyDateDateEdit.Name = "SurveyDateDateEdit";
            this.SurveyDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.SurveyDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SurveyDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SurveyDateDateEdit.Properties.Mask.EditMask = "g";
            this.SurveyDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SurveyDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.SurveyDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.SurveyDateDateEdit.Properties.ReadOnly = true;
            this.SurveyDateDateEdit.Size = new System.Drawing.Size(315, 20);
            this.SurveyDateDateEdit.StyleController = this.dataLayoutControl1;
            this.SurveyDateDateEdit.TabIndex = 0;
            this.SurveyDateDateEdit.ToolTip = "Survey Date set when Finished Survey button clicked.";
            // 
            // IsSpanClearCheckEdit
            // 
            this.IsSpanClearCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "IsSpanClear", true));
            this.IsSpanClearCheckEdit.Location = new System.Drawing.Point(638, 91);
            this.IsSpanClearCheckEdit.MenuManager = this.barManager1;
            this.IsSpanClearCheckEdit.Name = "IsSpanClearCheckEdit";
            this.IsSpanClearCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsSpanClearCheckEdit.Properties.ValueChecked = 1;
            this.IsSpanClearCheckEdit.Properties.ValueUnchecked = 0;
            this.IsSpanClearCheckEdit.Size = new System.Drawing.Size(168, 19);
            this.IsSpanClearCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsSpanClearCheckEdit.TabIndex = 1;
            this.IsSpanClearCheckEdit.CheckedChanged += new System.EventHandler(this.IsSpanClearCheckEdit_CheckedChanged);
            // 
            // IsShutdownRequiredCheckEdit
            // 
            this.IsShutdownRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "IsShutdownRequired", true));
            this.IsShutdownRequiredCheckEdit.Location = new System.Drawing.Point(169, 149);
            this.IsShutdownRequiredCheckEdit.MenuManager = this.barManager1;
            this.IsShutdownRequiredCheckEdit.Name = "IsShutdownRequiredCheckEdit";
            this.IsShutdownRequiredCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsShutdownRequiredCheckEdit.Properties.ValueChecked = 1;
            this.IsShutdownRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.IsShutdownRequiredCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.IsShutdownRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsShutdownRequiredCheckEdit.TabIndex = 3;
            this.IsShutdownRequiredCheckEdit.Validated += new System.EventHandler(this.IsShutdownRequiredCheckEdit_Validated);
            // 
            // HotGloveRequiredCheckEdit
            // 
            this.HotGloveRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "HotGloveRequired", true));
            this.HotGloveRequiredCheckEdit.Location = new System.Drawing.Point(169, 172);
            this.HotGloveRequiredCheckEdit.MenuManager = this.barManager1;
            this.HotGloveRequiredCheckEdit.Name = "HotGloveRequiredCheckEdit";
            this.HotGloveRequiredCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.HotGloveRequiredCheckEdit.Properties.ValueChecked = 1;
            this.HotGloveRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.HotGloveRequiredCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.HotGloveRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.HotGloveRequiredCheckEdit.TabIndex = 5;
            // 
            // LinesmanPossibleCheckEdit
            // 
            this.LinesmanPossibleCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "LinesmanPossible", true));
            this.LinesmanPossibleCheckEdit.Location = new System.Drawing.Point(169, 195);
            this.LinesmanPossibleCheckEdit.MenuManager = this.barManager1;
            this.LinesmanPossibleCheckEdit.Name = "LinesmanPossibleCheckEdit";
            this.LinesmanPossibleCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.LinesmanPossibleCheckEdit.Properties.ValueChecked = 1;
            this.LinesmanPossibleCheckEdit.Properties.ValueUnchecked = 0;
            this.LinesmanPossibleCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.LinesmanPossibleCheckEdit.StyleController = this.dataLayoutControl1;
            this.LinesmanPossibleCheckEdit.TabIndex = 6;
            // 
            // TrafficManagementRequiredCheckEdit
            // 
            this.TrafficManagementRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "TrafficManagementRequired", true));
            this.TrafficManagementRequiredCheckEdit.Location = new System.Drawing.Point(181, 424);
            this.TrafficManagementRequiredCheckEdit.MenuManager = this.barManager1;
            this.TrafficManagementRequiredCheckEdit.Name = "TrafficManagementRequiredCheckEdit";
            this.TrafficManagementRequiredCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.TrafficManagementRequiredCheckEdit.Properties.ValueChecked = 1;
            this.TrafficManagementRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.TrafficManagementRequiredCheckEdit.Size = new System.Drawing.Size(351, 19);
            this.TrafficManagementRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.TrafficManagementRequiredCheckEdit.TabIndex = 18;
            this.TrafficManagementRequiredCheckEdit.CheckedChanged += new System.EventHandler(this.TrafficManagementRequiredCheckEdit_CheckedChanged);
            this.TrafficManagementRequiredCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TrafficManagementRequiredCheckEdit_Validating);
            // 
            // TreeWithin3MetersCheckEdit
            // 
            this.TreeWithin3MetersCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "TreeWithin3Meters", true));
            this.TreeWithin3MetersCheckEdit.Location = new System.Drawing.Point(438, 252);
            this.TreeWithin3MetersCheckEdit.MenuManager = this.barManager1;
            this.TreeWithin3MetersCheckEdit.Name = "TreeWithin3MetersCheckEdit";
            this.TreeWithin3MetersCheckEdit.Properties.Caption = "(Calculated)";
            this.TreeWithin3MetersCheckEdit.Properties.ReadOnly = true;
            this.TreeWithin3MetersCheckEdit.Properties.ValueChecked = 1;
            this.TreeWithin3MetersCheckEdit.Properties.ValueUnchecked = 0;
            this.TreeWithin3MetersCheckEdit.Size = new System.Drawing.Size(368, 19);
            this.TreeWithin3MetersCheckEdit.StyleController = this.dataLayoutControl1;
            this.TreeWithin3MetersCheckEdit.TabIndex = 11;
            // 
            // TreeClimbableCheckEdit
            // 
            this.TreeClimbableCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "TreeClimbable", true));
            this.TreeClimbableCheckEdit.Location = new System.Drawing.Point(169, 300);
            this.TreeClimbableCheckEdit.MenuManager = this.barManager1;
            this.TreeClimbableCheckEdit.Name = "TreeClimbableCheckEdit";
            this.TreeClimbableCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.TreeClimbableCheckEdit.Properties.ValueChecked = 1;
            this.TreeClimbableCheckEdit.Properties.ValueUnchecked = 0;
            this.TreeClimbableCheckEdit.Size = new System.Drawing.Size(115, 19);
            this.TreeClimbableCheckEdit.StyleController = this.dataLayoutControl1;
            this.TreeClimbableCheckEdit.TabIndex = 13;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(19, 89);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(787, 842);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling10);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 20;
            // 
            // GUIDTextEdit
            // 
            this.GUIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "GUID", true));
            this.GUIDTextEdit.Location = new System.Drawing.Point(0, 0);
            this.GUIDTextEdit.MenuManager = this.barManager1;
            this.GUIDTextEdit.Name = "GUIDTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.GUIDTextEdit, true);
            this.GUIDTextEdit.Size = new System.Drawing.Size(0, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GUIDTextEdit, optionsSpelling11);
            this.GUIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GUIDTextEdit.TabIndex = 21;
            // 
            // PoleNumberTextEdit
            // 
            this.PoleNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "PoleNumber", true));
            this.PoleNumberTextEdit.Location = new System.Drawing.Point(157, 7);
            this.PoleNumberTextEdit.MenuManager = this.barManager1;
            this.PoleNumberTextEdit.Name = "PoleNumberTextEdit";
            this.PoleNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PoleNumberTextEdit, true);
            this.PoleNumberTextEdit.Size = new System.Drawing.Size(661, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PoleNumberTextEdit, optionsSpelling12);
            this.PoleNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.PoleNumberTextEdit.TabIndex = 0;
            // 
            // InfestationRateGridLookupEdit
            // 
            this.InfestationRateGridLookupEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "InfestationRate", true));
            this.InfestationRateGridLookupEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.InfestationRateGridLookupEdit.Location = new System.Drawing.Point(169, 91);
            this.InfestationRateGridLookupEdit.MenuManager = this.barManager1;
            this.InfestationRateGridLookupEdit.Name = "InfestationRateGridLookupEdit";
            this.InfestationRateGridLookupEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InfestationRateGridLookupEdit.Properties.DataSource = this.sp07120UTSurveyInfestationRateListBindingSource;
            this.InfestationRateGridLookupEdit.Properties.DisplayMember = "Description";
            this.InfestationRateGridLookupEdit.Properties.NullText = "";
            this.InfestationRateGridLookupEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.InfestationRateGridLookupEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.InfestationRateGridLookupEdit.Properties.ValueMember = "ID";
            this.InfestationRateGridLookupEdit.Size = new System.Drawing.Size(315, 20);
            this.InfestationRateGridLookupEdit.StyleController = this.dataLayoutControl1;
            this.InfestationRateGridLookupEdit.TabIndex = 0;
            // 
            // sp07120UTSurveyInfestationRateListBindingSource
            // 
            this.sp07120UTSurveyInfestationRateListBindingSource.DataMember = "sp07120_UT_Survey_Infestation_Rate_List";
            this.sp07120UTSurveyInfestationRateListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.colRecordOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID2
            // 
            this.colID2.Caption = "Infestation ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Infestation %";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 130;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // TrafficManagementResolvedCheckEdit
            // 
            this.TrafficManagementResolvedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "TrafficManagementResolved", true));
            this.TrafficManagementResolvedCheckEdit.EditValue = null;
            this.TrafficManagementResolvedCheckEdit.Location = new System.Drawing.Point(686, 424);
            this.TrafficManagementResolvedCheckEdit.MenuManager = this.barManager1;
            this.TrafficManagementResolvedCheckEdit.Name = "TrafficManagementResolvedCheckEdit";
            this.TrafficManagementResolvedCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.TrafficManagementResolvedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.TrafficManagementResolvedCheckEdit.Properties.ValueChecked = 1;
            this.TrafficManagementResolvedCheckEdit.Properties.ValueUnchecked = 0;
            this.TrafficManagementResolvedCheckEdit.Size = new System.Drawing.Size(108, 19);
            this.TrafficManagementResolvedCheckEdit.StyleController = this.dataLayoutControl1;
            this.TrafficManagementResolvedCheckEdit.TabIndex = 19;
            // 
            // ItemForGUID
            // 
            this.ItemForGUID.Control = this.GUIDTextEdit;
            this.ItemForGUID.CustomizationFormText = "GUID";
            this.ItemForGUID.Location = new System.Drawing.Point(0, 0);
            this.ItemForGUID.Name = "ItemForGUID";
            this.ItemForGUID.Size = new System.Drawing.Size(0, 0);
            this.ItemForGUID.Text = "GUID";
            this.ItemForGUID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSurveyedPoleID
            // 
            this.ItemForSurveyedPoleID.Control = this.SurveyedPoleIDTextEdit;
            this.ItemForSurveyedPoleID.CustomizationFormText = "Surveyed Pole ID";
            this.ItemForSurveyedPoleID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSurveyedPoleID.Name = "ItemForSurveyedPoleID";
            this.ItemForSurveyedPoleID.Size = new System.Drawing.Size(801, 24);
            this.ItemForSurveyedPoleID.Text = "Surveyed Pole ID";
            this.ItemForSurveyedPoleID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSurveyID
            // 
            this.ItemForSurveyID.Control = this.SurveyIDTextEdit;
            this.ItemForSurveyID.CustomizationFormText = "Survey ID";
            this.ItemForSurveyID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSurveyID.Name = "ItemForSurveyID";
            this.ItemForSurveyID.Size = new System.Drawing.Size(801, 24);
            this.ItemForSurveyID.Text = "Survey ID";
            this.ItemForSurveyID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPoleID
            // 
            this.ItemForPoleID.Control = this.PoleIDTextEdit;
            this.ItemForPoleID.CustomizationFormText = "Pole ID";
            this.ItemForPoleID.Location = new System.Drawing.Point(0, 0);
            this.ItemForPoleID.Name = "ItemForPoleID";
            this.ItemForPoleID.Size = new System.Drawing.Size(818, 24);
            this.ItemForPoleID.Text = "Pole ID";
            this.ItemForPoleID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSurveyStatusID
            // 
            this.ItemForSurveyStatusID.Control = this.SurveyStatusIDTextEdit;
            this.ItemForSurveyStatusID.CustomizationFormText = "Pole Survey Status ID:";
            this.ItemForSurveyStatusID.Location = new System.Drawing.Point(0, 210);
            this.ItemForSurveyStatusID.Name = "ItemForSurveyStatusID";
            this.ItemForSurveyStatusID.Size = new System.Drawing.Size(632, 24);
            this.ItemForSurveyStatusID.Text = "Pole Survey Status ID:";
            this.ItemForSurveyStatusID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFeederContractID
            // 
            this.ItemForFeederContractID.Control = this.FeederContractIDTextEdit;
            this.ItemForFeederContractID.CustomizationFormText = "Feeder Contract ID";
            this.ItemForFeederContractID.Location = new System.Drawing.Point(0, 48);
            this.ItemForFeederContractID.Name = "ItemForFeederContractID";
            this.ItemForFeederContractID.Size = new System.Drawing.Size(615, 24);
            this.ItemForFeederContractID.Text = "Feeder Contract ID";
            this.ItemForFeederContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(238, 58);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(240, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(825, 950);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.ItemForPoleNumber});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(815, 940);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 24);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(815, 916);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup5});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "General Info";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForInfestationRate,
            this.ItemForHotGloveRequired,
            this.ItemForLinesmanPossible,
            this.ItemForSurveyDate,
            this.ItemForSurveyStatus,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.ItemForTreeClimbable,
            this.layoutControlGroup3,
            this.emptySpaceItem6,
            this.emptySpaceItem1,
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.ItemForAccessMapPath,
            this.emptySpaceItem9,
            this.emptySpaceItem10,
            this.emptySpaceItem11,
            this.ItemForIsSpanClear,
            this.ItemForClearanceDistance,
            this.ItemForG55CategoryID,
            this.ItemForIsBaseClimbable,
            this.ItemForTreeWithin3Meters,
            this.emptySpaceItem12,
            this.ItemForSpanResilient,
            this.ItemForIvyOnPole,
            this.ItemForHeldUpTypeID,
            this.ItemForIsShutdownRequired,
            this.emptySpaceItem13,
            this.ItemForShutdownHours,
            this.ItemForLinearCutLengthID,
            this.ItemForClearanceDistanceUnderID,
            this.ItemForClearanceDistanceAboveID});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(791, 868);
            this.layoutControlGroup4.Text = "General Info";
            // 
            // ItemForInfestationRate
            // 
            this.ItemForInfestationRate.Control = this.InfestationRateGridLookupEdit;
            this.ItemForInfestationRate.CustomizationFormText = "Infestation Rate:";
            this.ItemForInfestationRate.Location = new System.Drawing.Point(0, 24);
            this.ItemForInfestationRate.Name = "ItemForInfestationRate";
            this.ItemForInfestationRate.Size = new System.Drawing.Size(469, 24);
            this.ItemForInfestationRate.Text = "Infestation Rate:";
            this.ItemForInfestationRate.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForHotGloveRequired
            // 
            this.ItemForHotGloveRequired.Control = this.HotGloveRequiredCheckEdit;
            this.ItemForHotGloveRequired.CustomizationFormText = "Hot Glove Required:";
            this.ItemForHotGloveRequired.Location = new System.Drawing.Point(0, 105);
            this.ItemForHotGloveRequired.MaxSize = new System.Drawing.Size(238, 23);
            this.ItemForHotGloveRequired.MinSize = new System.Drawing.Size(238, 23);
            this.ItemForHotGloveRequired.Name = "ItemForHotGloveRequired";
            this.ItemForHotGloveRequired.Size = new System.Drawing.Size(238, 23);
            this.ItemForHotGloveRequired.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHotGloveRequired.Text = "Hot Glove Required:";
            this.ItemForHotGloveRequired.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForLinesmanPossible
            // 
            this.ItemForLinesmanPossible.Control = this.LinesmanPossibleCheckEdit;
            this.ItemForLinesmanPossible.CustomizationFormText = "Linesman Possible:";
            this.ItemForLinesmanPossible.Location = new System.Drawing.Point(0, 128);
            this.ItemForLinesmanPossible.MaxSize = new System.Drawing.Size(238, 23);
            this.ItemForLinesmanPossible.MinSize = new System.Drawing.Size(238, 23);
            this.ItemForLinesmanPossible.Name = "ItemForLinesmanPossible";
            this.ItemForLinesmanPossible.Size = new System.Drawing.Size(238, 23);
            this.ItemForLinesmanPossible.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLinesmanPossible.Text = "Linesman Possible:";
            this.ItemForLinesmanPossible.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForSurveyDate
            // 
            this.ItemForSurveyDate.Control = this.SurveyDateDateEdit;
            this.ItemForSurveyDate.CustomizationFormText = "Survey Date";
            this.ItemForSurveyDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForSurveyDate.Name = "ItemForSurveyDate";
            this.ItemForSurveyDate.Size = new System.Drawing.Size(469, 24);
            this.ItemForSurveyDate.Text = "Survey Date";
            this.ItemForSurveyDate.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForSurveyStatus
            // 
            this.ItemForSurveyStatus.Control = this.SurveyStatusTextEdit;
            this.ItemForSurveyStatus.CustomizationFormText = "Pole Survey Status:";
            this.ItemForSurveyStatus.Location = new System.Drawing.Point(469, 0);
            this.ItemForSurveyStatus.Name = "ItemForSurveyStatus";
            this.ItemForSurveyStatus.Size = new System.Drawing.Size(322, 24);
            this.ItemForSurveyStatus.Text = "Pole Survey Status:";
            this.ItemForSurveyStatus.TextSize = new System.Drawing.Size(147, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 151);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(791, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(791, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 630);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(791, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 313);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(791, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTreeClimbable
            // 
            this.ItemForTreeClimbable.Control = this.TreeClimbableCheckEdit;
            this.ItemForTreeClimbable.CustomizationFormText = "Tree Climbable:";
            this.ItemForTreeClimbable.Location = new System.Drawing.Point(0, 233);
            this.ItemForTreeClimbable.Name = "ItemForTreeClimbable";
            this.ItemForTreeClimbable.Size = new System.Drawing.Size(269, 23);
            this.ItemForTreeClimbable.Text = "Tree Climbable:";
            this.ItemForTreeClimbable.TextSize = new System.Drawing.Size(147, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Invoicing";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Expanded = false;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSpanInvoiced,
            this.ItemForDateInvoiced,
            this.ItemForInvoiceNumber});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 827);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(791, 31);
            this.layoutControlGroup3.Text = "Invoicing";
            // 
            // ItemForSpanInvoiced
            // 
            this.ItemForSpanInvoiced.Control = this.SpanInvoicedCheckEdit;
            this.ItemForSpanInvoiced.CustomizationFormText = "Span Invoiced:";
            this.ItemForSpanInvoiced.Location = new System.Drawing.Point(0, 0);
            this.ItemForSpanInvoiced.Name = "ItemForSpanInvoiced";
            this.ItemForSpanInvoiced.Size = new System.Drawing.Size(750, 23);
            this.ItemForSpanInvoiced.Text = "Span Invoiced:";
            this.ItemForSpanInvoiced.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForDateInvoiced
            // 
            this.ItemForDateInvoiced.Control = this.DateInvoicedDateEdit;
            this.ItemForDateInvoiced.CustomizationFormText = "Date Invoiced:";
            this.ItemForDateInvoiced.Location = new System.Drawing.Point(0, 23);
            this.ItemForDateInvoiced.Name = "ItemForDateInvoiced";
            this.ItemForDateInvoiced.Size = new System.Drawing.Size(374, 24);
            this.ItemForDateInvoiced.Text = "Date Invoiced:";
            this.ItemForDateInvoiced.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForInvoiceNumber
            // 
            this.ItemForInvoiceNumber.Control = this.InvoiceNumberTextEdit;
            this.ItemForInvoiceNumber.CustomizationFormText = "Invoice Number:";
            this.ItemForInvoiceNumber.Location = new System.Drawing.Point(374, 23);
            this.ItemForInvoiceNumber.Name = "ItemForInvoiceNumber";
            this.ItemForInvoiceNumber.Size = new System.Drawing.Size(376, 24);
            this.ItemForInvoiceNumber.Text = "Invoice Number:";
            this.ItemForInvoiceNumber.TextSize = new System.Drawing.Size(147, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 817);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(791, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 858);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(791, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Deferral";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDeferred,
            this.ItemForDeferredUnits,
            this.ItemForDeferredUnitDescriptiorID,
            this.ItemForRevisitDate,
            this.ItemForDeferralReasonID,
            this.ItemForDeferralRemarks,
            this.ItemForDeferredEstimatedCuttingHours});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 640);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(791, 177);
            this.layoutControlGroup6.Text = "Deferral";
            // 
            // ItemForDeferred
            // 
            this.ItemForDeferred.Control = this.DeferredCheckEdit;
            this.ItemForDeferred.CustomizationFormText = "Deferred:";
            this.ItemForDeferred.Location = new System.Drawing.Point(0, 0);
            this.ItemForDeferred.Name = "ItemForDeferred";
            this.ItemForDeferred.Size = new System.Drawing.Size(415, 24);
            this.ItemForDeferred.Text = "Deferred:";
            this.ItemForDeferred.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForDeferredUnits
            // 
            this.ItemForDeferredUnits.Control = this.DeferredUnitsSpinEdit;
            this.ItemForDeferredUnits.CustomizationFormText = "Deferred Units:";
            this.ItemForDeferredUnits.Location = new System.Drawing.Point(0, 24);
            this.ItemForDeferredUnits.Name = "ItemForDeferredUnits";
            this.ItemForDeferredUnits.Size = new System.Drawing.Size(415, 24);
            this.ItemForDeferredUnits.Text = "Deferred For:";
            this.ItemForDeferredUnits.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForDeferredUnitDescriptiorID
            // 
            this.ItemForDeferredUnitDescriptiorID.Control = this.DeferredUnitDescriptiorIDGridLookUpEdit;
            this.ItemForDeferredUnitDescriptiorID.CustomizationFormText = "Deferred For Descriptor:";
            this.ItemForDeferredUnitDescriptiorID.Location = new System.Drawing.Point(415, 24);
            this.ItemForDeferredUnitDescriptiorID.Name = "ItemForDeferredUnitDescriptiorID";
            this.ItemForDeferredUnitDescriptiorID.Size = new System.Drawing.Size(352, 24);
            this.ItemForDeferredUnitDescriptiorID.Text = "Deferred For Descriptor:";
            this.ItemForDeferredUnitDescriptiorID.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForRevisitDate
            // 
            this.ItemForRevisitDate.Control = this.RevisitDateDateEdit;
            this.ItemForRevisitDate.CustomizationFormText = "Revisit Date:";
            this.ItemForRevisitDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForRevisitDate.Name = "ItemForRevisitDate";
            this.ItemForRevisitDate.Size = new System.Drawing.Size(415, 24);
            this.ItemForRevisitDate.Text = "Revisit Date:";
            this.ItemForRevisitDate.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForDeferralReasonID
            // 
            this.ItemForDeferralReasonID.Control = this.DeferralReasonIDGridLookUpEdit;
            this.ItemForDeferralReasonID.CustomizationFormText = "Deferral Reason:";
            this.ItemForDeferralReasonID.Location = new System.Drawing.Point(415, 48);
            this.ItemForDeferralReasonID.Name = "ItemForDeferralReasonID";
            this.ItemForDeferralReasonID.Size = new System.Drawing.Size(352, 24);
            this.ItemForDeferralReasonID.Text = "Deferral Reason:";
            this.ItemForDeferralReasonID.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForDeferralRemarks
            // 
            this.ItemForDeferralRemarks.Control = this.DeferralRemarksMemoEdit;
            this.ItemForDeferralRemarks.CustomizationFormText = "Deferral Remarks:";
            this.ItemForDeferralRemarks.Location = new System.Drawing.Point(0, 72);
            this.ItemForDeferralRemarks.MaxSize = new System.Drawing.Size(0, 59);
            this.ItemForDeferralRemarks.MinSize = new System.Drawing.Size(164, 59);
            this.ItemForDeferralRemarks.Name = "ItemForDeferralRemarks";
            this.ItemForDeferralRemarks.Size = new System.Drawing.Size(767, 59);
            this.ItemForDeferralRemarks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDeferralRemarks.Text = "Deferral Remarks:";
            this.ItemForDeferralRemarks.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForDeferredEstimatedCuttingHours
            // 
            this.ItemForDeferredEstimatedCuttingHours.Control = this.DeferredEstimatedCuttingHoursSpinEdit;
            this.ItemForDeferredEstimatedCuttingHours.Location = new System.Drawing.Point(415, 0);
            this.ItemForDeferredEstimatedCuttingHours.Name = "ItemForDeferredEstimatedCuttingHours";
            this.ItemForDeferredEstimatedCuttingHours.Size = new System.Drawing.Size(352, 24);
            this.ItemForDeferredEstimatedCuttingHours.Text = "Estimated Cutting Hours:";
            this.ItemForDeferredEstimatedCuttingHours.TextSize = new System.Drawing.Size(147, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Traffic Management";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTrafficManagementRequired,
            this.ItemForTrafficManagementResolved,
            this.layoutControlItem2,
            this.ItemForTrafficMapPath});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 323);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(791, 307);
            this.layoutControlGroup7.Text = "Traffic Management";
            // 
            // ItemForTrafficManagementRequired
            // 
            this.ItemForTrafficManagementRequired.Control = this.TrafficManagementRequiredCheckEdit;
            this.ItemForTrafficManagementRequired.CustomizationFormText = "Traffic Management Required:";
            this.ItemForTrafficManagementRequired.Location = new System.Drawing.Point(0, 0);
            this.ItemForTrafficManagementRequired.Name = "ItemForTrafficManagementRequired";
            this.ItemForTrafficManagementRequired.Size = new System.Drawing.Size(505, 23);
            this.ItemForTrafficManagementRequired.Text = "Traffic Management Required:";
            this.ItemForTrafficManagementRequired.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForTrafficManagementResolved
            // 
            this.ItemForTrafficManagementResolved.Control = this.TrafficManagementResolvedCheckEdit;
            this.ItemForTrafficManagementResolved.CustomizationFormText = "Traffic Management Resolved:";
            this.ItemForTrafficManagementResolved.Location = new System.Drawing.Point(505, 0);
            this.ItemForTrafficManagementResolved.Name = "ItemForTrafficManagementResolved";
            this.ItemForTrafficManagementResolved.Size = new System.Drawing.Size(262, 23);
            this.ItemForTrafficManagementResolved.Text = "Traffic Management Resolved:";
            this.ItemForTrafficManagementResolved.TextSize = new System.Drawing.Size(147, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.groupControl2;
            this.layoutControlItem2.CustomizationFormText = "Traffic Management Requirements:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 214);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(254, 214);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(767, 214);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Traffic Management Requirements:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ItemForTrafficMapPath
            // 
            this.ItemForTrafficMapPath.Control = this.TrafficMapPathButtonEdit;
            this.ItemForTrafficMapPath.CustomizationFormText = "Traffic Map:";
            this.ItemForTrafficMapPath.Location = new System.Drawing.Point(0, 237);
            this.ItemForTrafficMapPath.Name = "ItemForTrafficMapPath";
            this.ItemForTrafficMapPath.Size = new System.Drawing.Size(767, 24);
            this.ItemForTrafficMapPath.Text = "Traffic Map:";
            this.ItemForTrafficMapPath.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForAccessMapPath
            // 
            this.ItemForAccessMapPath.Control = this.AccessMapPathButtonEdit;
            this.ItemForAccessMapPath.CustomizationFormText = "Access Map:";
            this.ItemForAccessMapPath.Location = new System.Drawing.Point(0, 289);
            this.ItemForAccessMapPath.Name = "ItemForAccessMapPath";
            this.ItemForAccessMapPath.Size = new System.Drawing.Size(791, 24);
            this.ItemForAccessMapPath.Text = "Access Map:";
            this.ItemForAccessMapPath.TextSize = new System.Drawing.Size(147, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(238, 105);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(553, 23);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(238, 128);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(553, 23);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 279);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(791, 10);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIsSpanClear
            // 
            this.ItemForIsSpanClear.Control = this.IsSpanClearCheckEdit;
            this.ItemForIsSpanClear.CustomizationFormText = "Is Span Clear:";
            this.ItemForIsSpanClear.Location = new System.Drawing.Point(469, 24);
            this.ItemForIsSpanClear.Name = "ItemForIsSpanClear";
            this.ItemForIsSpanClear.Size = new System.Drawing.Size(322, 24);
            this.ItemForIsSpanClear.Text = "Is Span Clear:";
            this.ItemForIsSpanClear.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForClearanceDistance
            // 
            this.ItemForClearanceDistance.Control = this.ClearanceDistanceGridLookUpEdit;
            this.ItemForClearanceDistance.CustomizationFormText = "Clearance Distance (Side):";
            this.ItemForClearanceDistance.Location = new System.Drawing.Point(0, 161);
            this.ItemForClearanceDistance.Name = "ItemForClearanceDistance";
            this.ItemForClearanceDistance.Size = new System.Drawing.Size(269, 24);
            this.ItemForClearanceDistance.Text = "Clearance Distance (Side):";
            this.ItemForClearanceDistance.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForG55CategoryID
            // 
            this.ItemForG55CategoryID.Control = this.G55CategoryIDGridLookUpEdit;
            this.ItemForG55CategoryID.CustomizationFormText = "G55/2 Category:";
            this.ItemForG55CategoryID.Location = new System.Drawing.Point(0, 209);
            this.ItemForG55CategoryID.Name = "ItemForG55CategoryID";
            this.ItemForG55CategoryID.Size = new System.Drawing.Size(269, 24);
            this.ItemForG55CategoryID.Text = "G55/2 Category:";
            this.ItemForG55CategoryID.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForIsBaseClimbable
            // 
            this.ItemForIsBaseClimbable.Control = this.IsBaseClimbableCheckEdit;
            this.ItemForIsBaseClimbable.CustomizationFormText = "Is Base Climbable:";
            this.ItemForIsBaseClimbable.Location = new System.Drawing.Point(269, 233);
            this.ItemForIsBaseClimbable.Name = "ItemForIsBaseClimbable";
            this.ItemForIsBaseClimbable.Size = new System.Drawing.Size(522, 23);
            this.ItemForIsBaseClimbable.Text = "Is Base Climbable:";
            this.ItemForIsBaseClimbable.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForTreeWithin3Meters
            // 
            this.ItemForTreeWithin3Meters.Control = this.TreeWithin3MetersCheckEdit;
            this.ItemForTreeWithin3Meters.CustomizationFormText = "Tree Within 3 Meters:";
            this.ItemForTreeWithin3Meters.Location = new System.Drawing.Point(269, 185);
            this.ItemForTreeWithin3Meters.Name = "ItemForTreeWithin3Meters";
            this.ItemForTreeWithin3Meters.Size = new System.Drawing.Size(522, 23);
            this.ItemForTreeWithin3Meters.Text = "Tree Within 3 Meters:";
            this.ItemForTreeWithin3Meters.TextSize = new System.Drawing.Size(147, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(269, 208);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(522, 25);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSpanResilient
            // 
            this.ItemForSpanResilient.Control = this.SpanResilientCheckEdit;
            this.ItemForSpanResilient.CustomizationFormText = "Is Resilient:";
            this.ItemForSpanResilient.Location = new System.Drawing.Point(0, 256);
            this.ItemForSpanResilient.Name = "ItemForSpanResilient";
            this.ItemForSpanResilient.Size = new System.Drawing.Size(269, 23);
            this.ItemForSpanResilient.Text = "Is Resilient:";
            this.ItemForSpanResilient.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForIvyOnPole
            // 
            this.ItemForIvyOnPole.Control = this.IvyOnPoleCheckEdit;
            this.ItemForIvyOnPole.CustomizationFormText = "Ivy On Pole:";
            this.ItemForIvyOnPole.Location = new System.Drawing.Point(269, 256);
            this.ItemForIvyOnPole.Name = "ItemForIvyOnPole";
            this.ItemForIvyOnPole.Size = new System.Drawing.Size(522, 23);
            this.ItemForIvyOnPole.Text = "Ivy On Pole:";
            this.ItemForIvyOnPole.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForHeldUpTypeID
            // 
            this.ItemForHeldUpTypeID.Control = this.HeldUpTypeIDGridLookUpEdit;
            this.ItemForHeldUpTypeID.Location = new System.Drawing.Point(0, 58);
            this.ItemForHeldUpTypeID.MaxSize = new System.Drawing.Size(371, 24);
            this.ItemForHeldUpTypeID.MinSize = new System.Drawing.Size(371, 24);
            this.ItemForHeldUpTypeID.Name = "ItemForHeldUpTypeID";
            this.ItemForHeldUpTypeID.Size = new System.Drawing.Size(371, 24);
            this.ItemForHeldUpTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHeldUpTypeID.Text = "Held Up Type:";
            this.ItemForHeldUpTypeID.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForIsShutdownRequired
            // 
            this.ItemForIsShutdownRequired.Control = this.IsShutdownRequiredCheckEdit;
            this.ItemForIsShutdownRequired.CustomizationFormText = "Is Shutdown Required:";
            this.ItemForIsShutdownRequired.Location = new System.Drawing.Point(0, 82);
            this.ItemForIsShutdownRequired.MaxSize = new System.Drawing.Size(238, 23);
            this.ItemForIsShutdownRequired.MinSize = new System.Drawing.Size(238, 23);
            this.ItemForIsShutdownRequired.Name = "ItemForIsShutdownRequired";
            this.ItemForIsShutdownRequired.Size = new System.Drawing.Size(371, 23);
            this.ItemForIsShutdownRequired.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIsShutdownRequired.Text = "Is Shutdown Required:";
            this.ItemForIsShutdownRequired.TextSize = new System.Drawing.Size(147, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(371, 58);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(420, 23);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForShutdownHours
            // 
            this.ItemForShutdownHours.Control = this.ShutdownHoursSpinEdit;
            this.ItemForShutdownHours.Location = new System.Drawing.Point(371, 81);
            this.ItemForShutdownHours.Name = "ItemForShutdownHours";
            this.ItemForShutdownHours.Size = new System.Drawing.Size(420, 24);
            this.ItemForShutdownHours.Text = "Shutdown Hours:";
            this.ItemForShutdownHours.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForLinearCutLengthID
            // 
            this.ItemForLinearCutLengthID.Control = this.LinearCutLengthIDGridLookUpEdit;
            this.ItemForLinearCutLengthID.Location = new System.Drawing.Point(0, 185);
            this.ItemForLinearCutLengthID.Name = "ItemForLinearCutLengthID";
            this.ItemForLinearCutLengthID.Size = new System.Drawing.Size(269, 24);
            this.ItemForLinearCutLengthID.Text = "Linear Cut Length:";
            this.ItemForLinearCutLengthID.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForClearanceDistanceUnderID
            // 
            this.ItemForClearanceDistanceUnderID.Control = this.ClearanceDistanceUnderIDGridLookUpEdit;
            this.ItemForClearanceDistanceUnderID.CustomizationFormText = "Clearance Distance (Under):";
            this.ItemForClearanceDistanceUnderID.Location = new System.Drawing.Point(269, 161);
            this.ItemForClearanceDistanceUnderID.Name = "ItemForClearanceDistanceUnderID";
            this.ItemForClearanceDistanceUnderID.Size = new System.Drawing.Size(266, 24);
            this.ItemForClearanceDistanceUnderID.Text = "Clearance Distance (Under):";
            this.ItemForClearanceDistanceUnderID.TextSize = new System.Drawing.Size(147, 13);
            // 
            // ItemForClearanceDistanceAboveID
            // 
            this.ItemForClearanceDistanceAboveID.Control = this.ClearanceDistanceAboveIDFGridLookUpEdit;
            this.ItemForClearanceDistanceAboveID.Location = new System.Drawing.Point(535, 161);
            this.ItemForClearanceDistanceAboveID.Name = "ItemForClearanceDistanceAboveID";
            this.ItemForClearanceDistanceAboveID.Size = new System.Drawing.Size(256, 24);
            this.ItemForClearanceDistanceAboveID.Text = "Clearance Distance (Above):";
            this.ItemForClearanceDistanceAboveID.TextSize = new System.Drawing.Size(147, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImageOptions.Image")));
            this.layoutControlGroup5.CustomizationFormText = "Remarks";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks,
            this.layoutControlItem1});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(791, 868);
            this.layoutControlGroup5.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 22);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(791, 846);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.RemarksInformationLabel;
            this.layoutControlItem1.CustomizationFormText = "Remarks Information:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(430, 22);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(791, 22);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Remarks Information:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForPoleNumber
            // 
            this.ItemForPoleNumber.Control = this.PoleNumberTextEdit;
            this.ItemForPoleNumber.CustomizationFormText = "Pole Number:";
            this.ItemForPoleNumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForPoleNumber.Name = "ItemForPoleNumber";
            this.ItemForPoleNumber.Size = new System.Drawing.Size(815, 24);
            this.ItemForPoleNumber.Text = "Pole Number:";
            this.ItemForPoleNumber.TextSize = new System.Drawing.Size(147, 13);
            // 
            // xtraTabPageTrees
            // 
            this.xtraTabPageTrees.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageTrees.Controls.Add(this.panelControl5);
            this.xtraTabPageTrees.Name = "xtraTabPageTrees";
            this.xtraTabPageTrees.Size = new System.Drawing.Size(1087, 464);
            this.xtraTabPageTrees.Text = "Add Trees, Work and Defects";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl2.Location = new System.Drawing.Point(4, 42);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControl6);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1080, 419);
            this.splitContainerControl2.SplitterPosition = 512;
            this.splitContainerControl2.TabIndex = 22;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControl3);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "Trees Linked to Surveyed Pole";
            this.splitContainerControl3.Panel2.Controls.Add(this.xtraTabControl3);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(512, 419);
            this.splitContainerControl3.SplitterPosition = 203;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp07121UTSurveyedPoleLinkedTreesBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2DPMetres,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemCheckEdit6});
            this.gridControl3.Size = new System.Drawing.Size(508, 186);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp07121UTSurveyedPoleLinkedTreesBindingSource
            // 
            this.sp07121UTSurveyedPoleLinkedTreesBindingSource.DataMember = "sp07121_UT_Surveyed_Pole_Linked_Trees";
            this.sp07121UTSurveyedPoleLinkedTreesBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeID1,
            this.colPoleID1,
            this.colTypeID,
            this.colReferenceNumber,
            this.colStatusID1,
            this.colSizeBandID,
            this.colX1,
            this.colY1,
            this.colXYPairs,
            this.colLatitude1,
            this.colLongitude1,
            this.colLatLongPairs,
            this.colArea,
            this.colLength,
            this.colRemarks3,
            this.colGUID2,
            this.colMapID1,
            this.colCircuitID1,
            this.colPoleNumber2,
            this.colSizeBand,
            this.colStatus1,
            this.colObjectType,
            this.colSurveyedTreeID1,
            this.colTPONumber,
            this.colG55CategoryDescription,
            this.colG55CategoryID,
            this.colGrowthRate,
            this.colDeadTree});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView3_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colTreeID1
            // 
            this.colTreeID1.Caption = "Tree ID";
            this.colTreeID1.FieldName = "TreeID";
            this.colTreeID1.Name = "colTreeID1";
            this.colTreeID1.OptionsColumn.AllowEdit = false;
            this.colTreeID1.OptionsColumn.AllowFocus = false;
            this.colTreeID1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID1
            // 
            this.colPoleID1.Caption = "Pole ID";
            this.colPoleID1.FieldName = "PoleID";
            this.colPoleID1.Name = "colPoleID1";
            this.colPoleID1.OptionsColumn.AllowEdit = false;
            this.colPoleID1.OptionsColumn.AllowFocus = false;
            this.colPoleID1.OptionsColumn.ReadOnly = true;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 0;
            this.colReferenceNumber.Width = 128;
            // 
            // colStatusID1
            // 
            this.colStatusID1.Caption = "Status ID";
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            // 
            // colSizeBandID
            // 
            this.colSizeBandID.Caption = "Size Band ID";
            this.colSizeBandID.FieldName = "SizeBandID";
            this.colSizeBandID.Name = "colSizeBandID";
            this.colSizeBandID.OptionsColumn.AllowEdit = false;
            this.colSizeBandID.OptionsColumn.AllowFocus = false;
            this.colSizeBandID.OptionsColumn.ReadOnly = true;
            this.colSizeBandID.Width = 81;
            // 
            // colX1
            // 
            this.colX1.Caption = "X Coordinate";
            this.colX1.FieldName = "X";
            this.colX1.Name = "colX1";
            this.colX1.OptionsColumn.AllowEdit = false;
            this.colX1.OptionsColumn.AllowFocus = false;
            this.colX1.OptionsColumn.ReadOnly = true;
            this.colX1.Width = 83;
            // 
            // colY1
            // 
            this.colY1.Caption = "Y Coordinate";
            this.colY1.FieldName = "Y";
            this.colY1.Name = "colY1";
            this.colY1.OptionsColumn.AllowEdit = false;
            this.colY1.OptionsColumn.AllowFocus = false;
            this.colY1.OptionsColumn.ReadOnly = true;
            this.colY1.Width = 83;
            // 
            // colXYPairs
            // 
            this.colXYPairs.Caption = "X \\ Y Pairs";
            this.colXYPairs.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colXYPairs.FieldName = "XYPairs";
            this.colXYPairs.Name = "colXYPairs";
            this.colXYPairs.OptionsColumn.AllowEdit = false;
            this.colXYPairs.OptionsColumn.AllowFocus = false;
            this.colXYPairs.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colLatitude1
            // 
            this.colLatitude1.Caption = "Latitude";
            this.colLatitude1.FieldName = "Latitude";
            this.colLatitude1.Name = "colLatitude1";
            this.colLatitude1.OptionsColumn.AllowEdit = false;
            this.colLatitude1.OptionsColumn.AllowFocus = false;
            this.colLatitude1.OptionsColumn.ReadOnly = true;
            // 
            // colLongitude1
            // 
            this.colLongitude1.Caption = "Longitude";
            this.colLongitude1.FieldName = "Longitude";
            this.colLongitude1.Name = "colLongitude1";
            this.colLongitude1.OptionsColumn.AllowEdit = false;
            this.colLongitude1.OptionsColumn.AllowFocus = false;
            this.colLongitude1.OptionsColumn.ReadOnly = true;
            // 
            // colLatLongPairs
            // 
            this.colLatLongPairs.Caption = "Lat \\ Long Pairs";
            this.colLatLongPairs.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colLatLongPairs.FieldName = "LatLongPairs";
            this.colLatLongPairs.Name = "colLatLongPairs";
            this.colLatLongPairs.OptionsColumn.AllowEdit = false;
            this.colLatLongPairs.OptionsColumn.AllowFocus = false;
            this.colLatLongPairs.OptionsColumn.ReadOnly = true;
            this.colLatLongPairs.Width = 95;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area";
            this.colArea.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            this.colArea.Visible = true;
            this.colArea.VisibleIndex = 6;
            this.colArea.Width = 60;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colLength
            // 
            this.colLength.Caption = "Length";
            this.colLength.ColumnEdit = this.repositoryItemTextEdit2DPMetres;
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.OptionsColumn.AllowEdit = false;
            this.colLength.OptionsColumn.AllowFocus = false;
            this.colLength.OptionsColumn.ReadOnly = true;
            this.colLength.Visible = true;
            this.colLength.VisibleIndex = 7;
            // 
            // repositoryItemTextEdit2DPMetres
            // 
            this.repositoryItemTextEdit2DPMetres.AutoHeight = false;
            this.repositoryItemTextEdit2DPMetres.Mask.EditMask = "#######0.00 Metres";
            this.repositoryItemTextEdit2DPMetres.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DPMetres.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DPMetres.Name = "repositoryItemTextEdit2DPMetres";
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 10;
            this.colRemarks3.Width = 70;
            // 
            // colGUID2
            // 
            this.colGUID2.Caption = "GUID";
            this.colGUID2.FieldName = "GUID";
            this.colGUID2.Name = "colGUID2";
            this.colGUID2.OptionsColumn.AllowEdit = false;
            this.colGUID2.OptionsColumn.AllowFocus = false;
            this.colGUID2.OptionsColumn.ReadOnly = true;
            // 
            // colMapID1
            // 
            this.colMapID1.Caption = "Map ID";
            this.colMapID1.FieldName = "MapID";
            this.colMapID1.Name = "colMapID1";
            this.colMapID1.OptionsColumn.AllowEdit = false;
            this.colMapID1.OptionsColumn.AllowFocus = false;
            this.colMapID1.OptionsColumn.ReadOnly = true;
            // 
            // colCircuitID1
            // 
            this.colCircuitID1.Caption = "Circuit ID";
            this.colCircuitID1.FieldName = "CircuitID";
            this.colCircuitID1.Name = "colCircuitID1";
            this.colCircuitID1.OptionsColumn.AllowEdit = false;
            this.colCircuitID1.OptionsColumn.AllowFocus = false;
            this.colCircuitID1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleNumber2
            // 
            this.colPoleNumber2.Caption = "Pole Number";
            this.colPoleNumber2.FieldName = "PoleNumber";
            this.colPoleNumber2.Name = "colPoleNumber2";
            this.colPoleNumber2.OptionsColumn.AllowEdit = false;
            this.colPoleNumber2.OptionsColumn.AllowFocus = false;
            this.colPoleNumber2.OptionsColumn.ReadOnly = true;
            this.colPoleNumber2.Width = 104;
            // 
            // colSizeBand
            // 
            this.colSizeBand.Caption = "Size Band";
            this.colSizeBand.FieldName = "SizeBand";
            this.colSizeBand.Name = "colSizeBand";
            this.colSizeBand.OptionsColumn.AllowEdit = false;
            this.colSizeBand.OptionsColumn.AllowFocus = false;
            this.colSizeBand.OptionsColumn.ReadOnly = true;
            this.colSizeBand.Visible = true;
            this.colSizeBand.VisibleIndex = 1;
            this.colSizeBand.Width = 86;
            // 
            // colStatus1
            // 
            this.colStatus1.Caption = "Status";
            this.colStatus1.FieldName = "Status";
            this.colStatus1.Name = "colStatus1";
            this.colStatus1.OptionsColumn.AllowEdit = false;
            this.colStatus1.OptionsColumn.AllowFocus = false;
            this.colStatus1.OptionsColumn.ReadOnly = true;
            this.colStatus1.Visible = true;
            this.colStatus1.VisibleIndex = 3;
            this.colStatus1.Width = 70;
            // 
            // colObjectType
            // 
            this.colObjectType.Caption = "Object Type";
            this.colObjectType.FieldName = "ObjectType";
            this.colObjectType.Name = "colObjectType";
            this.colObjectType.OptionsColumn.AllowEdit = false;
            this.colObjectType.OptionsColumn.AllowFocus = false;
            this.colObjectType.OptionsColumn.ReadOnly = true;
            this.colObjectType.Visible = true;
            this.colObjectType.VisibleIndex = 5;
            this.colObjectType.Width = 80;
            // 
            // colSurveyedTreeID1
            // 
            this.colSurveyedTreeID1.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID1.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID1.Name = "colSurveyedTreeID1";
            this.colSurveyedTreeID1.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID1.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID1.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID1.Width = 106;
            // 
            // colTPONumber
            // 
            this.colTPONumber.Caption = "TPO Number";
            this.colTPONumber.FieldName = "TPONumber";
            this.colTPONumber.Name = "colTPONumber";
            this.colTPONumber.OptionsColumn.AllowEdit = false;
            this.colTPONumber.OptionsColumn.AllowFocus = false;
            this.colTPONumber.OptionsColumn.ReadOnly = true;
            this.colTPONumber.Visible = true;
            this.colTPONumber.VisibleIndex = 8;
            this.colTPONumber.Width = 99;
            // 
            // colG55CategoryDescription
            // 
            this.colG55CategoryDescription.Caption = "G55 Category";
            this.colG55CategoryDescription.FieldName = "G55CategoryDescription";
            this.colG55CategoryDescription.Name = "colG55CategoryDescription";
            this.colG55CategoryDescription.OptionsColumn.AllowEdit = false;
            this.colG55CategoryDescription.OptionsColumn.AllowFocus = false;
            this.colG55CategoryDescription.OptionsColumn.ReadOnly = true;
            this.colG55CategoryDescription.Visible = true;
            this.colG55CategoryDescription.VisibleIndex = 9;
            this.colG55CategoryDescription.Width = 88;
            // 
            // colG55CategoryID
            // 
            this.colG55CategoryID.Caption = "G55 Category ID";
            this.colG55CategoryID.FieldName = "G55CategoryID";
            this.colG55CategoryID.Name = "colG55CategoryID";
            this.colG55CategoryID.OptionsColumn.AllowEdit = false;
            this.colG55CategoryID.OptionsColumn.AllowFocus = false;
            this.colG55CategoryID.OptionsColumn.ReadOnly = true;
            this.colG55CategoryID.Width = 102;
            // 
            // colGrowthRate
            // 
            this.colGrowthRate.Caption = "Growth Rate";
            this.colGrowthRate.FieldName = "GrowthRate";
            this.colGrowthRate.Name = "colGrowthRate";
            this.colGrowthRate.OptionsColumn.AllowEdit = false;
            this.colGrowthRate.OptionsColumn.AllowFocus = false;
            this.colGrowthRate.OptionsColumn.ReadOnly = true;
            this.colGrowthRate.Visible = true;
            this.colGrowthRate.VisibleIndex = 2;
            this.colGrowthRate.Width = 82;
            // 
            // colDeadTree
            // 
            this.colDeadTree.Caption = "Dead Tree";
            this.colDeadTree.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colDeadTree.FieldName = "DeadTree";
            this.colDeadTree.Name = "colDeadTree";
            this.colDeadTree.OptionsColumn.AllowEdit = false;
            this.colDeadTree.OptionsColumn.AllowFocus = false;
            this.colDeadTree.OptionsColumn.ReadOnly = true;
            this.colDeadTree.Visible = true;
            this.colDeadTree.VisibleIndex = 4;
            this.colDeadTree.Width = 71;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // xtraTabControl3
            // 
            this.xtraTabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl3.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl3.Name = "xtraTabControl3";
            this.xtraTabControl3.SelectedTabPage = this.xtraTabPage9;
            this.xtraTabControl3.Size = new System.Drawing.Size(512, 203);
            this.xtraTabControl3.TabIndex = 0;
            this.xtraTabControl3.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage9,
            this.xtraTabPage10});
            // 
            // xtraTabPage9
            // 
            this.xtraTabPage9.Controls.Add(this.gridControl4);
            this.xtraTabPage9.Name = "xtraTabPage9";
            this.xtraTabPage9.Size = new System.Drawing.Size(507, 177);
            this.xtraTabPage9.Text = "Tree Species";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp07122UTSurveyedPoleSpeciesLinkedToTreeBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditNumeric0DP});
            this.gridControl4.Size = new System.Drawing.Size(507, 177);
            this.gridControl4.TabIndex = 2;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07122UTSurveyedPoleSpeciesLinkedToTreeBindingSource
            // 
            this.sp07122UTSurveyedPoleSpeciesLinkedToTreeBindingSource.DataMember = "sp07122_UT_Surveyed_Pole_Species_Linked_To_Tree";
            this.sp07122UTSurveyedPoleSpeciesLinkedToTreeBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeSpeciesID,
            this.colTreeID,
            this.colSpeciesID,
            this.colNumberOfTrees,
            this.colPercentage,
            this.colRemarks2,
            this.colGUID1,
            this.colSpecies,
            this.colTreeReferenceNumber,
            this.colPoleNumber1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpecies, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView4_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colTreeSpeciesID
            // 
            this.colTreeSpeciesID.Caption = "Tree Species ID";
            this.colTreeSpeciesID.FieldName = "TreeSpeciesID";
            this.colTreeSpeciesID.Name = "colTreeSpeciesID";
            this.colTreeSpeciesID.OptionsColumn.AllowEdit = false;
            this.colTreeSpeciesID.OptionsColumn.AllowFocus = false;
            this.colTreeSpeciesID.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesID.Width = 96;
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colSpeciesID
            // 
            this.colSpeciesID.Caption = "Species ID";
            this.colSpeciesID.FieldName = "SpeciesID";
            this.colSpeciesID.Name = "colSpeciesID";
            this.colSpeciesID.OptionsColumn.AllowEdit = false;
            this.colSpeciesID.OptionsColumn.AllowFocus = false;
            this.colSpeciesID.OptionsColumn.ReadOnly = true;
            // 
            // colNumberOfTrees
            // 
            this.colNumberOfTrees.Caption = "Number of Trees";
            this.colNumberOfTrees.ColumnEdit = this.repositoryItemTextEditNumeric0DP;
            this.colNumberOfTrees.FieldName = "NumberOfTrees";
            this.colNumberOfTrees.Name = "colNumberOfTrees";
            this.colNumberOfTrees.OptionsColumn.AllowEdit = false;
            this.colNumberOfTrees.OptionsColumn.AllowFocus = false;
            this.colNumberOfTrees.OptionsColumn.ReadOnly = true;
            this.colNumberOfTrees.Visible = true;
            this.colNumberOfTrees.VisibleIndex = 1;
            this.colNumberOfTrees.Width = 101;
            // 
            // repositoryItemTextEditNumeric0DP
            // 
            this.repositoryItemTextEditNumeric0DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric0DP.Mask.EditMask = "n0";
            this.repositoryItemTextEditNumeric0DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric0DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric0DP.Name = "repositoryItemTextEditNumeric0DP";
            // 
            // colPercentage
            // 
            this.colPercentage.Caption = "Percentage";
            this.colPercentage.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colPercentage.FieldName = "Percentage";
            this.colPercentage.Name = "colPercentage";
            this.colPercentage.OptionsColumn.AllowEdit = false;
            this.colPercentage.OptionsColumn.AllowFocus = false;
            this.colPercentage.OptionsColumn.ReadOnly = true;
            this.colPercentage.Visible = true;
            this.colPercentage.VisibleIndex = 2;
            this.colPercentage.Width = 76;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.AllowEdit = false;
            this.colRemarks2.OptionsColumn.AllowFocus = false;
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 3;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colSpecies
            // 
            this.colSpecies.Caption = "Species";
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.OptionsColumn.AllowEdit = false;
            this.colSpecies.OptionsColumn.AllowFocus = false;
            this.colSpecies.OptionsColumn.ReadOnly = true;
            this.colSpecies.Visible = true;
            this.colSpecies.VisibleIndex = 0;
            this.colSpecies.Width = 157;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Tree Reference";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 0;
            this.colTreeReferenceNumber.Width = 132;
            // 
            // colPoleNumber1
            // 
            this.colPoleNumber1.Caption = "Pole Number";
            this.colPoleNumber1.FieldName = "PoleNumber";
            this.colPoleNumber1.Name = "colPoleNumber1";
            this.colPoleNumber1.OptionsColumn.AllowEdit = false;
            this.colPoleNumber1.OptionsColumn.AllowFocus = false;
            this.colPoleNumber1.OptionsColumn.ReadOnly = true;
            this.colPoleNumber1.Width = 118;
            // 
            // xtraTabPage10
            // 
            this.xtraTabPage10.Controls.Add(this.gridControl5);
            this.xtraTabPage10.Name = "xtraTabPage10";
            this.xtraTabPage10.Size = new System.Drawing.Size(507, 177);
            this.xtraTabPage10.Text = "Tree Pictures";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07123UTSurveyedPoleTreePicturesListBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime,
            this.repositoryItemMemoExEdit5,
            this.repositoryItemHyperLinkEdit3});
            this.gridControl5.Size = new System.Drawing.Size(507, 177);
            this.gridControl5.TabIndex = 2;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07123UTSurveyedPoleTreePicturesListBindingSource
            // 
            this.sp07123UTSurveyedPoleTreePicturesListBindingSource.DataMember = "sp07123_UT_Surveyed_Pole_Tree_Pictures_List";
            this.sp07123UTSurveyedPoleTreePicturesListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn21, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn17, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView5_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Survey Picture ID";
            this.gridColumn12.FieldName = "SurveyPictureID";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 105;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Linked To Record ID";
            this.gridColumn13.FieldName = "LinkedToRecordID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 117;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Linked To Record Type ID";
            this.gridColumn14.FieldName = "LinkedToRecordTypeID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 144;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Picture Type ID";
            this.gridColumn15.FieldName = "PictureTypeID";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 95;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Picture Path";
            this.gridColumn16.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.gridColumn16.FieldName = "PicturePath";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 2;
            this.gridColumn16.Width = 296;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Date Taken";
            this.gridColumn17.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.gridColumn17.FieldName = "DateTimeTaken";
            this.gridColumn17.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 99;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Added By Staff ID";
            this.gridColumn18.FieldName = "AddedByStaffID";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 108;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "GUID";
            this.gridColumn19.FieldName = "GUID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Remarks";
            this.gridColumn20.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.gridColumn20.FieldName = "Remarks";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 3;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Linked To Tree";
            this.gridColumn21.FieldName = "LinkedRecordDescription";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 4;
            this.gridColumn21.Width = 641;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Added By";
            this.gridColumn22.FieldName = "AddedByStaffName";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 1;
            this.gridColumn22.Width = 108;
            // 
            // xtraTabControl6
            // 
            this.xtraTabControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl6.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl6.Name = "xtraTabControl6";
            this.xtraTabControl6.SelectedTabPage = this.xtraTabPageTreeWork;
            this.xtraTabControl6.Size = new System.Drawing.Size(562, 419);
            this.xtraTabControl6.TabIndex = 4;
            this.xtraTabControl6.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageTreeWork,
            this.xtraTabPageTreeDefects});
            // 
            // xtraTabPageTreeWork
            // 
            this.xtraTabPageTreeWork.Controls.Add(this.xtraTabControl5);
            this.xtraTabPageTreeWork.Name = "xtraTabPageTreeWork";
            this.xtraTabPageTreeWork.Size = new System.Drawing.Size(557, 393);
            this.xtraTabPageTreeWork.Text = "Tree Work";
            // 
            // xtraTabControl5
            // 
            this.xtraTabControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl5.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Right;
            this.xtraTabControl5.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl5.Name = "xtraTabControl5";
            this.xtraTabControl5.SelectedTabPage = this.xtraTabPage14;
            this.xtraTabControl5.Size = new System.Drawing.Size(557, 393);
            this.xtraTabControl5.TabIndex = 3;
            this.xtraTabControl5.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage14,
            this.xtraTabPage15});
            // 
            // xtraTabPage14
            // 
            this.xtraTabPage14.Controls.Add(this.splitContainerControl4);
            this.xtraTabPage14.Name = "xtraTabPage14";
            this.xtraTabPage14.Size = new System.Drawing.Size(530, 388);
            this.xtraTabPage14.Text = "This Survey";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.gridControl6);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.xtraTabControl4);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(530, 388);
            this.splitContainerControl4.SplitterPosition = 194;
            this.splitContainerControl4.TabIndex = 3;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp07127UTSurveyedPoleWorkListBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Block Add Records", "blockadd"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "Copy Actions into Memory", "copy"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 12, true, true, "Paste Actions From Memory", "paste")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDate,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditNumericHours,
            this.repositoryItemTextEditCost,
            this.repositoryItemMemoExEdit6,
            this.repositoryItemTextEditMeters,
            this.repositoryItemTextEditShortDate});
            this.gridControl6.Size = new System.Drawing.Size(530, 188);
            this.gridControl6.TabIndex = 2;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07127UTSurveyedPoleWorkListBindingSource
            // 
            this.sp07127UTSurveyedPoleWorkListBindingSource.DataMember = "sp07127_UT_Surveyed_Pole_Work_List";
            this.sp07127UTSurveyedPoleWorkListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colSurveyedTreeID,
            this.colJobTypeID,
            this.colReferenceNumber1,
            this.colDateRaised,
            this.colDateScheduled,
            this.colDateCompleted,
            this.colApproved,
            this.colApprovedByStaffID,
            this.colEstimatedLabourCost,
            this.colActualLabourCost,
            this.colEstimatedMaterialsCost,
            this.colActualMaterialsCost,
            this.colEstimatedEquipmentCost,
            this.colActualEquipmentCost,
            this.colEstimatedTotalCost,
            this.colActualTotalCost,
            this.colWorkOrderID,
            this.colSelfBillingInvoiceID,
            this.colFinanceSystemBillingID,
            this.colRemarks1,
            this.colGUID3,
            this.colLinkedRecordDescription1,
            this.colJobDescription,
            this.colEstimatedTotalSell,
            this.colActualTotalSell,
            this.colEstimatedLabourSell,
            this.colActualLabourSell,
            this.colEstimatedEquipmentSell,
            this.colActualEquipmentSell,
            this.colEstimatedMaterialsSell,
            this.colActualMaterialsSell,
            this.colAchievableClearance,
            this.colPossibleFlail,
            this.colRevisitDate,
            this.colActualHours1,
            this.colEstimatedHours1,
            this.colPossibleLiveWork,
            this.colClientID1,
            this.colDataEntryScreenName1,
            this.colReportLayoutName1,
            this.colMapID,
            this.colCreatedByStaffID});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 1;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView6_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyedTreeID
            // 
            this.colSurveyedTreeID.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID.Name = "colSurveyedTreeID";
            this.colSurveyedTreeID.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID.Width = 106;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colReferenceNumber1
            // 
            this.colReferenceNumber1.Caption = "Reference Number";
            this.colReferenceNumber1.FieldName = "ReferenceNumber";
            this.colReferenceNumber1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colReferenceNumber1.Name = "colReferenceNumber1";
            this.colReferenceNumber1.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber1.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber1.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber1.Visible = true;
            this.colReferenceNumber1.VisibleIndex = 1;
            this.colReferenceNumber1.Width = 124;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDate;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 2;
            this.colDateRaised.Width = 101;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "g";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colDateScheduled
            // 
            this.colDateScheduled.Caption = "Date Scheduled";
            this.colDateScheduled.ColumnEdit = this.repositoryItemTextEditDate;
            this.colDateScheduled.FieldName = "DateScheduled";
            this.colDateScheduled.Name = "colDateScheduled";
            this.colDateScheduled.OptionsColumn.AllowEdit = false;
            this.colDateScheduled.OptionsColumn.AllowFocus = false;
            this.colDateScheduled.OptionsColumn.ReadOnly = true;
            this.colDateScheduled.Visible = true;
            this.colDateScheduled.VisibleIndex = 3;
            this.colDateScheduled.Width = 96;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemTextEditDate;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 4;
            this.colDateCompleted.Width = 98;
            // 
            // colApproved
            // 
            this.colApproved.Caption = "Approved";
            this.colApproved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colApproved.FieldName = "Approved";
            this.colApproved.Name = "colApproved";
            this.colApproved.OptionsColumn.AllowEdit = false;
            this.colApproved.OptionsColumn.AllowFocus = false;
            this.colApproved.OptionsColumn.ReadOnly = true;
            this.colApproved.Visible = true;
            this.colApproved.VisibleIndex = 5;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colApprovedByStaffID
            // 
            this.colApprovedByStaffID.Caption = "Apporved By Staff ID";
            this.colApprovedByStaffID.FieldName = "ApprovedByStaffID";
            this.colApprovedByStaffID.Name = "colApprovedByStaffID";
            this.colApprovedByStaffID.OptionsColumn.AllowEdit = false;
            this.colApprovedByStaffID.OptionsColumn.AllowFocus = false;
            this.colApprovedByStaffID.OptionsColumn.ReadOnly = true;
            this.colApprovedByStaffID.Width = 124;
            // 
            // colEstimatedLabourCost
            // 
            this.colEstimatedLabourCost.Caption = "Est. Labour Cost";
            this.colEstimatedLabourCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedLabourCost.FieldName = "EstimatedLabourCost";
            this.colEstimatedLabourCost.Name = "colEstimatedLabourCost";
            this.colEstimatedLabourCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourCost.Visible = true;
            this.colEstimatedLabourCost.VisibleIndex = 15;
            this.colEstimatedLabourCost.Width = 101;
            // 
            // repositoryItemTextEditCost
            // 
            this.repositoryItemTextEditCost.AutoHeight = false;
            this.repositoryItemTextEditCost.Mask.EditMask = "c";
            this.repositoryItemTextEditCost.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCost.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCost.Name = "repositoryItemTextEditCost";
            // 
            // colActualLabourCost
            // 
            this.colActualLabourCost.Caption = "Act. Labour Cost";
            this.colActualLabourCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualLabourCost.FieldName = "ActualLabourCost";
            this.colActualLabourCost.Name = "colActualLabourCost";
            this.colActualLabourCost.OptionsColumn.AllowEdit = false;
            this.colActualLabourCost.OptionsColumn.AllowFocus = false;
            this.colActualLabourCost.OptionsColumn.ReadOnly = true;
            this.colActualLabourCost.Visible = true;
            this.colActualLabourCost.VisibleIndex = 16;
            this.colActualLabourCost.Width = 102;
            // 
            // colEstimatedMaterialsCost
            // 
            this.colEstimatedMaterialsCost.Caption = "Est. Material Cost";
            this.colEstimatedMaterialsCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedMaterialsCost.FieldName = "EstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.Name = "colEstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsCost.Visible = true;
            this.colEstimatedMaterialsCost.VisibleIndex = 23;
            this.colEstimatedMaterialsCost.Width = 106;
            // 
            // colActualMaterialsCost
            // 
            this.colActualMaterialsCost.Caption = "Act. Material Cost";
            this.colActualMaterialsCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualMaterialsCost.FieldName = "ActualMaterialsCost";
            this.colActualMaterialsCost.Name = "colActualMaterialsCost";
            this.colActualMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsCost.Visible = true;
            this.colActualMaterialsCost.VisibleIndex = 24;
            this.colActualMaterialsCost.Width = 107;
            // 
            // colEstimatedEquipmentCost
            // 
            this.colEstimatedEquipmentCost.Caption = "Est. Equip. Cost";
            this.colEstimatedEquipmentCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedEquipmentCost.FieldName = "EstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.Name = "colEstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentCost.Visible = true;
            this.colEstimatedEquipmentCost.VisibleIndex = 19;
            this.colEstimatedEquipmentCost.Width = 98;
            // 
            // colActualEquipmentCost
            // 
            this.colActualEquipmentCost.Caption = "Act. Equip. Cost";
            this.colActualEquipmentCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualEquipmentCost.FieldName = "ActualEquipmentCost";
            this.colActualEquipmentCost.Name = "colActualEquipmentCost";
            this.colActualEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentCost.Visible = true;
            this.colActualEquipmentCost.VisibleIndex = 20;
            this.colActualEquipmentCost.Width = 99;
            // 
            // colEstimatedTotalCost
            // 
            this.colEstimatedTotalCost.Caption = "Est. Total Cost";
            this.colEstimatedTotalCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedTotalCost.FieldName = "EstimatedTotalCost";
            this.colEstimatedTotalCost.Name = "colEstimatedTotalCost";
            this.colEstimatedTotalCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalCost.Visible = true;
            this.colEstimatedTotalCost.VisibleIndex = 11;
            this.colEstimatedTotalCost.Width = 92;
            // 
            // colActualTotalCost
            // 
            this.colActualTotalCost.Caption = "Act. Total Cost";
            this.colActualTotalCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualTotalCost.FieldName = "ActualTotalCost";
            this.colActualTotalCost.Name = "colActualTotalCost";
            this.colActualTotalCost.OptionsColumn.AllowEdit = false;
            this.colActualTotalCost.OptionsColumn.AllowFocus = false;
            this.colActualTotalCost.OptionsColumn.ReadOnly = true;
            this.colActualTotalCost.Visible = true;
            this.colActualTotalCost.VisibleIndex = 12;
            this.colActualTotalCost.Width = 93;
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.Visible = true;
            this.colWorkOrderID.VisibleIndex = 27;
            this.colWorkOrderID.Width = 77;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 82;
            // 
            // colFinanceSystemBillingID
            // 
            this.colFinanceSystemBillingID.Caption = "Finance Billing ID";
            this.colFinanceSystemBillingID.FieldName = "FinanceSystemBillingID";
            this.colFinanceSystemBillingID.Name = "colFinanceSystemBillingID";
            this.colFinanceSystemBillingID.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemBillingID.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemBillingID.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemBillingID.Width = 101;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 29;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colGUID3
            // 
            this.colGUID3.Caption = "GUID";
            this.colGUID3.FieldName = "GUID";
            this.colGUID3.Name = "colGUID3";
            this.colGUID3.OptionsColumn.AllowEdit = false;
            this.colGUID3.OptionsColumn.AllowFocus = false;
            this.colGUID3.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedRecordDescription1
            // 
            this.colLinkedRecordDescription1.Caption = "Linked To Tree";
            this.colLinkedRecordDescription1.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription1.Name = "colLinkedRecordDescription1";
            this.colLinkedRecordDescription1.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription1.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription1.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription1.Visible = true;
            this.colLinkedRecordDescription1.VisibleIndex = 17;
            this.colLinkedRecordDescription1.Width = 379;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 0;
            this.colJobDescription.Width = 223;
            // 
            // colEstimatedTotalSell
            // 
            this.colEstimatedTotalSell.Caption = "Est. Total Sell";
            this.colEstimatedTotalSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedTotalSell.FieldName = "EstimatedTotalSell";
            this.colEstimatedTotalSell.Name = "colEstimatedTotalSell";
            this.colEstimatedTotalSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalSell.Visible = true;
            this.colEstimatedTotalSell.VisibleIndex = 13;
            this.colEstimatedTotalSell.Width = 86;
            // 
            // colActualTotalSell
            // 
            this.colActualTotalSell.Caption = "Act. Total Sell";
            this.colActualTotalSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualTotalSell.FieldName = "ActualTotalSell";
            this.colActualTotalSell.Name = "colActualTotalSell";
            this.colActualTotalSell.OptionsColumn.AllowEdit = false;
            this.colActualTotalSell.OptionsColumn.AllowFocus = false;
            this.colActualTotalSell.OptionsColumn.ReadOnly = true;
            this.colActualTotalSell.Visible = true;
            this.colActualTotalSell.VisibleIndex = 14;
            this.colActualTotalSell.Width = 87;
            // 
            // colEstimatedLabourSell
            // 
            this.colEstimatedLabourSell.Caption = "Est. Labour Sell";
            this.colEstimatedLabourSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedLabourSell.FieldName = "EstimatedLabourSell";
            this.colEstimatedLabourSell.Name = "colEstimatedLabourSell";
            this.colEstimatedLabourSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourSell.Visible = true;
            this.colEstimatedLabourSell.VisibleIndex = 17;
            this.colEstimatedLabourSell.Width = 95;
            // 
            // colActualLabourSell
            // 
            this.colActualLabourSell.Caption = "Act. Labour Sell";
            this.colActualLabourSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualLabourSell.FieldName = "ActualLabourSell";
            this.colActualLabourSell.Name = "colActualLabourSell";
            this.colActualLabourSell.OptionsColumn.AllowEdit = false;
            this.colActualLabourSell.OptionsColumn.AllowFocus = false;
            this.colActualLabourSell.OptionsColumn.ReadOnly = true;
            this.colActualLabourSell.Visible = true;
            this.colActualLabourSell.VisibleIndex = 18;
            this.colActualLabourSell.Width = 96;
            // 
            // colEstimatedEquipmentSell
            // 
            this.colEstimatedEquipmentSell.Caption = "Est. Equip. Sell";
            this.colEstimatedEquipmentSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedEquipmentSell.FieldName = "EstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.Name = "colEstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentSell.Visible = true;
            this.colEstimatedEquipmentSell.VisibleIndex = 21;
            this.colEstimatedEquipmentSell.Width = 92;
            // 
            // colActualEquipmentSell
            // 
            this.colActualEquipmentSell.Caption = "Act. Equip. Sell";
            this.colActualEquipmentSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualEquipmentSell.FieldName = "ActualEquipmentSell";
            this.colActualEquipmentSell.Name = "colActualEquipmentSell";
            this.colActualEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentSell.Visible = true;
            this.colActualEquipmentSell.VisibleIndex = 22;
            this.colActualEquipmentSell.Width = 93;
            // 
            // colEstimatedMaterialsSell
            // 
            this.colEstimatedMaterialsSell.Caption = "Est. Material Sell";
            this.colEstimatedMaterialsSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedMaterialsSell.FieldName = "EstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.Name = "colEstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsSell.Visible = true;
            this.colEstimatedMaterialsSell.VisibleIndex = 25;
            this.colEstimatedMaterialsSell.Width = 100;
            // 
            // colActualMaterialsSell
            // 
            this.colActualMaterialsSell.Caption = "Act. Material Sell";
            this.colActualMaterialsSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualMaterialsSell.FieldName = "ActualMaterialsSell";
            this.colActualMaterialsSell.Name = "colActualMaterialsSell";
            this.colActualMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsSell.Visible = true;
            this.colActualMaterialsSell.VisibleIndex = 26;
            this.colActualMaterialsSell.Width = 101;
            // 
            // colAchievableClearance
            // 
            this.colAchievableClearance.Caption = "Achievable Clearance";
            this.colAchievableClearance.ColumnEdit = this.repositoryItemTextEditMeters;
            this.colAchievableClearance.FieldName = "AchievableClearance";
            this.colAchievableClearance.Name = "colAchievableClearance";
            this.colAchievableClearance.OptionsColumn.AllowEdit = false;
            this.colAchievableClearance.OptionsColumn.AllowFocus = false;
            this.colAchievableClearance.OptionsColumn.ReadOnly = true;
            this.colAchievableClearance.Visible = true;
            this.colAchievableClearance.VisibleIndex = 8;
            this.colAchievableClearance.Width = 124;
            // 
            // repositoryItemTextEditMeters
            // 
            this.repositoryItemTextEditMeters.AutoHeight = false;
            this.repositoryItemTextEditMeters.Mask.EditMask = "######0.00 M";
            this.repositoryItemTextEditMeters.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMeters.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMeters.Name = "repositoryItemTextEditMeters";
            // 
            // colPossibleFlail
            // 
            this.colPossibleFlail.Caption = "Possible Flail";
            this.colPossibleFlail.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleFlail.FieldName = "PossibleFlail";
            this.colPossibleFlail.Name = "colPossibleFlail";
            this.colPossibleFlail.OptionsColumn.AllowEdit = false;
            this.colPossibleFlail.OptionsColumn.AllowFocus = false;
            this.colPossibleFlail.OptionsColumn.ReadOnly = true;
            this.colPossibleFlail.Visible = true;
            this.colPossibleFlail.VisibleIndex = 6;
            this.colPossibleFlail.Width = 80;
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revist Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemTextEditShortDate;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsColumn.AllowEdit = false;
            this.colRevisitDate.OptionsColumn.AllowFocus = false;
            this.colRevisitDate.OptionsColumn.ReadOnly = true;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 28;
            // 
            // repositoryItemTextEditShortDate
            // 
            this.repositoryItemTextEditShortDate.AutoHeight = false;
            this.repositoryItemTextEditShortDate.Mask.EditMask = "d";
            this.repositoryItemTextEditShortDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditShortDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShortDate.Name = "repositoryItemTextEditShortDate";
            // 
            // colActualHours1
            // 
            this.colActualHours1.Caption = "Actual Hours";
            this.colActualHours1.ColumnEdit = this.repositoryItemTextEditNumericHours;
            this.colActualHours1.FieldName = "ActualHours";
            this.colActualHours1.Name = "colActualHours1";
            this.colActualHours1.OptionsColumn.AllowEdit = false;
            this.colActualHours1.OptionsColumn.AllowFocus = false;
            this.colActualHours1.OptionsColumn.ReadOnly = true;
            this.colActualHours1.Visible = true;
            this.colActualHours1.VisibleIndex = 10;
            this.colActualHours1.Width = 82;
            // 
            // repositoryItemTextEditNumericHours
            // 
            this.repositoryItemTextEditNumericHours.AutoHeight = false;
            this.repositoryItemTextEditNumericHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditNumericHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericHours.Name = "repositoryItemTextEditNumericHours";
            // 
            // colEstimatedHours1
            // 
            this.colEstimatedHours1.Caption = "Estimated Hours";
            this.colEstimatedHours1.ColumnEdit = this.repositoryItemTextEditNumericHours;
            this.colEstimatedHours1.FieldName = "EstimatedHours";
            this.colEstimatedHours1.Name = "colEstimatedHours1";
            this.colEstimatedHours1.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours1.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours1.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours1.Visible = true;
            this.colEstimatedHours1.VisibleIndex = 9;
            this.colEstimatedHours1.Width = 99;
            // 
            // colPossibleLiveWork
            // 
            this.colPossibleLiveWork.Caption = "Possible Live Work";
            this.colPossibleLiveWork.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork.Name = "colPossibleLiveWork";
            this.colPossibleLiveWork.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork.Visible = true;
            this.colPossibleLiveWork.VisibleIndex = 7;
            this.colPossibleLiveWork.Width = 109;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            this.colClientID1.Width = 62;
            // 
            // colDataEntryScreenName1
            // 
            this.colDataEntryScreenName1.Caption = "Data Entry Screen Name";
            this.colDataEntryScreenName1.FieldName = "DataEntryScreenName";
            this.colDataEntryScreenName1.Name = "colDataEntryScreenName1";
            this.colDataEntryScreenName1.OptionsColumn.AllowEdit = false;
            this.colDataEntryScreenName1.OptionsColumn.AllowFocus = false;
            this.colDataEntryScreenName1.OptionsColumn.ReadOnly = true;
            this.colDataEntryScreenName1.Width = 139;
            // 
            // colReportLayoutName1
            // 
            this.colReportLayoutName1.Caption = "Report Layout Name";
            this.colReportLayoutName1.FieldName = "ReportLayoutName";
            this.colReportLayoutName1.Name = "colReportLayoutName1";
            this.colReportLayoutName1.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName1.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName1.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName1.Width = 120;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 114;
            // 
            // xtraTabControl4
            // 
            this.xtraTabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl4.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl4.Name = "xtraTabControl4";
            this.xtraTabControl4.SelectedTabPage = this.xtraTabPage11;
            this.xtraTabControl4.Size = new System.Drawing.Size(530, 194);
            this.xtraTabControl4.TabIndex = 0;
            this.xtraTabControl4.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage11,
            this.xtraTabPage2,
            this.xtraTabPage12,
            this.xtraTabPage13});
            // 
            // xtraTabPage11
            // 
            this.xtraTabPage11.Controls.Add(this.gridControl8);
            this.xtraTabPage11.Name = "xtraTabPage11";
            this.xtraTabPage11.Size = new System.Drawing.Size(525, 168);
            this.xtraTabPage11.Text = "Work - Pictures";
            // 
            // gridControl8
            // 
            this.gridControl8.DataSource = this.sp07130UTSurveyedPoleWorkPicturesListBindingSource;
            this.gridControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl8.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl8_EmbeddedNavigator_ButtonClick);
            this.gridControl8.Location = new System.Drawing.Point(0, 0);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemHyperLinkEdit4,
            this.repositoryItemTextEditDateTime2});
            this.gridControl8.Size = new System.Drawing.Size(525, 168);
            this.gridControl8.TabIndex = 3;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp07130UTSurveyedPoleWorkPicturesListBindingSource
            // 
            this.sp07130UTSurveyedPoleWorkPicturesListBindingSource.DataMember = "sp07130_UT_Surveyed_Pole_Work_Pictures_List";
            this.sp07130UTSurveyedPoleWorkPicturesListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.colShortLinkedRecordDescription});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 1;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colShortLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn28, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView8_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Survey Picture ID";
            this.gridColumn23.FieldName = "SurveyPictureID";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 105;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Linked To Record ID";
            this.gridColumn24.FieldName = "LinkedToRecordID";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 117;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Linked To Record Type ID";
            this.gridColumn25.FieldName = "LinkedToRecordTypeID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Width = 144;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Picture Type ID";
            this.gridColumn26.FieldName = "PictureTypeID";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 95;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Picture Path";
            this.gridColumn27.ColumnEdit = this.repositoryItemHyperLinkEdit4;
            this.gridColumn27.FieldName = "PicturePath";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 2;
            this.gridColumn27.Width = 323;
            // 
            // repositoryItemHyperLinkEdit4
            // 
            this.repositoryItemHyperLinkEdit4.AutoHeight = false;
            this.repositoryItemHyperLinkEdit4.Name = "repositoryItemHyperLinkEdit4";
            this.repositoryItemHyperLinkEdit4.SingleClick = true;
            this.repositoryItemHyperLinkEdit4.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit4_OpenLink);
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Date Taken";
            this.gridColumn28.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn28.FieldName = "DateTimeTaken";
            this.gridColumn28.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 0;
            this.gridColumn28.Width = 113;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Added By Staff ID";
            this.gridColumn29.FieldName = "AddedByStaffID";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 108;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "GUID";
            this.gridColumn30.FieldName = "GUID";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Remarks";
            this.gridColumn31.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.gridColumn31.FieldName = "Remarks";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 3;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Linked To Survey";
            this.gridColumn32.FieldName = "LinkedRecordDescription";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 303;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Added By";
            this.gridColumn33.FieldName = "AddedByStaffName";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 1;
            this.gridColumn33.Width = 108;
            // 
            // colShortLinkedRecordDescription
            // 
            this.colShortLinkedRecordDescription.Caption = "Linked To Tree";
            this.colShortLinkedRecordDescription.FieldName = "ShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.Name = "colShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colShortLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colShortLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colShortLinkedRecordDescription.Visible = true;
            this.colShortLinkedRecordDescription.VisibleIndex = 4;
            this.colShortLinkedRecordDescription.Width = 231;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl27);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(525, 168);
            this.xtraTabPage2.Text = "Permissions";
            // 
            // gridControl27
            // 
            this.gridControl27.DataSource = this.sp07401UTPDLinkedToActionsBindingSource;
            this.gridControl27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl27.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl27.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl27.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl27.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl27.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl27.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl27.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl27.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl27.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl27.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl27.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl27.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Add Work to Existing Permission Document", "add to existing"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh data", "refresh")});
            this.gridControl27.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl27_EmbeddedNavigator_ButtonClick);
            this.gridControl27.Location = new System.Drawing.Point(0, 0);
            this.gridControl27.MainView = this.gridView27;
            this.gridControl27.MenuManager = this.barManager1;
            this.gridControl27.Name = "gridControl27";
            this.gridControl27.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit24,
            this.repositoryItemHyperLinkEdit7,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEdit0DP,
            this.repositoryItemCheckEdit5});
            this.gridControl27.Size = new System.Drawing.Size(525, 168);
            this.gridControl27.TabIndex = 0;
            this.gridControl27.UseEmbeddedNavigator = true;
            this.gridControl27.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView27});
            // 
            // sp07401UTPDLinkedToActionsBindingSource
            // 
            this.sp07401UTPDLinkedToActionsBindingSource.DataMember = "sp07401_UT_PD_Linked_To_Actions";
            this.sp07401UTPDLinkedToActionsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView27
            // 
            this.gridView27.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPermissionDocumentID,
            this.gridColumn51,
            this.colRaisedByID,
            this.colSignatureFile2,
            this.gridColumn53,
            this.gridColumn100,
            this.gridColumn101,
            this.gridColumn102,
            this.gridColumn103,
            this.colEmergencyAccess,
            this.colNearestTelephonePoint,
            this.colGridReference,
            this.gridColumn104,
            this.colHotGloveAccessAvailable,
            this.colAccessAgreedWithLandOwner,
            this.colAccessDetailsOnMap,
            this.colRoadsideAccessOnly,
            this.colEstimatedRevisitDate,
            this.colTPOTree,
            this.colPlanningConservation,
            this.colWildlifeDesignation,
            this.colSpecialInstructions,
            this.colPrimarySubName,
            this.colPrimarySubNumber,
            this.colFeederName,
            this.colFeederNumber,
            this.colLineNumber,
            this.colLVSubName,
            this.colLVSubNumber,
            this.colManagedUnitNumber,
            this.colEnvironmentalRANumber,
            this.colLandscapeImpactNumber,
            this.colSiteGridReference,
            this.colPermissionEmailed,
            this.colPostageRequired,
            this.colSentByPost,
            this.colSentByStaffID,
            this.gridColumn105,
            this.colOwnerName,
            this.colOwnerSalutation,
            this.colOwnerAddress,
            this.colOwnerPostcode,
            this.colOwnerTelephone,
            this.colOwnerEmail,
            this.gridColumn106,
            this.gridColumn107,
            this.colContractor,
            this.colArisingsChipRemove,
            this.colArisingsChipOnSite,
            this.colArisingsStackOnSite,
            this.colArisingsOther,
            this.colClientID,
            this.colRaisedByName,
            this.colSentByStaffName,
            this.colLinkedActionCount,
            this.colLinkedToDoActionCount,
            this.colOwnerType,
            this.colClientName,
            this.colDataEntryScreenName,
            this.colReportLayoutName});
            this.gridView27.GridControl = this.gridControl27;
            this.gridView27.Name = "gridView27";
            this.gridView27.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView27.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView27.OptionsLayout.StoreAppearance = true;
            this.gridView27.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView27.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView27.OptionsSelection.MultiSelect = true;
            this.gridView27.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView27.OptionsView.ColumnAutoWidth = false;
            this.gridView27.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView27.OptionsView.ShowGroupPanel = false;
            this.gridView27.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn51, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView27.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView27_CustomRowCellEdit);
            this.gridView27.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView27.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView27_SelectionChanged);
            this.gridView27.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView27.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView27_ShowingEditor);
            this.gridView27.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView27.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView27.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView27_MouseUp);
            this.gridView27.DoubleClick += new System.EventHandler(this.gridView27_DoubleClick);
            this.gridView27.GotFocus += new System.EventHandler(this.gridView27_GotFocus);
            // 
            // colPermissionDocumentID
            // 
            this.colPermissionDocumentID.Caption = "Permission Document ID";
            this.colPermissionDocumentID.FieldName = "PermissionDocumentID";
            this.colPermissionDocumentID.Name = "colPermissionDocumentID";
            this.colPermissionDocumentID.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentID.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentID.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentID.Width = 136;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Date Raised";
            this.gridColumn51.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn51.FieldName = "DateRaised";
            this.gridColumn51.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn51.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 2;
            this.gridColumn51.Width = 108;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colRaisedByID
            // 
            this.colRaisedByID.Caption = "Raised By ID";
            this.colRaisedByID.FieldName = "RaisedByID";
            this.colRaisedByID.Name = "colRaisedByID";
            this.colRaisedByID.OptionsColumn.AllowEdit = false;
            this.colRaisedByID.OptionsColumn.AllowFocus = false;
            this.colRaisedByID.OptionsColumn.ReadOnly = true;
            this.colRaisedByID.Width = 82;
            // 
            // colSignatureFile2
            // 
            this.colSignatureFile2.Caption = "Signature File";
            this.colSignatureFile2.ColumnEdit = this.repositoryItemHyperLinkEdit7;
            this.colSignatureFile2.FieldName = "SignatureFile";
            this.colSignatureFile2.Name = "colSignatureFile2";
            this.colSignatureFile2.OptionsColumn.ReadOnly = true;
            this.colSignatureFile2.Visible = true;
            this.colSignatureFile2.VisibleIndex = 12;
            this.colSignatureFile2.Width = 106;
            // 
            // repositoryItemHyperLinkEdit7
            // 
            this.repositoryItemHyperLinkEdit7.AutoHeight = false;
            this.repositoryItemHyperLinkEdit7.Name = "repositoryItemHyperLinkEdit7";
            this.repositoryItemHyperLinkEdit7.SingleClick = true;
            this.repositoryItemHyperLinkEdit7.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit7_OpenLink);
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "PDF File";
            this.gridColumn53.ColumnEdit = this.repositoryItemHyperLinkEdit7;
            this.gridColumn53.FieldName = "PDFFile";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 13;
            this.gridColumn53.Width = 118;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Email to Client Date";
            this.gridColumn100.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn100.FieldName = "EmailedToClientDate";
            this.gridColumn100.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn100.Visible = true;
            this.gridColumn100.VisibleIndex = 16;
            this.gridColumn100.Width = 114;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Emailed To Customer Date";
            this.gridColumn101.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn101.FieldName = "EmailedToCustomerDate";
            this.gridColumn101.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.AllowEdit = false;
            this.gridColumn101.OptionsColumn.AllowFocus = false;
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            this.gridColumn101.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn101.Visible = true;
            this.gridColumn101.VisibleIndex = 17;
            this.gridColumn101.Width = 147;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "Remarks";
            this.gridColumn102.ColumnEdit = this.repositoryItemMemoExEdit24;
            this.gridColumn102.FieldName = "Remarks";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.Visible = true;
            this.gridColumn102.VisibleIndex = 18;
            // 
            // repositoryItemMemoExEdit24
            // 
            this.repositoryItemMemoExEdit24.AutoHeight = false;
            this.repositoryItemMemoExEdit24.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit24.Name = "repositoryItemMemoExEdit24";
            this.repositoryItemMemoExEdit24.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit24.ShowIcon = false;
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "Signature Date";
            this.gridColumn103.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn103.FieldName = "SignatureDate";
            this.gridColumn103.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            this.gridColumn103.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn103.Width = 93;
            // 
            // colEmergencyAccess
            // 
            this.colEmergencyAccess.Caption = "Emergency Date";
            this.colEmergencyAccess.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colEmergencyAccess.FieldName = "EmergencyAccess";
            this.colEmergencyAccess.Name = "colEmergencyAccess";
            this.colEmergencyAccess.OptionsColumn.AllowEdit = false;
            this.colEmergencyAccess.OptionsColumn.AllowFocus = false;
            this.colEmergencyAccess.OptionsColumn.ReadOnly = true;
            this.colEmergencyAccess.Visible = true;
            this.colEmergencyAccess.VisibleIndex = 19;
            this.colEmergencyAccess.Width = 100;
            // 
            // colNearestTelephonePoint
            // 
            this.colNearestTelephonePoint.Caption = "Nearest Telephone Point";
            this.colNearestTelephonePoint.FieldName = "NearestTelephonePoint";
            this.colNearestTelephonePoint.Name = "colNearestTelephonePoint";
            this.colNearestTelephonePoint.OptionsColumn.AllowEdit = false;
            this.colNearestTelephonePoint.OptionsColumn.AllowFocus = false;
            this.colNearestTelephonePoint.OptionsColumn.ReadOnly = true;
            this.colNearestTelephonePoint.Visible = true;
            this.colNearestTelephonePoint.VisibleIndex = 20;
            this.colNearestTelephonePoint.Width = 139;
            // 
            // colGridReference
            // 
            this.colGridReference.Caption = "Grid Reference";
            this.colGridReference.FieldName = "GridReference";
            this.colGridReference.Name = "colGridReference";
            this.colGridReference.OptionsColumn.AllowEdit = false;
            this.colGridReference.OptionsColumn.AllowFocus = false;
            this.colGridReference.OptionsColumn.ReadOnly = true;
            this.colGridReference.Visible = true;
            this.colGridReference.VisibleIndex = 21;
            this.colGridReference.Width = 93;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Nearest A & E";
            this.gridColumn104.FieldName = "NearestAandE";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            this.gridColumn104.Visible = true;
            this.gridColumn104.VisibleIndex = 22;
            this.gridColumn104.Width = 107;
            // 
            // colHotGloveAccessAvailable
            // 
            this.colHotGloveAccessAvailable.Caption = "Hot Glove Access Available";
            this.colHotGloveAccessAvailable.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colHotGloveAccessAvailable.FieldName = "HotGloveAccessAvailable";
            this.colHotGloveAccessAvailable.Name = "colHotGloveAccessAvailable";
            this.colHotGloveAccessAvailable.OptionsColumn.AllowEdit = false;
            this.colHotGloveAccessAvailable.OptionsColumn.AllowFocus = false;
            this.colHotGloveAccessAvailable.OptionsColumn.ReadOnly = true;
            this.colHotGloveAccessAvailable.Visible = true;
            this.colHotGloveAccessAvailable.VisibleIndex = 23;
            this.colHotGloveAccessAvailable.Width = 150;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // colAccessAgreedWithLandOwner
            // 
            this.colAccessAgreedWithLandOwner.Caption = "Access Agreed with Landowner";
            this.colAccessAgreedWithLandOwner.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colAccessAgreedWithLandOwner.FieldName = "AccessAgreedWithLandOwner";
            this.colAccessAgreedWithLandOwner.Name = "colAccessAgreedWithLandOwner";
            this.colAccessAgreedWithLandOwner.OptionsColumn.AllowEdit = false;
            this.colAccessAgreedWithLandOwner.OptionsColumn.AllowFocus = false;
            this.colAccessAgreedWithLandOwner.OptionsColumn.ReadOnly = true;
            this.colAccessAgreedWithLandOwner.Visible = true;
            this.colAccessAgreedWithLandOwner.VisibleIndex = 24;
            this.colAccessAgreedWithLandOwner.Width = 171;
            // 
            // colAccessDetailsOnMap
            // 
            this.colAccessDetailsOnMap.Caption = "Access Details On Map";
            this.colAccessDetailsOnMap.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colAccessDetailsOnMap.FieldName = "AccessDetailsOnMap";
            this.colAccessDetailsOnMap.Name = "colAccessDetailsOnMap";
            this.colAccessDetailsOnMap.OptionsColumn.AllowEdit = false;
            this.colAccessDetailsOnMap.OptionsColumn.AllowFocus = false;
            this.colAccessDetailsOnMap.OptionsColumn.ReadOnly = true;
            this.colAccessDetailsOnMap.Visible = true;
            this.colAccessDetailsOnMap.VisibleIndex = 25;
            this.colAccessDetailsOnMap.Width = 129;
            // 
            // colRoadsideAccessOnly
            // 
            this.colRoadsideAccessOnly.Caption = "Roadside Access Only";
            this.colRoadsideAccessOnly.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colRoadsideAccessOnly.FieldName = "RoadsideAccessOnly";
            this.colRoadsideAccessOnly.Name = "colRoadsideAccessOnly";
            this.colRoadsideAccessOnly.OptionsColumn.AllowEdit = false;
            this.colRoadsideAccessOnly.OptionsColumn.AllowFocus = false;
            this.colRoadsideAccessOnly.OptionsColumn.ReadOnly = true;
            this.colRoadsideAccessOnly.Visible = true;
            this.colRoadsideAccessOnly.VisibleIndex = 26;
            this.colRoadsideAccessOnly.Width = 126;
            // 
            // colEstimatedRevisitDate
            // 
            this.colEstimatedRevisitDate.Caption = "Estimated Revisit Date";
            this.colEstimatedRevisitDate.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colEstimatedRevisitDate.FieldName = "EstimatedRevisitDate";
            this.colEstimatedRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEstimatedRevisitDate.Name = "colEstimatedRevisitDate";
            this.colEstimatedRevisitDate.OptionsColumn.AllowEdit = false;
            this.colEstimatedRevisitDate.OptionsColumn.AllowFocus = false;
            this.colEstimatedRevisitDate.OptionsColumn.ReadOnly = true;
            this.colEstimatedRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEstimatedRevisitDate.Visible = true;
            this.colEstimatedRevisitDate.VisibleIndex = 27;
            this.colEstimatedRevisitDate.Width = 129;
            // 
            // colTPOTree
            // 
            this.colTPOTree.Caption = "TPO Tree";
            this.colTPOTree.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colTPOTree.FieldName = "TPOTree";
            this.colTPOTree.Name = "colTPOTree";
            this.colTPOTree.OptionsColumn.AllowEdit = false;
            this.colTPOTree.OptionsColumn.AllowFocus = false;
            this.colTPOTree.OptionsColumn.ReadOnly = true;
            this.colTPOTree.Visible = true;
            this.colTPOTree.VisibleIndex = 28;
            // 
            // colPlanningConservation
            // 
            this.colPlanningConservation.Caption = "Planning Conservation";
            this.colPlanningConservation.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colPlanningConservation.FieldName = "PlanningConservation";
            this.colPlanningConservation.Name = "colPlanningConservation";
            this.colPlanningConservation.OptionsColumn.AllowEdit = false;
            this.colPlanningConservation.OptionsColumn.AllowFocus = false;
            this.colPlanningConservation.OptionsColumn.ReadOnly = true;
            this.colPlanningConservation.Visible = true;
            this.colPlanningConservation.VisibleIndex = 29;
            this.colPlanningConservation.Width = 128;
            // 
            // colWildlifeDesignation
            // 
            this.colWildlifeDesignation.Caption = "Wildlife Designation";
            this.colWildlifeDesignation.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colWildlifeDesignation.FieldName = "WildlifeDesignation";
            this.colWildlifeDesignation.Name = "colWildlifeDesignation";
            this.colWildlifeDesignation.OptionsColumn.AllowEdit = false;
            this.colWildlifeDesignation.OptionsColumn.AllowFocus = false;
            this.colWildlifeDesignation.OptionsColumn.ReadOnly = true;
            this.colWildlifeDesignation.Visible = true;
            this.colWildlifeDesignation.VisibleIndex = 30;
            this.colWildlifeDesignation.Width = 114;
            // 
            // colSpecialInstructions
            // 
            this.colSpecialInstructions.Caption = "Special Instructions";
            this.colSpecialInstructions.ColumnEdit = this.repositoryItemMemoExEdit24;
            this.colSpecialInstructions.FieldName = "SpecialInstructions";
            this.colSpecialInstructions.Name = "colSpecialInstructions";
            this.colSpecialInstructions.OptionsColumn.ReadOnly = true;
            this.colSpecialInstructions.Visible = true;
            this.colSpecialInstructions.VisibleIndex = 31;
            this.colSpecialInstructions.Width = 114;
            // 
            // colPrimarySubName
            // 
            this.colPrimarySubName.Caption = "Primary Sub Name";
            this.colPrimarySubName.FieldName = "PrimarySubName";
            this.colPrimarySubName.Name = "colPrimarySubName";
            this.colPrimarySubName.OptionsColumn.AllowEdit = false;
            this.colPrimarySubName.OptionsColumn.AllowFocus = false;
            this.colPrimarySubName.OptionsColumn.ReadOnly = true;
            this.colPrimarySubName.Visible = true;
            this.colPrimarySubName.VisibleIndex = 32;
            this.colPrimarySubName.Width = 108;
            // 
            // colPrimarySubNumber
            // 
            this.colPrimarySubNumber.Caption = "Primary Sub #";
            this.colPrimarySubNumber.FieldName = "PrimarySubNumber";
            this.colPrimarySubNumber.Name = "colPrimarySubNumber";
            this.colPrimarySubNumber.OptionsColumn.AllowEdit = false;
            this.colPrimarySubNumber.OptionsColumn.AllowFocus = false;
            this.colPrimarySubNumber.OptionsColumn.ReadOnly = true;
            this.colPrimarySubNumber.Visible = true;
            this.colPrimarySubNumber.VisibleIndex = 33;
            this.colPrimarySubNumber.Width = 89;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 34;
            this.colFeederName.Width = 85;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder #";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.Visible = true;
            this.colFeederNumber.VisibleIndex = 35;
            // 
            // colLineNumber
            // 
            this.colLineNumber.Caption = "Line #";
            this.colLineNumber.FieldName = "LineNumber";
            this.colLineNumber.Name = "colLineNumber";
            this.colLineNumber.OptionsColumn.AllowEdit = false;
            this.colLineNumber.OptionsColumn.AllowFocus = false;
            this.colLineNumber.OptionsColumn.ReadOnly = true;
            this.colLineNumber.Visible = true;
            this.colLineNumber.VisibleIndex = 36;
            // 
            // colLVSubName
            // 
            this.colLVSubName.Caption = "LV Sub Name";
            this.colLVSubName.FieldName = "LVSubName";
            this.colLVSubName.Name = "colLVSubName";
            this.colLVSubName.OptionsColumn.AllowEdit = false;
            this.colLVSubName.OptionsColumn.AllowFocus = false;
            this.colLVSubName.OptionsColumn.ReadOnly = true;
            this.colLVSubName.Visible = true;
            this.colLVSubName.VisibleIndex = 37;
            this.colLVSubName.Width = 83;
            // 
            // colLVSubNumber
            // 
            this.colLVSubNumber.Caption = "LV Sub #";
            this.colLVSubNumber.FieldName = "LVSubNumber";
            this.colLVSubNumber.Name = "colLVSubNumber";
            this.colLVSubNumber.OptionsColumn.AllowEdit = false;
            this.colLVSubNumber.OptionsColumn.AllowFocus = false;
            this.colLVSubNumber.OptionsColumn.ReadOnly = true;
            this.colLVSubNumber.Visible = true;
            this.colLVSubNumber.VisibleIndex = 38;
            // 
            // colManagedUnitNumber
            // 
            this.colManagedUnitNumber.Caption = "Managed Unit #";
            this.colManagedUnitNumber.FieldName = "ManagedUnitNumber";
            this.colManagedUnitNumber.Name = "colManagedUnitNumber";
            this.colManagedUnitNumber.OptionsColumn.AllowEdit = false;
            this.colManagedUnitNumber.OptionsColumn.AllowFocus = false;
            this.colManagedUnitNumber.OptionsColumn.ReadOnly = true;
            this.colManagedUnitNumber.Visible = true;
            this.colManagedUnitNumber.VisibleIndex = 39;
            this.colManagedUnitNumber.Width = 98;
            // 
            // colEnvironmentalRANumber
            // 
            this.colEnvironmentalRANumber.Caption = "Environmental RA #";
            this.colEnvironmentalRANumber.FieldName = "EnvironmentalRANumber";
            this.colEnvironmentalRANumber.Name = "colEnvironmentalRANumber";
            this.colEnvironmentalRANumber.OptionsColumn.AllowEdit = false;
            this.colEnvironmentalRANumber.OptionsColumn.AllowFocus = false;
            this.colEnvironmentalRANumber.OptionsColumn.ReadOnly = true;
            this.colEnvironmentalRANumber.Visible = true;
            this.colEnvironmentalRANumber.VisibleIndex = 40;
            this.colEnvironmentalRANumber.Width = 117;
            // 
            // colLandscapeImpactNumber
            // 
            this.colLandscapeImpactNumber.Caption = "Landscape Impact #";
            this.colLandscapeImpactNumber.FieldName = "LandscapeImpactNumber";
            this.colLandscapeImpactNumber.Name = "colLandscapeImpactNumber";
            this.colLandscapeImpactNumber.OptionsColumn.AllowEdit = false;
            this.colLandscapeImpactNumber.OptionsColumn.AllowFocus = false;
            this.colLandscapeImpactNumber.OptionsColumn.ReadOnly = true;
            this.colLandscapeImpactNumber.Visible = true;
            this.colLandscapeImpactNumber.VisibleIndex = 41;
            this.colLandscapeImpactNumber.Width = 119;
            // 
            // colSiteGridReference
            // 
            this.colSiteGridReference.Caption = "Site Grid Ref.";
            this.colSiteGridReference.FieldName = "SiteGridReference";
            this.colSiteGridReference.Name = "colSiteGridReference";
            this.colSiteGridReference.OptionsColumn.AllowEdit = false;
            this.colSiteGridReference.OptionsColumn.AllowFocus = false;
            this.colSiteGridReference.OptionsColumn.ReadOnly = true;
            this.colSiteGridReference.Visible = true;
            this.colSiteGridReference.VisibleIndex = 42;
            this.colSiteGridReference.Width = 85;
            // 
            // colPermissionEmailed
            // 
            this.colPermissionEmailed.Caption = "Permission Emailed";
            this.colPermissionEmailed.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colPermissionEmailed.FieldName = "PermissionEmailed";
            this.colPermissionEmailed.Name = "colPermissionEmailed";
            this.colPermissionEmailed.OptionsColumn.AllowEdit = false;
            this.colPermissionEmailed.OptionsColumn.AllowFocus = false;
            this.colPermissionEmailed.OptionsColumn.ReadOnly = true;
            this.colPermissionEmailed.Visible = true;
            this.colPermissionEmailed.VisibleIndex = 43;
            this.colPermissionEmailed.Width = 110;
            // 
            // colPostageRequired
            // 
            this.colPostageRequired.Caption = "Postage Required";
            this.colPostageRequired.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colPostageRequired.FieldName = "PostageRequired";
            this.colPostageRequired.Name = "colPostageRequired";
            this.colPostageRequired.OptionsColumn.AllowEdit = false;
            this.colPostageRequired.OptionsColumn.AllowFocus = false;
            this.colPostageRequired.OptionsColumn.ReadOnly = true;
            this.colPostageRequired.Visible = true;
            this.colPostageRequired.VisibleIndex = 44;
            this.colPostageRequired.Width = 106;
            // 
            // colSentByPost
            // 
            this.colSentByPost.Caption = "Sent By Post";
            this.colSentByPost.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colSentByPost.FieldName = "SentByPost";
            this.colSentByPost.Name = "colSentByPost";
            this.colSentByPost.OptionsColumn.AllowEdit = false;
            this.colSentByPost.OptionsColumn.AllowFocus = false;
            this.colSentByPost.OptionsColumn.ReadOnly = true;
            this.colSentByPost.Visible = true;
            this.colSentByPost.VisibleIndex = 45;
            this.colSentByPost.Width = 82;
            // 
            // colSentByStaffID
            // 
            this.colSentByStaffID.Caption = "Sent by Staff ID";
            this.colSentByStaffID.FieldName = "SentByStaffID";
            this.colSentByStaffID.Name = "colSentByStaffID";
            this.colSentByStaffID.OptionsColumn.AllowEdit = false;
            this.colSentByStaffID.OptionsColumn.AllowFocus = false;
            this.colSentByStaffID.OptionsColumn.ReadOnly = true;
            this.colSentByStaffID.Width = 99;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Reference Number";
            this.gridColumn105.FieldName = "ReferenceNumber";
            this.gridColumn105.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Visible = true;
            this.gridColumn105.VisibleIndex = 0;
            this.gridColumn105.Width = 124;
            // 
            // colOwnerName
            // 
            this.colOwnerName.Caption = "Owner Name";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.OptionsColumn.AllowEdit = false;
            this.colOwnerName.OptionsColumn.AllowFocus = false;
            this.colOwnerName.OptionsColumn.ReadOnly = true;
            this.colOwnerName.Visible = true;
            this.colOwnerName.VisibleIndex = 4;
            this.colOwnerName.Width = 104;
            // 
            // colOwnerSalutation
            // 
            this.colOwnerSalutation.Caption = "Owner Saluation";
            this.colOwnerSalutation.FieldName = "OwnerSalutation";
            this.colOwnerSalutation.Name = "colOwnerSalutation";
            this.colOwnerSalutation.OptionsColumn.AllowEdit = false;
            this.colOwnerSalutation.OptionsColumn.AllowFocus = false;
            this.colOwnerSalutation.OptionsColumn.ReadOnly = true;
            this.colOwnerSalutation.Visible = true;
            this.colOwnerSalutation.VisibleIndex = 9;
            this.colOwnerSalutation.Width = 100;
            // 
            // colOwnerAddress
            // 
            this.colOwnerAddress.Caption = "Owner Address";
            this.colOwnerAddress.FieldName = "OwnerAddress";
            this.colOwnerAddress.Name = "colOwnerAddress";
            this.colOwnerAddress.OptionsColumn.ReadOnly = true;
            this.colOwnerAddress.Visible = true;
            this.colOwnerAddress.VisibleIndex = 5;
            this.colOwnerAddress.Width = 134;
            // 
            // colOwnerPostcode
            // 
            this.colOwnerPostcode.Caption = "Owner Postcode";
            this.colOwnerPostcode.FieldName = "OwnerPostcode";
            this.colOwnerPostcode.Name = "colOwnerPostcode";
            this.colOwnerPostcode.OptionsColumn.AllowEdit = false;
            this.colOwnerPostcode.OptionsColumn.AllowFocus = false;
            this.colOwnerPostcode.OptionsColumn.ReadOnly = true;
            this.colOwnerPostcode.Visible = true;
            this.colOwnerPostcode.VisibleIndex = 6;
            this.colOwnerPostcode.Width = 100;
            // 
            // colOwnerTelephone
            // 
            this.colOwnerTelephone.Caption = "Owner Telephone";
            this.colOwnerTelephone.FieldName = "OwnerTelephone";
            this.colOwnerTelephone.Name = "colOwnerTelephone";
            this.colOwnerTelephone.OptionsColumn.AllowEdit = false;
            this.colOwnerTelephone.OptionsColumn.AllowFocus = false;
            this.colOwnerTelephone.OptionsColumn.ReadOnly = true;
            this.colOwnerTelephone.Visible = true;
            this.colOwnerTelephone.VisibleIndex = 7;
            this.colOwnerTelephone.Width = 106;
            // 
            // colOwnerEmail
            // 
            this.colOwnerEmail.Caption = "Owner Email";
            this.colOwnerEmail.FieldName = "OwnerEmail";
            this.colOwnerEmail.Name = "colOwnerEmail";
            this.colOwnerEmail.OptionsColumn.AllowEdit = false;
            this.colOwnerEmail.OptionsColumn.AllowFocus = false;
            this.colOwnerEmail.OptionsColumn.ReadOnly = true;
            this.colOwnerEmail.Visible = true;
            this.colOwnerEmail.VisibleIndex = 8;
            this.colOwnerEmail.Width = 80;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Owner Type ID";
            this.gridColumn106.FieldName = "OwnerTypeID";
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowFocus = false;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.Width = 94;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "Site Address";
            this.gridColumn107.FieldName = "SiteAddress";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.Visible = true;
            this.gridColumn107.VisibleIndex = 11;
            this.gridColumn107.Width = 103;
            // 
            // colContractor
            // 
            this.colContractor.Caption = "Contractor";
            this.colContractor.FieldName = "Contractor";
            this.colContractor.Name = "colContractor";
            this.colContractor.OptionsColumn.AllowEdit = false;
            this.colContractor.OptionsColumn.AllowFocus = false;
            this.colContractor.OptionsColumn.ReadOnly = true;
            this.colContractor.Visible = true;
            this.colContractor.VisibleIndex = 46;
            // 
            // colArisingsChipRemove
            // 
            this.colArisingsChipRemove.Caption = "Arising Chip & Remove";
            this.colArisingsChipRemove.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colArisingsChipRemove.FieldName = "ArisingsChipRemove";
            this.colArisingsChipRemove.Name = "colArisingsChipRemove";
            this.colArisingsChipRemove.OptionsColumn.AllowEdit = false;
            this.colArisingsChipRemove.OptionsColumn.AllowFocus = false;
            this.colArisingsChipRemove.OptionsColumn.ReadOnly = true;
            this.colArisingsChipRemove.Visible = true;
            this.colArisingsChipRemove.VisibleIndex = 47;
            this.colArisingsChipRemove.Width = 129;
            // 
            // colArisingsChipOnSite
            // 
            this.colArisingsChipOnSite.Caption = "Arising Chip On Site";
            this.colArisingsChipOnSite.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colArisingsChipOnSite.FieldName = "ArisingsChipOnSite";
            this.colArisingsChipOnSite.Name = "colArisingsChipOnSite";
            this.colArisingsChipOnSite.OptionsColumn.AllowEdit = false;
            this.colArisingsChipOnSite.OptionsColumn.AllowFocus = false;
            this.colArisingsChipOnSite.OptionsColumn.ReadOnly = true;
            this.colArisingsChipOnSite.Visible = true;
            this.colArisingsChipOnSite.VisibleIndex = 48;
            this.colArisingsChipOnSite.Width = 115;
            // 
            // colArisingsStackOnSite
            // 
            this.colArisingsStackOnSite.Caption = "Arising Stack On Site";
            this.colArisingsStackOnSite.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colArisingsStackOnSite.FieldName = "ArisingsStackOnSite";
            this.colArisingsStackOnSite.Name = "colArisingsStackOnSite";
            this.colArisingsStackOnSite.OptionsColumn.AllowEdit = false;
            this.colArisingsStackOnSite.OptionsColumn.AllowFocus = false;
            this.colArisingsStackOnSite.OptionsColumn.ReadOnly = true;
            this.colArisingsStackOnSite.Visible = true;
            this.colArisingsStackOnSite.VisibleIndex = 49;
            this.colArisingsStackOnSite.Width = 120;
            // 
            // colArisingsOther
            // 
            this.colArisingsOther.Caption = "Arising Other";
            this.colArisingsOther.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colArisingsOther.FieldName = "ArisingsOther";
            this.colArisingsOther.Name = "colArisingsOther";
            this.colArisingsOther.OptionsColumn.AllowEdit = false;
            this.colArisingsOther.OptionsColumn.AllowFocus = false;
            this.colArisingsOther.OptionsColumn.ReadOnly = true;
            this.colArisingsOther.Visible = true;
            this.colArisingsOther.VisibleIndex = 50;
            this.colArisingsOther.Width = 84;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colRaisedByName
            // 
            this.colRaisedByName.Caption = "Raised By Name";
            this.colRaisedByName.FieldName = "RaisedByName";
            this.colRaisedByName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRaisedByName.Name = "colRaisedByName";
            this.colRaisedByName.OptionsColumn.AllowEdit = false;
            this.colRaisedByName.OptionsColumn.AllowFocus = false;
            this.colRaisedByName.OptionsColumn.ReadOnly = true;
            this.colRaisedByName.Visible = true;
            this.colRaisedByName.VisibleIndex = 1;
            this.colRaisedByName.Width = 98;
            // 
            // colSentByStaffName
            // 
            this.colSentByStaffName.Caption = "Sent By Staff Name";
            this.colSentByStaffName.FieldName = "SentByStaffName";
            this.colSentByStaffName.Name = "colSentByStaffName";
            this.colSentByStaffName.OptionsColumn.AllowEdit = false;
            this.colSentByStaffName.OptionsColumn.AllowFocus = false;
            this.colSentByStaffName.OptionsColumn.ReadOnly = true;
            this.colSentByStaffName.Visible = true;
            this.colSentByStaffName.VisibleIndex = 51;
            this.colSentByStaffName.Width = 115;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Linked Work Count";
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 14;
            this.colLinkedActionCount.Width = 111;
            // 
            // colLinkedToDoActionCount
            // 
            this.colLinkedToDoActionCount.Caption = "Linked To Do Work";
            this.colLinkedToDoActionCount.FieldName = "LinkedToDoActionCount";
            this.colLinkedToDoActionCount.Name = "colLinkedToDoActionCount";
            this.colLinkedToDoActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedToDoActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedToDoActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedToDoActionCount.Visible = true;
            this.colLinkedToDoActionCount.VisibleIndex = 15;
            this.colLinkedToDoActionCount.Width = 110;
            // 
            // colOwnerType
            // 
            this.colOwnerType.Caption = "Owner Type";
            this.colOwnerType.FieldName = "OwnerType";
            this.colOwnerType.Name = "colOwnerType";
            this.colOwnerType.OptionsColumn.AllowEdit = false;
            this.colOwnerType.OptionsColumn.AllowFocus = false;
            this.colOwnerType.OptionsColumn.ReadOnly = true;
            this.colOwnerType.Visible = true;
            this.colOwnerType.VisibleIndex = 10;
            this.colOwnerType.Width = 80;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 3;
            this.colClientName.Width = 97;
            // 
            // colDataEntryScreenName
            // 
            this.colDataEntryScreenName.Caption = "Data Entry Screen";
            this.colDataEntryScreenName.FieldName = "DataEntryScreenName";
            this.colDataEntryScreenName.Name = "colDataEntryScreenName";
            this.colDataEntryScreenName.OptionsColumn.AllowEdit = false;
            this.colDataEntryScreenName.OptionsColumn.AllowFocus = false;
            this.colDataEntryScreenName.OptionsColumn.ReadOnly = true;
            this.colDataEntryScreenName.Width = 109;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Report Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.Width = 120;
            // 
            // repositoryItemTextEdit0DP
            // 
            this.repositoryItemTextEdit0DP.AutoHeight = false;
            this.repositoryItemTextEdit0DP.Mask.EditMask = "n0";
            this.repositoryItemTextEdit0DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit0DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit0DP.Name = "repositoryItemTextEdit0DP";
            // 
            // xtraTabPage12
            // 
            this.xtraTabPage12.Controls.Add(this.gridControl9);
            this.xtraTabPage12.Name = "xtraTabPage12";
            this.xtraTabPage12.Size = new System.Drawing.Size(525, 168);
            this.xtraTabPage12.Text = "Work - Required Equipment";
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.sp07139UTSurveyedTreeWorkLinkedEquipmentReadOnlyBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl9.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl9.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl9_EmbeddedNavigator_ButtonClick);
            this.gridControl9.Location = new System.Drawing.Point(0, 0);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditHours,
            this.repositoryItemTextEditMoney,
            this.repositoryItemMemoExEdit8});
            this.gridControl9.Size = new System.Drawing.Size(525, 168);
            this.gridControl9.TabIndex = 3;
            this.gridControl9.UseEmbeddedNavigator = true;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // sp07139UTSurveyedTreeWorkLinkedEquipmentReadOnlyBindingSource
            // 
            this.sp07139UTSurveyedTreeWorkLinkedEquipmentReadOnlyBindingSource.DataMember = "sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_Only";
            this.sp07139UTSurveyedTreeWorkLinkedEquipmentReadOnlyBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionEquipmentID,
            this.gridColumn34,
            this.colEquipmentID,
            this.colCreatedFromDefaultRequiredEquipmentID,
            this.colEstimatedHours,
            this.colActualHours,
            this.colEstimatedCost,
            this.colActualCost,
            this.gridColumn35,
            this.gridColumn36,
            this.colEquipmentDescription,
            this.colLinkedToWork,
            this.colCostPerUnit,
            this.colSellPerUnit,
            this.colEstimatedSell,
            this.colActualSell});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.GroupCount = 1;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowFilterEditor = false;
            this.gridView9.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView9.OptionsFilter.AllowMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView9.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView9.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.MultiSelect = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToWork, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView9.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView9.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView9_SelectionChanged);
            this.gridView9.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView9.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView9.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView9_MouseUp);
            this.gridView9.GotFocus += new System.EventHandler(this.gridView9_GotFocus);
            // 
            // colActionEquipmentID
            // 
            this.colActionEquipmentID.Caption = "Action Equipment ID";
            this.colActionEquipmentID.FieldName = "ActionEquipmentID";
            this.colActionEquipmentID.Name = "colActionEquipmentID";
            this.colActionEquipmentID.OptionsColumn.AllowEdit = false;
            this.colActionEquipmentID.OptionsColumn.AllowFocus = false;
            this.colActionEquipmentID.OptionsColumn.ReadOnly = true;
            this.colActionEquipmentID.Width = 118;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Action ID";
            this.gridColumn34.FieldName = "ActionID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            // 
            // colEquipmentID
            // 
            this.colEquipmentID.Caption = "Equipment ID";
            this.colEquipmentID.FieldName = "EquipmentID";
            this.colEquipmentID.Name = "colEquipmentID";
            this.colEquipmentID.OptionsColumn.AllowEdit = false;
            this.colEquipmentID.OptionsColumn.AllowFocus = false;
            this.colEquipmentID.OptionsColumn.ReadOnly = true;
            this.colEquipmentID.Width = 85;
            // 
            // colCreatedFromDefaultRequiredEquipmentID
            // 
            this.colCreatedFromDefaultRequiredEquipmentID.Caption = "Created From Default Required Equipment ID";
            this.colCreatedFromDefaultRequiredEquipmentID.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colCreatedFromDefaultRequiredEquipmentID.FieldName = "CreatedFromDefaultRequiredEquipmentID";
            this.colCreatedFromDefaultRequiredEquipmentID.Name = "colCreatedFromDefaultRequiredEquipmentID";
            this.colCreatedFromDefaultRequiredEquipmentID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromDefaultRequiredEquipmentID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromDefaultRequiredEquipmentID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromDefaultRequiredEquipmentID.Width = 238;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 1;
            this.colEstimatedHours.Width = 99;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "######0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 2;
            this.colActualHours.Width = 87;
            // 
            // colEstimatedCost
            // 
            this.colEstimatedCost.Caption = "Estimated Cost";
            this.colEstimatedCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedCost.FieldName = "EstimatedCost";
            this.colEstimatedCost.Name = "colEstimatedCost";
            this.colEstimatedCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedCost.Visible = true;
            this.colEstimatedCost.VisibleIndex = 5;
            this.colEstimatedCost.Width = 93;
            // 
            // repositoryItemTextEditMoney
            // 
            this.repositoryItemTextEditMoney.AutoHeight = false;
            this.repositoryItemTextEditMoney.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney.Name = "repositoryItemTextEditMoney";
            // 
            // colActualCost
            // 
            this.colActualCost.Caption = "Actual Cost";
            this.colActualCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualCost.FieldName = "ActualCost";
            this.colActualCost.Name = "colActualCost";
            this.colActualCost.OptionsColumn.AllowEdit = false;
            this.colActualCost.OptionsColumn.AllowFocus = false;
            this.colActualCost.OptionsColumn.ReadOnly = true;
            this.colActualCost.Visible = true;
            this.colActualCost.VisibleIndex = 6;
            this.colActualCost.Width = 91;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Remarks";
            this.gridColumn35.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.gridColumn35.FieldName = "Remarks";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "GUID";
            this.gridColumn36.FieldName = "GUID";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            // 
            // colEquipmentDescription
            // 
            this.colEquipmentDescription.Caption = "Equipment";
            this.colEquipmentDescription.FieldName = "EquipmentDescription";
            this.colEquipmentDescription.Name = "colEquipmentDescription";
            this.colEquipmentDescription.OptionsColumn.AllowEdit = false;
            this.colEquipmentDescription.OptionsColumn.AllowFocus = false;
            this.colEquipmentDescription.OptionsColumn.ReadOnly = true;
            this.colEquipmentDescription.Visible = true;
            this.colEquipmentDescription.VisibleIndex = 0;
            this.colEquipmentDescription.Width = 304;
            // 
            // colLinkedToWork
            // 
            this.colLinkedToWork.Caption = "Linked To Tree Work";
            this.colLinkedToWork.FieldName = "LinkedToWork";
            this.colLinkedToWork.Name = "colLinkedToWork";
            this.colLinkedToWork.OptionsColumn.AllowEdit = false;
            this.colLinkedToWork.OptionsColumn.AllowFocus = false;
            this.colLinkedToWork.OptionsColumn.ReadOnly = true;
            this.colLinkedToWork.Visible = true;
            this.colLinkedToWork.VisibleIndex = 6;
            this.colLinkedToWork.Width = 285;
            // 
            // colCostPerUnit
            // 
            this.colCostPerUnit.Caption = "Cost Per Hour";
            this.colCostPerUnit.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colCostPerUnit.FieldName = "CostPerUnit";
            this.colCostPerUnit.Name = "colCostPerUnit";
            this.colCostPerUnit.OptionsColumn.AllowEdit = false;
            this.colCostPerUnit.OptionsColumn.AllowFocus = false;
            this.colCostPerUnit.OptionsColumn.ReadOnly = true;
            this.colCostPerUnit.Visible = true;
            this.colCostPerUnit.VisibleIndex = 3;
            this.colCostPerUnit.Width = 84;
            // 
            // colSellPerUnit
            // 
            this.colSellPerUnit.Caption = "Sell Per Hour";
            this.colSellPerUnit.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colSellPerUnit.FieldName = "SellPerUnit";
            this.colSellPerUnit.Name = "colSellPerUnit";
            this.colSellPerUnit.OptionsColumn.AllowEdit = false;
            this.colSellPerUnit.OptionsColumn.AllowFocus = false;
            this.colSellPerUnit.OptionsColumn.ReadOnly = true;
            this.colSellPerUnit.Visible = true;
            this.colSellPerUnit.VisibleIndex = 4;
            this.colSellPerUnit.Width = 78;
            // 
            // colEstimatedSell
            // 
            this.colEstimatedSell.Caption = "Estimated Sell";
            this.colEstimatedSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedSell.FieldName = "EstimatedSell";
            this.colEstimatedSell.Name = "colEstimatedSell";
            this.colEstimatedSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedSell.Visible = true;
            this.colEstimatedSell.VisibleIndex = 7;
            this.colEstimatedSell.Width = 87;
            // 
            // colActualSell
            // 
            this.colActualSell.Caption = "Actual Sell";
            this.colActualSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualSell.FieldName = "ActualSell";
            this.colActualSell.Name = "colActualSell";
            this.colActualSell.OptionsColumn.AllowEdit = false;
            this.colActualSell.OptionsColumn.AllowFocus = false;
            this.colActualSell.OptionsColumn.ReadOnly = true;
            this.colActualSell.Visible = true;
            this.colActualSell.VisibleIndex = 8;
            // 
            // xtraTabPage13
            // 
            this.xtraTabPage13.Controls.Add(this.gridControl10);
            this.xtraTabPage13.Name = "xtraTabPage13";
            this.xtraTabPage13.Size = new System.Drawing.Size(525, 168);
            this.xtraTabPage13.Text = "Work - Required Materials";
            // 
            // gridControl10
            // 
            this.gridControl10.DataSource = this.sp07140UTSurveyedTreeWorkLinkedMaterialsReadOnlyBindingSource;
            this.gridControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl10.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl10.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl10_EmbeddedNavigator_ButtonClick);
            this.gridControl10.Location = new System.Drawing.Point(0, 0);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditNumeric2DP,
            this.repositoryItemTextEditMoney2,
            this.repositoryItemMemoExEdit9});
            this.gridControl10.Size = new System.Drawing.Size(525, 168);
            this.gridControl10.TabIndex = 3;
            this.gridControl10.UseEmbeddedNavigator = true;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp07140UTSurveyedTreeWorkLinkedMaterialsReadOnlyBindingSource
            // 
            this.sp07140UTSurveyedTreeWorkLinkedMaterialsReadOnlyBindingSource.DataMember = "sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_Only";
            this.sp07140UTSurveyedTreeWorkLinkedMaterialsReadOnlyBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionMaterialID,
            this.colActionID1,
            this.colMaterialID,
            this.colEstimatedUnits,
            this.colActualUnits,
            this.colUnitsDescriptorID,
            this.colEstimatedCost1,
            this.colActualCost1,
            this.gridColumn37,
            this.gridColumn38,
            this.colMaterialDescription,
            this.colLinkedToWork1,
            this.colUnitsDescriptor,
            this.colCostPerUnit1,
            this.colSellPerUnit1,
            this.colEstimatedSell1,
            this.colActualSell1});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.GroupCount = 1;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowFilterEditor = false;
            this.gridView10.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView10.OptionsFilter.AllowMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView10.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView10.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.MultiSelect = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToWork1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView10.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView10.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView10_SelectionChanged);
            this.gridView10.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView10.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView10.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView10_MouseUp);
            this.gridView10.GotFocus += new System.EventHandler(this.gridView10_GotFocus);
            // 
            // colActionMaterialID
            // 
            this.colActionMaterialID.Caption = "Action Material ID";
            this.colActionMaterialID.FieldName = "ActionMaterialID";
            this.colActionMaterialID.Name = "colActionMaterialID";
            this.colActionMaterialID.OptionsColumn.AllowEdit = false;
            this.colActionMaterialID.OptionsColumn.AllowFocus = false;
            this.colActionMaterialID.OptionsColumn.ReadOnly = true;
            this.colActionMaterialID.Width = 106;
            // 
            // colActionID1
            // 
            this.colActionID1.Caption = "Action ID";
            this.colActionID1.FieldName = "ActionID";
            this.colActionID1.Name = "colActionID1";
            this.colActionID1.OptionsColumn.AllowEdit = false;
            this.colActionID1.OptionsColumn.AllowFocus = false;
            this.colActionID1.OptionsColumn.ReadOnly = true;
            // 
            // colMaterialID
            // 
            this.colMaterialID.Caption = "Material ID";
            this.colMaterialID.FieldName = "MaterialID";
            this.colMaterialID.Name = "colMaterialID";
            this.colMaterialID.OptionsColumn.AllowEdit = false;
            this.colMaterialID.OptionsColumn.AllowFocus = false;
            this.colMaterialID.OptionsColumn.ReadOnly = true;
            // 
            // colEstimatedUnits
            // 
            this.colEstimatedUnits.Caption = "Estimated Units";
            this.colEstimatedUnits.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colEstimatedUnits.FieldName = "EstimatedUnits";
            this.colEstimatedUnits.Name = "colEstimatedUnits";
            this.colEstimatedUnits.OptionsColumn.AllowEdit = false;
            this.colEstimatedUnits.OptionsColumn.AllowFocus = false;
            this.colEstimatedUnits.OptionsColumn.ReadOnly = true;
            this.colEstimatedUnits.Visible = true;
            this.colEstimatedUnits.VisibleIndex = 1;
            this.colEstimatedUnits.Width = 95;
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // colActualUnits
            // 
            this.colActualUnits.Caption = "Actual Units";
            this.colActualUnits.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colActualUnits.FieldName = "ActualUnits";
            this.colActualUnits.Name = "colActualUnits";
            this.colActualUnits.OptionsColumn.AllowEdit = false;
            this.colActualUnits.OptionsColumn.AllowFocus = false;
            this.colActualUnits.OptionsColumn.ReadOnly = true;
            this.colActualUnits.Visible = true;
            this.colActualUnits.VisibleIndex = 2;
            this.colActualUnits.Width = 78;
            // 
            // colUnitsDescriptorID
            // 
            this.colUnitsDescriptorID.Caption = "Units Descriptor ID";
            this.colUnitsDescriptorID.FieldName = "UnitsDescriptorID";
            this.colUnitsDescriptorID.Name = "colUnitsDescriptorID";
            this.colUnitsDescriptorID.OptionsColumn.AllowEdit = false;
            this.colUnitsDescriptorID.OptionsColumn.AllowFocus = false;
            this.colUnitsDescriptorID.OptionsColumn.ReadOnly = true;
            this.colUnitsDescriptorID.Width = 111;
            // 
            // colEstimatedCost1
            // 
            this.colEstimatedCost1.Caption = "Estimated Cost";
            this.colEstimatedCost1.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedCost1.FieldName = "EstimatedCost";
            this.colEstimatedCost1.Name = "colEstimatedCost1";
            this.colEstimatedCost1.OptionsColumn.AllowEdit = false;
            this.colEstimatedCost1.OptionsColumn.AllowFocus = false;
            this.colEstimatedCost1.OptionsColumn.ReadOnly = true;
            this.colEstimatedCost1.Visible = true;
            this.colEstimatedCost1.VisibleIndex = 6;
            this.colEstimatedCost1.Width = 93;
            // 
            // repositoryItemTextEditMoney2
            // 
            this.repositoryItemTextEditMoney2.AutoHeight = false;
            this.repositoryItemTextEditMoney2.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney2.Name = "repositoryItemTextEditMoney2";
            // 
            // colActualCost1
            // 
            this.colActualCost1.Caption = "Actual Cost";
            this.colActualCost1.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualCost1.FieldName = "ActualCost";
            this.colActualCost1.Name = "colActualCost1";
            this.colActualCost1.OptionsColumn.AllowEdit = false;
            this.colActualCost1.OptionsColumn.AllowFocus = false;
            this.colActualCost1.OptionsColumn.ReadOnly = true;
            this.colActualCost1.Visible = true;
            this.colActualCost1.VisibleIndex = 7;
            this.colActualCost1.Width = 88;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Remarks";
            this.gridColumn37.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.gridColumn37.FieldName = "Remarks";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "GUID";
            this.gridColumn38.FieldName = "GUID";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            // 
            // colMaterialDescription
            // 
            this.colMaterialDescription.Caption = "Material";
            this.colMaterialDescription.FieldName = "MaterialDescription";
            this.colMaterialDescription.Name = "colMaterialDescription";
            this.colMaterialDescription.OptionsColumn.AllowEdit = false;
            this.colMaterialDescription.OptionsColumn.AllowFocus = false;
            this.colMaterialDescription.OptionsColumn.ReadOnly = true;
            this.colMaterialDescription.Visible = true;
            this.colMaterialDescription.VisibleIndex = 0;
            this.colMaterialDescription.Width = 231;
            // 
            // colLinkedToWork1
            // 
            this.colLinkedToWork1.Caption = "Linked To Tree Work";
            this.colLinkedToWork1.FieldName = "LinkedToWork";
            this.colLinkedToWork1.Name = "colLinkedToWork1";
            this.colLinkedToWork1.OptionsColumn.AllowEdit = false;
            this.colLinkedToWork1.OptionsColumn.AllowFocus = false;
            this.colLinkedToWork1.OptionsColumn.ReadOnly = true;
            this.colLinkedToWork1.Visible = true;
            this.colLinkedToWork1.VisibleIndex = 7;
            this.colLinkedToWork1.Width = 240;
            // 
            // colUnitsDescriptor
            // 
            this.colUnitsDescriptor.FieldName = "UnitsDescriptor";
            this.colUnitsDescriptor.Name = "colUnitsDescriptor";
            this.colUnitsDescriptor.Visible = true;
            this.colUnitsDescriptor.VisibleIndex = 5;
            this.colUnitsDescriptor.Width = 97;
            // 
            // colCostPerUnit1
            // 
            this.colCostPerUnit1.Caption = "Cost Per Unit";
            this.colCostPerUnit1.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostPerUnit1.FieldName = "CostPerUnit";
            this.colCostPerUnit1.Name = "colCostPerUnit1";
            this.colCostPerUnit1.OptionsColumn.AllowEdit = false;
            this.colCostPerUnit1.OptionsColumn.AllowFocus = false;
            this.colCostPerUnit1.OptionsColumn.ReadOnly = true;
            this.colCostPerUnit1.Visible = true;
            this.colCostPerUnit1.VisibleIndex = 3;
            this.colCostPerUnit1.Width = 84;
            // 
            // colSellPerUnit1
            // 
            this.colSellPerUnit1.Caption = "Sell Per Unit";
            this.colSellPerUnit1.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellPerUnit1.FieldName = "SellPerUnit";
            this.colSellPerUnit1.Name = "colSellPerUnit1";
            this.colSellPerUnit1.OptionsColumn.AllowEdit = false;
            this.colSellPerUnit1.OptionsColumn.AllowFocus = false;
            this.colSellPerUnit1.OptionsColumn.ReadOnly = true;
            this.colSellPerUnit1.Visible = true;
            this.colSellPerUnit1.VisibleIndex = 4;
            this.colSellPerUnit1.Width = 78;
            // 
            // colEstimatedSell1
            // 
            this.colEstimatedSell1.Caption = "Estimated Sell";
            this.colEstimatedSell1.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colEstimatedSell1.FieldName = "EstimatedSell";
            this.colEstimatedSell1.Name = "colEstimatedSell1";
            this.colEstimatedSell1.OptionsColumn.AllowEdit = false;
            this.colEstimatedSell1.OptionsColumn.AllowFocus = false;
            this.colEstimatedSell1.OptionsColumn.ReadOnly = true;
            this.colEstimatedSell1.Visible = true;
            this.colEstimatedSell1.VisibleIndex = 8;
            this.colEstimatedSell1.Width = 87;
            // 
            // colActualSell1
            // 
            this.colActualSell1.Caption = "Actual Sell";
            this.colActualSell1.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualSell1.FieldName = "ActualSell";
            this.colActualSell1.Name = "colActualSell1";
            this.colActualSell1.OptionsColumn.AllowEdit = false;
            this.colActualSell1.OptionsColumn.AllowFocus = false;
            this.colActualSell1.OptionsColumn.ReadOnly = true;
            this.colActualSell1.Visible = true;
            this.colActualSell1.VisibleIndex = 9;
            // 
            // xtraTabPage15
            // 
            this.xtraTabPage15.Controls.Add(this.gridControl7);
            this.xtraTabPage15.Name = "xtraTabPage15";
            this.xtraTabPage15.Size = new System.Drawing.Size(530, 388);
            this.xtraTabPage15.Text = "Historical Work";
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp07149UTSurveyedPoleWorkHistoricalListBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEditHours2,
            this.repositoryItemTextEditMoney3,
            this.repositoryItemMemoExEdit10});
            this.gridControl7.Size = new System.Drawing.Size(530, 388);
            this.gridControl7.TabIndex = 3;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp07149UTSurveyedPoleWorkHistoricalListBindingSource
            // 
            this.sp07149UTSurveyedPoleWorkHistoricalListBindingSource.DataMember = "sp07149_UT_Surveyed_Pole_Work_Historical_List";
            this.sp07149UTSurveyedPoleWorkHistoricalListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn67,
            this.gridColumn69,
            this.gridColumn82,
            this.gridColumn83,
            this.gridColumn84,
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87,
            this.gridColumn88,
            this.gridColumn89,
            this.colActualHours2,
            this.colEstimatedHours2,
            this.colPossibleLiveWork1});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn67, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn43, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn69, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView7_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Action ID";
            this.gridColumn39.FieldName = "ActionID";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Surveyed Tree ID";
            this.gridColumn40.FieldName = "SurveyedTreeID";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Width = 106;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Job Type ID";
            this.gridColumn41.FieldName = "JobTypeID";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 79;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Reference Number";
            this.gridColumn42.FieldName = "ReferenceNumber";
            this.gridColumn42.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 1;
            this.gridColumn42.Width = 124;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Date Raised";
            this.gridColumn43.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.gridColumn43.FieldName = "DateRaised";
            this.gridColumn43.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 2;
            this.gridColumn43.Width = 92;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Date Scheduled";
            this.gridColumn44.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.gridColumn44.FieldName = "DateScheduled";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 3;
            this.gridColumn44.Width = 96;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Date Completed";
            this.gridColumn45.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.gridColumn45.FieldName = "DateCompleted";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 4;
            this.gridColumn45.Width = 98;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Approved";
            this.gridColumn46.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn46.FieldName = "Approved";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 5;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Apporved By Staff ID";
            this.gridColumn47.FieldName = "ApprovedByStaffID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Width = 124;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Est. Labour Cost";
            this.gridColumn54.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn54.FieldName = "EstimatedLabourCost";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 13;
            this.gridColumn54.Width = 101;
            // 
            // repositoryItemTextEditMoney3
            // 
            this.repositoryItemTextEditMoney3.AutoHeight = false;
            this.repositoryItemTextEditMoney3.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney3.Name = "repositoryItemTextEditMoney3";
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Act. Labour Cost";
            this.gridColumn55.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn55.FieldName = "ActualLabourCost";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 14;
            this.gridColumn55.Width = 102;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Est. Material Cost";
            this.gridColumn56.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn56.FieldName = "EstimatedMaterialsCost";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 21;
            this.gridColumn56.Width = 106;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Act. Material Cost";
            this.gridColumn57.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn57.FieldName = "ActualMaterialsCost";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 22;
            this.gridColumn57.Width = 107;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Est. Equip. Cost";
            this.gridColumn58.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn58.FieldName = "EstimatedEquipmentCost";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 17;
            this.gridColumn58.Width = 98;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Act. Equip. Cost";
            this.gridColumn59.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn59.FieldName = "ActualEquipmentCost";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 18;
            this.gridColumn59.Width = 99;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Est. Total Cost";
            this.gridColumn60.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn60.FieldName = "EstimatedTotalCost";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 9;
            this.gridColumn60.Width = 92;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Act. Total Cost";
            this.gridColumn61.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn61.FieldName = "ActualTotalCost";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.Visible = true;
            this.gridColumn61.VisibleIndex = 10;
            this.gridColumn61.Width = 93;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Work Order";
            this.gridColumn62.FieldName = "WorkOrderID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.Visible = true;
            this.gridColumn62.VisibleIndex = 25;
            this.gridColumn62.Width = 77;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Self Billing ID";
            this.gridColumn63.FieldName = "SelfBillingInvoiceID";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 26;
            this.gridColumn63.Width = 82;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Finance Billing ID";
            this.gridColumn64.FieldName = "FinanceSystemBillingID";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 27;
            this.gridColumn64.Width = 101;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Remarks";
            this.gridColumn65.ColumnEdit = this.repositoryItemMemoExEdit10;
            this.gridColumn65.FieldName = "Remarks";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Visible = true;
            this.gridColumn65.VisibleIndex = 28;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "GUID";
            this.gridColumn66.FieldName = "GUID";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Linked To Tree";
            this.gridColumn67.FieldName = "LinkedRecordDescription";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Visible = true;
            this.gridColumn67.VisibleIndex = 17;
            this.gridColumn67.Width = 379;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Job Description";
            this.gridColumn69.FieldName = "JobDescription";
            this.gridColumn69.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Visible = true;
            this.gridColumn69.VisibleIndex = 0;
            this.gridColumn69.Width = 223;
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Est. Total Sell";
            this.gridColumn82.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn82.FieldName = "EstimatedTotalSell";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.Visible = true;
            this.gridColumn82.VisibleIndex = 11;
            this.gridColumn82.Width = 86;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Act. Total Sell";
            this.gridColumn83.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn83.FieldName = "ActualTotalSell";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Visible = true;
            this.gridColumn83.VisibleIndex = 12;
            this.gridColumn83.Width = 87;
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Est. Labour Sell";
            this.gridColumn84.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn84.FieldName = "EstimatedLabourSell";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Visible = true;
            this.gridColumn84.VisibleIndex = 15;
            this.gridColumn84.Width = 95;
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Act. Labour Sell";
            this.gridColumn85.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn85.FieldName = "ActualLabourSell";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Visible = true;
            this.gridColumn85.VisibleIndex = 16;
            this.gridColumn85.Width = 96;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Est. Equip. Sell";
            this.gridColumn86.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn86.FieldName = "EstimatedEquipmentSell";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.Visible = true;
            this.gridColumn86.VisibleIndex = 19;
            this.gridColumn86.Width = 92;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Act. Equip. Sell";
            this.gridColumn87.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn87.FieldName = "ActualEquipmentSell";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Visible = true;
            this.gridColumn87.VisibleIndex = 20;
            this.gridColumn87.Width = 93;
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Est. Material Sell";
            this.gridColumn88.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn88.FieldName = "EstimatedMaterialsSell";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Visible = true;
            this.gridColumn88.VisibleIndex = 23;
            this.gridColumn88.Width = 100;
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Act. Material Sell";
            this.gridColumn89.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn89.FieldName = "ActualMaterialsSell";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Visible = true;
            this.gridColumn89.VisibleIndex = 24;
            this.gridColumn89.Width = 101;
            // 
            // colActualHours2
            // 
            this.colActualHours2.Caption = "Actual Hours";
            this.colActualHours2.ColumnEdit = this.repositoryItemTextEditHours2;
            this.colActualHours2.FieldName = "ActualHours";
            this.colActualHours2.Name = "colActualHours2";
            this.colActualHours2.OptionsColumn.AllowEdit = false;
            this.colActualHours2.OptionsColumn.AllowFocus = false;
            this.colActualHours2.OptionsColumn.ReadOnly = true;
            this.colActualHours2.Visible = true;
            this.colActualHours2.VisibleIndex = 8;
            this.colActualHours2.Width = 82;
            // 
            // repositoryItemTextEditHours2
            // 
            this.repositoryItemTextEditHours2.AutoHeight = false;
            this.repositoryItemTextEditHours2.Mask.EditMask = "######0.00 Hours";
            this.repositoryItemTextEditHours2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours2.Name = "repositoryItemTextEditHours2";
            // 
            // colEstimatedHours2
            // 
            this.colEstimatedHours2.Caption = "Estimated Hours";
            this.colEstimatedHours2.ColumnEdit = this.repositoryItemTextEditHours2;
            this.colEstimatedHours2.FieldName = "EstimatedHours";
            this.colEstimatedHours2.Name = "colEstimatedHours2";
            this.colEstimatedHours2.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours2.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours2.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours2.Visible = true;
            this.colEstimatedHours2.VisibleIndex = 7;
            this.colEstimatedHours2.Width = 99;
            // 
            // colPossibleLiveWork1
            // 
            this.colPossibleLiveWork1.Caption = "Possible Live Work";
            this.colPossibleLiveWork1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colPossibleLiveWork1.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork1.Name = "colPossibleLiveWork1";
            this.colPossibleLiveWork1.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork1.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork1.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork1.Visible = true;
            this.colPossibleLiveWork1.VisibleIndex = 6;
            this.colPossibleLiveWork1.Width = 109;
            // 
            // xtraTabPageTreeDefects
            // 
            this.xtraTabPageTreeDefects.Controls.Add(this.splitContainerControl1);
            this.xtraTabPageTreeDefects.Name = "xtraTabPageTreeDefects";
            this.xtraTabPageTreeDefects.Size = new System.Drawing.Size(557, 393);
            this.xtraTabPageTreeDefects.Text = "Tree Defects";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl12);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl7);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(557, 393);
            this.splitContainerControl1.SplitterPosition = 200;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl12
            // 
            this.gridControl12.DataSource = this.sp07150UTSurveyedTreeDefectListBindingSource;
            this.gridControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl12.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl12.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl12_EmbeddedNavigator_ButtonClick);
            this.gridControl12.Location = new System.Drawing.Point(0, 0);
            this.gridControl12.MainView = this.gridView12;
            this.gridControl12.MenuManager = this.barManager1;
            this.gridControl12.Name = "gridControl12";
            this.gridControl12.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11});
            this.gridControl12.Size = new System.Drawing.Size(557, 187);
            this.gridControl12.TabIndex = 3;
            this.gridControl12.UseEmbeddedNavigator = true;
            this.gridControl12.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12});
            // 
            // sp07150UTSurveyedTreeDefectListBindingSource
            // 
            this.sp07150UTSurveyedTreeDefectListBindingSource.DataMember = "sp07150_UT_Surveyed_Tree_Defect_List";
            this.sp07150UTSurveyedTreeDefectListBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeDefectID,
            this.colSurveyedTreeID2,
            this.colDefectID,
            this.colRemarks4,
            this.colGUID4,
            this.colLinkedRecordDescription2,
            this.colDefectDescription});
            this.gridView12.GridControl = this.gridControl12;
            this.gridView12.GroupCount = 1;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsSelection.MultiSelect = true;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDefectDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView12.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView12.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView12_SelectionChanged);
            this.gridView12.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView12.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView12.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView12_MouseUp);
            this.gridView12.DoubleClick += new System.EventHandler(this.gridView12_DoubleClick);
            this.gridView12.GotFocus += new System.EventHandler(this.gridView12_GotFocus);
            // 
            // colTreeDefectID
            // 
            this.colTreeDefectID.Caption = "Tree Defect ID";
            this.colTreeDefectID.FieldName = "TreeDefectID";
            this.colTreeDefectID.Name = "colTreeDefectID";
            this.colTreeDefectID.OptionsColumn.AllowEdit = false;
            this.colTreeDefectID.OptionsColumn.AllowFocus = false;
            this.colTreeDefectID.OptionsColumn.ReadOnly = true;
            this.colTreeDefectID.Width = 92;
            // 
            // colSurveyedTreeID2
            // 
            this.colSurveyedTreeID2.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID2.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID2.Name = "colSurveyedTreeID2";
            this.colSurveyedTreeID2.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID2.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID2.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID2.Width = 106;
            // 
            // colDefectID
            // 
            this.colDefectID.Caption = "Defect ID";
            this.colDefectID.FieldName = "DefectID";
            this.colDefectID.Name = "colDefectID";
            this.colDefectID.OptionsColumn.AllowEdit = false;
            this.colDefectID.OptionsColumn.AllowFocus = false;
            this.colDefectID.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit11;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 1;
            this.colRemarks4.Width = 233;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // colGUID4
            // 
            this.colGUID4.Caption = "GUID";
            this.colGUID4.FieldName = "GUID";
            this.colGUID4.Name = "colGUID4";
            this.colGUID4.OptionsColumn.AllowEdit = false;
            this.colGUID4.OptionsColumn.AllowFocus = false;
            this.colGUID4.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedRecordDescription2
            // 
            this.colLinkedRecordDescription2.Caption = "Linked To Tree";
            this.colLinkedRecordDescription2.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription2.Name = "colLinkedRecordDescription2";
            this.colLinkedRecordDescription2.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription2.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription2.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription2.Visible = true;
            this.colLinkedRecordDescription2.VisibleIndex = 3;
            this.colLinkedRecordDescription2.Width = 405;
            // 
            // colDefectDescription
            // 
            this.colDefectDescription.Caption = "Defect Description";
            this.colDefectDescription.FieldName = "DefectDescription";
            this.colDefectDescription.Name = "colDefectDescription";
            this.colDefectDescription.OptionsColumn.AllowEdit = false;
            this.colDefectDescription.OptionsColumn.AllowFocus = false;
            this.colDefectDescription.OptionsColumn.ReadOnly = true;
            this.colDefectDescription.Visible = true;
            this.colDefectDescription.VisibleIndex = 0;
            this.colDefectDescription.Width = 358;
            // 
            // xtraTabControl7
            // 
            this.xtraTabControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl7.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl7.Name = "xtraTabControl7";
            this.xtraTabControl7.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl7.Size = new System.Drawing.Size(557, 200);
            this.xtraTabControl7.TabIndex = 0;
            this.xtraTabControl7.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl13);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(552, 174);
            this.xtraTabPage1.Text = "Tree Defect Pictures";
            // 
            // gridControl13
            // 
            this.gridControl13.DataSource = this.sp07154UTSurveyedTreeDefectPicturesListBindingSource;
            this.gridControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl13.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl13.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl13_EmbeddedNavigator_ButtonClick);
            this.gridControl13.Location = new System.Drawing.Point(0, 0);
            this.gridControl13.MainView = this.gridView13;
            this.gridControl13.MenuManager = this.barManager1;
            this.gridControl13.Name = "gridControl13";
            this.gridControl13.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit12,
            this.repositoryItemHyperLinkEdit5,
            this.repositoryItemTextEdit4});
            this.gridControl13.Size = new System.Drawing.Size(552, 174);
            this.gridControl13.TabIndex = 4;
            this.gridControl13.UseEmbeddedNavigator = true;
            this.gridControl13.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView13});
            // 
            // sp07154UTSurveyedTreeDefectPicturesListBindingSource
            // 
            this.sp07154UTSurveyedTreeDefectPicturesListBindingSource.DataMember = "sp07154_UT_Surveyed_Tree_Defect_Pictures_List";
            this.sp07154UTSurveyedTreeDefectPicturesListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.gridColumn77,
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81});
            this.gridView13.GridControl = this.gridControl13;
            this.gridView13.GroupCount = 1;
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsSelection.MultiSelect = true;
            this.gridView13.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn81, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn75, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView13.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView13.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView13_SelectionChanged);
            this.gridView13.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView13.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView13.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView13_MouseUp);
            this.gridView13.GotFocus += new System.EventHandler(this.gridView13_GotFocus);
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Survey Picture ID";
            this.gridColumn70.FieldName = "SurveyPictureID";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Width = 105;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Linked To Record ID";
            this.gridColumn71.FieldName = "LinkedToRecordID";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Width = 117;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Linked To Record Type ID";
            this.gridColumn72.FieldName = "LinkedToRecordTypeID";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Width = 144;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Picture Type ID";
            this.gridColumn73.FieldName = "PictureTypeID";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Width = 95;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Picture Path";
            this.gridColumn74.ColumnEdit = this.repositoryItemHyperLinkEdit5;
            this.gridColumn74.FieldName = "PicturePath";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Visible = true;
            this.gridColumn74.VisibleIndex = 2;
            this.gridColumn74.Width = 323;
            // 
            // repositoryItemHyperLinkEdit5
            // 
            this.repositoryItemHyperLinkEdit5.AutoHeight = false;
            this.repositoryItemHyperLinkEdit5.Name = "repositoryItemHyperLinkEdit5";
            this.repositoryItemHyperLinkEdit5.SingleClick = true;
            this.repositoryItemHyperLinkEdit5.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit5_OpenLink);
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Date Taken";
            this.gridColumn75.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn75.FieldName = "DateTimeTaken";
            this.gridColumn75.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn75.Visible = true;
            this.gridColumn75.VisibleIndex = 0;
            this.gridColumn75.Width = 99;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "g";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Added By Staff ID";
            this.gridColumn76.FieldName = "AddedByStaffID";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Width = 108;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "GUID";
            this.gridColumn77.FieldName = "GUID";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Remarks";
            this.gridColumn78.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.gridColumn78.FieldName = "Remarks";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.Visible = true;
            this.gridColumn78.VisibleIndex = 3;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Linked To Survey";
            this.gridColumn79.FieldName = "LinkedRecordDescription";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.Width = 303;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Added By";
            this.gridColumn80.FieldName = "AddedByStaffName";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Visible = true;
            this.gridColumn80.VisibleIndex = 1;
            this.gridColumn80.Width = 108;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Linked To Tree";
            this.gridColumn81.FieldName = "ShortLinkedRecordDescription";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Visible = true;
            this.gridColumn81.VisibleIndex = 4;
            this.gridColumn81.Width = 231;
            // 
            // panelControl5
            // 
            this.panelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl5.Controls.Add(this.checkEditNoWorkRequired);
            this.panelControl5.Controls.Add(this.pictureEdit8);
            this.panelControl5.Controls.Add(this.labelControl5);
            this.panelControl5.Location = new System.Drawing.Point(4, 4);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1079, 32);
            this.panelControl5.TabIndex = 21;
            // 
            // checkEditNoWorkRequired
            // 
            this.checkEditNoWorkRequired.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "NoWorkRequired", true));
            this.checkEditNoWorkRequired.Location = new System.Drawing.Point(662, -1);
            this.checkEditNoWorkRequired.MenuManager = this.barManager1;
            this.checkEditNoWorkRequired.Name = "checkEditNoWorkRequired";
            this.checkEditNoWorkRequired.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.checkEditNoWorkRequired.Properties.Appearance.Options.UseForeColor = true;
            this.checkEditNoWorkRequired.Properties.Caption = "(No Work Required)";
            this.checkEditNoWorkRequired.Properties.ValueChecked = 1;
            this.checkEditNoWorkRequired.Properties.ValueUnchecked = 0;
            this.checkEditNoWorkRequired.Size = new System.Drawing.Size(139, 19);
            this.checkEditNoWorkRequired.TabIndex = 22;
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.EditValue = ((object)(resources.GetObject("pictureEdit8.EditValue")));
            this.pictureEdit8.Location = new System.Drawing.Point(5, 5);
            this.pictureEdit8.MenuManager = this.barManager1;
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit8.Size = new System.Drawing.Size(20, 20);
            this.pictureEdit8.TabIndex = 19;
            // 
            // labelControl5
            // 
            this.labelControl5.AllowHtmlString = true;
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.labelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl5.AutoEllipsis = true;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(29, 2);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(1044, 27);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = resources.GetString("labelControl5.Text");
            // 
            // xtraTabPageRiskAssessment
            // 
            this.xtraTabPageRiskAssessment.Controls.Add(this.splitContainerControl6);
            this.xtraTabPageRiskAssessment.Controls.Add(this.panelControl7);
            this.xtraTabPageRiskAssessment.Name = "xtraTabPageRiskAssessment";
            this.xtraTabPageRiskAssessment.Size = new System.Drawing.Size(1087, 464);
            this.xtraTabPageRiskAssessment.Text = "Risk Assessment";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl6.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl6.Location = new System.Drawing.Point(4, 39);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.gridControl22);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.gridControl23);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(1079, 422);
            this.splitContainerControl6.SplitterPosition = 465;
            this.splitContainerControl6.TabIndex = 23;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // gridControl22
            // 
            this.gridControl22.DataSource = this.sp07254UTSurveyedPoleRiskAssessmentListBindingSource;
            this.gridControl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl22.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl22.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl22.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl22.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl22.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl22.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl22.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Site Specific Risk Assessment", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl22.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl22_EmbeddedNavigator_ButtonClick);
            this.gridControl22.Location = new System.Drawing.Point(0, 0);
            this.gridControl22.MainView = this.gridView22;
            this.gridControl22.MenuManager = this.barManager1;
            this.gridControl22.Name = "gridControl22";
            this.gridControl22.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit2,
            this.repositoryItemMemoExEdit21,
            this.repositoryItemCheckEdit4});
            this.gridControl22.Size = new System.Drawing.Size(465, 422);
            this.gridControl22.TabIndex = 24;
            this.gridControl22.UseEmbeddedNavigator = true;
            this.gridControl22.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView22});
            // 
            // sp07254UTSurveyedPoleRiskAssessmentListBindingSource
            // 
            this.sp07254UTSurveyedPoleRiskAssessmentListBindingSource.DataMember = "sp07254_UT_Surveyed_Pole_Risk_Assessment_List";
            this.sp07254UTSurveyedPoleRiskAssessmentListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView22
            // 
            this.gridView22.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRiskAssessmentID,
            this.colSurveyedPoleID1,
            this.colDateTimeRecorded,
            this.colPersonCompletingTypeID,
            this.colPersonCompletingID,
            this.colPermissionChecked,
            this.colElectricalAssessmentVerified,
            this.colElectricalRiskCategoryID,
            this.colTypeOfWorkingID,
            this.colPermitHolderName,
            this.colNRSWAOpeningNoticeNo,
            this.colRemarks6,
            this.colGUID6,
            this.colRiskAssessmentTypeID,
            this.colLastUpdated,
            this.colElectricalRiskCategory,
            this.colTypeOfWorking,
            this.colPersonCompletingType,
            this.colPersonCompletingName,
            this.colRiskAssessmentType,
            this.colSiteAddress,
            this.colMEWPArialRescuers,
            this.colEmergencyKitLocation,
            this.colAirAmbulanceLat,
            this.colAirAmbulanceLong,
            this.colBanksMan,
            this.colEmergencyServicesMeetPoint,
            this.colMobilePhoneSignalBars,
            this.colNearestAandE,
            this.colNearestLandLine});
            this.gridView22.GridControl = this.gridControl22;
            this.gridView22.Name = "gridView22";
            this.gridView22.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView22.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView22.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView22.OptionsLayout.StoreAppearance = true;
            this.gridView22.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView22.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView22.OptionsSelection.MultiSelect = true;
            this.gridView22.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView22.OptionsView.ColumnAutoWidth = false;
            this.gridView22.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView22.OptionsView.ShowGroupPanel = false;
            this.gridView22.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTimeRecorded, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView22.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView22.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView22_SelectionChanged);
            this.gridView22.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView22.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView22.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView22_MouseUp);
            this.gridView22.DoubleClick += new System.EventHandler(this.gridView22_DoubleClick);
            this.gridView22.GotFocus += new System.EventHandler(this.gridView22_GotFocus);
            // 
            // colRiskAssessmentID
            // 
            this.colRiskAssessmentID.Caption = "Risk Assessment ID";
            this.colRiskAssessmentID.FieldName = "RiskAssessmentID";
            this.colRiskAssessmentID.Name = "colRiskAssessmentID";
            this.colRiskAssessmentID.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentID.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentID.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentID.Width = 114;
            // 
            // colSurveyedPoleID1
            // 
            this.colSurveyedPoleID1.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID1.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID1.Name = "colSurveyedPoleID1";
            this.colSurveyedPoleID1.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID1.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID1.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID1.Width = 104;
            // 
            // colDateTimeRecorded
            // 
            this.colDateTimeRecorded.Caption = "Created On";
            this.colDateTimeRecorded.ColumnEdit = this.repositoryItemDateEdit2;
            this.colDateTimeRecorded.FieldName = "DateTimeRecorded";
            this.colDateTimeRecorded.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateTimeRecorded.Name = "colDateTimeRecorded";
            this.colDateTimeRecorded.OptionsColumn.AllowEdit = false;
            this.colDateTimeRecorded.OptionsColumn.AllowFocus = false;
            this.colDateTimeRecorded.OptionsColumn.ReadOnly = true;
            this.colDateTimeRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateTimeRecorded.Visible = true;
            this.colDateTimeRecorded.VisibleIndex = 0;
            this.colDateTimeRecorded.Width = 102;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.Mask.EditMask = "g";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // colPersonCompletingTypeID
            // 
            this.colPersonCompletingTypeID.Caption = "Person Completing Type ID";
            this.colPersonCompletingTypeID.FieldName = "PersonCompletingTypeID";
            this.colPersonCompletingTypeID.Name = "colPersonCompletingTypeID";
            this.colPersonCompletingTypeID.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingTypeID.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingTypeID.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingTypeID.Width = 151;
            // 
            // colPersonCompletingID
            // 
            this.colPersonCompletingID.Caption = "Person Completing ID";
            this.colPersonCompletingID.FieldName = "PersonCompletingID";
            this.colPersonCompletingID.Name = "colPersonCompletingID";
            this.colPersonCompletingID.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingID.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingID.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingID.Width = 124;
            // 
            // colPermissionChecked
            // 
            this.colPermissionChecked.Caption = "Permission Checked";
            this.colPermissionChecked.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colPermissionChecked.FieldName = "PermissionChecked";
            this.colPermissionChecked.Name = "colPermissionChecked";
            this.colPermissionChecked.OptionsColumn.AllowEdit = false;
            this.colPermissionChecked.OptionsColumn.AllowFocus = false;
            this.colPermissionChecked.OptionsColumn.ReadOnly = true;
            this.colPermissionChecked.Visible = true;
            this.colPermissionChecked.VisibleIndex = 4;
            this.colPermissionChecked.Width = 115;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colElectricalAssessmentVerified
            // 
            this.colElectricalAssessmentVerified.Caption = "Electrical Assessment Verified";
            this.colElectricalAssessmentVerified.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colElectricalAssessmentVerified.FieldName = "ElectricalAssessmentVerified";
            this.colElectricalAssessmentVerified.Name = "colElectricalAssessmentVerified";
            this.colElectricalAssessmentVerified.OptionsColumn.AllowEdit = false;
            this.colElectricalAssessmentVerified.OptionsColumn.AllowFocus = false;
            this.colElectricalAssessmentVerified.OptionsColumn.ReadOnly = true;
            this.colElectricalAssessmentVerified.Visible = true;
            this.colElectricalAssessmentVerified.VisibleIndex = 5;
            this.colElectricalAssessmentVerified.Width = 162;
            // 
            // colElectricalRiskCategoryID
            // 
            this.colElectricalRiskCategoryID.Caption = "Electrical Risk Category ID";
            this.colElectricalRiskCategoryID.FieldName = "ElectricalRiskCategoryID";
            this.colElectricalRiskCategoryID.Name = "colElectricalRiskCategoryID";
            this.colElectricalRiskCategoryID.OptionsColumn.AllowEdit = false;
            this.colElectricalRiskCategoryID.OptionsColumn.AllowFocus = false;
            this.colElectricalRiskCategoryID.OptionsColumn.ReadOnly = true;
            this.colElectricalRiskCategoryID.Width = 147;
            // 
            // colTypeOfWorkingID
            // 
            this.colTypeOfWorkingID.Caption = "Type Of Working ID";
            this.colTypeOfWorkingID.FieldName = "TypeOfWorkingID";
            this.colTypeOfWorkingID.Name = "colTypeOfWorkingID";
            this.colTypeOfWorkingID.OptionsColumn.AllowEdit = false;
            this.colTypeOfWorkingID.OptionsColumn.AllowFocus = false;
            this.colTypeOfWorkingID.OptionsColumn.ReadOnly = true;
            this.colTypeOfWorkingID.Width = 116;
            // 
            // colPermitHolderName
            // 
            this.colPermitHolderName.Caption = "Permit Holder Name";
            this.colPermitHolderName.FieldName = "PermitHolderName";
            this.colPermitHolderName.Name = "colPermitHolderName";
            this.colPermitHolderName.OptionsColumn.AllowEdit = false;
            this.colPermitHolderName.OptionsColumn.AllowFocus = false;
            this.colPermitHolderName.OptionsColumn.ReadOnly = true;
            this.colPermitHolderName.Visible = true;
            this.colPermitHolderName.VisibleIndex = 8;
            this.colPermitHolderName.Width = 115;
            // 
            // colNRSWAOpeningNoticeNo
            // 
            this.colNRSWAOpeningNoticeNo.Caption = "NRSWA Opening Notice";
            this.colNRSWAOpeningNoticeNo.FieldName = "NRSWAOpeningNoticeNo";
            this.colNRSWAOpeningNoticeNo.Name = "colNRSWAOpeningNoticeNo";
            this.colNRSWAOpeningNoticeNo.OptionsColumn.AllowEdit = false;
            this.colNRSWAOpeningNoticeNo.OptionsColumn.AllowFocus = false;
            this.colNRSWAOpeningNoticeNo.OptionsColumn.ReadOnly = true;
            this.colNRSWAOpeningNoticeNo.Visible = true;
            this.colNRSWAOpeningNoticeNo.VisibleIndex = 9;
            this.colNRSWAOpeningNoticeNo.Width = 134;
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.OptionsColumn.ReadOnly = true;
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit21
            // 
            this.repositoryItemMemoExEdit21.AutoHeight = false;
            this.repositoryItemMemoExEdit21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit21.Name = "repositoryItemMemoExEdit21";
            this.repositoryItemMemoExEdit21.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit21.ShowIcon = false;
            // 
            // colGUID6
            // 
            this.colGUID6.Caption = "GUID";
            this.colGUID6.FieldName = "GUID";
            this.colGUID6.Name = "colGUID6";
            this.colGUID6.OptionsColumn.AllowEdit = false;
            this.colGUID6.OptionsColumn.AllowFocus = false;
            this.colGUID6.OptionsColumn.ReadOnly = true;
            // 
            // colRiskAssessmentTypeID
            // 
            this.colRiskAssessmentTypeID.Caption = "Risk Assessment Type ID";
            this.colRiskAssessmentTypeID.FieldName = "RiskAssessmentTypeID";
            this.colRiskAssessmentTypeID.Name = "colRiskAssessmentTypeID";
            this.colRiskAssessmentTypeID.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentTypeID.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentTypeID.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentTypeID.Width = 141;
            // 
            // colLastUpdated
            // 
            this.colLastUpdated.Caption = "Last Updated";
            this.colLastUpdated.ColumnEdit = this.repositoryItemDateEdit2;
            this.colLastUpdated.FieldName = "LastUpdated";
            this.colLastUpdated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastUpdated.Name = "colLastUpdated";
            this.colLastUpdated.OptionsColumn.AllowEdit = false;
            this.colLastUpdated.OptionsColumn.AllowFocus = false;
            this.colLastUpdated.OptionsColumn.ReadOnly = true;
            this.colLastUpdated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastUpdated.Visible = true;
            this.colLastUpdated.VisibleIndex = 10;
            this.colLastUpdated.Width = 85;
            // 
            // colElectricalRiskCategory
            // 
            this.colElectricalRiskCategory.Caption = "Electrical Risk Category";
            this.colElectricalRiskCategory.FieldName = "ElectricalRiskCategory";
            this.colElectricalRiskCategory.Name = "colElectricalRiskCategory";
            this.colElectricalRiskCategory.OptionsColumn.AllowEdit = false;
            this.colElectricalRiskCategory.OptionsColumn.AllowFocus = false;
            this.colElectricalRiskCategory.OptionsColumn.ReadOnly = true;
            this.colElectricalRiskCategory.Visible = true;
            this.colElectricalRiskCategory.VisibleIndex = 6;
            this.colElectricalRiskCategory.Width = 133;
            // 
            // colTypeOfWorking
            // 
            this.colTypeOfWorking.Caption = "Type of Working";
            this.colTypeOfWorking.FieldName = "TypeOfWorking";
            this.colTypeOfWorking.Name = "colTypeOfWorking";
            this.colTypeOfWorking.OptionsColumn.AllowEdit = false;
            this.colTypeOfWorking.OptionsColumn.AllowFocus = false;
            this.colTypeOfWorking.OptionsColumn.ReadOnly = true;
            this.colTypeOfWorking.Visible = true;
            this.colTypeOfWorking.VisibleIndex = 7;
            this.colTypeOfWorking.Width = 100;
            // 
            // colPersonCompletingType
            // 
            this.colPersonCompletingType.Caption = "Person Type";
            this.colPersonCompletingType.FieldName = "PersonCompletingType";
            this.colPersonCompletingType.Name = "colPersonCompletingType";
            this.colPersonCompletingType.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingType.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingType.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingType.Visible = true;
            this.colPersonCompletingType.VisibleIndex = 3;
            this.colPersonCompletingType.Width = 81;
            // 
            // colPersonCompletingName
            // 
            this.colPersonCompletingName.Caption = "Person Completing Name";
            this.colPersonCompletingName.FieldName = "PersonCompletingName";
            this.colPersonCompletingName.Name = "colPersonCompletingName";
            this.colPersonCompletingName.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingName.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingName.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingName.Visible = true;
            this.colPersonCompletingName.VisibleIndex = 2;
            this.colPersonCompletingName.Width = 140;
            // 
            // colRiskAssessmentType
            // 
            this.colRiskAssessmentType.Caption = "Risk Assessment Type";
            this.colRiskAssessmentType.FieldName = "RiskAssessmentType";
            this.colRiskAssessmentType.Name = "colRiskAssessmentType";
            this.colRiskAssessmentType.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentType.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentType.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentType.Visible = true;
            this.colRiskAssessmentType.VisibleIndex = 1;
            this.colRiskAssessmentType.Width = 127;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Site Address and Access";
            this.colSiteAddress.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Visible = true;
            this.colSiteAddress.VisibleIndex = 12;
            this.colSiteAddress.Width = 138;
            // 
            // colMEWPArialRescuers
            // 
            this.colMEWPArialRescuers.Caption = "MEWP \\ Aerial Rescuers";
            this.colMEWPArialRescuers.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colMEWPArialRescuers.FieldName = "MEWPArialRescuers";
            this.colMEWPArialRescuers.Name = "colMEWPArialRescuers";
            this.colMEWPArialRescuers.OptionsColumn.ReadOnly = true;
            this.colMEWPArialRescuers.Visible = true;
            this.colMEWPArialRescuers.VisibleIndex = 13;
            this.colMEWPArialRescuers.Width = 135;
            // 
            // colEmergencyKitLocation
            // 
            this.colEmergencyKitLocation.Caption = "Emergency Kit Location";
            this.colEmergencyKitLocation.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colEmergencyKitLocation.FieldName = "EmergencyKitLocation";
            this.colEmergencyKitLocation.Name = "colEmergencyKitLocation";
            this.colEmergencyKitLocation.OptionsColumn.ReadOnly = true;
            this.colEmergencyKitLocation.Visible = true;
            this.colEmergencyKitLocation.VisibleIndex = 14;
            this.colEmergencyKitLocation.Width = 132;
            // 
            // colAirAmbulanceLat
            // 
            this.colAirAmbulanceLat.Caption = "Air Ambulance Lat";
            this.colAirAmbulanceLat.FieldName = "AirAmbulanceLat";
            this.colAirAmbulanceLat.Name = "colAirAmbulanceLat";
            this.colAirAmbulanceLat.OptionsColumn.AllowEdit = false;
            this.colAirAmbulanceLat.OptionsColumn.AllowFocus = false;
            this.colAirAmbulanceLat.OptionsColumn.ReadOnly = true;
            this.colAirAmbulanceLat.Visible = true;
            this.colAirAmbulanceLat.VisibleIndex = 15;
            this.colAirAmbulanceLat.Width = 107;
            // 
            // colAirAmbulanceLong
            // 
            this.colAirAmbulanceLong.Caption = "Air Ambulance Long";
            this.colAirAmbulanceLong.FieldName = "AirAmbulanceLong";
            this.colAirAmbulanceLong.Name = "colAirAmbulanceLong";
            this.colAirAmbulanceLong.OptionsColumn.AllowEdit = false;
            this.colAirAmbulanceLong.OptionsColumn.AllowFocus = false;
            this.colAirAmbulanceLong.OptionsColumn.ReadOnly = true;
            this.colAirAmbulanceLong.Visible = true;
            this.colAirAmbulanceLong.VisibleIndex = 16;
            this.colAirAmbulanceLong.Width = 115;
            // 
            // colBanksMan
            // 
            this.colBanksMan.Caption = "Banksman";
            this.colBanksMan.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colBanksMan.FieldName = "BanksMan";
            this.colBanksMan.Name = "colBanksMan";
            this.colBanksMan.OptionsColumn.ReadOnly = true;
            this.colBanksMan.Visible = true;
            this.colBanksMan.VisibleIndex = 17;
            // 
            // colEmergencyServicesMeetPoint
            // 
            this.colEmergencyServicesMeetPoint.Caption = "Emergency Service Meet Point";
            this.colEmergencyServicesMeetPoint.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colEmergencyServicesMeetPoint.FieldName = "EmergencyServicesMeetPoint";
            this.colEmergencyServicesMeetPoint.Name = "colEmergencyServicesMeetPoint";
            this.colEmergencyServicesMeetPoint.OptionsColumn.ReadOnly = true;
            this.colEmergencyServicesMeetPoint.Visible = true;
            this.colEmergencyServicesMeetPoint.VisibleIndex = 18;
            this.colEmergencyServicesMeetPoint.Width = 166;
            // 
            // colMobilePhoneSignalBars
            // 
            this.colMobilePhoneSignalBars.Caption = "Phone Signal Bars";
            this.colMobilePhoneSignalBars.FieldName = "MobilePhoneSignalBars";
            this.colMobilePhoneSignalBars.Name = "colMobilePhoneSignalBars";
            this.colMobilePhoneSignalBars.OptionsColumn.AllowEdit = false;
            this.colMobilePhoneSignalBars.OptionsColumn.AllowFocus = false;
            this.colMobilePhoneSignalBars.OptionsColumn.ReadOnly = true;
            this.colMobilePhoneSignalBars.Visible = true;
            this.colMobilePhoneSignalBars.VisibleIndex = 19;
            this.colMobilePhoneSignalBars.Width = 106;
            // 
            // colNearestAandE
            // 
            this.colNearestAandE.Caption = "Nearest A & E";
            this.colNearestAandE.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colNearestAandE.FieldName = "NearestAandE";
            this.colNearestAandE.Name = "colNearestAandE";
            this.colNearestAandE.OptionsColumn.ReadOnly = true;
            this.colNearestAandE.Visible = true;
            this.colNearestAandE.VisibleIndex = 20;
            this.colNearestAandE.Width = 88;
            // 
            // colNearestLandLine
            // 
            this.colNearestLandLine.Caption = "Nearest Landline";
            this.colNearestLandLine.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colNearestLandLine.FieldName = "NearestLandLine";
            this.colNearestLandLine.Name = "colNearestLandLine";
            this.colNearestLandLine.OptionsColumn.ReadOnly = true;
            this.colNearestLandLine.Visible = true;
            this.colNearestLandLine.VisibleIndex = 21;
            this.colNearestLandLine.Width = 101;
            // 
            // gridControl23
            // 
            this.gridControl23.DataSource = this.sp07255UTSurveyedPoleRiskAssessmentQuestionsListBindingSource;
            this.gridControl23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl23.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl23.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl23.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl23.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl23.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl23.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl23.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl23.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl23_EmbeddedNavigator_ButtonClick);
            this.gridControl23.Location = new System.Drawing.Point(0, 0);
            this.gridControl23.MainView = this.gridView23;
            this.gridControl23.MenuManager = this.barManager1;
            this.gridControl23.Name = "gridControl23";
            this.gridControl23.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit3,
            this.repositoryItemMemoExEdit22});
            this.gridControl23.Size = new System.Drawing.Size(608, 422);
            this.gridControl23.TabIndex = 25;
            this.gridControl23.UseEmbeddedNavigator = true;
            this.gridControl23.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView23});
            // 
            // sp07255UTSurveyedPoleRiskAssessmentQuestionsListBindingSource
            // 
            this.sp07255UTSurveyedPoleRiskAssessmentQuestionsListBindingSource.DataMember = "sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_List";
            this.sp07255UTSurveyedPoleRiskAssessmentQuestionsListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView23
            // 
            this.gridView23.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRiskQuestionID,
            this.colRiskAssessmentID1,
            this.colQuestionDescription,
            this.colQuestionNotes,
            this.colQuestionOrder,
            this.colAnswer,
            this.colRemarks7,
            this.colMasterQuestionID,
            this.colGUID7,
            this.colRiskAssessmentDateTimeRecorded,
            this.colRiskAssessmentType1,
            this.colAnswerDescription,
            this.colAnswerText1,
            this.colAnswerText2,
            this.colAnswerText3,
            this.colRiskAssessmentTypeID1});
            this.gridView23.GridControl = this.gridControl23;
            this.gridView23.GroupCount = 1;
            this.gridView23.Name = "gridView23";
            this.gridView23.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView23.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView23.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView23.OptionsLayout.StoreAppearance = true;
            this.gridView23.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView23.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView23.OptionsSelection.MultiSelect = true;
            this.gridView23.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView23.OptionsView.ColumnAutoWidth = false;
            this.gridView23.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView23.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRiskAssessmentDateTimeRecorded, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQuestionOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView23.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView23.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView23_SelectionChanged);
            this.gridView23.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView23.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView23.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView23.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView23_MouseUp);
            this.gridView23.GotFocus += new System.EventHandler(this.gridView23_GotFocus);
            // 
            // colRiskQuestionID
            // 
            this.colRiskQuestionID.Caption = "Risk Question ID";
            this.colRiskQuestionID.FieldName = "RiskQuestionID";
            this.colRiskQuestionID.Name = "colRiskQuestionID";
            this.colRiskQuestionID.OptionsColumn.AllowEdit = false;
            this.colRiskQuestionID.OptionsColumn.AllowFocus = false;
            this.colRiskQuestionID.OptionsColumn.ReadOnly = true;
            this.colRiskQuestionID.Width = 100;
            // 
            // colRiskAssessmentID1
            // 
            this.colRiskAssessmentID1.Caption = "Risk Assessment ID";
            this.colRiskAssessmentID1.FieldName = "RiskAssessmentID";
            this.colRiskAssessmentID1.Name = "colRiskAssessmentID1";
            this.colRiskAssessmentID1.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentID1.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentID1.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentID1.Width = 114;
            // 
            // colQuestionDescription
            // 
            this.colQuestionDescription.Caption = "Question";
            this.colQuestionDescription.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colQuestionDescription.FieldName = "QuestionDescription";
            this.colQuestionDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuestionDescription.Name = "colQuestionDescription";
            this.colQuestionDescription.OptionsColumn.ReadOnly = true;
            this.colQuestionDescription.Visible = true;
            this.colQuestionDescription.VisibleIndex = 1;
            this.colQuestionDescription.Width = 328;
            // 
            // repositoryItemMemoExEdit22
            // 
            this.repositoryItemMemoExEdit22.AutoHeight = false;
            this.repositoryItemMemoExEdit22.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit22.Name = "repositoryItemMemoExEdit22";
            this.repositoryItemMemoExEdit22.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit22.ShowIcon = false;
            // 
            // colQuestionNotes
            // 
            this.colQuestionNotes.Caption = "Question Notes";
            this.colQuestionNotes.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colQuestionNotes.FieldName = "QuestionNotes";
            this.colQuestionNotes.Name = "colQuestionNotes";
            this.colQuestionNotes.OptionsColumn.ReadOnly = true;
            this.colQuestionNotes.Visible = true;
            this.colQuestionNotes.VisibleIndex = 3;
            this.colQuestionNotes.Width = 166;
            // 
            // colQuestionOrder
            // 
            this.colQuestionOrder.Caption = "Order";
            this.colQuestionOrder.FieldName = "QuestionOrder";
            this.colQuestionOrder.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuestionOrder.Name = "colQuestionOrder";
            this.colQuestionOrder.OptionsColumn.AllowEdit = false;
            this.colQuestionOrder.OptionsColumn.AllowFocus = false;
            this.colQuestionOrder.OptionsColumn.ReadOnly = true;
            this.colQuestionOrder.Visible = true;
            this.colQuestionOrder.VisibleIndex = 0;
            this.colQuestionOrder.Width = 62;
            // 
            // colAnswer
            // 
            this.colAnswer.Caption = "Answer ID";
            this.colAnswer.FieldName = "Answer";
            this.colAnswer.Name = "colAnswer";
            this.colAnswer.OptionsColumn.AllowEdit = false;
            this.colAnswer.OptionsColumn.AllowFocus = false;
            this.colAnswer.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks7
            // 
            this.colRemarks7.Caption = "Remarks";
            this.colRemarks7.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colRemarks7.FieldName = "Remarks";
            this.colRemarks7.Name = "colRemarks7";
            this.colRemarks7.OptionsColumn.ReadOnly = true;
            this.colRemarks7.Visible = true;
            this.colRemarks7.VisibleIndex = 8;
            this.colRemarks7.Width = 175;
            // 
            // colMasterQuestionID
            // 
            this.colMasterQuestionID.Caption = "Master Question ID";
            this.colMasterQuestionID.FieldName = "MasterQuestionID";
            this.colMasterQuestionID.Name = "colMasterQuestionID";
            this.colMasterQuestionID.OptionsColumn.AllowEdit = false;
            this.colMasterQuestionID.OptionsColumn.AllowFocus = false;
            this.colMasterQuestionID.OptionsColumn.ReadOnly = true;
            this.colMasterQuestionID.Width = 114;
            // 
            // colGUID7
            // 
            this.colGUID7.Caption = "GUID";
            this.colGUID7.FieldName = "GUID";
            this.colGUID7.Name = "colGUID7";
            this.colGUID7.OptionsColumn.AllowEdit = false;
            this.colGUID7.OptionsColumn.AllowFocus = false;
            this.colGUID7.OptionsColumn.ReadOnly = true;
            // 
            // colRiskAssessmentDateTimeRecorded
            // 
            this.colRiskAssessmentDateTimeRecorded.Caption = "Risk Assessment Date";
            this.colRiskAssessmentDateTimeRecorded.ColumnEdit = this.repositoryItemDateEdit3;
            this.colRiskAssessmentDateTimeRecorded.FieldName = "RiskAssessmentDateTimeRecorded";
            this.colRiskAssessmentDateTimeRecorded.Name = "colRiskAssessmentDateTimeRecorded";
            this.colRiskAssessmentDateTimeRecorded.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentDateTimeRecorded.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentDateTimeRecorded.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentDateTimeRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRiskAssessmentDateTimeRecorded.Visible = true;
            this.colRiskAssessmentDateTimeRecorded.VisibleIndex = 7;
            this.colRiskAssessmentDateTimeRecorded.Width = 126;
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit3.Mask.EditMask = "g";
            this.repositoryItemDateEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // colRiskAssessmentType1
            // 
            this.colRiskAssessmentType1.Caption = "Assessment Type";
            this.colRiskAssessmentType1.FieldName = "RiskAssessmentType";
            this.colRiskAssessmentType1.Name = "colRiskAssessmentType1";
            this.colRiskAssessmentType1.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentType1.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentType1.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentType1.Visible = true;
            this.colRiskAssessmentType1.VisibleIndex = 2;
            this.colRiskAssessmentType1.Width = 105;
            // 
            // colAnswerDescription
            // 
            this.colAnswerDescription.Caption = "Answer";
            this.colAnswerDescription.FieldName = "AnswerDescription";
            this.colAnswerDescription.Name = "colAnswerDescription";
            this.colAnswerDescription.OptionsColumn.AllowEdit = false;
            this.colAnswerDescription.OptionsColumn.AllowFocus = false;
            this.colAnswerDescription.OptionsColumn.ReadOnly = true;
            this.colAnswerDescription.Visible = true;
            this.colAnswerDescription.VisibleIndex = 4;
            this.colAnswerDescription.Width = 57;
            // 
            // colAnswerText1
            // 
            this.colAnswerText1.Caption = "Significant Hazards";
            this.colAnswerText1.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colAnswerText1.FieldName = "AnswerText1";
            this.colAnswerText1.Name = "colAnswerText1";
            this.colAnswerText1.OptionsColumn.ReadOnly = true;
            this.colAnswerText1.Visible = true;
            this.colAnswerText1.VisibleIndex = 5;
            this.colAnswerText1.Width = 175;
            // 
            // colAnswerText2
            // 
            this.colAnswerText2.Caption = "People Affected";
            this.colAnswerText2.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colAnswerText2.FieldName = "AnswerText2";
            this.colAnswerText2.Name = "colAnswerText2";
            this.colAnswerText2.OptionsColumn.ReadOnly = true;
            this.colAnswerText2.Visible = true;
            this.colAnswerText2.VisibleIndex = 6;
            this.colAnswerText2.Width = 175;
            // 
            // colAnswerText3
            // 
            this.colAnswerText3.Caption = "Controls";
            this.colAnswerText3.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colAnswerText3.FieldName = "AnswerText3";
            this.colAnswerText3.Name = "colAnswerText3";
            this.colAnswerText3.OptionsColumn.ReadOnly = true;
            this.colAnswerText3.Visible = true;
            this.colAnswerText3.VisibleIndex = 7;
            this.colAnswerText3.Width = 175;
            // 
            // colRiskAssessmentTypeID1
            // 
            this.colRiskAssessmentTypeID1.Caption = "Risk Assessment Type ID";
            this.colRiskAssessmentTypeID1.FieldName = "RiskAssessmentTypeID";
            this.colRiskAssessmentTypeID1.Name = "colRiskAssessmentTypeID1";
            this.colRiskAssessmentTypeID1.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentTypeID1.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentTypeID1.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentTypeID1.Width = 141;
            // 
            // panelControl7
            // 
            this.panelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl7.Controls.Add(this.pictureEdit1);
            this.panelControl7.Controls.Add(this.labelControl7);
            this.panelControl7.Location = new System.Drawing.Point(4, 4);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(1079, 32);
            this.panelControl7.TabIndex = 22;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(5, 5);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(20, 20);
            this.pictureEdit1.TabIndex = 19;
            // 
            // labelControl7
            // 
            this.labelControl7.AllowHtmlString = true;
            this.labelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.labelControl7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl7.AutoEllipsis = true;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(29, 2);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(1044, 27);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Create any required <b>Site Specific</b> or <b>G55/2</b> Risk Assessments here.\r\n" +
    "Once done, click <b>Next</b> button to proceed to final step.";
            // 
            // xtraTabPageFinish
            // 
            this.xtraTabPageFinish.Controls.Add(this.groupControl1);
            this.xtraTabPageFinish.Controls.Add(this.groupControlMissingInfo);
            this.xtraTabPageFinish.Controls.Add(this.btnOnHold);
            this.xtraTabPageFinish.Controls.Add(this.btnClose);
            this.xtraTabPageFinish.Controls.Add(this.panelControl4);
            this.xtraTabPageFinish.Controls.Add(this.btnFinish);
            this.xtraTabPageFinish.Controls.Add(this.pictureEdit6);
            this.xtraTabPageFinish.Name = "xtraTabPageFinish";
            this.xtraTabPageFinish.Size = new System.Drawing.Size(1087, 464);
            this.xtraTabPageFinish.Text = "Finish Survey";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.ItemForFiveYearClearanceAchieved);
            this.groupControl1.Controls.Add(this.FiveYearClearanceAchievedCheckEdit);
            this.groupControl1.Location = new System.Drawing.Point(210, 49);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(251, 44);
            this.groupControl1.TabIndex = 28;
            this.groupControl1.Text = "Important";
            // 
            // ItemForFiveYearClearanceAchieved
            // 
            this.ItemForFiveYearClearanceAchieved.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForFiveYearClearanceAchieved.Appearance.Options.UseFont = true;
            this.ItemForFiveYearClearanceAchieved.Location = new System.Drawing.Point(6, 25);
            this.ItemForFiveYearClearanceAchieved.Name = "ItemForFiveYearClearanceAchieved";
            this.ItemForFiveYearClearanceAchieved.Size = new System.Drawing.Size(153, 13);
            this.ItemForFiveYearClearanceAchieved.TabIndex = 27;
            this.ItemForFiveYearClearanceAchieved.Text = "5 Year Clearance Achieved:";
            // 
            // FiveYearClearanceAchievedCheckEdit
            // 
            this.FiveYearClearanceAchievedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07114UTSurveyedPoleItemBindingSource, "FiveYearClearanceAchieved", true));
            this.FiveYearClearanceAchievedCheckEdit.Location = new System.Drawing.Point(164, 21);
            this.FiveYearClearanceAchievedCheckEdit.MenuManager = this.barManager1;
            this.FiveYearClearanceAchievedCheckEdit.Name = "FiveYearClearanceAchievedCheckEdit";
            this.FiveYearClearanceAchievedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.FiveYearClearanceAchievedCheckEdit.Properties.ValueChecked = 1;
            this.FiveYearClearanceAchievedCheckEdit.Properties.ValueUnchecked = 0;
            this.FiveYearClearanceAchievedCheckEdit.Size = new System.Drawing.Size(82, 19);
            this.FiveYearClearanceAchievedCheckEdit.StyleController = this.dataLayoutControl1;
            this.FiveYearClearanceAchievedCheckEdit.TabIndex = 26;
            // 
            // groupControlMissingInfo
            // 
            this.groupControlMissingInfo.AppearanceCaption.ForeColor = System.Drawing.Color.Red;
            this.groupControlMissingInfo.AppearanceCaption.Options.UseForeColor = true;
            this.groupControlMissingInfo.Controls.Add(this.btnFixFinishSurveyIssue);
            this.groupControlMissingInfo.Controls.Add(this.labelFinishSurveyWarningInfo);
            this.groupControlMissingInfo.Controls.Add(this.labelFinishSurveyWarning);
            this.groupControlMissingInfo.Location = new System.Drawing.Point(480, 49);
            this.groupControlMissingInfo.Name = "groupControlMissingInfo";
            this.groupControlMissingInfo.Size = new System.Drawing.Size(428, 184);
            this.groupControlMissingInfo.TabIndex = 25;
            this.groupControlMissingInfo.Text = "Finish Survey - Missing Info";
            this.groupControlMissingInfo.Visible = false;
            // 
            // btnFixFinishSurveyIssue
            // 
            this.btnFixFinishSurveyIssue.Enabled = false;
            this.btnFixFinishSurveyIssue.Location = new System.Drawing.Point(343, 158);
            this.btnFixFinishSurveyIssue.Name = "btnFixFinishSurveyIssue";
            this.btnFixFinishSurveyIssue.Size = new System.Drawing.Size(77, 19);
            this.btnFixFinishSurveyIssue.TabIndex = 24;
            this.btnFixFinishSurveyIssue.Text = "Go To Details";
            this.btnFixFinishSurveyIssue.Visible = false;
            this.btnFixFinishSurveyIssue.Click += new System.EventHandler(this.btnFixFinishSurveyIssue_Click);
            // 
            // labelFinishSurveyWarningInfo
            // 
            this.labelFinishSurveyWarningInfo.AllowHtmlString = true;
            this.labelFinishSurveyWarningInfo.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelFinishSurveyWarningInfo.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelFinishSurveyWarningInfo.Appearance.ImageIndex = 3;
            this.labelFinishSurveyWarningInfo.Appearance.ImageList = this.imageCollection1;
            this.labelFinishSurveyWarningInfo.Appearance.Options.UseForeColor = true;
            this.labelFinishSurveyWarningInfo.Appearance.Options.UseImageAlign = true;
            this.labelFinishSurveyWarningInfo.Appearance.Options.UseImageIndex = true;
            this.labelFinishSurveyWarningInfo.Appearance.Options.UseImageList = true;
            this.labelFinishSurveyWarningInfo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelFinishSurveyWarningInfo.Location = new System.Drawing.Point(5, 24);
            this.labelFinishSurveyWarningInfo.Name = "labelFinishSurveyWarningInfo";
            this.labelFinishSurveyWarningInfo.Size = new System.Drawing.Size(320, 16);
            this.labelFinishSurveyWarningInfo.TabIndex = 24;
            this.labelFinishSurveyWarningInfo.Text = "       Before Finishing the survey the following must be corrected...";
            this.labelFinishSurveyWarningInfo.Visible = false;
            // 
            // labelFinishSurveyWarning
            // 
            this.labelFinishSurveyWarning.AllowHtmlString = true;
            this.labelFinishSurveyWarning.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelFinishSurveyWarning.Appearance.Options.UseForeColor = true;
            this.labelFinishSurveyWarning.Appearance.Options.UseTextOptions = true;
            this.labelFinishSurveyWarning.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelFinishSurveyWarning.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelFinishSurveyWarning.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelFinishSurveyWarning.Location = new System.Drawing.Point(7, 46);
            this.labelFinishSurveyWarning.Name = "labelFinishSurveyWarning";
            this.labelFinishSurveyWarning.Size = new System.Drawing.Size(416, 131);
            this.labelFinishSurveyWarning.TabIndex = 23;
            this.labelFinishSurveyWarning.Text = "The <b>Is Span Clear</b> or <b>Infestation Rate</b> must have a value set.";
            this.labelFinishSurveyWarning.Visible = false;
            // 
            // btnOnHold
            // 
            this.btnOnHold.Location = new System.Drawing.Point(210, 255);
            this.btnOnHold.Name = "btnOnHold";
            this.btnOnHold.Size = new System.Drawing.Size(251, 44);
            this.btnOnHold.TabIndex = 22;
            this.btnOnHold.Text = "Set Survey On-Hold";
            this.btnOnHold.Click += new System.EventHandler(this.btnOnHold_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(210, 189);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(251, 44);
            this.btnClose.TabIndex = 21;
            this.btnClose.Text = "Close Survey Without Completing";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.pictureEdit7);
            this.panelControl4.Controls.Add(this.labelControl4);
            this.panelControl4.Location = new System.Drawing.Point(208, 4);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(875, 32);
            this.panelControl4.TabIndex = 20;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.EditValue = ((object)(resources.GetObject("pictureEdit7.EditValue")));
            this.pictureEdit7.Location = new System.Drawing.Point(5, 5);
            this.pictureEdit7.MenuManager = this.barManager1;
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Size = new System.Drawing.Size(20, 20);
            this.pictureEdit7.TabIndex = 19;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.labelControl4.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl4.AutoEllipsis = true;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(29, 1);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(841, 26);
            this.labelControl4.TabIndex = 14;
            this.labelControl4.Text = resources.GetString("labelControl4.Text");
            // 
            // btnFinish
            // 
            this.btnFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnFinish.Appearance.Options.UseFont = true;
            this.btnFinish.Location = new System.Drawing.Point(210, 123);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(251, 44);
            this.btnFinish.TabIndex = 19;
            this.btnFinish.Text = "Finish Survey";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit6.EditValue = ((object)(resources.GetObject("pictureEdit6.EditValue")));
            this.pictureEdit6.Location = new System.Drawing.Point(2, 3);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ShowMenu = false;
            this.pictureEdit6.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit6.Size = new System.Drawing.Size(200, 459);
            this.pictureEdit6.TabIndex = 18;
            // 
            // dataSet_UT_Edit1
            // 
            this.dataSet_UT_Edit1.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.ImageOptions.ImageIndex = 7;
            this.btnNext.ImageOptions.ImageList = this.imageCollection1;
            this.btnNext.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnNext.Location = new System.Drawing.Point(1013, 521);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 16;
            this.btnNext.Text = "Next";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrior
            // 
            this.btnPrior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrior.ImageOptions.ImageIndex = 6;
            this.btnPrior.ImageOptions.ImageList = this.imageCollection1;
            this.btnPrior.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnPrior.Location = new System.Drawing.Point(932, 521);
            this.btnPrior.Name = "btnPrior";
            this.btnPrior.Size = new System.Drawing.Size(75, 23);
            this.btnPrior.TabIndex = 19;
            this.btnPrior.Text = "Previous";
            this.btnPrior.Click += new System.EventHandler(this.btnPrior_Click);
            // 
            // sp07114_UT_Surveyed_Pole_ItemTableAdapter
            // 
            this.sp07114_UT_Surveyed_Pole_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07117_UT_Pole_Survey_Pictures_ListTableAdapter
            // 
            this.sp07117_UT_Pole_Survey_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07119_UT_Pole_Survey_Historical_Pictures_ListTableAdapter
            // 
            this.sp07119_UT_Pole_Survey_Historical_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07120_UT_Survey_Infestation_Rate_ListTableAdapter
            // 
            this.sp07120_UT_Survey_Infestation_Rate_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07121_UT_Surveyed_Pole_Linked_TreesTableAdapter
            // 
            this.sp07121_UT_Surveyed_Pole_Linked_TreesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07122_UT_Surveyed_Pole_Species_Linked_To_TreeTableAdapter
            // 
            this.sp07122_UT_Surveyed_Pole_Species_Linked_To_TreeTableAdapter.ClearBeforeFill = true;
            // 
            // sp07123_UT_Surveyed_Pole_Tree_Pictures_ListTableAdapter
            // 
            this.sp07123_UT_Surveyed_Pole_Tree_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07127_UT_Surveyed_Pole_Work_ListTableAdapter
            // 
            this.sp07127_UT_Surveyed_Pole_Work_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter
            // 
            this.sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_OnlyTableAdapter
            // 
            this.sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_OnlyTableAdapter.ClearBeforeFill = true;
            // 
            // sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_OnlyTableAdapter
            // 
            this.sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_OnlyTableAdapter.ClearBeforeFill = true;
            // 
            // sp07149_UT_Surveyed_Pole_Work_Historical_ListTableAdapter
            // 
            this.sp07149_UT_Surveyed_Pole_Work_Historical_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07150_UT_Surveyed_Tree_Defect_ListTableAdapter
            // 
            this.sp07150_UT_Surveyed_Tree_Defect_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07154_UT_Surveyed_Tree_Defect_Pictures_ListTableAdapter
            // 
            this.sp07154_UT_Surveyed_Tree_Defect_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLinkedRecords});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelLinkedRecords
            // 
            this.dockPanelLinkedRecords.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLinkedRecords.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanelLinkedRecords.ID = new System.Guid("6b570636-1b5e-488d-87fb-fcdf03e79a90");
            this.dockPanelLinkedRecords.Location = new System.Drawing.Point(0, 340);
            this.dockPanelLinkedRecords.Name = "dockPanelLinkedRecords";
            this.dockPanelLinkedRecords.Options.ShowCloseButton = false;
            this.dockPanelLinkedRecords.OriginalSize = new System.Drawing.Size(200, 176);
            this.dockPanelLinkedRecords.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanelLinkedRecords.SavedIndex = 0;
            this.dockPanelLinkedRecords.Size = new System.Drawing.Size(1092, 176);
            this.dockPanelLinkedRecords.Text = "Records Linked to Pole";
            this.dockPanelLinkedRecords.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.xtraTabControl8);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 30);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(1086, 143);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // xtraTabControl8
            // 
            this.xtraTabControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl8.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl8.Name = "xtraTabControl8";
            this.xtraTabControl8.SelectedTabPage = this.xtraTabPage6;
            this.xtraTabControl8.Size = new System.Drawing.Size(1086, 143);
            this.xtraTabControl8.TabIndex = 25;
            this.xtraTabControl8.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage6,
            this.xtraTabPage5,
            this.xtraTabPage7,
            this.xtraTabPage8,
            this.xtraTabPage16});
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.gridSplitContainer8);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(1081, 114);
            this.xtraTabPage6.Text = "Linked Access Issues";
            // 
            // gridSplitContainer8
            // 
            this.gridSplitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer8.Grid = this.gridControl16;
            this.gridSplitContainer8.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer8.Name = "gridSplitContainer8";
            this.gridSplitContainer8.Panel1.Controls.Add(this.gridControl16);
            this.gridSplitContainer8.Size = new System.Drawing.Size(1081, 114);
            this.gridSplitContainer8.TabIndex = 0;
            // 
            // gridControl16
            // 
            this.gridControl16.DataSource = this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource;
            this.gridControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl16.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl16.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl16.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl16.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl16.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl16.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl16.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl16.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl16.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl16.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl16.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl16.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl16.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl16_EmbeddedNavigator_ButtonClick);
            this.gridControl16.Location = new System.Drawing.Point(0, 0);
            this.gridControl16.MainView = this.gridView16;
            this.gridControl16.MenuManager = this.barManager1;
            this.gridControl16.Name = "gridControl16";
            this.gridControl16.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit15});
            this.gridControl16.Size = new System.Drawing.Size(1081, 114);
            this.gridControl16.TabIndex = 4;
            this.gridControl16.UseEmbeddedNavigator = true;
            this.gridControl16.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView16});
            // 
            // sp07070UTPoleManagerLinkedAccessIssuesBindingSource
            // 
            this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource.DataMember = "sp07070_UT_Pole_Manager_Linked_Access_Issues";
            this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView16
            // 
            this.gridView16.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleAccessIssueID,
            this.colPoleID2,
            this.colAccessIssueID,
            this.gridColumn122,
            this.colCircuitID2,
            this.colPoleNumber3,
            this.colClientID3,
            this.colCircuitNumber2,
            this.colPoleName1,
            this.colClientName3,
            this.colAccessIssue,
            this.gridColumn123});
            this.gridView16.GridControl = this.gridControl16;
            this.gridView16.Name = "gridView16";
            this.gridView16.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView16.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView16.OptionsLayout.StoreAppearance = true;
            this.gridView16.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView16.OptionsSelection.MultiSelect = true;
            this.gridView16.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView16.OptionsView.ColumnAutoWidth = false;
            this.gridView16.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView16.OptionsView.ShowGroupPanel = false;
            this.gridView16.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAccessIssue, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView16.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView16.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView16_SelectionChanged);
            this.gridView16.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView16.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView16.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView16.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView16_MouseUp);
            this.gridView16.DoubleClick += new System.EventHandler(this.gridView16_DoubleClick);
            this.gridView16.GotFocus += new System.EventHandler(this.gridView16_GotFocus);
            // 
            // colPoleAccessIssueID
            // 
            this.colPoleAccessIssueID.Caption = "Pole Access Issue ID";
            this.colPoleAccessIssueID.FieldName = "PoleAccessIssueID";
            this.colPoleAccessIssueID.Name = "colPoleAccessIssueID";
            this.colPoleAccessIssueID.OptionsColumn.AllowEdit = false;
            this.colPoleAccessIssueID.OptionsColumn.AllowFocus = false;
            this.colPoleAccessIssueID.OptionsColumn.ReadOnly = true;
            this.colPoleAccessIssueID.Width = 120;
            // 
            // colPoleID2
            // 
            this.colPoleID2.Caption = "Pole ID";
            this.colPoleID2.FieldName = "PoleID";
            this.colPoleID2.Name = "colPoleID2";
            this.colPoleID2.OptionsColumn.AllowEdit = false;
            this.colPoleID2.OptionsColumn.AllowFocus = false;
            this.colPoleID2.OptionsColumn.ReadOnly = true;
            // 
            // colAccessIssueID
            // 
            this.colAccessIssueID.Caption = "Access Issue ID";
            this.colAccessIssueID.FieldName = "AccessIssueID";
            this.colAccessIssueID.Name = "colAccessIssueID";
            this.colAccessIssueID.OptionsColumn.AllowEdit = false;
            this.colAccessIssueID.OptionsColumn.AllowFocus = false;
            this.colAccessIssueID.OptionsColumn.ReadOnly = true;
            this.colAccessIssueID.Width = 97;
            // 
            // gridColumn122
            // 
            this.gridColumn122.Caption = "GUID";
            this.gridColumn122.FieldName = "GUID";
            this.gridColumn122.Name = "gridColumn122";
            this.gridColumn122.OptionsColumn.AllowEdit = false;
            this.gridColumn122.OptionsColumn.AllowFocus = false;
            this.gridColumn122.OptionsColumn.ReadOnly = true;
            // 
            // colCircuitID2
            // 
            this.colCircuitID2.Caption = "Circuit ID";
            this.colCircuitID2.FieldName = "CircuitID";
            this.colCircuitID2.Name = "colCircuitID2";
            this.colCircuitID2.OptionsColumn.AllowEdit = false;
            this.colCircuitID2.OptionsColumn.AllowFocus = false;
            this.colCircuitID2.OptionsColumn.ReadOnly = true;
            // 
            // colPoleNumber3
            // 
            this.colPoleNumber3.Caption = "Pole Number";
            this.colPoleNumber3.FieldName = "PoleNumber";
            this.colPoleNumber3.Name = "colPoleNumber3";
            this.colPoleNumber3.OptionsColumn.AllowEdit = false;
            this.colPoleNumber3.OptionsColumn.AllowFocus = false;
            this.colPoleNumber3.OptionsColumn.ReadOnly = true;
            this.colPoleNumber3.Width = 81;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            // 
            // colCircuitNumber2
            // 
            this.colCircuitNumber2.Caption = "Circuit Number";
            this.colCircuitNumber2.FieldName = "CircuitNumber";
            this.colCircuitNumber2.Name = "colCircuitNumber2";
            this.colCircuitNumber2.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber2.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber2.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber2.Width = 91;
            // 
            // colPoleName1
            // 
            this.colPoleName1.Caption = "Pole Name";
            this.colPoleName1.FieldName = "PoleName";
            this.colPoleName1.Name = "colPoleName1";
            this.colPoleName1.OptionsColumn.AllowEdit = false;
            this.colPoleName1.OptionsColumn.AllowFocus = false;
            this.colPoleName1.OptionsColumn.ReadOnly = true;
            this.colPoleName1.Width = 350;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 78;
            // 
            // colAccessIssue
            // 
            this.colAccessIssue.Caption = "Access Issue";
            this.colAccessIssue.FieldName = "AccessIssue";
            this.colAccessIssue.Name = "colAccessIssue";
            this.colAccessIssue.OptionsColumn.AllowEdit = false;
            this.colAccessIssue.OptionsColumn.AllowFocus = false;
            this.colAccessIssue.OptionsColumn.ReadOnly = true;
            this.colAccessIssue.Visible = true;
            this.colAccessIssue.VisibleIndex = 0;
            this.colAccessIssue.Width = 210;
            // 
            // gridColumn123
            // 
            this.gridColumn123.Caption = "Remarks";
            this.gridColumn123.ColumnEdit = this.repositoryItemMemoExEdit15;
            this.gridColumn123.FieldName = "Remarks";
            this.gridColumn123.Name = "gridColumn123";
            this.gridColumn123.OptionsColumn.ReadOnly = true;
            this.gridColumn123.Visible = true;
            this.gridColumn123.VisibleIndex = 1;
            this.gridColumn123.Width = 250;
            // 
            // repositoryItemMemoExEdit15
            // 
            this.repositoryItemMemoExEdit15.AutoHeight = false;
            this.repositoryItemMemoExEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit15.Name = "repositoryItemMemoExEdit15";
            this.repositoryItemMemoExEdit15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit15.ShowIcon = false;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.gridSplitContainer5);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1081, 114);
            this.xtraTabPage5.Text = "Linked Environmental Issues";
            // 
            // gridSplitContainer5
            // 
            this.gridSplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer5.Grid = this.gridControl17;
            this.gridSplitContainer5.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer5.Name = "gridSplitContainer5";
            this.gridSplitContainer5.Panel1.Controls.Add(this.gridControl17);
            this.gridSplitContainer5.Size = new System.Drawing.Size(1081, 114);
            this.gridSplitContainer5.TabIndex = 0;
            // 
            // gridControl17
            // 
            this.gridControl17.DataSource = this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource;
            this.gridControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl17.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl17.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl17.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl17.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl17_EmbeddedNavigator_ButtonClick);
            this.gridControl17.Location = new System.Drawing.Point(0, 0);
            this.gridControl17.MainView = this.gridView17;
            this.gridControl17.MenuManager = this.barManager1;
            this.gridControl17.Name = "gridControl17";
            this.gridControl17.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit16});
            this.gridControl17.Size = new System.Drawing.Size(1081, 114);
            this.gridControl17.TabIndex = 3;
            this.gridControl17.UseEmbeddedNavigator = true;
            this.gridControl17.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView17});
            // 
            // sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource
            // 
            this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource.DataMember = "sp07075_UT_Pole_Manager_Linked_Environmental_Issues";
            this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView17
            // 
            this.gridView17.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn124,
            this.gridColumn125,
            this.gridColumn126,
            this.gridColumn127,
            this.gridColumn128,
            this.gridColumn129,
            this.gridColumn130,
            this.gridColumn131,
            this.gridColumn132,
            this.gridColumn133,
            this.gridColumn134,
            this.gridColumn135});
            this.gridView17.GridControl = this.gridControl17;
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView17.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView17.OptionsLayout.StoreAppearance = true;
            this.gridView17.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView17.OptionsSelection.MultiSelect = true;
            this.gridView17.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView17.OptionsView.ColumnAutoWidth = false;
            this.gridView17.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            this.gridView17.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn134, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView17.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView17.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView17_SelectionChanged);
            this.gridView17.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView17.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView17.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView17.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView17_MouseUp);
            this.gridView17.DoubleClick += new System.EventHandler(this.gridView17_DoubleClick);
            this.gridView17.GotFocus += new System.EventHandler(this.gridView17_GotFocus);
            // 
            // gridColumn124
            // 
            this.gridColumn124.Caption = "Pole Environmental Issue ID";
            this.gridColumn124.FieldName = "PoleEnvironmentalIssueID";
            this.gridColumn124.Name = "gridColumn124";
            this.gridColumn124.OptionsColumn.AllowEdit = false;
            this.gridColumn124.OptionsColumn.AllowFocus = false;
            this.gridColumn124.OptionsColumn.ReadOnly = true;
            this.gridColumn124.Width = 120;
            // 
            // gridColumn125
            // 
            this.gridColumn125.Caption = "Pole ID";
            this.gridColumn125.FieldName = "PoleID";
            this.gridColumn125.Name = "gridColumn125";
            this.gridColumn125.OptionsColumn.AllowEdit = false;
            this.gridColumn125.OptionsColumn.AllowFocus = false;
            this.gridColumn125.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn126
            // 
            this.gridColumn126.Caption = "Environmental Issue ID";
            this.gridColumn126.FieldName = "EnvironmentalIssueID";
            this.gridColumn126.Name = "gridColumn126";
            this.gridColumn126.OptionsColumn.AllowEdit = false;
            this.gridColumn126.OptionsColumn.AllowFocus = false;
            this.gridColumn126.OptionsColumn.ReadOnly = true;
            this.gridColumn126.Width = 97;
            // 
            // gridColumn127
            // 
            this.gridColumn127.Caption = "GUID";
            this.gridColumn127.FieldName = "GUID";
            this.gridColumn127.Name = "gridColumn127";
            this.gridColumn127.OptionsColumn.AllowEdit = false;
            this.gridColumn127.OptionsColumn.AllowFocus = false;
            this.gridColumn127.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn128
            // 
            this.gridColumn128.Caption = "Circuit ID";
            this.gridColumn128.FieldName = "CircuitID";
            this.gridColumn128.Name = "gridColumn128";
            this.gridColumn128.OptionsColumn.AllowEdit = false;
            this.gridColumn128.OptionsColumn.AllowFocus = false;
            this.gridColumn128.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn129
            // 
            this.gridColumn129.Caption = "Pole Number";
            this.gridColumn129.FieldName = "PoleNumber";
            this.gridColumn129.Name = "gridColumn129";
            this.gridColumn129.OptionsColumn.AllowEdit = false;
            this.gridColumn129.OptionsColumn.AllowFocus = false;
            this.gridColumn129.OptionsColumn.ReadOnly = true;
            this.gridColumn129.Width = 81;
            // 
            // gridColumn130
            // 
            this.gridColumn130.Caption = "Client ID";
            this.gridColumn130.FieldName = "ClientID";
            this.gridColumn130.Name = "gridColumn130";
            this.gridColumn130.OptionsColumn.AllowEdit = false;
            this.gridColumn130.OptionsColumn.AllowFocus = false;
            this.gridColumn130.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn131
            // 
            this.gridColumn131.Caption = "Circuit Number";
            this.gridColumn131.FieldName = "CircuitNumber";
            this.gridColumn131.Name = "gridColumn131";
            this.gridColumn131.OptionsColumn.AllowEdit = false;
            this.gridColumn131.OptionsColumn.AllowFocus = false;
            this.gridColumn131.OptionsColumn.ReadOnly = true;
            this.gridColumn131.Width = 91;
            // 
            // gridColumn132
            // 
            this.gridColumn132.Caption = "Pole Name";
            this.gridColumn132.FieldName = "PoleName";
            this.gridColumn132.Name = "gridColumn132";
            this.gridColumn132.OptionsColumn.AllowEdit = false;
            this.gridColumn132.OptionsColumn.AllowFocus = false;
            this.gridColumn132.OptionsColumn.ReadOnly = true;
            this.gridColumn132.Width = 350;
            // 
            // gridColumn133
            // 
            this.gridColumn133.Caption = "Client Name";
            this.gridColumn133.FieldName = "ClientName";
            this.gridColumn133.Name = "gridColumn133";
            this.gridColumn133.OptionsColumn.AllowEdit = false;
            this.gridColumn133.OptionsColumn.AllowFocus = false;
            this.gridColumn133.OptionsColumn.ReadOnly = true;
            this.gridColumn133.Width = 78;
            // 
            // gridColumn134
            // 
            this.gridColumn134.Caption = "Environmental Issue";
            this.gridColumn134.FieldName = "EnvironmentalIssue";
            this.gridColumn134.Name = "gridColumn134";
            this.gridColumn134.OptionsColumn.AllowEdit = false;
            this.gridColumn134.OptionsColumn.AllowFocus = false;
            this.gridColumn134.OptionsColumn.ReadOnly = true;
            this.gridColumn134.Visible = true;
            this.gridColumn134.VisibleIndex = 0;
            this.gridColumn134.Width = 210;
            // 
            // gridColumn135
            // 
            this.gridColumn135.Caption = "Remarks";
            this.gridColumn135.ColumnEdit = this.repositoryItemMemoExEdit16;
            this.gridColumn135.FieldName = "Remarks";
            this.gridColumn135.Name = "gridColumn135";
            this.gridColumn135.OptionsColumn.ReadOnly = true;
            this.gridColumn135.Visible = true;
            this.gridColumn135.VisibleIndex = 1;
            this.gridColumn135.Width = 250;
            // 
            // repositoryItemMemoExEdit16
            // 
            this.repositoryItemMemoExEdit16.AutoHeight = false;
            this.repositoryItemMemoExEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit16.Name = "repositoryItemMemoExEdit16";
            this.repositoryItemMemoExEdit16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit16.ShowIcon = false;
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.gridSplitContainer6);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(1081, 114);
            this.xtraTabPage7.Text = "Linked Site Hazards";
            // 
            // gridSplitContainer6
            // 
            this.gridSplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer6.Grid = this.gridControl18;
            this.gridSplitContainer6.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer6.Name = "gridSplitContainer6";
            this.gridSplitContainer6.Panel1.Controls.Add(this.gridControl18);
            this.gridSplitContainer6.Size = new System.Drawing.Size(1081, 114);
            this.gridSplitContainer6.TabIndex = 0;
            // 
            // gridControl18
            // 
            this.gridControl18.DataSource = this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource;
            this.gridControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl18.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl18.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl18.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl18.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl18_EmbeddedNavigator_ButtonClick);
            this.gridControl18.Location = new System.Drawing.Point(0, 0);
            this.gridControl18.MainView = this.gridView18;
            this.gridControl18.MenuManager = this.barManager1;
            this.gridControl18.Name = "gridControl18";
            this.gridControl18.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit17});
            this.gridControl18.Size = new System.Drawing.Size(1081, 114);
            this.gridControl18.TabIndex = 4;
            this.gridControl18.UseEmbeddedNavigator = true;
            this.gridControl18.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView18});
            // 
            // sp07080UTPoleManagerLinkedSiteHazardsBindingSource
            // 
            this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource.DataMember = "sp07080_UT_Pole_Manager_Linked_Site_Hazards";
            this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView18
            // 
            this.gridView18.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn136,
            this.gridColumn137,
            this.gridColumn138,
            this.gridColumn139,
            this.gridColumn140,
            this.gridColumn141,
            this.gridColumn142,
            this.gridColumn143,
            this.gridColumn144,
            this.gridColumn145,
            this.gridColumn146,
            this.gridColumn147});
            this.gridView18.GridControl = this.gridControl18;
            this.gridView18.Name = "gridView18";
            this.gridView18.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView18.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView18.OptionsLayout.StoreAppearance = true;
            this.gridView18.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView18.OptionsSelection.MultiSelect = true;
            this.gridView18.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView18.OptionsView.ColumnAutoWidth = false;
            this.gridView18.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView18.OptionsView.ShowGroupPanel = false;
            this.gridView18.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn146, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView18.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView18.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView18_SelectionChanged);
            this.gridView18.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView18.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView18.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView18.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView18_MouseUp);
            this.gridView18.DoubleClick += new System.EventHandler(this.gridView18_DoubleClick);
            this.gridView18.GotFocus += new System.EventHandler(this.gridView18_GotFocus);
            // 
            // gridColumn136
            // 
            this.gridColumn136.Caption = "Pole Site Hazard ID";
            this.gridColumn136.FieldName = "PoleSiteHazardID";
            this.gridColumn136.Name = "gridColumn136";
            this.gridColumn136.OptionsColumn.AllowEdit = false;
            this.gridColumn136.OptionsColumn.AllowFocus = false;
            this.gridColumn136.OptionsColumn.ReadOnly = true;
            this.gridColumn136.Width = 120;
            // 
            // gridColumn137
            // 
            this.gridColumn137.Caption = "Pole ID";
            this.gridColumn137.FieldName = "PoleID";
            this.gridColumn137.Name = "gridColumn137";
            this.gridColumn137.OptionsColumn.AllowEdit = false;
            this.gridColumn137.OptionsColumn.AllowFocus = false;
            this.gridColumn137.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn138
            // 
            this.gridColumn138.Caption = "Site Hazard ID";
            this.gridColumn138.FieldName = "SiteHazardID";
            this.gridColumn138.Name = "gridColumn138";
            this.gridColumn138.OptionsColumn.AllowEdit = false;
            this.gridColumn138.OptionsColumn.AllowFocus = false;
            this.gridColumn138.OptionsColumn.ReadOnly = true;
            this.gridColumn138.Width = 97;
            // 
            // gridColumn139
            // 
            this.gridColumn139.Caption = "GUID";
            this.gridColumn139.FieldName = "GUID";
            this.gridColumn139.Name = "gridColumn139";
            this.gridColumn139.OptionsColumn.AllowEdit = false;
            this.gridColumn139.OptionsColumn.AllowFocus = false;
            this.gridColumn139.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn140
            // 
            this.gridColumn140.Caption = "Circuit ID";
            this.gridColumn140.FieldName = "CircuitID";
            this.gridColumn140.Name = "gridColumn140";
            this.gridColumn140.OptionsColumn.AllowEdit = false;
            this.gridColumn140.OptionsColumn.AllowFocus = false;
            this.gridColumn140.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn141
            // 
            this.gridColumn141.Caption = "Pole Number";
            this.gridColumn141.FieldName = "PoleNumber";
            this.gridColumn141.Name = "gridColumn141";
            this.gridColumn141.OptionsColumn.AllowEdit = false;
            this.gridColumn141.OptionsColumn.AllowFocus = false;
            this.gridColumn141.OptionsColumn.ReadOnly = true;
            this.gridColumn141.Width = 81;
            // 
            // gridColumn142
            // 
            this.gridColumn142.Caption = "Client ID";
            this.gridColumn142.FieldName = "ClientID";
            this.gridColumn142.Name = "gridColumn142";
            this.gridColumn142.OptionsColumn.AllowEdit = false;
            this.gridColumn142.OptionsColumn.AllowFocus = false;
            this.gridColumn142.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn143
            // 
            this.gridColumn143.Caption = "Circuit Number";
            this.gridColumn143.FieldName = "CircuitNumber";
            this.gridColumn143.Name = "gridColumn143";
            this.gridColumn143.OptionsColumn.AllowEdit = false;
            this.gridColumn143.OptionsColumn.AllowFocus = false;
            this.gridColumn143.OptionsColumn.ReadOnly = true;
            this.gridColumn143.Width = 91;
            // 
            // gridColumn144
            // 
            this.gridColumn144.Caption = "Pole Name";
            this.gridColumn144.FieldName = "PoleName";
            this.gridColumn144.Name = "gridColumn144";
            this.gridColumn144.OptionsColumn.AllowEdit = false;
            this.gridColumn144.OptionsColumn.AllowFocus = false;
            this.gridColumn144.OptionsColumn.ReadOnly = true;
            this.gridColumn144.Width = 350;
            // 
            // gridColumn145
            // 
            this.gridColumn145.Caption = "Client Name";
            this.gridColumn145.FieldName = "ClientName";
            this.gridColumn145.Name = "gridColumn145";
            this.gridColumn145.OptionsColumn.AllowEdit = false;
            this.gridColumn145.OptionsColumn.AllowFocus = false;
            this.gridColumn145.OptionsColumn.ReadOnly = true;
            this.gridColumn145.Width = 78;
            // 
            // gridColumn146
            // 
            this.gridColumn146.Caption = "Site Hazard";
            this.gridColumn146.FieldName = "SiteHazard";
            this.gridColumn146.Name = "gridColumn146";
            this.gridColumn146.OptionsColumn.AllowEdit = false;
            this.gridColumn146.OptionsColumn.AllowFocus = false;
            this.gridColumn146.OptionsColumn.ReadOnly = true;
            this.gridColumn146.Visible = true;
            this.gridColumn146.VisibleIndex = 0;
            this.gridColumn146.Width = 210;
            // 
            // gridColumn147
            // 
            this.gridColumn147.Caption = "Remarks";
            this.gridColumn147.ColumnEdit = this.repositoryItemMemoExEdit17;
            this.gridColumn147.FieldName = "Remarks";
            this.gridColumn147.Name = "gridColumn147";
            this.gridColumn147.OptionsColumn.ReadOnly = true;
            this.gridColumn147.Visible = true;
            this.gridColumn147.VisibleIndex = 1;
            this.gridColumn147.Width = 250;
            // 
            // repositoryItemMemoExEdit17
            // 
            this.repositoryItemMemoExEdit17.AutoHeight = false;
            this.repositoryItemMemoExEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit17.Name = "repositoryItemMemoExEdit17";
            this.repositoryItemMemoExEdit17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit17.ShowIcon = false;
            // 
            // xtraTabPage8
            // 
            this.xtraTabPage8.Controls.Add(this.gridSplitContainer7);
            this.xtraTabPage8.Name = "xtraTabPage8";
            this.xtraTabPage8.Size = new System.Drawing.Size(1081, 114);
            this.xtraTabPage8.Text = "Linked Electrical Hazards";
            // 
            // gridSplitContainer7
            // 
            this.gridSplitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer7.Grid = this.gridControl19;
            this.gridSplitContainer7.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer7.Name = "gridSplitContainer7";
            this.gridSplitContainer7.Panel1.Controls.Add(this.gridControl19);
            this.gridSplitContainer7.Size = new System.Drawing.Size(1081, 114);
            this.gridSplitContainer7.TabIndex = 0;
            // 
            // gridControl19
            // 
            this.gridControl19.DataSource = this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource;
            this.gridControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl19.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl19.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl19.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl19.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl19.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl19.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl19.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl19.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl19.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl19.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl19.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl19.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl19.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl19_EmbeddedNavigator_ButtonClick);
            this.gridControl19.Location = new System.Drawing.Point(0, 0);
            this.gridControl19.MainView = this.gridView19;
            this.gridControl19.MenuManager = this.barManager1;
            this.gridControl19.Name = "gridControl19";
            this.gridControl19.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit18});
            this.gridControl19.Size = new System.Drawing.Size(1081, 114);
            this.gridControl19.TabIndex = 4;
            this.gridControl19.UseEmbeddedNavigator = true;
            this.gridControl19.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView19});
            // 
            // sp07085UTPoleManagerLinkedElectricalHazardsBindingSource
            // 
            this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource.DataMember = "sp07085_UT_Pole_Manager_Linked_Electrical_Hazards";
            this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView19
            // 
            this.gridView19.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn148,
            this.gridColumn149,
            this.gridColumn150,
            this.gridColumn151,
            this.gridColumn152,
            this.gridColumn153,
            this.gridColumn154,
            this.gridColumn155,
            this.gridColumn156,
            this.gridColumn157,
            this.gridColumn158,
            this.gridColumn159});
            this.gridView19.GridControl = this.gridControl19;
            this.gridView19.Name = "gridView19";
            this.gridView19.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView19.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView19.OptionsLayout.StoreAppearance = true;
            this.gridView19.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView19.OptionsSelection.MultiSelect = true;
            this.gridView19.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView19.OptionsView.ColumnAutoWidth = false;
            this.gridView19.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView19.OptionsView.ShowGroupPanel = false;
            this.gridView19.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn158, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView19.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView19.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView19_SelectionChanged);
            this.gridView19.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView19.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView19.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView19.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView19_MouseUp);
            this.gridView19.DoubleClick += new System.EventHandler(this.gridView19_DoubleClick);
            this.gridView19.GotFocus += new System.EventHandler(this.gridView19_GotFocus);
            // 
            // gridColumn148
            // 
            this.gridColumn148.Caption = "Pole Electrical Hazard ID";
            this.gridColumn148.FieldName = "PoleElectricalHazardID";
            this.gridColumn148.Name = "gridColumn148";
            this.gridColumn148.OptionsColumn.AllowEdit = false;
            this.gridColumn148.OptionsColumn.AllowFocus = false;
            this.gridColumn148.OptionsColumn.ReadOnly = true;
            this.gridColumn148.Width = 120;
            // 
            // gridColumn149
            // 
            this.gridColumn149.Caption = "Pole ID";
            this.gridColumn149.FieldName = "PoleID";
            this.gridColumn149.Name = "gridColumn149";
            this.gridColumn149.OptionsColumn.AllowEdit = false;
            this.gridColumn149.OptionsColumn.AllowFocus = false;
            this.gridColumn149.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn150
            // 
            this.gridColumn150.Caption = "Electrical Hazard ID";
            this.gridColumn150.FieldName = "ElectricalHazardID";
            this.gridColumn150.Name = "gridColumn150";
            this.gridColumn150.OptionsColumn.AllowEdit = false;
            this.gridColumn150.OptionsColumn.AllowFocus = false;
            this.gridColumn150.OptionsColumn.ReadOnly = true;
            this.gridColumn150.Width = 97;
            // 
            // gridColumn151
            // 
            this.gridColumn151.Caption = "GUID";
            this.gridColumn151.FieldName = "GUID";
            this.gridColumn151.Name = "gridColumn151";
            this.gridColumn151.OptionsColumn.AllowEdit = false;
            this.gridColumn151.OptionsColumn.AllowFocus = false;
            this.gridColumn151.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn152
            // 
            this.gridColumn152.Caption = "Circuit ID";
            this.gridColumn152.FieldName = "CircuitID";
            this.gridColumn152.Name = "gridColumn152";
            this.gridColumn152.OptionsColumn.AllowEdit = false;
            this.gridColumn152.OptionsColumn.AllowFocus = false;
            this.gridColumn152.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn153
            // 
            this.gridColumn153.Caption = "Pole Number";
            this.gridColumn153.FieldName = "PoleNumber";
            this.gridColumn153.Name = "gridColumn153";
            this.gridColumn153.OptionsColumn.AllowEdit = false;
            this.gridColumn153.OptionsColumn.AllowFocus = false;
            this.gridColumn153.OptionsColumn.ReadOnly = true;
            this.gridColumn153.Width = 81;
            // 
            // gridColumn154
            // 
            this.gridColumn154.Caption = "Client ID";
            this.gridColumn154.FieldName = "ClientID";
            this.gridColumn154.Name = "gridColumn154";
            this.gridColumn154.OptionsColumn.AllowEdit = false;
            this.gridColumn154.OptionsColumn.AllowFocus = false;
            this.gridColumn154.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn155
            // 
            this.gridColumn155.Caption = "Circuit Number";
            this.gridColumn155.FieldName = "CircuitNumber";
            this.gridColumn155.Name = "gridColumn155";
            this.gridColumn155.OptionsColumn.AllowEdit = false;
            this.gridColumn155.OptionsColumn.AllowFocus = false;
            this.gridColumn155.OptionsColumn.ReadOnly = true;
            this.gridColumn155.Width = 91;
            // 
            // gridColumn156
            // 
            this.gridColumn156.Caption = "Pole Name";
            this.gridColumn156.FieldName = "PoleName";
            this.gridColumn156.Name = "gridColumn156";
            this.gridColumn156.OptionsColumn.AllowEdit = false;
            this.gridColumn156.OptionsColumn.AllowFocus = false;
            this.gridColumn156.OptionsColumn.ReadOnly = true;
            this.gridColumn156.Width = 350;
            // 
            // gridColumn157
            // 
            this.gridColumn157.Caption = "Client Name";
            this.gridColumn157.FieldName = "ClientName";
            this.gridColumn157.Name = "gridColumn157";
            this.gridColumn157.OptionsColumn.AllowEdit = false;
            this.gridColumn157.OptionsColumn.AllowFocus = false;
            this.gridColumn157.OptionsColumn.ReadOnly = true;
            this.gridColumn157.Width = 78;
            // 
            // gridColumn158
            // 
            this.gridColumn158.Caption = "Electrical Hazard";
            this.gridColumn158.FieldName = "ElectricalHazard";
            this.gridColumn158.Name = "gridColumn158";
            this.gridColumn158.OptionsColumn.AllowEdit = false;
            this.gridColumn158.OptionsColumn.AllowFocus = false;
            this.gridColumn158.OptionsColumn.ReadOnly = true;
            this.gridColumn158.Visible = true;
            this.gridColumn158.VisibleIndex = 0;
            this.gridColumn158.Width = 210;
            // 
            // gridColumn159
            // 
            this.gridColumn159.Caption = "Remarks";
            this.gridColumn159.ColumnEdit = this.repositoryItemMemoExEdit18;
            this.gridColumn159.FieldName = "Remarks";
            this.gridColumn159.Name = "gridColumn159";
            this.gridColumn159.OptionsColumn.ReadOnly = true;
            this.gridColumn159.Visible = true;
            this.gridColumn159.VisibleIndex = 1;
            this.gridColumn159.Width = 250;
            // 
            // repositoryItemMemoExEdit18
            // 
            this.repositoryItemMemoExEdit18.AutoHeight = false;
            this.repositoryItemMemoExEdit18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit18.Name = "repositoryItemMemoExEdit18";
            this.repositoryItemMemoExEdit18.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit18.ShowIcon = false;
            // 
            // xtraTabPage16
            // 
            this.xtraTabPage16.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage16.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage16.ImageOptions.Image")));
            this.xtraTabPage16.ImageOptions.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.xtraTabPage16.Name = "xtraTabPage16";
            this.xtraTabPage16.Size = new System.Drawing.Size(1081, 114);
            this.xtraTabPage16.Text = "Linked Documents";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl20;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl20);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1081, 114);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl20
            // 
            this.gridControl20.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl20.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl20.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl20.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl20.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl20.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl20.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl20.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl20.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl20.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl20.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl20.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl20.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl20.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl20_EmbeddedNavigator_ButtonClick);
            this.gridControl20.Location = new System.Drawing.Point(0, 0);
            this.gridControl20.MainView = this.gridView20;
            this.gridControl20.MenuManager = this.barManager1;
            this.gridControl20.Name = "gridControl20";
            this.gridControl20.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit19,
            this.repositoryItemHyperLinkEdit6});
            this.gridControl20.Size = new System.Drawing.Size(1081, 114);
            this.gridControl20.TabIndex = 1;
            this.gridControl20.UseEmbeddedNavigator = true;
            this.gridControl20.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView20});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView20
            // 
            this.gridView20.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.gridColumn160,
            this.gridColumn161,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.gridColumn162,
            this.colDateAdded,
            this.gridColumn163,
            this.gridColumn164,
            this.colDocumentRemarks});
            this.gridView20.GridControl = this.gridControl20;
            this.gridView20.Name = "gridView20";
            this.gridView20.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView20.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView20.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView20.OptionsLayout.StoreAppearance = true;
            this.gridView20.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView20.OptionsSelection.MultiSelect = true;
            this.gridView20.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView20.OptionsView.ColumnAutoWidth = false;
            this.gridView20.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView20.OptionsView.ShowGroupPanel = false;
            this.gridView20.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView20.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView20.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView20_SelectionChanged);
            this.gridView20.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView20.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView20.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView20.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView20_MouseUp);
            this.gridView20.DoubleClick += new System.EventHandler(this.gridView20_DoubleClick);
            this.gridView20.GotFocus += new System.EventHandler(this.gridView20_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // gridColumn160
            // 
            this.gridColumn160.Caption = "Linked Record ID";
            this.gridColumn160.FieldName = "LinkedToRecordID";
            this.gridColumn160.Name = "gridColumn160";
            this.gridColumn160.OptionsColumn.AllowEdit = false;
            this.gridColumn160.OptionsColumn.AllowFocus = false;
            this.gridColumn160.OptionsColumn.ReadOnly = true;
            this.gridColumn160.Width = 102;
            // 
            // gridColumn161
            // 
            this.gridColumn161.Caption = "Linked Record Type ID";
            this.gridColumn161.FieldName = "LinkedToRecordTypeID";
            this.gridColumn161.Name = "gridColumn161";
            this.gridColumn161.OptionsColumn.AllowEdit = false;
            this.gridColumn161.OptionsColumn.AllowFocus = false;
            this.gridColumn161.OptionsColumn.ReadOnly = true;
            this.gridColumn161.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit6;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit6
            // 
            this.repositoryItemHyperLinkEdit6.AutoHeight = false;
            this.repositoryItemHyperLinkEdit6.Name = "repositoryItemHyperLinkEdit6";
            this.repositoryItemHyperLinkEdit6.SingleClick = true;
            this.repositoryItemHyperLinkEdit6.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit6_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // gridColumn162
            // 
            this.gridColumn162.Caption = "Added By Staff ID";
            this.gridColumn162.FieldName = "AddedByStaffID";
            this.gridColumn162.Name = "gridColumn162";
            this.gridColumn162.OptionsColumn.AllowEdit = false;
            this.gridColumn162.OptionsColumn.AllowFocus = false;
            this.gridColumn162.OptionsColumn.ReadOnly = true;
            this.gridColumn162.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // gridColumn163
            // 
            this.gridColumn163.Caption = "Linked To";
            this.gridColumn163.FieldName = "LinkedRecordDescription";
            this.gridColumn163.Name = "gridColumn163";
            this.gridColumn163.OptionsColumn.AllowEdit = false;
            this.gridColumn163.OptionsColumn.AllowFocus = false;
            this.gridColumn163.OptionsColumn.ReadOnly = true;
            this.gridColumn163.Width = 350;
            // 
            // gridColumn164
            // 
            this.gridColumn164.Caption = "Added By";
            this.gridColumn164.FieldName = "AddedByStaffName";
            this.gridColumn164.Name = "gridColumn164";
            this.gridColumn164.OptionsColumn.AllowEdit = false;
            this.gridColumn164.OptionsColumn.AllowFocus = false;
            this.gridColumn164.OptionsColumn.ReadOnly = true;
            this.gridColumn164.Visible = true;
            this.gridColumn164.VisibleIndex = 4;
            this.gridColumn164.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit19;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            this.colDocumentRemarks.Width = 134;
            // 
            // repositoryItemMemoExEdit19
            // 
            this.repositoryItemMemoExEdit19.AutoHeight = false;
            this.repositoryItemMemoExEdit19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit19.Name = "repositoryItemMemoExEdit19";
            this.repositoryItemMemoExEdit19.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit19.ShowIcon = false;
            // 
            // sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter
            // 
            this.sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter
            // 
            this.sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter
            // 
            this.sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter
            // 
            this.sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07046_UT_Inspection_Cycles_With_BlankTableAdapter
            // 
            this.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07186_UT_G55_Categories_With_BlankTableAdapter
            // 
            this.sp07186_UT_G55_Categories_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp07254_UT_Surveyed_Pole_Risk_Assessment_ListTableAdapter
            // 
            this.sp07254_UT_Surveyed_Pole_Risk_Assessment_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_ListTableAdapter
            // 
            this.sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_ListTableAdapter.ClearBeforeFill = true;
            // 
            // labelControlStep
            // 
            this.labelControlStep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlStep.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControlStep.Appearance.Options.UseFont = true;
            this.labelControlStep.Appearance.Options.UseTextOptions = true;
            this.labelControlStep.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControlStep.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.labelControlStep.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlStep.Location = new System.Drawing.Point(1000, 28);
            this.labelControlStep.Name = "labelControlStep";
            this.labelControlStep.Size = new System.Drawing.Size(88, 15);
            this.labelControlStep.TabIndex = 24;
            this.labelControlStep.Text = "Step 1 of 6";
            // 
            // sp07356_UT_Clearance_Distances_With_BlankTableAdapter
            // 
            this.sp07356_UT_Clearance_Distances_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07357_UT_Deferral_Reasons_With_BlankTableAdapter
            // 
            this.sp07357_UT_Deferral_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter
            // 
            this.sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter.ClearBeforeFill = true;
            // 
            // sp07366_UT_Traffic_Management_Items_With_BlankTableAdapter
            // 
            this.sp07366_UT_Traffic_Management_Items_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07401_UT_PD_Linked_To_ActionsTableAdapter
            // 
            this.sp07401_UT_PD_Linked_To_ActionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07464_UT_Linear_Cut_Lengths_With_BlankTableAdapter
            // 
            this.sp07464_UT_Linear_Cut_Lengths_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07465_UT_Held_Up_Types_With_BlankTableAdapter
            // 
            this.sp07465_UT_Held_Up_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending26
            // 
            this.xtraGridBlending26.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending26.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending26.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending26.GridControl = this.gridControl26;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // xtraGridBlending12
            // 
            this.xtraGridBlending12.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending12.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending12.GridControl = this.gridControl12;
            // 
            // xtraGridBlending7
            // 
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending7.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending7.GridControl = this.gridControl7;
            // 
            // xtraGridBlending27
            // 
            this.xtraGridBlending27.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending27.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending27.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending27.GridControl = this.gridControl27;
            // 
            // xtraGridBlending9
            // 
            this.xtraGridBlending9.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending9.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending9.GridControl = this.gridControl9;
            // 
            // xtraGridBlending10
            // 
            this.xtraGridBlending10.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending10.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending10.GridControl = this.gridControl10;
            // 
            // xtraGridBlending22
            // 
            this.xtraGridBlending22.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending22.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending22.GridControl = this.gridControl22;
            // 
            // xtraGridBlending23
            // 
            this.xtraGridBlending23.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending23.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending23.GridControl = this.gridControl23;
            // 
            // frm_UT_Survey_Pole2
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1092, 546);
            this.Controls.Add(this.labelControlStep);
            this.Controls.Add(this.btnPrior);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Survey_Pole2";
            this.Text = "Survey Pole";
            this.Activated += new System.EventHandler(this.frm_UT_Survey_Pole2_Activated);
            this.Deactivate += new System.EventHandler(this.frm_UT_Survey_Pole2_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Survey_Pole2_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Survey_Pole2_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnNext, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            this.Controls.SetChildIndex(this.btnPrior, 0);
            this.Controls.SetChildIndex(this.labelControlStep, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSignatureCapture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageWelcome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            this.xtraTabPageTakePicture.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07117UTPoleSurveyPicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07119UTPoleSurveyHistoricalPicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            this.xtraTabPageGeneralInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ClearanceDistanceAboveIDFGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07114UTSurveyedPoleItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07356UTClearanceDistancesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredEstimatedCuttingHoursSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShutdownHoursSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeldUpTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07465UTHeldUpTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinearCutLengthIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07464UTLinearCutLengthsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IvyOnPoleCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpanResilientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClearanceDistanceUnderIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessMapPathButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficMapPathButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07367UTTrafficManagementForSurveyedPoleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07366UTTrafficManagementItemsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferralRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferralReasonIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07357UTDeferralReasonsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClearanceDistanceGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateInvoicedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateInvoicedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpanInvoicedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsBaseClimbableCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredUnitDescriptiorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07046UTInspectionCyclesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeferredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G55CategoryIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07186UTG55CategoriesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyStatusIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyStatusTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyedPoleIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSpanClearCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsShutdownRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotGloveRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinesmanPossibleCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficManagementRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeWithin3MetersCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeClimbableCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GUIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfestationRateGridLookupEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07120UTSurveyInfestationRateListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficManagementResolvedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGUID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyedPoleID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInfestationRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHotGloveRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinesmanPossible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeClimbable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpanInvoiced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateInvoiced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferred)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredUnitDescriptiorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferralReasonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferralRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeferredEstimatedCuttingHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrafficManagementRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrafficManagementResolved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrafficMapPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessMapPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSpanClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearanceDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForG55CategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsBaseClimbable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeWithin3Meters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpanResilient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIvyOnPole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeldUpTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsShutdownRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShutdownHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinearCutLengthID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearanceDistanceUnderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearanceDistanceAboveID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleNumber)).EndInit();
            this.xtraTabPageTrees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07121UTSurveyedPoleLinkedTreesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DPMetres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).EndInit();
            this.xtraTabControl3.ResumeLayout(false);
            this.xtraTabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07122UTSurveyedPoleSpeciesLinkedToTreeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.xtraTabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07123UTSurveyedPoleTreePicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl6)).EndInit();
            this.xtraTabControl6.ResumeLayout(false);
            this.xtraTabPageTreeWork.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl5)).EndInit();
            this.xtraTabControl5.ResumeLayout(false);
            this.xtraTabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07127UTSurveyedPoleWorkListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl4)).EndInit();
            this.xtraTabControl4.ResumeLayout(false);
            this.xtraTabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07130UTSurveyedPoleWorkPicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07401UTPDLinkedToActionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit0DP)).EndInit();
            this.xtraTabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07139UTSurveyedTreeWorkLinkedEquipmentReadOnlyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            this.xtraTabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07140UTSurveyedTreeWorkLinkedMaterialsReadOnlyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            this.xtraTabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07149UTSurveyedPoleWorkHistoricalListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours2)).EndInit();
            this.xtraTabPageTreeDefects.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07150UTSurveyedTreeDefectListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl7)).EndInit();
            this.xtraTabControl7.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07154UTSurveyedTreeDefectPicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNoWorkRequired.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            this.xtraTabPageRiskAssessment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07254UTSurveyedPoleRiskAssessmentListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07255UTSurveyedPoleRiskAssessmentQuestionsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.xtraTabPageFinish.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FiveYearClearanceAchievedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMissingInfo)).EndInit();
            this.groupControlMissingInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelLinkedRecords.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl8)).EndInit();
            this.xtraTabControl8.ResumeLayout(false);
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).EndInit();
            this.gridSplitContainer8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).EndInit();
            this.gridSplitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).EndInit();
            this.gridSplitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).EndInit();
            this.xtraTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).EndInit();
            this.gridSplitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).EndInit();
            this.xtraTabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWelcome;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageTakePicture;
        private DevExpress.XtraBars.BarCheckItem bciViewAdditionalInfo;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnNext;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageGeneralInfo;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageTrees;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnPrior;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit SurveyedPoleIDTextEdit;
        private System.Windows.Forms.BindingSource sp07114UTSurveyedPoleItemBindingSource;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DevExpress.XtraEditors.TextEdit SurveyIDTextEdit;
        private DevExpress.XtraEditors.TextEdit PoleIDTextEdit;
        private DevExpress.XtraEditors.TextEdit FeederContractIDTextEdit;
        private DevExpress.XtraEditors.DateEdit SurveyDateDateEdit;
        private DevExpress.XtraEditors.CheckEdit IsSpanClearCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsShutdownRequiredCheckEdit;
        private DevExpress.XtraEditors.CheckEdit HotGloveRequiredCheckEdit;
        private DevExpress.XtraEditors.CheckEdit LinesmanPossibleCheckEdit;
        private DevExpress.XtraEditors.CheckEdit TrafficManagementRequiredCheckEdit;
        private DevExpress.XtraEditors.CheckEdit TreeWithin3MetersCheckEdit;
        private DevExpress.XtraEditors.CheckEdit TreeClimbableCheckEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit GUIDTextEdit;
        private DevExpress.XtraEditors.TextEdit PoleNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGUID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyedPoleID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoleID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFeederContractID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsSpanClear;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInfestationRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsShutdownRequired;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHotGloveRequired;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinesmanPossible;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTrafficManagementRequired;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTrafficManagementResolved;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeWithin3Meters;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeClimbable;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DataSet_UT_EditTableAdapters.sp07114_UT_Surveyed_Pole_ItemTableAdapter sp07114_UT_Surveyed_Pole_ItemTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit InfestationRateGridLookupEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.CheckEdit TrafficManagementResolvedCheckEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoleNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageFinish;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.CheckEdit checkEditNoWorkRequired;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage9;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage10;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage11;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage12;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage13;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage14;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage15;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.SimpleButton btnOnHold;
        private DevExpress.XtraEditors.TextEdit SurveyStatusTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyStatus;
        private DevExpress.XtraEditors.TextEdit SurveyStatusIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyStatusID;
        private System.Windows.Forms.BindingSource sp07117UTPoleSurveyPicturesListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyPictureID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPicturePath;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DataSet_UT_EditTableAdapters.sp07117_UT_Pole_Survey_Pictures_ListTableAdapter sp07117_UT_Pole_Survey_Pictures_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp07119UTPoleSurveyHistoricalPicturesListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DataSet_UT_EditTableAdapters.sp07119_UT_Pole_Survey_Historical_Pictures_ListTableAdapter sp07119_UT_Pole_Survey_Historical_Pictures_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp07120UTSurveyInfestationRateListBindingSource;
        private DataSet_UT_EditTableAdapters.sp07120_UT_Survey_Infestation_Rate_ListTableAdapter sp07120_UT_Survey_Infestation_Rate_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private System.Windows.Forms.BindingSource sp07121UTSurveyedPoleLinkedTreesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSizeBandID;
        private DevExpress.XtraGrid.Columns.GridColumn colX1;
        private DevExpress.XtraGrid.Columns.GridColumn colY1;
        private DevExpress.XtraGrid.Columns.GridColumn colXYPairs;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colLatLongPairs;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID2;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colSizeBand;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType;
        private DataSet_UT_EditTableAdapters.sp07121_UT_Surveyed_Pole_Linked_TreesTableAdapter sp07121_UT_Surveyed_Pole_Linked_TreesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DPMetres;
        private System.Windows.Forms.BindingSource sp07122UTSurveyedPoleSpeciesLinkedToTreeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOfTrees;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric0DP;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber1;
        private DataSet_UT_EditTableAdapters.sp07122_UT_Surveyed_Pole_Species_Linked_To_TreeTableAdapter sp07122_UT_Surveyed_Pole_Species_Linked_To_TreeTableAdapter;
        private System.Windows.Forms.BindingSource sp07123UTSurveyedPoleTreePicturesListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DataSet_UT_EditTableAdapters.sp07123_UT_Surveyed_Pole_Tree_Pictures_ListTableAdapter sp07123_UT_Surveyed_Pole_Tree_Pictures_ListTableAdapter;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageTreeWork;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageTreeDefects;
        private DevExpress.XtraGrid.GridControl gridControl12;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraEditors.SimpleButton btnTakePicture;
        private System.Windows.Forms.BindingSource sp07127UTSurveyedPoleWorkListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDateScheduled;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colApproved;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByStaffID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemBillingID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DataSet_UT_EditTableAdapters.sp07127_UT_Surveyed_Pole_Work_ListTableAdapter sp07127_UT_Surveyed_Pole_Work_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID1;
        private System.Windows.Forms.BindingSource sp07130UTSurveyedPoleWorkPicturesListBindingSource;
        private DataSet_UT_EditTableAdapters.sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colShortLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private System.Windows.Forms.BindingSource sp07139UTSurveyedTreeWorkLinkedEquipmentReadOnlyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromDefaultRequiredEquipmentID;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney;
        private DevExpress.XtraGrid.Columns.GridColumn colActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToWork;
        private DataSet_UT_EditTableAdapters.sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_OnlyTableAdapter sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_OnlyTableAdapter;
        private System.Windows.Forms.BindingSource sp07140UTSurveyedTreeWorkLinkedMaterialsReadOnlyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionMaterialID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActualUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedCost1;
        private DevExpress.XtraGrid.Columns.GridColumn colActualCost1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToWork1;
        private DataSet_UT_EditTableAdapters.sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_OnlyTableAdapter sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_OnlyTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnit1;
        private System.Windows.Forms.BindingSource sp07149UTSurveyedPoleWorkHistoricalListBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DataSet_UT_EditTableAdapters.sp07149_UT_Surveyed_Pole_Work_Historical_ListTableAdapter sp07149_UT_Surveyed_Pole_Work_Historical_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp07150UTSurveyedTreeDefectListBindingSource;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeDefectID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDefectID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colDefectDescription;
        private DataSet_UTTableAdapters.sp07150_UT_Surveyed_Tree_Defect_ListTableAdapter sp07150_UT_Surveyed_Tree_Defect_ListTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl7;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.GridControl gridControl13;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private System.Windows.Forms.BindingSource sp07154UTSurveyedTreeDefectPicturesListBindingSource;
        private DataSet_UT_EditTableAdapters.sp07154_UT_Surveyed_Tree_Defect_Pictures_ListTableAdapter sp07154_UT_Surveyed_Tree_Defect_Pictures_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedSell1;
        private DevExpress.XtraGrid.Columns.GridColumn colActualSell1;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DataSet_UT_Edit dataSet_UT_Edit1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLinkedRecords;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl8;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer8;
        private DevExpress.XtraGrid.GridControl gridControl16;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView16;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleAccessIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID2;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn122;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID2;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessIssue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn123;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit15;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer5;
        private DevExpress.XtraGrid.GridControl gridControl17;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn124;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn125;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn126;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn127;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn128;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn129;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn130;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn131;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn132;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn133;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn134;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn135;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit16;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer6;
        private DevExpress.XtraGrid.GridControl gridControl18;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn136;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn137;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn138;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn139;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn140;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn141;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn142;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn143;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn144;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn145;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn146;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn147;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit17;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage8;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer7;
        private DevExpress.XtraGrid.GridControl gridControl19;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn148;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn149;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn150;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn151;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn152;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn153;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn154;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn155;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn156;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn157;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn158;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn159;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit18;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage16;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridControl gridControl20;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView20;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn160;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn161;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn162;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn163;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn164;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit19;
        private System.Windows.Forms.BindingSource sp07070UTPoleManagerLinkedAccessIssuesBindingSource;
        private System.Windows.Forms.BindingSource sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource;
        private System.Windows.Forms.BindingSource sp07080UTPoleManagerLinkedSiteHazardsBindingSource;
        private System.Windows.Forms.BindingSource sp07085UTPoleManagerLinkedElectricalHazardsBindingSource;
        private DataSet_UTTableAdapters.sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter;
        private DataSet_UTTableAdapters.sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter;
        private DataSet_UTTableAdapters.sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter;
        private DataSet_UTTableAdapters.sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DataSet_AT dataSet_AT;
        private DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiShow;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditSignatureCapture;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DevExpress.XtraBars.BarCheckItem bciShowLinkedRecords;
        private DevExpress.XtraEditors.LabelControl labelFinishSurveyWarning;
        private DevExpress.XtraEditors.SimpleButton btnFixFinishSurveyIssue;
        private DevExpress.XtraEditors.GridLookUpEdit G55CategoryIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraLayout.LayoutControlItem ItemForG55CategoryID;
        private DevExpress.XtraEditors.CheckEdit DeferredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferred;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.GridLookUpEdit DeferredUnitDescriptiorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraEditors.SpinEdit DeferredUnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferredUnits;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferredUnitDescriptiorID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private System.Windows.Forms.BindingSource sp07046UTInspectionCyclesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter sp07046_UT_Inspection_Cycles_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder1;
        private System.Windows.Forms.BindingSource sp07186UTG55CategoriesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07186_UT_G55_Categories_With_BlankTableAdapter sp07186_UT_G55_Categories_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder2;
        private DevExpress.XtraGrid.Columns.GridColumn colTPONumber;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.CheckEdit IsBaseClimbableCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsBaseClimbable;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageRiskAssessment;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private DevExpress.XtraGrid.GridControl gridControl22;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView22;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit21;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraGrid.GridControl gridControl23;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView23;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit22;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private System.Windows.Forms.BindingSource sp07254UTSurveyedPoleRiskAssessmentListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionChecked;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalAssessmentVerified;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalRiskCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeOfWorkingID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermitHolderName;
        private DevExpress.XtraGrid.Columns.GridColumn colNRSWAOpeningNoticeNo;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID6;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdated;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeOfWorking;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingType;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingName;
        private DataSet_UT_EditTableAdapters.sp07254_UT_Surveyed_Pole_Risk_Assessment_ListTableAdapter sp07254_UT_Surveyed_Pole_Risk_Assessment_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentType;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private System.Windows.Forms.BindingSource sp07255UTSurveyedPoleRiskAssessmentQuestionsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskQuestionID;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswer;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks7;
        private DevExpress.XtraGrid.Columns.GridColumn colMasterQuestionID;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID7;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentDateTimeRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentType1;
        private DataSet_UT_EditTableAdapters.sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_ListTableAdapter sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerText1;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerText2;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerText3;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colMEWPArialRescuers;
        private DevExpress.XtraGrid.Columns.GridColumn colEmergencyKitLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colAirAmbulanceLat;
        private DevExpress.XtraGrid.Columns.GridColumn colAirAmbulanceLong;
        private DevExpress.XtraGrid.Columns.GridColumn colBanksMan;
        private DevExpress.XtraGrid.Columns.GridColumn colEmergencyServicesMeetPoint;
        private DevExpress.XtraGrid.Columns.GridColumn colMobilePhoneSignalBars;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestAandE;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestLandLine;
        private DevExpress.XtraGrid.Columns.GridColumn colAchievableClearance;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMeters;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleFlail;
        private DevExpress.XtraEditors.LabelControl labelControlStep;
        private DevExpress.XtraEditors.GroupControl groupControlMissingInfo;
        private DevExpress.XtraEditors.LabelControl labelFinishSurveyWarningInfo;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl ItemForFiveYearClearanceAchieved;
        private DevExpress.XtraEditors.CheckEdit FiveYearClearanceAchievedCheckEdit;
        private DevExpress.XtraEditors.TextEdit InvoiceNumberTextEdit;
        private DevExpress.XtraEditors.DateEdit DateInvoicedDateEdit;
        private DevExpress.XtraEditors.CheckEdit SpanInvoicedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpanInvoiced;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateInvoiced;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvoiceNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShortDate;
        private DevExpress.XtraEditors.DateEdit RevisitDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRevisitDate;
        private DevExpress.XtraEditors.LabelControl RemarksInformationLabel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.GridLookUpEdit ClearanceDistanceGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView24;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClearanceDistance;
        private System.Windows.Forms.BindingSource sp07356UTClearanceDistancesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07356_UT_Clearance_Distances_With_BlankTableAdapter sp07356_UT_Clearance_Distances_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraEditors.GridLookUpEdit DeferralReasonIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView25;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferralReasonID;
        private DevExpress.XtraEditors.MemoEdit DeferralRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferralRemarks;
        private System.Windows.Forms.BindingSource sp07357UTDeferralReasonsWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07357_UT_Deferral_Reasons_With_BlankTableAdapter sp07357_UT_Deferral_Reasons_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours1;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours1;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours2;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours2;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.GridControl gridControl26;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView26;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit23;
        private System.Windows.Forms.BindingSource sp07367UTTrafficManagementForSurveyedPoleBindingSource;
        private DataSet_UT_EditTableAdapters.sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private System.Windows.Forms.BindingSource sp07366UTTrafficManagementItemsWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07366_UT_Traffic_Management_Items_With_BlankTableAdapter sp07366_UT_Traffic_Management_Items_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.ButtonEdit TrafficMapPathButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTrafficMapPath;
        private DevExpress.XtraEditors.ButtonEdit AccessMapPathButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccessMapPath;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl27;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView27;
        private System.Windows.Forms.BindingSource sp07401UTPDLinkedToActionsBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colSignatureFile2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn colEmergencyAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestTelephonePoint;
        private DevExpress.XtraGrid.Columns.GridColumn colGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveAccessAvailable;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessAgreedWithLandOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessDetailsOnMap;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadsideAccessOnly;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedRevisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTPOTree;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningConservation;
        private DevExpress.XtraGrid.Columns.GridColumn colWildlifeDesignation;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecialInstructions;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimarySubName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimarySubNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLineNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLVSubName;
        private DevExpress.XtraGrid.Columns.GridColumn colLVSubNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colManagedUnitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colEnvironmentalRANumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLandscapeImpactNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionEmailed;
        private DevExpress.XtraGrid.Columns.GridColumn colPostageRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByPost;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerSalutation;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerEmail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn colContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsChipRemove;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsChipOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsStackOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsOther;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToDoActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerType;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryScreenName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit0DP;
        private DataSet_UT_WorkOrderTableAdapters.sp07401_UT_PD_Linked_To_ActionsTableAdapter sp07401_UT_PD_Linked_To_ActionsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryScreenName1;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName1;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.GridLookUpEdit ClearanceDistanceUnderIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClearanceDistanceUnderID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrowthRate;
        private DevExpress.XtraGrid.Columns.GridColumn colDeadTree;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.CheckEdit IvyOnPoleCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SpanResilientCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpanResilient;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIvyOnPole;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraEditors.GridLookUpEdit LinearCutLengthIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinearCutLengthID;
        private System.Windows.Forms.BindingSource sp07464UTLinearCutLengthsWithBlankBindingSource;
        private DataSet_UTTableAdapters.sp07464_UT_Linear_Cut_Lengths_With_BlankTableAdapter sp07464_UT_Linear_Cut_Lengths_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private DevExpress.XtraGrid.Columns.GridColumn colFromValue;
        private DevExpress.XtraGrid.Columns.GridColumn colToValue;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks5;
        private DevExpress.XtraEditors.GridLookUpEdit HeldUpTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHeldUpTypeID;
        private System.Windows.Forms.BindingSource sp07465UTHeldUpTypesWithBlankBindingSource;
        private DataSet_UTTableAdapters.sp07465_UT_Held_Up_Types_With_BlankTableAdapter sp07465_UT_Held_Up_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending26;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending12;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending7;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending27;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending9;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending10;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending22;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.SpinEdit ShutdownHoursSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShutdownHours;
        private DevExpress.XtraEditors.SpinEdit DeferredEstimatedCuttingHoursSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeferredEstimatedCuttingHours;
        private DevExpress.XtraEditors.GridLookUpEdit ClearanceDistanceAboveIDFGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClearanceDistanceAboveID;
    }
}
